﻿-- 테이블 순서는 관계를 고려하여 한 번에 실행해도 에러가 발생하지 않게 정렬되었습니다.

DROP TABLE APPRMI_H
DROP TABLE APPRMI_FII
DROP TABLE APPRMI_NAD
DROP TABLE APPRMI_RFF
DROP TABLE BANKCODE_SWIFT

-- APPRMI_H Table Create SQL
CREATE TABLE APPRMI_H
(
    "MAINT_NO"          varchar(35)     NOT NULL, 
    "APP_DT"            varchar(10)     NOT NULL, 
    "REG_DT"            datetime        NOT NULL    DEFAULT getdate(), 
    "USER_ID"           varchar(10)     NOT NULL, 
    "TRN_HOPE_DT"       varchar(10)     NOT NULL, 
    "IMPT_SCHEDULE_DT"  varchar(10)     NULL, 
    "ETC_NO"            varchar(70)     NULL, 
    "TRD_CD"            varchar(3)      NOT NULL, 
    "IMPT_REMIT_CD"     varchar(3)      NOT NULL, 
    "IMPT_TRAN_FEE_CD"  varchar(3)      NULL, 
    "REMIT_DESC"        varchar(350)    NULL, 
    "REMIT_CD"          varchar(1)      NULL, 
    "ADDED_FEE_TAR"     varchar(3)      NOT NULL, 
    "TOTAL_AMT"         money           NOT NULL, 
    "TOTAL_AMT_UNIT"    varchar(3)      NOT NULL, 
    "FORE_AMT"          money           NULL, 
    "FORE_AMT_UNIT"     varchar(3)      NULL, 
    "ONLY_PWD"          varchar(17)     NOT NULL, 
    "AUTH_NM1"          varchar(35)     NULL, 
    "AUTH_NM2"          varchar(35)     NULL, 
    "AUTH_NM3"          varchar(35)     NULL, 
    "MESSAGE1"          varchar(3)      NULL, 
    "MESSAGE2"          varchar(3)      NULL, 
    "CHK2"              int             NOT NULL    DEFAULT 0, 
    "CHK3"              int             NOT NULL    DEFAULT 0, 
    PRIMARY KEY (MAINT_NO)
)
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문서번호', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'MAINT_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '신청일자', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'APP_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '등록일자', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'REG_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '작성아이디', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'USER_ID'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '이체희망일자', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'TRN_HOPE_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입예정일', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'IMPT_SCHEDULE_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '기타번호', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'ETC_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '거래형태구분', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'TRD_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입송금신청용도코드', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'IMPT_REMIT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수수료유형코드', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'IMPT_TRAN_FEE_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '송금내역', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'REMIT_DESC'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '송금방법', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'REMIT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '부가수수료부담자', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'ADDED_FEE_TAR'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '총 송금액', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'TOTAL_AMT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '총 송금액 통화단위', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'TOTAL_AMT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '외화계좌 송금액', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'FORE_AMT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '외화계좌 송금액 통화단위', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'FORE_AMT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '송금신청업무 전용 PASSWORD등을 입력', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'ONLY_PWD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '명의인1', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'AUTH_NM1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '명의인2', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'AUTH_NM2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '식별부호(전자서명)', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'AUTH_NM3'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문서저장구분', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'CHK2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문저전송구분', 
   'user', dbo, 'table', 'APPRMI_H', 'column', 'CHK3'
GO


-- APPRMI_H Table Create SQL
CREATE TABLE APPRMI_FII
(
    "MAINT_NO"    varchar(35)    NOT NULL, 
    "TAG"         varchar(5)     NOT NULL, 
    "ORD"         int            NOT NULL, 
    "ACCNT_NO"    varchar(17)    NOT NULL, 
    "ACCNT_NM"    varchar(35)    NOT NULL, 
    "ACCNT_UNIT"  varchar(3)     NULL, 
    "BNK_CD"      varchar(11)    NULL, 
    "BNK_NM1"     varchar(35)    NULL, 
    "BNK_NM2"     varchar(35)    NULL, 
    "BNK_NM3"     varchar(35)    NULL, 
    "BNK_NM4"     varchar(35)    NULL, 
    "BNK_NAT_CD"  varchar(3)     NULL, 
    PRIMARY KEY (MAINT_NO, TAG, ORD)
)
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문서번호', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'MAINT_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '한정어', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'TAG'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '우선순위', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'ORD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '계좌번호', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'ACCNT_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '계좌주', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'ACCNT_NM'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '통화코드', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'ACCNT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행코드', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행명', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_NM1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행명 추가', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_NM2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행지점명', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_NM3'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행지점명 추가', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_NM4'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '의뢰은행국가코드', 
   'user', dbo, 'table', 'APPRMI_FII', 'column', 'BNK_NAT_CD'
GO


-- APPRMI_H Table Create SQL
CREATE TABLE APPRMI_NAD
(
    "MAINT_NO"   varchar(35)    NOT NULL, 
    "TAG"        varchar(5)     NOT NULL, 
    "CODE"       varchar(10)    NULL, 
    "BUS_NO"     varchar(17)    NOT NULL, 
    "BUS_NM1"    varchar(35)    NOT NULL, 
    "BUS_NM2"    varchar(35)    NULL, 
    "BUS_NM3"    varchar(35)    NULL, 
    "BUS_ADDR1"  varchar(35)    NOT NULL, 
    "BUS_ADDR2"  varchar(35)    NULL, 
    "BUS_ADDR3"  varchar(35)    NULL, 
    "NAT_CD"     varchar(3)     NULL, 
    "TEL_NO"     varchar(25)    NULL, 
    PRIMARY KEY (MAINT_NO, TAG)
)
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문서번호', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'MAINT_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '한정어', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'TAG'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '코드', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'CODE'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '사업자등록번호', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상호1', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_NM1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상호2', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_NM2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상호3', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_NM3'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '주소1', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_ADDR1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '주소2', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_ADDR2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '주소3', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'BUS_ADDR3'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '국가코드', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'NAT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '전화번호', 
   'user', dbo, 'table', 'APPRMI_NAD', 'column', 'TEL_NO'
GO


-- APPRMI_H Table Create SQL
CREATE TABLE APPRMI_RFF
(
    "MAINT_NO"          varchar(35)    NOT NULL, 
    "SEQ"               int            NOT NULL    IDENTITY, 
    "RFF_NO"            varchar(35)    NOT NULL, 
    "AMT"               money          NOT NULL, 
    "AMT_UNIT"          varchar(3)     NOT NULL, 
    "REMIT_AMT"         money          NOT NULL, 
    "REMIT_AMT_UNIT"    varchar(3)     NOT NULL, 
    "CIF_AMT"           money          NULL, 
    "CIF_AMT_UNIT"      varchar(3)     NULL, 
    "HS_CD"             varchar(10)    NULL, 
    "GOODS_NM"          varchar(35)    NULL, 
    "IMPT_CD"           varchar(3)     NULL, 
    "CONT_DT"           varchar(10)    NULL, 
    "TOD_CD"            varchar(3)     NULL, 
    "TOD_REMARK"        varchar(70)    NULL, 
    "EXPORT_NAT_CD"     varchar(2)     NULL, 
    "EXPORT_OW_NM"      varchar(35)    NULL, 
    "EXPORT_OW_ADDR"    varchar(35)    NULL, 
    "EXPORT_OW_NAT_CD"  varchar(35)    NULL, 
    "DETAIL_DESC1"      varchar(70)    NULL, 
    "DETAIL_DESC2"      varchar(70)    NULL, 
    "DETAIL_DESC3"      varchar(70)    NULL, 
    "DETAIL_DESC4"      varchar(70)    NULL, 
    "DETAIL_DESC5"      varchar(70)    NULL, 
    "IMPORT_OW_NM"      varchar(35)    NULL, 
    "IMPORT_OW_ADDR"    varchar(35)    NULL, 
    "IMPORT_OW_NAT_CD"  varchar(35)    NULL, 
    "REG_DT"            datetime       NULL        DEFAULT getdate(), 
    "TMP_DEL"           bit            NOT NULL    DEFAULT 0, 
    PRIMARY KEY (MAINT_NO, SEQ)
)
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '문서번호', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'MAINT_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '순번', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'SEQ'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '계약서번호 또는 수입신고필증번호', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'RFF_NO'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '결제금액 또는 계약금액', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'AMT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '통화코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'AMT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '신청금액(송금금액)', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'REMIT_AMT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '통화코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'REMIT_AMT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '과세가격', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'CIF_AMT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '통화코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'CIF_AMT_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    'HS코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'HS_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '대표물품명', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'GOODS_NM'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입용도', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'IMPT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '계약일자', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'CONT_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '가격조건', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'TOD_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '가격조건이 ZZZ일때 평문입력', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'TOD_REMARK'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '국가코드 ISO TDED', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'EXPORT_NAT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수출자상호', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'EXPORT_OW_NM'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수출자주소', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'EXPORT_OW_ADDR'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수출자국가코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'EXPORT_OW_NAT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상세정보1', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'DETAIL_DESC1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상세정보2', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'DETAIL_DESC2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상세정보3', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'DETAIL_DESC3'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상세정보4', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'DETAIL_DESC4'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '상세정보5', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'DETAIL_DESC5'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입자상호', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'IMPORT_OW_NM'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입자주소', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'IMPORT_OW_ADDR'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수입자국가코드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'IMPORT_OW_NAT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '등록일자', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'REG_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '임시삭제용 필드', 
   'user', dbo, 'table', 'APPRMI_RFF', 'column', 'TMP_DEL'
GO


-- APPRMI_H Table Create SQL
CREATE TABLE BANKCODE_SWIFT
(
    "BANK_CD"        varchar(11)    NOT NULL, 
    "BANK_NM1"       varchar(35)    NOT NULL, 
    "BANK_NM2"       varchar(35)    NULL, 
    "BANK_BNCH_NM1"  varchar(35)    NULL, 
    "BANK_BNCH_NM2"  varchar(35)    NULL, 
    "ACCT_NM"        varchar(35)    NOT NULL, 
    "BANK_NAT_CD"    varchar(2)     NOT NULL, 
    "IS_USE"         bit            NOT NULL    DEFAULT 1, 
    "BANK_UNIT"      varchar(3)     NOT NULL, 
    "REG_DT"         datetime       NOT NULL    DEFAULT getdate(), 
    "UPT_DT"         datetime       NOT NULL    DEFAULT getdate(), 
    PRIMARY KEY (BANK_CD)
)
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '외국환 은행 SWIFT코드', 
   'user', dbo, 'table', 'BANKCODE_SWIFT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    'SWIFT 코드', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '은행명', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_NM1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '은행명', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_NM2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '지점명', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_BNCH_NM1'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '지점명', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_BNCH_NM2'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '계좌주', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'ACCT_NM'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '은행국가코드', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_NAT_CD'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '사용여부', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'IS_USE'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '통화단위', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'BANK_UNIT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '등록일자', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'REG_DT'
GO

EXECUTE sys.sp_addextendedproperty 'MS_Description',
    '수정일자', 
   'user', dbo, 'table', 'BANKCODE_SWIFT', 'column', 'UPT_DT'
GO

INSERT INTO KIS_COMM.dbo.BANKCODE_SWIFT (BANK_CD, BANK_NM1, BANK_NM2, BANK_BNCH_NM1, BANK_BNCH_NM2, ACCT_NM, BANK_NAT_CD, IS_USE, BANK_UNIT) VALUES (N'COUTGB22XXX', N'COUTTS AND CO', null, null, null, N'SHRIMPS LDN LTD.', N'GB', 1, N'GBP');
