unit CreateDocuments;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Dialogs,
  StdCtrls, ExtCtrls, FileCtrl, ADODB, DB, StrUtils, EDIFACT, Clipbrd, Forms, DateUtils;

//------------------------------------------------------------------------------
// 2018-10-12 SWIFT NEW
//------------------------------------------------------------------------------
  function APP700_NEW(MAINT_NO,MSEQ,RecvCode : String):String;
  function APP707_NEW(MAINT_NO,MSEQ,AMDNO,RecvCode : String):String;
//------------------------------------------------------------------------------
// 2019-08-20 PAYORD 지급지시서 추가
//------------------------------------------------------------------------------
  function PAYORD(MAINT_NO, RecvCode : String):String;
  function APPCOP(MAINT_NO, RecvCode : String):String;
  
  function LOCAPP(MAINT_NO,RecvCode : STRING):String;
  function APPPCR(MAINT_NO,RecvCode : String):String;
  function APP700(MAINT_NO,RecvCode : String):String;
  function APP707(MAINT_NO,RecvCode : String):String;
  function APPLOG(MAINT_NO,RecvCode : String):String;
  function DOMOFC(MAINT_NO,RecvCode : String):String;
  function LOCAMR(MAINT_NO:string;MSEQ : Integer;RecvCode : String):String;
  function VATBI1(MAINT_NO,RecvCode : String):String;
  function VATBI3(MAINT_NO,RecvCode : String):String;
  function LOCRC2(MAINT_NO,RecvCode : String):String;
  function APPSPC(MAINT_NO,RecvCode : String):String;
//------------------------------------------------------------------------------
// 날짜: 2019.03.22
// 작성함수 : APPSPC_NEW
// 작성인 : 최두일
//------------------------------------------------------------------------------
  function APPSPC_NEW(MAINT_NO,BGM1,BGM2,BGM3,BGM4,RecvCode : String):String;
//------------------------------------------------------------------------------
// 2021-05-25
// 이덕수
// 수입송금신청서(APPRMI)
//------------------------------------------------------------------------------
  function APPRMI(MAINT_NO, RecvCode : String):String;


  procedure ClearBuffer;
  Procedure AddBuffer(Str : String;bNewLine : Boolean = True);
  procedure AddBufferChild(Str : String); overload;
  procedure AddBufferChild(value : Integer); overload;
  procedure AddBufferChild(value : Currency); overload;
  procedure AddBufferChildTime(sType,value : String; sFormatDate : String = '101');

Var
  Buffer : String;

implementation

uses DocumentQry, Commonlib, CD_TERM;

Const
  NEWLINE = #13#10;
  BLANK4 = '    ';

procedure ClearBuffer;
begin
  Buffer := '';
end;

Procedure AddBuffer(Str : String;bNewLine : Boolean = True);
begin
  IF bNewLine Then
    Buffer := Buffer + Str + NEWLINE
  else
    Buffer := Buffer + Str;
end;

procedure AddBufferChild(Str : String);
begin
  Buffer := Buffer + BLANK4 + Str + NEWLINE;
end;

procedure AddBufferChild(value : Integer);
begin
  Buffer := Buffer + BLANK4 + IntToStr(value) + NEWLINE;
end;

procedure AddBufferChild(value : Currency);
begin
  Buffer := Buffer + BLANK4 + CurrToStr(value) + NEWLINE;
end;

//------------------------------------------------------------------------------
// 시간별 포맷
//------------------------------------------------------------------------------
// 101 : YYMMDD
// 102 : CCYYMMDD
// 804 : Day
// 717 : YYMMDDYYMMDD
procedure AddBufferChildTime(sType,value : String; sFormatDate : String = '101');
var
  TempStr : String;
begin
  IF not AnsiMatchText(sFormatDate,['101','102','804']) Then
    raise Exception.Create('올바른 포맷값을 입력하세요'#13#10'101: YYMMDD / 102: CCYYMMDD');

  TempStr := value;
  IF sFormatDate = '101' Then
    TempStr := FormatDateTime('YYMMDD',ConvertStr2Date(value))
  else
  IF sFormatDate = '102' Then
    TempStr := FormatDateTime('YYYYMMDD',ConvertStr2Date(value));

  AddBufferChild(UpperCase(sType));
  AddBufferChild(TempStr);
  AddBufferChild(sFormatDate);
end;

function LOCAPP(MAINT_NO,RecvCode : STRING):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;

  nLoop,CountIdx,nLoopSub : Integer;
  StringBuffer : TStringList;
  StringFTX : TStringList;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;

  try
    DMDocumentQry.OPEN_LOCAPP(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := '2AD' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'LOCAPP ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['LOCAPP']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    //BGM-----------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    AddBufferChild('2AD');
    AddBufferChild(MAINT_NO);
    AddBufferChild('9');
    AddBufferChild('AB');

    //BUS-----------------------------------------------------------------------
    AddBuffer('BUS 1      11');
    AddBufferChild('1');
    AddBufferChild(DMDocumentQry.getStr('LOCAPP','BUSINESS')); //개설근거별 용도

    //REF-----------------------------------------------------------------------
    //REF 세번부호
    AddBuffer('RFF 1      12');
    AddBufferChild('HS');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BSN_HSCODE') );

    //REF 물품매도 확약서 번호  1..9
    AddBuffer('RFF 2      12');
    AddBufferChild('AAG');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','OFFERNO1') );
    CountIdx := 3;

    for nLoop := 2 to 9 do
    begin
      IF Trim(DMDocumentQry.getStr('LOCAPP','OFFERNO'+IntToStr(nLoop))) = '' Then Continue;

      AddBuffer('REF '+IntToStr(nLoop+1)+'      12');
      AddBufferChild('AAG');
      AddBufferChild( DMDocumentQry.getStr('LOCAPP','OFFERNO'+IntToStr(nLoop)) );
      inc(CountIdx);

    end;

    //RFF 개설회차
    AddBuffer('RFF '+IntToStr(CountIdx)+'      12');
    AddBufferChild('2AG');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','OPEN_NO') );

    //DTM 개설신청일자
    AddBuffer('DTM 1      13');
    AddBufferChildTime('2AA',DMDocumentQry.getStr('LOCAPP','APP_DATE'),'101');
//    AddBufferChild('2AA');
//    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APP_DATE') );
//    AddBufferChild('101');

    //DTM 서류제시기간
    AddBuffer('DTM 2      13');
    AddBufferChild('272');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','DOC_PRD') );
    AddBufferChild('804');

    //DTM 물품 인도기일
    AddBuffer('DTM 3      13');
    AddBufferChildTime('2',DMDocumentQry.getStr('LOCAPP','DELIVERY'),'101');
//    AddBufferChild('2');
//    AddBufferChild( DMDocumentQry.getStr('LOCAPP','DELIVERY') );
//    AddBufferChild('101');

    //DTm 유효기일
    AddBuffer('DTM 4      13');
    AddBufferChildTime('123',DMDocumentQry.getStr('LOCAPP','EXPIRY'),'101');
//    AddBufferChild('123');
//    AddBufferChild( DMDocumentQry.getStr('LOCAPP','EXPIRY') );
//    AddBufferChild('101');

    //TSR 허용/불허
    AddBuffer('TSR 1      14');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','TRANSPRT') );

    //FTX 대표공급물품명
    StringFTX.Text := DMDocumentQry.getStr('LOCAPP','GOODDES1');
    AddBuffer('FTX 1      15');
    AddBufferChild('AAA');
    for nLoop := 0 to StringFTX.Count-1 do
    begin
      AddBufferChild(StringFTX.Strings[nLoop]);
    end;
    //빈공간
    for nLoop := 0 to 4-(StringFTX.Count) do
    begin
      AddBufferChild('');
    end;

    //FTX 기타정보
    IF DMDocumentQry.getStr('LOCAPP','REMARK1') <> '' Then
    begin
      StringFTX.Text := DMDocumentQry.getStr('LOCAPP','REMARK1');
      AddBuffer('FTX 2      15');
      AddBufferChild('ACB');
      for nLoop := 0 to StringFTX.Count-1 do
      begin
        AddBufferChild(StringFTX.Strings[nLoop]);
      end;
      //빈공간
      for nLoop := 0 to 4-(StringFTX.Count) do
      begin
        AddBufferChild('');
      end;
    end;

    //FII 은행코드
    AddBuffer('FII 1      16');
    AddBufferChild('AZ');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','AP_BANK') );
    AddBufferChild('25');
    AddBufferChild('BOK');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','AP_BANK1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','AP_BANK2') );

    //NAD 개설의뢰인
    AddBuffer('NAD 1      17');
    AddBufferChild('DF');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPLIC1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPLIC2') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPLIC3') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPADDR1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPADDR2') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','APPADDR3') );

    //NAD 수혜자
    AddBuffer('NAD 2      17');
    AddBufferChild('DG');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BNFEMAILID') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BNFDOMAIN') );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BENEFC1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BENEFC2') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BENEFC3')+DMDocumentQry.getStr('LOCAPP','CHKNAME1')+'/'+DMDocumentQry.getStr('LOCAPP','CHKNAME1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BNFADDR1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BNFADDR2') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','BNFADDR3') );

    //NAD 전자서명
    AddBuffer('NAD 3      17');
    AddBufferChild('AX');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','EXNAME1') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','EXNAME2') );
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','EXNAME3') );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');

    //DOC
    CountIdx := 1;
    for nLoop := 1 to 5 do
    begin
//      AddBuffer('DOC '+ IntToStr(nLoop) +'      18');

      IF DMDocumentQry.getStr('LOCAPP','DOCCOPY'+IntToStr(nLoop)) <> '0' Then
      begin
        AddBuffer('DOC '+ IntToStr(CountIdx) +'      18');
        Case nLoop of
          1: AddBufferChild('2AH');
          2: AddBufferChild('2AJ');
          3: AddBufferChild('2AK');
          4: AddBufferChild('2AP');
          5: AddBufferChild('310');
        end;
//        AddBufferChild('1');
        AddBufferChild(DMDocumentQry.getStr('LOCAPP','DOCCOPY'+IntToStr(nLoop)));

        //------------------------------------------------------------------------------
        // 기타구비서류
        //------------------------------------------------------------------------------
//        IF CountIdx = 1 Then
        // 2018-11-27
        // 공급자발행 물품매도확약서 사본일 경우에만 생성
        if nLoop = 5 Then
        begin
          StringFTX.Text := DMDocumentQry.getStr('LOCAPP','DOC_ETC1');
          IF Trim(stringFTX.Text) <> '' Then
          begin
            AddBuffer('FTX 1      20');
            AddBufferChild('ABX');
            for nLoopSub := 0 to StringFTX.Count-1 do
            begin
              AddBufferChild(StringFTX.Strings[nLoopSub]);
            end;
            //빈공간
            for nLoopSub := 0 to 4-(StringFTX.Count) do
            begin
              AddBufferChild('');
            end;
          end;
        end;

        Inc(CountIdx);
      end
      else
        Continue;
    end;

    //BUS
    AddBuffer('BUS 1      19');
    AddBufferChild( DMDocumentQry.getStr('LOCAPP','LOC_TYPE') );

    //MOA
    AddBuffer('MOA 1      21');
    IF DMDocumentQry.getStr('LOCAPP','LOC_AMTC') = 'KRW' THEN
      AddBufferChild('2AE')
    else
      AddBufferChild('2AD');
    AddBufferChild(DMDocumentQry.getCurrency('LOCAPP','LOC_AMT'));
    AddBufferChild(DMDocumentQry.getStr('LOCAPP','LOC_AMTC'));

    //PCD
    IF not AnsiMatchText( Trim(DMDocumentQry.getStr('LOCAPP','CD_PERP')) , ['','0'] ) Then
    begin
      AddBuffer('PCD 1      22');
      AddBufferChild('13');
      IF not AnsiMatchText( Trim(DMDocumentQry.getStr('LOCAPP','CD_PERM')) , ['','0'] ) Then
        AddBufferChild(DMDocumentQry.getStr('LOCAPP','CD_PERP')+'.'+DMDocumentQry.getStr('LOCAPP','CD_PERM'))
      else
        AddBufferChild(DMDocumentQry.getStr('LOCAPP','CD_PERP')+'.0');
    end;

    // 원수출신용장
    IF Trim(DMDocumentQry.getStr('LOCAPP','DOC_DTL')) <> '' Then
    begin
      AddBuffer('DOC 1      1A');
      AddBufferChild(Trim(DMDocumentQry.getStr('LOCAPP','DOC_DTL')));

      //RFF
      AddBuffer('RFF 1      23');
      AddBufferChild('AAC');
      AddBufferChild( Trim(DMDocumentQry.getStr('LOCAPP','DOC_NO')) );

      //MOA 결제통화 및 금액표시
      IF Trim( DMDocumentQry.getStr('LOCAPP','DOC_AMTC') ) <> '' Then
      begin
        AddBuffer('MOA 1      24');
        AddBufferChild('212');
        AddBufferChild( DMDocumentQry.getStr('LOCAPP','DOC_AMT'));
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','DOC_AMTC') ) );
      end;

      CountIdx := 1;
      //DTM 선적(인도)기일을 표시
      IF Trim( DMDocumentQry.getStr('LOCAPP','LOADDATE') ) <> '' Then
      begin
        AddBuffer('DTM '+IntToStr(CountIdx)+'      25');
        AddBufferChild('38');
        AddBufferChild(RightStr(Trim( DMDocumentQry.getStr('LOCAPP','LOADDATE') ),6));
        AddBufferChild('101');
        Inc(CountIdx);
      end;

      //DTM 유효기일을 표시
      IF Trim( DMDocumentQry.getStr('LOCAPP','EXPDATE') ) <> '' Then
      begin
        AddBuffer('DTM '+IntToStr(CountIdx)+'      25');
        AddBufferChild('123');
        AddBufferChild(RightStr(Trim( DMDocumentQry.getStr('LOCAPP','EXPDATE') ),6));
        AddBufferChild('101');
      end;

      //NAD 수출(공급)상대방
      IF Trim( DMDocumentQry.getStr('LOCAPP','IM_NAME1') ) <> '' Then
      begin
        AddBuffer('NAD 1      26');
        AddBufferChild('IM');
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','IM_NAME1') ) );
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','IM_NAME2') ) );
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','IM_NAME3') ) );
      end;

      //LOC 수출지역
      IF Trim( DMDocumentQry.getStr('LOCAPP','DEST') ) <> '' Then
      begin
        AddBuffer('LOC 1      27');
        AddBufferChild('28');
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','DEST') ) );
        AddBufferChild('162');
        AddBufferChild('5');
      end;

      //FII 발행은행(확인기관)표시
      IF Trim( DMDocumentQry.getStr('LOCAPP','ISBANK1') ) <> '' Then
      begin
        AddBuffer('FII 1      28');
        AddBufferChild('AZ');
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','ISBANK1') ) );
        AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','ISBANK2') ) );
      end;
    end;

    //PAT 대금결제조건
    IF Trim( DMDocumentQry.getStr('LOCAPP','PAYMENT') ) <> '' Then
    begin
      AddBuffer('PAT 1      29');
      AddBufferChild('1');
      AddBufferChild( Trim( DMDocumentQry.getStr('LOCAPP','PAYMENT') ) );
    end;

    //FTX 대표수출물품명을 표시
    IF Trim( DMDocumentQry.getStr('LOCAPP','EXGOOD1') ) <> '' Then
    begin
      AddBuffer('FTX 1      2A');
      AddBufferChild('AAA');
      StringFTX.Text := DMDocumentQry.getStr('LOCAPP','EXGOOD1');
      for nLoop := 0 to 4 do
      begin
        IF StringFTX.Count >= nLoop+1 Then
        begin
          AddBufferChild(StringFTX.Strings[nLoop]);
        end;
      end;
    end;

    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;    
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
  end;

end;

function APPPCR(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;
  DocumentCODE : string;

  nLoop : Integer;
  FTXCount : Integer;
  RecNo : integer;

  StringBuffer : TStringList;
  List1AA : TStringList;
begin
  StringBuffer := TStringList.Create;
  List1AA := TStringList.Create;

  try
    DMDocumentQry.OPEN_APPPCR(MAINT_NO);

//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    IF Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('Chgcd').AsString) = '' THEN
      DocumentCODE := '2CG'
    else
      DocumentCODE := Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('Chgcd').AsString);

    FILENAME := DocumentCODE + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'APPPCR ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['APPPCR']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    //BGM-----------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    //AddBufferChild('2CG');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('Chgcd').AsString );
    AddBufferChild('');
    AddBufferChild(MAINT_NO);
    AddBufferChild('9');
    AddBufferChild('AB');

    //NAD-----------------------------------------------------------------------
    //신청인
    AddBuffer('NAD 1      11');
    AddBufferChild('MS');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('EMAIL_ID').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('EMAIL_DOMAIN').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_NAME1').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_NAME2').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('AX_NAME2').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR1').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR2').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR3').AsString );
//    IF Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR2').AsString) <> '' then
//    begin
//      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR2').AsString );
//      IF Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR3').AsString) <> '' then
//      begin
//        AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_ADDR3').AsString );
//      end;
//    end;

    //NAD-----------------------------------------------------------------------
    //공급자
    AddBuffer('NAD 2      11');
    AddBufferChild('SU');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('EMAIL_ID').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('EMAIL_DOMAIN').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_NAME1').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_NAME2').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR3').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR1').AsString );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR2').AsString );
    AddBufferChild('');
//    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR3').AsString );
//    IF Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR2').AsString) <> '' then
//    begin
//      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR2').AsString );
//      IF Trim(DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR3').AsString) <> '' then
//      begin
//        AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('SUP_ADDR3').AsString );
//      end;
//    end;

    //BUS-----------------------------------------------------------------------
    AddBuffer('BUS 1      12');
    AddBufferChild('1');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('ITM_GBN').AsString );
    AddBufferChild(''); // Code list qualifier
    AddBufferChild(''); //Code list responsible agency, coded
    //Business description : 구매(공급)원료/물품 등의 용도명세를 평문으로 입력시 상위 구분은 'ZZZ'가 되어야함
    AddBufferChild(''); //Business description
    AddBufferChild(''); //GEOGRAPHIC ENVIRONMENT, CODE
    AddBufferChild(''); //TYPE OF FINANCIAL TRANSACTION
    AddBufferChild(''); //Bank operation, coded
    AddBufferChild(''); //Code list qualifier
    AddBufferChild(''); //Code list responsible agency, coded
    AddBufferChild(''); //INTRA-COMPANY PAYMENT, CODED

    //세부사항
    DMDocumentQry.qryAPPPCRD1.First;
    FTXCount := 1;

    while not DMDocumentQry.qryAPPPCRD1.Eof do
    begin
      //LIN---------------------------------------------------------------------
      RecNo := DMDocumentQry.qryAPPPCRD1.RecNo;
      AddBuffer( 'LIN ' + IntToStr(RecNo) + '      13' );
      AddBufferChild(IntToStr(RecNo));

      //PIA---------------------------------------------------------------------
      AddBuffer('PIA 1      20');
      AddBufferChild('1');
      AddBufferChild(DMDocumentQry.qryAPPPCRD1.FieldByName('HS_NO').AsString);

      //IMD---------------------------------------------------------------------
      List1AA.Text := DMDocumentQry.qryAPPPCRD1.FieldByName('NAME1').AsString;
      for nLoop := 0 to List1AA.Count -1 do
      begin
        AddBuffer('IMD ' + IntToStr(nLoop+1) + '      21' );
        AddBufferChild('1AA');
        AddBufferChild(List1AA.Strings[nLoop]);
      end;

      //QTY---------------------------------------------------------------------
      //단위가 없으면 PASS
      IF (Trim(DMDocumentQry.qryAPPPCRD1.FieldByName('QTYC').AsString) <> '') Then
      begin
        AddBuffer('QTY 1      22');
        AddBufferChild('1');
        AddBufferChild( CurrToStr(DMDocumentQry.qryAPPPCRD1.FieldByName('QTY').AsCurrency) );
        AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('QTYC').AsString );
      end;

      //FTX---------------------------------------------------------------------
      //규격
      List1AA.Text := DMDocumentQry.qryAPPPCRD1.FieldByName('SIZE').AsString;
      IF List1AA.Count > 0 Then
      begin
        AddBuffer('FTX ' + IntToStr(FTXCount) + '     23');
        AddBufferChild('AAA');
        for nLoop := 0 to List1AA.Count-1 do
        begin
          AddBufferChild(List1AA.Strings[nLoop]);
        end;

        for nLoop := 1 to 5-List1AA.Count do
        begin
          AddBufferChild('');
        end;        

        inc(FTXCount);
      end;

      //FTX---------------------------------------------------------------------
      //비고
      List1AA.Text := DMDocumentQry.qryAPPPCRD1.FieldByName('REMARK').AsString;
      IF List1AA.Count > 0 Then
      begin
        AddBuffer('FTX ' + IntToStr(FTXCount) + '     23');
        AddBufferChild('AAI');
        for nLoop := 0 to List1AA.Count-1 do
        begin
          AddBufferChild(List1AA.Strings[nLoop]);
        end;
      end;

      //MOA---------------------------------------------------------------------
      AddBuffer('MOA 1      24');
      AddBufferChild('203');
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('AMT1').asCurrency );
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('AMT1C').AsString );

      //PRI---------------------------------------------------------------------
      AddBuffer('PRI 1      25');
      AddBufferChild('CAL');
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('PRI2').asCurrency );
      AddBufferChild('PE');
      AddBufferChild('CUP');
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('PRI_BASE').asCurrency );
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('PRI_BASEC').asString );

      //DTM---------------------------------------------------------------------
      AddBuffer('DTM 1      26');
      AddBufferChild('1AF');
      AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('APP_DATE').asString );
      AddBufferChild('102');

      //ALC---------------------------------------------------------------------
      IF AnsiMatchText( DMDocumentQry.qryAPPPCRD1.FieldByName('ACHG_G').AsString , ['Q','S','U']) Then
      begin
        AddBuffer('ALC 1      27');
        AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('ACHG_G').AsString );
        AddBufferChild('2AZ');
        //MOA
        AddBuffer('MOA 1      30');
        AddBufferChild(8);
        AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('AC1_AMT').asCurrency );
        AddBufferChild('');

        AddBuffer('MOA 2      30');
        AddBufferChild('2AD');
        AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('AC2_AMT').asCurrency );
        AddBufferChild( DMDocumentQry.qryAPPPCRD1.FieldByName('AC1_C').AsString );
      end;

      DMDocumentQry.qryAPPPCRD1.Next;
    end;

    //CNT-----------------------------------------------------------------------
    AddBuffer('CNT 1      14');
    AddBufferChild('1AA');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('TQTY').asCurrency );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('TQTYC').AsString );

    //MOA----------------------------------------------------------------------
    AddBuffer('MOA 1      15');
    AddBufferChild('128');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('TAMT1').asCurrency );
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('TAMT1C').AsString );

    //ALC
    IF AnsiMatchText( DMDocumentQry.qryAPPPCR_H.FieldByName('CHG_G').AsString , ['Q','S','U']) Then
    begin
      AddBuffer('ALC 1      16');
      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('CHG_G').AsString );
      AddBufferChild('2AZ');
      //MOA
      AddBuffer('MOA 1      28');
      AddBufferChild(131);
      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('C2_AMT').asCurrency );
      AddBufferChild('');

      AddBuffer('MOA 2      28');
      AddBufferChild('2AD');
      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('C1_AMT').asCurrency );
      AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('C1_C').AsString );
    end;

    //근거서류------------------------------------------------------------------
    DMDocumentQry.qryAPPPCRD2.First;
    while not DMDocumentQry.qryAPPPCRD2.Eof do
    begin
      RecNo := DMDocumentQry.qryAPPPCRD2.RecNo;

      //DOC---------------------------------------------------------------------
      AddBuffer('DOC '+IntToStr(RecNo) + '      17');
      AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('DOC_G').AsString );
      AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('DOC_D').AsString );

      //근거서류 발급기관
      //FII---------------------------------------------------------------------
      IF Trim(DMDocumentQry.qryAPPPCRD2.FieldByName('BAL_CD').AsString)  <> '' Then
      begin
        AddBuffer('FII 1      29');
        AddBufferChild('4AE'); //PARTY QUALIFIER
        AddBufferChild(DMDocumentQry.qryAPPPCRD2.FieldByName('BAL_CD').AsString);       //Institution name identification
        AddBufferChild('');      //Code list qualifier
        AddBufferChild('');      //Code list Responsible agency, coded        
        AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('BAL_NAME1').AsString ); //Institution name
        AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('BAL_NAME2').AsString  ); //Institution branch place
        AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('NAT').AsString) //COUNTRY,CODE
      end;

      //PIA---------------------------------------------------------------------
      IF Trim(DMDocumentQry.qryAPPPCRD2.FieldByName('BHS_NO').AsString)  <> '' Then
      begin
        AddBuffer('PIA 1      2A');
        AddBufferChild('1');
        AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('BHS_NO').AsString );
      end;

      //IMD---------------------------------------------------------------------
      List1AA.Text := DMDocumentQry.qryAPPPCRD2.FieldByName('BNAME1').AsString;
      for nLoop := 0 to List1AA.Count -1 do
      begin
        AddBuffer('IMD ' + IntToStr(nLoop+1) + '      2B' );
        AddBufferChild('1AA');
        AddBufferChild(List1AA.Strings[nLoop]);
      end;

      //FTX---------------------------------------------------------------------
      //규격
      List1AA.Text := DMDocumentQry.qryAPPPCRD2.FieldByName('BSIZE1').AsString;
      IF List1AA.Count > 0 Then
      begin
        AddBuffer('FTX 1      2C');
        AddBufferChild('AAA');
        for nLoop := 0 to List1AA.Count-1 do
        begin
          AddBufferChild(List1AA.Strings[nLoop]);
        end;

        for nLoop := 1 to 5-List1AA.Count do
        begin
          AddBufferChild('');
        end;

      end;

      //MOA---------------------------------------------------------------------
      //총변동금액
      AddBuffer('MOA 1      2D');
      AddBufferChild('212');
      AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('BAMT').asCurrency );
      AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('BAMTC').AsString );

      //DTM---------------------------------------------------------------------
      IF Trim(DMDocumentQry.qryAPPPCRD2.FieldByName('SHIP_DATE').AsString) <> '' Then
      begin
        AddBuffer('DTM 1      2E');
        AddBufferChild(38);
        AddBufferChild( DMDocumentQry.qryAPPPCRD2.FieldByName('SHIP_DATE').AsString );
        AddBufferChild(102);
      end;

      DMDocumentQry.qryAPPPCRD2.Next;
    end;

    //세금계산서----------------------------------------------------------------
    DMDocumentQry.qryAPPPCR_TAX.First;
    while not DMDocumentQry.qryAPPPCR_TAX.Eof do
    begin
      RecNo := DMDocumentQry.qryAPPPCR_TAX.RecNo;

      //RFF---------------------------------------------------------------------
      AddBuffer('RFF '+IntToStr(RecNo) + '      18');
      AddBufferChild('FE');
      AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('BILL_NO').AsString );

      //DTM---------------------------------------------------------------------
      AddBuffer('DTM 1      2F');
      AddBufferChild('1AH');
      AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('BILL_DATE').AsString );
      AddBufferChild(102);

      //MOA---------------------------------------------------------------------
      AddBuffer('MOA 1      2G');
      AddBufferChild(11);
      AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('BILL_AMOUNT').AsString );
      AddBufferChild('KRW');

      AddBuffer('MOA 2      2G');
      AddBufferChild(150);
      AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('TAX_AMOUNT').AsString );
      AddBufferChild('KRW');

      //IMD---------------------------------------------------------------------
      for nLoop := 1 to 5 do
      begin
        IF Trim(DMDocumentQry.qryAPPPCR_TAX.FieldByName('ITEM_NAME'+IntToStr(nLoop)).AsString) <> '' then
        begin
          AddBuffer('IMD 1      2H');
          AddBufferChild('1AA');
          AddBufferChild(DMDocumentQry.qryAPPPCR_TAX.FieldByName('ITEM_NAME'+IntToStr(nLoop)).AsString);
        end;
      end;

      //FTX---------------------------------------------------------------------
      //규격
      List1AA.Text := DMDocumentQry.qryAPPPCR_TAX.FieldByName('ITEM_DESC').AsString;
      IF List1AA.Count > 0 Then
      begin
        AddBuffer('FTX 1      2I');
        AddBufferChild('AAA');
        for nLoop := 0 to List1AA.Count-1 do
        begin
          AddBufferChild(List1AA.Strings[nLoop]);
        end;
      end;

      //QTY---------------------------------------------------------------------
//      IF TRIM(DMDocumentQry.qryAPPPCR_TAX.FieldByName('QUANTITY_UNIT').AsString) <> '' Then
//      begin
        AddBuffer('QTY 1      2J');
        AddBufferChild(1);
        AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('QUANTITY').Ascurrency );
        AddBufferChild( DMDocumentQry.qryAPPPCR_TAX.FieldByName('QUANTITY_UNIT').AsString );
//      end;

      DMDocumentQry.qryAPPPCR_TAX.Next;
    end;

    //NAD-----------------------------------------------------------------------
    AddBuffer('NAD 1      19');
    AddBufferChild('AX');
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('APP_NAME1').AsString ); //이름
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('AX_NAME1').AsString ); //명의인
    AddBufferChild( DMDocumentQry.qryAPPPCR_H.FieldByName('AX_NAME3').AsString ); //전자서명

    //FII-----------------------------------------------------------------------
    AddBuffer('FII 1      1A');
    AddBufferChild('B4');
    AddBufferChild(DMDocumentQry.qryAPPPCR_H.FieldByName('BK_CD').AsString);
    AddBufferChild('25');
    AddBufferChild('BOK');
    AddBufferChild(DMDocumentQry.qryAPPPCR_H.FieldByName('BK_NAME1').AsString);
    AddBufferChild(DMDocumentQry.qryAPPPCR_H.FieldByName('BK_NAME2').AsString);


    //UNT
    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;
  finally
    DMDocumentQry.CLOSE_APPPCR;
    List1AA.Free;
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
  end;
end;

function APP700(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;

  nLoop,CountIdx,nLoopSub : Integer;
  StringBuffer : TStringList;
  StringLoop : TStringList;

begin
  StringBuffer := TStringList.Create;
  StringLoop := TStringList.Create;

  try
    DMDocumentQry.OPEN_APP700(MAINT_NO);
    TXTPATH := 'C:\WINMATE\FLATOUT\';
//    TXTPATH := 'C:\WINMATE\OUTSAVE\';
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    ForceDirectories(TXTPATH);

    FILENAME := 'EDI_APP700' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'APP700 ' , False);
    AddBuffer( Format('%-3s',['2'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['APP700']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    //BGM-----------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    AddBufferChild('460');
    AddBufferChild(MAINT_NO);
    AddBufferChild('9');
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('MESSAGE2').AsString );

    //BUS-----------------------------------------------------------------------
    //[APP700]page2 - Form of Documentary Credit(DOC_CD)
    AddBuffer('BUS 1      11');
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('DOC_CD').AsString );

    //INP-----------------------------------------------------------------------
      {[APP700] 4403 = 5  : page1 - 개설방법(IN_MATHOD)
                4403 = 4  : page5 - Confirmation Instructions(CONFIRMM)
                4403 = 55 : page4 - 운송수단(CARRIAGE)
      }
    CountIdx := 1;
    if DMDocumentQry.qryAPP7001.FieldByName('IN_MATHOD').AsString <> '' then
    begin
      AddBuffer('INP '+ IntToStr(CountIdx) +'      12');
      AddBufferChild('1');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('IN_MATHOD').AsString );
      Inc(CountIdx);
    end;

    if DMDocumentQry.qryAPP7003.FieldByName('CONFIRMM').AsString <> '' then
    begin
      AddBuffer('INP '+ IntToStr(CountIdx) +'      12');
      AddBufferChild('1');
      AddBufferChild('4');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('CONFIRMM').AsString );
      Inc(CountIdx);
    end;

    if DMDocumentQry.qryAPP7003.FieldByName('CARRIAGE').AsString <> '' then
    begin
      AddBuffer('INP '+ IntToStr(CountIdx) +'      12');
      AddBufferChild('1');
      AddBufferChild('55');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('CARRIAGE').AsString );
      Inc(CountIdx);
    end;

    //FCA-----------------------------------------------------------------------
    //[APP700]page6 - CHARGE(CHARGE)
    AddBuffer('FCA 1      13');
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('CHARGE').AsString );

    //DTM-----------------------------------------------------------------------
    //송신보낸 날짜
    AddBuffer('DTM 1      14');
    AddBufferChildTime('2AA', FormatDateTime('YYYYMMDD', Now) ,'101');
    //[APP700]page6 - Period for Presentation(PERIOD)
    AddBuffer('DTM 2      14');
    AddBufferChildTime('272', DMDocumentQry.qryAPP7003.FieldByName('PERIOD').AsString ,'804');

    //TSR-----------------------------------------------------------------------
      {[APP700]page4 - Transhipment(TSHIP)
               page4 - Partial Shipments(PSHIP) }
    AddBuffer('TSR 1      15');
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('TSHIP').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('PSHIP').AsString );

    //PAI-----------------------------------------------------------------------
    //[APP700]page1 - 신용공여(AD_PAY)
    IF Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_PAY').AsString)  <> '' Then
    begin
      AddBuffer('PAI 1      16');
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_PAY').AsString );
    END;

    //PAT-----------------------------------------------------------------------
    //[APP700]page3 - Drafts at ... 화환어음조건 (DRAFT1,DRAFT2,DRAFT3)
    CountIdx := 1;
    for nLoop := 1 to 3 do
    begin
     if (Trim(DMDocumentQry.qryAPP7002.FieldByName('DRAFT'+IntToStr(nLoop)).AsString) <> '') then
     begin
      AddBuffer('PAT '+ IntToStr(CountIdx) +'      17');
      AddBufferChild('2AB');
      AddBufferChild('2AO');
      AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('DRAFT'+IntToStr(nLoop)).AsString );
      Inc(CountIdx);
     end;
    end;

    //[APP700]page3 - Mixed Payment Detail(혼합지급조건)(MIX_PAY1,MIX_PAY2,MIX_PAY3,MIX_PAY4)
    //Drafts at ... 화환어음조건의 값이 없을 경우 버퍼에 추가
    if (Trim(DMDocumentQry.qryAPP7002.FieldByName('DRAFT1').AsString) = '') and
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('DRAFT2').AsString) = '') and
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('DRAFT3').AsString) = '') then
    begin
      for nLoop := 1 to 4 do
      begin
       if (Trim(DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY'+IntToStr(nLoop)).AsString) <> '') then
       begin
        AddBuffer('PAT '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('6');
        AddBufferChild('2AM');
        AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY'+IntToStr(nLoop)).AsString );
        Inc(CountIdx);
       end;
      end;

      //[APP700]page3 - Deferred Payment Details(연지급조건명세)(DEF_PAY1,DEF_PAY2,DEF_PAY3,DEF_PAY4)
      //Drafts at ... 화환어음조건의 값과 Mixed Payment Detail(혼합지급조건)이 둘다 없을 경우 버퍼에 추가
      if (Trim(DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY1').AsString) = '') and
         (Trim(DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY2').AsString) = '') and
         (Trim(DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY3').AsString) = '') and
         (Trim(DMDocumentQry.qryAPP7002.FieldByName('MIX_PAY4').AsString) = '') then
      begin
        for nLoop := 1 to 4 do
        begin
         if (Trim(DMDocumentQry.qryAPP7002.FieldByName('DEF_PAY'+IntToStr(nLoop)).AsString) <> '') then
         begin
          AddBuffer('PAT '+ IntToStr(CountIdx) +'      17');
          AddBufferChild('4');
          AddBufferChild('2AN');
          AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('DEF_PAY'+IntToStr(nLoop)).AsString );
          Inc(CountIdx);
         end;
        end;
      end;
    end;

    //FTX-----------------------------------------------------------------------
    //[APP700]page1 - 기타정보(AD_INFO1~AD_INFO5)
    IF (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_INFO1').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_INFO2').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_INFO3').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_INFO4').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_INFO5').AsString)  <> '')  Then
    begin
      AddBuffer('FTX 1      18');
      AddBufferChild('ACB');
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_INFO1').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_INFO2').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_INFO3').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_INFO4').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_INFO5').AsString );
    END;

    //FII-----------------------------------------------------------------------
    //[APP700]page1 - 개설의뢰은행(AP_BANK)
    AddBuffer('FII 1      19');
    AddBufferChild('AW');
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AP_BANK').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AP_BANK1').AsString + DMDocumentQry.qryAPP7001.FieldByName('AP_BANK2').AsString);
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AP_BANK3').AsString + DMDocumentQry.qryAPP7001.FieldByName('AP_BANK4').AsString);
      //COMM - 개설의뢰은행 TEL (조건중 3055 = DG인경우 사용 이부분 확인해야함 기존상역프로그램은 DG가 아니여도 추가됨 )
      IF Trim(DMDocumentQry.qryAPP7001.FieldByName('AP_BANK5').AsString) <> '' then
      BEGIN
        AddBuffer('COM 1      19');
        AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AP_BANK5').AsString );
        AddBufferChild('TE');
      end;
    //[APP700]page1 - 통지은행(AD_BANK)
    IF (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_BANK').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_BANK1').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_BANK2').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_BANK3').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7001.FieldByName('AD_BANK4').AsString) <> '') then
    begin
      AddBuffer('FII 2      19');
      AddBufferChild('2AA');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_BANK1').AsString + DMDocumentQry.qryAPP7001.FieldByName('AD_BANK2').AsString);
      AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('AD_BANK3').AsString + DMDocumentQry.qryAPP7001.FieldByName('AD_BANK4').AsString);
    end;

    //NAD-----------------------------------------------------------------------
    //[APP700]page2 - APPLICANT(APPLIC1~APPLIC4)
    AddBuffer('NAD 1      1A');
    AddBufferChild('DF');
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('APPLIC1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('APPLIC2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('APPLIC3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('APPLIC4').AsString );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
      //APPLICANT - TEL (APPLIC5)
      if Trim(DMDocumentQry.qryAPP7002.FieldByName('APPLIC5').AsString) <> '' then
      begin
        AddBuffer('COM 1      1A');
        AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('APPLIC5').AsString );
        AddBufferChild('TE');
      end;
    //[APP700]page2 - Beneficiary(BENEFC1~BENEFC5)
    AddBuffer('NAD 2      1A');
    AddBufferChild('DG');
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('BENEFC1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('BENEFC2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('BENEFC3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('BENEFC4').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('BENEFC5').AsString );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    //[APP700]page1 - 명의인  (EX_NAME1,EX_NAME2,EX_NAME3,EX_ADDR1,EX_ADDR2)
    AddBuffer('NAD 3      1A');
    AddBufferChild('2AE');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('EX_NAME1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('EX_NAME2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('EX_NAME3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('EX_ADDR1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('EX_ADDR2').AsString );

    //DTM-----------------------------------------------------------------------
    //[APP700]page2 - Date and Place of Expiry-Date(EX_DATE)
    AddBuffer('DTM 1      1B');
    AddBufferChildTime('123', DMDocumentQry.qryAPP7001.FieldByName('EX_DATE').AsString ,'101');

    //[APP700]page2 - Date and Place of Expiry-Date(EX_DATE)
    AddBuffer('LOC 1      1B');
    AddBufferChild('143');
    AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('EX_PLACE').AsString );

    //MOA-----------------------------------------------------------------------
    //[APP700]page2 - Currency Code,Amount(CD_AMT,CD_CUR)
    AddBuffer('MOA 1      1C');
    AddBufferChild('212');
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('CD_AMT').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('CD_CUR').AsString );

    //FTX-----------------------------------------------------------------------
    //[APP700]page2 - Additional Amount Covered(AA_CV1~AA_CV4)
    IF (Trim(DMDocumentQry.qryAPP7002.FieldByName('AA_CV1').AsString)  <> '') or
      (Trim(DMDocumentQry.qryAPP7002.FieldByName('AA_CV2').AsString)  <> '') or
      (Trim(DMDocumentQry.qryAPP7002.FieldByName('AA_CV3').AsString)  <> '') or
      (Trim(DMDocumentQry.qryAPP7002.FieldByName('AA_CV4').AsString)  <> '')   Then
    begin
       AddBuffer('FTX 1      1C');
       AddBufferChild('ABT');
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('AA_CV1').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('AA_CV2').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('AA_CV3').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('AA_CV4').AsString );
    end;

    //ALC-----------------------------------------------------------------------
    //[APP700]page2 - Maximum Credit Amount(CD_MAX)
    if Trim(DMDocumentQry.qryAPP7002.FieldByName('CD_MAX').AsString) <>'' then
    begin
      AddBuffer('ALC 1      20');
      AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('CD_MAX').AsString);
    end;

    //PCD-----------------------------------------------------------------------
    //[APP700]page2 -Percentage Credit Amount Tolerance(CD_PERP,CD_PERM)
    if (DMDocumentQry.qryAPP7002.FieldByName('CD_PERP').AsCurrency > 0) OR (DMDocumentQry.qryAPP7002.FieldByName('CD_PERM').AsCurrency > 0) then
    begin
      AddBuffer('PCD 1      20');
      AddBufferChild('13');
      AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('CD_PERP').AsString + '.' + DMDocumentQry.qryAPP7002.FieldByName('CD_PERM').AsString);
    end;

    //LOC-----------------------------------------------------------------------
    //[APP700]page4 - 선적항,도착항,수탁발송지,최종목적지
    if DMDocumentQry.qryAPP7003.FieldByName('CARRIAGE').AsString <> 'DQ' then
    begin
      if Trim(DMDocumentQry.qryAPP7003.FieldByName('SUNJUCK_PORT').AsString) <> ''then
      begin
        AddBuffer('LOC 1      1D');
        AddBufferChild('76');
        AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('SUNJUCK_PORT').AsString );

        //LOC-------------------------------------------------------------------
        //[APP700]page4 - Latest Date of Shipment
        AddBuffer('DTM 1      1D');
        AddBufferChildTime('38', DMDocumentQry.qryAPP7002.FieldByName('LST_DATE').AsString ,'101');
      end;

      if Trim(DMDocumentQry.qryAPP7003.FieldByName('DOCHACK_PORT').AsString) <> '' then
      begin
        AddBuffer('LOC 2      1D');
        AddBufferChild('12');
        AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOCHACK_PORT').AsString );
      end;
    end
    else if DMDocumentQry.qryAPP7003.FieldByName('CARRIAGE').AsString = 'DQ' then
    begin
      if Trim(DMDocumentQry.qryAPP7002.FieldByName('LOAD_ON').AsString) <> ''then
      begin
        AddBuffer('LOC 1      1D');
        AddBufferChild('149');
        AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('LOAD_ON').AsString );

        //LOC-------------------------------------------------------------------
        //[APP700]page4 - Latest Date of Shipment
        AddBuffer('DTM 1      1D');
        AddBufferChildTime('38', DMDocumentQry.qryAPP7002.FieldByName('LST_DATE').AsString ,'101');
      end;

      if Trim(DMDocumentQry.qryAPP7002.FieldByName('FOR_TRAN').AsString) <> '' then
      begin
        AddBuffer('LOC 2      1D');
        AddBufferChild('148');
        AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('FOR_TRAN').AsString );
      end;
    end;

    //FTX-----------------------------------------------------------------------
    //[APP700]page4 - Shipment Period
    CountIdx := 1;
    if (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD1').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD2').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD3').AsString) <> '') then
    begin
       AddBuffer('FTX '+IntToStr(CountIdx)+'      21');
       AddBufferChild('2AF');
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD1').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD2').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD3').AsString );
       Inc(CountIdx);
    end;

    if (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD4').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD5').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD6').AsString) <> '') then
    begin
       AddBuffer('FTX '+IntToStr(CountIdx)+'      21');
       AddBufferChild('2AF');
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD4').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD5').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7002.FieldByName('SHIP_PD6').AsString );
    end;

    //FTX-----------------------------------------------------------------------
    //[APP700]page2 - Terms of Price
    AddBuffer('TOD 1      1E');
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('TERM_PR').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('TERM_PR_M').AsString);

    //LOC-----------------------------------------------------------------------
    //[APP700]PAGE2 - Place of Terms of Price
    AddBuffer('LOC 1      22');
    AddBufferChild('1');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.QRYAPP7003.FieldByName('PL_TERM').AsString );
    //[APP700]PAGE4 - Country of Origin
    AddBuffer('LOC 2      22');
    AddBufferChild('27');
    AddBufferChild( DMDocumentQry.QRYAPP7003.FieldByName('ORIGIN').AsString );
    AddBufferChild('162');
    AddBufferChild('5');
    AddBufferChild( DMDocumentQry.QRYAPP7003.FieldByName('ORIGIN_M').AsString );

    //LOC-----------------------------------------------------------------------
    //[APP700]PAGE3 - Description of Goods and/or Services
    StringLoop.Text := DMDocumentQry.qryAPP7002.FieldByName('DESGOOD_1').AsString;
    if Trim(StringLoop.Text) <> '' then
    begin
      CountIdx := 1;
      //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
      AddBuffer('FTX '+ IntToStr(CountIdx)  +'      23');
      AddBufferChild('AAA');
      for nLoop := 1 to StringLoop.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(CountIdx);
          AddBuffer('FTX '+ IntToStr(CountIdx)  +'      23');
          AddBufferChild('AAA');
        end;

        AddBufferChild('+' + StringLoop.Strings[nLoop-1]);
      end;
      // StringLoop가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
      if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringLoop.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringLoop.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;

    //LOC-----------------------------------------------------------------------
    //[APP700]PAGE6 - SHIPMENT BY
    CountIdx := 1;
    if (DMDocumentQry.qryAPP7003.FieldByName('ACD_2AA').AsBoolean = True) and
       (Trim(DMDocumentQry.qryAPP7003.FieldByName('ACD_2AA_1').AsString) <> '') then
    begin
      AddBuffer('ALI '+IntToStr(CountIdx)+'      1F');
      AddBufferChild('2AA');
      inc(CountIdx);

      AddBuffer('NAD 1      1F');
      AddBufferChild('CA');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('ACD_2AA_1').AsString );
    end;

    //[APP700]PAGE6 - ACCEPTANCE COMMISSION DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT
    if (DMDocumentQry.qryAPP7003.FieldByName('ACD_2AB').AsBoolean = True) then
    begin
      AddBuffer('ALI '+IntToStr(CountIdx)+'      1F');
      AddBufferChild('2AB');
      inc(CountIdx);
    end;

    //[APP700]PAGE6 - ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
    if (DMDocumentQry.qryAPP7003.FieldByName('ACD_2AC').AsBoolean = True) then
    begin
      AddBuffer('ALI '+IntToStr(CountIdx)+'      1F');
      AddBufferChild('2AC');
      inc(CountIdx);
    end;

    //[APP700]PAGE6 - LATE PRESENTATION B/L ACCEPTABLE
    if (DMDocumentQry.qryAPP7003.FieldByName('ACD_2AD').AsBoolean = True) then
    begin
      AddBuffer('ALI '+IntToStr(CountIdx)+'      1F');
      AddBufferChild('2AD');
      inc(CountIdx);
    end;

    //APP700]PAGE6 - OTHER CONDITION(S)    ( if any )
    if (DMDocumentQry.qryAPP7003.FieldByName('ACD_2AE').AsBoolean = True) and
       (Trim(DMDocumentQry.qryAPP7003.FieldByName('ACD_2AE_1').AsString) <> '') then
    begin
      AddBuffer('ALI '+IntToStr(CountIdx)+'      1F');
      AddBufferChild('2AE');

      StringLoop.Clear;
      StringLoop.Text := DMDocumentQry.qryAPP7003.FieldByName('ACD_2AE_1').AsString;
      CountIdx := 1;
      //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
      AddBuffer('FTX '+ IntToStr(CountIdx)  +'      24');
      AddBufferChild('ABS');
      for nLoop := 1 to StringLoop.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(CountIdx);
          AddBuffer('FTX '+ IntToStr(CountIdx)  +'      24');
          AddBufferChild('ABS');
        end;

        AddBufferChild(StringLoop.Strings[nLoop-1]);
      end;

      if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringLoop.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringLoop.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;

    //DOC1-----------------------------------------------------------------------
    //[APP700]PAGE - Documents Required ... - 380
    CountIdx := 1;
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_380').AsBoolean = True) and
       (Trim(DMDocumentQry.qryAPP7003.FieldByName('DOC_380_1').AsString) <> '') then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('380');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_380_1').AsString );
      inc(CountIdx);
    end;

    //[APP700]PAGE - Documents Required ... - 705
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_705').AsBoolean = True) then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('705');
      AddBufferChild('');
      inc(CountIdx);

      //ALI---------------------------------------------------------------------
      if Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_705_3').AsString)) <> '' then
      begin
        AddBuffer('ALI 1      26');
        AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_705_3').AsString );

        //NAD-------------------------------------------------------------------
        nLoopSub := 1;
        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_705_1').AsString)) <> '') or
           (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_705_2').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('CN');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_705_1').AsString );
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_705_2').AsString );
           Inc(nLoopSub);
        end;

        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_705_4').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('NI');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_705_4').AsString );
           AddBufferChild('');
        end;
      end;
    end;

    //[APP700]PAGE - Documents Required ... - 740
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_740').AsBoolean = True) then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('740');
      AddBufferChild('');
      inc(CountIdx);

      //ALI---------------------------------------------------------------------
      if Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_740_3').AsString)) <> '' then
      begin
        AddBuffer('ALI 1      26');
        AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_740_3').AsString );

        //NAD-------------------------------------------------------------------
        nLoopSub := 1;
        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_740_1').AsString)) <> '') or
           (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_740_2').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('CN');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_740_1').AsString );
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_740_2').AsString );
           Inc(nLoopSub);
        end;

        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_740_4').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('NI');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_740_4').AsString );
           AddBufferChild('');
        end;
      end;
    end;


    //[APP700]PAGE - Documents Required ... - 760
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_760').AsBoolean = True) then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('760');
      AddBufferChild('');
      inc(CountIdx);

      //ALI---------------------------------------------------------------------
      if Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_760_3').AsString)) <> '' then
      begin
        AddBuffer('ALI 1      26');
        AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_760_3').AsString );

        //NAD-------------------------------------------------------------------
        nLoopSub := 1;
        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_760_1').AsString)) <> '') or
           (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_760_2').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('CN');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_760_1').AsString );
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_760_2').AsString );
           Inc(nLoopSub);
        end;

        if (Trim((DMDocumentQry.qryAPP7003.FieldByName('DOC_760_4').AsString)) <> '') then
        begin
           AddBuffer('NAD '+ IntToStr(nLoopSub) +'      30');
           AddBufferChild('NI');
           AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_760_4').AsString );
           AddBufferChild('');
        end;
      end;
    end;

    //[APP700]PAGE - Documents Required ... - 530
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_530').AsBoolean = True) then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('530');
      AddBufferChild('');
      inc(CountIdx);

      //FTX---------------------------------------------------------------------
      AddBuffer('FTX 1      25');
      AddBufferChild('INS');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_530_1').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_530_2').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
    end;

    //[APP700]PAGE - Documents Required ... - 271
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_271').AsBoolean = True) and
       (Trim(DMDocumentQry.qryAPP7003.FieldByName('DOC_271_1').AsString) <> '') then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('271');
      AddBufferChild( DMDocumentQry.qryAPP7003.FieldByName('DOC_271_1').AsString );
      inc(CountIdx);
    end;

    //[APP700]PAGE - Documents Required ... - 861
    if (DMDocumentQry.qryAPP7003.FieldByName('DOC_861').AsBoolean = True) then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('861');
      AddBufferChild('');
      Inc(CountIdx);
    end;

    //[APP700]PAGE - Documents Required ... - DOC_2AA
    if DMDocumentQry.qryAPP7003.FieldByName('DOC_2AA').AsBoolean = True then
    begin
      AddBuffer('DOC '+IntToStr(CountIdx)+'      1G');
      AddBufferChild('2AA');
      AddBufferChild('');
      Inc(CountIdx);

      StringLoop.Clear;
      StringLoop.Text := DMDocumentQry.qryAPP7003.FieldByName('DOC_2AA_1').AsString;
      nLoopSub := 1;
      //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
      AddBuffer('FTX '+ IntToStr(nLoopSub)  +'      25');
      AddBufferChild('ABX');
      for nLoop := 1 to StringLoop.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(nLoopSub);
          AddBuffer('FTX '+ IntToStr(nLoopSub)  +'      25');
          AddBufferChild('ABX');
        end;

        AddBufferChild( StringLoop.Strings[nLoop-1]);
      end;

      if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringLoop.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringLoop.Count)) do
        begin
          AddBufferChild('');
        end;
      end;

    end;

    //[APP700]page1 - 수입용도 , I/L 정보
    CountIdx := 1;
    for nLoop := 1 to 5 do
    begin
     if (Trim(DMDocumentQry.qryAPP7001.FieldByName('IMP_CD'+IntToStr(nLoop)).AsString) <> '') then
     begin
       AddBuffer('RFF '+ IntToStr(CountIdx) +'      1H');
       AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('IMP_CD'+IntToStr(nLoop)).AsString );
       AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('IL_NO'+IntToStr(nLoop)).AsString );
       Inc(CountIdx);

       //MOA--------------------------------------------------------------------
       //
       AddBuffer('MOA 1      1H');
       AddBufferChild('2AC');
       AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('IL_AMT'+IntToStr(nLoop)).AsString );
       AddBufferChild( DMDocumentQry.qryAPP7001.FieldByName('IL_CUR'+IntToStr(nLoop)).AsString );
     end;
    end;

    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;
  finally
    //StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    DMDocumentQry.CLOSE_APP700;
    StringBuffer.Free;
    StringLoop.Free;
  end;

end;

function APP707(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;

  nLoop,CountIdx,nLoopSub : Integer;
  StringBuffer : TStringList;
  StringLoop : TStringList;
begin
  StringBuffer := TStringList.Create;
  StringLoop := TStringList.Create;

  try
    DMDocumentQry.OPEN_APP707(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
//    TXTPATH := 'C:\WINMATE\OUTSAVE\';
    ForceDirectories(TXTPATH);

    FILENAME := 'EDI_APP707' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'APP707 ' , False);
    AddBuffer( Format('%-3s',['2'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['APP707']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    //BGM-----------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    AddBufferChild('469');
    AddBufferChild(MAINT_NO);
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('MESSAGE1').AsString);
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('MESSAGE2').AsString);

    //INP-----------------------------------------------------------------------
    // 개설방법
    AddBuffer('INP 1      11');
    AddBufferChild('1');
    AddBufferChild('5');
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('IN_MATHOD').AsString);
    // 운송수단
    if Trim(DMDocumentQry.qryAPP7072.FieldByName('CARRIAGE').AsString) <>'' then
    begin
      AddBuffer('INP 2      11');
      AddBufferChild('1');
      AddBufferChild('55');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('CARRIAGE').AsString);
    end;

    //RFF-----------------------------------------------------------------------
    AddBuffer('RFF 1      12');
    AddBufferChild('2AD');
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('CD_NO').AsString );

    AddBuffer('RFF 2      12');
    AddBufferChild('2AB');
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AMD_NO').AsString );

    //DTM-----------------------------------------------------------------------
    //Date Of Issue
    AddBuffer('DTM 1      13');
    AddBufferChildTime('182', DMDocumentQry.qryAPP7071.FieldByName('ISS_DATE').AsString ,'101');
    //신청일자
    AddBuffer('DTM 2      13');
    AddBufferChildTime('2AA', DMDocumentQry.qryAPP7071.FieldByName('APP_DATE').AsString ,'101');
    //유효기일(New Date of Expiry)
    AddBuffer('DTM 3      13');
    AddBufferChildTime('123', DMDocumentQry.qryAPP7071.FieldByName('EX_DATE').AsString ,'101');
    //최종선적일자(Latest Date of Shipment)
    AddBuffer('DTM 4      13');
    AddBufferChildTime('38', DMDocumentQry.qryAPP7072.FieldByName('LST_DATE').AsString ,'101');


    //LOC-----------------------------------------------------------------------
    //[선적항,도착항
    CountIdx := 1;
    if Trim(DMDocumentQry.qryAPP7072.FieldByName('CARRIAGE').AsString) = 'DT' then
    begin
      if Trim(DMDocumentQry.qryAPP7072.FieldByName('SUNJUCK_PORT').AsString) <> ''then
      begin
        AddBuffer('LOC '+ IntToStr(CountIdx) +'      14');
        AddBufferChild('76');
        AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SUNJUCK_PORT').AsString );
        Inc(CountIdx);
      end;

      if Trim(DMDocumentQry.qryAPP7072.FieldByName('DOCHACK_PORT').AsString) <> '' then
      begin
        AddBuffer('LOC '+ IntToStr(CountIdx) +'      14');
        AddBufferChild('12');
        AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('DOCHACK_PORT').AsString );
      end;
    end;
    //수탁발송지,최종목적지
    if Trim(DMDocumentQry.qryAPP7072.FieldByName('CARRIAGE').AsString) = 'DQ' then
    begin
      if Trim(DMDocumentQry.qryAPP7072.FieldByName('LOAD_ON').AsString) <> ''then
      begin
        AddBuffer('LOC '+ IntToStr(CountIdx) +'      14');
        AddBufferChild('149');
        AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('LOAD_ON').AsString );
        Inc(CountIdx);
      end;

      if Trim(DMDocumentQry.qryAPP7072.FieldByName('FOR_TRAN').AsString) <> '' then
      begin
        AddBuffer('LOC '+ IntToStr(CountIdx) +'      14');
        AddBufferChild('148');
        AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('FOR_TRAN').AsString );
      end;
    end;
//------------------------------------------------------------------------------
//countIdx (FTX n      15) 가 다끝날때까지 사용
    //FTX-----------------------------------------------------------------------
    //기타정보(AD_INFO1~AD_INFO5)
    CountIdx := 1;
    IF (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_INFO1').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_INFO2').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_INFO3').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_INFO4').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_INFO5').AsString)  <> '')  Then
    begin
      AddBuffer('FTX '+ IntToStr(CountIdx) +'      15');
      AddBufferChild('ACB');
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_INFO1').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_INFO2').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_INFO3').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_INFO4').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_INFO5').AsString );
      Inc(CountIdx);
    end;

    //FTX-----------------------------------------------------------------------
    IF (Trim(DMDocumentQry.qryAPP7072.FieldByName('AA_CV1').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('AA_CV2').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('AA_CV3').AsString)  <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('AA_CV4').AsString)  <> '') then
    begin
      AddBuffer('FTX '+ IntToStr(CountIdx) +'      15');
      AddBufferChild('ABT');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('AA_CV1').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('AA_CV2').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('AA_CV3').AsString );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('AA_CV4').AsString );
      AddBufferChild('');
      Inc(CountIdx);
    end;

    //FTX-----------------------------------------------------------------------
    // Shipment Period
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD1').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD2').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD3').AsString) <> '') then
    begin
       AddBuffer('FTX '+IntToStr(CountIdx)+'      21');
       AddBufferChild('2AF');
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD1').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD2').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD3').AsString );
       AddBufferChild('');
       AddBufferChild('');
       Inc(CountIdx);
    end;
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD4').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD5').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD6').AsString) <> '') then
    begin
       AddBuffer('FTX '+IntToStr(CountIdx)+'      21');
       AddBufferChild('2AF');
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD4').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD5').AsString );
       AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('SHIP_PD6').AsString );
       AddBufferChild('');
       AddBufferChild('');
       Inc(CountIdx);
    end;

    //FTX-----------------------------------------------------------------------
    //Narrative
    StringLoop.Text := DMDocumentQry.qryAPP7072.FieldByName('NARRAT_1').AsString;
    if Trim(StringLoop.Text) <> '' then
    begin
      //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
      AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
      AddBufferChild('2AD');
      for nLoop := 1 to StringLoop.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(CountIdx);
          AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
          AddBufferChild('2AD');
        end;

        AddBufferChild(StringLoop.Strings[nLoop-1]);
      end;
      // StringLoop가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
      if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringLoop.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringLoop.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;
//------------------------------------------------------------------------------

   //FII-----------------------------------------------------------------------
    //개설의뢰은행(AP_BANK)
    AddBuffer('FII 1      16');
    AddBufferChild('AW');
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AP_BANK').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AP_BANK1').AsString + DMDocumentQry.qryAPP7071.FieldByName('AP_BANK2').AsString);
    AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AP_BANK3').AsString + DMDocumentQry.qryAPP7071.FieldByName('AP_BANK4').AsString);
    //COMM - 개설의뢰은행 TEL (조건중 3055 = DG인경우 사용 이부분 확인해야함 기존상역프로그램은 DG가 아니여도 추가됨 )
    IF Trim(DMDocumentQry.qryAPP7071.FieldByName('AP_BANK5').AsString) <> '' then
    BEGIN
      AddBuffer('COM 1      16');
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AP_BANK5').AsString );
      AddBufferChild('TE');
    end;
    //통지은행(AD_BANK)
    IF (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_BANK').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_BANK1').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_BANK2').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_BANK3').AsString) <> '') OR
       (Trim(DMDocumentQry.qryAPP7071.FieldByName('AD_BANK4').AsString) <> '') then
    begin
      AddBuffer('FII 2      16');
      AddBufferChild('2AA');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_BANK1').AsString + DMDocumentQry.qryAPP7071.FieldByName('AD_BANK2').AsString);
      AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('AD_BANK3').AsString + DMDocumentQry.qryAPP7071.FieldByName('AD_BANK4').AsString);
    end;

    //NAD-----------------------------------------------------------------------
    //APPLICANT(APPLIC1~APPLIC4)
    AddBuffer('NAD 1      17');
    AddBufferChild('DF');
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('APPLIC1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('APPLIC2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('APPLIC3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('APPLIC4').AsString );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
      //APPLICANT - TEL (APPLIC5)
      if Trim(DMDocumentQry.qryAPP7072.FieldByName('APPLIC5').AsString) <> '' then
      begin
        AddBuffer('COM 1      17');
        AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('APPLIC5').AsString );
      end;
    //Beneficiary(BENEFC1~BENEFC5)
    AddBuffer('NAD 2      17');
    AddBufferChild('DG');
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BENEFC1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BENEFC2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BENEFC3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BENEFC4').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BENEFC5').AsString );
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    // 명의인  (EX_NAME1,EX_NAME2,EX_NAME3,EX_ADDR1,EX_ADDR2)
    AddBuffer('NAD 3      17');
    AddBufferChild('2AE');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild('');
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('EX_NAME1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('EX_NAME2').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('EX_NAME3').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('EX_ADDR1').AsString );
    AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('EX_ADDR2').AsString );


    //MOA-----------------------------------------------------------------------
    CountIdx := 1;
    //Increase of  Documentary Credit Amount
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('INCD_CUR').AsString) <> '') and
       (DMDocumentQry.qryAPP7072.FieldByName('INCD_AMT').AsCurrency <> 0) then
    begin
      AddBuffer('MOA '+ IntToStr(CountIdx) +'      18');
      AddBufferChild('2AA');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('INCD_AMT').AsCurrency );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('INCD_CUR').AsString );
      Inc(CountIdx);
    end;
    //Decrease of  Documentary Credit Amount
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('DECD_CUR').AsString) <> '') and
       (DMDocumentQry.qryAPP7072.FieldByName('DECD_AMT').AsCurrency <> 0) then
    begin
      AddBuffer('MOA '+ IntToStr(CountIdx) +'      18');
      AddBufferChild('2AB');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('DECD_AMT').AsCurrency );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('DECD_CUR').AsString );
      Inc(CountIdx);
    end;
    //New Documentary CreditAmount after Amendment
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('NWCD_CUR').AsString) <> '') and
       (DMDocumentQry.qryAPP7072.FieldByName('NWCD_AMT').AsCurrency <> 0) then
    begin
      AddBuffer('MOA '+ IntToStr(CountIdx) +'      18');
      AddBufferChild('212');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('NWCD_AMT').AsCurrency );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('NWCD_CUR').AsString );
      Inc(CountIdx);
    end;
    //New Documentary CreditAmount before Amendment
    if (Trim(DMDocumentQry.qryAPP7072.FieldByName('BFCD_CUR').AsString) <> '') and
       (DMDocumentQry.qryAPP7072.FieldByName('BFCD_AMT').AsCurrency <> 0) then
    begin
      AddBuffer('MOA '+ IntToStr(CountIdx) +'      18');
      AddBufferChild('ZZZ');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BFCD_AMT').AsCurrency );
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('BFCD_CUR').AsString );
    end;


    //ALC-----------------------------------------------------------------------
    //[APP700]page2 - Maximum Credit Amount(CD_MAX)
    if Trim(DMDocumentQry.qryAPP7072.FieldByName('CD_MAX').AsString) <>'' then
    begin
      AddBuffer('ALC 1      20');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('CD_MAX').AsString);
    end;
    if (DMDocumentQry.qryAPP7072.FieldByName('CD_PERP').AsCurrency > 0) OR (DMDocumentQry.qryAPP7072.FieldByName('CD_PERM').AsCurrency > 0) then
    begin
      AddBuffer('PCD 1      20');
      AddBufferChild('13');
      AddBufferChild( DMDocumentQry.qryAPP7072.FieldByName('CD_PERP').AsString + '.' + DMDocumentQry.qryAPP7072.FieldByName('CD_PERM').AsString);
    end;

    //RFF-----------------------------------------------------------------------
    //수입용도 , I/L 정보
    CountIdx := 1;
    for nLoop := 1 to 5 do
    begin
     if (Trim(DMDocumentQry.qryAPP7071.FieldByName('IMP_CD'+IntToStr(nLoop)).AsString) <> '') then
     begin
       AddBuffer('RFF '+ IntToStr(CountIdx) +'      19');
       AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('IMP_CD'+IntToStr(nLoop)).AsString );
       AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('IL_NO'+IntToStr(nLoop)).AsString );
       Inc(CountIdx);

       //MOA--------------------------------------------------------------------
       //
       AddBuffer('MOA 1      19');
       AddBufferChild('2AC');
       AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('IL_AMT'+IntToStr(nLoop)).AsString );
       AddBufferChild( DMDocumentQry.qryAPP7071.FieldByName('IL_CUR'+IntToStr(nLoop)).AsString );
     end;
    end;



    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;
  finally
    //StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    DMDocumentQry.CLOSE_APP707;
    StringBuffer.Free;
    StringLoop.Free;
  end;

end;

function APPLOG(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;
  nLoop,CountIdx,nLoopSub : Integer;
  StringBuffer : TStringList;
  StringLoop : TStringList;
begin
   StringBuffer := TStringList.Create;
  StringLoop := TStringList.Create;

  try
    DMDocumentQry.OPEN_APPLOG(MAINT_NO);
    TXTPATH := 'C:\WINMATE\FLATOUT\';
//    TXTPATH := 'C:\WINMATE\OUTSAVE\';
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    ForceDirectories(TXTPATH);

    FILENAME := 'EDI_APPLOG' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'APPLOG ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['APPLOG']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

//BGM---------------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    AddBufferChild( DMDocumentQry.qryAPPLOG.FieldByName('MESSAGE1').asString );
    AddBufferChild(MAINT_NO);
    AddBufferChild( DMDocumentQry.qryAPPLOG.FieldByName('MESSAGE2').AsString);
    AddBufferChild( DMDocumentQry.qryAPPLOG.FieldByName('MESSAGE3').AsString);

//NAD---------------------------------------------------------------------------
    //선박회사 / 운송사명-------------------------------------------------------
    CountIdx := 1;
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME1').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME2').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME3').AsString) <> '') then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
      AddBufferChild('CA');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME1').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME2').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CR_NAME3').AsString);
      Inc(CountIdx);
    end;
    //송하인--------------------------------------------------------------------
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME1').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME2').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME3').AsString) <> '') then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
      AddBufferChild('SE');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME1').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME2').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SE_NAME3').AsString);
      Inc(CountIdx);
    end;
    //신청인--------------------------------------------------------------------
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME1').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME2').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME3').AsString) <> '') then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
      AddBufferChild('MS');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME1').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME2').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('MS_NAME3').AsString);
      Inc(CountIdx);
    end;
    //인수예정자----------------------------------------------------------------
     if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME1').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME2').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME3').AsString) <> '') then
     begin
       AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
       AddBufferChild('B5');
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME1').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME2').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('B5_NAME3').AsString);
       Inc(CountIdx);
     end;
     //수하인-------------------------------------------------------------------
     if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME1').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME2').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME3').AsString) <> '') then
     begin
       AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
       AddBufferChild('CN');
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME1').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME2').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CN_NAME3').AsString);
       Inc(CountIdx);
     end;
     //신청인 전자서명(명의인)--------------------------------------------------
     if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME1').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME2').AsString) <> '') or
        (Trim(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME3').AsString) <> '') then
     begin
       AddBuffer('NAD '+ IntToStr(CountIdx) +'      11');
       AddBufferChild('AX');
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME1').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME2').AsString);
       AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('AX_NAME3').AsString);
     end;
//MOA---------------------------------------------------------------------------
    //상업송장금액--------------------------------------------------------------
    AddBuffer('MOA 1      12');
    AddBufferChild('77');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('INV_AMT').AsCurrency);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('INV_AMTC').AsString);
//RFF---------------------------------------------------------------------------
    //신용장(계약서)번호--------------------------------------------------------
    AddBuffer('RFF 1      13');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('LC_G').AsString);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('LC_NO').AsString);
    //선하증권(운송장) 번호-----------------------------------------------------
    AddBuffer('RFF 2      13');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('BL_G').AsString);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('BL_NO').AsString);
//TDT---------------------------------------------------------------------------
    //운행정보(항해번호/비행편번호 , 선박명/기명 )------------------------------
    AddBuffer('TDT 1      14');
    AddBufferChild('20');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CARRIER2').AsString);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('CARRIER1').AsString);
//DTM---------------------------------------------------------------------------
    //도착(예정)일--------------------------------------------------------------
    AddBuffer('DTM 1      15');
    AddBufferChildTime('132', DMDocumentQry.qryAPPLOG.FieldByName('AR_DATE').AsString ,'101');
    //선하증권 발급일자---------------------------------------------------------
    AddBuffer('DTM 2      15');
    AddBufferChildTime('95', DMDocumentQry.qryAPPLOG.FieldByName('BL_DATE').AsString ,'101');
    //선하증권 발급일자---------------------------------------------------------
    AddBuffer('DTM 3      15');
    AddBufferChildTime('2AA', DMDocumentQry.qryAPPLOG.FieldByName('APP_DATE').AsString ,'101');
//LOC---------------------------------------------------------------------------
    //선적항--------------------------------------------------------------------
    AddBuffer('LOC 1      16');
    AddBufferChild('9');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('LOAD_LOC').AsString);
    AddBufferChild('162');
    AddBufferChild('5');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('LOAD_TXT').AsString);
    //도착항--------------------------------------------------------------------
    AddBuffer('LOC 2      16');
    AddBufferChild('8');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('ARR_LOC').AsString);
    AddBufferChild('162');
    AddBufferChild('5');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('ARR_TXT').AsString);
//PCI---------------------------------------------------------------------------
    //화물표시 및 번호----------------------------------------------------------
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK1').AsString) <> '') or  (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK2').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK3').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK4').AsString) <> '') or  (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK5').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK6').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK7').AsString) <> '') or  (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK8').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK9').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK7').AsString) <> '') then
    begin
      AddBuffer('PCI 1      17');
      AddBufferChild('28');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK1').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK2').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK3').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK4').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK5').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK6').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK7').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK8').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK9').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('SPMARK10').AsString);
    end;
//PAC---------------------------------------------------------------------------
    //포장수--------------------------------------------------------------------
    AddBuffer('PAC 1      18');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('PAC_QTY').AsCurrency);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('PAC_QTYC').AsString);
//FTX---------------------------------------------------------------------------
    //상품명세------------------------------------------------------------------
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS1').AsString) <> '') or  (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS2').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS3').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS4').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS5').AsString) <> '') then
    begin
      AddBuffer('FTX 1      19');
      AddBufferChild('AAA');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS1').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS2').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS3').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS4').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS5').AsString);
    end;
    if (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS6').AsString) <> '') or  (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS7').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS8').AsString) <> '') or
       (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS9').AsString) <> '') or (Trim(DMDocumentQry.qryAPPLOG.FieldByName('GOODS10').AsString) <> '') then
    begin
      AddBuffer('FTX 2      19');
      AddBufferChild('AAA');
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS6').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS7').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS8').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS9').AsString);
      AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('GOODS10').AsString);
    end;
//PAT---------------------------------------------------------------------------
    //대금결제조건(결제기간)----------------------------------------------------
    AddBuffer('PAT 1      1A');
    AddBufferChild('1AA');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('TRM_PAYC').AsString);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('TRM_PAY').AsString);
//FII---------------------------------------------------------------------------
    //발급은행------------------------------------------------------------------
    AddBuffer('FII 1      1B');
    AddBufferChild('BI');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('BANK_CD').AsString);
    AddBufferChild('25');
    AddBufferChild('BOK');
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('BANK_TXT').AsString);
    AddBufferChild(DMDocumentQry.qryAPPLOG.FieldByName('BANK_BR').AsString);
//------------------------------------------------------------------------------
    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;

  finally
    //StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    DMDocumentQry.CLOSE_APPLOG;
    StringBuffer.Free;
    StringLoop.Free;
  end;
end;

function DOMOFC(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;
  nLoop,CountIdx,nLoopSub,DetailCount : Integer;
  StringBuffer : TStringList;
  StringLoop : TStringList;
begin
  StringBuffer := TStringList.Create;
  StringLoop := TStringList.Create;

  try
    DMDocumentQry.OPEN_DOMOFC(MAINT_NO);
    TXTPATH := 'C:\WINMATE\FLATOUT\';
//    TXTPATH := 'C:\WINMATE\OUTSAVE\';
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    ForceDirectories(TXTPATH);

    FILENAME := 'EDI_DOMOFC' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'DOMOFR ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['DOMOFR']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

//BGM---------------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    //1BK :내국신용장 물품매도확약서(제안): 수혜업체가 개설업체에게 오퍼 신청하는 경우
    //1BL :내국신용장 물품매도확약서(확인): 수혜업체가 전송한 오퍼에 대해 개설업체가 확인하여 전송하는 경우
    AddBufferChild('1BL');
    AddBufferChild(MAINT_NO);
    AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('MESSAGE1').AsString);
    AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('MESSAGE2').AsString);

//LOC---------------------------------------------------------------------------
    //원산지 국가 , 지방명
    CountIdx := 1;
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN1N').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN1LOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      12' );
      AddBufferChild('27');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN1N').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN1LOC').AsString );
      Inc(CountIdx);
    end;
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN2N').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN2LOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      12' );
      AddBufferChild('27');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN2N').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN2LOC').AsString );
      Inc(CountIdx);
    end;
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN3N').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN3LOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      12' );
      AddBufferChild('27');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN3N').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN3LOC').AsString );
      Inc(CountIdx);
    end;
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN4N').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN4LOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      12' );
      AddBufferChild('27');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN4N').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN4LOC').AsString );
      Inc(CountIdx);
    end;
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN5N').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN5LOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      12' );
      AddBufferChild('27');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN5N').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('ORGN5LOC').AsString );
    end;
//FTX---------------------------------------------------------------------------
    CountIdx := 1;
    //유효기간
    if (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ1').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ2').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ3').AsString) <> '') or
       (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ4').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      13' );
      AddBufferChild('1AB');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_SQ5').AsString );
      Inc(CountIdx);
    end;
    //결제조건
    if (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC1').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC2').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC3').AsString) <> '') or
       (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC4').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      13' );
      AddBufferChild('AAB');
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PAY_ETC5').AsString );
      Inc(CountIdx);
    end;
    //검사방법
    if (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST1').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST2').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST3').AsString) <> '') or
       (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST4').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      13' );
      AddBufferChild('ITS');
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('TSTINST5').AsString );
      Inc(CountIdx);
    end;
    //포장방법
    if (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST1').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST2').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST3').AsString) <> '') or
       (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST4').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      13' );
      AddBufferChild('PKG');
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH2.FieldByName('PCKINST5').AsString );
    end;
//RFF---------------------------------------------------------------------------
    //물품매도확약서 발행번호
    if Trim( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_NO').AsString ) <> '' then
    begin
      AddBuffer('RFF 1      14');
      AddBufferChild('AAG');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_NO').AsString );
    end;
//DTM---------------------------------------------------------------------------
    //물품매도확약서의 발행일자
    if Trim( DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_DATE').AsString ) <> '' then
    begin
      AddBuffer('DTM 1      20');
      AddBufferChildTime('182' , DMDocumentQry.qryDOMOFCH1.FieldByName('OFR_DATE').AsString , '101');
    end;
//RFF---------------------------------------------------------------------------
    CountIdx := 2;
    //수요자참조번호
    if Trim( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_RENO').AsString ) <> '' then
    begin
     AddBuffer('RFF '+IntToStr(CountIdx)+'      14');
     AddBufferChild('CR');
     AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_RENO').AsString );
     Inc(CountIdx);
    end;
    //대표공급물품 HS부호
    if Trim( DMDocumentQry.qryDOMOFCH1.FieldByName('HS_CODE').AsString ) <> '' then
    begin
     AddBuffer('RFF '+IntToStr(CountIdx)+'      14');
     AddBufferChild('HS');
     AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('HS_CODE').AsString );
    end;
//NAD---------------------------------------------------------------------------
    CountIdx := 1;
    //발행자 (무역대리점번호와 상호1이 입력되어있을경우)
    if (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NO').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NAME1').AsString) <> '') then
    begin
      AddBuffer('NAD '+IntToStr(CountIdx)+'      15');
      AddBufferChild('SE');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NO').AsString );
      AddBufferChild('1AD');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NAME1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NAME2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_NAME3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_ADDR1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_ADDR2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('SR_ADDR3').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(CountIdx);
    end;
    //수요자 (수요자사업자번호와 상호1이 입력되어있는경우)
    if (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NO').AsString ) <> '') and (Trim(DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NAME1').AsString ) <> '') then
    begin
      AddBuffer('NAD '+IntToStr(CountIdx)+'      15');
      AddBufferChild('BY');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NO').AsString );
      AddBufferChild('1AB');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NAME1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NAME2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_NAME3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_ADDR1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_ADDR2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH1.FieldByName('UD_ADDR3').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(CountIdx);
    end;
//TDT---------------------------------------------------------------------------
    //운송방법(운송명세)
    if Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('TRNS_ID').AsString) <> '' then
    begin
      AddBuffer('TDT 1      16');
      AddBufferChild('20');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('TRNS_ID').AsString );
    end;
//LOC---------------------------------------------------------------------------
    CountIdx := 1;
    //선적국가, 선적항  ,
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADD').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADLOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      21');
      AddBufferChild('9');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADD').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADLOC').AsString );
      Inc(CountIdx);
    end;
    //도착국가 , 도착항
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DEST').AsString) <> '') or (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTLOC').AsString) <> '') then
    begin
      AddBuffer('LOC '+IntToStr(CountIdx)+'      21');
      AddBufferChild('8');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DEST').AsString );
      AddBufferChild('162');
      AddBufferChild('5');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTLOC').AsString );
    end;
//FTX---------------------------------------------------------------------------
    CountIdx := 1;
    //선적시기
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT1').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT2').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT3').AsString) <> '') and
       (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT4').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      22');
      AddBufferChild('1AC');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('LOADTXT5').AsString );
      Inc(CountIdx);
    end;
    //도착시기
    if (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT1').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT2').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT3').AsString) <> '') and
       (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT4').AsString) <> '') and (Trim(DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT5').AsString) <> '') then
    begin
      AddBuffer('FTX '+IntToStr(CountIdx)+'      22');
      AddBufferChild('1AD');
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT1').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT2').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT3').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT4').AsString );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('DESTTXT5').AsString );
    end;
//UNS---------------------------------------------------------------------------
    //부분제어(부분식별자로 전자문서내 머리부문과 상세부문을 분리)
    If DMDocumentQry.qryDOMOFCD.RecordCount > 0 then
    begin
      AddBuffer('UNS 1');
      AddBufferChild('D');

      //상세내역(품명) 갯수만큼 반복
      DetailCount := 1;
      DMDocumentQry.qryDOMOFCD.First;
      while not DMDocumentQry.qryDOMOFCD.Eof do
      begin
        //LIN-------------------------------------------------------------------
          //상품 및 규격기재시 순번을 기재하는 항목
          AddBuffer('LIN '+ IntToStr(DetailCount) +'      19');
          AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('SEQ').AsString );
          Inc(DetailCount);
        //PIA-------------------------------------------------------------------
          //HS부호를 기재
          if Trim(DMDocumentQry.qryDOMOFCD.FieldByName('HS_NO').AsString) <> '' then
          begin
            AddBuffer('PIA 1      25');
            AddBufferChild('1');
            AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('HS_NO').AsString );
          end;
        //IMD-------------------------------------------------------------------
          //해당물품의 품명을 기재
          StringLoop.Clear;
          StringLoop.Text := ( DMDocumentQry.qryDOMOFCD.FieldByName('NAME1').AsString );
          if Trim(StringLoop.Text) <> '' then
          begin
            for nLoop := 1 to StringLoop.Count do
            begin
              AddBuffer('IMD '+ IntToStr(nLoop) +'      26');
              AddBufferChild('1AA');
              AddBufferChild(StringLoop.Strings[nLoop-1]);
            end;
          end;
        //QTY-------------------------------------------------------------------
          //수량
          CountIdx := 1;
          if DMDocumentQry.qryDOMOFCD.FieldByName('QTY').AsCurrency <> 0 then
          begin
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('1');
            AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('QTY').AsCurrency));
            AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('QTY_G').AsString );
            Inc(CountIdx);
          end;
          //수량소계
          if DMDocumentQry.qryDOMOFCD.FieldByName('STQTY').AsCurrency <> 0 then
          begin
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('3');
            AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('STQTY').AsCurrency) );
            AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('STQTY_G').AsString );
          end;
        //MOA-------------------------------------------------------------------
          //금액
          CountIdx := 1;
          if DMDocumentQry.qryDOMOFCD.FieldByName('AMT').AsCurrency <> 0 then
          begin
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      28');
            AddBufferChild('203');
            AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('AMT').AsCurrency) );
            AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('AMT_G').AsString );
            Inc(CountIdx);
          end;
          //금액소계
          if DMDocumentQry.qryDOMOFCD.FieldByName('STAMT').AsCurrency <> 0 then
          begin
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      28');
            AddBufferChild('17');
            AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('STAMT').AsCurrency) );
            AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('STAMT_G').AsString );
          end;
        //FTX-------------------------------------------------------------------
          //물품의 규격
          CountIdx := 1;
          StringLoop.Clear;
          StringLoop.Text := DMDocumentQry.qryDOMOFCD.FieldByName('SIZE1').AsString;
          if Trim(StringLoop.Text) <> '' then
          begin
            //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      29');
            AddBufferChild('AAA');
            for nLoop := 1 to StringLoop.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                Inc(CountIdx);
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      29');
                AddBufferChild('AAA');
              end;

              AddBufferChild(StringLoop.Strings[nLoop-1]);
            end;
            // StringLoop가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringLoop.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringLoop.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
        //PRI-------------------------------------------------------------------
          //단가를 기재
          AddBuffer('PRI 1      2A');
          AddBufferChild('CAL');
          AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('PRICE').AsCurrency) );
          AddBufferChild('PE');
          AddBufferChild('CUP');
          AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCD.FieldByName('QTYG').AsCurrency) );
          AddBufferChild( DMDocumentQry.qryDOMOFCD.FieldByName('QTYG_G').AsString );
        DMDocumentQry.qryDOMOFCD.Next;
      end;//While End
    end;// If End

//UNS---------------------------------------------------------------------------
    //전자문서의 상세부분과 꼬리부분을 분리하는 부문제어전송항목
    AddBuffer('UNS 1');
    AddBufferChild('S');
//RFF---------------------------------------------------------------------------
    //참조번호기재
    //Maint_Rff에 값이들어있어도 unh파일에 생성이되지않음.

//FTX---------------------------------------------------------------------------
    //참조사항,기타사항
    CountIdx := 1;
    StringLoop.Clear;
    StringLoop.Text := DMDocumentQry.qryDOMOFCH1.FieldByName('REMARK_1').AsString;
    if Trim(StringLoop.Text) <> '' then
    begin
      //처음  AddBuffer,AddBufferChild 출력후 stringLoop.Text 출력시작
      AddBuffer('FTX '+ IntToStr(CountIdx)  +'      1B');
      AddBufferChild('AAI');
      for nLoop := 1 to StringLoop.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(CountIdx);
          AddBuffer('FTX '+ IntToStr(CountIdx)  +'      1B');
          AddBufferChild('AAI');
        end;

        AddBufferChild(StringLoop.Strings[nLoop-1]);
      end;
      // StringLoop가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
      if (StringLoop.Count> 5) and ((5-(StringLoop.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringLoop.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringLoop.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringLoop.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;
//MOA---------------------------------------------------------------------------
    //총금액
    if DMDocumentQry.qryDOMOFCH3.FieldByName('TAMT').AsCurrency > 0 then
    begin
      AddBuffer('MOA 1      1C');
      AddBufferChild('128');
      AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCH3.FieldByName('TAMT').AsCurrency) );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('TAMTCUR').AsString );
    end;
//CNT---------------------------------------------------------------------------
    //총합계
    if DMDocumentQry.qryDOMOFCH3.FieldByName('TQTY').AsCurrency > 0 then
    begin
      AddBuffer('CNT 1      1D');
      AddBufferChild('1AA');
      AddBufferChild( CurrToStr(DMDocumentQry.qryDOMOFCH3.FieldByName('TQTY').AsCurrency) );
      AddBufferChild( DMDocumentQry.qryDOMOFCH3.FieldByName('TQTYCUR').AsString );
    end;                                                                          
//------------------------------------------------------------------------------
    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;

  finally
    //StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    DMDocumentQry.CLOSE_DOMOFC;
    StringBuffer.Free;
    StringLoop.Free;
  end;

end;

function LOCAMR(MAINT_NO:string;MSEQ : Integer;RecvCode : String):String;
var
  RECEIVER,
  TXTPATH, FILENAME : string;

  nLoop,CountIdx,nLoopSub : Integer;
  StringBuffer : TStringList;
  StringFTX : TStringList;
  i : Integer;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;

  try
    DMDocumentQry.OPEN_LOCAMR(MAINT_NO,MSEQ);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := 'LOCAMR' + MAINT_NO+'-'+IntToStr(MSEQ) + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'LOCAMR ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['LOCAMR']) , False);
    AddBuffer( MAINT_NO+'-'+IntToStr(MSEQ)+NEWLINE+'UNH');

    //BGM-----------------------------------------------------------------------
    AddBuffer('BGM 1      10');
    AddBufferChild('2AE');
    AddBufferChild(MAINT_NO+'-'+IntToStr(MSEQ));
    AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('MESSAGE1').AsString );
    AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('MESSAGE2').AsString );

//RFF---------------------------------------------------------------------------
    CountIdx := 1;
    //내국신용장번호
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('LC_NO').AsString ) <> '' then
    begin
     AddBuffer('RFF '+IntToStr(CountIdx)+'      11');
     AddBufferChild('LC');
     AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('LC_NO').AsString );
     Inc(CountIdx);
    end;
    //최종물품매도확약서번호
    for i := 1 to 9 do
    begin
      if Trim( DMDocumentQry.qryLOCAMR.FieldByName('OFFERNO'+IntToStr(i)).AsString ) <> '' then
      begin
        AddBuffer('RFF '+IntToStr(CountIdx)+'      11');
        AddBufferChild('AAG');
        AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('OFFERNO'+IntToStr(i)).AsString );
        if i <> 9 then Inc(CountIdx);
      end;
    end;
//DTM---------------------------------------------------------------------------
    CountIdx := 1;
    //조건변경신청일자
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('APP_DATE').AsString ) <> '' then
    begin
      AddBuffer('DTM '+ IntToStr(CountIdx) +'      12');
      AddBufferChildTime('2AA' , DMDocumentQry.qryLOCAMR.FieldByName('APP_DATE').AsString , '101');
      Inc(CountIdx);
    end;
    //개설일자
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('ISS_DATE').AsString ) <> '' then
    begin
      AddBuffer('DTM '+ IntToStr(CountIdx) +'      12');
      AddBufferChildTime('182' , DMDocumentQry.qryLOCAMR.FieldByName('ISS_DATE').AsString , '101');
    end;
//FTX---------------------------------------------------------------------------
    //기타정보
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('REMARK1').AsString ) <> '' then
    begin
      StringFTX.Clear;
      StringFTX.Text := DMDocumentQry.qryLOCAMR.FieldByName('REMARK1').AsString;
      AddBuffer('FTX 1      13');
      AddBufferChild('ACB');
      for nLoop := 1 to StringFTX.Count do
      begin
        AddBufferChild(StringFTX.Strings[nLoop-1]);
      end;
      if (StringFTX.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringFTX.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;
//FII---------------------------------------------------------------------------
    CountIdx := 1;
    //개설은행
    if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK').AsString ) <> '') or
       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK1').AsString ) <> '') or
       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK2').AsString ) <> '') then
    begin
      AddBuffer('FII 1      14');
      AddBufferChild('AZ');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK').AsString );
      AddBufferChild('25');
      AddBufferChild('BOK');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('ISSBANK2').AsString );
    end;
//NAD---------------------------------------------------------------------------
    //개설의뢰인
    if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('APPLIC1').AsString ) <> '' ) or
       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('APPLIC2').AsString ) <> '' ) then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      15');
      AddBufferChild('DF');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPLIC1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPLIC2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPLIC3').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPADDR1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPADDR2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('APPADDR3').AsString );
      Inc(CountIdx);
    end;
    //수혜자
    if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('BENEFC1').AsString ) <> '' ) or
       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('BENEFC2').AsString ) <> '' ) then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      15');
      AddBufferChild('DG');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BNFEMAILID').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BNFDOMAIN').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BENEFC1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BENEFC2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BENEFC3').AsString  +
                      DMDocumentQry.qryLOCAMR.FieldByName('CHKNAME1').AsString + '/' +
                      DMDocumentQry.qryLOCAMR.FieldByName('CHKNAME2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BNFADDR1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BNFADDR2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('BNFADDR3').AsString );
      Inc(CountIdx);
    end;
    //전자서명
    if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('EXNAME1').AsString ) <> '' ) or
       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('EXNAME2').AsString ) <> '' ) then
    begin
      AddBuffer('NAD '+ IntToStr(CountIdx) +'      15');
      AddBufferChild('AX');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('EXNAME1').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('EXNAME2').AsString );
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('EXNAME3').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
    end;
//RFF---------------------------------------------------------------------------
    //조건변경 회차
    AddBuffer('RFF 1      16');
    AddBufferChild('2AB');
    AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('AMD_NO').AsString );
//DTM---------------------------------------------------------------------------
    CountIdx := 1;
    //변경후 물품인도기일
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('DELIVERY').AsString ) <> '' then
    begin
      AddBuffer('DTM '+ IntToStr(CountIdx) +'      20');
      AddBufferChildTime('2' , DMDocumentQry.qryLOCAMR.FieldByName('DELIVERY').AsString , '101');
      Inc(CountIdx);
    end;
    //변경후 유효기일
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('EXPIRY').AsString ) <> '' then
    begin
      AddBuffer('DTM '+ IntToStr(CountIdx) +'      20');
      AddBufferChildTime('123' , DMDocumentQry.qryLOCAMR.FieldByName('EXPIRY').AsString , '101');
    end;
//FTX---------------------------------------------------------------------------
    StringFTX.Text := DMDocumentQry.qryLOCAMR.FieldByName('CHGINFO1').AsString;
    if Trim(StringFTX.Text) <> '' then
    begin
      CountIdx := 1;
      //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
      AddBuffer('FTX '+ IntToStr(CountIdx)  +'      21');
      AddBufferChild('CHG');
      for nLoop := 1 to StringFTX.Count do
      begin
        // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
        if ((nLoop mod 5) = 1) and (nLoop > 5) then
        begin
          Inc(CountIdx);
          AddBuffer('FTX '+ IntToStr(CountIdx)  +'      21');
          AddBufferChild('CHG');
        end;

        AddBufferChild(StringFTX.Strings[nLoop-1]);
      end;
      // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
      if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
      begin
        // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
        for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
        begin
          AddBufferChild('');
        end;
      end
      else if (StringFTX.Count < 5) then
      begin
        for nLoop := 1 to (5-(StringFTX.Count)) do
        begin
          AddBufferChild('');
        end;
      end;
    end;
//BUS---------------------------------------------------------------------------
    //내국신용장 종류
    if Trim( DMDocumentQry.qryLOCAMR.FieldByName('LOC_TYPE').AsString ) <> '' then
    begin
      AddBuffer('BUS 1      22');
      AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('LOC_TYPE').AsString );

      //MOA---------------------------------------------------------------------------
          //변경후 외화(원화)금액
          if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('LOC_AMTC').AsString ) <> '') then
      //       (DMDocumentQry.qryLOCAMR.FieldByName('LOC_AMT').AsCurrency <> 0 ) then
          begin
            AddBuffer('MOA 1      30');
            if Trim( DMDocumentQry.qryLOCAMR.FieldByName('LOC_AMTC').AsString ) = 'KRW' then
              AddBufferChild('2AE')
            else
              AddBufferChild('2AD');
            AddBufferChild( CurrToStr( DMDocumentQry.qryLOCAMR.FieldByName('LOC_AMT').AsCurrency ) );
            AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('LOC_AMTC').AsString );
          end;
      //PCD---------------------------------------------------------------------------
          //허용오차
      //    if (Trim( DMDocumentQry.qryLOCAMR.FieldByName('CD_PERP').AsString ) <> '0') OR
      //       (Trim( DMDocumentQry.qryLOCAMR.FieldByName('CD_PERM').AsString ) <> '0') then
          IF not(DMDocumentQry.qryLOCAMR.FieldByName('CD_PERP').IsNull AND DMDocumentQry.qryLOCAMR.FieldByName('CD_PERM').IsNull)
          Then
          begin
            AddBuffer('PCD 1      31');
            AddBufferChild('13');
            AddBufferChild( DMDocumentQry.qryLOCAMR.FieldByName('CD_PERP').AsString +'.'+ DMDocumentQry.qryLOCAMR.FieldByName('CD_PERM').AsString );
          end;

    end;

    AddBuffer('UNT',False);
    StringBuffer.Text := Buffer;
    Result := Buffer;
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
    DMDocumentQry.CLOSE_LOCAMR;
  end;
end;

function VATBI1(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,  TXTPATH, FILENAME: string;
  CountIdx,NADCount,FTXCount,
  LINCount,MOACount,
  uptaLoop,ItemLoop           : Integer;
  StringBuffer                : TStringList;
  StringFTX                   : TStringList;
  StringIMD                   : TStringList;
  i , nLoop  : Integer;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;
  StringIMD := TStringList.Create;
  try
    DMDocumentQry.OPEN_VATBI1(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := 'VATBI1' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'VATBIL ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['VATBIL']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    with DMDocumentQry do
    begin
      //BGM-----------------------------------------------------------------------
      AddBuffer('BGM 1      10');
      AddBufferChild( qryVATBI1H.FieldByName('VAT_CODE').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('VAT_TYPE').AsString );
      AddBufferChild( MAINT_NO );
      AddBufferChild( qryVATBI1H.FieldByName('MESSAGE1').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('MESSAGE2').AsString );
      //GIS-----------------------------------------------------------------------
      if Trim(qryVATBI1H.FieldByName('NEW_INDICATOR').AsString) <> '' then  //수정사유코드
      begin
        AddBuffer('GIS 1      11');
        AddBufferChild('BD');
        AddBufferChild( qryVATBI1H.FieldByName('NEW_INDICATOR').AsString);
      end;
      //RFF-----------------------------------------------------------------------
      CountIdx := 1;
      if Trim( qryVATBI1H.FieldByName('RE_NO').AsString) <> ''  then //책번호의 권
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('RE');
        AddBufferChild( qryVATBI1H.FieldByName('RE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI1H.FieldByName('SE_NO').AsString) <> ''  then //책번호의 호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('SE');
        AddBufferChild( qryVATBI1H.FieldByName('SE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI1H.FieldByName('FS_NO').AsString) <> '' then //일련번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('FS');
        AddBufferChild( qryVATBI1H.FieldByName('FS_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI1H.FieldByName('ACE_NO').AsString) <> '' then //관련참조번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('ACE');
        AddBufferChild( qryVATBI1H.FieldByName('ACE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI1H.FieldByName('RFF_NO').AsString) <> '' then //세금계산서번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('DM');
        AddBufferChild( qryVATBI1H.FieldByName('RFF_NO').AsString );
      end;
      //NAD---------------------------------------------------------------------
      NADCount := 1;
      AddBuffer('NAD '+ IntToStr(NADCount) +'      13');
      AddBufferChild('SE');
      AddBufferChild( qryVATBI1H.FieldByName('SE_SAUP').AsString );
      AddBufferChild('');
      AddBufferChild( qryVATBI1H.FieldByName('SE_ADDR1').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_ADDR2').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_ADDR3').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_ADDR4').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_ADDR5').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_NAME1').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_NAME2').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_NAME3').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('SE_SAUP1').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(NADCount);
      //FTX---------------------------------------------------------------------
      FTXCount := 1;
      if (Trim( qryVATBI1H.FieldByName('SE_FTX1').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('SE_FTX2').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('SE_FTX3').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('SE_FTX4').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('SE_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI1H.FieldByName('SE_FTX1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('SE_FTX2').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('SE_FTX3').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('SE_FTX4').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('SE_FTX5').AsString );
        Inc(FTXCount);
      end;
      //IMD---------------------------------------------------------------------
      //공급자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('SE_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //공급자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('SE_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //NAD---------------------------------------------------------------------
      AddBuffer('NAD '+ IntToStr(NADCount) +'      13'); //공급받는자
      AddBufferChild('BY');
      AddBufferChild( qryVATBI1H.FieldByName('BY_SAUP').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_SAUP_CODE').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_ADDR1').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_ADDR2').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_ADDR3').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_ADDR4').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_ADDR5').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_NAME1').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_NAME2').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_NAME3').AsString );
      AddBufferChild( qryVATBI1H.FieldByName('BY_SAUP1').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(NADCount);
      //FTX---------------------------------------------------------------------
      FTXCount := 1;  //공급받는자 담당자
      if (Trim( qryVATBI1H.FieldByName('BY_FTX1').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('BY_FTX2').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('BY_FTX3').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('BY_FTX4').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('BY_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX2').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX3').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX4').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX5').AsString );
        Inc(FTXCount);
      end;
      //공급받는자 담당자2
      if (Trim( qryVATBI1H.FieldByName('BY_FTX1_1').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('BY_FTX2_1').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('BY_FTX3_1').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('BY_FTX4_1').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('BY_FTX5_1').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX1_1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX2_1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX3_1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX4_1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('BY_FTX5_1').AsString );
      end;
      //IMD---------------------------------------------------------------------
      //공급받는자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('BY_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //공급받는자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('BY_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //NAD---------------------------------------------------------------------
      if (Trim( qryVATBI1H.FieldByName('AG_NAME1').AsString ) <> '' ) OR
         (Trim( qryVATBI1H.FieldByName('AG_NAME2').AsString ) <> '' ) OR
         (Trim( qryVATBI1H.FieldByName('AG_NAME3').AsString ) <> '' ) THEN
      BEGIN
        AddBuffer('NAD '+ IntToStr(NADCount) +'      13'); //수탁자
        AddBufferChild('AG');
        AddBufferChild( qryVATBI1H.FieldByName('AG_SAUP').AsString );
        AddBufferChild('');
        AddBufferChild( qryVATBI1H.FieldByName('AG_ADDR1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_ADDR2').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_ADDR3').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_ADDR4').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_ADDR5').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_NAME1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_NAME2').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_NAME3').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_SAUP1').AsString );
        AddBufferChild('');
        AddBufferChild('');
      end;
      //FTX---------------------------------------------------------------------
      //수탁자 담당자
      if (Trim( qryVATBI1H.FieldByName('AG_FTX1').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('AG_FTX2').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('AG_FTX3').AsString ) <> '') or (Trim( qryVATBI1H.FieldByName('AG_FTX4').AsString ) <> '') or
         (Trim( qryVATBI1H.FieldByName('AG_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX 1      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI1H.FieldByName('AG_FTX1').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_FTX2').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_FTX3').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_FTX4').AsString );
        AddBufferChild( qryVATBI1H.FieldByName('AG_FTX5').AsString );
      end;
      //IMD---------------------------------------------------------------------
      //수탁자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('AG_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //수탁자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI1H.FieldByName('AG_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //DTM---------------------------------------------------------------------
      AddBuffer('DTM 1      14'); //작성일자
      AddBufferChildTime('182', qryVATBI1H.FieldByName('DRAW_DAT').AsString , '101');
      //SEQ---------------------------------------------------------------------
      if (Trim(qryVATBI1H.FieldByName('DETAILNO').AsString) <> '') and (qryVATBI1H.FieldByName('DETAILNO').AsCurrency <> 0) then
      begin
        AddBuffer('SEQ 1      22'); //공란수
        AddBufferChild( qryVATBI1H.FieldByName('DETAILNO').AsCurrency );
      end;
      //MOA---------------------------------------------------------------------
      CountIdx := 1;
       //공급가액
      if (Trim(qryVATBI1H.FieldByName('SUP_AMT').AsString) <> '') and (qryVATBI1H.FieldByName('SUP_AMT').AsCurrency <> 0) then
      begin
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      23');
        AddBufferChild('79');
        AddBufferChild(IntToStr(qryVATBI1H.FieldByName('SUP_AMT').AsInteger)); //소숫점이하 절사
        AddBufferChild('KRW');
        Inc(CountIdx);
      end;
      //세액
      if (Trim(qryVATBI1H.FieldByName('TAX_AMT').AsString) <> '') then
      begin
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      23');
        AddBufferChild('176');
        AddBufferChild(IntToStr(qryVATBI1H.FieldByName('TAX_AMT').AsInteger)); //소숫점이하 절사
        AddBufferChild('KRW');
      end;
      //FTX---------------------------------------------------------------------
      StringFTX.Text := qryVATBI1H.FieldByName('REMARK1').AsString;  // 비고 참조사항
      if Trim(StringFTX.Text) <> '' then
      begin
        CountIdx := 1;
        //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
        AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
        AddBufferChild('AAI');
        for nLoop := 1 to StringFTX.Count do
        begin
          // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
          if ((nLoop mod 5) = 1) and (nLoop > 5) then
          begin
            Inc(CountIdx);
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
            AddBufferChild('AAI');
          end;

          AddBufferChild(StringFTX.Strings[nLoop-1]);
        end;
        // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
        if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
        begin
          // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
          for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
          begin
            AddBufferChild('');
          end;
        end
        else if (StringFTX.Count < 5) then
        begin
          for nLoop := 1 to (5-(StringFTX.Count)) do
          begin
            AddBufferChild('');
          end;
        end;
      end;
      //------------------------------------------------------------------------
      if qryVATBI1D.RecordCount > 0 then
      begin
        //품목내역의 개수만큼 반복
        LINCount := 1;
        qryVATBI1D.First;
        while not qryVATBI1D.Eof do
        begin
          //LIN-----------------------------------------------------------------
          //상품 및 규격기재시 순번을 기재하는 항목
          //ShowMessage(IntToStr(LINCount));
          if Trim(qryVATBI1D.FieldByName('SEQ').AsString) <> '' then
          begin
            AddBuffer('LIN '+ IntToStr(LINCount) +'      16');
            AddBufferChild( qryVATBI1D.FieldByName('SEQ').AsString );
            Inc(LINCount);
          end;
          //DTM-----------------------------------------------------------------
          if Trim(qryVATBI1D.FieldByName('DE_DATE').AsString) <> '' then
          begin
            AddBuffer('DTM 1      24'); //공급일자
            AddBufferChildTime('35', qryVATBI1D.FieldByName('DE_DATE').AsString ,'102');
          end;
          //IMD-----------------------------------------------------------------
          StringIMD.Clear;
          StringIMD.Text := ( qryVATBI1D.FieldByName('NAME1').AsString );
          if Trim(StringIMD.Text) <> '' then
          begin
            for uptaLoop := 1 to StringIMD.Count do
            begin
              AddBuffer('IMD '+ IntToStr(uptaLoop) +'      25');
              AddBufferChild('1AA');
              AddBufferChild(StringIMD.Strings[uptaLoop-1]);
            end;
          end;
          //FTX-----------------------------------------------------------------
          //품목의 규격---------------------------------------------------------
          StringFTX.Clear;
          StringFTX.Text := qryVATBI1D.FieldByName('SIZE1').AsString;
          if Trim(StringFTX.Text) <> '' then
          begin
            CountIdx := 1;
            //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
            AddBufferChild('AAA');
            Inc(CountIdx);
            for nLoop := 1 to StringFTX.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
                AddBufferChild('AAA');
                Inc(CountIdx);
              end;

              AddBufferChild(StringFTX.Strings[nLoop-1]);
            end;
            // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringFTX.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringFTX.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
          //품목의 REMARK-------------------------------------------------------
          StringFTX.Clear;
          StringFTX.Text := qryVATBI1D.FieldByName('DE_REM1').AsString;
          if Trim(StringFTX.Text) <> '' then
          begin
            //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
            AddBufferChild('ACB');
            for nLoop := 1 to StringFTX.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                Inc(CountIdx);
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
                AddBufferChild('ACB');
              end;

              AddBufferChild(StringFTX.Strings[nLoop-1]);
            end;
            // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringFTX.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringFTX.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
          //QTY-----------------------------------------------------------------
          if (Trim(qryVATBI1D.FieldByName('QTY').AsString) <> '') and (qryVATBI1D.FieldByName('QTY').AsCurrency <> 0) then
          begin // 물품의 수량
            CountIdx := 1;
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('1');
            AddBufferChild( CurrToStr( qryVATBI1D.FieldByName('QTY').AsCurrency ) );
            AddBufferChild( qryVATBI1D.FieldByName('QTY_G').AsString );
            Inc(CountIdx);
          end;
          if (Trim(qryVATBI1D.FieldByName('STQTY').AsString) <> '') and (qryVATBI1D.FieldByName('STQTY').AsCurrency <> 0) then
          begin //물품의 수량소계
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('3');
            AddBufferChild( CurrToStr( qryVATBI1D.FieldByName('STQTY').AsCurrency ) );
            AddBufferChild( qryVATBI1D.FieldByName('STQTY_G').AsString );
          end;
          //PRI-----------------------------------------------------------------
          if ((Trim( qryVATBI1D.FieldByName('PRICE').AsString ) <> '') and ( qryVATBI1D.FieldByName('PRICE').AsCurrency <> 0)) or
             ((Trim( qryVATBI1D.FieldByName('QTYG').AsString ) <> '') and (qryVATBI1D.FieldByName('QTYG').AsCurrency <> 0)) then
          begin // 물품의 단가, 단가기준수량
            AddBuffer('PRI 1      28'); //공급일자
            AddBufferChild('CAL');
            AddBufferChild( CurrToStr(qryVATBI1D.FieldByName('PRICE').AsCurrency) );
            AddBufferChild('PE');
            AddBufferChild('CUP');
            AddBufferChild( CurrToStr(qryVATBI1D.FieldByName('QTYG').AsCurrency) );
            AddBufferChild( qryVATBI1D.FieldByName('QTYG_G').AsString );
          end;
          //MOA-----------------------------------------------------------------
          CountIdx := 1;
          if ((Trim( qryVATBI1D.FieldByName('SUPAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('SUPAMT').AsCurrency <> 0)) then
          begin //원화공급가액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('203');
            AddBufferChild( qryVATBI1D.FieldByName('SUPAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI1D.FieldByName('TAXAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('TAXAMT').AsCurrency <> 0)) then
          begin //세액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('124');
            AddBufferChild( qryVATBI1D.FieldByName('TAXAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI1D.FieldByName('USAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('USAMT').AsCurrency <> 0)) then
          begin //외화공급가액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('261');
            AddBufferChild( qryVATBI1D.FieldByName('USAMT').AsCurrency );
            AddBufferChild( qryVATBI1D.FieldByName('USAMT_G').AsString );
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI1D.FieldByName('SUPSTAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('SUPSTAMT').AsCurrency <> 0)) then
          begin //원화공급가액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('17');
            AddBufferChild( qryVATBI1D.FieldByName('SUPSTAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI1D.FieldByName('TAXSTAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('TAXSTAMT').AsCurrency <> 0)) then
          begin //세액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('168');
            AddBufferChild( qryVATBI1D.FieldByName('TAXSTAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI1D.FieldByName('USSTAMT').AsString ) <> '') and ( qryVATBI1D.FieldByName('USSTAMT').AsCurrency <> 0)) then
          begin //외화공급가액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('289');
            AddBufferChild( qryVATBI1D.FieldByName('USSTAMT').AsCurrency );
            AddBufferChild( qryVATBI1D.FieldByName('USSTAMT_G').AsString );
          end;
          if ((Trim( qryVATBI1D.FieldByName('RATE').AsString ) <> '') and ( qryVATBI1D.FieldByName('RATE').AsCurrency <> 0)) then
          begin //외화공급가액 소계
            AddBuffer('CUX 1      2A');
            AddBufferChild( qryVATBI1D.FieldByName('RATE').AsCurrency );
          end;

          qryVATBI1D.Next;

        end;//While End
        //UNS-------------------------------------------------------------------
        AddBuffer('UNS 1        ');
        AddBufferChild('S');
      end; //qryVATBI1D.RecordCount > 0 조건문 End

      //MOA---------------------------------------------------------------------
      CountIdx := 1;
      if ((Trim( qryVATBI1H.FieldByName('TAMT').AsString ) <> '') and ( qryVATBI1H.FieldByName('TAMT').AsCurrency <> 0)) then
      begin //총금액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('128');
        AddBufferChild( qryVATBI1H.FieldByName('TAMT').AsCurrency );
        AddBufferChild( 'KRW');
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI1H.FieldByName('SUPTAMT').AsString ) <> '') and ( qryVATBI1H.FieldByName('SUPTAMT').AsCurrency <> 0)) then
      begin //총공급가액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('79');
        AddBufferChild( qryVATBI1H.FieldByName('SUPTAMT').AsCurrency );
        AddBufferChild( 'KRW');
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI1H.FieldByName('TAXTAMT').AsString ) <> '') and ( qryVATBI1H.FieldByName('TAXTAMT').AsCurrency <> 0)) then
      begin //총세액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('176');
        AddBufferChild( qryVATBI1H.FieldByName('TAXTAMT').AsCurrency );
        AddBufferChild( 'KRW' );
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI1H.FieldByName('USTAMT').AsString ) <> '') and ( qryVATBI1H.FieldByName('USTAMT').AsCurrency <> 0)) then
      begin //총외화공급가액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('14');
        AddBufferChild( qryVATBI1H.FieldByName('USTAMT').AsCurrency );
        AddBufferChild( qryVATBI1H.FieldByName('USTAMTC').AsString );
        Inc(CountIdx);
      end;
      //CNT---------------------------------------------------------------------
      if ((Trim( qryVATBI1H.FieldByName('TQTY').AsString ) <> '') and ( qryVATBI1H.FieldByName('TQTY').AsCurrency <> 0)) then
      begin //총수량
        AddBuffer('CNT 1      18');
        AddBufferChild('1AA');
        AddBufferChild( qryVATBI1H.FieldByName('TQTY').AsCurrency );
        AddBufferChild( qryVATBI1H.FieldByName('TQTYC').AsString );
      end;
      //PAI---------------------------------------------------------------------
      CountIdx := 1;
      //결제방법 - 현금---------------------------------------------------------
      if ( qryVATBI1H.FieldByName('AMT11').AsCurrency <> 0) or ( qryVATBI1H.FieldByName('AMT12').AsCurrency <> 0) or
         (trim( qryVATBI1H.FieldByName('AMT11').AsString) <> '') or (trim( qryVATBI1H.FieldByName('AMT12').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('10');
        MOACount := 1;
        if (qryVATBI1H.FieldByName('AMT12').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT12').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //현금-원화
          AddBufferChild( qryVATBI1H.FieldByName('AMT12').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI1H.FieldByName('AMT11').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT11').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //현금-외화
          AddBufferChild( qryVATBI1H.FieldByName('AMT11').AsCurrency );
          AddBufferChild( qryVATBI1H.FieldByName('AMT11C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 수표---------------------------------------------------------
      if ( qryVATBI1H.FieldByName('AMT21').AsCurrency <> 0) or ( qryVATBI1H.FieldByName('AMT22').AsCurrency <> 0) or
         (trim( qryVATBI1H.FieldByName('AMT21').AsString) <> '') or (trim( qryVATBI1H.FieldByName('AMT22').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('20');
        MOACount := 1;
        if (qryVATBI1H.FieldByName('AMT22').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT22').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //수표-원화
          AddBufferChild( qryVATBI1H.FieldByName('AMT22').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI1H.FieldByName('AMT21').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT21').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //수표-외화
          AddBufferChild( qryVATBI1H.FieldByName('AMT21').AsCurrency );
          AddBufferChild( qryVATBI1H.FieldByName('AMT21C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 어음---------------------------------------------------------
      if ( qryVATBI1H.FieldByName('AMT31').AsCurrency <> 0) or ( qryVATBI1H.FieldByName('AMT32').AsCurrency <> 0) or
         (trim( qryVATBI1H.FieldByName('AMT31').AsString) <> '') or (trim( qryVATBI1H.FieldByName('AMT32').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('2AA');
        MOACount := 1;
        if (qryVATBI1H.FieldByName('AMT32').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT32').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //어음-원화
          AddBufferChild( qryVATBI1H.FieldByName('AMT32').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI1H.FieldByName('AMT31').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT31').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //어음-외화
          AddBufferChild( qryVATBI1H.FieldByName('AMT31').AsCurrency );
          AddBufferChild( qryVATBI1H.FieldByName('AMT31C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 외상미수금---------------------------------------------------
      if ( qryVATBI1H.FieldByName('AMT41').AsCurrency <> 0) or ( qryVATBI1H.FieldByName('AMT42').AsCurrency <> 0) or
         (trim( qryVATBI1H.FieldByName('AMT41').AsString) <> '') or (trim( qryVATBI1H.FieldByName('AMT42').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('30');
        MOACount := 1;
        if (qryVATBI1H.FieldByName('AMT42').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT42').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //외상미수금-원화
          AddBufferChild( qryVATBI1H.FieldByName('AMT42').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI1H.FieldByName('AMT41').AsCurrency <> 0) and (Trim(qryVATBI1H.FieldByName('AMT41').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //외상미수금-외화
          AddBufferChild( qryVATBI1H.FieldByName('AMT41').AsCurrency );
          AddBufferChild( qryVATBI1H.FieldByName('AMT41C').AsString );
        end;
      end;
      //GIS---------------------------------------------------------------------
      if qryVATBI1H.FieldByName('INDICATOR').AsString <> '' then
      begin
        AddBuffer('GIS 1      1A');
        AddBufferChild( qryVATBI1H.FieldByName('INDICATOR').AsString );
      end;

      AddBuffer('UNT',False);
      StringBuffer.Text := Buffer;
      Result := Buffer;
    end; // with문
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
    StringIMD.Free;
    DMDocumentQry.CLOSE_VATBI1;
  end;
end;

function VATBI3(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,  TXTPATH, FILENAME: string;
  CountIdx,NADCount,FTXCount,
  LINCount,MOACount,
  uptaLoop,ItemLoop           : Integer;
  StringBuffer                : TStringList;
  StringFTX                   : TStringList;
  StringIMD                   : TStringList;
  i , nLoop  : Integer;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;
  StringIMD := TStringList.Create;
  try
    DMDocumentQry.OPEN_VATBI3(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := 'VATBI3' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'VATBIL ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['VATBIL']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    with DMDocumentQry do
    begin
      //BGM-----------------------------------------------------------------------
      AddBuffer('BGM 1      10');
      AddBufferChild( qryVATBI3H.FieldByName('VAT_CODE').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('VAT_TYPE').AsString );
      AddBufferChild( MAINT_NO );
      AddBufferChild( qryVATBI3H.FieldByName('MESSAGE1').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('MESSAGE2').AsString );
      //GIS-----------------------------------------------------------------------
      if Trim(qryVATBI3H.FieldByName('NEW_INDICATOR').AsString) <> '' then  //수정사유코드
      begin
        AddBuffer('GIS 1      11');
        AddBufferChild('BD');
        AddBufferChild( qryVATBI3H.FieldByName('NEW_INDICATOR').AsString);
      end;
      //RFF-----------------------------------------------------------------------
      CountIdx := 1;
      if Trim( qryVATBI3H.FieldByName('RE_NO').AsString) <> ''  then //책번호의 권
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('RE');
        AddBufferChild( qryVATBI3H.FieldByName('RE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI3H.FieldByName('SE_NO').AsString) <> ''  then //책번호의 호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('SE');
        AddBufferChild( qryVATBI3H.FieldByName('SE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI3H.FieldByName('FS_NO').AsString) <> '' then //일련번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('FS');
        AddBufferChild( qryVATBI3H.FieldByName('FS_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI3H.FieldByName('ACE_NO').AsString) <> '' then //관련참조번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('ACE');
        AddBufferChild( qryVATBI3H.FieldByName('ACE_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim(qryVATBI3H.FieldByName('RFF_NO').AsString) <> '' then //세금계산서번호
      begin
        AddBuffer('RFF '+ IntToStr(CountIdx) +'      12');
        AddBufferChild('DM');
        AddBufferChild( qryVATBI3H.FieldByName('RFF_NO').AsString );
      end;
      //NAD---------------------------------------------------------------------
      NADCount := 1;
      AddBuffer('NAD '+ IntToStr(NADCount) +'      13');
      AddBufferChild('SE');
      AddBufferChild( qryVATBI3H.FieldByName('SE_SAUP').AsString );
      AddBufferChild('');
      AddBufferChild( qryVATBI3H.FieldByName('SE_ADDR1').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_ADDR2').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_ADDR3').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_ADDR4').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_ADDR5').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_NAME1').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_NAME2').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_NAME3').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('SE_SAUP1').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(NADCount);
      //FTX---------------------------------------------------------------------
      FTXCount := 1;
      if (Trim( qryVATBI3H.FieldByName('SE_FTX1').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('SE_FTX2').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('SE_FTX3').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('SE_FTX4').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('SE_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI3H.FieldByName('SE_FTX1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('SE_FTX2').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('SE_FTX3').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('SE_FTX4').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('SE_FTX5').AsString );
        Inc(FTXCount);
      end;
      //IMD---------------------------------------------------------------------
      //공급자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('SE_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //공급자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('SE_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //NAD---------------------------------------------------------------------
      AddBuffer('NAD '+ IntToStr(NADCount) +'      13'); //공급받는자
      AddBufferChild('BY');
      AddBufferChild( qryVATBI3H.FieldByName('BY_SAUP').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_SAUP_CODE').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_ADDR1').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_ADDR2').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_ADDR3').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_ADDR4').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_ADDR5').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_NAME1').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_NAME2').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_NAME3').AsString );
      AddBufferChild( qryVATBI3H.FieldByName('BY_SAUP1').AsString );
      AddBufferChild('');
      AddBufferChild('');
      Inc(NADCount);
      //FTX---------------------------------------------------------------------
      FTXCount := 1;  //공급받는자 담당자
      if (Trim( qryVATBI3H.FieldByName('BY_FTX1').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('BY_FTX2').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('BY_FTX3').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('BY_FTX4').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('BY_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX2').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX3').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX4').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX5').AsString );
        Inc(FTXCount);
      end;
      //공급받는자 담당자2
      if (Trim( qryVATBI3H.FieldByName('BY_FTX1_1').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('BY_FTX2_1').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('BY_FTX3_1').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('BY_FTX4_1').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('BY_FTX5_1').AsString ) <> '') then
      begin
        AddBuffer('FTX '+ IntToStr(FTXCount) +'      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX1_1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX2_1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX3_1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX4_1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('BY_FTX5_1').AsString );
      end;
      //IMD---------------------------------------------------------------------
      //공급받는자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('BY_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //공급받는자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('BY_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //NAD---------------------------------------------------------------------
      if (Trim( qryVATBI3H.FieldByName('AG_NAME1').AsString ) <> '' ) OR
         (Trim( qryVATBI3H.FieldByName('AG_NAME2').AsString ) <> '' ) OR
         (Trim( qryVATBI3H.FieldByName('AG_NAME3').AsString ) <> '' ) THEN
      BEGIN
        AddBuffer('NAD '+ IntToStr(NADCount) +'      13'); //수탁자
        AddBufferChild('AG');
        AddBufferChild( qryVATBI3H.FieldByName('AG_SAUP').AsString );
        AddBufferChild('');
        AddBufferChild( qryVATBI3H.FieldByName('AG_ADDR1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_ADDR2').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_ADDR3').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_ADDR4').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_ADDR5').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_NAME1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_NAME2').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_NAME3').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_SAUP1').AsString );
        AddBufferChild('');
        AddBufferChild('');
      end;
      //FTX---------------------------------------------------------------------
      //수탁자 담당자
      if (Trim( qryVATBI3H.FieldByName('AG_FTX1').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('AG_FTX2').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('AG_FTX3').AsString ) <> '') or (Trim( qryVATBI3H.FieldByName('AG_FTX4').AsString ) <> '') or
         (Trim( qryVATBI3H.FieldByName('AG_FTX5').AsString ) <> '') then
      begin
        AddBuffer('FTX 1      20');
        AddBufferChild('5AN');
        AddBufferChild( qryVATBI3H.FieldByName('AG_FTX1').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_FTX2').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_FTX3').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_FTX4').AsString );
        AddBufferChild( qryVATBI3H.FieldByName('AG_FTX5').AsString );
      end;
      //IMD---------------------------------------------------------------------
      //수탁자의 업태
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('AG_UPTA1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for uptaLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(uptaLoop) +'      21');
          AddBufferChild('SG');
          AddBufferChild(StringIMD.Strings[uptaLoop-1]);
        end;
      end;
      //수탁자의 종목
      StringIMD.Clear;
      StringIMD.Text := ( qryVATBI3H.FieldByName('AG_ITEM1').AsString );
      if Trim(StringIMD.Text) <> '' then
      begin
        for ItemLoop := 1 to StringIMD.Count do
        begin
          AddBuffer('IMD '+ IntToStr(ItemLoop+(uptaLoop-1)) +'      21');
          AddBufferChild('HN');
          AddBufferChild(StringIMD.Strings[ItemLoop-1]);
        end;
      end;
      //DTM---------------------------------------------------------------------
      AddBuffer('DTM 1      14'); //작성일자
      AddBufferChildTime('182', qryVATBI3H.FieldByName('DRAW_DAT').AsString , '101');
      //SEQ---------------------------------------------------------------------
      if (Trim(qryVATBI3H.FieldByName('DETAILNO').AsString) <> '') and (qryVATBI3H.FieldByName('DETAILNO').AsCurrency <> 0) then
      begin
        AddBuffer('SEQ 1      22'); //공란수
        AddBufferChild( qryVATBI3H.FieldByName('DETAILNO').AsCurrency );
      end;
      //MOA---------------------------------------------------------------------
      CountIdx := 1;
       //공급가액
      if (Trim(qryVATBI3H.FieldByName('SUP_AMT').AsString) <> '') and (qryVATBI3H.FieldByName('SUP_AMT').AsCurrency <> 0) then
      begin
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      23');
        AddBufferChild('79');
        AddBufferChild(IntToStr(qryVATBI3H.FieldByName('SUP_AMT').AsInteger)); //소숫점이하 절사
        AddBufferChild('KRW');
        Inc(CountIdx);
      end;
      //세액
      if (Trim(qryVATBI3H.FieldByName('TAX_AMT').AsString) <> '') then
      begin
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      23');
        AddBufferChild('176');
        AddBufferChild(IntToStr(qryVATBI3H.FieldByName('TAX_AMT').AsInteger)); //소숫점이하 절사
        AddBufferChild('KRW');
      end;
      //FTX---------------------------------------------------------------------
      StringFTX.Text := qryVATBI3H.FieldByName('REMARK1').AsString;  // 비고 참조사항
      if Trim(StringFTX.Text) <> '' then
      begin
        CountIdx := 1;
        //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
        AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
        AddBufferChild('AAI');
        for nLoop := 1 to StringFTX.Count do
        begin
          // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
          if ((nLoop mod 5) = 1) and (nLoop > 5) then
          begin
            Inc(CountIdx);
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      15');
            AddBufferChild('AAI');
          end;

          AddBufferChild(StringFTX.Strings[nLoop-1]);
        end;
        // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
        if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
        begin
          // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
          for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
          begin
            AddBufferChild('');
          end;
        end
        else if (StringFTX.Count < 5) then
        begin
          for nLoop := 1 to (5-(StringFTX.Count)) do
          begin
            AddBufferChild('');
          end;
        end;
      end;
      //------------------------------------------------------------------------
      if qryVATBI3D.RecordCount > 0 then
      begin
        //품목내역의 개수만큼 반복
        LINCount := 1;
        qryVATBI3D.First;
        while not qryVATBI3D.Eof do
        begin
          //LIN-----------------------------------------------------------------
          //상품 및 규격기재시 순번을 기재하는 항목
          //ShowMessage(IntToStr(LINCount));
          if Trim(qryVATBI3D.FieldByName('SEQ').AsString) <> '' then
          begin
            AddBuffer('LIN '+ IntToStr(LINCount) +'      16');
            AddBufferChild( qryVATBI3D.FieldByName('SEQ').AsString );
            Inc(LINCount);
          end;
          //DTM-----------------------------------------------------------------
          if Trim(qryVATBI3D.FieldByName('DE_DATE').AsString) <> '' then
          begin
            AddBuffer('DTM 1      24'); //공급일자
            AddBufferChildTime('35', qryVATBI3D.FieldByName('DE_DATE').AsString ,'102');
          end;
          //IMD-----------------------------------------------------------------
          StringIMD.Clear;
          StringIMD.Text := ( qryVATBI3D.FieldByName('NAME1').AsString );
          if Trim(StringIMD.Text) <> '' then
          begin
            for uptaLoop := 1 to StringIMD.Count do
            begin
              AddBuffer('IMD '+ IntToStr(uptaLoop) +'      25');
              AddBufferChild('1AA');
              AddBufferChild(StringIMD.Strings[uptaLoop-1]);
            end;
          end;
          //FTX-----------------------------------------------------------------
          //품목의 규격---------------------------------------------------------
          StringFTX.Clear;
          StringFTX.Text := qryVATBI3D.FieldByName('SIZE1').AsString;
          if Trim(StringFTX.Text) <> '' then
          begin
            CountIdx := 1;
            //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
            AddBufferChild('AAA');
            Inc(CountIdx);
            for nLoop := 1 to StringFTX.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
                AddBufferChild('AAA');
                Inc(CountIdx);
              end;

              AddBufferChild(StringFTX.Strings[nLoop-1]);
            end;
            // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringFTX.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringFTX.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
          //품목의 REMARK-------------------------------------------------------
          StringFTX.Clear;
          StringFTX.Text := qryVATBI3D.FieldByName('DE_REM1').AsString;
          if Trim(StringFTX.Text) <> '' then
          begin
            //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
            AddBufferChild('ACB');
            for nLoop := 1 to StringFTX.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                Inc(CountIdx);
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      26');
                AddBufferChild('ACB');
              end;

              AddBufferChild(StringFTX.Strings[nLoop-1]);
            end;
            // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringFTX.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringFTX.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
          //QTY-----------------------------------------------------------------
          if (Trim(qryVATBI3D.FieldByName('QTY').AsString) <> '') and (qryVATBI3D.FieldByName('QTY').AsCurrency <> 0) then
          begin // 물품의 수량
            CountIdx := 1;
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('1');
            AddBufferChild( CurrToStr( qryVATBI3D.FieldByName('QTY').AsCurrency ) );
            AddBufferChild( qryVATBI3D.FieldByName('QTY_G').AsString );
            Inc(CountIdx);
          end;
          if (Trim(qryVATBI3D.FieldByName('STQTY').AsString) <> '') and (qryVATBI3D.FieldByName('STQTY').AsCurrency <> 0) then
          begin //물품의 수량소계
            AddBuffer('QTY '+ IntToStr(CountIdx) +'      27');
            AddBufferChild('3');
            AddBufferChild( CurrToStr( qryVATBI3D.FieldByName('STQTY').AsCurrency ) );
            AddBufferChild( qryVATBI3D.FieldByName('STQTY_G').AsString );
          end;
          //PRI-----------------------------------------------------------------
          if ((Trim( qryVATBI3D.FieldByName('PRICE').AsString ) <> '') and ( qryVATBI3D.FieldByName('PRICE').AsCurrency <> 0)) or
             ((Trim( qryVATBI3D.FieldByName('QTYG').AsString ) <> '') and (qryVATBI3D.FieldByName('QTYG').AsCurrency <> 0)) then
          begin // 물품의 단가, 단가기준수량
            AddBuffer('PRI 1      28'); //공급일자
            AddBufferChild('CAL');
            AddBufferChild( CurrToStr(qryVATBI3D.FieldByName('PRICE').AsCurrency) );
            AddBufferChild('PE');
            AddBufferChild('CUP');
            AddBufferChild( CurrToStr(qryVATBI3D.FieldByName('QTYG').AsCurrency) );
            AddBufferChild( qryVATBI3D.FieldByName('QTYG_G').AsString );
          end;
          //MOA-----------------------------------------------------------------
          CountIdx := 1;
          if ((Trim( qryVATBI3D.FieldByName('SUPAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('SUPAMT').AsCurrency <> 0)) then
          begin //원화공급가액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('203');
            AddBufferChild( qryVATBI3D.FieldByName('SUPAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI3D.FieldByName('TAXAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('TAXAMT').AsCurrency <> 0)) then
          begin //세액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('124');
            AddBufferChild( qryVATBI3D.FieldByName('TAXAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI3D.FieldByName('USAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('USAMT').AsCurrency <> 0)) then
          begin //외화공급가액
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('261');
            AddBufferChild( qryVATBI3D.FieldByName('USAMT').AsCurrency );
            AddBufferChild( qryVATBI3D.FieldByName('USAMT_G').AsString );
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI3D.FieldByName('SUPSTAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('SUPSTAMT').AsCurrency <> 0)) then
          begin //원화공급가액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('17');
            AddBufferChild( qryVATBI3D.FieldByName('SUPSTAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI3D.FieldByName('TAXSTAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('TAXSTAMT').AsCurrency <> 0)) then
          begin //세액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('168');
            AddBufferChild( qryVATBI3D.FieldByName('TAXSTAMT').AsCurrency );
            AddBufferChild('KRW');
            Inc(CountIdx);
          end;
          if ((Trim( qryVATBI3D.FieldByName('USSTAMT').AsString ) <> '') and ( qryVATBI3D.FieldByName('USSTAMT').AsCurrency <> 0)) then
          begin //외화공급가액 소계
            AddBuffer('MOA '+ IntToStr(CountIdx) +'      29');
            AddBufferChild('289');
            AddBufferChild( qryVATBI3D.FieldByName('USSTAMT').AsCurrency );
            AddBufferChild( qryVATBI3D.FieldByName('USSTAMT_G').AsString );
          end;
          if ((Trim( qryVATBI3D.FieldByName('RATE').AsString ) <> '') and ( qryVATBI3D.FieldByName('RATE').AsCurrency <> 0)) then
          begin //외화공급가액 소계
            AddBuffer('CUX 1      2A');
            AddBufferChild( qryVATBI3D.FieldByName('RATE').AsCurrency );
          end;

          qryVATBI3D.Next;

        end;//While End
        //UNS-------------------------------------------------------------------
        AddBuffer('UNS 1        ');
        AddBufferChild('S');
      end; //qryVATBI3D.RecordCount > 0 조건문 End

      //MOA---------------------------------------------------------------------
      CountIdx := 1;
      if ((Trim( qryVATBI3H.FieldByName('TAMT').AsString ) <> '') and ( qryVATBI3H.FieldByName('TAMT').AsCurrency <> 0)) then
      begin //총금액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('128');
        AddBufferChild( qryVATBI3H.FieldByName('TAMT').AsCurrency );
        AddBufferChild( 'KRW');
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI3H.FieldByName('SUPTAMT').AsString ) <> '') and ( qryVATBI3H.FieldByName('SUPTAMT').AsCurrency <> 0)) then
      begin //총공급가액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('79');
        AddBufferChild( qryVATBI3H.FieldByName('SUPTAMT').AsCurrency );
        AddBufferChild( 'KRW');
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI3H.FieldByName('TAXTAMT').AsString ) <> '') and ( qryVATBI3H.FieldByName('TAXTAMT').AsCurrency <> 0)) then
      begin //총세액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('176');
        AddBufferChild( qryVATBI3H.FieldByName('TAXTAMT').AsCurrency );
        AddBufferChild( 'KRW' );
        Inc(CountIdx);
      end;
      if ((Trim( qryVATBI3H.FieldByName('USTAMT').AsString ) <> '') and ( qryVATBI3H.FieldByName('USTAMT').AsCurrency <> 0)) then
      begin //총외화공급가액
        AddBuffer('MOA '+ IntToStr(CountIdx) +'      17');
        AddBufferChild('14');
        AddBufferChild( qryVATBI3H.FieldByName('USTAMT').AsCurrency );
        AddBufferChild( qryVATBI3H.FieldByName('USTAMTC').AsString );
        Inc(CountIdx);
      end;
      //CNT---------------------------------------------------------------------
      if ((Trim( qryVATBI3H.FieldByName('TQTY').AsString ) <> '') and ( qryVATBI3H.FieldByName('TQTY').AsCurrency <> 0)) then
      begin //총수량
        AddBuffer('CNT 1      18');
        AddBufferChild('1AA');
        AddBufferChild( qryVATBI3H.FieldByName('TQTY').AsCurrency );
        AddBufferChild( qryVATBI3H.FieldByName('TQTYC').AsString );
      end;
      //PAI---------------------------------------------------------------------
      CountIdx := 1;
      //결제방법 - 현금---------------------------------------------------------
      if ( qryVATBI3H.FieldByName('AMT11').AsCurrency <> 0) or ( qryVATBI3H.FieldByName('AMT12').AsCurrency <> 0) or
         (trim( qryVATBI3H.FieldByName('AMT11').AsString) <> '') or (trim( qryVATBI3H.FieldByName('AMT12').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('10');
        MOACount := 1;
        if (qryVATBI3H.FieldByName('AMT12').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT12').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //현금-원화
          AddBufferChild( qryVATBI3H.FieldByName('AMT12').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI3H.FieldByName('AMT11').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT11').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //현금-외화
          AddBufferChild( qryVATBI3H.FieldByName('AMT11').AsCurrency );
          AddBufferChild( qryVATBI3H.FieldByName('AMT11C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 수표---------------------------------------------------------
      if ( qryVATBI3H.FieldByName('AMT21').AsCurrency <> 0) or ( qryVATBI3H.FieldByName('AMT22').AsCurrency <> 0) or
         (trim( qryVATBI3H.FieldByName('AMT21').AsString) <> '') or (trim( qryVATBI3H.FieldByName('AMT22').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('20');
        MOACount := 1;
        if (qryVATBI3H.FieldByName('AMT22').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT22').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //수표-원화
          AddBufferChild( qryVATBI3H.FieldByName('AMT22').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI3H.FieldByName('AMT21').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT21').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //수표-외화
          AddBufferChild( qryVATBI3H.FieldByName('AMT21').AsCurrency );
          AddBufferChild( qryVATBI3H.FieldByName('AMT21C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 어음---------------------------------------------------------
      if ( qryVATBI3H.FieldByName('AMT31').AsCurrency <> 0) or ( qryVATBI3H.FieldByName('AMT32').AsCurrency <> 0) or
         (trim( qryVATBI3H.FieldByName('AMT31').AsString) <> '') or (trim( qryVATBI3H.FieldByName('AMT32').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('2AA');
        MOACount := 1;
        if (qryVATBI3H.FieldByName('AMT32').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT32').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //어음-원화
          AddBufferChild( qryVATBI3H.FieldByName('AMT32').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI3H.FieldByName('AMT31').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT31').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //어음-외화
          AddBufferChild( qryVATBI3H.FieldByName('AMT31').AsCurrency );
          AddBufferChild( qryVATBI3H.FieldByName('AMT31C').AsString );
        end;
        Inc(CountIdx);
      end;
      //결제방법 - 외상미수금---------------------------------------------------
      if ( qryVATBI3H.FieldByName('AMT41').AsCurrency <> 0) or ( qryVATBI3H.FieldByName('AMT42').AsCurrency <> 0) or
         (trim( qryVATBI3H.FieldByName('AMT41').AsString) <> '') or (trim( qryVATBI3H.FieldByName('AMT42').AsString) <> '') then
      begin
        AddBuffer('PAI '+ IntToStr(CountIdx) +'      19');
        AddBufferChild('30');
        MOACount := 1;
        if (qryVATBI3H.FieldByName('AMT42').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT42').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AE'); //외상미수금-원화
          AddBufferChild( qryVATBI3H.FieldByName('AMT42').AsCurrency );
          AddBufferChild('KRW');
          Inc(MOACount);
        end;
        if (qryVATBI3H.FieldByName('AMT41').AsCurrency <> 0) and (Trim(qryVATBI3H.FieldByName('AMT41').AsString) <> '')then
        begin
          AddBuffer('MOA '+ IntToStr(MOACount) +'      2B');
          AddBufferChild('2AD'); //외상미수금-외화
          AddBufferChild( qryVATBI3H.FieldByName('AMT41').AsCurrency );
          AddBufferChild( qryVATBI3H.FieldByName('AMT41C').AsString );
        end;
      end;
      //GIS---------------------------------------------------------------------
      if qryVATBI3H.FieldByName('INDICATOR').AsString <> '' then
      begin
        AddBuffer('GIS 1      1A');
        AddBufferChild( qryVATBI3H.FieldByName('INDICATOR').AsString );
      end;

      AddBuffer('UNT',False);
      StringBuffer.Text := Buffer;
      Result := Buffer;
    end; // with문
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
    StringIMD.Free;
    DMDocumentQry.CLOSE_VATBI3;
  end;
end;

function LOCRC2(MAINT_NO,RecvCode : String):String;
var
  RECEIVER,TXTPATH,FILENAME  : string;
  StringBuffer               : TStringList;
  StringFTX                  : TStringList;
  StringIMD                  : TStringList;
  i , nLoop , DetailCount ,
  countIdx , TaxCount        : Integer;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;
  StringIMD := TStringList.Create;

  try
    DMDocumentQry.OPEN_LOCRC2(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := 'LOCRC2' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'LOCRCT ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['LOCRC2']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    with DMDocumentQry do
    begin
      //BGM---------------------------------------------------------------------
      AddBuffer('BGM 1      10');
      AddBufferChild('2AH');
      AddBufferChild( qryLOCRC2H.FieldByName('MAINT_NO').AsString );
      AddBufferChild( qryLOCRC2H.FieldByName('MESSAGE1').AsString );
      AddBufferChild( qryLOCRC2H.FieldByName('MESSAGE2').AsString );
      //RFF---------------------------------------------------------------------
      CountIdx := 1;
      if Trim( qryLOCRC2H.FieldByName('RFF_NO').AsString ) <> '' then
      begin // 발급번호
        AddBuffer( 'RFF '+ IntToStr(CountIdx) +'      11' );
        AddBufferChild('DM');
        AddBufferChild( qryLOCRC2H.FieldByName('RFF_NO').AsString );
        Inc(CountIdx);
      end;
      if Trim( qryLOCRC2H.FieldByName('BSN_HSCODE').AsString ) <> '' then
      begin // 대표공급물품 HSCODE
        AddBuffer( 'RFF '+ IntToStr(CountIdx) +'      11' );
        AddBufferChild('HS');
        AddBufferChild( qryLOCRC2H.FieldByName('BSN_HSCODE').AsString );
      end;
      //DTM---------------------------------------------------------------------
      CountIdx := 1;
      if Trim( qryLOCRC2H.FieldByName('GET_DAT').AsString ) <> '' then
      begin // 인수일자
        AddBuffer( 'DTM '+ IntToStr(CountIdx) +'      12' );
        AddBufferChildTime( '50' , qryLOCRC2H.FieldByName('GET_DAT').AsString , '101' );
        Inc(CountIdx);
      end;
      if Trim( qryLOCRC2H.FieldByName('ISS_DAT').AsString ) <> '' then
      begin // 발급일자
        AddBuffer( 'DTM '+ IntToStr(CountIdx) +'      12' );
        AddBufferChildTime( '182' , qryLOCRC2H.FieldByName('ISS_DAT').AsString , '101' );
        Inc(CountIdx);
      end; // 문서유효일자
      if Trim( qryLOCRC2H.FieldByName('EXP_DAT').AsString ) <> '' then
      begin
        AddBuffer( 'DTM '+ IntToStr(CountIdx) +'      12' );
        AddBufferChildTime( '36' , qryLOCRC2H.FieldByName('EXP_DAT').AsString , '101' );
      end;
      //NAD---------------------------------------------------------------------
      CountIdx := 1;
      if (Trim( qryLOCRC2H.FieldByName('BENEFC1').AsString ) <> '') or
         (Trim( qryLOCRC2H.FieldByName('BENEFC2').AsString ) <> '') then
      begin // 수혜자
        AddBuffer( 'NAD '+ IntToStr(CountIdx) +'      13' );
        AddBufferChild('SU');
        AddBufferChild( qryLOCRC2H.FieldByName('BENEFC2').AsString ); //수혜자 상호
        AddBufferChild( qryLOCRC2H.FieldByName('BENEFC1').AsString ); //수혜자 사업자등록번호
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        Inc(CountIdx);
      end;  // 개설업체
      if (Trim( qryLOCRC2H.FieldByName('APPLIC1').AsString ) <> '') or (Trim( qryLOCRC2H.FieldByName('APPLIC2').AsString ) <> '') or
         (Trim( qryLOCRC2H.FieldByName('APPLIC3').AsString ) <> '') or (Trim( qryLOCRC2H.FieldByName('APPLIC5').AsString ) <> '') or
         (Trim( qryLOCRC2H.FieldByName('APPLIC5').AsString ) <> '') or (Trim( qryLOCRC2H.FieldByName('APPLIC6').AsString ) <> '') or (Trim( qryLOCRC2H.FieldByName('APPLIC7').AsString ) <> '') then
      begin
        AddBuffer( 'NAD '+ IntToStr(CountIdx) +'      13' );
        AddBufferChild('AP');
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC7').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC1').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC2').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC3').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC4').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC5').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('APPLIC6').AsString );
      end;
      //MOA---------------------------------------------------------------------
      CountIdx := 1;
      if ( Trim(qryLOCRC2H.FieldByName('RCT_AMT1').AsString) <> '' ) and ( qryLOCRC2H.FieldByName('RCT_AMT1').AsCurrency <> 0 ) then
      begin // 인수금액 외화
        AddBuffer( 'MOA '+ IntToStr(CountIdx) +'      14' );
        AddBufferChild('2AD');
        AddBufferChild( CurrToStr(qryLOCRC2H.FieldByName('RCT_AMT1').AsCurrency) );
        AddBufferChild( qryLOCRC2H.FieldByName('RCT_AMT1C').AsString );
        Inc(CountIdx);
      end;
      if ( Trim(qryLOCRC2H.FieldByName('RCT_AMT2').AsString) <> '' ) and ( qryLOCRC2H.FieldByName('RCT_AMT2').AsCurrency <> 0 ) then
      begin //인수금액 원화
        AddBuffer( 'MOA '+ IntToStr(CountIdx) +'      14' );
        AddBufferChild('2AE');
        AddBufferChild( CurrToStr(qryLOCRC2H.FieldByName('RCT_AMT2').AsCurrency) );
        AddBufferChild( qryLOCRC2H.FieldByName('RCT_AMT2C').AsString );
      end;
      //CUX---------------------------------------------------------------------
      if ( Trim(qryLOCRC2H.FieldByName('RATE').AsString) <> '' ) and (qryLOCRC2H.FieldByName('RATE').AsCurrency <> 0 ) then
      begin //환율
        AddBuffer('CUX 1      15');
        AddBufferChild( CurrToStr(qryLOCRC2H.FieldByName('RATE').AsCurrency) );
      end;
      //FTX---------------------------------------------------------------------
      StringFTX.Text := qryLOCRC2H.FieldByName('REMARK1').AsString;  // 기타조건
      if Trim(StringFTX.Text) <> '' then
      begin
        CountIdx := 1;
        //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
        AddBuffer('FTX '+ IntToStr(CountIdx)  +'      16');
        AddBufferChild('ABS');
        for nLoop := 1 to StringFTX.Count do
        begin
          AddBufferChild(StringFTX.Strings[nLoop-1]);
        end;
        if StringFTX.Count < 5 then
        begin
          for nLoop := 1 to (5-(StringFTX.Count)) do
          begin
            AddBufferChild('');
          end;
        end;
      end;
      //LIN---------------------------------------------------------------------
      //부분제어(부분식별자로 전자문서내 머리부문과 상세부문을 분리)
      If qryLOCRC2D.RecordCount > 0 then
      begin
        //상세내역(품명) 갯수만큼 반복
        DetailCount := 1;
        qryLOCRC2D.First;
        while not qryLOCRC2D.Eof do
        begin
          //LIN-------------------------------------------------------------------
          //상품 및 규격기재시 순번을 기재하는 항목
          AddBuffer('LIN '+ IntToStr(DetailCount) +'      17');
          AddBufferChild( qryLOCRC2D.FieldByName('SEQ').AsString );
          Inc(DetailCount);
          //PIA-------------------------------------------------------------------
          if Trim( qryLOCRC2D.FieldByName('HS_NO').AsString ) <> '' then
          begin //HS부호 또는 상품의 부호
            AddBuffer( 'PIA 1      20' );
            AddBufferChild('1');
            AddBufferChild( qryLOCRC2D.FieldByName('HS_NO').AsString );
          end;
          //IMD-------------------------------------------------------------------
          //해당 구매물품의 품명
          CountIdx := 1;
          StringIMD.Clear;
          StringIMD.Text := ( qryLOCRC2D.FieldByName('NAME1').AsString );
          if Trim(StringIMD.Text) <> '' then
          begin
            for nLoop := 1 to StringIMD.Count do
            begin
//              IF nLoop > 4 Then Break;
              AddBuffer('IMD '+ IntToStr(nLoop) +'      21');
              AddBufferChild('1AA');
              AddBufferChild(StringIMD.Strings[nLoop-1]);
            end;
          end;
          //FTX-------------------------------------------------------------------
          //상품내역의 규격
          countIdx := 1;
          StringFTX.Clear;
          StringFTX.Text := qryLOCRC2D.FieldByName('SIZE1').AsString;
          if Trim(StringFTX.Text) <> '' then
          begin
            //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
            AddBuffer('FTX '+ IntToStr(CountIdx)  +'      22');
            AddBufferChild('AAA');
            for nLoop := 1 to StringFTX.Count do
            begin
              // 5의배수 라인 출력후 다음 라인에 AddBuffer,AddBufferChild출력
              if ((nLoop mod 5) = 1) and (nLoop > 5) then
              begin
                Inc(CountIdx);
                AddBuffer('FTX '+ IntToStr(CountIdx)  +'      22');
                AddBufferChild('AAA');
              end;

              AddBufferChild(StringFTX.Strings[nLoop-1]);
            end;
            // StringFTX가 5라인 이상 이고 5,10,15,20...75,80째가 아닐경우에만 버퍼추가
            if (StringFTX.Count> 5) and ((5-(StringFTX.Count mod 5)) <> 5) then
            begin
              // 빈공간 출력갯수 = 5 - (stringLoop의 라인수 mod 5)
              for nLoop := 1 to (5-(StringFTX.Count mod 5)) do
              begin
                AddBufferChild('');
              end;
            end
            else if (StringFTX.Count < 5) then
            begin
              for nLoop := 1 to (5-(StringFTX.Count)) do
              begin
                AddBufferChild('');
              end;
            end;
          end;
          //QTY-------------------------------------------------------------------
          countIdx := 1;
          if (Trim(qryLOCRC2D.FieldByName('QTY').AsString) <> '') and (qryLOCRC2D.FieldByName('QTY').AsCurrency <> 0) then
          begin // 구매물품의 수량
            AddBuffer('QTY '+ IntToStr(CountIdx)  +'      23');
            AddBufferChild('1');
            AddBufferChild( CurrToStr(qryLOCRC2D.FieldByName('QTY').AsCurrency) );
            AddBufferChild( qryLOCRC2D.FieldByName('QTY_G').AsString );
            Inc(countIdx);
          end;
          if (Trim(qryLOCRC2D.FieldByName('STQTY').AsString) <> '') and (qryLOCRC2D.FieldByName('STQTY').AsCurrency <> 0 ) then
          begin // 구매물품의 수량소계
            AddBuffer('QTY '+ IntToStr(CountIdx)  +'      23');
            AddBufferChild('3');
            AddBufferChild( CurrToStr(qryLOCRC2D.FieldByName('STQTY').AsCurrency) );
            AddBufferChild( qryLOCRC2D.FieldByName('STQTY_G').AsString );
          end;
          //PRI-------------------------------------------------------------------
          if ((Trim (qryLOCRC2D.FieldByName('PRICE').AsString ) <> '' ) and (qryLOCRC2D.FieldByName('PRICE').AsCurrency <> 0)) or
             ((Trim( qryLOCRC2D.FieldByName('QTYG').AsString ) <> '' ) and (qryLOCRC2D.FieldByName('QTYG').AsCurrency <> 0)) then
          begin // 구매물품의 단가 , 단가기준수량
            AddBuffer('PRI 1      24');
            AddBufferChild('CAL');
            AddBufferChild( CurrToStr( qryLOCRC2D.FieldByName('PRICE').AsCurrency ) );
            AddBufferChild('PE');
            AddBufferChild('CUP');
            AddBufferChild( CurrToStr( qryLOCRC2D.FieldByName('QTYG').AsCurrency ) );
            AddBufferChild( qryLOCRC2D.FieldByName('QTYG_G').AsString );
          end;
          //MOA-------------------------------------------------------------------
          countIdx := 1;
          if (Trim (qryLOCRC2D.FieldByName('AMT').AsString ) <> '' ) and (qryLOCRC2D.FieldByName('AMT').AsCurrency <> 0) then
          begin // 금액
            AddBuffer('MOA '+ IntToStr(CountIdx)  +'      25');
            AddBufferChild('203');
            AddBufferChild( CurrToStr(qryLOCRC2D.FieldByName('AMT').AsCurrency) );
            AddBufferChild( qryLOCRC2D.FieldByName('AMT_G').AsString );
            Inc(countIdx);
          end;
          if (Trim (qryLOCRC2D.FieldByName('STAMT').AsString ) <> '' ) and (qryLOCRC2D.FieldByName('STAMT').AsCurrency <> 0) then
          begin // 금액소계
            AddBuffer('MOA '+ IntToStr(CountIdx)  +'      25');
            AddBufferChild('17');
            AddBufferChild( CurrToStr(qryLOCRC2D.FieldByName('STAMT').AsCurrency) );
            AddBufferChild( qryLOCRC2D.FieldByName('STAMT_G').AsString );
          end;
          qryLOCRC2D.Next;
        end;//While End
        //UNS---------------------------------------------------------------------
        AddBuffer('UNS 1        ');
        AddBufferChild('S');
      end;// If End
      //CNT---------------------------------------------------------------------
      if (Trim( qryLOCRC2H.FieldByName('TQTY').AsString ) <> '') and ( qryLOCRC2H.FieldByName('TQTY').AsCurrency <>0 ) then
      begin // 총합계
        AddBuffer('CNT 1      18');
        AddBufferChild('1AA');
        AddBufferChild( CurrToStr(qryLOCRC2H.FieldByName('TQTY').AsCurrency) );
        AddBufferChild( qryLOCRC2H.FieldByName('TQTY_G').AsString );
      end;
      //MOA---------------------------------------------------------------------
      if (Trim( qryLOCRC2H.FieldByName('TAMT').AsString ) <> '') and ( qryLOCRC2H.FieldByName('TAMT').AsCurrency <>0 ) then
      begin // 총금액
        AddBuffer('MOA 1      19');
        AddBufferChild('128');
        AddBufferChild( CurrToStr(qryLOCRC2H.FieldByName('TAMT').AsCurrency ) );
        AddBufferChild( qryLOCRC2H.FieldByName('TAMT_G').AsString );
      end;
      //RFF---------------------------------------------------------------------
      if Trim( qryLOCRC2H.FieldByName('LOC_NO').AsString ) <> '' then
      begin
        AddBuffer('RFF 1      1A');
        AddBufferChild('LC');
        AddBufferChild( qryLOCRC2H.FieldByName('LOC_NO').AsString );
      end;
      //FII---------------------------------------------------------------------
      if (Trim( qryLOCRC2H.FieldByName('AP_BANK').AsString) <> '') or (Trim( qryLOCRC2H.FieldByName('AP_BANK1').AsString ) <> '') then
      begin //개설은행 및 전자서명
        AddBuffer('FII 1      26');
        AddBufferChild('AZ');
        AddBufferChild( qryLOCRC2H.FieldByName('AP_BANK').AsString );
        AddBufferChild('25');
        AddBufferChild('BOK');
        AddBufferChild( qryLOCRC2H.FieldByName('AP_NAME').AsString );
        AddBufferChild( qryLOCRC2H.FieldByName('AP_BANK1').AsString + qryLOCRC2H.FieldByName('AP_BANK2').AsString  );
        AddBufferChild( qryLOCRC2H.FieldByName('AP_BANK3').AsString + qryLOCRC2H.FieldByName('AP_BANK4').AsString  );
      end;
      //MOA---------------------------------------------------------------------
      countIdx := 1;
      if (Trim( qryLOCRC2H.FieldByName('LOC1AMT').AsString ) <> '') and ( qryLOCRC2H.FieldByName('LOC1AMT').AsCurrency <>0 ) then
      begin //내국신용장금액(외화)
        AddBuffer('MOA '+ IntToStr(CountIdx)  +'      27');
        AddBufferChild('2AD');
        AddBufferChild( CurrToStr( qryLOCRC2H.FieldByName('LOC1AMT').AsCurrency ) );
        AddBufferChild( qryLOCRC2H.FieldByName('LOC1AMTC').AsString );
        Inc(countIdx);
      end;
      if (Trim( qryLOCRC2H.FieldByName('LOC2AMT').AsString ) <> '') and ( qryLOCRC2H.FieldByName('LOC2AMT').AsCurrency <>0 ) then
      begin //내국신용장금액(원화)
        AddBuffer('MOA '+ IntToStr(CountIdx)  +'      27');
        AddBufferChild('2AE');
        AddBufferChild( CurrToStr( qryLOCRC2H.FieldByName('LOC2AMT').AsCurrency ) );
        AddBufferChild( qryLOCRC2H.FieldByName('LOC2AMTC').AsString );
      end;
      //CUX---------------------------------------------------------------------
      if (Trim( qryLOCRC2H.FieldByName('EX_RATE').AsString ) <> '') and ( qryLOCRC2H.FieldByName('EX_RATE').AsCurrency <>0 ) then
      begin
        AddBuffer('CUX 1      28');
        AddBufferChild( CurrToStr( qryLOCRC2H.FieldByName('EX_RATE').AsCurrency ) );
      end;
      //DTM---------------------------------------------------------------------
      countIdx := 1;
      if Trim( qryLOCRC2H.FieldByName('LOADDATE').AsString ) <> '' then
      begin
        AddBuffer('DTM '+ IntToStr(CountIdx)  +'      29');
        AddBufferChildTime('2' , qryLOCRC2H.FieldByName('LOADDATE').AsString , '101' );
        Inc(countIdx);
      end;
      if Trim( qryLOCRC2H.FieldByName('EXPDATE').AsString ) <> '' then
      begin
        AddBuffer('DTM '+ IntToStr(CountIdx)  +'      29');
        AddBufferChildTime('123' , qryLOCRC2H.FieldByName('EXPDATE').AsString , '101' );
      end;
      //FTX---------------------------------------------------------------------
      StringFTX.Text := qryLOCRC2H.FieldByName('LOC_REM1').AsString;  // 참고사항
      if Trim(StringFTX.Text) <> '' then
      begin
        CountIdx := 1;
        //처음  AddBuffer,AddBufferChild 출력후 StringFTX.Text 출력시작
        AddBuffer('FTX '+ IntToStr(CountIdx)  +'      2A');
        AddBufferChild('ACB');
        for nLoop := 1 to StringFTX.Count do
        begin
          AddBufferChild(StringFTX.Strings[nLoop-1]);
        end;
        if StringFTX.Count < 5 then
        begin
          for nLoop := 1 to (5-(StringFTX.Count)) do
          begin
            AddBufferChild('');
          end;
        end;
      end;
      //세금계산서 -------------------------------------------------------------
      if qryLOCRC2TAX.RecordCount > 0 then
      begin
        //세금계산서 레코드만큼 반복
        TaxCount := 1;
        qryLOCRC2TAX.First;
        while not qryLOCRC2TAX.Eof do
        begin
          //DOC-----------------------------------------------------------------
//          if Trim(qryLOCRC2TAX.FieldByName('BILL_NO').AsString) <> '' then
//          begin //세금계산서 번호
          AddBuffer('DOC '+ IntToStr(TaxCount)  +'      1B');
          AddBufferChild('2AJ');
          AddBufferChild( AnsiReplaceText( qryLOCRC2TAX.FieldByName('BILL_NO').AsString ,'-',''));
          Inc(TaxCount);
//          end;
          //MOA-----------------------------------------------------------------
            countIdx := 1;
//          if (Trim( qryLOCRC2TAX.FieldByName('BILL_AMOUNT').AsString ) <> '') and ( qryLOCRC2TAX.FieldByName('BILL_AMOUNT').AsCurrency <>0 ) then
//          begin //세금계산서 공급가액
          AddBuffer('MOA '+ IntToStr(countIdx)  +'      2B');
          AddBufferChild('79');
          AddBufferChild( CurrToStr(  qryLOCRC2TAX.FieldByName('BILL_AMOUNT').AsCurrency ) );
          AddBufferChild( qryLOCRC2TAX.FieldByName('BILL_AMOUNT_UNIT').AsString );
          Inc(countIdx);
//          end;
//          if (Trim( qryLOCRC2TAX.FieldByName('TAX_AMOUNT').AsString ) <> '') and ( qryLOCRC2TAX.FieldByName('TAX_AMOUNT').AsCurrency <>0 ) then
//          begin //세금계산서 세액
          AddBuffer('MOA '+ IntToStr(countIdx)  +'      2B');
          AddBufferChild('176');
          AddBufferChild( CurrToStr(  qryLOCRC2TAX.FieldByName('TAX_AMOUNT').AsCurrency ) );
          AddBufferChild( qryLOCRC2TAX.FieldByName('TAX_AMOUNT_UNIT').AsString );
//          end;
          //DTM-----------------------------------------------------------------
//          if Trim( qryLOCRC2TAX.FieldByName('BILL_DATE').AsString ) <> '' then
//          begin
          AddBuffer('DTM 1      2C');
          AddBufferChildTime('2AA' , qryLOCRC2TAX.FieldByName('BILL_DATE').AsString , '101');
//          end;

          qryLOCRC2TAX.Next;
        end;  // while end
      end;  // end if

      AddBuffer('UNT',False);
      StringBuffer.Text := Buffer;
      Result := Buffer;
    end; // with문
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
    StringIMD.Free;
    DMDocumentQry.CLOSE_LOCRC2;
  end;
end;

function APPSPC(MAINT_NO,RecvCode : STRING):String;
var
  RECEIVER,TXTPATH,FILENAME  : string;
  StringBuffer               : TStringList;
  StringFTX                  : TStringList;
  i , nLoop , DetailCount ,
  countIdx                    : Integer;
  DocCount : TAPPSPCDOC;
begin
  StringBuffer := TStringList.Create;
  StringFTX := TStringList.Create;
  try
    DMDocumentQry.OPEN_APPSPC(MAINT_NO);
//    TXTPATH := 'C:\KOMSINGLE TEST\';
    TXTPATH := 'C:\WINMATE\FLATOUT\';
    ForceDirectories(TXTPATH);

    FILENAME := 'APPSPC' + MAINT_NO + '.UNH';

    RECEIVER := RecvCode;

    //HEADER--------------------------------------------------------------------
    ClearBuffer;
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( Format('%-17s',[RECEIVER])+' ' , False);
    AddBuffer( 'APPSPC ' , False);
    AddBuffer( Format('%-3s',['1'])+' ' , False);
    AddBuffer( Format('%-3s',['911'])+' ' , False);
    AddBuffer( Format('%-6s',['APPSPC']) , False);
    AddBuffer( MAINT_NO+NEWLINE+'UNH');

    with DMDocumentQry do
    begin
      //BGM---------------------------------------------------------------------
      AddBuffer('BGM 1');
      AddBufferChild( qryAPPSPCH.FieldByName('BGM_GUBUN').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( qryAPPSPCH.FieldByName('TOTDOC_NO').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('MESSAGE1').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('MESSAGE2').AsString );
      //FII---------------------------------------------------------------------
      AddBuffer('FII 1'); //추심매입은행
      AddBufferChild('OM');
      AddBufferChild( qryAPPSPCH.FieldByName('CP_ACCOUNTNO').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_NAME1').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_NAME2').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_CURR').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_BANK').AsString );
      AddBufferChild('25');
      AddBufferChild('BOK');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( qryAPPSPCH.FieldByName('CP_BANKNAME').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_BANKBU').AsString );
      AddBufferChild('');
      //FTX---------------------------------------------------------------------
      countIdx := 1;
      if Trim(qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO1').AsString) <> '' then
      begin // 추가계좌1번
        AddBuffer('FTX '+IntToStr(countIdx));
        AddBufferChild('ACB');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO1').AsString );
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_NAME1').AsString );
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_CURR1').AsString );
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        Inc(countIdx);
      end;
      if Trim(qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO2').AsString) <> '' then
      begin // 추가계좌2번
        AddBuffer('FTX '+IntToStr(countIdx));
        AddBufferChild('ACB');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO2').AsString );
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_NAME2').AsString );
        AddBufferChild( qryAPPSPCH.FieldByName('CP_ADD_CURR2').AsString );
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
      end;
      //DTM---------------------------------------------------------------------
      AddBuffer('DTM 1'); //신청일자
      AddBufferChildTime('2AA', qryAPPSPCH.FieldByName('APP_DATE').AsString , '102'  );
      //RFF---------------------------------------------------------------------
      AddBuffer('RFF 1'); //신청번호
      if Trim(qryAPPSPCH.FieldByName('BGM_GUBUN').AsString) = '2BK' then
      begin //추심일경우
        AddBufferChild('2BB');
      end
      else if Trim(qryAPPSPCH.FieldByName('BGM_GUBUN').AsString) = '2BJ' then
      begin //매입일경우
        AddBufferChild('2BA');
      end;
      AddBufferChild( qryAPPSPCH.FieldByName('CP_NO').AsString );
      AddBufferChild('');
      //NAD---------------------------------------------------------------------
      AddBuffer('NAD 1'); //신청인
      AddBufferChild('4AA');
      AddBufferChild( qryAPPSPCH.FieldByName('APP_SAUPNO').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( qryAPPSPCH.FieldByName('APP_SNAME').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('APP_NAME').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('APP_ELEC').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('APP_ADDR1').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('APP_ADDR2').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('APP_ADDR3').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      //NAD---------------------------------------------------------------------
      AddBuffer('NAD 1'); //구매자
      AddBufferChild('DF');
      AddBufferChild( qryAPPSPCH.FieldByName('DF_SAUPNO').AsString );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( qryAPPSPCH.FieldByName('DF_EMAIL1').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('DF_EMAIL2').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('DF_NAME1').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('DF_NAME2').AsString);
      AddBufferChild( qryAPPSPCH.FieldByName('DF_NAME3').AsString);
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      //MOA---------------------------------------------------------------------
      AddBuffer('MOA 2'); //추심금액 외화
      AddBufferChild('2AD');
      AddBufferChild( qryAPPSPCH.FieldByName('CP_AMTU').AsString );
      AddBufferChild( qryAPPSPCH.FieldByName('CP_AMTC').AsString );
      AddBufferChild('');
      AddBufferChild('');

      AddBuffer('MOA 3');  //추심금액 원화
      AddBufferChild('2AE');
      AddBufferChild( qryAPPSPCH.FieldByName('CP_AMT').AsString );
      AddBufferChild('KRW');
      AddBufferChild('');
      AddBufferChild('');
      //CUX---------------------------------------------------------------------
      AddBuffer('CUX 1'); //원화환산 추심금액 산출에 적용된 적용환율
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild( qryAPPSPCH.FieldByName('CP_CUX').AsString );
      AddBufferChild('');
      //BUS---------------------------------------------------------------------
      DocCount := getAPPSPCCount(MAINT_NO); // 물품수령증명서 , 세금계산서 , 상업송장의 건수를 조회하는 함수
      AddBuffer('BUS 1');
      AddBufferChild('2AH');
      AddBufferChild('ZZZ');
      AddBufferChild('');
      AddBufferChild('');
      //ShowMessage( IntToStr(DocCount.DOC_2AH));
      AddBufferChild( DocCount.DOC_2AH );
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      //BUS---------------------------------------------------------------------
      AddBuffer('BUS 2');
      AddBufferChild('2AI');
      AddBufferChild('ZZZ');
      AddBufferChild('');
      AddBufferChild('');
      if DocCount.DOC_2AJ >= 1 then
        AddBufferChild( DocCount.DOC_2AJ )
      else if DocCount.DOC_1BW >= 1 then
        AddBufferChild( DocCount.DOC_1BW);
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');
      AddBufferChild('');

      qryAPPSPCD1.First;
      while not qryAPPSPCD1.Eof do
      begin
        //DOC---------------------------------------------------------------------
        //물품수령증명서, 세금계산서 ,상업송장 KEYY 값
        AddBuffer('DOC 1');
        if qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString = '2AH' then
        begin
          AddBufferChild('2AH');
        end
        else if qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString = '2AJ' then
        begin
          AddBufferChild('2AJ');
        end
        else if qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString = '1BW' then
        begin
          AddBufferChild('1BW');
        end
        else if qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString = '2AP' then
        begin
          AddBufferChild('2AP');
        end;
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild( qryAPPSPCD1.FieldByName('DOC_NO').AsString );
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        AddBufferChild('');
        //RFF-------------------------------------------------------------------
        if Trim(qryAPPSPCD1.FieldByName('LR_NO').AsString) <> '' then
        begin //내국신용장 및 물품수령증명서 상의 내국신용장번호
          AddBuffer('RFF 1');
          AddBufferChild('LC');
          AddBufferChild( qryAPPSPCD1.FieldByName('LR_NO').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('LR_NO2').AsString) <> '' then
        begin //물품수령증명서 발급번호
          AddBuffer('RFF 1');
          AddBufferChild('CRG');
          AddBufferChild( qryAPPSPCD1.FieldByName('LR_NO2').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('BSN_HSCODE').AsString) <> '' then
        begin //대표공급물품 HS 코드
          AddBuffer('RFF 1');
          AddBufferChild('HS');
          AddBufferChild( qryAPPSPCD1.FieldByName('BSN_HSCODE').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('VB_RENO').AsString) <> '' then
        begin //세금계산서 권번호
          AddBuffer('RFF 2');
          AddBufferChild('RE');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_RENO').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('VB_SENO').AsString) <> '' then
        begin //세금계산서 호번호
          AddBuffer('RFF 1');
          AddBufferChild('SE');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_SENO').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('VB_FSNO').AsString) <> '' then
        begin //세금계산서 일련번호
          AddBuffer('RFF 1');
          AddBufferChild('FS');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_FSNO').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('VB_MAINTNO').AsString) <> '' then
        begin //세금계산서 관리번호
          AddBuffer('RFF 1');
          AddBufferChild('ACE');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_MAINTNO').AsString );
          AddBufferChild('');
        end;
        if Trim(qryAPPSPCD1.FieldByName('VB_DMNO').AsString) <> '' then
        begin //세금계산서 참조번호
          AddBuffer('RFF 1');
          AddBufferChild('DM');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_DMNO').AsString );
          AddBufferChild('');
        end;
        //DTM-------------------------------------------------------------------
        countIdx := 1;
        if Trim(qryAPPSPCD1.FieldByName('ISS_DATE').AsString) <> '' then
        begin // 내국신용장 개설일자, 물품수령증명서 발급일자, 세금계산서 작성일, 상업송장 발급일
          AddBuffer('DTM ' + IntToStr(countIdx));
          AddBufferChild('182');
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_DATE').AsString );
          AddBufferChild('102');
          Inc(countIdx);
        end;
        if Trim(qryAPPSPCD1.FieldByName('GET_DATE').AsString) <> '' then
        begin // 물품수령증명서 상의 내국신용장 물품인도기일
          AddBuffer('DTM ' + IntToStr(countIdx));
          AddBufferChild('2');
          AddBufferChild( qryAPPSPCD1.FieldByName('GET_DATE').AsString );
          AddBufferChild('102');
          Inc(countIdx);
        end;
        if Trim(qryAPPSPCD1.FieldByName('EXP_DATE').AsString) <> '' then
        begin // 물품수령증명서 상의 내국신용장 유효기일
          AddBuffer('DTM ' + IntToStr(countIdx));
          AddBufferChild('123');
          AddBufferChild( qryAPPSPCD1.FieldByName('EXP_DATE').AsString );
          AddBufferChild('102');
          Inc(countIdx);
        end;
        if Trim(qryAPPSPCD1.FieldByName('ACC_DATE').AsString) <> '' then
        begin // 물품수령증명서 인수일자
          AddBuffer('DTM ' + IntToStr(countIdx));
          AddBufferChild('50');
          AddBufferChild( qryAPPSPCD1.FieldByName('ACC_DATE').AsString );
          AddBufferChild('102');
          Inc(countIdx);
        end;
        if Trim(qryAPPSPCD1.FieldByName('VAL_DATE').AsString) <> '' then
        begin // 물품수령증명서 유효기일
          AddBuffer('DTM ' + IntToStr(countIdx));
          AddBufferChild('36');
          AddBufferChild( qryAPPSPCD1.FieldByName('VAL_DATE').AsString );
          AddBufferChild('102');
        end;
        //NAD-------------------------------------------------------------------
        //물품공급자(수출자)의 사업자등록번호, 상호, 대표자성명, 전자서명, 주소 등을 기재하는 전송항목
        countIdx := 1;
        if Trim(qryAPPSPCD1.FieldByName('SE_SNAME').AsString) <> '' then
        begin
          AddBuffer('NAD ' + IntToStr(countIdx));
          //물품수령증명서 물품공급자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then
            AddBufferChild('SU');
          //세금계산서상의 공급자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ' then
            AddBufferChild('SE');
          //상업송장의 수출자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '1BW' then
            AddBufferChild('EX');
          AddBufferChild( qryAPPSPCD1.FieldByName('SE_SAUPNO').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_SNAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_NAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_ELEC').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_ADDR1').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_ADDR2').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('SE_ADDR3').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //물품수령인/공급받는자(수입자)의 사업자등록번호, 상호, 대표자명, 주소등을 기재하는 전송항목
        if Trim(qryAPPSPCD1.FieldByName('BY_SNAME').AsString) <> '' then
        begin
          AddBuffer('NAD ' + IntToStr(countIdx));
          //물품수령증명서 물품공급자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then
            AddBufferChild('AP');
          //세금계산서상의 공급자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ' then
            AddBufferChild('BY');
          //상업송장의 수출자
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '1BW' then
            AddBufferChild('IM');
          AddBufferChild( qryAPPSPCD1.FieldByName('BY_SAUPNO').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_SNAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_NAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_ELEC').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_ADDR1').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_ADDR2').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('BY_ADDR3').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //수탁자
        if Trim(qryAPPSPCD1.FieldByName('AG_SNAME').AsString) <> '' then
        begin
          AddBuffer('NAD ' + IntToStr(countIdx));
          AddBufferChild('AG');
          AddBufferChild( qryAPPSPCD1.FieldByName('AG_SAUPNO').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_SNAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_NAME').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_ELEC').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_ADDR1').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_ADDR2').AsString );
          AddBufferChild( qryAPPSPCD1.FieldbyName('AG_ADDR3').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
        end;
        //IMD-------------------------------------------------------------------
        //공급자 업태
        countIdx := 1;
        for i := 1 to 3 do
        begin
          if qryAPPSPCD1.FieldByName('SG_UPTAI1_'+IntToStr(i)).AsString <> '' then
          begin
            AddBuffer('IMD ' + IntToStr(countIdx));
            AddBufferChild('');
            AddBufferChild('');
            AddBufferChild('SG');
            AddBufferChild('SU');
            AddBufferChild('');
            AddBufferChild( qryAPPSPCD1.FieldByName('SG_UPTAI1_'+IntToStr(i)).AsString );
            AddBufferChild('');
            Inc(countIdx);
          end;
        end;
        //공급자의 종목
        countIdx := 1;
        for i := 1 to 3 do
        begin
          if qryAPPSPCD1.FieldByName('HN_ITEM1_'+IntToStr(i)).AsString <> '' then
          begin
            AddBuffer('IMD ' + IntToStr(countIdx));
            AddBufferChild('');
            AddBufferChild('');
            AddBufferChild('HN');
            AddBufferChild('SU');
            AddBufferChild('');
            AddBufferChild( qryAPPSPCD1.FieldByName('HN_ITEM1_'+IntToStr(i)).AsString );
            AddBufferChild('');
            Inc(countIdx);
          end;
        end;
        //공급받는자의 업태
        countIdx := 1;
        for i := 1 to 3 do
        begin
          if qryAPPSPCD1.FieldByName('SG_UPTAI2_'+IntToStr(i)).AsString <> '' then
          begin
            AddBuffer('IMD ' + IntToStr(countIdx));
            AddBufferChild('');
            AddBufferChild('');
            AddBufferChild('SG');
            AddBufferChild('BY');
            AddBufferChild('');
            AddBufferChild( qryAPPSPCD1.FieldByName('SG_UPTAI2_'+IntToStr(i)).AsString );
            AddBufferChild('');
            Inc(countIdx);
          end;
        end;
        //공급받는자의 종목
        countIdx := 1;
        for i := 1 to 3 do
        begin
          if qryAPPSPCD1.FieldByName('HN_ITEM2_'+IntToStr(i)).AsString <> '' then
          begin
            AddBuffer('IMD ' + IntToStr(countIdx));
            AddBufferChild('');
            AddBufferChild('');
            AddBufferChild('HN');
            AddBufferChild('BY');
            AddBufferChild('');
            AddBufferChild( qryAPPSPCD1.FieldByName('HN_ITEM2_'+IntToStr(i)).AsString );
            AddBufferChild('');
            Inc(countIdx);
          end;
        end;
        //CUX-------------------------------------------------------------------
        //물품수령증명서 매매기준율
        if (Trim(qryAPPSPCD1.FieldByName('EXCH').AsString) <> '') and (qryAPPSPCD1.FieldByName('EXCH').AsCurrency <> 0) then
        begin
          AddBuffer('CUX 1');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBuffer('');
          AddBufferChild( qryAPPSPCD1.FieldByName('EXCH').AsCurrency );
          AddBuffer('');
        end;
        //BUS-------------------------------------------------------------------
        //내국신용장 종류
        if Trim( qryAPPSPCD1.FieldByName('LA_TYPE').AsString ) <> '' then
        begin
          AddBuffer('BUS 1');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild( qryAPPSPCD1.FieldByName('LA_TYPE').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
        end;
        //MOA-------------------------------------------------------------------
        countIdx := 1;
        //물품수령증명서 상의 내국신용장 개설금액(외화)
        if (Trim( qryAPPSPCD1.FieldByName('ISS_AMTU').AsString ) <> '') and (qryAPPSPCD1.FieldByName('ISS_AMTU').AsCurrency <> 0) then
        begin
          AddBuffer('MOA ' + IntToStr(countIdx));
          AddBufferChild('2BD');
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_AMTU').AsCurrency );
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_AMTC').AsString );
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //물품수령증명서 상의 내국신용장 개설금액(원화)
        if (Trim( qryAPPSPCD1.FieldByName('ISS_AMT').AsString ) <> '') and (qryAPPSPCD1.FieldByName('ISS_AMT').AsCurrency <> 0) then
        begin
          AddBuffer('MOA ' + IntToStr(countIdx));
          AddBufferChild('2BE');
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_AMT').AsCurrency );
          AddBufferChild( 'KRW' );
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //물품수령증명서 인수금액(외화), 세금계산서 세액(원화)
        if (Trim( qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsString ) <> '') and (qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsCurrency <> 0) then
        begin
          AddBuffer('MOA ' + IntToStr(countIdx));
          AddBufferChild('2AD');
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsCurrency );

          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then //물품수령명서
            AddBufferChild( qryAPPSPCD1.FieldByName('ISS_EXPAMTC').AsString  );

          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ' then //세금계산서
            AddBufferChild('KRW');

          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //물품수령증명서 인수금액(원화), 세금계산서 공급가액  입력
        if (Trim( qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsString ) <> '') and (qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsCurrency <> 0) then
        begin
          AddBuffer('MOA ' + IntToStr(countIdx));
          AddBufferChild('2AE');
          AddBufferChild( qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsCurrency );
          AddBufferChild('KRW');
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //물품수령증명서 총금액, 세금계산서 총금액(공급가액 + 세액), 상업송장 총금액
        if (Trim( qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsString ) <> '') and (qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsCurrency <> 0) then
        begin
          AddBuffer('MOA ' + IntToStr(countIdx));
          AddBufferChild('128');
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ' then //세금계산서
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsCurrency + qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsCurrency);
            AddBufferChild('KRW');
          end
          else //물품수령증명서 , 상업송장
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsCurrency );
            AddBufferChild( qryAPPSPCD1.FieldByName('ISS_TOTAMTC').AsString );
          end;
          AddBufferChild('');
          AddBufferChild('');
          Inc(countIdx);
        end;
        //CNT-------------------------------------------------------------------
        //총수량
        if (Trim( qryAPPSPCD1.FieldByName('TOTCNT').AsString ) <> '') and (qryAPPSPCD1.FieldByName('TOTCNT').AsCurrency <> 0) then
        begin
          AddBuffer('CNT 1');
          AddBufferChild('1AA');
          AddBufferChild( qryAPPSPCD1.FieldByName('TOTCNT').AsCurrency );
          AddBufferChild( qryAPPSPCD1.FieldByName('TOTCNTC').AsString );
        end;
        //PAI-------------------------------------------------------------------
        //결제방법을 CODE로 기재하는 전송항목.
        for i := 1 to 4 do
        begin
          if ((Trim(qryAPPSPCD1.FieldByName('PAI_AMT' + IntToStr(i)).AsString) <> '') and (qryAPPSPCD1.FieldByName('PAI_AMT' + IntToStr(i)).AsCurrency <> 0)) or
             ((Trim(qryAPPSPCD1.FieldByName('PAI_AMTU' + IntToStr(i)).AsString) <> '') and (qryAPPSPCD1.FieldByName('PAI_AMTU' + IntToStr(i)).AsCurrency <> 0)) then
          begin
            AddBuffer('PAI 1');
            AddBufferChild('');
            AddBufferChild('');
            case i of
              1 : AddBufferChild('10'); //현금
              2 : AddBufferChild('20'); //수표
              3 : AddBufferChild('2AA'); //어음
              4 : AddBufferChild('30'); //외상
            end;
            AddBufferChild('');
            AddBufferChild('');
            AddBufferChild('');
          end;
          //MOA-----------------------------------------------------------------
          countIdx := 1;
          //각 결제방법에 따른 해당금액(원화)를 기재하는 전송항목
          if (Trim(qryAPPSPCD1.FieldByName('PAI_AMT' + IntToStr(i)).AsString) <> '') and (qryAPPSPCD1.FieldByName('PAI_AMT' + IntToStr(i)).AsCurrency <> 0) then
          begin
            AddBuffer('MOA ' + IntToStr(countIdx));
            AddBufferChild('2AE');
            AddBufferChild( qryAPPSPCD1.FieldByName('PAI_AMT' + IntToStr(i)).AsCurrency );
            AddBufferChild('KRW');
            AddBufferChild('');
            AddBufferChild('');
            Inc(countIdx);
          end;
          //각 결제방법에 따른 해당금액(외화)를 기재하는 전송항목
          if (Trim(qryAPPSPCD1.FieldByName('PAI_AMTU' + IntToStr(i)).AsString) <> '') and (qryAPPSPCD1.FieldByName('PAI_AMTU' + IntToStr(i)).AsCurrency <> 0) then
          begin
            AddBuffer('MOA ' + IntToStr(countIdx));
            AddBufferChild('2AD');
            AddBufferChild( qryAPPSPCD1.FieldByName('PAI_AMTU' + IntToStr(i)).AsCurrency );
            AddBufferChild( qryAPPSPCD1.FieldByName('PAI_AMTC' + IntToStr(i)).AsString );
            AddBufferChild('');
            AddBufferChild('');
          end;
        end;
        //GIS-------------------------------------------------------------------
        //영수/청구 구분을 기재
        if (Trim(qryAPPSPCD1.FieldByName('VB_GUBUN').AsString) <> '') and (Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ') then
        begin
          AddBuffer('GIS 1');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_GUBUN').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
        end;
        //FII-------------------------------------------------------------------
        //물품수령증명서 상의 개설은행 , 내국신용장의 개설은행
        if (Trim(qryAPPSPCD1.FieldByName('LA_BANKNAME').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('LA_BANKNAMEP').AsString) <> '') then
        begin
          AddBuffer('FII 1');
          AddBufferChild('AZ');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');

          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANK').AsString );
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AP' then
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANKBUCODE').AsString );

          AddBufferChild('25');
          AddBufferChild('BOK');

          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_ELEC').AsString );
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AP' then
            AddBufferChild('');

          AddBufferChild('');
          AddBufferChild('');

          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AH' then
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANKNAME').AsString );
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANKBU').AsString );
          end;
          if Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AP' then
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANKNAMEP').AsString );
            AddBufferChild( qryAPPSPCD1.FieldByName('LA_BANKBUP').AsString );
          end;

          AddBuffer('');
        end;
        //SEQ-------------------------------------------------------------------
        //세금계산서의 공란수
        if (Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = '2AJ') and (Trim(qryAPPSPCD1.FieldByName('VB_DETAILNO').AsString) <> '' ) then
        begin
          AddBuffer('SEQ 1');
          AddBufferChild('');
          AddBufferChild( qryAPPSPCD1.FieldByName('VB_DETAILNO').AsString );
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
        end;
        //FTX-------------------------------------------------------------------
        //물품수령증명서상의 내국신용장 참조사항, 세금계산서의 비고(상업송장 의 항차 등 내역표기)
        if (Trim(qryAPPSPCD1.FieldByName('REMARK1_1').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK1_2').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK1_3').AsString) <> '')
            or (Trim(qryAPPSPCD1.FieldByName('REMARK1_4').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK1_5').AsString) <> '') then
        begin
          AddBuffer('FTX 1');
          AddBufferChild('ACB');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          for i := 1 to 5 do
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('REMARK1_' + IntToStr(i) ).AsString );
          end;
          AddBufferChild('');
        end;
        //물품수령증명서 상의 기타조건
        if (Trim(qryAPPSPCD1.FieldByName('REMARK2_1').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK2_2').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK2_3').AsString) <> '')
            or (Trim(qryAPPSPCD1.FieldByName('REMARK2_4').AsString) <> '') or (Trim(qryAPPSPCD1.FieldByName('REMARK2_5').AsString) <> '') then
        begin
          AddBuffer('FTX 1');
          AddBufferChild('ABS');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          AddBufferChild('');
          for i := 1 to 5 do
          begin
            AddBufferChild( qryAPPSPCD1.FieldByName('REMARK2_' + IntToStr(i) ).AsString );
          end;
          AddBufferChild('');
        end;

        if qryAPPSPCD2.RecordCount > 0 then
        begin
          qryAPPSPCD2.First;
          while not qryAPPSPCD2.Eof do
          begin
            if (Trim(qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString) = Trim(qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString)) and
               (Trim(qryAPPSPCD1.FieldByName('SEQ').AsString) = Trim(qryAPPSPCD2.FieldByName('SEQ').AsString)) then
            begin
              //LIN---------------------------------------------------------------
              //일련번호
              AddBuffer('LIN 1');
              AddBufferChild( qryAPPSPCD2.FieldByName('LINE_NO').AsString );
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              AddBufferChild('');
              //PIA---------------------------------------------------------------
              //HS부호
              if Trim(qryAPPSPCD2.FieldByName('HS_NO').AsString) <> '' then
              begin
                AddBuffer('PIA 1');
                AddBufferChild('1');
                AddBufferChild( qryAPPSPCD2.FieldByName('HS_NO').AsString );
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
              end;
              //DTM---------------------------------------------------------------
              //세금계산서 라인별 공급일자
              if (Trim(qryAPPSPCD2.FieldByName('DE_DATE').AsString) <> '') and (Trim(qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString) = '2AJ') then
              begin
                AddBuffer('DTM 1');
                AddBufferChildTime('35' , qryAPPSPCD2.FieldByName('DE_DATE').AsString , '102');
              end;
              //IMD---------------------------------------------------------------
              //품명을 기재하는 전송항목.
              countIdx := 1;
              for i := 1 to 4 do
              begin
                if qryAPPSPCD2.FieldByName('IMD_CODE' + IntToStr((i))).AsString <> '' then
                begin
                  AddBuffer('IMD' + IntToStr(countIdx));
                  AddBufferChild('');
                  AddBufferChild('');
                  AddBufferChild('1AA');
                  AddBufferChild('');
                  AddBufferChild('');
                  AddBufferChild( qryAPPSPCD2.FieldByName('IMD_CODE' + IntToStr((i))).AsString );
                  AddBufferChild('');
                  Inc(countIdx);
                end;
              end;
              //FTX---------------------------------------------------------------
              //물품수령증명서, 세금계산서, 상업송장의 규격
              countIdx := 1;
              if (Trim(qryAPPSPCD2.FieldByName('SIZE1').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE2').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE3').AsString) <> '') or
                 (Trim(qryAPPSPCD2.FieldByName('SIZE4').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE5').AsString) <> '') then
              begin
                AddBuffer('FTX ' + IntToStr(countIdx));
                AddBufferChild('AAA');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                for i := 1 to 5 do
                begin
                  AddBufferChild( qryAPPSPCD2.FieldByName('SIZE' + IntToStr(i)).AsString );
                end;
                AddBufferChild('');
                Inc(countIdx);
              end;
              if (Trim(qryAPPSPCD2.FieldByName('SIZE6').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE7').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE8').AsString) <> '') or
                 (Trim(qryAPPSPCD2.FieldByName('SIZE9').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('SIZE10').AsString) <> '') then
              begin
                AddBuffer('FTX ' + IntToStr(countIdx));
                AddBufferChild('AAA');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                for i := 6 to 10 do
                begin
                  AddBufferChild( qryAPPSPCD2.FieldByName('SIZE' + IntToStr(i)).AsString );
                end;
                AddBufferChild('');
                Inc(countIdx);
              end;
              //세금계산서  라인별 비고
              if ((Trim(qryAPPSPCD2.FieldByName('BIGO1').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('BIGO2').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('BIGO3').AsString) <> '') or
                 (Trim(qryAPPSPCD2.FieldByName('BIGO4').AsString) <> '') or (Trim(qryAPPSPCD2.FieldByName('BIGO5').AsString) <> '')) and (Trim(qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString) = '2AJ' ) then
              begin
                AddBuffer('FTX ' + IntToStr(countIdx));
                AddBufferChild('ACB');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                for i := 1 to 5 do
                begin
                  AddBufferChild( qryAPPSPCD2.FieldByName('BIGO' + IntToStr(i)).AsString );
                end;
                AddBufferChild('');
              end;
              //QTY---------------------------------------------------------------
              //물품의 수량을 기재하는 전송항목
              countIdx := 1 ;
              if (Trim(qryAPPSPCD2.FieldByName('QTY').AsString) <> '') and (qryAPPSPCD2.FieldByName('QTY').AsCurrency <> 0) then
              begin
                AddBuffer('QTY ' + IntToStr(countIdx));
                AddBufferChild('1');
                AddBufferChild(qryAPPSPCD2.FieldByName('QTY').AsCurrency);
                AddBufferChild(qryAPPSPCD2.FieldByName('QTYC').AsString);
                Inc(countIdx);
              end;
              //수량소계
              if (Trim(qryAPPSPCD2.FieldByName('TOTQTY').AsString) <> '') and (qryAPPSPCD2.FieldByName('TOTQTY').AsCurrency <> 0) then
              begin
                AddBuffer('QTY ' + IntToStr(countIdx));
                AddBufferChild('3');
                AddBufferChild(qryAPPSPCD2.FieldByName('TOTQTY').AsCurrency);
                AddBufferChild(qryAPPSPCD2.FieldByName('TOTQTYC').AsString);
              end;
              //PRI---------------------------------------------------------------
              //각 규격별 단가를 기재하는 전송항목.
              if ((Trim(qryAPPSPCD2.FieldByName('PRICE').AsString) <> '') and (qryAPPSPCD2.FieldByName('PRICE').AsCurrency <> 0)) or
                 ((Trim(qryAPPSPCD2.FieldByName('PRICE_G').AsString) <> '') and (qryAPPSPCD2.FieldByName('PRICE_G').AsCurrency <> 0)) then
              begin
                AddBuffer('PRI 1');
                AddBufferChild('CAL');
                AddBufferChild( qryAPPSPCD2.FieldByName('PRICE').AsCurrency );
                AddBufferChild('PE');
                AddBufferChild('CUP');
                AddBufferChild( qryAPPSPCD2.FieldByName('PRICE_G').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('PRICEC').AsString );
                AddBufferChild('');
              end;
              //MOA---------------------------------------------------------------
              //세금계산서 라인별 공급가액
              countIdx := 1;
              if ((Trim(qryAPPSPCD2.FieldByName('SUP_AMT').AsString) <> '') and (qryAPPSPCD2.FieldByName('SUP_AMT').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('203');
                AddBufferChild( qryAPPSPCD2.FieldByName('SUP_AMT').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('SUP_AMTC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;
              //세금계산서 공급가액소계
              if ((Trim(qryAPPSPCD2.FieldByName('SUP_TOTAMT').AsString) <> '') and (qryAPPSPCD2.FieldByName('SUP_TOTAMT').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('17');
                AddBufferChild( qryAPPSPCD2.FieldByName('SUP_TOTAMT').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('SUP_TOTAMTC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;
              //세금계산서 라인별 외화공급가액
              if (Trim(qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString) = '2AJ') and
                 ((Trim(qryAPPSPCD2.FieldByName('VB_AMT').AsString) <> '') and (qryAPPSPCD2.FieldByName('VB_AMT').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('261');
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_AMT').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_AMTC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;
              //세금계산서 외화공급가액소계
              if ((Trim(qryAPPSPCD2.FieldByName('VB_TOTAMT').AsString) <> '') and (qryAPPSPCD2.FieldByName('VB_TOTAMT').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('289');
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TOTAMT').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TOTAMTC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;

              //세금계산서 라인별 세액
              if ((Trim(qryAPPSPCD2.FieldByName('VB_TAX').AsString) <> '') and (qryAPPSPCD2.FieldByName('VB_TAX').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('124');
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TAX').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TAXC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;
              //세금계산서 세액소계
              if ((Trim(qryAPPSPCD2.FieldByName('VB_TOTTAX').AsString) <> '') and (qryAPPSPCD2.FieldByName('VB_TOTTAX').AsCurrency <> 0)) then
              begin
                AddBuffer('MOA ' + IntToStr(countIdx));
                AddBufferChild('168');
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TOTTAX').AsCurrency );
                AddBufferChild( qryAPPSPCD2.FieldByName('VB_TOTTAXC').AsString );
                AddBufferChild('');
                AddBufferChild('');
                Inc(countIdx);
              end;

              // 문서에는 있지만 기존 상역에서는 값이있어도 버퍼에 추가되지않음.
              //상업송장 라인별 외화공급가액
              //if (Trim(qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString) = '1BW') and
              //   ((Trim(qryAPPSPCD2.FieldByName('SUP_AMT').AsString) <> '') and (qryAPPSPCD2.FieldByName('SUP_AMT').AsCurrency <> 0)) then
              //begin
              //  AddBuffer('MOA ' + IntToStr(countIdx));
              //  AddBufferChild('261');
              //  AddBufferChild( qryAPPSPCD2.FieldByName('SUP_AMT').AsCurrency );
              //  AddBufferChild( qryAPPSPCD2.FieldByName('SUP_AMTC').AsString );
              //  AddBufferChild('');
              //  AddBufferChild('');
              //  Inc(countIdx);
              //end;

              //CUX---------------------------------------------------------------
              //환율
              if ((Trim(qryAPPSPCD2.FieldByName('CUX_RATE').AsString) <> '') and (qryAPPSPCD2.FieldByName('CUX_RATE').AsCurrency <> 0)) then
              begin
                AddBuffer('CUX 1');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild('');
                AddBufferChild( qryAPPSPCD2.FieldByName('CUX_RATE').AsCurrency );
                AddBufferChild('');
              end;
            end;
            qryAPPSPCD2.Next;
          end; //qryAPPSPCD2 While end; 
        end; //qryAPPSPCD2.RecordCount end;

        qryAPPSPCD1.Next;
      end; //qryAPPSPCD1 While end;

      AddBuffer('UNT',False);
      StringBuffer.Text := Buffer;
      Result := Buffer;
    end; // with문
  finally
//    StringBuffer.SaveToFile(TXTPATH + '\' + FILENAME);
    StringBuffer.Free;
    StringFTX.Free;
    DMDocumentQry.CLOSE_APPSCP;
  end;
end;


function APP700_NEW(MAINT_NO,MSEQ,RecvCode : String):String;
var
  REC_700 : TEDIFACT;
  TempList : TStringList;
  i, INNER_CNT : integer;
  TMP_MEMO : TStringList;
  TMP_CNT : integer;
  nCURSOR : Integer;
  TMP_STR : String;
begin
  Result := 'NO DATA';
  REC_700 := TEDIFACT.Create;
  TempList := TStringList.Create;
  TMP_MEMO := TStringList.Create;
  try
    with REC_700, DMDocumentQry.qryAPP700_NEW do
    begin
      DMDocumentQry.qryAPP700_NEW.Close;
      DMDocumentQry.qryAPP700_NEW.Parameters[0].Value := MAINT_NO;
      DMDocumentQry.qryAPP700_NEW.Parameters[1].Value := MSEQ;
      Open;
      IF DMDocumentQry.qryAPP700_NEW.RecordCount = 0 Then
      begin
        Exit;
      end;
//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'APP700';
      DOCNO := MAINT_NO+'_'+MSEQ;
      TempList.Add(Header);
//------------------------------------------------------------------------------
// 시작
//------------------------------------------------------------------------------
      SEGMENT := 'BGM';
      ID := '10';
      COUNT := 1;
      Items[0] := '460';
      Items[1] := MAINT_NO+'_'+MSEQ;
      Items[2] := FieldbyName('MESSAGE1').AsString;
      Items[3] := FieldbyName('MESSAGE2').AsString;
      TempList.Add(Text(4));

      SEGMENT := 'BUS';
      ID := '11';
      COUNT := 1;
      Items[0] := FieldbyName('DOC_CD').AsString;
      TempList.Add(Text(1));

      SEGMENT := 'INP';
      ID := '12';
      COUNT := 1;
      Items[0] := '1';
      Items[1] := '5';
      Items[2] := FieldByName('IN_MATHOD').AsString;
      TempList.Add(Text(3));

      SEGMENT := 'INP';
      ID := '12';
      COUNT := 2;
      Items[0] := '1';
      Items[1] := '55';
      Items[2] := FieldByName('CARRIAGE').AsString;
      TempList.Add(Text(3));

      SEGMENT := 'INP';
      ID := '12';
      COUNT := 3;
      Items[0] := '1';
      Items[1] := '4';
      Items[2] := FieldByName('CONFIRMM').AsString;
      TempList.Add(Text(3));

//------------------------------------------------------------------------------
// 수수료부담자
//------------------------------------------------------------------------------
      IF FieldByName('CHARGE').AsString <> '2AH' Then
      begin
        SEGMENT := 'ALC';
        ID := '13';
        COUNT := 1;
        Items[0] := FieldByName('CHARGE').AsString;
        items[1] := '';
        TempList.Add(Text(2));
      end
      else
      begin
        TMP_MEMO.Text := FieldByName('CHARGE_1').AsString;
        TMP_CNT := 6;
        if TMP_MEMO.Count < 6 then
          TMP_CNT := TMP_MEMO.Count;
        for i := 0 to TMP_CNT-1 do
        begin
          SEGMENT := 'ALC';
          ID := '13';
          COUNT := i+1;
          Items[0] := '2AH';
          Items[1] := TMP_MEMO.Strings[i];          
          TempList.Add(Text(2));
        end;
      end;

//------------------------------------------------------------------------------
// 개설신청일자
//------------------------------------------------------------------------------
      SEGMENT := 'DTM';
      ID := '14';
      COUNT := 1;
      Items[0] := '2AA';
      Items[1] := RightStr( FieldByName('APP_DATE').AsString ,6);
      Items[2] := '101';
      TempList.Add(Text(3));

//------------------------------------------------------------------------------
// 선적/환적
//------------------------------------------------------------------------------
      SEGMENT := 'TSR';
      ID := '15';
      COUNT := 1;
      Items[0] := FieldByName('TSHIP').AsString;
      Items[1] := FieldByName('PSHIP').AsString;
      TempList.Add(Text(2));
//------------------------------------------------------------------------------
// 지급지시
//------------------------------------------------------------------------------
      IF Trim(FieldByName('AD_PAY').AsString) <> '' Then
      begin
        SEGMENT := 'PAI';
        ID := '16';
        COUNT := 1;
        Items[0] := Trim(FieldByName('AD_PAY').AsString);
        TempList.Add(Text(1));
      end;
//------------------------------------------------------------------------------
// 지급조건
//------------------------------------------------------------------------------
      IF Trim(FieldByName('DRAFT1').AsString) <> '' Then
      begin
        nCURSOR := 1;
        for i := 0 to 2 do
        begin
          IF Trim(FieldByName('DRAFT'+IntToStr(i+1)).AsString) <> '' Then
          begin
            SEGMENT := 'PAT';
            ID := '17';
            COUNT := nCURSOR;
            Items[0] := '2AB';
            Items[1] := '2AO';
            Items[2] := Trim(FieldByName('DRAFT'+IntToStr(i+1)).AsString);
            TempList.Add(Text(3));
            Inc(nCURSOR);
          end;
        end;
      end
      else
      IF Trim(FieldByName('MIX_PAY1').AsString) <> '' Then
      begin
        nCURSOR := 1;
        for i := 0 to 3 do
        begin
          IF Trim(FieldByName('MIX_PAY'+IntToStr(i+1)).AsString) <> '' Then
          begin
            SEGMENT := 'PAT';
            ID := '17';
            COUNT := nCURSOR;
            Items[0] := '6';
            Items[1] := '2AM';
            Items[2] := Trim(FieldByName('MIX_PAY'+IntToStr(i+1)).AsString);
            TempList.Add(Text(3));
            Inc(nCURSOR);
          end;
        end;
      end
      else
      IF Trim(FieldByName('DEF_PAY1').AsString) <> '' Then
      begin
        nCURSOR := 1;
        for i := 0 to 3 do
        begin
          IF Trim(FieldByName('DEF_PAY'+IntToStr(i+1)).AsString) <> '' Then
          begin
            SEGMENT := 'PAT';
            ID := '17';
            COUNT := nCURSOR;
            Items[0] := '4';
            Items[1] := '2AN';
            Items[2] := Trim(FieldByName('DEF_PAY'+IntToStr(i+1)).AsString);
            TempList.Add(Text(3));
            Inc(nCURSOR);
          end;
        end;
      end;

//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
      nCURSOR := 1;
      IF Trim(FieldByName('AD_INFO1').AsString)+
         Trim(FieldByName('AD_INFO2').AsString)+
         Trim(FieldByName('AD_INFO3').AsString)+
         Trim(FieldByName('AD_INFO4').AsString)+
         Trim(FieldByName('AD_INFO5').AsString) <> '' Then
      begin
        SEGMENT := 'FTX';
        ID := '18';
        COUNT := nCURSOR;
        Items[0] := 'ACB';
        Items[1] := FieldByName('AD_INFO1').AsString;
        Items[2] := FieldByName('AD_INFO2').AsString;
        Items[3] := FieldByName('AD_INFO3').AsString;
        Items[4] := FieldByName('AD_INFO4').AsString;
        Items[5] := FieldByName('AD_INFO5').AsString;
        TempList.Add(Text(6));
        Inc(nCURSOR);
      end;

//------------------------------------------------------------------------------
// 특별지급조건
//------------------------------------------------------------------------------
      TMP_MEMO.Text := FieldByName('SPECIAL_PAY').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '18';
            COUNT := nCURSOR;
            Items[0] := 'PCB';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

//------------------------------------------------------------------------------
// 개설(의뢰)은행
//------------------------------------------------------------------------------
      nCURSOR := 1;
      SEGMENT := 'FII';
      ID := '19';
      COUNT := nCURSOR;
      Items[0] := 'AW';
      Items[1] := FieldByName('AP_BANK').AsString;
      Items[2] := '';
      Items[3] := '';
      Items[4] := FieldByName('AP_BANK1').AsString + FieldByName('AP_BANK2').AsString;
      Items[5] := FieldByName('AP_BANK3').AsString + FieldByName('AP_BANK4').AsString;
      TempList.Add(Text(6));
      Inc(nCURSOR);

//------------------------------------------------------------------------------
// 개설(의뢰)은행 전화번호 C
//------------------------------------------------------------------------------
      IF Trim(FieldByName('AP_BANK5').AsString) <> '' Then
      begin
        SEGMENT := 'COM';
        ID := '20';
        COUNT := 1;
        Items[0] := Trim(FieldByName('AP_BANK5').AsString);
        Items[1] := 'TE';
        TempList.Add(Text(2));
      end;

//------------------------------------------------------------------------------
// 희망통지은행 C
//------------------------------------------------------------------------------
      IF (Trim(FieldByName('AD_BANK_BIC').AsString) <> '') OR
         (Trim(FieldByName('AD_BANK1').AsString) <> '') Then
      begin
        SEGMENT := 'FII';
        ID := '19';
        COUNT := nCURSOR;
        Items[0] := '2AA';
        Items[1] := FieldByName('AD_BANK_BIC').AsString;
        IF Trim(Items[1]) <> '' then
        begin
          Items[2] := '25';
          Items[3] := '5';
          Items[4] := '';
          Items[5] := '';
        end
        else
        begin
          Items[2] := '';
          Items[3] := '';
          Items[4] := FieldByName('AD_BANK1').AsString+FieldByName('AD_BANK2').AsString;
          Items[5] := FieldByName('AD_BANK3').AsString+FieldByName('AD_BANK4').AsString;
        end;
        TempList.Add(Text(6));
        Inc(nCURSOR);
      end;

//------------------------------------------------------------------------------
// 확인은행 C (확인지시문언 MAY ADD/CONFIRM 시 필수)
//------------------------------------------------------------------------------
      IF (Trim(FieldByName('CONFIRM_BICCD').AsString) <> '') OR
         (Trim(FieldByName('CONFIRM_BANKNM').AsString) <> '') Then
      begin
        SEGMENT := 'FII';
        ID := '19';
        COUNT := nCURSOR;
        Items[0] := '4AE';
        Items[1] := FieldByName('CONFIRM_BICCD').AsString;
        IF Trim(Items[1]) <> '' then
        begin
          Items[2] := '25';
          Items[3] := '5';
          Items[4] := '';
          Items[5] := '';
        end
        else
        begin
          Items[2] := '';
          Items[3] := '';
          Items[4] := FieldByName('CONFIRM_BANKNM').AsString;
          Items[5] := FieldByName('CONFIRM_BANKBR').AsString;
        end;
        TempList.Add(Text(6));
        Inc(nCURSOR);
      end;

//------------------------------------------------------------------------------
// 개설의뢰인 M
//------------------------------------------------------------------------------
      nCURSOR := 1;
      SEGMENT := 'NAD';
      ID := '1A';
      COUNT := nCURSOR;
      Items[0]  := 'DF';   //3035
      Items[1]  := Trim(FieldByName('APPLIC1').AsString);    //3124 Name and address line
      Items[2]  := Trim(FieldByName('APPLIC2').AsString);    //3124 Name and address line
      Items[3]  := Trim(FieldByName('APPLIC3').AsString);    //3124 Name and address line
      Items[4]  := Trim(FieldByName('APPLIC4').AsString);    //3124 Name and address line
      Items[5]  := '';                                       //3124 Name and address line
      Items[6]  := '';                                       //3036 party name
      Items[7]  := '';                                       //3036 party name
      Items[8]  := '';                                       //3036 party name
      Items[9]  := '';                                       //3042 Steet and number/p.o box
      Items[10] := '';                                       //3042 Steet and number/p.o box
      TempList.Add(Text(11));
      Inc(nCURSOR);

//------------------------------------------------------------------------------
// 개설의뢰인 전화번호 C
//------------------------------------------------------------------------------
      IF Trim(FieldByName('APPLIC5').AsString) <> '' Then
      begin
        SEGMENT := 'COM';
        ID := '21';
        COUNT := 1;
        Items[0] := Trim(FieldByName('APPLIC5').AsString);
        items[1] := 'TE';
        TempList.Add(Text(2));
      end;

//------------------------------------------------------------------------------
// 수익자 M
//------------------------------------------------------------------------------
      SEGMENT := 'NAD';
      ID := '1A';
      COUNT := nCURSOR;
      Items[0]  := 'DG';   //3035
      Items[1]  := Trim(FieldByName('BENEFC1').AsString);    //3124 Name and address line
      Items[2]  := Trim(FieldByName('BENEFC2').AsString);    //3124 Name and address line
      Items[3]  := Trim(FieldByName('BENEFC3').AsString);    //3124 Name and address line
      Items[4]  := Trim(FieldByName('BENEFC4').AsString);    //3124 Name and address line
      Items[5]  := Trim(FieldByName('BENEFC5').AsString);    //3124 Name and address line
      Items[6]  := '';                                       //3036 party name
      Items[7]  := '';                                       //3036 party name
      Items[8]  := '';                                       //3036 party name
      Items[9]  := '';                                       //3042 Steet and number/p.o box
      Items[10] := '';                                       //3042 Steet and number/p.o box
      TempList.Add(Text(11));
      Inc(nCURSOR);

//------------------------------------------------------------------------------
// 개설의뢰인 전자서명 M
//------------------------------------------------------------------------------
      SEGMENT := 'NAD';
      ID := '1A';
      COUNT := nCURSOR;
      Items[0]  := '2AE';   //3035
      Items[1]  := '';    //3124 Name and address line
      Items[2]  := '';    //3124 Name and address line
      Items[3]  := '';    //3124 Name and address line
      Items[4]  := '';    //3124 Name and address line
      Items[5]  := '';    //3124 Name and address line
      Items[6]  := Trim(FieldByName('EX_NAME1').AsString); //3036 party name
      Items[7]  := Trim(FieldByName('EX_NAME2').AsString); //3036 party name
      Items[8]  := Trim(FieldByName('EX_NAME3').AsString); //3036 party name
      Items[9]  := Trim(FieldByName('EX_ADDR1').AsString); //3042 Steet and number/p.o box
      Items[10] := Trim(FieldByName('EX_ADDR2').AsString); //3042 Steet and number/p.o box
      TempList.Add(Text(11));

//------------------------------------------------------------------------------
// 유효기일 M
//------------------------------------------------------------------------------
      SEGMENT := 'DTM';
      ID := '1B';
      COUNT := 1;
      items[0] := '123';
      items[1] := RightStr( Trim(FieldByName('EX_DATE').AsString) ,6);
      items[2] := '101';
      TempList.Add(Text(3));
//------------------------------------------------------------------------------
// 서류제시장소 M
//------------------------------------------------------------------------------
      SEGMENT := 'LOC';
      ID := '22';
      COUNT := 1;
      Items[0] := '143';
      Items[1] := Trim(FieldByName('EX_PLACE').AsString);
      TempList.Add(Text(2));
//------------------------------------------------------------------------------
// 그룹구분자 추가
//------------------------------------------------------------------------------
      SEGMENT := 'UNS';
      ID := '1C';
      COUNT := 1;
      Items[0] := 'D';
      TempList.Add(Text(1));
//------------------------------------------------------------------------------
// 서류제시기간 M
//------------------------------------------------------------------------------
      SEGMENT := 'DTM';
      ID := '1D';
      COUNT := 1;
      Items[0] := '272';
      Items[1] := Trim(FieldByName('PERIOD').AsString);
      Items[2] := '804';
      TempList.Add(Text(3));
//------------------------------------------------------------------------------
// 서류제시기간 상세 C
//------------------------------------------------------------------------------
      IF FieldByName('PERIOD_IDX').AsInteger = 1 Then
      begin
        SEGMENT := 'FTX';
        ID := '23';
        COUNT := 1;
        Items[0] := 'DOC';
        Items[1] := Trim(FieldByName('PERIOD_TXT').AsString);
        TempList.Add(Text(2));
      end;

//------------------------------------------------------------------------------
// 개설금액 M
//------------------------------------------------------------------------------
      SEGMENT := 'MOA';
      ID := '1E';
      COUNT := 1;
      Items[0] := '212';
      Items[1] := Trim(FormatFloat('0.####',FieldByName('cd_amt').AsCurrency));
      Items[2] := Trim(UpperCase(FieldByName('CD_CUR').AsString));
      TempList.Add(Text(3));
//------------------------------------------------------------------------------
// 부가금액부담 C
//------------------------------------------------------------------------------
      IF Trim(FieldByName('AA_CV1').AsString) <> '' Then
      begin
        SEGMENT := 'FTX';
        ID := '24';
        COUNT := 1;
        Items[0] := 'ABT';
        Items[1] := Trim(FieldByName('AA_CV1').AsString);
        Items[2] := Trim(FieldByName('AA_CV2').AsString);
        Items[3] := Trim(FieldByName('AA_CV3').AsString);
        Items[4] := Trim(FieldByName('AA_CV4').AsString);
        TempList.Add(Text(5));
      end;
//------------------------------------------------------------------------------
// 과부족허용율 C
//------------------------------------------------------------------------------
      IF (FieldByName('CD_PERP').AsInteger + FieldByName('CD_PERM').AsInteger) <> 0 Then
      begin
        SEGMENT := 'PCD';
        ID := '25';
        COUNT := 1;
        Items[0] := '13';
        Items[1] := FormatFloat('0',FieldByName('CD_PERP').AsInteger) + '.' + FormatFloat('0',FieldByName('CD_PERM').AsInteger);
        TempList.Add(Text(2));
      end;

//------------------------------------------------------------------------------
//    LOC 수탁발송지1/선적항2/도착항3/최종목적지4
//        운송수단이 DT(해상/항공운송)의 경우 2,3사용
//    DTM 최종선적일자5
//    FTX 선적기간6
//------------------------------------------------------------------------------
      nCURSOR := 1;
      IF Trim(UpperCase(FieldByName('CARRIAGE').AsString)) = 'DT' Then
      begin
        //선적항
        SEGMENT := 'LOC';
        ID := '1F';
        COUNT := nCURSOR;
        Items[0] := '76';
        items[1] := Trim(FieldByName('SUNJUCK_PORT').AsString);
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //최종선적일자 또는 선적기간
        IF Trim(FieldByName('LST_DATE').AsString) <> '' Then
        begin
          SEGMENT := 'DTM';
          ID := '26';
          COUNT := 1;
          Items[0] := '38';
          Items[1] := RightStr( Trim(FieldByName('LST_DATE').AsString) ,6);
          Items[2] := '101';
          TempList.Add(Text(3));
        end
        else
        IF Trim(FieldByName('SHIP_PD1').AsString) <> '' Then
        begin
          SEGMENT := 'FTX';
          ID := '27';
          COUNT := 1;
          Items[0] := '2AF';
          Items[1] := Trim(FieldByName('SHIP_PD1').AsString);
          Items[2] := Trim(FieldByName('SHIP_PD2').AsString);
          Items[3] := Trim(FieldByName('SHIP_PD3').AsString);
          TempList.Add(Text(4));

          IF Trim(FieldByName('SHIP_PD4').AsString) <> '' Then
          begin
            SEGMENT := 'FTX';
            ID := '27';
            COUNT := 2;
            Items[0] := '2AF';
            Items[1] := Trim(FieldByName('SHIP_PD4').AsString);
            Items[2] := Trim(FieldByName('SHIP_PD5').AsString);
            Items[3] := '';
            TempList.Add(Text(4));
          end;
        end;

        //도착항
        SEGMENT := 'LOC';
        ID := '1F';
        COUNT := nCURSOR;
        Items[0] := '12';
        items[1] := Trim(FieldByName('DOCHACK_PORT').AsString);
        TempList.Add(Text(2));
      end
      else
      IF Trim(UpperCase(FieldByName('CARRIAGE').AsString)) = 'DQ' Then
      begin
        //수탁발송지
        SEGMENT := 'LOC';
        ID := '1F';
        COUNT := nCURSOR;
        Items[0] := '149';
        items[1] := Trim(FieldByName('LOAD_ON').AsString);
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //최종선적일자 또는 선적기간
        IF Trim(FieldByName('LST_DATE').AsString) <> '' Then
        begin
          SEGMENT := 'DTM';
          ID := '26';
          COUNT := 1;
          Items[0] := '38';
//          Items[1] := Trim(FieldByName('LST_DATE').AsString);
          Items[1] := RightStr(Trim(FieldByName('LST_DATE').AsString),6);
          Items[2] := '101';
          TempList.Add(Text(3));
        end
        else
        IF Trim(FieldByName('SHIP_PD1').AsString) <> '' Then
        begin
          SEGMENT := 'FTX';
          ID := '27';
          COUNT := 1;
          Items[0] := '2AF';
          Items[1] := Trim(FieldByName('SHIP_PD1').AsString);
          Items[2] := Trim(FieldByName('SHIP_PD2').AsString);
          Items[3] := Trim(FieldByName('SHIP_PD3').AsString);
          TempList.Add(Text(4));

          IF Trim(FieldByName('SHIP_PD4').AsString) <> '' Then
          begin
            SEGMENT := 'FTX';
            ID := '27';
            COUNT := 2;
            Items[0] := '2AF';
            Items[1] := Trim(FieldByName('SHIP_PD4').AsString);
            Items[2] := Trim(FieldByName('SHIP_PD5').AsString);
            Items[3] := '';
            TempList.Add(Text(4));
          end;
        end;

        //도착항
        SEGMENT := 'LOC';
        ID := '1F';
        COUNT := nCURSOR;
        Items[0] := '148';
        items[1] := Trim(FieldByName('FOR_TRAN').AsString);
        TempList.Add(Text(2));
      end;
//------------------------------------------------------------------------------
// 가격조건표시 M
//------------------------------------------------------------------------------
      SEGMENT := 'TOD';
      ID := '1G';
      COUNT := 1;
      IF not Assigned(CD_TERM_frm) Then CD_TERM_frm := TCD_TERM_frm.Create(Application);
      try
        IF CD_TERM_frm.SearchData(UpperCase(FieldByName('TERM_PR').AsString)) > 0 Then
        begin
          Items[0] := FieldByName('TERM_PR').AsString;
          items[1] := '';
        end
        else
        begin
          IF CompareDate(EncodeDate(2020,1,1), Now) <= 0 Then
          begin
            Items[0] := 'ZZZ';
            items[1] := FieldByName('TERM_PR_M').AsString;
          end
          else
          begin
            Items[0] := FieldByName('TERM_PR').AsString;
            items[1] := '';
          end;
        end;
      finally
        FreeAndNil( CD_TERM_frm );
      end;
      TempList.Add(Text(2));

//------------------------------------------------------------------------------
// 가격조건 관련장소 C
//------------------------------------------------------------------------------
      nCURSOR := 1;
      IF Trim(FieldByName('PL_TERM').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '28';
        COUNT := nCURSOR;
        Items[0] := '1';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := Trim(FieldByName('PL_TERM').AsString);
        TempList.Add(Text(5));
        Inc(nCURSOR);
      end;

      IF Trim(FieldByName('PL_TERM').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '28';
        COUNT := nCURSOR;
        Items[0] := '27';
        Items[1] := Trim(FieldByName('ORIGIN').AsString);
        Items[2] := '162';
        Items[3] := '5';
        Items[4] := Trim(FieldByName('ORIGIN_M').AsString);
        TempList.Add(Text(5));
      end;
//------------------------------------------------------------------------------
// 상품(용역)명세
//------------------------------------------------------------------------------
      nCURSOR := 1;
      TMP_MEMO.Text := FieldByName('DESGOOD_1').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '29';
            COUNT := nCURSOR;
            Items[0] := 'AAA';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

//------------------------------------------------------------------------------
// 주요부가조건
//------------------------------------------------------------------------------
      // 2AA SHIPMENT BY
      nCURSOR := 1;
      IF FieldByName('ACD_2AA').AsBoolean Then
      begin
        SEGMENT := 'ALI';
        ID := '1H';
        COUNT := nCURSOR;
        Items[0] := '2AA';
        TempList.Add(Text(1));
        Inc(nCURSOR);

        // SHIPMENT NAME
        SEGMENT := 'NAD';
        ID := '2A';
        COUNT := 1;
        Items[0] := 'CA';
        Items[1] := FieldByName('ACD_2AA_1').AsString;
        TempList.Add(Text(2));
      end;
      // 2AB ACCEPTANCE ~
      IF FieldByName('ACD_2AB').AsBoolean Then
      begin
        SEGMENT := 'ALI';
        ID := '1H';
        COUNT := nCURSOR;
        Items[0] := '2AB';
        TempList.Add(Text(1));
        Inc(nCURSOR);
      end;
      // 2AC ALL DOCUMENTS ~
      IF FieldByName('ACD_2AC').AsBoolean Then
      begin
        SEGMENT := 'ALI';
        ID := '1H';
        COUNT := nCURSOR;
        Items[0] := '2AC';
        TempList.Add(Text(1));
        Inc(nCURSOR);
      end;
      // 2AD LATE PRESENTATION B/L ~
      IF FieldByName('ACD_2AD').AsBoolean Then
      begin
        SEGMENT := 'ALI';
        ID := '1H';
        COUNT := nCURSOR;
        Items[0] := '2AD';
        TempList.Add(Text(1));
        Inc(nCURSOR);
      end;
      // 2AE OTHER ADDITIONAL CONDITIONS
      IF FieldByName('ACD_2AE').AsBoolean Then
      begin
        SEGMENT := 'ALI';
        ID := '1H';
        COUNT := nCURSOR;
        Items[0] := '2AE';
        TempList.Add(Text(1));
        Inc(nCURSOR);

        nCURSOR := 1;
        TMP_MEMO.Text := FieldByName('ACD_2AE_1').AsString;
        TMP_CNT := TMP_MEMO.Count;
        IF TMP_CNT > 0 Then
        begin
          for i := 0 to TMP_CNT-1 do
          begin
            IF i mod 5 = 0 Then
            begin
              SEGMENT := 'FTX';
              ID := '2B';
              COUNT := nCURSOR;
              Items[0] := 'ABS';
              Items[1] := TMP_MEMO[i];

              Items[2] := '';
              Items[3] := '';
              Items[4] := '';
              Items[5] := '';

              IF i+1 <= TMP_CNT-1 Then
                Items[2] := TMP_MEMO[i+1];
              IF i+2 <= TMP_CNT-1 Then
                Items[3] := TMP_MEMO[i+2];
              IF i+3 <= TMP_CNT-1 Then
                Items[4] := TMP_MEMO[i+3];
              IF i+4 <= TMP_CNT-1 Then
                Items[5] := TMP_MEMO[i+4];
              TempList.Add(Text(6));
              Inc(nCURSOR);
            end;
          end;
        end;
      end;

//------------------------------------------------------------------------------
// 주요구비서류
//------------------------------------------------------------------------------
      //380 COMMERCIAL INVOICE
      nCURSOR := 1;
      IF FieldByName('DOC_380').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '380';
        Items[1] := FieldByName('DOC_380_1').AsString;
        TempList.Add(Text(2));
        Inc(nCURSOR);
      end;

      //705,706,717,718,707
      IF FieldByName('DOC_705').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := FieldByName('DOC_705_GUBUN').AsString;
        Items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //운임지불여부 표시
        SEGMENT := 'ALI';
        ID := '2D';
        COUNT := 1;
        Items[0] := FieldByName('DOC_705_3').AsString;
        TempList.Add(Text(1));

        //수하인
        INNER_CNT := 1;
        IF Trim(FieldByName('DOC_705_1').AsString) + Trim(FieldByName('DOC_705_1').AsString) <> '' Then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'CN';
          Items[1] := FieldByName('DOC_705_1').AsString;
          Items[2] := FieldByName('DOC_705_2').AsString;
          TempList.Add(Text(3));
          Inc(INNER_CNT);
        end;

        //통지처
        IF Trim(FieldByName('DOC_705_4').AsString) <> '' then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'NI';
          Items[1] := FieldByName('DOC_705_4').AsString;
          items[2] := '';
          TempList.Add(Text(3));
        end;
      end;

      // 740
      IF FieldByName('DOC_740').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '740';
        Items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //운임지불여부 표시
        SEGMENT := 'ALI';
        ID := '2D';
        COUNT := 1;
        Items[0] := FieldByName('DOC_740_3').AsString;
        TempList.Add(Text(1));

        //수하인
        INNER_CNT := 1;
        IF Trim(FieldByName('DOC_740_1').AsString) + Trim(FieldByName('DOC_740_1').AsString) <> '' Then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'CN';
          Items[1] := FieldByName('DOC_740_1').AsString;
          Items[2] := FieldByName('DOC_740_2').AsString;
          TempList.Add(Text(3));
          Inc(INNER_CNT);
        end;

        //통지처
        IF Trim(FieldByName('DOC_740_4').AsString) <> '' then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'NI';
          Items[1] := FieldByName('DOC_740_4').AsString;
          items[2] := '';
          TempList.Add(Text(3));
        end;
      end;

      // 760
      IF FieldByName('DOC_760').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '760';
        Items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //운임지불여부 표시
        SEGMENT := 'ALI';
        ID := '2D';
        COUNT := 1;
        Items[0] := FieldByName('DOC_760_3').AsString;
        TempList.Add(Text(1));

        //수하인
        INNER_CNT := 1;
        IF Trim(FieldByName('DOC_760_1').AsString) + Trim(FieldByName('DOC_760_1').AsString) <> '' Then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'CN';
          Items[1] := FieldByName('DOC_760_1').AsString;
          Items[2] := FieldByName('DOC_760_2').AsString;
          TempList.Add(Text(3));
          Inc(INNER_CNT);
        end;

        //통지처
        IF Trim(FieldByName('DOC_760_4').AsString) <> '' then
        begin
          SEGMENT := 'NAD';
          ID := '30';
          COUNT := INNER_CNT;
          Items[0] := 'NI';
          Items[1] := FieldByName('DOC_760_4').AsString;
          items[2] := '';
          TempList.Add(Text(3));
        end;
      end;

      // 530
      IF FieldByName('DOC_530').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '530';
        Items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //부보조건
        INNER_CNT := 1;
        IF Trim(FieldByName('DOC_530_1').AsString) + Trim(FieldByName('DOC_530_2').AsString) <> '' Then
        begin
          SEGMENT := 'FTX';
          ID := '2C';
          COUNT := INNER_CNT;
          Items[0] := 'INS';
          Items[1] := FieldByName('DOC_530_1').AsString;
          Items[2] := FieldByName('DOC_530_2').AsString;
          items[3] := '';
          items[4] := '';
          items[5] := '';
          TempList.Add(Text(6));
          Inc(INNER_CNT);
        end;
      end;

      // 271
//      nCURSOR := 1;
      IF FieldByName('DOC_271').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '271';
        Items[1] := FieldByName('DOC_271_1').AsString;
        TempList.Add(Text(2));
        Inc(nCURSOR);
      end;

      // 861
//      nCURSOR := 1;
      IF FieldByName('DOC_861').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '861';
        items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);
      end;

      // 2AA
//      nCURSOR := 1;
      IF FieldByName('DOC_2AA').AsBoolean Then
      begin
        SEGMENT := 'DOC';
        ID := '1I';
        COUNT := nCURSOR;
        Items[0] := '2AA';
        items[1] := '';
        TempList.Add(Text(2));
        Inc(nCURSOR);

        //기타 구비서류
        INNER_CNT := 1;
        TMP_MEMO.Text := FieldByName('DOC_2AA_1').AsString;
        TMP_CNT := TMP_MEMO.Count;
        IF TMP_CNT > 0 Then
        begin
          for i := 0 to TMP_CNT-1 do
          begin
            IF i mod 5 = 0 Then
            begin
              SEGMENT := 'FTX';
              ID := '2C';
              COUNT := INNER_CNT;
              Items[0] := 'ABX';
              Items[1] := TMP_MEMO[i];

              Items[2] := '';
              Items[3] := '';
              Items[4] := '';
              Items[5] := '';

              IF i+1 <= TMP_CNT-1 Then
                Items[2] := TMP_MEMO[i+1];
              IF i+2 <= TMP_CNT-1 Then
                Items[3] := TMP_MEMO[i+2];
              IF i+3 <= TMP_CNT-1 Then
                Items[4] := TMP_MEMO[i+3];
              IF i+4 <= TMP_CNT-1 Then
                Items[5] := TMP_MEMO[i+4];
              TempList.Add(Text(6));
              Inc(INNER_CNT);
            end;
          end;
        end;
      end;

//------------------------------------------------------------------------------
// 수임용도/수입승인번호/금액
//------------------------------------------------------------------------------
      nCURSOR := 1;
      for i := 1 to 5 do
      begin
        IF (FieldByName('IMP_CD'+IntToStr(i)).AsString) <> '' Then
        begin
          //수입용도
          SEGMENT := 'RFF';
          ID := '1J';
          COUNT := nCURSOR;
          Items[0] := FieldByName('IMP_CD'+IntToStr(i)).AsString;
          Items[1] := AnsiReplaceText( FieldByName('IL_NO'+IntToStr(i)).AsString , '-', '');
          TempList.Add(Text(2));
          inc(nCURSOR);

          //금액
          SEGMENT := 'MOA';
          ID := '2E';
          COUNT := 1;
          Items[0] := '2AC';
          Items[1] := FieldByName('IL_AMT'+IntToStr(i)).AsString;
          Items[2] := FieldByName('IL_CUR'+IntToStr(i)).AsString;
          TempList.Add(Text(3));          
        end;
      end;

      TempList.Add(Footer);
    end;

//    Clipboard.AsText := TempList.Text;
    Result := TempList.Text
  finally
    TempList.Free;
    REC_700.Free;
  end;
end;

function APP707_NEW(MAINT_NO,MSEQ,AMDNO,RecvCode : String):String;
var
  REC_707 : TEDIFACT;
  TempList : TStringList;
  i, INNER_CNT : integer;
  TMP_MEMO : TStringList;
  TMP_CNT : integer;
  nCURSOR : Integer;

begin
  Result := 'NO DATA';
  REC_707 := TEDIFACT.Create;
  TempList := TStringList.Create;
  TMP_MEMO := TStringList.Create;
  try
    with REC_707, DMDocumentQry.qryAPP707_NEW do
    begin
      DMDocumentQry.qryAPP707_NEW.Close;
      DMDocumentQry.qryAPP707_NEW.Parameters[0].Value := MAINT_NO;
      DMDocumentQry.qryAPP707_NEW.Parameters[1].Value := MSEQ;
      DMDocumentQry.qryAPP707_NEW.Parameters[2].Value := AMDNO;
      Open;
      
      IF DMDocumentQry.qryAPP707_NEW.RecordCount = 0 Then
      begin
        Exit;
      end;
//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'APP707';
//      DOCNO := MAINT_NO;
      DOCNO := MAINT_NO+'_'+MSEQ+'__'+AMDNO;
      TempList.Add(Header);
//------------------------------------------------------------------------------
// 시작
//------------------------------------------------------------------------------
      SEGMENT := 'BGM';
      ID := '10';
      COUNT := 1;
      Items[0] := '469';
//      Items[1] := MAINT_NO;
      Items[1] := MAINT_NO+'_'+MSEQ+'__'+AMDNO;
      Items[2] := FieldbyName('MESSAGE1').AsString;
      Items[3] := FieldbyName('MESSAGE2').AsString;
      TempList.Add(Text(4));

      //개설방법
      SEGMENT := 'INP';
      ID := '11';
      COUNT := 1;
      Items[0] := '1';
      Items[1] := '5';
      Items[2] := FieldByName('IN_MATHOD').AsString;
      TempList.Add(Text(3));

      //운송수단 변경시에만 표시
      nCURSOR := 2;
      IF Trim(FieldByName('CARRIAGE').AsString) <> '' Then
      begin
        SEGMENT := 'INP';
        ID := '11';
        COUNT := nCURSOR;
        Items[0] := '1';
        Items[1] := '55';
        Items[2] := FieldByName('CARRIAGE').AsString;
        TempList.Add(Text(3));
        Inc(nCURSOR);
      end;

      //운송장취소요청
      IF AnsiMatchText( UpperCase( FieldByName('IS_CANCEL').AsString) , ['CR','CANCEL'] ) Then
      begin
        SEGMENT := 'INP';
        ID := '11';
        COUNT := nCURSOR;
        Items[0] := '1';
        Items[1] := '29';
        Items[2] := 'CR';
        TempList.Add(Text(3));
        Inc(nCURSOR);
      end;

      //확인지시문언 변경시에만 표시
      IF Trim(FieldByName('CONFIRM').AsString) <> '' Then
      begin
        SEGMENT := 'INP';
        ID := '11';
        COUNT := nCURSOR;
        Items[0] := '1';
        Items[1] := '4';
        Items[2] := FieldByName('CONFIRM').AsString;
        TempList.Add(Text(3));
      end;

      //발신은행 참조사항 표시(신용장 번호)
      SEGMENT := 'RFF';
      ID := '12';
      COUNT := 1;
      Items[0] := '2AD';
      Items[1] := FieldByName('CD_NO').AsString;
      TempList.Add(Text(2));

      //조건변경횟수
      SEGMENT := 'RFF';
      ID := '12';
      COUNT := 2;
      Items[0] := '2AB';
      Items[1] := FieldByName('AMD_NO').AsString;
      TempList.Add(Text(2));

      //조건변경 신청일자
      nCURSOR := 1;
      SEGMENT := 'DTM';
      ID := '13';
      COUNT := nCURSOR;
      Items[0] := '2AA';
      items[1] := RightStr( FieldByName('APP_DATE').AsString , 6);
      Items[2] := '101';
      TempList.Add(Text(3));
      Inc(nCURSOR);

      //개설일자 표시
      SEGMENT := 'DTM';
      ID := '13';
      COUNT := nCURSOR;
      Items[0] := '182';
      items[1] := RightStr( FieldByName('ISS_DATE').AsString,6 );
      Items[2] := '101';
      TempList.Add(Text(3));
      Inc(nCURSOR);

      //최종선적일자가 변경되었을때 표시
      IF Trim(FieldByName('LST_DATE').AsString) <> '' Then
      begin
        SEGMENT := 'DTM';
        ID := '13';
        COUNT := nCURSOR;
        Items[0] := '38';
        items[1] := RightStr( FieldByName('LST_DATE').AsString ,6);
        Items[2] := '101';
        TempList.Add(Text(3));
        Inc(nCURSOR);
      end;

      //신용장오류 변경 시 표시
      IF Trim(FieldByName('DOC_CD').AsString) <> '' Then
      begin
        SEGMENT := 'BUS';
        ID := '14';
        COUNT := 1;
        Items[0] := FieldByName('DOC_CD').AsString;
        TempList.Add(Text(1));
      end;

      //신용장 증액분 표시
      nCURSOR := 1;
      IF FieldByName('INCD_AMT').AsInteger > 0 Then
      begin
        SEGMENT := 'MOA';
        ID := '15';
        COUNT := nCURSOR;
        Items[0] := '2AA';
        Items[1] := FormatFloat('0.####', FieldByName('INCD_AMT').AsCurrency);
        items[2] := FieldByName('INCD_CUR').AsString;
        TempList.Add(Text(3));
        Inc(nCURSOR);
      end;
      //신용장 감액분 표시
      IF FieldByName('DECD_AMT').AsInteger > 0 Then
      begin
        SEGMENT := 'MOA';
        ID := '15';
        COUNT := nCURSOR;
        Items[0] := '2AB';
        Items[1] := FormatFloat('0.####', FieldByName('DECD_AMT').AsCurrency);
        items[2] := FieldByName('DECD_CUR').AsString;
        TempList.Add(Text(3));
      end;

      //과부족 허용율 변경시 표시
      IF FieldByName('CD_PERP').AsInteger + FieldByName('CD_PERM').AsInteger > 0 Then
      begin
        SEGMENT := 'PCD';
        ID := '16';
        COUNT := 1;
        Items[0] := '13';
        Items[1] := FieldByName('CD_PERP').AsString+'.'+FieldByName('CD_PERM').AsString;
        TempList.Add(text(2));
      end;

      //수수료 부담자변경을 표시
      nCURSOR := 1;
      IF Trim(FieldByName('CHARGE').AsString) <> '' Then
      begin
        IF FieldByName('CHARGE').AsString = '2AH' Then
        begin
          TMP_MEMO.Text := FieldByName('CHARGE_1').AsString;
          TMP_CNT := TMP_MEMO.Count;

          IF TMP_CNT > 5 Then TMP_CNT := 5;

          for i := 0 to TMP_CNT-1 do
          begin
            SEGMENT := 'ALC';
            ID := '17';
            COUNT := nCURSOR;
            Items[0] := '2AH';            
            Items[1] := TMP_MEMO.Strings[i];
            TempList.Add(Text(2));
            Inc(nCURSOR);
          end;
        end
        else
        begin
          SEGMENT := 'ALC';
          ID := '17';
          COUNT := nCURSOR;
          Items[0] := FieldByName('CHARGE').AsString;
          Items[1] := '';
          TempList.Add(Text(2));
          Inc(nCURSOR);
        end;
      end;

      // 신용장 변경수수료 부담자
      IF Trim(FieldByName('AMD_CHARGE').AsString) <> '' Then
      begin
        IF FieldByName('AMD_CHARGE').AsString = '2AE' Then
        begin
          TMP_MEMO.Text := FieldByName('AMD_CHARGE_1').AsString;
          TMP_CNT := TMP_MEMO.Count;

          IF TMP_CNT > 5 Then TMP_CNT := 5;

          for i := 0 to TMP_CNT-1 do
          begin
            SEGMENT := 'ALC';
            ID := '17';
            COUNT := nCURSOR;
            Items[0] := '2AE';
            Items[1] := TMP_MEMO.Strings[i];
            TempList.Add(Text(2));
            Inc(nCURSOR);
          end;
        end
        else
        begin
          SEGMENT := 'ALC';
          ID := '17';        
          COUNT := nCURSOR;
          Items[0] := FieldByName('AMD_CHARGE').AsString;
          Items[1] := '';
          TempList.Add(Text(2));
          Inc(nCURSOR);
        end;
      end;

      //분할선적/환적허용여부 변경시 표시
      IF ( FieldByName('PSHIP').AsString <> '' ) OR ( FieldByName('TSHIP').AsString <> '' ) Then
      begin
        SEGMENT := 'TSR';
        ID := '18';
        COUNT := 1;
        Items[0] := FieldByName('TSHIP').AsString;
        Items[1] := FieldByName('PSHIP').AsString;
        TempList.Add(Text(2));        
      end;

      //수탁(발송)지 변경을 표시
      nCURSOR := 1;
      IF Trim(FieldByName('LOAD_ON').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '19';      
        COUNT := nCURSOR;
        Items[0] := '149';
        items[1] := Trim(FieldByName('LOAD_ON').AsString);
        TempList.Add(text(2));
        Inc(nCURSOR);
      end;

      //선적항 변경을 표시
      IF Trim(FieldByName('SUNJUCK_PORT').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '19';        
        COUNT := nCURSOR;
        Items[0] := '76';
        Items[1] := Trim(FieldByName('SUNJUCK_PORT').AsString);
        TempList.Add(Text(2));
        Inc(nCURSOR);
      end;

      //도착항 변경을 표시
      IF Trim(FieldByName('DOCHACK_PORT').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '19';        
        COUNT := nCURSOR;
        Items[0] := '12';
        Items[1] := Trim(FieldByName('DOCHACK_PORT').AsString);
        TempList.Add(Text(2));
        Inc(nCURSOR);
      end;

      //최종목적지 변경을 표시
      IF Trim(FieldByName('FOR_TRAN').AsString) <> '' Then
      begin
        SEGMENT := 'LOC';
        ID := '19';        
        COUNT := nCURSOR;
        Items[0] := '148';
        items[1] := Trim(FieldByName('FOR_TRAN').AsString);
        TempList.Add(text(2));
      end;

      nCURSOR := 1;
      //화환어음조건 변경시 표시
      IF Trim(FieldByName('DRAFT1').AsString) <> '' Then
      begin
        for i := 1 to 3 do
        begin
          IF Trim(FieldByName('DRAFT'+IntToStr(i)).AsString) = '' Then Continue;
          SEGMENT := 'PAT';
          ID := '1A';
          COUNT := nCURSOR;
          Items[0] := '2AB';
          Items[1] := '2AO';
          Items[2] := Trim(FieldByName('DRAFT'+IntToStr(i)).AsString);
          TempList.Add(Text(3));
          inc(nCURSOR);
        end;
      end;

      //혼합지급조건명세 변경시 표시
      IF Trim(FieldByName('MIX1').AsString) <> '' Then
      begin
        for i := 1 to 4 do
        begin
          IF Trim(FieldByName('MIX'+IntToStr(i)).AsString) = '' Then Continue;
          SEGMENT := 'PAT';
          ID := '1A';
          COUNT := nCURSOR;
          Items[0] := '6';
          Items[1] := '2AM';
          Items[2] := Trim(FieldByName('MIX'+IntToStr(i)).AsString);
          TempList.Add(Text(3));
          inc(nCURSOR);
        end;
      end;

      //매입/연지급조건명세 변경시 표시
      IF Trim(FieldByName('DEFPAY1').AsString) <> '' Then
      begin
        for i := 1 to 4 do
        begin
          IF Trim(FieldByName('DEFPAY'+IntToStr(i)).AsString) = '' Then Continue;
          SEGMENT := 'PAT';
          ID := '1A';
          COUNT := nCURSOR;
          Items[0] := '4';
          Items[1] := '2AN';
          Items[2] := Trim(FieldByName('DEFPAY'+IntToStr(i)).AsString);
          TempList.Add(Text(3));
          inc(nCURSOR);
        end;
      end;

      //기타정보를 표시
      nCURSOR := 1;
      IF Trim(FieldByName('AD_INFO1').AsString) <> '' then
      begin
        SEGMENT := 'FTX';
        ID := '1B';
        COUNT := nCURSOR;
        Items[0] := 'ACB';
        Items[1] := Trim(FieldByName('AD_INFO1').AsString);
        Items[2] := Trim(FieldByName('AD_INFO2').AsString);
        Items[3] := Trim(FieldByName('AD_INFO3').AsString);
        Items[4] := Trim(FieldByName('AD_INFO4').AsString);
        Items[5] := Trim(FieldByName('AD_INFO5').AsString);
        TempList.Add(Text(6));
        inc(nCURSOR);
      end;

      //부가금액 부담 변경을 표시
      IF Trim(FieldByName('AA_CV1').AsString) <> '' then
      begin
        SEGMENT := 'FTX';
        ID := '1B';
        COUNT := nCURSOR;
        Items[0] := 'ABT';
        Items[1] := Trim(FieldByName('AA_CV1').AsString);
        Items[2] := Trim(FieldByName('AA_CV2').AsString);
        Items[3] := Trim(FieldByName('AA_CV3').AsString);
        Items[4] := Trim(FieldByName('AA_CV4').AsString);
        Items[5] := '';
        TempList.Add(Text(6));
        inc(nCURSOR);
      end;

      //선적기간 변경을 표시
      IF Trim(FieldByName('SHIP_PD1').AsString) <> '' then
      begin
        SEGMENT := 'FTX';
        ID := '1B';
        COUNT := nCURSOR;
        Items[0] := '2AF';
        Items[1] := Trim(FieldByName('SHIP_PD1').AsString);
        Items[2] := Trim(FieldByName('SHIP_PD2').AsString);
        Items[3] := Trim(FieldByName('SHIP_PD3').AsString);
        Items[4] := Trim(FieldByName('SHIP_PD4').AsString);
        Items[5] := Trim(FieldByName('SHIP_PD5').AsString);
        TempList.Add(Text(6));
        inc(nCURSOR);
      end;

      //상품(용역)명세 변경을 표시
      TMP_MEMO.Text := FieldByName('GOODS_DESC').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '1B';
            COUNT := nCURSOR;
            Items[0] := 'AAA';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

      //주요 구비서류 변경을 표시
      TMP_MEMO.Text := FieldByName('DOC_REQ').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '1B';
            COUNT := nCURSOR;
            Items[0] := 'ABX';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

      //부가조건 변경을 표시
      TMP_MEMO.Text := FieldByName('ADD_CONDITION').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '1B';
            COUNT := nCURSOR;
            Items[0] := 'ABS';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

      //수익자에 대한 특별지급조건 변경을 표시
      TMP_MEMO.Text := FieldByName('SPECIAL_PAY').AsString;
      TMP_CNT := TMP_MEMO.Count;
      IF TMP_CNT > 0 Then
      begin
        for i := 0 to TMP_CNT-1 do
        begin
          IF i mod 5 = 0 Then
          begin
            SEGMENT := 'FTX';
            ID := '1B';
            COUNT := nCURSOR;
            Items[0] := 'PCB';
            Items[1] := TMP_MEMO[i];

            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';

            IF i+1 <= TMP_CNT-1 Then
              Items[2] := TMP_MEMO[i+1];
            IF i+2 <= TMP_CNT-1 Then
              Items[3] := TMP_MEMO[i+2];
            IF i+3 <= TMP_CNT-1 Then
              Items[4] := TMP_MEMO[i+3];
            IF i+4 <= TMP_CNT-1 Then
              Items[5] := TMP_MEMO[i+4];
            TempList.Add(Text(6));
            Inc(nCURSOR);
          end;
        end;
      end;

      // 개설(의뢰)은행 표시
      nCURSOR := 1;
      SEGMENT := 'FII';
      ID := '1C';
      COUNT := nCURSOR;
      Items[0] := 'AW';
      Items[1] := FieldByName('AP_BANK').AsString;
      Items[2] := '';
      Items[3] := '';
      Items[4] := FieldByName('AP_BANK1').AsString + FieldByName('AP_BANK2').AsString;
      Items[5] := FieldByName('AP_BANK3').AsString + FieldByName('AP_BANK4').AsString;
      TempList.Add(Text(6));
      Inc(nCURSOR);

      //개설(의뢰)은행 전화번호 표시
      IF Trim(FieldByName('AP_BANK5').AsString) <> '' Then
      begin
        SEGMENT := 'COM';
        ID := '20';
        COUNT := 1;
        Items[0] := Trim(FieldByName('AP_BANK5').AsString);
        Items[1] := 'TE';
      end;

      //(희망)통지은행 변경을 표시
      IF (Trim(FieldByName('AD_BANK').AsString) <> '') OR (Trim(FieldByName('AD_BANK1').AsString) <> '') Then
      begin
        SEGMENT := 'FII';
        ID := '1C';
        COUNT := nCURSOR;
        Items[0] := '2AA';
        IF Trim(FieldByName('AD_BANK').AsString) <> '' Then
        begin
          Items[1] := Trim(FieldByName('AD_BANK').AsString);
          Items[2] := '25';
          Items[3] := '5';
          Items[4] := FieldByName('AD_BANK1').AsString + FieldByName('AD_BANK2').AsString;
          Items[5] := FieldByName('AD_BANK3').AsString + FieldByName('AD_BANK4').AsString;
        end
        else
        begin
          Items[1] := '';
          Items[2] := '';
          Items[3] := '';
          Items[4] := FieldByName('AD_BANK1').AsString + FieldByName('AD_BANK2').AsString;
          Items[5] := FieldByName('AD_BANK3').AsString + FieldByName('AD_BANK4').AsString;
        end;
        TempList.Add(Text(6));
        Inc(nCURSOR);
      end;

      //확인은행
      IF (Trim(FieldByName('CONFIRM_BIC').AsString) <> '') OR (Trim(FieldByName('CONFIRM1').AsString) <> '') Then
      begin
        SEGMENT := 'FII';
        ID := '1C';
        COUNT := nCURSOR;
        Items[0] := '4AE';
        IF Trim(FieldByName('CONFIRM_BIC').AsString) <> '' Then
        begin
          Items[1] := Trim(FieldByName('CONFIRM_BIC').AsString);
          Items[2] := '25';
          Items[3] := '5';
          Items[4] := FieldByName('CONFIRM1').AsString + FieldByName('CONFIRM2').AsString;
          Items[5] := FieldByName('CONFIRM3').AsString + FieldByName('CONFIRM4').AsString;
        end
        else
        begin
          Items[1] := '';
          Items[2] := '';
          Items[3] := '';
          Items[4] := FieldByName('CONFIRM1').AsString + FieldByName('CONFIRM2').AsString;
          Items[5] := FieldByName('CONFIRM3').AsString + FieldByName('CONFIRM4').AsString;
        end;
        TempList.Add(Text(6));
        Inc(nCURSOR);
      end;

      //유효기일/서류제시장소 변경을 표시
      IF (Trim(FieldByName('EX_DATE').AsString) <> '') OR (Trim(FieldByName('EX_PLACE').AsString) <> '') Then
      begin
        //유효기일
        SEGMENT := 'DTM';
        ID := '1D';
        COUNT := 1;
        Items[0] := '123';
        Items[1] := RightStr( Trim(FieldByName('EX_DATE').AsString) ,6);
        Items[2] := '101';
        TempList.Add(Text(3));

        //서류제시장소
        SEGMENT := 'LOC';
        ID := '21';
        COUNT := 1;
        Items[0] := '143';
        Items[1] := Trim(FieldByName('EX_PLACE').AsString);
        TempList.Add(Text(2));
      end;

      //개설의뢰인, 수익자정보변경

      //개설의뢰인 표시
      nCURSOR := 1;
      SEGMENT := 'NAD';
      ID := '1E';
      COUNT := nCURSOR;
      Items[0]  := 'DF';
      Items[1]  := FieldByName('APPLIC1').AsString;
      Items[2]  := FieldByName('APPLIC2').AsString;
      Items[3]  := FieldByName('APPLIC3').AsString;
      Items[4]  := FieldByName('APPLIC4').AsString;
      Items[5]  := '';
      Items[6]  := '';
      Items[7]  := '';
      Items[8]  := '';
      Items[9]  := '';
      Items[10] := '';
      TempList.Add(Text(11));
      Inc(nCURSOR);

      //개설의뢰인 전화번호
      if Trim(FieldByName('APPLIC5').AsString) <> '' Then
      begin
        SEGMENT := 'COM';
        ID := '22';
        COUNT := 1;
        Items[0] := Trim(FieldByName('APPLIC5').AsString);
        Items[1] := 'TE';
        TempList.Add(Text(2));
      end;

      //개설의뢰인 정보변경을 표시
      if FieldByName('APPLIC_CHG').AsBoolean Then
      begin
        SEGMENT := 'NAD';
        ID := '1E';
        COUNT := nCURSOR;
        Items[0]  := 'WX';
        Items[1]  := FieldByName('APPLIC1').AsString;
        Items[2]  := FieldByName('APPLIC2').AsString;
        Items[3]  := FieldByName('APPLIC3').AsString;
        Items[4]  := FieldByName('APPLIC4').AsString;
        Items[5]  := '';
        Items[6]  := '';
        Items[7]  := '';
        Items[8]  := '';
        Items[9]  := '';
        Items[10] := '';
        TempList.Add(Text(11));
        Inc(nCURSOR);
      end;

      //수익자정보 변경을 표시
      IF FieldByName('BENEFC_CHG').AsBoolean Then
      begin
        SEGMENT := 'NAD';
        ID := '1E';
        COUNT := nCURSOR;
        Items[0]  := 'DG';
        Items[1]  := FieldByName('BENEFC1').AsString;
        Items[2]  := FieldByName('BENEFC2').AsString;
        Items[3]  := FieldByName('BENEFC3').AsString;
        Items[4]  := FieldByName('BENEFC4').AsString;
        Items[5]  := FieldByName('BENEFC5').AsString;
        Items[6]  := '';
        Items[7]  := '';
        Items[8]  := '';
        Items[9]  := '';
        Items[10] := '';
        TempList.Add(Text(11));
        Inc(nCURSOR);
      end;

      //개설의뢰인 전자서명 표시
      SEGMENT := 'NAD';
      ID := '1E';
      COUNT := nCURSOR;
      Items[0]  := '2AE';
      Items[1]  := '';
      Items[2]  := '';
      Items[3]  := '';
      Items[4]  := '';
      Items[5]  := '';
      Items[6]  := FieldByName('EX_NAME1').AsString;
      Items[7]  := FieldByName('EX_NAME2').AsString;
      Items[8]  := FieldByName('EX_NAME3').AsString;
      Items[9]  := FieldByName('EX_ADDR1').AsString;
      Items[10] := FieldByName('EX_ADDR2').AsString;
      TempList.Add(Text(11));
      Inc(nCURSOR);

      //서류제시기간 변경
      IF FieldByName('PERIOD_DAYS').AsInteger > 0 Then
      begin
        SEGMENT := 'DTM';
        ID := '1F';
        COUNT := 1;
        Items[0] := '272';
        Items[1] := FieldByName('PERIOD_DAYS').AsString;
        Items[2] := '804';
        TempList.Add(Text(3));

        IF FieldByName('PERIOD_DAYS').AsInteger = 1 Then
        begin
          SEGMENT := 'FTX';
          ID := '23';
          COUNT := 1;
          Items[0] := 'DOC';
          Items[1] := FieldByName('PERIOD_DETAIL').AsString;
          TempList.Add(Text(2));
        end;
      end;

      //수입승인번호 변경을 표시
      nCURSOR := 1;
      for i := 1 to 5 do
      begin
        IF Trim(FieldByName('IMP_CD'+IntToStr(i)).AsString) = '' Then Continue;

        SEGMENT := 'RFF';
        ID := '1G';
        COUNT := nCURSOR;
        Items[0] := Trim(FieldByName('IMP_CD'+IntToStr(i)).AsString);
        Items[1] := Trim(FieldByName('IL_NO'+IntToStr(i)).AsString);
        TempList.Add(Text(2));

        IF FieldByName('IL_AMT'+IntToStr(i)).AsInteger <> 0 Then
        begin
          SEGMENT := 'MOA';
          ID := '24';
          COUNT := 1;
          Items[0] := '2AC';
          Items[1] := FormatFloat('0.####',FieldByName('IL_AMT'+IntToStr(i)).AsCurrency);
          Items[2] := Trim(FieldByName('IL_CUR'+IntToStr(i)).AsString);
          TempList.Add(Text(3));
        end;

        Inc(nCURSOR);
      end;
      TempList.Add(Footer);
    end;

    Result := TempList.Text;

  finally
    TempList.Free;
    REC_707.Free;
  end;
end;

function APPSPC_NEW(MAINT_NO,BGM1,BGM2,BGM3,BGM4,RecvCode : String):String;
var
  i,j,iCount,iSEQ :Integer;
  sDocGubun : String;
  SPC_FACT : TEDIFACT;
  TempList : TStringList;
  DocCount : TAPPSPCDOC;

begin
  SPC_FACT := TEDIFACT.Create;
  TempList := TStringList.Create;
  try
    with SPC_FACT do
    begin
      //준비한 APPSPC문서의 관리번호가 존재하지 않을경우
      DMDocumentQry.OPEN_APPSPC(MAINT_NO);
      If DMDocumentQry.qryAPPSPCH.RecordCount = 0 then
      begin
        exit;
      end;
//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'APPSPC';
      DOCNO := MAINT_NO;
      TempList.Add(Header('1'));
//------------------------------------------------------------------------------
// 시작
//------------------------------------------------------------------------------
      //문서번호,문서기능,문서유형.
      SEGMENT := 'BGM';
      ID := '10';
      COUNT := 1;
      Items[0] := DMDocumentQry.qryAPPSPCH.FieldByName('BGM_GUBUN').AsString;
      Items[1] := '';
      Items[2] := '';
      Items[3] := '';
      Items[4] := BGM1+'-'+BGM2+BGM3+BGM4;
      Items[5] := DMDocumentQry.qryAPPSPCH.FieldByName('MESSAGE1').AsString;
      if Trim(DMDocumentQry.qryAPPSPCH.FieldByName('MESSAGE2').AsString) = '' then
        Items[6] := 'AB'
      else
        Items[6] := DMDocumentQry.qryAPPSPCH.FieldByName('MESSAGE2').AsString;

      TempList.Add(Text(7));

      //추심(매입)은행 정보
      SEGMENT := 'FII';
      ID := '11';
      COUNT := 1;
      Items[0] := 'OM';
      Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ACCOUNTNO').AsString;
      Items[2] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_NAME1').AsString;
      Items[3] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_NAME2').AsString;
      Items[4] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_CURR').AsString;
      Items[5] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_BANK').AsString;
      Items[6] := '25';
      Items[7] := 'BOK';
      Items[8] := '';
      Items[9] := '';
      Items[10] := '';
      Items[11] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_BANKNAME').AsString;
      Items[12] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_BANKBU').AsString;
      Items[13] := '';
      TempList.Add(Text(14));

      //추가계좌1
      iCount := 1;
      IF Trim(DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO1').AsString) <> '' then
      begin
        SEGMENT := 'FTX';
        ID := '2E';
        Count := iCount;
        Items[0] := 'ACB';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO1').AsString;
        Items[6] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_NAME1').AsString;
        Items[7] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_CURR1').AsString;
        Items[8] := '';
        Items[9] := '';
        Items[10] := '';

        iCount := iCount + 1;
        TempList.Add(Text(11));
      end;

      //추가계좌2
      IF Trim(DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO2').AsString) <> '' then
      begin
        SEGMENT := 'FTX';
        ID := '2E';
        COUNT := iCount;
        Items[0] := 'ACB';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_ACCOUNTNO2').AsString;
        Items[6] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_NAME2').AsString;
        Items[7] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_ADD_CURR2').AsString;
        Items[8] := '';
        Items[9] := '';
        Items[10] := '';

        TempList.Add(Text(11));
      end;

      //신청일
      SEGMENT := 'DTM';
      ID := '12';
      COUNT := 1;
      Items[0] := '2AA';
      Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_DATE').AsString;
      Items[2] := '102';
      TempList.Add(Text(3));

      //추심(매입) 신청번호
      SEGMENT := 'RFF';
      ID := '13';
      COUNT := 1;
      Items[0] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_CODE').AsString;
      Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_NO').AsString;
      Items[2] := '';
      TempList.Add(Text(3));
      

      //신청인정보
      SEGMENT := 'NAD';
      ID := '14';
      COUNT := 1;
      Items[0] := '4AA';
      Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_SAUPNO').AsString;
      Items[2] := '';
      Items[3] := '';
      Items[4] := '';
      Items[5] := '';
      Items[6] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_SNAME').AsString;
      Items[7] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_NAME').AsString;
      Items[8] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_ELEC').AsString;
      Items[9] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_ADDR1').AsString;
      Items[10] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_ADDR2').AsString;
      Items[11] := DMDocumentQry.qryAPPSPCH.FieldByName('APP_ADDR3').AsString;
      Items[12] := '';
      Items[13] := '';
      Items[14] := '';
      Items[15] := '';
      TempList.Add(Text(16));

      //구매자정보
      SEGMENT := 'NAD';
      ID := '14';
      COUNT := 2;
      Items[0] := 'DF';
      Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_SAUPNO').AsString;
      Items[2] := '';
      Items[3] := '';
      Items[4] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_EMAIL1').AsString;
      Items[5] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_EMAIL2').AsString;
      Items[6] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_NAME1').AsString;
      Items[7] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_NAME2').AsString;
      Items[8] := DMDocumentQry.qryAPPSPCH.FieldByName('DF_NAME3').AsString;
      Items[9] := '';
      Items[10] := '';
      Items[11] := '';
      Items[12] := '';
      Items[13] := '';
      Items[14] := '';
      Items[15] := '';
      TempList.Add(Text(16));

      //추심(매입)금액 (외화)+통화단위
      if DMDocumentQry.qryAPPSPCH.FieldByName('CP_AMTU').AsCurrency > 0 then
      begin
        SEGMENT := 'MOA';
        ID := '15';
        COUNT := 1;
        Items[0] := '2AD';
        Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_AMTU').AsString;
        Items[2] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_AMTC').AsString;
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //추심(매입)금액 (원화)
      if DMDocumentQry.qryAPPSPCH.FieldByName('CP_AMT').AsCurrency > 0 then
      begin
        SEGMENT := 'MOA';
        ID := '15';
        COUNT := 2;
        Items[0] := '2AE';
        Items[1] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_AMT').AsString;
        Items[2] := 'KRW';
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //적용환율
      if DMDocumentQry.qryAPPSPCH.FieldByName('CP_CUX').AsCurrency > 0 then
      begin
        SEGMENT := 'CUX';
        ID := '16';
        COUNT := 1;
        Items[0] := '';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := '';
        Items[6] := '';
        Items[7] := '';
        Items[8] := DMDocumentQry.qryAPPSPCH.FieldByName('CP_CUX').AsString;
        Items[9] := '';
        TempList.Add(Text(10));
      end;

      DocCount := DMDocumentQry.getAPPSPCCount(MAINT_NO); // 물품수령증명서 , 세금계산서 , 상업송장의 건수를 조회하는 함수
      //물품수령증명서 첨부통수
      SEGMENT := 'BUS';
      ID := '17';
      COUNT := 1;
      Items[0] := '2AH';
      Items[1] := 'ZZZ';
      Items[2] := '';
      Items[3] := '';
      Items[4] := IntToStr(DocCount.DOC_2AH);
      Items[5] := '';
      Items[6] := '';
      Items[7] := '';
      Items[8] := '';
      Items[9] := '';
      Items[10] := '';
      TempList.Add(Text(11));

      //세금계산서 or 상업송장 첨부통수
      SEGMENT := 'BUS';
      ID := '17';
      COUNT := 2;
      Items[0] := '2AI';
      Items[1] := 'ZZZ';
      Items[2] := '';
      Items[3] := '';

      if DocCount.DOC_2AJ >= 1 then
         Items[4] := IntToStr(DocCount.DOC_2AJ)
      else if DocCount.DOC_1BW >= 1 then
         Items[4] := IntToStr(DocCount.DOC_1BW);

      Items[5] := '';
      Items[6] := '';
      Items[7] := '';
      Items[8] := '';
      Items[9] := '';
      Items[10] := '';
      TempList.Add(Text(11));

//------------------------------------------------------------------------------
// APPSPC_D1 세부정보
//------------------------------------------------------------------------------
      DMDocumentQry.qryAPPSPCD1.First;
      while not DMDocumentQry.qryAPPSPCD1.Eof do
      begin
        sDocGubun := DMDocumentQry.qryAPPSPCD1.FieldByName('DOC_GUBUN').AsString;
        iSEQ := DMDocumentQry.qryAPPSPCD1.FieldByName('SEQ').AsInteger;
        //DOC : 추심(매입)관련 전자문서번호(내국신용장, 물품수령증명서, 세금계산서, 상업송장)
        SEGMENT := 'DOC';
        ID := '18';
        COUNT := 1;
        Items[0] := sDocGubun;
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := DMDocumentQry.qryAPPSPCD1.FieldByName('DOC_NO').AsString;
        Items[5] := '';
        Items[6] := '';
        Items[7] := '';
        Items[8] := '';
        Items[9] := '';
        Items[10] := '';
        TempList.Add(Text(11));

        //RFF(LC) : 내국신용장 및 물품수령증명서 상의 내국신용장번호
        if (sDocGubun = '2AP') or (sDocGubun = '2AH') then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'LC';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('LR_NO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(CRG) : 물품수령증명서 발급번호
        if (sDocGubun = '2AH') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('LR_NO2').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'CRG';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('LR_NO2').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(HS) : 물품수령증명서 대표공급물품 HS 코드
        if(sDocGubun = '2AH') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('BSN_HSCODE').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'HS';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('BSN_HSCODE').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(RE) : 세금계산서 권번호
        if(sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_RENO').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'RE';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_RENO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(SE) : 세금계산서 호번호
        if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_SENO').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'SE';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_SENO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(FS) : 세금계산서 일련번호
        if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_FSNO').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'FS';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_FSNO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(ACE) : 세금계산서 관리번호
        IF (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_MAINTNO').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'ACE';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_MAINTNO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;

        //RFF(DM) : 세금계산서 참조번호
        IF (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_DMNO').AsString) <> '' ) then
        begin
          SEGMENT := 'RFF';
          ID := '20';
          COUNT := 1;
          Items[0] := 'DM';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_DMNO').AsString;
          Items[2] := '';
          TempList.Add(Text(3));
        end;


        //DTM(182) : 내국신용장 개설일자, 물품수령증명서 발급일자, 세금계산서 작성일, 상업송장 발급일
        SEGMENT := 'DTM';
        ID := '21';
        COUNT := 1;
        Items[0] := '182';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_DATE').AsString;
        Items[2] := '102';
        TempList.Add(Text(3));

        //DMT(2) : 물품수령증명서의 내국신용장 인도기일
        IF sDocGubun = '2AH' then
        begin
          SEGMENT := 'DTM';
          ID := '21';
          COUNT := 1;
          Items[0] := '2';
          Items[1] :=  DMDocumentQry.qryAPPSPCD1.FieldByName('GET_DATE').AsString;
          Items[2] := '102';
          TempList.Add(Text(3));
        end;

        //DMT(123) : 물품수령증명서 상의 내국신용장 유효기일
        if sDocGubun = '2AH' then
        begin
          SEGMENT := 'DTM';
          ID := '21';
          COUNT := 1;
          Items[0] := '123';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('EXP_DATE').AsString;
          Items[2] := '102';
          TempList.Add(Text(3));
        end;

        //DTM(50) : 물품수령증명서 인수일자
        if sDocGubun = '2AH' then
        begin
          SEGMENT := 'DTM';
          ID := '21';
          COUNT := 1;
          Items[0] := '50';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ACC_DATE').AsString;
          Items[2] := '102';
          TempList.Add(Text(3));
        end;

        //DTM(36) : 물품수령증명서 유효기일 C
        if (sDocGubun = '2AH') and ( Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VAL_DATE').AsString) <> '') then
        begin
          SEGMENT := 'DTM';
          ID := '21';
          COUNT := 1;
          Items[0] := '36';
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VAL_DATE').AsString;
          Items[2] := '102';
          TempList.Add(Text(3));
        end;

        //NAD(SU,SE,EX) 물품공급자(수출자)의 사업자등록번호, 상호, 대표자성명, 전자서명, 주소 등을 기재하는 전송항목
        if sDocGubun <> '2AP' then
        begin
          SEGMENT := 'NAD';
          ID := '22';
          COUNT := 1;
          if sDocGubun = '2AH' then
          begin

            Items[0] := 'SU';
            Items[1] := '';
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_SNAME').AsString;
            Items[10] := '';
            Items[11] := '';
            Items[12] := '';
            Items[13] := '';
            Items[14] := '';

          end
          else if sDocGubun = '2AJ' then
          begin

            Items[0] := 'SE';
            Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_SAUPNO').AsString;
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_NAME').AsString;
            Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ELEC').AsString;
            Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR1').AsString;
            Items[13] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR2').AsString;
            Items[14] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR3').AsString;

          end
          else if sDocGubun = '1BW' then
          begin

            Items[0] := 'EX';
            Items[1] := '';
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_NAME').AsString;
            Items[11] := '';
            Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR1').AsString;
            Items[13] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR2').AsString;
            Items[14] := DMDocumentQry.qryAPPSPCD1.FieldByName('SE_ADDR3').AsString;

          end;
          Items[2] :=  '';
          Items[3] :=  '';
          Items[4] :=  '';
          Items[5] :=  '';
          Items[6] :=  '';
          Items[7] :=  '';
          Items[8] :=  '';
          Items[15] := '';
          Items[16] := '';
          Items[17] := '';
          Items[18] := '';
          TempList.Add(Text(19));

        //NAD(SU,SE,EX) 물품공급자(수출자)의 사업자등록번호, 상호, 대표자성명, 전자서명, 주소 등을 기재하는 전송항목
        SEGMENT := 'NAD';
        ID := '22';
        COUNT := 1;
          if sDocGubun = '2AH' then
          begin

            Items[0] := 'AP';
            Items[1] := '';
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_NAME').AsString;
            Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_ELEC').AsString;

          end
          else if sDocGubun = '2AJ' then
          begin

            Items[0] := 'BY';
            Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_SAUPNO').AsString;
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_NAME').AsString;
            Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_ELEC').AsString;

          end
          else if sDocGubun = '1BW' then
          begin

            Items[0] := 'IM';
            Items[1] := '';
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_NAME').AsString;
            Items[11] := '';

          end;
          Items[2] := '';
          Items[3] := '';
          Items[4] := '';
          Items[5] := '';
          Items[6] := '';
          Items[7] := '';
          Items[8] := '';
          Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_ADDR1').AsString;
          Items[13] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_ADDR2').AsString;
          Items[14] := DMDocumentQry.qryAPPSPCD1.FieldByName('BY_ADDR3').AsString;
          Items[15] := '';
          Items[16] := '';
          Items[17] := '';
          Items[18] := '';
          TempList.Add(Text(19));

          //NAD(AG) : 세금계산서 수탁자
          if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('AG_SNAME').AsString) <> '') then
          begin
            SEGMENT := 'NAD';
            ID := '22';
            COUNT := 1;
            Items[0] := 'AG';
            Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_SAUPNO').AsString;
            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';
            Items[6] := '';
            Items[7] := '';
            Items[8] := '';
            Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_SNAME').AsString;
            Items[10] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_NAME').AsString;
            Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_ELEC').AsString;
            Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_ADDR1').AsString;
            Items[13] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_ADDR2').AsString;
            Items[14] := DMDocumentQry.qryAPPSPCD1.FieldByName('AG_ADDR3').AsString;
            Items[15] := '';
            Items[16] := '';
            Items[17] := '';
            Items[18] := '';
            TempList.Add(Text(19));
          end;
        end;
        iCount := 1;
        //IMD(SG-SU) 공급자 업태 (3반복)
        if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI1_1').AsString) <> '' then
        begin
          for i:=1 to 3 do
          begin
            if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI1_'+IntToStr(iCount)).AsString) <> '' then
            begin
              SEGMENT := 'IMD';
              ID := '23';
              COUNT := iCount;
              Items[0] := '';
              Items[1] := '';
              Items[2] := 'SG';
              Items[3] := 'SU';
              Items[4] := '';
              Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI1_'+IntToStr(iCount)).AsString;
              Items[6] := '';
              TempList.Add(Text(7));
              iCount := iCount + 1;
            end;
          end;
        end;

        iCount := 1;
        //IMD(HN-SU) 공급자 종목
        if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM1_1').AsString) <> '' then
        begin
          for i:=1 to 3 do
          begin
            if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM1_'+IntToStr(iCount)).AsString) <> '' then
            begin
              SEGMENT := 'IMD';
              ID := '23';
              COUNT := iCount;
              Items[0] := '';
              Items[1] := '';
              Items[2] := 'HN';
              Items[3] := 'SU';
              Items[4] := '';
              Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM1_'+IntToStr(iCount)).AsString;
              Items[6] := '';
              TempList.Add(Text(7));
              iCount := iCount + 1;
            end;
          end;
        end;

      iCount := 1;
      //IMD(SG-BY)공급받는자 업태
      if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI2_1').AsString) <> '' then
      begin
        for i:=1 to 3 do
        begin
          if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI2_'+IntToStr(iCount)).AsString) <> '' then
          begin
            SEGMENT := 'IMD';
            ID := '23';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := 'SG';
            Items[3] := 'BY';
            Items[4] := '';
            Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('SG_UPTAI2_'+IntToStr(iCount)).AsString;
            Items[6] := '';
            TempList.Add(Text(7));
            iCount := iCount + 1;
          end;
        end;
      end;

      iCount := 1;
      //IMD(HN-BY) 공급받는자 종목
      if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM2_1').AsString) <> '' then
      begin
        for i:=1 to 3 do
        begin
          if Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM2_'+IntToStr(iCount)).AsString) <> '' then
          begin
            SEGMENT := 'IMD';
            ID := '23';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := 'HN';
            Items[3] := 'BY';
            Items[4] := '';
            Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('HN_ITEM2_'+IntToStr(iCount)).AsString;
            Items[6] := '';
            TempList.Add(Text(7));
            iCount := iCount + 1;
          end;
        end;
      end;

      //CUX : 물품수령증명서 매매기준율 C
      if DMDocumentQry.qryAPPSPCD1.FieldByName('EXCH').AsCurrency > 0 then
      begin
        SEGMENT := 'CUX';
        ID := '24';
        COUNT := 1;
        Items[0] := '';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := '';
        Items[6] := '';
        Items[7] := '';
        Items[8] := DMDocumentQry.qryAPPSPCD1.FieldByName('EXCH').AsString;
        Items[9] := '';
        TempList.Add(Text(10));
      end;

      //BUS 내국신용장 종류 C
      if (sDocGubun = '2AP') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('LA_TYPE').AsString) <> '') then
      begin
        SEGMENT := 'BUS';
        ID := '25';
        COUNT := 1;
        Items[0] := '';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := '';
        Items[6] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_TYPE').AsString;
        Items[7] := '';
        Items[8] := '';
        Items[9] := '';
        Items[10] := '';
        TempList.Add(Text(11));
      end;

      //MOA(2BD) : 물품수령증명서 상의 내국신용장 개설금액(외화)
      if (sDocGubun = '2AH') and (DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_AMTU').AsCurrency > 0) then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '2BD';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_AMTU').AsString;
        Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_AMTC').AsString;
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //MOA(2BE) : 물품수령증명서 상의 내국신용장 개설금액(원화)
      if (sDocGubun = '2AH') and (DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_AMT').AsCurrency > 0) then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '2BE';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_AMT').AsString;
        Items[2] := 'KRW';
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //MOA(2AD) : 물품수령증명서 상의 인수금액(외화) or 세금계산서 상의 세액(원화)
      if ((sDocGubun = '2AH') or (sDocGubun = '2AJ') ) and (DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsCurrency > 0) then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '2AD';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsString;
        if sDocGubun = '2AH' then
          Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMTC').AsString
        else if sDocGubun = '2AJ' then
          Items[2] := 'KRW';
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //MOA(2AE) : 물품수령증명서 인수금액(원화)C, 세금계산서 공급가액M 입력
      if ((sDocGubun = '2AH') and (DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsCurrency > 0)) or (sDocGubun = '2AJ') then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '2AE';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsString;
        Items[2] := 'KRW';
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //MOA(128) : 물품수령증명서 총금액C, 세금계산서 총금액(공급가액 + 세액), 상업송장 총금액M
      if (sDocGubun = '2AH') and (DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsCurrency > 0) then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '128';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsString;
        Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_TOTAMTC').AsString;
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end
      else if (sDocGubun = '2AJ') or (sDocGubun = '1BW') then
      begin
        SEGMENT := 'MOA';
        ID := '26';
        COUNT := 1;
        Items[0] := '128';
        if sDocGubun = '2AJ' then
        begin
          Items[1] := CurrToStr(DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMT').AsCurrency +DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_EXPAMTU').AsCurrency);
          Items[2] := 'KRW'
        end
        else if sDocGubun = '1BW' then
        begin
          Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_TOTAMT').AsString;
          Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('ISS_TOTAMTC').AsString;
        end;
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //CNT(1AA) : 총수량
      if (sDocGubun <> '2AP') and (DMDocumentQry.qryAPPSPCD1.FieldByName('TOTCNT').AsCurrency > 0) then
      begin
        SEGMENT := 'CNT';
        ID := '27';
        COUNT := 1;
        Items[0] := '1AA';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('TOTCNT').AsString;
        Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('TOTCNTC').AsString;
        TempList.Add(Text(3));
      end;

      iCount := 1;
      //PAI 결제방법을 CODE로 기재(세금계산서) M
      if sDocGubun = '2AJ' then
      begin
          //현금
          if (DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT1').AsCurrency > 0 )or( DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU1').AsCurrency > 0 ) then
          begin
            SEGMENT := 'PAI';
            ID := '28';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := '10';
            Items[3] := '';
            Items[4] := '';
            Items[5] :=  '';
            TempList.Add(Text(6));
            //현금 원화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT1').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AE';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT1').AsString;
              Items[2] := 'KRW';
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            //현금 외화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU1').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AD';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU1').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTC1').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            Inc(iCount);
          end;

          //수표
          if (DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT2').AsCurrency > 0 )or( DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU2').AsCurrency > 0 ) then
          begin
            SEGMENT := 'PAI';
            ID := '28';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := '20';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';
            TempList.Add(Text(6));
            //수표 원화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT2').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AE';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT2').AsString;
              Items[2] := 'KRW';
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            //수표 외화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU2').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AD';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU2').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTC2').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            Inc(iCount);
          end;
          //어음
          if (DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT3').AsCurrency > 0 )or( DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU3').AsCurrency > 0 ) then
          begin
            SEGMENT := 'PAI';
            ID := '28';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := '2AA';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';
            TempList.Add(Text(6));  
            //어음 원화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT3').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AE';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT3').AsString;
              Items[2] := 'KRW';
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            //어음 외화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU3').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AD';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU3').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTC3').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            Inc(iCount);
          end;
          //외상(매출금/미수금)
          if (DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT4').AsCurrency > 0 )or( DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU4').AsCurrency > 0 ) then
          begin
            SEGMENT := 'PAI';
            ID := '28';
            COUNT := iCount;
            Items[0] := '';
            Items[1] := '';
            Items[2] := '30';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';
            TempList.Add(Text(6)); 
            //현금 원화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT4').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AE';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMT4').AsString;
              Items[2] := 'KRW';
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            //현금 외화
            if DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU4').AsCurrency > 0 then
            begin
              SEGMENT := 'MOA';
              ID := '38';
              COUNT := iCount;
              Items[0] := '2AD';
              Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTU4').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD1.FieldByName('PAI_AMTC4').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;
            Inc(iCount);
          end;


      end;
      iCount := 1;
      //GIS : 세금계산서 영수:1/청구:18 구분
      if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_GUBUN').AsString) <> '') then
      begin
        SEGMENT := 'GIS';
        ID := '29';
        COUNT := 1;
        Items[0] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_GUBUN').AsString;
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        TempList.Add(Text(4));
      end;

      //FII(AZ) : 내국신용장 개설은행(2AP , 2AH) M
      if (sDocGubun = '2AH') or (sDocGubun = '2AP') then
      begin
        SEGMENT := 'FII';
        ID := '2A';
        COUNT := iCount;
        if sDocGubun ='2AP' then
        begin
          Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANKBUCODE').AsString;
          Items[8] := '';
          Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANKNAMEP').AsString;
          Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANKBUP').AsString;
        end
        else if sDocGubun = '2AH' then
        begin
          Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANK').AsString;
          Items[8] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_ELEC').AsString;
          Items[11] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANKNAME').AsString;
          Items[12] := DMDocumentQry.qryAPPSPCD1.FieldByName('LA_BANKBU').AsString;
        end;
        Items[0] := 'AZ';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[6] := '25';
        Items[7] := 'BOK';
        Items[9] := '';
        Items[10] := '';
        Items[13] := '';
        TempList.Add(Text(14));

        Inc(iCount);
      end;

      //SEQ 세금계산서 공급가액란 공란수 C
      if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('VB_DETAILNO').AsString) <> '') then
      begin
        SEGMENT := 'SEQ';
        ID := '2B';
        COUNT := 1;
        Items[0] := '';
        Items[1] := DMDocumentQry.qryAPPSPCD1.FieldByName('VB_DETAILNO').AsString;
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        TempList.Add(Text(5));
      end;

      //FTX(ACB) 물품수령증명서상의 내국신용장 참조사항C, 세금계산서의 비고C, 상업송장의 항차 등 C 내역표기
      if (sDocGubun <> '2AP')  and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_1').AsString) <> '') then
      begin
        SEGMENT := 'FTX';
        ID := '2C';
        COUNT := 1;
        Items[0] := 'ACB';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_1').AsString;
        Items[6] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_2').AsString;
        Items[7] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_3').AsString;
        Items[8] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_4').AsString;
        Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK1_5').AsString;
        Items[10] := '';
        TempList.Add(Text(11));
      end;

      //FTX(ABS) 물품수령증명서 상의 기타조건
      if (sDocGubun = '2AH') and (Trim(DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_1').AsString) <> '') then
      begin
        SEGMENT := 'FTX';
        ID := '2C';
        COUNT := iCount;
        Items[0] := 'ABS';
        Items[1] := '';
        Items[2] := '';
        Items[3] := '';
        Items[4] := '';
        Items[5] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_1').AsString;
        Items[6] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_2').AsString;
        Items[7] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_3').AsString;
        Items[8] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_4').AsString;
        Items[9] := DMDocumentQry.qryAPPSPCD1.FieldByName('REMARK2_5').AsString;
        Items[10] := '';
        TempList.Add(Text(11));
      end;
//------------------------------------------------------------------------------
// APPSPC_D2 세부정보
//------------------------------------------------------------------------------

      if DMDocumentQry.qryAPPSPCD2.RecordCount > 0 then
      begin
        DMDocumentQry.qryAPPSPCD2.First;
        iCount := 1;
        while not DMDocumentQry.qryAPPSPCD2.Eof do
        begin
          if (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('DOC_GUBUN').AsString) = sDocGubun ) and
             (DMDocumentQry.qryAPPSPCD2.FieldByName('SEQ').AsInteger = iSEQ) then
          begin
            //LIN 물품수령증명서의 품목번호M 또는 세금계산서M, 상업송장의 품목 일련번호M를 기재하는 항목
            SEGMENT := 'LIN';
            ID := '2D';
            COUNT := 1;
            Items[0] := DMDocumentQry.qryAPPSPCD2.FieldByName('LINE_NO').AsString;
            Items[1] := '';
            Items[2] := '';
            Items[3] := '';
            Items[4] := '';
            Items[5] := '';
            Items[6] := '';
            Items[7] := '';
            Items[8] := '';
            TempList.Add(Text(9));

            //PIA(1) : HS부호 물품수령증명서 C
            if (sDocGubun = '2AH') and (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('HS_NO').AsString) <> '') then
            begin
              SEGMENT := 'PIA';
              ID := '30';
              COUNT := 1;
              Items[0] := '1';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('HS_NO').AsString;
              Items[2] := '';
              Items[3] := '';
              Items[4] := '';
              Items[5] := '';
              Items[6] := '';
              Items[7] := '';
              Items[8] := '';
              Items[9] := '';
              Items[10] := '';
              Items[11] := '';
              Items[12] := '';
              Items[13] := '';
              Items[14] := '';
              Items[15] := '';
              Items[16] := '';
              Items[17] := '';
              Items[18] := '';
              Items[19] := '';
              Items[20] := '';
              TempList.Add(Text(21));
            end;

            //DTM(35) 세금계산서 라인별 공급일자
            if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('DE_DATE').AsString) <> '' ) then
            begin
              SEGMENT := 'DTM';
              ID := '31';
              COUNT := 1;
              Items[0] := '35';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('DE_DATE').AsString;
              Items[2] := '102';
              TempList.Add(Text(3));
            end;

            iCount := 1;
            //IMD(1AA) 물품수령증명서, 세금계산서, 상업송장의 품명 C
            for i:= 1 to 4 do
            begin
              if (sDocGubun <> '2AP') and (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('IMD_CODE'+IntToStr(i)).AsString) <> '' ) then
              begin
                SEGMENT := 'IMD';
                ID := '32';
                COUNT := iCount;
                Items[0] := '';
                Items[1] := '';
                Items[2] := '1AA';
                Items[3] := '';
                Items[4] := '';
                Items[5] := DMDocumentQry.qryAPPSPCD2.FieldByName('IMD_CODE'+IntToStr(i)).AsString;
                Items[6] := '';
                TempList.Add(Text(7));
                Inc(iCount);
              end;
            end;

            iCount := 1;
            //FTX(AAA) 물품수령증명서, 세금계산서, 상업송장의 규격 C
            for i:= 1 to 2 do
            begin
              if (sDocGubun <> '2AP') and (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5)-4)).AsString) <> '' ) then
              begin
                SEGMENT := 'FTX';
                ID := '33';
                COUNT := iCount;
                Items[0] := 'AAA';
                Items[1] := '';
                Items[2] := '';
                Items[3] := '';
                Items[4] := '';
                Items[5] := DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5)-4)).AsString;
                Items[6] := DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5)-3)).AsString;
                Items[7] := DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5)-2)).AsString;
                Items[8] := DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5)-1)).AsString;
                Items[9] := DMDocumentQry.qryAPPSPCD2.FieldByName('SIZE'+IntToStr((i*5))).AsString;
                Items[10] := '';
                TempList.Add(Text(11));
                Inc(iCount);
              end;
            end;

            //FTX(ACB) 세금계산서 라인별 비고(참조사항) C
            if (sDocGubun = '2AJ') and (Trim(DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO1').AsString) <> '') then
            begin
              SEGMENT := 'FTX';
              ID := '33';
              COUNT := 1;
              Items[0] := 'ACB';
              Items[1] := '';
              Items[2] := '';
              Items[3] := '';
              Items[4] := '';
              Items[5] := DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO1').AsString;
              Items[6] := DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO2').AsString;
              Items[7] := DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO3').AsString;
              Items[8] := DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO4').AsString;
              Items[9] := DMDocumentQry.qryAPPSPCD2.FieldByName('BIGO5').AsString;
              Items[10] := '';
              TempList.Add(Text(11));
            end;

            //QTY(1) 물품의 수량을 기재 C
            if  (sDocGubun <> '2AP') and (DMDocumentQry.qryAPPSPCD2.FieldByName('QTY').AsCurrency > 0) then
            begin
              SEGMENT := 'QTY';
              ID := '34';
              COUNT := 1;
              Items[0] := '1';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('QTY').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('QTYC').AsString;

              TempList.Add(Text(3));
            end;

            //QTY(3) 수량소계 C
            if ((sDocGubun <> '2AH') or (sDocGubun <> '2AJ')) and (DMDocumentQry.qryAPPSPCD2.FieldByName('TOTQTY').AsCurrency > 0) then
            begin
              SEGMENT := 'QTY';
              ID := '34';
              COUNT := 1;
              Items[0] := '3';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('TOTQTY').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('TOTQTYC').AsString;
              TempList.Add(Text(3));
            end;

            //PRI(CAL) 규격별 단가.
            if (sDocGubun <> '2AP') and (DMDocumentQry.qryAPPSPCD2.FieldByName('PRICE').AsCurrency > 0) then
            begin
              SEGMENT := 'PRI';
              ID := '35';
              COUNT := 1;
              Items[0] := 'CAL';
              Items[1] :=  DMDocumentQry.qryAPPSPCD2.FieldByName('PRICE').AsString;
              Items[2] := 'PE';
              Items[3] := 'CUP';
              if DMDocumentQry.qryappspcd2.FieldByName('PRICE_G').AsCurrency > 0 then
                Items[4] := DMDocumentQry.qryAPPSPCD2.FieldByName('PRICE_G').AsString
              else
                Items[4] := '';
              Items[5] := DMDocumentQry.qryAPPSPCD2.FieldByName('PRICEC').AsString;
              Items[6] := '';
              TempList.Add(Text(7));
            end;

            //MOA(203) 세금계산서 라인별 공급가액
            if ((sDocGubun = '2AJ') OR (sDocGubun = '2AH')) and (DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_AMT').AsCurrency > 0 ) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '203';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_AMT').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_AMTC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //MOA(124) 세금계산서 라인별 세액
            if (sDocGubun = '2AJ') and (DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TAX').AsCurrency > 0) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '124';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TAX').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TAXC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //MOA(261) 세금계산서, 상업송장 라인별 외화공급가액 C
            if ((sDocGubun = '2AJ') OR (sDocGubun = '1BW')) and (DMDocumentQry.qryAPPSPCD2.FieldByName('VB_AMT').AsCurrency > 0) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '261';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_AMT').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_AMTC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //MOA(17) 세금계산서 공급가액 소계 C
            if ((sDocGubun = '2AJ') OR (sDocGubun = '2AH')) and (DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_TOTAMT').AsCurrency > 0) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '17';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_TOTAMT').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('SUP_TOTAMTC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //MOA(168) 세금계산서 세액 소계 C
            if (sDocGubun = '2AJ') and (DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTTAX').AsCurrency > 0) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '168';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTTAX').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTTAXC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //MOA(289) 세금계산서 외화공급가액 소계 C
            if (sDocGubun = '2AJ') and (DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTAMT').AsCurrency > 0) then
            begin
              SEGMENT := 'MOA';
              ID := '36';
              COUNT := 1;
              Items[0] := '289';
              Items[1] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTAMT').AsString;
              Items[2] := DMDocumentQry.qryAPPSPCD2.FieldByName('VB_TOTAMTC').AsString;
              Items[3] := '';
              Items[4] := '';
              TempList.Add(Text(5));
            end;

            //CUX 세금계산서 환율
            if (sDocGubun = '2AJ') and (DMDocumentQry.qryAPPSPCD2.FieldByName('CUX_RATE').AsCurrency > 0) then
            begin
              SEGMENT := 'CUX';
              ID := '37';
              COUNT := 1;
              Items[0] := '';
              Items[1] := '';
              Items[2] := '';
              Items[3] := '';
              Items[4] := '';
              Items[5] := '';
              Items[6] := '';
              Items[7] := '';
              Items[8] := DMDocumentQry.qryAPPSPCD2.FieldByName('CUX_RATE').AsString;
              Items[9] := '';
              TempList.Add(Text(10));
            end;

          end;
        DMDocumentQry.qryAPPSPCD2.Next;
        end;
      end;
      DMDocumentQry.qryAPPSPCD1.Next;
      end;
    TempList.Add(Footer);
    end;
    Result := TempList.Text;
  finally
    SPC_FACT.Free;
    TempList.Free;
  end;
end;

function PAYORD(MAINT_NO, RecvCode : String):String;
var
  EDI_PAYORD : TEDIFACT;
  TempList : TStringList;
  i : Integer;
  TempStr : String;
begin
  EDI_PAYORD := TEDIFACT.Create;
  TempList := TStringList.Create;
  try
    with EDI_PAYORD, DMDocumentQry.qryPAYORD do
    begin
      if DMDocumentQry.OPEN_PAYORD(MAINT_NO) <= 0 Then exit;
      //------------------------------------------------------------------------------
      // 헤더
      //------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'PAYORD';
      DOCNO := MAINT_NO;
      TempList.Add(Header('1'));

      //----DTM-----------------------------------------------------------------
      SEGMENT := 'BGM';
      ID := '10';
      COUNT := 1;
      Items[0] := '450';
      Items[1] := MAINT_NO;
      Items[2] := FieldbyName('MESSAGE1').AsString;
      Items[3] := FieldbyName('MESSAGE2').AsString;
      TempList.Add(Text(4));

      // 2AA 신청일자
      SEGMENT := 'DTM';
      ID := '11';
      COUNT := 1;
      Items[0] := '2AA';
      Items[1] := RightStr( FieldByName('APP_DATE').AsString, 6 );
      Items[2] := '101';
      TempList.Add(Text(3));

      // 203 이체희망일자
      SEGMENT := 'DTM';
      ID := '11';
      COUNT := 2;
      Items[0] := '203';
      Items[1] := RightStr( FieldByName('EXE_DATE').AsString, 6 );
      Items[2] := '101';
      TempList.Add(Text(3));

      // 151 수입예정일 (C)
      SEGMENT := 'DTM';
      ID := '11';
      COUNT := 3;
      Items[0] := '151';
      Items[1] := FieldByName('IMP_DATE').AsString;
      Items[2] := '102';
      TempList.Add(Text(3));

      //----RFF-----------------------------------------------------------------
      // HS코드
      SEGMENT := 'RFF';
      ID := '12';
      COUNT := 1;
      Items[0] := 'HS';
      Items[1] := FieldByName('HS_CODE').AsString;
      IF Trim(Items[1]) <> '' Then
        TempList.Add(Text(3))
      else
        ItemClear;

      // 기타번호
      SEGMENT := 'RFF';
      ID := '12';
      COUNT := 2;
      Items[0] := 'ACD';
      Items[1] := FieldByName('ADREFNO').AsString;
      IF Trim(Items[1]) <> '' Then
        TempList.Add(Text(3))
      else
        ItemClear;

      //----BUS-----------------------------------------------------------------
      // 거래형태 구분
      SEGMENT := 'BUS';
      ID := '13';
      COUNT := 1;
      Items[0] := '2BA';
      Items[1] := FieldByName('BF_CODE').AsString;
      TempList.Add(Text(4));

      // 지급지시서 용도
      SEGMENT := 'BUS';
      ID := '13';
      COUNT := 2;
      Items[2] := FieldByName('BUS_CD').AsString;
      // BUS_CD 가 [2AJ]만 입력
      if Trim(UpperCase(Items[2])) = '2AJ' Then
        Items[3] := FieldByName('BUS_CD1').AsString;
      TempList.Add(Text(4));

      //----FTX-----------------------------------------------------------------
      // 송금내역
      SEGMENT := 'FTX';
      ID := '14';
      COUNT := 1;
      Items[0] := 'PMD';
      Items[1] := FieldByName('PAYDET1').AsString;
      Items[2] := FieldByName('PAYDET2').AsString;
      Items[3] := FieldByName('PAYDET3').AsString;
      Items[4] := FieldByName('PAYDET4').AsString;
      Items[5] := FieldByName('PAYDET5').AsString;
      TempList.Add(Text);

      //----PAI-----------------------------------------------------------------
      // 송금방법
      SEGMENT := 'PAI';
      ID := '15';
      COUNT := 1;
      Items[0] := FieldByName('SND_CD').AsString;
      if trim(Items[0]) = '' Then
        itemClear
      else
        TempList.Add(Text(1));

      //----FCA-----------------------------------------------------------------
      // 부가수수료부담자
      SEGMENT := 'FCA';
      ID := '16';
      COUNT := 1;
      Items[0] := FieldByName('CHARGE_TO').AsString;
      TempList.Add(Text(1));

      //----MOA-----------------------------------------------------------------
      // 원화계좌 지급금액
      SEGMENT := 'MOA';
      ID := '17';
      COUNT := 1;
      Items[0] := '9';
      Items[1] := FormatFloat('0.###', FieldByName('PAY_AMT').AsFloat);
      Items[2] := FieldByName('PAY_AMTC').AsString;
      TempList.Add(Text(3));

      // 외화계좌 지급금액
      SEGMENT := 'MOA';
      ID := '17';
      COUNT := 2;
      Items[0] := '2AD';
      Items[1] := FormatFloat('0.###', FieldByName('PAY_AMT2').AsFloat);
      Items[2] := FieldByName('PAY_AMT2C').AsString;
      if (Items[1] = '0') or ((Items[1] = '')) Then
        ItemClear
      else
        TempList.Add(Text(3));

      //----FII-----------------------------------------------------------------
      // 지급의뢰인은행(외화원금계좌)
      SEGMENT := 'FII';
      ID := '18';
      COUNT := 1;
      Items[0] := 'OR';
      Items[1] := '';
      // 계좌번호 3192
      // 계좌주 3192
      Items[2] := FieldByName('OD_ACCNT1').AsString;
      Items[3] := FieldByName('OD_ACCNT2').AsString;
      // 통화코드 6345
      Items[4] := FieldByName('OD_CURR').AsString;
      // 지급의뢰인은행코드 3433
      Items[5] := FieldByName('OD_BANK').AsString;
      Items[6] := '25';
      // 지급의뢰인은행의 관리기관
      Items[7] := 'FTC';
      // 3434 NOT USED
      Items[8] := '';
      // 1131 NOT USED
      Items[9] := '';
      // 3055 NOT USED
      Items[10] := '';
      // 지급의뢰인 은행명 3432
      Items[11] := Trim(FieldByName('OD_BANK1').AsString)+Trim(FieldByName('OD_BANK2').AsString);
      // 지급의뢰인 지점명 3436
      Items[12] := Trim(FieldByName('OD_NAME1').AsString)+Trim(FieldByName('OD_NAME2').AsString);
      // 지급의뢰인은행의 국가코드 3207
      Items[13] := Trim(FieldByName('OD_Nation').AsString);
      TempList.Add(Text(14));

      // 수익자은행
      SEGMENT := 'FII';
      ID := '18';
      COUNT := 2;
      Items[0] := 'BF';
      Items[1] := '';
      // 계좌번호 3192
      // 계좌주 3192
      Items[2] := FieldByName('BN_ACCNT1').AsString;
      Items[3] := FieldByName('BN_ACCNT2').AsString;
      // 통화코드 6345
      Items[4] := FieldByName('BN_CURR').AsString;
      // 수익자은행코드 3433
      Items[5] := FieldByName('BN_BANK').AsString;
      Items[6] := '25';
      // 수익자은행의 관리기관
      Items[7] := 'FTC';
      // 3434 NOT USED
      Items[8] := '';
      // 1131 NOT USED
      Items[9] := '';
      // 3055 NOT USED
      Items[10] := '';
      // 수익자 은행명 3432
      Items[11] := Trim(FieldByName('BN_BANK1').AsString)+Trim(FieldByName('BN_BANK2').AsString);
      // 수익자 지점명 3436
      Items[12] := Trim(FieldByName('BN_NAME1').AsString)+Trim(FieldByName('BN_NAME2').AsString);
      // 수익자은행의 국가코드 3207
      Items[13] := Trim(FieldByName('BN_Nation').AsString);
      TempList.Add(Text(14));

      // 중간경유은행
      SEGMENT := 'FII';
      ID := '18';
      COUNT := 3;
      Items[0] := 'I1';
      Items[1] := '';
      // 계좌번호 3192
      // 계좌주 3192
      Items[2] := FieldByName('IT_ACCNT1').AsString;
      Items[3] := FieldByName('IT_ACCNT2').AsString;
      // 통화코드 6345
      Items[4] := FieldByName('IT_CURR').AsString;
      // 중간경유은행코드 3433
      Items[5] := FieldByName('IT_BANK').AsString;
      Items[6] := '25';
      // 중간경유은행의 관리기관
      Items[7] := 'FTC';
      // 3434 NOT USED
      Items[8] := '';
      // 1131 NOT USED
      Items[9] := '';
      // 3055 NOT USED
      Items[10] := '';
      // 중간경유 은행명 3432
      Items[11] := Trim(FieldByName('IT_BANK1').AsString)+Trim(FieldByName('IT_BANK2').AsString);
      // 중간경유 지점명 3436
      Items[12] := Trim(FieldByName('IT_NAME1').AsString)+Trim(FieldByName('IT_NAME2').AsString);
      // 중간경유은행의 국가코드 3207
      Items[13] := Trim(FieldByName('IT_Nation').AsString);
      IF Trim(Items[5]) = '' Then
        ItemClear
      else
        TempList.Add(Text(14));

      // 지급의뢰인(원화원금계좌)
      SEGMENT := 'FII';
      ID := '18';
      COUNT := 4;
      Items[0] := 'EC';
      Items[1] := '';
      // 계좌번호 3192
      // 계좌주 3192
      Items[2] := FieldByName('EC_ACCNT1').AsString;
      Items[3] := FieldByName('EC_ACCNT2').AsString;
      // 통화코드 6345
      Items[4] := 'KRW';
      // 중간경유은행코드 3433
      Items[5] := FieldByName('EC_BANK').AsString;
      Items[6] := '25';
      // 중간경유은행의 관리기관
      Items[7] := 'FTC';
      // 3434 NOT USED
      Items[8] := '';
      // 1131 NOT USED
      Items[9] := '';
      // 3055 NOT USED
      Items[10] := '';
      // 중간경유 은행명 3432
      Items[11] := Trim(FieldByName('EC_BANK1').AsString)+Trim(FieldByName('EC_BANK2').AsString);
      // 중간경유 지점명 3436
      Items[12] := Trim(FieldByName('EC_NAME1').AsString)+Trim(FieldByName('EC_NAME2').AsString);
      // 중간경유은행의 국가코드 3207
      Items[13] := Trim(FieldByName('EC_Nation').AsString);
      IF Trim(Items[5]) = '' Then
        ItemClear
      else
        TempList.Add(Text(14));

      //----NAD-----------------------------------------------------------------
      // 지급의뢰인
      SEGMENT := 'NAD';
      ID := '19';
      COUNT := 1;
      Items[0] := 'OY';
      Items[1] := '';
      // 지급의뢰인
      Items[2] := FieldByName('APP_NAME1').AsString;
      Items[3] := FieldByName('APP_NAME2').AsString;
      Items[4] := FieldByName('APP_NAME3').AsString;
      Items[5] := FieldByName('APP_STR1').AsString;
      Items[6] := '';
      Items[7] := '';
      // 국가 3027 NOT USED
      Items[8] := '';
      TempList.Add(Text(9));

      // 지급의뢰인 전화번호
      SEGMENT := 'COM';
      ID := '20';
      COUNT := 1;
      Items[0] := FieldByName('APP_TELE').AsString;
      Items[1] := 'TE';
      IF Trim(Items[0]) = '' Then
        ItemClear
      else
        TempList.Add(Text(2));

      // 수익자
      SEGMENT := 'NAD';
      ID := '19';
      COUNT := 2;
      Items[0] := 'BE';
      Items[1] := '';
      Items[2] := FieldByName('BEN_NAME1').AsString;
      Items[3] := FieldByName('BEN_NAME2').AsString;
      Items[4] := FieldByName('BEN_NAME3').AsString;
      Items[5] := FieldByName('BEN_STR1').AsString;
      Items[6] := FieldByName('BEN_STR2').AsString;
      Items[7] := FieldByName('BEN_STR3').AsString;
      // 국가 3027
      Items[8] := FieldByName('BEN_NATION').AsString;
      TempList.Add(Text(9));
      // 수익자 전화번호
      SEGMENT := 'COM';
      ID := '20';
      COUNT := 1;
      Items[0] := FieldByName('BEN_TELE').AsString;
      Items[1] := 'TE';
      IF Trim(Items[0]) = '' Then
        ItemClear
      else
        TempList.Add(Text(2));

      // 전자서명
      SEGMENT := 'NAD';
      ID := '19';
      COUNT := 3;
      Items[0] := 'AX';
      Items[1] := '';
      Items[2] := FieldByName('SND_NAME1').AsString;
      Items[3] := FieldByName('SND_NAME2').AsString;
      Items[4] := FieldByName('SND_NAME3').AsString;
      TempList.Add(Text(9));

      //----INP-----------------------------------------------------------------
      TempStr := Trim(FieldByName('REMARK1').AsString)+
                 Trim(FieldByName('REMARK2').AsString)+
                 Trim(FieldByName('REMARK3').AsString)+
                 Trim(FieldByName('REMARK4').AsString)+
                 Trim(FieldByName('REMARK5').AsString);
      IF Trim(TempStr) <> '' Then
      begin
        SEGMENT := 'INP';
        ID := '1A';
        COUNT := 1;
        Items[0] := 'OY';
        Items[1] := 'OR';
        TempList.Add(Text(2));

        SEGMENT := 'FTX';
        ID := '21';
        COUNT := 1;
        Items[0] := 'ACB';
        Items[1] := Trim(FieldByName('REMARK1').AsString);
        Items[2] := Trim(FieldByName('REMARK2').AsString);
        Items[3] := Trim(FieldByName('REMARK3').AsString);
        Items[4] := Trim(FieldByName('REMARK4').AsString);
        Items[5] := Trim(FieldByName('REMARK5').AsString);
        TempList.Add(Text);
      end;

      //----DOC-----------------------------------------------------------------
      // 입금관련서류
      IF Trim(FieldByName('DOC_GU').AsString) <> '' Then
      begin
        SEGMENT := 'DOC';
        ID := '1B';
        COUNT := 1;
        Items[0] := FieldByName('DOC_GU').AsString;
        Items[1] := FieldByName('DOC_NO1').AsString;
        TempList.Add(Text(3));

        IF Trim(FieldByName('DOC_DTE1').AsString) <> '' then
        begin
          SEGMENT := 'DTM';
          ID := '22';
          COUNT := 1;
          Items[0] := '171';
          Items[1] := RightStr(FieldByName('DOC_DTE1').AsString, 6);
          Items[2] := '101';
          TempList.Add(Text(3));
        end;
      end;
      TempList.Add(Footer);
    end;
    Result := TempList.Text;
  finally
    TempList.Free;
    EDI_PAYORD.Free;
    DMDocumentQry.qryPAYORD.Close;
  end;
end;

function APPCOP(MAINT_NO, RecvCode : String):String;
var
  EDI_APPCOP : TEDIFACT;
  TempList, FTX_TMP : TStringList;
  i, nCount : Integer;
  TempStr : String;

begin
  EDI_APPCOP := TEDIFACT.Create;
  TempList := TStringList.Create;
  FTX_TMP := TStringList.Create;
  try
    with EDI_APPCOP, DMDocumentQry do
    begin
      if OPEN_APPCOP(MAINT_NO) <= 0 Then exit;
      //------------------------------------------------------------------------------
      // 헤더
      //------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'PAYORD';
      DOCNO := MAINT_NO;
      TempList.Add(Header('1'));

      SEGMENT := 'BGM';
      ID := '10';
      COUNT := 1;
      Items[0] := qryAPPCOP.FieldByName('MESSAGE1').AsString;
      Items[1] := MAINT_NO;
      Items[2] := qryAPPCOP.FieldbyName('MESSAGE2').AsString;
      Items[3] := qryAPPCOP.FieldbyName('MESSAGE3').AsString;
      TempList.Add(Text(4));

      //------------------------------------------------------------------------------
      // RFF 계약서번호
      //------------------------------------------------------------------------------
      SEGMENT := 'RFF';
      ID := '11';
      COUNT := 1;
      Items[0] := 'CT';
      Items[1] := qryAPPCOP.FieldByName('CT_NO').AsString;
      TempList.Add(Text(2));

      if Trim(qryAPPCOP.FieldByName('AFY_NO').AsString) <> '' Then
      begin
        SEGMENT := 'RFF';
        ID := '11';
        COUNT := 2;
        Items[0] := 'AFY';
        Items[1] := qryAPPCOP.FieldByName('AFY_NO').AsString;
        TempList.Add(Text(2));
      end;

      //------------------------------------------------------------------------------
      // NAD
      //------------------------------------------------------------------------------
      // 신청인
      SEGMENT := 'NAD';
      ID := '12';
      COUNT := 1;
      Items[0] := 'MS';
      Items[1] := qryAPPCOP.FieldByName('MS_SAUP').AsString;
      Items[2] := '1AB';
      Items[3] := qryAPPCOP.FieldByName('MS_NAME1').AsString;
      Items[4] := qryAPPCOP.FieldByName('MS_NAME2').AsString;
      Items[5] := qryAPPCOP.FieldByName('MS_NAME3').AsString;
      Items[6] := qryAPPCOP.FieldByName('MS_ADDR1').AsString;
      Items[7] := qryAPPCOP.FieldByName('MS_ADDR2').AsString;
      Items[8] := qryAPPCOP.FieldByName('MS_ADDR3').AsString;
      TempList.Add(Text(9));

      // 지급상대인
      SEGMENT := 'NAD';
      ID := '12';
      COUNT := 2;
      Items[0] := 'SE';
      Items[1] := '';
      Items[2] := '';
      Items[3] := qryAPPCOP.FieldByName('SE_NAME1').AsString;
      Items[4] := qryAPPCOP.FieldByName('SE_NAME2').AsString;
      Items[5] := qryAPPCOP.FieldByName('SE_NAME3').AsString;
      TempList.Add(Text(9));

      // 신청인 전자서명
      SEGMENT := 'NAD';
      ID := '12';
      COUNT := 3;
      Items[0] := 'AX';
      Items[1] := '';
      Items[2] := '';
      Items[3] := qryAPPCOP.FieldByName('MS_NAME1').AsString;
      Items[4] := qryAPPCOP.FieldByName('MS_NAME2').AsString;
      Items[5] := qryAPPCOP.FieldByName('AX_NAME3').AsString;
      TempList.Add(Text(9));

      //------------------------------------------------------------------------------
      // COM
      //------------------------------------------------------------------------------
      // 신청인 전화번호
      SEGMENT := 'COM';
      ID := '13';
      COUNT := 1;
      Items[0] := qryAPPCOP.FieldByName('MS_TEL').AsString;
      Items[1] := 'TE';
      TempList.Add(Text(2));

      //------------------------------------------------------------------------------
      // MOA
      //------------------------------------------------------------------------------
      // 신청금액
      SEGMENT := 'MOA';
      ID := '14';
      COUNT := 1;
      Items[0] := '9';
      Items[1] := FormatFloat('0.####', qryAPPCOP.FieldByName('AMT').AsFloat);
      Items[2] := UpperCase( qryAPPCOP.FieldByName('AMT').AsString );
      TempList.Add(Text(3));

      //------------------------------------------------------------------------------
      // FTX 지급사유
      //------------------------------------------------------------------------------
      IF qryAPPCOP.FieldByName('BUS_G').AsString = 'ACD' Then
      begin
        FTX_TMP.Text := qryAPPCOP.FieldByName('REMARK1').AsString;
        i := 0;
        nCount := 1;
        while i <= FTX_TMP.Count-1 do
        begin
          SEGMENT := 'FTX';
          ID := '15';
          COUNT := nCount;
          Items[0] := 'ACD';
          Items[1] := FTX_TMP.Strings[i];
          Inc(i);
          Items[2] := FTX_TMP.Strings[i];
          Inc(i);
          Items[3] := FTX_TMP.Strings[i];
          Inc(i);
          Items[4] := FTX_TMP.Strings[i];
          Inc(i);
          Items[5] := FTX_TMP.Strings[i];
          Inc(i);

          TempList.Add(Text(6));
          inc(nCount);
        end;
      end
      else
      IF qryAPPCOP.FieldByName('BUS_G').AsString = 'ACB' Then
      begin
        qryAPPCOP_D.First;
        while not qryAPPCOP_D.Eof do
        begin
          SEGMENT := 'FTX';
          ID := '15';
          COUNT := qryAPPCOP_D.RecNo;
          Items[0] := 'ACB';
          Items[1] := qryAPPCOP_D.FieldByName('IMP_NO').AsString+'/'+qryAPPCOP_D.FieldByName('BANK_CD').AsString;
          Items[2] := qryAPPCOP_D.FieldByName('D_AMTC').AsString;
          Items[3] := FormatFloat('0.####', qryAPPCOP_D.FieldByName('D_AMT').AsFloat);
          Items[4] := qryAPPCOP_D.FieldByName('TERMS').AsString;
          Items[5] := qryAPPCOP_D.FieldByName('IMP_CD').AsString+'/';
          TempList.Add(Text(6));
          qryAPPCOP_D.Next;
        end;
      end;
      IF qryAPPCOP.FieldByName('BUS_G').AsString = 'ACE' Then
      begin
        Result := '';
        raise Exception.Create('중계무역방식은 KIS에 문의해주세요');
      end;

      //------------------------------------------------------------------------------
      // IMD
      //------------------------------------------------------------------------------
      nCount := 1;
      for i := 1 to 5 do
      begin
        if Trim(qryAPPCOP.FieldByName('GOODS'+IntToStr(i)).AsString) <> '' then
        begin
          SEGMENT := 'IMD';
          ID := '16';
          COUNT := nCount;
          Items[0] := '1AA';
          Items[1] := qryAPPCOP.FieldByName('GOODS'+IntToStr(i)).AsString;
          TempList.Add(Text(2));
          inc(nCount);
        end;
      end;

      //------------------------------------------------------------------------------
      // PIA
      //------------------------------------------------------------------------------
      if Trim(qryAPPCOP.FieldByName('HS_NO').AsString) <> '' then
      begin
        SEGMENT := 'PIA';
        ID := '17';
        COUNT := 1;
        Items[0] := '1';
        Items[1] := qryAPPCOP.FieldByName('HS_NO').AsString;
        TempList.Add(Text(2));
      end;

      //------------------------------------------------------------------------------
      // BUS
      //------------------------------------------------------------------------------
      SEGMENT := 'BUS';
      ID := '18';
      COUNT := 1;
      Items[0] := '2AD';
      Items[1] := qryAPPCOP.FieldByName('BUS_G').AsString;
      Items[2] := qryAPPCOP.FieldByName('BUS_D').AsString;
      TempList.Add(Text(3));

      //------------------------------------------------------------------------------
      // DTM
      //------------------------------------------------------------------------------
      nCount := 2;
      //결제기간(당초)
      SEGMENT := 'DTM';
      ID := '19';
      COUNT := 1;
      Items[0] := '13';
      Items[1] := RightStr(qryAPPCOP.FieldByName('OTRM_DATE').AsString, 6);
      Items[2] := '101';
      TempList.Add(Text(3));

      //결제기간(변경기간)
      SEGMENT := 'DTM';
      ID := '19';
      COUNT := nCount;
      Items[0] := '14';
      Items[1] := RightStr(qryAPPCOP.FieldByName('NTRM_DATE').AsString, 6);
      Items[2] := '101';
      if Trim(Items[1]) <> '' then
      begin
        TempList.Add(Text(3));
        inc(nCount); 
      end
      else
        ItemClear;

      // 신청일자
      SEGMENT := 'DTM';
      ID := '19';
      COUNT := nCount;
      Items[0] := '182';
      Items[1] := RightStr(qryAPPCOP.FieldByName('OAPP_DATE').AsString, 6);
      Items[2] := '101';
      if Trim(Items[1]) <> '' then
      begin
        TempList.Add(Text(3));
        inc(nCount); 
      end
      else
        ItemClear;

      // 변경신청일자
      SEGMENT := 'DTM';
      ID := '19';
      COUNT := nCount;
      Items[0] := '1AC';
      Items[1] := RightStr(qryAPPCOP.FieldByName('NAPP_DATE').AsString, 6);
      Items[2] := '101';
      if Trim(Items[1]) <> '' then
      begin
        TempList.Add(Text(3));
        inc(nCount); 
      end
      else
        ItemClear;

      // 확인일자
      SEGMENT := 'DTM';
      ID := '19';
      COUNT := nCount;
      Items[0] := '55';
      Items[1] := RightStr(qryAPPCOP.FieldByName('OCFM_DATE').AsString, 6);
      Items[2] := '101';
      if Trim(Items[1]) <> '' then
      begin
        TempList.Add(Text(3));
        inc(nCount); 
      end
      else
        ItemClear;
      //------------------------------------------------------------------------------
      // END
      //-----------------------------------------------------------------------------
      TempList.Add(Footer);
    end;
    Result := TempList.Text;
  finally
    FTX_TMP.Free;
    TempList.Free;
    EDI_APPCOP.Free;
    DMDocumentQry.qryAPPCOP.Close;
    if DMDocumentQry.qryAPPCOP_D.Active Then
      DMDocumentQry.qryAPPCOP_D.Close;
  end;
end;

function APPRMI(MAINT_NO, RecvCode : String):String;
var
  EDI : TEDIFACT;
  TempList : TStringList;
  TEMP_STR_LIST : TStringList;
begin
  EDI := TEDIFACT.Create;
  TempList := TStringList.Create;
  TEMP_STR_LIST := TStringList.Create;
  
  DMDocumentQry.OPEN_APPRMI(MAINT_NO);
  try
    with EDI, DMDocumentQry do
    begin
      //------------------------------------------------------------------------------
      // 헤더
      //------------------------------------------------------------------------------
      SENDER := RecvCode;
      RECVER := RecvCode;
      DOCCD := 'APPRMI';
      DOCNO := MAINT_NO;
      TempList.Add(Header('1'));

      // BGM
      SEGMENT := 'BGM';
      Items[0] := 'RMI';
      Items[4] := MAINT_NO;
      Items[5] := '9';
      Items[6] := 'AB';
      TempList.Add(Text(7));

      // DTM 신청일자
      SEGMENT := 'DTM';
      COUNT := 1;
      Items[0] := '2AA';
      Items[1] := RightBStr(qryAPPRMI_H.FieldByName('APP_DT').AsString, 6);
      Items[2] := '101';
      TempList.Add(Text(3));

      // DTM 이체희망일자
      SEGMENT := 'DTM';
      Items[0] := '203';
      Items[1] := RightBStr(qryAPPRMI_H.FieldByName('TRN_HOPE_DT').AsString, 6);
      Items[2] := '101';
      TempList.Add(Text(3));

      if qryAPPRMI_H.FieldByName('IMPT_SCHEDULE_DT').AsString <> '' then
      begin
        // DTM 수입예정일
        SEGMENT := 'DTM';
        Items[0] := '151';
        Items[1] := qryAPPRMI_H.FieldByName('IMPT_SCHEDULE_DT').AsString;
        Items[2] := '102';
        TempList.Add(Text(3));
      end;

      if qryAPPRMI_H.FieldByName('ETC_NO').AsString <> '' then
      begin
        // RFF 참조번호
        SEGMENT := 'RFF';
        COUNT := 1;
        items[0] := 'ACD';
        items[1] := qryAPPRMI_H.FieldByName('ETC_NO').AsString;
        TempList.Add(Text(3));
      end;

      // BUS 거래형태 구분
      SEGMENT := 'BUS';
      COUNT := 1;      
      items[0] := '2BA';
      items[1] := qryAPPRMI_H.FieldByName('TRD_CD').AsString;
      TempList.Add(Text(11));

      // BUS 수입송금신청용도
      SEGMENT := 'BUS';
      items[6] := qryAPPRMI_H.FieldByName('IMPT_REMIT_CD').AsString;
      items[7] := qryAPPRMI_H.FieldByName('IMPT_TRAN_FEE_CD').AsString;
      TempList.Add(Text(11));

      // FTX 송금내역
      IF qryAPPRMI_H.FieldByName('REMIT_DESC').AsString <> '' then
      begin
        TEMP_STR_LIST.Text := qryAPPRMI_H.FieldByName('REMIT_DESC').AsString;
        SEGMENT := 'FTX';
        COUNT := 1;
        items[0] := 'PMD';
        if TEMP_STR_LIST.Count > 0 Then
          items[5] := TEMP_STR_LIST.Strings[0];
        if TEMP_STR_LIST.Count > 1 Then
          items[6] := TEMP_STR_LIST.Strings[1];
        if TEMP_STR_LIST.Count > 2 Then
          items[7] := TEMP_STR_LIST.Strings[2];
        if TEMP_STR_LIST.Count > 3 Then
          items[8] := TEMP_STR_LIST.Strings[3];
        TempList.Add(Text(11));
      end;

      IF qryAPPRMI_H.FieldByName('REMIT_CD').AsString <> '' then
      begin
        // PAI 송금방법
        SEGMENT := 'PAI';
        COUNT := 1;
        Items[5] := qryAPPRMI_H.FieldByName('REMIT_CD').AsString;
        TempList.Add(Text(6));
      end;

      // FCA 부가수수료부담자
      SEGMENT := 'FCA';
      COUNT := 1;
      Items[0] := qryAPPRMI_H.FieldByName('ADDED_FEE_TAR').AsString;
      TempList.Add(Text(6));

      // MOA 총 송금액
      SEGMENT := 'MOA';
      COUNT := 1;
      Items[0] := '9';
      Items[1] := qryAPPRMI_H.FieldByName('TOTAL_AMT').AsString;
      Items[2] := qryAPPRMI_H.FieldByName('TOTAL_AMT_UNIT').AsString;
      TempList.Add(Text(5));

      // MOA 외화 계좌 송금액
      IF qryAPPRMI_H.FieldByName('FORE_AMT_UNIT').AsString <> '' then
      begin
        SEGMENT := 'MOA';
        Items[0] := '2AD';
        Items[1] := qryAPPRMI_H.FieldByName('FORE_AMT').AsString;
        Items[2] := qryAPPRMI_H.FieldByName('FORE_AMT_UNIT').AsString;
        TempList.Add(Text(5));
      end;

      COUNT := 1;
      while not qryAPPRMI_FII.Eof do
      begin
        // FII 3194부분은 공백
        SEGMENT := 'FII';
        Items[0] := qryAPPRMI_FII.FieldByName('TAG').AsString;
        Items[2] := qryAPPRMI_FII.FieldByName('ACCNT_NO').AsString;   // 3192 계좌번호
        Items[3] := qryAPPRMI_FII.FieldByName('ACCNT_NM').AsString;   // 3192 계좌주
        Items[4] := qryAPPRMI_FII.FieldByName('ACCNT_UNIT').AsString;   // 6345
        items[5] := qryAPPRMI_FII.FieldByName('BNK_CD').AsString;
        Items[6] := '25';
        Items[11] := qryAPPRMI_FII.FieldByName('BNK_NM1').AsString+qryAPPRMI_FII.FieldByName('BNK_NM2').AsString;
        Items[12] := qryAPPRMI_FII.FieldByName('BNK_NM3').AsString+qryAPPRMI_FII.FieldByName('BNK_NM4').AsString;
        items[13] := qryAPPRMI_FII.FieldByName('BNK_NAT_CD').AsString;
        TempList.Add(Text(14));

        qryAPPRMI_FII.Next;
      end;

      while not qryAPPRMI_NAD.Eof do
      begin
        SEGMENT := 'NAD';
        COUNT := qryAPPRMI_NAD.RecNo;
        Items[0] := qryAPPRMI_NAD.FieldByName('TAG').AsString;
        Items[1] := qryAPPRMI_NAD.FieldByName('BUS_NO').AsString;
        Items[9] := qryAPPRMI_NAD.FieldByName('BUS_NM1').AsString;
        Items[10] := qryAPPRMI_NAD.FieldByName('BUS_NM2').AsString;
        Items[11] := qryAPPRMI_NAD.FieldByName('BUS_NM3').AsString;
        Items[12] := qryAPPRMI_NAD.FieldByName('BUS_ADDR1').AsString;
        Items[13] := qryAPPRMI_NAD.FieldByName('BUS_ADDR2').AsString;
        Items[14] := qryAPPRMI_NAD.FieldByName('BUS_ADDR3').AsString;
        TempList.Add(Text(19));

        IF qryAPPRMI_NAD.FieldByName('TEL_NO').AsString <> '' Then
        begin
          SEGMENT := 'COM';
          COUNT := 1;
          Items[0] := qryAPPRMI_NAD.FieldByName('TEL_NO').AsString;
          Items[1] := 'TE';
          TempList.Add(Text(2));
        end;

        qryAPPRMI_NAD.Next;
      end;

      // NAD 전자서명
      SEGMENT := 'NAD';
      COUNT := 3;
      Items[0] := 'AX';
      Items[1] := qryAPPRMI_H.FieldByName('ONLY_PWD').AsString;

      Items[9] := qryAPPRMI_H.FieldByName('AUTH_NM1').AsString;
      Items[10] := qryAPPRMI_H.FieldByName('AUTH_NM2').AsString;
      Items[11] := qryAPPRMI_H.FieldByName('AUTH_NM3').AsString;

      TempList.Add(Text(19));

      // SEGMENT GROUP 구비서류
      while not qryAPPRMI_RFF.Eof do
      begin
        // RFF
        SEGMENT := 'RFF';
        COUNT := qryAPPRMI_RFF.RecNo;
        Items[0] := '2AX';
        Items[1] :=  qryAPPRMI_RFF.FieldByName('RFF_NO').AsString;
        TempList.Add(Text(3));

        COUNT := 1;
        // MOA  결제금액 또는 계약금액
        SEGMENT := 'MOA';
        Items[0] := '1DA';
        Items[1] := qryAPPRMI_RFF.FieldByName('AMT').AsString;
        Items[2] := qryAPPRMI_RFF.FieldByName('AMT_UNIT').AsString;
        TempList.Add(Text(5));

        // MOA 신청금액(송금금액)
        SEGMENT := 'MOA';
        Items[0] := '12';
        Items[1] := qryAPPRMI_RFF.FieldByName('REMIT_AMT').AsString;
        Items[2] := qryAPPRMI_RFF.FieldByName('REMIT_AMT_UNIT').AsString;
        TempList.Add(Text(5));

        // MOA 과세가격(CIF)
        IF (qryAPPRMI_RFF.FieldByName('CIF_AMT_UNIT').AsString <> '') then
        begin
          SEGMENT := 'MOA';
          Items[0] := '15';
          Items[1] := qryAPPRMI_RFF.FieldByName('CIF_AMT').AsString;
          Items[2] := qryAPPRMI_RFF.FieldByName('CIF_AMT_UNIT').AsString;
          TempList.Add(Text(5));
        end;

        // PIA HS코드
        if (qryAPPRMI_RFF.FieldByName('HS_CD').AsString <> '') then
        begin
          SEGMENT := 'PIA';
          COUNT := 1;
          Items[0] := '1';
          Items[1] := qryAPPRMI_RFF.FieldByName('HS_CD').AsString;
          TempList.Add(Text(21));
        end;

        // IMD 대표물품명
        if (qryAPPRMI_RFF.FieldByName('GOODS_NM').AsString <> '') then
        begin
          SEGMENT := 'IMD';
          COUNT := 1;
          Items[2] := '1AA';
          Items[5] := qryAPPRMI_RFF.FieldByName('GOODS_NM').AsString;
          TempList.Add(Text(7));
        end;

        // BUS 수입용도
        if (qryAPPRMI_RFF.FieldByName('IMPT_CD').AsString <> '') then
        begin
          SEGMENT := 'BUS';
          COUNT := 1;
          Items[0] := '2BT';
          Items[1] := qryAPPRMI_RFF.FieldByName('IMPT_CD').AsString;
          TempList.Add(Text(11));
        end;

        // DTM 수입신고수리일자 또는 계약일자
        if (qryAPPRMI_RFF.FieldByName('CONT_DT').AsString <> '') then
        begin
          SEGMENT := 'DTM';
          COUNT := 1;
          Items[0] := '150';
          Items[1] := qryAPPRMI_RFF.FieldByName('CONT_DT').AsString;
          Items[2] := '102';
          TempList.Add(Text(3));
        end;

        // TOD 가격조건
        if (qryAPPRMI_RFF.FieldByName('TOD_CD').AsString <> '') then
        begin
          SEGMENT := 'TOD';
          COUNT := 1;
          Items[2] := qryAPPRMI_RFF.FieldByName('TOD_CD').AsString;
          Items[5] := qryAPPRMI_RFF.FieldByName('TOD_REMARK').AsString;
          TempList.Add(Text(6));
        end;

        // LOC 적출국(수출국)
        if (qryAPPRMI_RFF.FieldByName('EXPORT_NAT_CD').AsString <> '') then
        begin
          SEGMENT := 'LOC';
          COUNT := 1;
          Items[0] := '116';
          Items[1] := qryAPPRMI_RFF.FieldByName('EXPORT_NAT_CD').AsString;
          Items[2] := '162';
          Items[3] := '5';
          TempList.Add(Text(14));
        end;

        // FTX 품목상세정보 또는 송금개상 상세정보
        IF (qryAPPRMI_RFF.FieldByName('DETAIL_DESC1').AsString <> '') Then
        begin
          SEGMENT := 'FTX';
          COUNT := 1;
          Items[0] := 'ACB';
          Items[5] := qryAPPRMI_RFF.FieldByName('DETAIL_DESC1').AsString;
          Items[6] := qryAPPRMI_RFF.FieldByName('DETAIL_DESC2').AsString;
          Items[7] := qryAPPRMI_RFF.FieldByName('DETAIL_DESC3').AsString;
          Items[8] := qryAPPRMI_RFF.FieldByName('DETAIL_DESC4').AsString;
          Items[9] := qryAPPRMI_RFF.FieldByName('DETAIL_DESC5').AsString;
          TempList.Add(Text(11));
        end;

        // NAD 수출자
        COUNT := 1;
        IF (qryAPPRMI_RFF.FieldByName('EXPORT_OW_NM').AsString <> '') Then
        begin
          SEGMENT := 'NAD';
          Items[0] := 'EX';
          Items[9] := qryAPPRMI_RFF.FieldByName('EXPORT_OW_NM').AsString;
          Items[10] := qryAPPRMI_RFF.FieldByName('EXPORT_OW_ADDR').AsString;
          Items[11] := qryAPPRMI_RFF.FieldByName('EXPORT_OW_NAT_CD').AsString;
          TempList.Add(Text(19));
        end;

        // NAD 수입자
        IF (qryAPPRMI_RFF.FieldByName('IMPORT_OW_NM').AsString <> '') Then
        begin
          SEGMENT := 'NAD';
          Items[0] := 'IM';
          Items[9] := qryAPPRMI_RFF.FieldByName('IMPORT_OW_NM').AsString;
          Items[10] := qryAPPRMI_RFF.FieldByName('IMPORT_OW_ADDR').AsString;
          Items[11] := qryAPPRMI_RFF.FieldByName('IMPORT_OW_NAT_CD').AsString;
          TempList.Add(Text(19));
        end;

        qryAPPRMI_RFF.Next;

      end;
      //------------------------------------------------------------------------------
      // END
      //-----------------------------------------------------------------------------
      TempList.Add(Footer);
    end;

    Result := TempList.Text;
    TempList.SaveToFile('C:\apprmi.txt');
  finally
    TEMP_STR_LIST.Free;
    TempList.Free;
    EDI.Free;
    DMDocumentQry.CLOSE_APPRMI;
  end;
end;

end.

