object DMDocumentQry: TDMDocumentQry
  OldCreateOrder = False
  Left = 845
  Top = 292
  Height = 525
  Width = 629
  object qryAPPPCR_H: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[APP_CODE]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_ADDR1]'
      '      ,[APP_ADDR2]'
      '      ,[APP_ADDR3]'
      '      ,[AX_NAME1]'
      '      ,[AX_NAME2]'
      '      ,[AX_NAME3]'
      '      ,[SUP_CODE]'
      '      ,[SUP_NAME1]'
      '      ,[SUP_NAME2]'
      '      ,[SUP_NAME3]'
      '      ,[SUP_ADDR1]'
      '      ,[SUP_ADDR2]'
      '      ,[SUP_ADDR3]'
      '      ,[ITM_G1]'
      '      ,[ITM_G2]'
      '      ,[ITM_G3]'
      '      ,[TQTY]'
      '      ,[TQTYC]'
      '      ,[TAMT1]'
      '      ,[TAMT1C]'
      '      ,[TAMT2]'
      '      ,[TAMT2C]'
      '      ,[CHG_G]'
      '      ,[C1_AMT]'
      '      ,[C1_C]'
      '      ,[C2_AMT]'
      '      ,[BK_NAME1]'
      '      ,[BK_NAME2]'
      '      ,[PRNO]'
      '      ,[PCrLic_No]'
      '      ,[ChgCd]'
      '      ,[APP_DATE]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_CD]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[F_INTERFACE]'
      '      ,[SRBUHO]'
      '      ,[ITM_GBN]'
      '      ,[ITM_GNAME]'
      '      ,[ISSUE_GBN]'
      '      ,[DOC_GBN]'
      '      ,[DOC_NO]'
      '      ,[BAMT2]'
      '      ,[BAMTC2]'
      '      ,[BAMT3]'
      '      ,[BAMTC3]'
      '      ,[PCRLIC_ISS_NO]'
      '      ,[FIN_CODE]'
      '      ,[FIN_NAME]'
      '      ,[FIN_NAME2]'
      '      ,[NEGO_APPDT]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '      ,[BK_CD]'
      '  FROM [APPPCR_H]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 32
    Top = 16
  end
  object qryAPPPCRD1: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[HS_NO]'
      '      ,[NAME]'
      '      ,[NAME1]'
      '      ,[SIZE]'
      '      ,[SIZE1]'
      '      ,[REMARK]'
      '      ,[QTY]'
      '      ,[QTYC]'
      '      ,[AMT1]'
      '      ,[AMT1C]'
      '      ,[AMT2]'
      '      ,[PRI1]'
      '      ,[PRI2]'
      '      ,[PRI_BASE]'
      '      ,[PRI_BASEC]'
      '      ,[ACHG_G]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[LOC_TYPE]'
      '      ,[NAMESS]'
      '      ,[SIZESS]'
      '      ,[APP_DATE]'
      '      ,[ITM_NUM]'
      '  FROM [APPPCRD1]'
      '  WHERE KEYY = :MAINT_NO'
      'ORDER BY KEYY,SEQ')
    Left = 32
    Top = 64
  end
  object qryAPPPCRD2: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[BAL_CD]'
      '      ,[NAT]'
      '  FROM [APPPCRD2]'
      '  WHERE KEYY = :MAINT_NO')
    Left = 32
    Top = 112
  end
  object qryAPPPCR_TAX: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[DATEE]'
      '      ,[BILL_NO]'
      '      ,[BILL_DATE]'
      '      ,[ITEM_NAME1]'
      '      ,[ITEM_NAME2]'
      '      ,[ITEM_NAME3]'
      '      ,[ITEM_NAME4]'
      '      ,[ITEM_NAME5]'
      '      ,[ITEM_DESC]'
      '      ,[BILL_AMOUNT]'
      '      ,[BILL_AMOUNT_UNIT]'
      '      ,[TAX_AMOUNT]'
      '      ,[TAX_AMOUNT_UNIT]'
      '      ,[QUANTITY]'
      '      ,[QUANTITY_UNIT]'
      '  FROM [APPPCR_TAX]'
      '  WHERE KEYY = :MAINT_NO')
    Left = 32
    Top = 160
  end
  object qryLOCAPP: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      '/****** SSMS'#51032' SelectTopNRows '#47749#47161' '#49828#53356#47549#53944' ******/'
      'SELECT [MAINT_NO]'
      '      ,[DATEE]'
      '      ,[USER_ID]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[BUSINESS]'
      '      ,[OFFERNO1]'
      '      ,[OFFERNO2]'
      '      ,[OFFERNO3]'
      '      ,[OFFERNO4]'
      '      ,[OFFERNO5]'
      '      ,[OFFERNO6]'
      '      ,[OFFERNO7]'
      '      ,[OFFERNO8]'
      '      ,[OFFERNO9]'
      '      ,[OPEN_NO]'
      '      ,[APP_DATE]'
      '      ,[DOC_PRD]'
      '      ,[DELIVERY]'
      '      ,[EXPIRY]'
      '      ,[TRANSPRT]'
      '      ,[GOODDES]'
      '      ,[GOODDES1]'
      '      ,[REMARK]'
      '      ,[REMARK1]'
      '      ,[AP_BANK]'
      '      ,[AP_BANK1]'
      '      ,[AP_BANK2]'
      '      ,[APPLIC]'
      '      ,[APPLIC1]'
      '      ,[APPLIC2]'
      '      ,[APPLIC3]'
      '      ,[BENEFC]'
      '      ,[BENEFC1]'
      '      ,[BENEFC2]'
      '      ,[BENEFC3]'
      '      ,[EXNAME1]'
      '      ,[EXNAME2]'
      '      ,[EXNAME3]'
      '      ,[DOCCOPY1]'
      '      ,[DOCCOPY2]'
      '      ,[DOCCOPY3]'
      '      ,[DOCCOPY4]'
      '      ,[DOCCOPY5]'
      '      ,[DOC_ETC]'
      '      ,[DOC_ETC1]'
      '      ,[LOC_TYPE]'
      '      ,[LOC_AMT]'
      '      ,[LOC_AMTC]'
      '      ,[DOC_DTL]'
      '      ,[DOC_NO]'
      '      ,[DOC_AMT]'
      '      ,[DOC_AMTC]'
      '      ,[LOADDATE]'
      '      ,[EXPDATE]'
      '      ,[IM_NAME]'
      '      ,[IM_NAME1]'
      '      ,[IM_NAME2]'
      '      ,[IM_NAME3]'
      '      ,[DEST]'
      '      ,[ISBANK1]'
      '      ,[ISBANK2]'
      '      ,[PAYMENT]'
      '      ,[EXGOOD]'
      '      ,[EXGOOD1]'
      '      ,[CHKNAME1]'
      '      ,[CHKNAME2]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[PRNO]'
      '      ,[LCNO]'
      '      ,[BSN_HSCODE]'
      '      ,[APPADDR1]'
      '      ,[APPADDR2]'
      '      ,[APPADDR3]'
      '      ,[BNFADDR1]'
      '      ,[BNFADDR2]'
      '      ,[BNFADDR3]'
      '      ,[BNFEMAILID]'
      '      ,[BNFDOMAIN]'
      '      ,[CD_PERP]'
      '      ,[CD_PERM]'
      '  FROM [LOCAPP]'
      '  WHERE MAINT_NO = :MAINT_NO')
    Left = 104
    Top = 16
  end
  object qryAPP7001: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[APP_DATE]'
      '      ,[IN_MATHOD]'
      '      ,[AP_BANK]'
      '      ,[AP_BANK1]'
      '      ,[AP_BANK2]'
      '      ,[AP_BANK3]'
      '      ,[AP_BANK4]'
      '      ,[AP_BANK5]'
      '      ,[AD_BANK]'
      '      ,[AD_BANK1]'
      '      ,[AD_BANK2]'
      '      ,[AD_BANK3]'
      '      ,[AD_BANK4]'
      '      ,[AD_PAY]'
      '      ,[IL_NO1]'
      '      ,[IL_NO2]'
      '      ,[IL_NO3]'
      '      ,[IL_NO4]'
      '      ,[IL_NO5]'
      '      ,[IL_AMT1]'
      '      ,[IL_AMT2]'
      '      ,[IL_AMT3]'
      '      ,[IL_AMT4]'
      '      ,[IL_AMT5]'
      '      ,[IL_CUR1]'
      '      ,[IL_CUR2]'
      '      ,[IL_CUR3]'
      '      ,[IL_CUR4]'
      '      ,[IL_CUR5]'
      '      ,[AD_INFO1]'
      '      ,[AD_INFO2]'
      '      ,[AD_INFO3]'
      '      ,[AD_INFO4]'
      '      ,[AD_INFO5]'
      '      ,[DOC_CD]'
      '      ,[EX_DATE]'
      '      ,[EX_PLACE]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[prno]'
      '      ,[F_INTERFACE]'
      '      ,[IMP_CD1]'
      '      ,[IMP_CD2]'
      '      ,[IMP_CD3]'
      '      ,[IMP_CD4]'
      '      ,[IMP_CD5]'
      '  FROM [APP700_1]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 168
    Top = 16
  end
  object qryAPP7002: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[APPLIC1]'
      '      ,[APPLIC2]'
      '      ,[APPLIC3]'
      '      ,[APPLIC4]'
      '      ,[APPLIC5]'
      '      ,[BENEFC]'
      '      ,[BENEFC1]'
      '      ,[BENEFC2]'
      '      ,[BENEFC3]'
      '      ,[BENEFC4]'
      '      ,[BENEFC5]'
      '      ,[CD_AMT]'
      '      ,[CD_CUR]'
      '      ,[CD_PERP]'
      '      ,[CD_PERM]'
      '      ,[CD_MAX]'
      '      ,[AA_CV1]'
      '      ,[AA_CV2]'
      '      ,[AA_CV3]'
      '      ,[AA_CV4]'
      '      ,[DRAFT1]'
      '      ,[DRAFT2]'
      '      ,[DRAFT3]'
      '      ,[MIX_PAY1]'
      '      ,[MIX_PAY2]'
      '      ,[MIX_PAY3]'
      '      ,[MIX_PAY4]'
      '      ,[DEF_PAY1]'
      '      ,[DEF_PAY2]'
      '      ,[DEF_PAY3]'
      '      ,[DEF_PAY4]'
      '      ,[PSHIP]'
      '      ,[TSHIP]'
      '      ,[LOAD_ON]'
      '      ,[FOR_TRAN]'
      '      ,[LST_DATE]'
      '      ,[SHIP_PD1]'
      '      ,[SHIP_PD2]'
      '      ,[SHIP_PD3]'
      '      ,[SHIP_PD4]'
      '      ,[SHIP_PD5]'
      '      ,[SHIP_PD6]'
      '      ,[DESGOOD_1]'
      '  FROM [APP700_2]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 168
    Top = 64
  end
  object qryAPP7003: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[DOC_380]'
      '      ,[DOC_380_1]'
      '      ,[DOC_705]'
      '      ,[DOC_705_1]'
      '      ,[DOC_705_2]'
      '      ,[DOC_705_3]'
      '      ,[DOC_705_4]'
      '      ,[DOC_740]'
      '      ,[DOC_740_1]'
      '      ,[DOC_740_2]'
      '      ,[DOC_740_3]'
      '      ,[DOC_740_4]'
      '      ,[DOC_530]'
      '      ,[DOC_530_1]'
      '      ,[DOC_530_2]'
      '      ,[DOC_271]'
      '      ,[DOC_271_1]'
      '      ,[DOC_861]'
      '      ,[DOC_2AA]'
      '      ,[DOC_2AA_1]'
      '      ,[ACD_2AA]'
      '      ,[ACD_2AA_1]'
      '      ,[ACD_2AB]'
      '      ,[ACD_2AC]'
      '      ,[ACD_2AD]'
      '      ,[ACD_2AE]'
      '      ,[ACD_2AE_1]'
      '      ,[CHARGE]'
      '      ,[PERIOD]'
      '      ,[CONFIRMM]'
      '      ,[INSTRCT]'
      '      ,[INSTRCT_1]'
      '      ,[EX_NAME1]'
      '      ,[EX_NAME2]'
      '      ,[EX_NAME3]'
      '      ,[EX_ADDR1]'
      '      ,[EX_ADDR2]'
      '      ,[ORIGIN]'
      '      ,[ORIGIN_M]'
      '      ,[PL_TERM]'
      '      ,[TERM_PR]'
      '      ,[TERM_PR_M]'
      '      ,[SRBUHO]'
      '      ,[DOC_705_GUBUN]'
      '      ,[CARRIAGE]'
      '      ,[DOC_760]'
      '      ,[DOC_760_1]'
      '      ,[DOC_760_2]'
      '      ,[DOC_760_3]'
      '      ,[DOC_760_4]'
      '      ,[SUNJUCK_PORT]'
      '      ,[DOCHACK_PORT]'
      '  FROM [APP700_3]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 168
    Top = 112
  end
  object qryAPP7071: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        '        MAINT_NO, MSeq, AMD_NO, MESSAGE1, MESSAGE2, USER_ID, DAT' +
        'EE, APP_DATE, IN_MATHOD, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, ' +
        'AP_BANK4, AP_BANK5, '
      
        '        AD_BANK, AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK4, AD_PAY,' +
        ' IL_NO1, IL_NO2, IL_NO3, IL_NO4, IL_NO5, IL_AMT1, IL_AMT2, IL_AM' +
        'T3, IL_AMT4, IL_AMT5, '
      
        '        IL_CUR1, IL_CUR2, IL_CUR3, IL_CUR4, IL_CUR5, AD_INFO1, A' +
        'D_INFO2, AD_INFO3, AD_INFO4, AD_INFO5, CD_NO, ISS_DATE, EX_DATE,' +
        ' EX_PLACE, CHK1, CHK2, CHK3, prno, '
      '        F_INTERFACE, IMP_CD1, IMP_CD2, IMP_CD3, IMP_CD4, IMP_CD5'
      'FROM APP707_1'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 232
    Top = 16
  end
  object qryAPP7072: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        '             MAINT_NO, MSeq, Amd_No, APPLIC1, APPLIC2, APPLIC3, ' +
        'APPLIC4, APPLIC5, BENEFC, BENEFC1, BENEFC2, BENEFC3, BENEFC4, BE' +
        'NEFC5, INCD_CUR, INCD_AMT, DECD_CUR, DECD_AMT, '
      
        '             NWCD_CUR, NWCD_AMT, CD_PERP, CD_PERM, CD_MAX, AA_CV' +
        '1, AA_CV2, AA_CV3, AA_CV4, LOAD_ON, FOR_TRAN, LST_DATE, SHIP_PD,' +
        ' SHIP_PD1, SHIP_PD2, SHIP_PD3, SHIP_PD4, SHIP_PD5, '
      
        '             SHIP_PD6, NARRAT, NARRAT_1, EX_NAME1, EX_NAME2, EX_' +
        'NAME3, EX_ADDR1, EX_ADDR2, SRBUHO, BFCD_AMT, BFCD_CUR, CARRIAGE,' +
        ' SUNJUCK_PORT, DOCHACK_PORT'
      'FROM APP707_2'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 232
    Top = 64
  end
  object qryAPPLOG: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, CR_NAME' +
        '1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NAME1, M' +
        'S_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, INV_AM' +
        'TC, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE, BL_DA' +
        'TE, APP_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, ARR_TXT, SPMARK1, SPM' +
        'ARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPMARK7, SPMARK8, SPMA' +
        'RK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOODS2, GOODS3, GOODS4' +
        ', GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOODS10, TRM_PAYC, TRM' +
        '_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2, CHK3, PRNO, CN_NAM' +
        'E1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, B5_NAME3'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      #9',LOADCODE.LOADNAME'
      #9',ARRCODE.ARRNAME'
      #9',TRMPAYCCODE.TRMPAYCNAME'
      #9',SUNSACODE.CRNAEM3NAME'
      'FROM APPLOG'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'APPLOG'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as LOADNAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADCODE ON LOAD_LOC = LOADCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as ARRNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#44397#44032#39') ARRCODE ON ARR_LOC = ARRCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as TRMPAYCNAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39#44208#51228#44592#44036#39') TRMPAYCCODE ON TRM_PAYC = TRMPAYCCO' +
        'DE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as CRNAEM3NAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39'SUNSA'#39') SUNSACODE ON CR_NAME3 = SUNSACODE.' +
        'CODE'
      ''
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 296
    Top = 16
  end
  object qryDOMOFCH1: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9'MAINT_NO, MESSAGE1, MESSAGE2, Chk1, Chk2, Chk3, Maint_Rff, USER' +
        '_ID, DATEE, D_CHK, SR_CODE, SR_NO, SR_NAME1, SR_NAME2, SR_NAME3,' +
        ' SR_ADDR1, SR_ADDR2, SR_ADDR3, UD_CODE, UD_NO, UD_NAME1, UD_NAME' +
        '2, UD_NAME3, UD_ADDR1, UD_ADDR2, UD_ADDR3, UD_RENO, OFR_NO, OFR_' +
        'DATE, OFR_SQ, OFR_SQ1, OFR_SQ2, OFR_SQ3, OFR_SQ4, OFR_SQ5, REMAR' +
        'K, REMARK_1, PRNO, HS_CODE'
      'FROM DOMOFC_H1'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 368
    Top = 16
  end
  object qryDOMOFCH2: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9'MAINT_NO, TSTINST, TSTINST1, TSTINST2, TSTINST3, TSTINST4, TSTI' +
        'NST5, PCKINST, PCKINST1, PCKINST2, PCKINST3, PCKINST4, PCKINST5,' +
        ' PAY, PAY_ETC, PAY_ETC1, PAY_ETC2, PAY_ETC3, PAY_ETC4, PAY_ETC5'
      'FROM DOMOFC_H2'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 368
    Top = 64
  end
  object qryDOMOFCH3: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9'MAINT_NO, TERM_DEL, TERM_NAT, TERM_LOC, ORGN1N, ORGN1LOC, ORGN2' +
        'N, ORGN2LOC, ORGN3N, ORGN3LOC, ORGN4N, ORGN4LOC, ORGN5N, ORGN5LO' +
        'C, TRNS_ID, LOADD, LOADLOC, LOADTXT, LOADTXT1, LOADTXT2, LOADTXT' +
        '3, LOADTXT4, LOADTXT5, DEST, DESTLOC, DESTTXT, DESTTXT1, DESTTXT' +
        '2, DESTTXT3, DESTTXT4, DESTTXT5, TQTY, TQTYCUR, TAMT, TAMTCUR'
      'FROM DOMOFC_H3'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 368
    Top = 112
  end
  object qryDOMOFCD: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9'KEYY, SEQ, HS_CHK, HS_NO, NAME_CHK, NAME_COD, NAME, NAME1, SIZE' +
        ', SIZE1, QTY, QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, AMT, AMT_G, S' +
        'TQTY, STQTY_G, STAMT, STAMT_G'
      'FROM DOMOFC_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 368
    Top = 160
  end
  object qryLOCAMR: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ, AMD_NO, USER_ID, DATEE, MESSAGE1, MESSAGE' +
        '2, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFE' +
        'RNO6, OFFERNO7, OFFERNO8, OFFERNO9, APP_DATE, ISS_DATE, REMARK, ' +
        'REMARK1, ISSBANK, ISSBANK1, ISSBANK2, APPLIC, APPLIC1, APPLIC2, ' +
        'APPLIC3, BENEFC, BENEFC1, BENEFC2, BENEFC3, EXNAME1, EXNAME2, EX' +
        'NAME3, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYPE, LOC_AMT, L' +
        'OC_AMTC, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, APPADDR1, A' +
        'PPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, BNF' +
        'DOMAIN, CD_PERP, CD_PERM'
      'FROM LOCAMR'
      'WHERE MAINT_NO = :MAINT_NO'
      'AND MSEQ = :MSEQ')
    Left = 32
    Top = 232
  end
  object qryVATBI1H: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_NO, FS_' +
        'NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, SE_ADD' +
        'R1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITEM1, BY' +
        '_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY_ADDR3' +
        ', BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, AG_NAM' +
        'E1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_UPTA, A' +
        'G_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, TAX_AMT' +
        ', REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AMT22, A' +
        'MT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT, SUPT' +
        'AMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CHK3, PR' +
        'NO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5, SE_SA' +
        'UP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX4, SE_' +
        'FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2, BY_S' +
        'AUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1_1, BY' +
        '_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADDR5, AG' +
        '_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, AG_FTX4, '
      'AG_FTX5, SE_NAME3, BY_NAME3'
      'FROM VATBI1_H'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 440
    Top = 16
  end
  object qryVATBI1D: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY, QTY_G' +
        ', QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, USAMT_G, ' +
        'SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, RATE'
      'FROM VATBI1_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 440
    Top = 72
  end
  object qryVATBI3H: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_NO, FS_' +
        'NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, SE_ADD' +
        'R1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITEM1, BY' +
        '_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY_ADDR3' +
        ', BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, AG_NAM' +
        'E1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_UPTA, A' +
        'G_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, TAX_AMT' +
        ', REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AMT22, A' +
        'MT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT, SUPT' +
        'AMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CHK3, PR' +
        'NO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5, SE_SA' +
        'UP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX4, SE_' +
        'FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2, BY_S' +
        'AUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1_1, BY' +
        '_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADDR5, AG' +
        '_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, AG_FTX4, '
      'AG_FTX5, SE_NAME3, BY_NAME3'
      'FROM VATBI3_H'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 512
    Top = 16
  end
  object qryVATBI3D: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY, QTY_G' +
        ', QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, USAMT_G, ' +
        'SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, RATE'
      'FROM VATBI3_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 512
    Top = 72
  end
  object qryLOCRC2H: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RFF_NO, GET_DAT, ' +
        'ISS_DAT, EXP_DAT, BENEFC, BENEFC1, APPLIC, APPLIC1, APPLIC2, APP' +
        'LIC3, APPLIC4, APPLIC5, APPLIC6, RCT_AMT1, RCT_AMT1C, RCT_AMT2, ' +
        'RCT_AMT2C, RATE, REMARK, REMARK1, TQTY, TQTY_G, TAMT, TAMT_G, LO' +
        'C_NO, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP_NAME, ' +
        'LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, LOADDATE, EXPDATE' +
        ', LOC_REM, LOC_REM1, CHK1, CHK2, CHK3, PRNO, AMAINT_NO, BSN_HSCO' +
        'DE, BENEFC2, APPLIC7'
      'FROM LOCRC2_H'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 96
    Top = 232
  end
  object qryLOCRC2D: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'KEYY, SEQ, HS_NO, NAME_COD, NAME1, SIZE1, QTY, QTY_G, QTYG, QTY' +
        'G_G, PRICE, PRICE_G, AMT, AMT_G, STQTY, STQTY_G, STAMT, STAMT_G,' +
        ' ASEQ, AMAINT_NO'
      'FROM LOCRC2_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 96
    Top = 280
  end
  object qryLOCRC2TAX: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'KEYY, SEQ, BILL_NO, BILL_DATE, BILL_AMOUNT, BILL_AMOUNT_UNIT, T' +
        'AX_AMOUNT, TAX_AMOUNT_UNIT'
      'FROM LOCRC2_TAX'
      'WHERE KEYY = :MAINT_NO')
    Left = 96
    Top = 328
  end
  object qryAPPSPCH: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2, CHK3,' +
        ' BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, CP_ACCOUNTNO, CP_NAME1, CP_N' +
        'AME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACCOUNTNO' +
        '1, CP_ADD_NAME1, CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_NAME2, ' +
        'CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAME, APP' +
        '_SAUPNO, APP_NAME, APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADDR3, CP' +
        '_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_EMAIL2,' +
        ' DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT,BGM1+'#39'-'#39'+B' +
        'GM2+BGM3+BGM4 TOTDOC_NO'
      'FROM APPSPC_H'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 168
    Top = 232
  end
  object qryAPPSPCD1: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        '     KEYY, DOC_GUBUN, SEQ, DOC_NO, LR_GUBUN, LR_NO, LR_NO2, ISS_' +
        'DATE, GET_DATE, EXP_DATE, ACC_DATE, VAL_DATE, BSN_HSCODE, ISS_EX' +
        'PAMTU, ISS_EXPAMTC, ISS_EXPAMT, EXCH, ISS_AMTU, ISS_AMTC, ISS_AM' +
        'T, ISS_TOTAMT, ISS_TOTAMTC, TOTCNT, TOTCNTC, VB_RENO, VB_SENO, V' +
        'B_FSNO, VB_MAINTNO, VB_DMNO, VB_DETAILNO, VB_GUBUN, SE_GUBUN, SE' +
        '_SAUPNO, SE_CODE, SE_SNAME, SE_NAME, SE_ELEC, SE_ADDR1, SE_ADDR2' +
        ', SE_ADDR3, SG_UPTAI1_1, SG_UPTAI1_2, SG_UPTAI1_3, HN_ITEM1_1, H' +
        'N_ITEM1_2, HN_ITEM1_3, BY_GUBUN, BY_SAUPNO, BY_CODE, BY_SNAME, B' +
        'Y_NAME, BY_ELEC, BY_ADDR1, BY_ADDR2, BY_ADDR3, SG_UPTAI2_1, SG_U' +
        'PTAI2_2, SG_UPTAI2_3, HN_ITEM2_1, HN_ITEM2_2, HN_ITEM2_3, AG_SAU' +
        'PNO, AG_CODE, AG_SNAME, AG_NAME, AG_ELEC, AG_ADDR1, AG_ADDR2, AG' +
        '_ADDR3, LA_BANK, LA_BANKNAME, LA_BANKBU, LA_ELEC, PAI_CODE1, PAI' +
        '_CODE2, PAI_CODE3, PAI_CODE4, PAI_AMTC1, PAI_AMTC2, PAI_AMTC3, P' +
        'AI_AMTC4, PAI_AMTU1, PAI_AMTU2, PAI_AMTU3, PAI_AMTU4, PAI_AMT1, ' +
        'PAI_AMT2, PAI_AMT3, PAI_AMT4, LA_TYPE, LA_BANKBUCODE, LA_BANKNAM' +
        'EP, LA_BANKBUP, REMARK1_1, REMARK1_2, REMARK1_3, REMARK1_4, '
      
        '     REMARK1_5, REMARK2_1, REMARK2_2, REMARK2_3, REMARK2_4, REMA' +
        'RK2_5'
      'FROM APPSPC_D1'
      'WHERE KEYY = :MAINT_NO')
    Left = 168
    Top = 280
  end
  object qryAPPSPCD2: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'KEYY, DOC_GUBUN, SEQ, LINE_NO, HS_NO, DE_DATE, IMD_CODE1, IMD_C' +
        'ODE2, IMD_CODE3, IMD_CODE4, SIZE1, SIZE2, SIZE3, SIZE4, SIZE5, S' +
        'IZE6, SIZE7, SIZE8, SIZE9, SIZE10, QTY, QTYC, TOTQTY, TOTQTYC, P' +
        'RICE, PRICE_G, PRICEC, CUX_RATE, SUP_AMT, SUP_AMTC, VB_TAX, VB_T' +
        'AXC, VB_AMT, VB_AMTC, SUP_TOTAMT, SUP_TOTAMTC, VB_TOTTAX, VB_TOT' +
        'TAXC, VB_TOTAMT, VB_TOTAMTC, BIGO1, BIGO2, BIGO3, BIGO4, BIGO5'
      'FROM APPSPC_D2'
      'WHERE KEYY = :MAINT_NO')
    Left = 168
    Top = 328
  end
  object qryAPP700_NEW: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM V_APP700 WHERE MAINT_NO = :MAINT_NO AND MSEQ = :MS' +
        'EQ')
    Left = 368
    Top = 216
  end
  object qryAPP707_NEW: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'AMD_NO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM V_APP707 WHERE MAINT_NO = :MAINT_NO AND MSEQ = :MS' +
        'EQ AND AMD_NO = :AMD_NO')
    Left = 368
    Top = 264
  end
  object qryPAYORD: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM PAYORD WHERE MAINT_NO = :MAINT_NO')
    Left = 368
    Top = 320
  end
  object qryAPPCOP: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPCOP WHERE MAINT_NO = :MAINT_NO')
    Left = 368
    Top = 368
  end
  object qryAPPCOP_D: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPCOP_D WHERE KEYY = :MAINT_NO')
    Left = 448
    Top = 368
  end
  object qryAPPRMI_H: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[APP_DT]'
      '      ,[REG_DT]'
      '      ,[USER_ID]'
      '      ,[TRN_HOPE_DT]'
      '      ,[IMPT_SCHEDULE_DT]'
      '      ,[ETC_NO]'
      '      ,[TRD_CD]'
      '      ,[IMPT_REMIT_CD]'
      '      ,[IMPT_TRAN_FEE_CD]'
      '      ,[REMIT_DESC]'
      '      ,[REMIT_CD]'
      '      ,[ADDED_FEE_TAR]'
      '      ,[TOTAL_AMT]'
      '      ,[TOTAL_AMT_UNIT]'
      '      ,[FORE_AMT]'
      '      ,[FORE_AMT_UNIT]'
      '      ,[ONLY_PWD]'
      '      ,[AUTH_NM1]'
      '      ,[AUTH_NM2]'
      '      ,[AUTH_NM3]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[CHK2]'
      '  FROM [APPRMI_H]'
      '  WHERE MAINT_NO = :MAINT_NO')
    Left = 512
    Top = 136
  end
  object qryAPPRMI_NAD: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, TAG, CODE, BUS_NO, BUS_NM1, BUS_NM2, BUS_NM3, B' +
        'US_ADDR1, BUS_ADDR2, BUS_ADDR3, NAT_CD, TEL_NO'
      'FROM APPRMI_NAD'
      'WHERE MAINT_NO = :MAINT_NO'
      'ORDER BY TAG DESC')
    Left = 512
    Top = 184
  end
  object qryAPPRMI_FII: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, TAG, ACCNT_NO, ACCNT_NM, ACCNT_UNIT, BNK_CD, BN' +
        'K_NM1, BNK_NM2, BNK_NM3, BNK_NM4, BNK_NAT_CD'
      'FROM APPRMI_FII'
      'WHERE MAINT_NO = :MAINT_NO'
      'ORDER BY ORD')
    Left = 512
    Top = 232
  end
  object qryAPPRMI_RFF: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, SEQ, RFF_NO, AMT, AMT_UNIT, REMIT_AMT, REMIT_AM' +
        'T_UNIT, CIF_AMT, CIF_AMT_UNIT, HS_CD, GOODS_NM, IMPT_CD, CONT_DT' +
        ', TOD_CD, TOD_REMARK, EXPORT_NAT_CD, EXPORT_OW_NM, EXPORT_OW_ADD' +
        'R, EXPORT_OW_NAT_CD, DETAIL_DESC1, DETAIL_DESC2, DETAIL_DESC3, D' +
        'ETAIL_DESC4, DETAIL_DESC5, IMPORT_OW_NM, IMPORT_OW_ADDR, IMPORT_' +
        'OW_NAT_CD, REG_DT, TMP_DEL'
      'FROM APPRMI_RFF'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 512
    Top = 280
  end
end
