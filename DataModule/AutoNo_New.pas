unit AutoNo_New;

interface

uses
  SysUtils, Classes;

type
  TdmAutoNo_New = class(TDataModule)
  private
    { Private declarations }
    FAUTOIDX : Integer;
    function getAUTOIDX: Integer;
  public
    { Public declarations }
    property AUTOIDX : Integer read getAUTOIDX;


  end;

var
  dmAutoNo_New: TdmAutoNo_New;

implementation

uses CodeContents;

{$R *.dfm}

{ TdmAutoNo_New }

function TdmAutoNo_New.getAUTOIDX: Integer;
begin
  DMCodeContents.CODEREFRESH;
  FAUTOIDX := DMCodeContents.ConfigAutoGubun.AsInteger;
  Result := FAUTOIDX;
end;

end.
