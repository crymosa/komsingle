unit DocumentQry;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TAPPSPCDOC = record
    DOC_2AH : Integer;  //물품수령증명서
    DOC_2AJ : Integer;  //세금계산서
    DOC_1BW : Integer;  //상업송장
  end;

  TDMDocumentQry = class(TDataModule)
    qryAPPPCR_H: TADOQuery;
    qryAPPPCRD1: TADOQuery;
    qryAPPPCRD2: TADOQuery;
    qryAPPPCR_TAX: TADOQuery;
    qryLOCAPP: TADOQuery;
    qryAPP7001: TADOQuery;
    qryAPP7002: TADOQuery;
    qryAPP7003: TADOQuery;
    qryAPP7071: TADOQuery;
    qryAPP7072: TADOQuery;
    qryAPPLOG: TADOQuery;
    qryDOMOFCH1: TADOQuery;
    qryDOMOFCH2: TADOQuery;
    qryDOMOFCH3: TADOQuery;
    qryDOMOFCD: TADOQuery;
    qryLOCAMR: TADOQuery;
    qryVATBI1H: TADOQuery;
    qryVATBI1D: TADOQuery;
    qryVATBI3H: TADOQuery;
    qryVATBI3D: TADOQuery;
    qryLOCRC2H: TADOQuery;
    qryLOCRC2D: TADOQuery;
    qryLOCRC2TAX: TADOQuery;
    qryAPPSPCH: TADOQuery;
    qryAPPSPCD1: TADOQuery;
    qryAPPSPCD2: TADOQuery;
    qryAPP700_NEW: TADOQuery;
    qryAPP707_NEW: TADOQuery;
    qryPAYORD: TADOQuery;
    qryAPPCOP: TADOQuery;
    qryAPPCOP_D: TADOQuery;
    qryAPPRMI_H: TADOQuery;
    qryAPPRMI_NAD: TADOQuery;
    qryAPPRMI_FII: TADOQuery;
    qryAPPRMI_RFF: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OPEN_APPPCR(MAINT_NO : String);
    procedure OPEN_LOCAPP(MAINT_NO : String);
    procedure OPEN_APP700(MAINT_NO : String);
    procedure OPEN_APP707(MAINT_NO : String);
    procedure OPEN_APPLOG(MAINT_NO : String);
    procedure OPEN_DOMOFC(MAINT_NO : String);
    procedure OPEN_LOCAMR(MAINT_NO : String; MSEQ : integer);
    procedure OPEN_VATBI1(MAINT_NO : String);
    procedure OPEN_VATBI3(MAINT_NO : String);
    procedure OPEN_LOCRC2(MAINT_NO : String);
    procedure OPEN_APPSPC(MAINT_NO : String);
    function OPEN_PAYORD(MAINT_NO : String):Integer;
    function OPEN_APPCOP(MAINT_NO: String): Integer;
    function OPEN_APPRMI(MAINT_NO: String):Integer;

    procedure CLOSE_APPPCR;
    procedure CLOSE_LOCAPP;
    procedure CLOSE_APP700;
    procedure CLOSE_APP707;
    procedure CLOSE_APPLOG;
    procedure CLOSE_DOMOFC;
    procedure CLOSE_LOCAMR;
    procedure CLOSE_VATBI1;
    procedure CLOSE_VATBI3;
    procedure CLOSE_LOCRC2;
    procedure CLOSE_APPSCP;
    procedure CLOSE_PAYORD;
    procedure CLOSE_APPRMI;


    function getStr(sTABLE,sFIELD : string):String;
//    function getint(sTABLE,sFIELD : string):integer;
    function getCurrency(sTABLE,sFIELD : string):Currency;

    //APPSPC 물품수령증명서와 세금계산서,상업송장의 건수를 확인하는 함수
    function getAPPSPCCount(KEYY : String) : TAPPSPCDOC;


  end;

var
  DMDocumentQry: TDMDocumentQry;

implementation

uses MSSQL;

{$R *.dfm}

{ TDMDocumentQry }

procedure TDMDocumentQry.CLOSE_APPPCR;
begin
  qryAPPPCR_H.Close;
  qryAPPPCRD1.Close;
  qryAPPPCRD2.Close;
  qryAPPPCR_TAX.Close;
end;

procedure TDMDocumentQry.CLOSE_LOCAPP;
begin
  qryLOCAPP.Close;
end;

procedure TDMDocumentQry.CLOSE_APP700;
begin
  qryAPP7001.Close;
  qryAPP7002.Close;
  qryAPP7003.Close
end;

procedure TDMDocumentQry.CLOSE_APP707;
begin
  qryAPP7071.Close;
  qryAPP7072.Close;
end;

procedure TDMDocumentQry.CLOSE_APPLOG;
begin
 qryAPPLOG.Close;
end;

procedure TDMDocumentQry.CLOSE_DOMOFC;
begin
  qryDOMOFCH1.Close;
  qryDOMOFCH2.Close;
  qryDOMOFCH3.Close;
  qryDOMOFCD.Close;
end;

procedure TDMDocumentQry.CLOSE_LOCAMR;
begin
  qryLOCAMR.Close;
end;

procedure TDMDocumentQry.CLOSE_VATBI1;
begin
  qryVATBI1H.Close;
  qryVATBI1D.Close;
end;

procedure TDMDocumentQry.CLOSE_VATBI3;
begin
  qryVATBI3H.Close;
  qryVATBI3D.Close;
end;

procedure TDMDocumentQry.CLOSE_LOCRC2;
begin
  qryLOCRC2H.Close;
  qryLOCRC2D.Close;
  qryLOCRC2TAX.Close;
end;

procedure TDMDocumentQry.CLOSE_APPSCP;
begin
  qryAPPSPCH.Close;
  qryAPPSPCD1.Close;
  qryAPPSPCD2.Close;
end;

function TDMDocumentQry.getCurrency(sTABLE, sFIELD: string): Currency;
begin
  Result := 0;
  IF sTABLE = 'LOCAPP' Then
    Result := qryLOCAPP.FieldByName(sFIELD).AsCurrency
  else if sTABLE = 'APP700_1' then
    Result := qryAPP7001.FieldByName(sFIELD).AsCurrency
  else if sTABLE = 'APP700_2' then
    Result := qryAPP7002.FieldByName(sFIELD).AsCurrency
  else if sTABLE = 'APP700_3' then
    Result := qryAPP7003.FieldByName(sFIELD).AsCurrency
  else if sTABLE = 'APP707_1' then
    Result := qryAPP7071.FieldByName(sFIELD).AsCurrency
  else if sTABLE = 'APP707_2' then
    Result := qryAPP7072.FieldByName(sFIELD).AsCurrency;
end;

function TDMDocumentQry.getStr(sTABLE, sFIELD: string): String;
begin
  Result := '';
  IF sTABLE = 'LOCAPP' Then
    Result := qryLOCAPP.FieldByName(sFIELD).AsString
  else if sTABLE = 'APP700_1' then
    Result := qryAPP7001.FieldByName(sFIELD).AsString
  else if sTABLE = 'APP700_2' then
    Result := qryAPP7002.FieldByName(sFIELD).AsString
  else if sTABLE = 'APP700_3' then
    Result := qryAPP7003.FieldByName(sFIELD).AsString
  else if sTABLE = 'APP707_1' then
    Result := qryAPP7071.FieldByName(sFIELD).AsString
  else if sTABLE = 'APP707_2' then
    Result := qryAPP7072.FieldByName(sFIELD).AsString;
end;

procedure TDMDocumentQry.OPEN_APPPCR(MAINT_NO: String);
begin
  qryAPPPCR_H.Close;
  qryAPPPCR_H.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPPCR_H.Open;

  qryAPPPCRD1.Close;
  qryAPPPCRD1.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPPCRD1.Open;

  qryAPPPCRD2.Close;
  qryAPPPCRD2.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPPCRD2.Open;

  qryAPPPCR_TAX.Close;
  qryAPPPCR_TAX.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPPCR_TAX.Open;
end;

procedure TDMDocumentQry.OPEN_LOCAPP(MAINT_NO: String);
begin
  qryLOCAPP.Close;
  qryLOCAPP.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryLOCAPP.Open;
end;

procedure TDMDocumentQry.OPEN_APP700(MAINT_NO: String);
begin
  qryAPP7001.Close;
  qryAPP7001.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPP7001.Open;

  qryAPP7002.Close;
  qryAPP7002.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPP7002.Open;

  qryAPP7003.Close;
  qryAPP7003.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPP7003.Open;
end;

procedure TDMDocumentQry.OPEN_APP707(MAINT_NO: String);
begin
  qryAPP7071.Close;
  qryAPP7071.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPP7071.Open;

  qryAPP7072.Close;
  qryAPP7072.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPP7072.Open;
end;

procedure TDMDocumentQry.OPEN_APPLOG(MAINT_NO: String);
begin
  qryAPPLOG.Close;
  qryAPPLOG.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPLOG.Open;
end;

procedure TDMDocumentQry.OPEN_DOMOFC(MAINT_NO: String);
begin
  qryDOMOFCH1.Close;
  qryDOMOFCH1.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryDOMOFCH1.Open;

  qryDOMOFCH2.Close;
  qryDOMOFCH2.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;        
  qryDOMOFCH2.Open;

  qryDOMOFCH3.Close;
  qryDOMOFCH3.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryDOMOFCH3.Open;

  qryDOMOFCD.Close;
  qryDOMOFCD.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryDOMOFCD.Open;
end;

procedure TDMDocumentQry.OPEN_LOCAMR(MAINT_NO: String; MSEQ : integer);
begin
  qryLOCAMR.Close;
  qryLOCAMR.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryLOCAMR.Parameters.ParamByName('MSEQ').Value := MSEQ;  
  qryLOCAMR.Open;
end;

procedure TDMDocumentQry.OPEN_VATBI1(MAINT_NO: String);
begin
  qryVATBI1H.Close;
  qryVATBI1H.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryVATBI1H.Open;

  qryVATBI1D.Close;
  qryVATBI1D.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryVATBI1D.Open;
end;

procedure TDMDocumentQry.OPEN_VATBI3(MAINT_NO: String);
begin
  qryVATBI3H.Close;
  qryVATBI3H.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryVATBI3H.Open;

  qryVATBI3D.Close;
  qryVATBI3D.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryVATBI3D.Open;
end;

procedure TDMDocumentQry.OPEN_LOCRC2(MAINT_NO: String);
begin
  qryLOCRC2H.Close;
  qryLOCRC2H.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryLOCRC2H.Open;

  qryLOCRC2D.Close;
  qryLOCRC2D.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryLOCRC2D.Open;

  qryLOCRC2TAX.Close;
  qryLOCRC2TAX.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryLOCRC2TAX.Open;

end;

procedure TDMDocumentQry.OPEN_APPSPC(MAINT_NO: String);
begin
  qryAPPSPCH.Close;
  qryAPPSPCH.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPSPCH.Open;

  qryAPPSPCD1.Close;
  qryAPPSPCD1.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPSPCD1.Open;

  qryAPPSPCD2.Close;
  qryAPPSPCD2.Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
  qryAPPSPCD2.Open;
end;

function TDMDocumentQry.getAPPSPCCount(KEYY: String): TAPPSPCDOC;
begin
  Result.DOC_2AH := 0;
  Result.DOC_2AJ := 0;
  Result.DOC_1BW := 0;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := ' SELECT DOC_GUBUN , count(*) as DocCount FROM APPSPC_D1 WHERE KEYY = ' + QuotedStr(KEYY) + 'GROUP BY KEYY , DOC_GUBUN';
      Open;

      First;
      while not Eof do
      begin
        if FieldByName('DOC_GUBUN').AsString = '2AH' then
          Result.DOC_2AH := FieldByName('DocCount').AsInteger;

        if FieldByName('DOC_GUBUN').AsString = '2AJ' then
          Result.DOC_2AJ := FieldByName('DocCount').AsInteger;

        if FieldByName('DOC_GUBUN').AsString = '1BW' then
          Result.DOC_1BW := FieldByName('DocCount').AsInteger;

        Next;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDMDocumentQry.CLOSE_PAYORD;
begin
  if qryPAYORD.Active Then
    qryPAYORD.Close;
end;

function TDMDocumentQry.OPEN_PAYORD(MAINT_NO: String):Integer;
begin
  result := -1;
  with qryPAYORD do
  begin
    Close;
    Parameters[0].Value := MAINT_NO;
    Open;

    Result := RecordCount;
  end;
end;

function TDMDocumentQry.OPEN_APPCOP(MAINT_NO: String):Integer;
begin
  result := -1;
  qryAPPCOP.Close;
  qryAPPCOP.Parameters[0].Value := MAINT_NO;
  qryAPPCOP.Open;
  result := qryAPPCOP.RecordCount;

  if Result > 0 Then
  begin
    qryAPPCOP_D.Close;
    qryAPPCOP_D.Parameters[0].Value := MAINT_NO;
    qryAPPCOP_D.Open;
  end;
end;


function TDMDocumentQry.OPEN_APPRMI(MAINT_NO: String): Integer;
begin
  result := -1;
  qryAPPRMI_H.Parameters[0].Value := MAINT_NO;
  qryAPPRMI_NAD.Parameters[0].Value := MAINT_NO;
  qryAPPRMI_FII.Parameters[0].Value := MAINT_NO;
  qryAPPRMI_RFF.Parameters[0].Value := MAINT_NO;
  qryAPPRMI_H.Open;
  qryAPPRMI_NAD.Open;
  qryAPPRMI_FII.Open;
  qryAPPRMI_RFF.Open;
end;

procedure TDMDocumentQry.CLOSE_APPRMI;
begin
  qryAPPRMI_H.Close;
  qryAPPRMI_NAD.Close;
  qryAPPRMI_FII.Close;
  qryAPPRMI_RFF.Close;
end;

end.
