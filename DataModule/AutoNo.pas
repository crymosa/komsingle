unit AutoNo;

interface

uses
  SysUtils, Classes, DB, ADODB, Variants,Dialogs, StrUtils;

type
  TDMAutoNo = class(TDataModule)
    qryCheck: TADOQuery;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    qryAutoInc: TADOQuery;
  private
    { Private declarations }
    FWORKTABLE : string;
    FNO : Integer;
    DEPTCODE : String;
    FREGDATE : String;
    FAUTOIDX : integer;
    function CheckNo:Boolean;
    procedure NewNo;
    procedure UpdateNo(LastSEQ : Integer);
  public
    { Public declarations }
    function GetDocumentNo(WorkTable : String):String;
    function GetDocumentNoAutoInc(WorkTable : String):String;
    procedure RenewNo;
  end;

var
  DMAutoNo: TDMAutoNo;
CONST
  FSQL = 'SELECT DOCID, DEPTCODE, REGDATE, LASTSEQ FROM AUTOINCR';
implementation

uses CodeContents, MSSQL, VarDefine;

{$R *.dfm}

{ TDMAutoNo }

function TDMAutoNo.CheckNo: Boolean;
begin
  with qryCheck do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DOCID = '+QuotedStr(FWORKTABLE));
    Case FAUTOIDX of
      //년월+일련번호
      1: SQL.Add('AND REGDATE = '+QuotedStr( FormatDateTime('YYYYMM',Now) ));
      //아이디+년월일+일련번호
      //2: SQL.Add('AND REGDATE = '+QuotedStr( FormatDateTime('YYYYMMDD',Now) ));
      //아이디+년월+일련번호
      //3: SQL.Add('AND REGDATE = '+QuotedStr( FormatDateTime('YYYYMM',Now) ));

      //년원일+일련번호
      //4: SQL.Add('AND REGDATE LIKE '+QuotedStr( '%'+FormatDateTime('YYYYMMDD',Now)+'%' ));
      //2017년 11월 16일 AUTOINCR 테이블에 값이 YYMMDD로 저장이되어 관리번호가 정상적으로
      //생성되지않아 수정하였씀.
       4: SQL.Add('AND REGDATE LIKE '+QuotedStr( '%'+FormatDateTime('YYMMDD',Now)+'%' ));

    end;
    SQL.Add('AND DEPTCODE = '+QuotedStr(LoginData.sID));
    //ShowMessage(SQL.Text);
    Open;

    Result := qryCheck.RecordCount > 0 ;
  end;
end;

function TDMAutoNo.GetDocumentNo(WorkTable: String): String;
begin
  FWORKTABLE := WorkTable;

  DMCodeContents.CODEREFRESH;
  FAUTOIDX := DMCodeContents.ConfigAutoGubun.AsInteger;

  IF FAUTOIDX = 0 Then
  begin
    Result := '';
    Exit;
  end;

  Case FAUTOIDX of
    1: FREGDATE := FormatDateTime('YYMM',Now);
    4: FREGDATE := FormatDateTime('YYMMDD',Now);
  end;

  try
    IF CheckNo Then
    begin
      FNO := qryCheck.FieldByName('LASTSEQ').AsInteger+1;
    end
    else
    begin
      FNO := 1;
    end;

    Result := FREGDATE+LoginData.sID+FormatFloat('0000',FNO);
  finally
    qryCheck.Close;
  end;

end;

function TDMAutoNo.GetDocumentNoAutoInc(WorkTable: String): String;
var
  nNo : Integer;
begin
  FWORKTABLE := WorkTable;

  DMCodeContents.CODEREFRESH;
  FAUTOIDX := DMCodeContents.ConfigAutoGubun.AsInteger;

  Result := '';

//------------------------------------------------------------------------------
// 내국신용장, 인수증일경우에는 다른방식으로 제출번호 가져옴
//------------------------------------------------------------------------------
  IF AnsiMatchText(WorkTable, ['LOCAPP','LOCRCT','APPSPC']) Then
  begin
    FREGDATE := FormatDateTime('YYMMDD',Now);
    with TADOQuery.Create(nil) do
    begin
      try
        Close;
        Connection := DMMssql.KISConnect;
        SQL.Text := 'SELECT LASTSEQ FROM AUTOINCR with(nolock) WHERE DOCID = '+QuotedStr(WorkTable)+' AND DEPTCODE = '+QuotedStr(LoginData.sID)+' AND REGDATE = '+QuotedStr(FREGDATE);
        Open;

        IF RecordCount = 0 Then
        begin
          Close;
          SQL.Text := 'INSERT INTO AUTOINCR(DOCID, DEPTCODE, REGDATE, LASTSEQ) VALUES('+QuotedStr(WorkTable)+','+QuotedStr(LoginData.sID)+','+QuotedStr(FREGDATE)+', ''1'')';
          ExecSQL;

          nNo := 1;
        end
        else
        begin
          nNo := FieldByName('LASTSEQ').AsInteger + 1;
          Close;
          SQL.Text := 'UPDATE AUTOINCR SET LASTSEQ = CONVERT(varchar,CONVERT(int,LASTSEQ)+1) WHERE DOCID = '+QuotedStr(WorkTable)+' AND DEPTCODE = '+QuotedStr(LoginData.sID)+' AND REGDATE = '+QuotedStr(FREGDATE);
          ExecSQL;
        end;
      finally
        Close;
        Free;
      end;
    end;

    Case AnsiIndexText(WorkTable,['LOCAPP','LOCRCT','APPSPC']) of
      0: Result := FREGDATE+FormatFloat('000',nNo);
      1,2: Result := FREGDATE+FormatFloat('00000',nNo);
    end;
    Exit;
  end;

//------------------------------------------------------------------------------
// 일반 문서들
//------------------------------------------------------------------------------
  IF (FAUTOIDX = 0)Then
  begin
    Exit;
  end;

  Case FAUTOIDX of
    1: FREGDATE := FormatDateTime('YYMM',Now);
    4: FREGDATE := FormatDateTime('YYMMDD',Now);
//    1: FREGDATE := FormatDateTime('YYYYMM',Now);
//    4: FREGDATE := FormatDateTime('YYYYMMDD',Now);
  end;

  with TADOQuery.Create(nil) do
  begin
    try
      Close;
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT LASTSEQ FROM AUTOINCR with(nolock) WHERE DOCID = '+QuotedStr(WorkTable)+' AND DEPTCODE = '+QuotedStr(LoginData.sID)+' AND REGDATE = '+QuotedStr(FREGDATE);
      Open;

      IF RecordCount = 0 Then
      begin
        Close;
        SQL.Text := 'INSERT INTO AUTOINCR(DOCID, DEPTCODE, REGDATE, LASTSEQ) VALUES('+QuotedStr(WorkTable)+','+QuotedStr(LoginData.sID)+','+QuotedStr(FREGDATE)+', ''1'')';
        ExecSQL;
        Result := FREGDATE+LoginData.sID+'0001';
      end
      else
      begin
        Result := FREGDATE+LoginData.sID+FormatFloat('0000',FieldByName('LASTSEQ').AsInteger+1);
        Close;
        SQL.Text := 'UPDATE AUTOINCR SET LASTSEQ = CONVERT(varchar,CONVERT(int,LASTSEQ)+1) WHERE DOCID = '+QuotedStr(WorkTable)+' AND DEPTCODE = '+QuotedStr(LoginData.sID)+' AND REGDATE = '+QuotedStr(FREGDATE);
        ExecSQL;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDMAutoNo.NewNo;
begin
  with qryIns do
  begin
    Close;
    Parameters.ParamByName('DOCID').Value := FWORKTABLE;
    Parameters.ParamByName('DEPTCODE').Value := LoginData.sID;
    Parameters.ParamByName('REGDATE').Value := FREGDATE;
    Parameters.ParamByName('LASTSEQ').Value := '0001';
    ExecSQL;
  end;
end;

procedure TDMAutoNo.RenewNo;
begin
  IF FNO = 1 Then
    NewNo
  else
    UpdateNo(FNO);
end;

procedure TDMAutoNo.UpdateNo(LastSEQ: integer);
begin
  with qryMod do
  begin
    Close;
    Parameters.ParamByName('DOCID').Value := FWORKTABLE;
    Parameters.ParamByName('DEPTCODE').Value := LoginData.sID;
    Parameters.ParamByName('REGDATE').Value := FREGDATE;
    Parameters.ParamByName('LASTSEQ').Value := FormatFloat('0000',LastSEQ);
    ExecSQL;    
  end;
end;

end.
