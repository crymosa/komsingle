unit CreateTable;

interface

uses
  SysUtils, Classes, ADODB;

type
  TDMCreateTable = class(TDataModule)
    CreateTableForCREADV: TADOCommand;
    CreateTableATTNTC: TADOCommand;
    cmdLDANTC: TADOCommand;
    cmdAddCodes: TADOCommand;
    cmdAddColums: TADOCommand;
    cmdAddDocName: TADOCommand;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Run;
  end;

var
  DMCreateTable: TDMCreateTable;

implementation

uses
  MSSQL;

{$R *.dfm}

{ TDMCreateTable }

procedure TDMCreateTable.Run;
begin
  //입금통지서 추가 테이블
  CreateTableForCREADV.Execute;
  //근거서류첨부 응답서 추가 테이블
  CreateTableATTNTC.Execute;
  //판매대금추심 도착 통보서 추가 코드 - LDANTC
  cmdLDANTC.Execute;
  //기타코드 추가
  cmdAddCodes.Execute;
  //물품매도확약서 컬럼추가
  cmdAddColums.Execute;
  //문서명 추가
  cmdAddDocName.Execute;
end;

end.
