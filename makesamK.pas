unit makesamK;

interface
    uses
        SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Dialogs,
        StdCtrls, ExtCtrls, FileCtrl;
    function  locapp_makesam(in_str: string):boolean;

    procedure write_teg(seg:string;tag:string);
    function  fil_cr(cnt:integer):string;

implementation

const cr : string = #13;
var   samfile1,samfile2,samfile : textfile;

function locapp_makesam(in_str: string):boolean;
var  a,b,c,d,e,s,t,sp : string;
     str, receiver, txtpath, txtname,end_sw : string;
     gubun_cnt,tax_cnt,loop_count,loop_count1,loop_lng,w_length, w_sp_length,wrk_cnt,zero_leng : integer;
     wrk_buffer: array[0..3] of char;
     aa,bb,t1: string;
     j : integer;
begin
     str := '      ';
     wrk_cnt:=0;
     txtpath := edisenKom_frm.config_table.fieldbyname('conf_edi_dir').asstring +'\flatout'+'\';

     txtname := '2AD'+in_str+'.UNH';
     assignfile(samfile,txtpath+txtname);
     rewrite(samfile);

     edisenKom_frm.Locapp_table.indexfieldnames := 'MAINT_NO';
     if  edisenKom_frm.Locapp_table.findkey([in_str])= false then
     begin
          exit;
     end;
     receiver := edisenKom_frm.Locapp_table.fieldbyname('송수신상대처').asstring;
     w_length :=length(receiver);
     w_sp_length := 17 - w_length;
     sp := '                 ';
     // Header Line
     t := receiver + copy(sp,1,w_sp_length)      //거래처코드
        + ' '
        + receiver + copy(sp,1,w_sp_length)      //기능군코드
        + ' '                                    //공백//
        + 'LOCAPP'                               //문서명//
        + ' '                                    //공백//
        + '1  '                                  //개정번호//
        + ' '                                    //공백//
        + '911'                                  //배포번호(911)
        + ' '                                    //공백//
        + 'LOCAPP'                         //응용키값(문서)//
        + edisenKom_frm.Locapp_table.fieldbyname('MAINT_NO').asstring;
     writeln(samfile,t);
     write_teg('UNH','');

     //   BGM (제출번호,전송구분,응답형태)
     t := '2AD' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('MAINT_NO').asstring + cr
        + '9' + cr
        + 'AB' + cr;
     write_teg('BGM 1      10',t);

     //
     t := '1' + cr + edisenKom_frm.Locapp_table.fieldbyname('BUSINESS').asstring + cr ;  //개설근거별 용도
     write_teg('BUS 1      11',t);
     //
     t := 'HS' + cr + edisenKom_frm.Locapp_table.fieldbyname('BSN_HSCODE').asstring + cr;
     write_teg('RFF 1      12',t);
     t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO1').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
     write_teg('RFF 2      12',t);

     j := 2;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO2').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO2').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO3').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO3').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO4').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO4').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO5').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO5').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO6').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO6').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO7').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO7').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO8').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO8').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     if   edisenKom_frm.Locapp_table.fieldbyname('OFFERNO9').asstring <> '' then
     begin
          inc(j);
          t := 'AAG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OFFERNO9').asstring + cr;   //1153 = AAG : 뭎품매도확약서번호
          write_teg('RFF '+inttostr(j)+'      12',t);
     end;
     inc(j);
     if   (edisenKom_frm.Locapp_table.fieldbyname('OPEN_NO').asstring = '' ) or
          (edisenKom_frm.Locapp_table.fieldbyname('OPEN_NO').asstring = '0' ) then
     begin
          t := '2AG' + cr + '1' + cr;   // 회차
     end else
     t := '2AG' + cr + edisenKom_frm.Locapp_table.fieldbyname('OPEN_NO').asstring + cr;   // 회차
     write_teg('RFF '+inttostr(j)+'      12',t);

     //
     t := '2AA'  + cr
        + copy(edisenKom_frm.Locapp_table.fieldbyname('APP_DATE').asstring,3,6) + cr  //개설신청일자
        + '101' + cr;
     write_teg('DTM 1      13',t);

     t := '272' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('DOC_PRD').asstring + cr  //서류제시 기간
        + '804' + cr;
     write_teg('DTM 2      13',t);

     t := '2' + cr
        + copy(edisenKom_frm.Locapp_table.fieldbyname('DELIVERY').asstring,3,6) + cr  //물품인도기일을 표시
        + '101' + cr;
     write_teg('DTM 3      13',t);

     t := '123' + cr
        + copy(edisenKom_frm.Locapp_table.fieldbyname('EXPIRY').asstring,3,6) + cr  //유효기일을 표시
        + '101' + cr;
     write_teg('DTM 4      13',t);


     t := edisenKom_frm.Locapp_table.fieldbyname('TRANSPRT').asstring + cr;  //분할인도 허용여부를 표시
     write_teg('TSR 1      14',t);

     edisenKom_frm.ListBox1.Items.Text := edisenKom_frm.Locapp_table.FieldByName('GOODDES1').AsString; //대표물품공급
     if   edisenKom_frm.ListBox1.Items.count = 5 then
     begin
          t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('GOODDES1').asstring + cr;  //대표 공급물품명을 표시
     end
     else
     if   edisenKom_frm.ListBox1.Items.count = 4 then
     begin
          t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('GOODDES1').asstring + cr
               + cr;
     end
     else
     if   edisenKom_frm.ListBox1.Items.count = 3 then
     begin
          t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('GOODDES1').asstring + cr
               + cr + cr;
     end
     else
     if   edisenKom_frm.ListBox1.Items.count = 2 then
     begin
          t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('GOODDES1').asstring + cr
               + cr +cr+cr;
     end
     else
     if   edisenKom_frm.ListBox1.Items.count = 1 then
     begin
          t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('GOODDES1').asstring + cr
               +cr+cr+cr+cr;
     end;
     write_teg('FTX 1      15',t);

     if edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring <> '' then
     begin
          edisenKom_frm.ListBox1.Items.Text := edisenKom_frm.Locapp_table.FieldByName('REMARK1').AsString; //기타정보
          if   edisenKom_frm.ListBox1.Items.count = 5 then
          begin
               t := 'ACB' + cr + edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 4 then
          begin
               t := 'ACB' + cr + edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring + cr
                    + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 3 then
          begin
               t := 'ACB' + cr + edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring + cr
                    + cr + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 2 then
          begin
               t := 'ACB' + cr + edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring + cr
                    + cr +cr+cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 1 then
          begin
               t := 'ACB' + cr + edisenKom_frm.Locapp_table.fieldbyname('REMARK1').asstring + cr
                    +cr+cr+cr+cr;
          end;
          write_teg('FTX 2      15',t);
     end;

     t := 'AZ' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('AP_BANK').asstring + cr  //개설은행  내국신용장 개설은행(부)점을 코드
        + '25' + cr
        + 'BOK' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('AP_BANK1').asstring + cr  //내국신용장 개설은행명을 평문으로 입력하는
        + edisenKom_frm.Locapp_table.fieldbyname('AP_BANK2').asstring + cr; //내국신용장 개설은행의 (부)점명을 평문으로 입력하는 부분
     write_teg('FII 1      16',t);

     t := 'DF' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('APPLIC1').asstring + cr  // ① 개설의뢰인의
        + edisenKom_frm.Locapp_table.fieldbyname('APPLIC2').asstring + cr  // 상호,
        + edisenKom_frm.Locapp_table.fieldbyname('APPLIC3').asstring + cr; //대표자명 등을 입력
     write_teg('NAD 1      17',t);

     IF edisenKom_frm.Locapp_table.fieldbyname('CHKNAME1').asstring <> '' then
     begin
          t := 'DG' + cr
               + edisenKom_frm.Locapp_table.fieldbyname('BENEFC1').asstring + cr  // 수혜자
               + edisenKom_frm.Locapp_table.fieldbyname('BENEFC2').asstring + cr  // 상호,
               + edisenKom_frm.Locapp_table.fieldbyname('BENEFC3').asstring  //대표자명 등을 입력
               + '/' + edisenKom_frm.Locapp_table.fieldbyname('CHKNAME1').asstring + '/' +
               edisenKom_frm.Locapp_table.fieldbyname('CHKNAME2').asstring + cr;
     end
     else
     t := 'DG' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('BENEFC1').asstring + cr  // 수혜자
        + edisenKom_frm.Locapp_table.fieldbyname('BENEFC2').asstring + cr  // 상호,
        + edisenKom_frm.Locapp_table.fieldbyname('BENEFC3').asstring + cr; //대표자명 등을 입력
     write_teg('NAD 2      17',t);

     t := 'AX' + cr
        + edisenKom_frm.Locapp_table.fieldbyname('EXNAME1').asstring + cr  // 전자서명 상호
        + edisenKom_frm.Locapp_table.fieldbyname('EXNAME2').asstring + cr  // 대표명
        + edisenKom_frm.Locapp_table.fieldbyname('EXNAME3').asstring + cr; //자전서명값
     write_teg('NAD 3      17',t);

     j:= 0;
     inc(j);
     t := '2AH' + cr
          + edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY1').asstring + cr ; //물품수령증명서
     write_teg('DOC '+inttostr(j)+'      18',t);

     if  edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY2').asfloat > 0 then  //공급자발행 세금계산서 사본
     begin
         inc(j);
         t := '2AJ' + cr
              + edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY2').asstring + cr ;
         write_teg('DOC '+inttostr(j)+'      18',t);
     end;
     if  edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY3').asfloat > 0 then  // 물품명세가 기재된 송장
     begin
         inc(j);
         t := '2AK' + cr
              + edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY3').asstring + cr ;
         write_teg('DOC '+inttostr(j)+'      18',t);
     end;

     inc(j);
     t := '2AP' + cr
          + edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY4').asstring + cr ; // 본 내국신용장의 사본
     write_teg('DOC '+inttostr(j)+'      18',t);

     inc(j);
     t := '310' + cr
          + edisenKom_frm.Locapp_table.fieldbyname('DOCCOPY5').asstring + cr ;  // 공급자발행 물품매도확약서 사본
     write_teg('DOC '+inttostr(j)+'      18',t);

     if edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring <> '' then
     begin
          edisenKom_frm.ListBox1.Items.Text := edisenKom_frm.Locapp_table.FieldByName('DOC_ETC1').AsString; //Page2 기타구비서류
          if   edisenKom_frm.ListBox1.Items.count = 5 then
          begin
               t := 'ABX' + cr + edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 4 then
          begin
               t := 'ABX' + cr + edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring + cr
                    + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 3 then
          begin
               t := 'ABX' + cr + edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring + cr
                    + cr + cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 2 then
          begin
               t := 'ABX' + cr + edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring + cr
                    + cr +cr+cr;
          end
          else
          if   edisenKom_frm.ListBox1.Items.count = 1 then
          begin
               t := 'ABX' + cr + edisenKom_frm.Locapp_table.fieldbyname('DOC_ETC1').asstring + cr
                    +cr+cr+cr+cr;
          end;
          write_teg('FTX 1      20',t);
     end;

     t :=  edisenKom_frm.Locapp_table.fieldbyname('LOC_TYPE').asstring + cr;  //) 내국신용장 종류를
     write_teg('BUS 1      18',t);

     if   edisenKom_frm.Locapp_table.fieldbyname('LOC_TYPE').asstring = '2AC' then  //순수원화표시
     begin
          t := '2AE' + cr  //원화금액 표시
             + edisenKom_frm.Locapp_table.fieldbyname('LOC_AMT').asString + cr
             + edisenKom_frm.Locapp_table.fieldbyname('LOC_AMTC').asString + cr;
          write_teg('MOA 1      21',t);
     end
     else
     begin
          t := '2AD' + cr   //외화금액 표시
             + edisenKom_frm.Locapp_table.fieldbyname('LOC_AMT').asString + cr
             + edisenKom_frm.Locapp_table.fieldbyname('LOC_AMTC').asString + cr;
          write_teg('MOA 1      21',t);
     end;


     if   edisenKom_frm.Locapp_table.fieldbyname('DOC_DTL').asstring <> '' then  //개설근거서류종류
     begin
          t := edisenKom_frm.Locapp_table.fieldbyname('DOC_DTL').asString + cr;
          write_teg('DOC 1      1A',t);

          t := 'AAC' + cr
             + edisenKom_frm.Locapp_table.fieldbyname('DOC_NO').asString + cr ;    //신용장(계약서)번호
          write_teg('RFF 1      22',t);


          if   edisenKom_frm.Locapp_table.fieldbyname('DOC_AMT').asfloat > 0 then  //결재금액
          begin
               t := '212' + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('DOC_AMT').asString + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('DOC_AMTC').asString + cr;
               write_teg('MOA 1      23',t);
          end;
          j := 0;
          if   edisenKom_frm.Locapp_table.fieldbyname('LOADDATE').asString <> '' then  //선적(인도)기일을 표시
          begin
               inc(j);
               t := '38' + cr
                    + copy(edisenKom_frm.Locapp_table.fieldbyname('LOADDATE').asString,3,6) + cr
                    + '101' + cr  ;
               write_teg('DTM '+inttostr(j)+'      24',t);
          end;
          if   edisenKom_frm.Locapp_table.fieldbyname('EXPDATE').asString <> '' then  //유효기일
          begin
               inc(j);
               t := '123' + cr
                    + copy(edisenKom_frm.Locapp_table.fieldbyname('EXPDATE').asString,3,6) + cr
                    + '101' + cr  ;
               write_teg('DTM '+inttostr(j)+'      24',t);
          end;

          if   edisenKom_frm.Locapp_table.fieldbyname('IM_NAME1').asString <> '' then  //수출(공급)상대방을 표시
          begin
               t := 'IM' + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('IM_NAME1').asString + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('IM_NAME2').asString + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('IM_NAME3').asString + cr;
               write_teg('NAD 1      25',t);
          end;

          if   edisenKom_frm.Locapp_table.fieldbyname('DEST').asString <> '' then  //수출지역을 표시 코드(2자리)
          begin
               t := '28' + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('DEST').asString + cr
                    + '162' + cr
                    + '5' + cr;
               write_teg('LOC 1      26',t);
          end;

          if   edisenKom_frm.Locapp_table.fieldbyname('ISBANK1').asString <> '' then  //발행은행을 표시
          begin
               t := 'AZ' + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('ISBANK1').asString + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('ISBANK2').asString + cr;
               write_teg('FII 1      27',t);
          end;

          if   edisenKom_frm.Locapp_table.fieldbyname('PAYMENT').asString <> '' then  //대금결제조건을 표시
          begin
               t := '1' + cr
                    + edisenKom_frm.Locapp_table.fieldbyname('PAYMENT').asString + cr;
               write_teg('PAT 1      28',t);
          end;

          if   edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asString <> '' then  //대표 수출물품명을 표시
          begin
               edisenKom_frm.ListBox1.Items.Text := edisenKom_frm.Locapp_table.FieldByName('EXGOOD1').AsString;
               if   edisenKom_frm.ListBox1.Items.count = 5 then
               begin
                    t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asstring + cr;
               end
               else
               if   edisenKom_frm.ListBox1.Items.count = 4 then
               begin
                    t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asstring + cr
                         + cr;
               end
               else
               if   edisenKom_frm.ListBox1.Items.count = 3 then
               begin
                    t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asstring + cr
                         + cr + cr;
               end
               else
               if   edisenKom_frm.ListBox1.Items.count = 2 then
               begin
                    t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asstring + cr
                         + cr +cr+cr;
               end
               else
               if   edisenKom_frm.ListBox1.Items.count = 1 then
               begin
                    t := 'AAA' + cr + edisenKom_frm.Locapp_table.fieldbyname('EXGOOD1').asstring + cr
                         +cr+cr+cr+cr;
               end;
               write_teg('FTX 1      29',t);
          end;
     end;
     write_teg('UNT','');

     t:=#26;
     write(samfile,t);
     closefile(samfile);
     edisenKom_frm.Locapp_table.edit;
     edisenKom_frm.Locapp_table.fieldbyname('전송').asstring   := '변환';
     edisenKom_frm.Locapp_table.post;
     edisenKom_frm.Locapp_table.refresh;
end;


procedure write_teg(seg:string;tag:string);
var  wrk_str,out_str : string;
     i,j : integer;
begin
  //     write_teg('UNH','');
//     write_teg('BGM 1      10',t);  
     wrk_str := '';
     if   seg <> '' then
     begin
          out_str := seg + #13#10;
     end
     else
     begin
          out_str := '';
     end;
     j := 0;

     for  i := 1 to length(tag) do
     begin
          wrk_str := wrk_str + copy(tag,i,1);
          if   copy(tag,i,1) = #13 then
          begin
               out_str := out_str + '    ' + wrk_str + #10;
               wrk_str := '';
          end;
     end;

     if   wrk_str <> '' then
     begin
          out_str := out_str + '    ' + wrk_str + #13#10;
     end;
     write(samfile,out_str);
end;

function  fil_cr(cnt:integer):string;
var  wrk_str : string;
     i : integer;
begin
     wrk_str := '';
     for  i := 1 to cnt do
     begin
          wrk_str := wrk_str + #13;
     end;
     result := wrk_str;
end;

end.
