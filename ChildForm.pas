unit ChildForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinProvider,TypeDefine,sEdit, sDBEdit, sMaskEdit, sCurrencyEdit, sMemo, sPanel, sComboBox, StrUtils, DB,
  DBGrids, Grids, acDBGrid, sCheckBox, ADODB, sButton, sSpeedButton;

type
  TChildForm_frm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CHK2GetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CHK3GetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    ProgramControlType : TProgramControlType;
    procedure RefreshButton(var sParent: TsPanel);
    procedure ClearControl(var sParent: TsPanel);
    procedure ClearGridTitleCaption(var TargetGrid : TsDBGrid);
    procedure EnableControl(var sParent: TsPanel; value : Boolean=True);
    procedure ReadOnlyControl(var sParent: TsPanel; ReadOnly : Boolean=True);
    procedure ButtonEnable; virtual; abstract;
    function CM_INDEX(var SearchBox: TsComboBox; sDelimiter: array of Char; CodeSize: Integer; value: string; nDefualtIndex: integer = 0): Integer;
    function CM_TEXT(var SearchBox: TsComboBox; sDelimiter: array of Char): String;
    function CheckMaxCols(var sMemo: TsMemo; LimitCols : Integer):integer;
    procedure AddErrMsg(var sList : TStringList; CheckObj : TObject; sMsg : String); overload;
    procedure AddErrMSG(var sList : TStringList; sMsg : String); overload;
    function ExecSQL(sSQL : String):Integer; overload;
    procedure ExecSQL(sSQL: TStream); overload;
    procedure ReadData; virtual; abstract;
    //조건변경신청서 작성시 작성번호 채번
    function getCompareDocno(DOCNO: String): Integer; virtual; abstract;
    //SAM FILE 작성
    procedure ReadyDocument(DocNo: String); virtual; abstract;
    //로그작성용
    procedure AddLog(obj, sData: String); virtual;
    function getStringFromQuery(sQuery : String; FieldIndex : Integer = 0):String;
    function getIntFromQuery(sQuery : String; FieldIndex : Integer = 0):Integer;
    // 빈 items을 갖는 memo text를 정리
    procedure ClearEmptyLine(var sList : TsMemo);
    // 빈 컴포넌트 값 찾기(
    function isEmpty(myObject : TObject):boolean;
    // dll 패치 적용
    procedure DllPatchExecute(resourceName : String);
  public
    { Public declarations }
  end;

var
  ChildForm_frm: TChildForm_frm;
const
  INPUT_OK_COLOR : TColor = $0075BAFF;
  INPUT_NO_COLOR : TColor = $00F4CB7D;

implementation


uses StdCtrls, MSSQL, VarDefine;

{$R *.dfm}


{ TChildForm_frm }

procedure TChildForm_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  IF (ActiveControl is TsEdit) OR
     (ActiveControl is TsDBEdit) OR
     (ActiveControl is TsMaskEdit) OR
     (ActiveControl is TsCurrencyEdit)
  Then
  begin
    IF ActiveControl.Tag = -1 Then Exit; 
    IF Key = #13 Then
       SelectNext(ActiveControl, True, True);      
  end;
end;

procedure TChildForm_frm.FormActivate(Sender: TObject);
begin
  sSkinProvider1.SkinData.SkinManager.RepaintForms(True);
end;

procedure TChildForm_frm.EnableControl(var sParent: TsPanel; value : Boolean=True);
var
  i : Integer;
begin
  for i := 0 to sParent.ControlCount-1 do
  begin
    IF sParent.Controls[i].Tag = -1 Then Continue;
    
    IF sParent.Controls[i] is TsMemo then
      (sParent.Controls[i] as TsMemo).ReadOnly := not value
    else
      sParent.Controls[i].Enabled := value;
  end;
end;

procedure TChildForm_frm.ReadOnlyControl(var sParent: TsPanel;
  ReadOnly: Boolean);
var
  i : Integer;
begin
  for i := 0 to sParent.ControlCount-1 do
  begin
    IF sParent.Controls[i].Tag = -1 then Continue;

    IF sParent.Controls[i] is TsEdit Then
      (sParent.Controls[i] As TsEdit).ReadOnly := ReadOnly;

    IF sParent.Controls[i] is TsMaskEdit Then
      (sParent.Controls[i] As TsMaskEdit).ReadOnly := ReadOnly;

    IF sParent.Controls[i] is TsComboBox Then
      (sParent.Controls[i] As TsComboBox).ReadOnly := ReadOnly;

    IF sParent.Controls[i] is TsCurrencyEdit Then
      (sParent.Controls[i] As TsCurrencyEdit).ReadOnly := ReadOnly;

    IF sParent.Controls[i] is TsMemo Then
      (sParent.Controls[i] As TsMemo).ReadOnly := ReadOnly;

    IF sParent.Controls[i] is TsCheckBox Then
      (sParent.Controls[i] As TsCheckBox).ReadOnly := ReadOnly;
  end;
end;

procedure TChildForm_frm.ClearControl(var sParent: TsPanel);
var
  i : integer;
begin
  for i := 0 to sParent.ControlCount-1 do
  begin
    if sParent.Controls[i].Tag > -1 Then
    begin
      if sParent.Controls[i] is TsEdit Then (sParent.Controls[i] as TsEdit).Clear
      else
      if sParent.Controls[i] is TsCurrencyEdit Then (sParent.Controls[i] as TsCurrencyEdit).Value := 0
      else
      if sParent.Controls[i] is TsMaskEdit Then (sParent.Controls[i] as TsMaskEdit).Clear
      else
      if sParent.Controls[i] is TsMemo Then (sParent.Controls[i] as TsMemo).Clear
      else
      if sParent.Controls[i] is TsComboBox Then (sParent.Controls[i] as TsComboBox).ItemIndex := 0
      else
      if sParent.Controls[i] is TsCheckBox Then (sParent.Controls[i] as TsCheckBox).Checked := False;
    end;
  end;
end;

procedure TChildForm_frm.FormCreate(Sender: TObject);
begin
  ProgramControlType := ctView;
end;

function TChildForm_frm.CM_INDEX(var SearchBox: TsComboBox;
  sDelimiter: array of Char; CodeSize: Integer; value: string;
  nDefualtIndex: integer): Integer;
var
  TempStr: string;
  PIDX, i: integer;
begin
  Result := -1;

  if High(sDelimiter) = 0 then
  begin
    for i := 0 to SearchBox.Items.Count - 1 do
    begin
      TempStr := SearchBox.Items.Strings[i];
      PIDX := POS(sDelimiter[0], TempStr);
      if PIDX = 0 then
        Continue;
      if AnsiCompareText(value, LeftStr(TempStr, PIDX - 1)) = 0 then
      begin
        Result := i;
        SearchBox.ItemIndex := i;
        Break;
      end;
    end;
  end;

  if Result = -1 then
  begin
    SearchBox.ItemIndex := nDefualtIndex;
  end;
end;

function TChildForm_frm.CM_TEXT(var SearchBox: TsComboBox; sDelimiter: array of Char): String;
var
  TempStr : String;
  PIDX : integer;
begin
  Result := '';

  if High(sDelimiter) = 0 then
  begin
    TempStr := SearchBox.Text;
    PIDX := POS(sDelimiter[0], TempStr);
    Result := LeftStr(TempStr, PIDX - 1);
  end;
end;

procedure TChildForm_frm.CHK2GetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  nIndex : integer;
begin
  nIndex := StrToIntDef( Sender.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TChildForm_frm.CHK3GetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  nIndex : integer;  
begin
  IF Length(Sender.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(Sender.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TChildForm_frm.DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

function TChildForm_frm.CheckMaxCols(var sMemo: TsMemo;
  LimitCols: Integer): integer;
var
  i : Integer;
begin
  Result := -1;

  for i := 0 to sMemo.Lines.Count-1 do
  begin
    IF LimitCols < Length(sMemo.Lines[i]) Then
    begin
      Result := i+1;
      Break;
    end;
  end;
end;

procedure TChildForm_frm.AddErrMsg(var sList : TStringList; CheckObj : TObject; sMsg : String);
begin
  IF CheckObj is TsEdit Then
  begin
    IF Trim((CheckObj as TsEdit).Text) = '' Then
      sList.Add(sMsg);
  end
  else
  IF CheckObj is TsMaskEdit Then
  begin
    IF Trim((CheckObj as TsMaskEdit).Text) = '' Then
      sList.Add(sMsg);
  end
  else
  IF CheckObj is TsComboBox Then
  begin
    IF Trim((CheckObj as TsComboBox).Text) = '' Then
      sList.Add(sMsg);
  end;
end;

function TChildForm_frm.ExecSQL(sSQL: String): Integer;
begin
  Result := -1;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sSQL;
      Result := ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;



procedure TChildForm_frm.AddErrMsg(var sList: TStringList; sMsg: String);
begin
  sList.Add(sMsg);
end;

procedure TChildForm_frm.AddLog(obj, sData : String);
var
  LOG_PATH : String;
  LOG_DATA : TStringList;
begin
  LOG_PATH := ExtractFilePath(Application.ExeName)+'LOG\';
  ForceDirectories(LOG_PATH);

  LOG_DATA := TStringList.Create;
  try
    LOG_DATA.Text := sData;
    LOG_DATA.SaveToFile(LOG_PATH+FormatDateTime('YYYYMMDDHHNNSS',Now)+'_'+obj+'_log.txt');
  finally
    LOG_DATA.Free;
  end;
end;

procedure TChildForm_frm.FormShow(Sender: TObject);
begin
  Self.Left := 1;
  Self.Top := 1;
end;

procedure TChildForm_frm.ClearGridTitleCaption(var TargetGrid: TsDBGrid);
var
  i : integer;
  sTitle : String;
begin
  for i := 0 to TargetGrid.Columns.Count-1 do
  begin
    sTitle := TargetGrid.Columns[i].Title.Caption;
    sTitle := AnsiReplaceText(AnsiReplaceText(sTitle,'▲',''),'▼','');
    TargetGrid.Columns[i].Title.Caption := sTitle;
  end;
end;

procedure TChildForm_frm.RefreshButton(var sParent: TsPanel);
var
  i : integer;
  sCaption : String;
begin
//------------------------------------------------------------------------------
// AlphaControl 버튼 눌려있는 버그 없애기위해
//------------------------------------------------------------------------------
  for i := 0 to sParent.ControlCount-1 do
  begin
    if (sParent.Controls[i] is TsButton) AND (sParent.Controls[i] as TsButton).Enabled Then
    begin
      (sParent.Controls[i] as TsButton).SetFocus;
    end;
  end;
  SendMessage(Self.Handle, WM_NEXTDLGCTL, 0, 0);
end;

function TChildForm_frm.getIntFromQuery(sQuery: String;
  FieldIndex: Integer): Integer;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sQuery;
      Open;

      Result := Fields[FieldIndex].AsInteger;
    finally
      Close;
      Free;
    end;
  end;
end;

function TChildForm_frm.getStringFromQuery(sQuery: String;
  FieldIndex: Integer): String;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sQuery;
      Open;

      Result := Fields[FieldIndex].AsString;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TChildForm_frm.ClearEmptyLine(var sList: TsMemo);
var
  i : integer;
begin
  for i := sList.Lines.Count-1 downto 0 do
  begin
    if Trim(sList.Lines.Strings[i]) <> '' Then
      Break
    else
      sList.Lines.Delete(i);
  end;
end;

procedure TChildForm_frm.DllPatchExecute(resourceName: String);
var
  DLL_HAEDER : THandle;
  ResStream : TResourceStream;
begin
  inherited;
  // 코드생성 쿼리
  DLL_HAEDER := LoadLibrary(DLL_NAME);
  try
    if DLL_HAEDER < 32 Then
    begin
      ShowMessage('NOT FOUND "'+DLL_NAME+'"');
      Exit;
    end;

    ResStream := TResourceStream.Create(DLL_HAEDER, resourceName, RT_RCDATA);
    ExecSQL(ResStream);
  finally
    FreeLibrary(DLL_HAEDER);
  end;
end;

procedure TChildForm_frm.ExecSQL(sSQL: TStream);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.LoadFromStream(sSQL);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

function TChildForm_frm.isEmpty(myObject: TObject): boolean;
begin
  Result := false;
  if myObject is TsEdit then
    Result := Trim((myObject as TsEdit).Text) = '';
  if myObject is TsMaskEdit then
    Result := Trim((myObject as TsMaskEdit).Text) = '';
  if myObject is TsCurrencyEdit then
    result := (myObject as TsCurrencyEdit).Value = 0;
end;


end.
