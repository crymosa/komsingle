unit Commonlib;

interface

uses
  Dialogs,Windows,SysUtils,StrUtils,DateUtils,DB,ADODB,Classes, Graphics,
  Variants, sPanel, sPageControl, sEdit, sCurrencyEdit, sMaskEdit, sScrollBox, sMemo, sBitBtn, sButton, sCheckBox, sGroupBox,
  sDBEdit, sDBMemo, sSpeedButton, sComboBox, sLabel, Types, Math;

function ParentMessage(MsgDlgType : TMsgDlgType; MSG : String):Boolean;
function ConfirmMessage(MSG : String):Boolean;
function WarningMessage(MSG : string):Boolean;

//세번부호 FORMAT
function HSCodeFormat(HSCODE : string; bEnable : Boolean = True):String;

function decodeCharDate(strDate : String):TDateTime;
function decodeDateStr(strdate : string):String;
//StrtoCurr Defalut 0
function STC(Value : String):Currency;
function CTNULL(Format :string;value : Currency):String;

//단위에 따라 DisplayFormat 변경
procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TBCDField); overload;
procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TFloatField); overload;
procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TIntegerField); overload;

function WinExecAndWait32(FileName:String ; Visibility : integer):Integer ;

//------------------------------------------------------------------------------
// MIG문서 관련
//------------------------------------------------------------------------------
//수신문서 데이터중 콜론 분리
function DivideColon(Text: String; Getidx: Integer): String;
//파라미터 리셋
procedure ParameterReset(var qry: TADOQuery);

//한글자르기
function CopyK(S: String; Index, Count: Integer; bSpace: Boolean=False): String;
function Trims(sValue : string; nStart,nLength: Integer):String;

//------------------------------------------------------------------------------
// 계산관련 함수
//------------------------------------------------------------------------------
function CompareExtended(val1, val2 : Extended):TValueRelationship;

//------------------------------------------------------------------------------
// 날짜관련 함수
//------------------------------------------------------------------------------
function isDate_8String(YYYYMMDD : string):Boolean;
function ConvertStr2Date(strDate : string):TDateTime;
//------------------------------------------------------------------------------
// 문자열함수
//------------------------------------------------------------------------------
function isEmptyText(sText : string):Boolean;
function TrimMemo(sMemo : TStrings):string;
function PosMulti(subChars: array of string; text: String): Boolean;
//------------------------------------------------------------------------------
// 개발에 필요한 함수
//------------------------------------------------------------------------------
procedure ClearControlValue(Owner : TsPanel); overload;
procedure ClearControlValue(Owner : TsTabSheet); overload;
procedure ClearControlValue(Owner : TsScrollBox); overload;
procedure ClearControlValue(Owner : TsGroupBox); overload;

procedure ReadOnlyControlValue(Owner : TsPanel;bValue : Boolean = True); overload;
procedure ReadOnlyControlValue(Owner : TsTabSheet;bValue : Boolean = True); overload;
procedure ReadOnlyControlValue(Owner : TsScrollBox;bValue : Boolean = True); overload;
procedure ReadOnlyControlValue(Owner : TsGroupBox;bValue : Boolean = True); overload;

procedure EnabledControlValue(Owner : TsPanel;bValue : Boolean = True); overload;
procedure EnabledControlValue(Owner : TsTabSheet;bValue : Boolean = True); overload;
procedure EnabledControlValue(Owner : TsScrollBox;bValue : Boolean = True); overload;
procedure EnabledControlValue(Owner : TsGroupBox;bValue : Boolean = True); overload;

function CheckEmptyValue(Owner : TsPanel):String; overload;
function CheckEmptyValue(Owner : TsScrollBox):String; overload;
function CheckEmptyValue(Owner : TsTabSheet):String; overload;

implementation

function ParentMessage(MsgDlgType : TMsgDlgType; MSG : String):Boolean;
begin
  IF MsgDlgType = mtConfirmation Then
    Result := MessageBox(0,PChar(MSG),'확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK
  else
  IF MsgDlgType = mtWarning Then
    Result := MessageBox(0,PCHAR(MSG),'확인',MB_OKCANCEL+MB_ICONWARNING) = ID_OK;
end;

function ConfirmMessage(MSG : String):Boolean;
begin
  Result := ParentMessage(mtConfirmation,MSG);
end;

function WarningMessage(MSG : string):Boolean;
begin
  Result := ParentMessage(mtWarning,MSG);
end;

function HSCodeFormat(HSCODE : string; bEnable : Boolean = True):String;
var
  TempStr : String;
begin
  TempStr := AnsiReplaceText( AnsiReplaceText(HSCODE,'-','') , '.' , '' );

  Result := TempStr;

  IF (Length(Tempstr) = 10) AND bEnable Then
    Result := LeftStr(TempStr,4)+'.'+MidStr(TempStr,5,2)+'-'+RightStr(TempStr,4)
end;

function decodeCharDate(strDate : String):TDateTime;
var
  sYear,
  sMonth,
  sDay : Integer;
begin
  sYear  := StrToInt(LeftStr(strDate,4));
  sMonth := StrToInt(MidStr(strDate,5,2));
  sDay   := StrToInt(RightStr(strDate,2));
  Result := EncodeDateTime(sYear,sMonth,sDay,0,0,0,0);
end;

function STC(Value : String):Currency;
begin
  value := AnsiReplaceText(Value,',','');
  Result := StrToCurrDef(Value,0);
end;

function WinExecAndWait32(FileName:String ; Visibility : integer):Integer ;
var
     zAppName    : Array[0..512] of Char ;
     zCurDir     : Array[0..255] of Char ;
     WorkDir     : String ;
     StartupInfo : TStartupInfo ;
     ProcessInfo : TProcessInformation ;
begin
     StrPCopy(zAppName,FileName) ;
     GetDir(0,WorkDir) ;
     StrPCopy(zCurDir,WorkDir) ;
     FillChar(StartupInfo,SizeOf(StartupInfo),#0) ;
     StartupInfo.cb := SizeOf(StartupInfo) ;

     StartupInfo.dwFlags := STARTF_USESHOWWINDOW ;
     StartupInfo.wShowWindow := Visibility ;
     if   not CreateProcess(nil,
                          zAppName,                      { pointer to command line string }
                          nil,                           { pointer to process security attributes }
                          nil,                           { pointer to thread security attributes }
                          False,                         { handle inheritance flag }
                          CREATE_NEW_CONSOLE or          { creation flags }
                          NORMAL_PRIORITY_CLASS,
                          nil,                           { pointer to new environment block }
                          nil,                           { pointer to current directory name }
                          StartupInfo,                   { pointer to STARTUPINFO }
                          ProcessInfo) then
     begin
          Result := -1 ; { pointer to PROCESS_INF }
     end
     else
     begin
          WaitforSingleObject(ProcessInfo.hProcess,INFINITE) ;
          //GetExitCodeProcess(ProcessInfo.hProcess,Result) ;

          Result := 1 ;
     end ;
end;

procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TBCDField); overload;
begin

  IF AnsiMatchText(Danwi,['KRW','']) OR (TargetField.AsCurrency = 0) Then
    TargetField.DisplayFormat := '#,0'
  else
    TargetField.DisplayFormat := '#,0 .000';
end;

procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TFloatField); overload;
begin
  IF AnsiMatchText(Danwi,['KRW','']) OR (TargetField.AsCurrency = 0) Then
    TargetField.DisplayFormat := '#,0'
  else
    TargetField.DisplayFormat := '#,0 .000';
end;

procedure ChangeFormatFORDanwi(const Danwi : string; var TargetField : TIntegerField); overload;
begin
  IF AnsiMatchText(Danwi,['KRW','']) OR (TargetField.AsInteger = 0) Then
    TargetField.DisplayFormat := '#,0'
  else
    TargetField.DisplayFormat := '#,0 .000';
end;

//콜론 분리하여 원하는 인덱스의 문자열가져오기
//해당 인덱스가 존재하지 않는다면 에러
function DivideColon(Text: String; Getidx: Integer): String;
var
  DivideList : TStringList;
begin
  DivideList := TStringList.Create;
  try
    DivideList.Delimiter := ':';
    Text := AnsiReplaceText(Text,' ','|');
    DivideList.DelimitedText := Text;

    IF Getidx = -1 Then
    begin
      Result := AnsiReplaceText(DivideList.Text,'|',' ');
    end
    else
    IF DivideList.Count-1 < Getidx Then
    BEGIN
//      Raise Exception.Create('변환중 오류가 발생하였습니다'#13#10'MSG : DivideColon_['+Text+'] GetIDX : '+IntToStr(Getidx));
      Result := '';    
//      Result := '-1';
      {IF DivideList.Strings[0] = 'BGM' Then
      begin
        //BGM의 경우 인덱스 3번에 문서기능이 4번에는 응답유형
        Case DivideList.Count Then
          4:
          begin
            Case Getidx of
              3: Result := DivideList.Strings[3];
              4: Result := 'NA';
            end;
          end;

          5:
          begin
            Case Getidx of
              3: Result := DivideList.Strings[3];
              4: Result := DivideList.Strings[4];
            end;
          end;
        end;
      end
      else
        Result := '-1' }
    end
    Else
      Result := AnsiReplaceText(DivideList.Strings[Getidx],'|',' ');
  finally
//    ShowMessage(inttostr(Length(Result))+'_'+Result);
    DivideList.Free;
  end;
end;

procedure ParameterReset(var qry: TADOQuery);
var
  nLoop : integer;
begin
  for nLoop := 0 to qry.Parameters.Count - 1 do
  begin
    IF paNullable in qry.Parameters[nLoop].Attributes Then
    begin
      qry.Parameters[nLoop].Value := Null;
    end
    else
    begin
      if qry.Parameters[nLoop].DataType IN [ftsmallint, ftBCD, ftfloat, ftinteger] then
        qry.Parameters[nLoop].Value := 0
      else
        qry.Parameters[nLoop].Value := '';
    end;
  end;
end;

function decodeDateStr(strdate : string):String;
begin
  Case Length(Strdate) of
    8:  Result := LeftStr(strdate,4)+'.'+MidStr(strdate,5,2)+'.'+RightStr(strdate,2)
    else
      Result := '';
  end;
end;

function CTNULL(Format :string;value : Currency):String;
begin
  IF Format = '' Then Format := '#,0.000';
  Result := '';
  IF Value <> 0 Then
  begin
    Result := FormatFloat(Format,value);
  end;
end;

function CopyK(S: String; Index, Count: Integer; bSpace: Boolean=False): String;
//==============================================================================
// PROCEDURE NAME : CopyK 
// DESCRIPTION : 
//      한글(2byte)을 Copy함수로 자를때 한글반바이트(1byte) 때문에 특수문자 또는 
//      한글이 깨지는 문제처리. 
// 
//      시작점(Index)이 한글 끝byte이면 다음 글자부터 자른다. (Index+1) 
//      끝문자가 한글 시작byte이면 이전 글자까지 자른다. (Count-1) 
//      시작과 끝이 한글반바이트면 안쪽에 있는 문자만 자른다.(Index+1, Count-1) 
// 
//      CopyK('안녕하세요', 2, 5, False)  ->  [녕하] 
//      CopyK('안녕하세요', 3, 5, False)  ->  [녕하] 
//      CopyK('안녕하세요', 2, 5, True)  ->  [녕하 ] 
//      CopyK('안녕하세요', 3, 5, True)  ->  [녕하 ] 
//      CopyK('안녕하세요', 2, 6, True)  ->  [녕하  ] 
// RETURN : 
//      자른문자열 
// PARAMETER : 
//      S      - 자를 문자열 
//      Index  - 시작점 
//      Count  - 갯수 
//      bSpace - 한글반바이트(1byte) 처리로 자른공간을 공백으로 채운다 
// HISTORY : 
//==============================================================================
var 
  sp: String;
  nLen: Integer;
begin 
  Result := ''; 
  sp    := ''; 

  nLen := Length(S);

  // 시작위치가 길이보다 크면 Exit 
  if (nLen < Index) then Exit; 

  // 자를 갯수보다 길이가 작으면 Count를 문자열 길이만큼 계산 
  // S인자 끝이 이미 깨져있을 경우... 
  if (nLen < (Index + Count-1)) then Count := nLen - Index + 1; 

  // 시작문자가 한글 끝byte면 다음 글자부터 시작한다. 
  if ByteType(S, Index) = mbTrailByte then 
  begin 
    inc(Index); 
    dec(Count); 
    
    if bSpace then sp := sp + ' '; // 줄어든 만큼 공백을 채운다. 
  end; 


  // 끝문자가 한글 시작byte면 줄여서 자른다. 
  if ByteType(S, Index+Count-1) = mbLeadByte then 
  begin
    dec(Count); 

    if bSpace then sp := sp + ' '; // 줄어든 만큼 공백을 채운다. 
  end; 

  Result := copy(S, Index, Count) + sp; 

end;

function isDate_8String(YYYYMMDD : string):Boolean;
var
  nYear, nMonth, nDay : Integer;
begin
  YYYYMMDD := AnsiReplaceText(YYYYMMDD,'.','');
  YYYYMMDD := AnsiReplaceText(YYYYMMDD,'-','');
  YYYYMMDD := AnsiReplaceText(YYYYMMDD,' ','');

  nYear  := StrToInt(LeftStr(YYYYMMDD,4));
  nMonth := StrToInt(MidStr(YYYYMMDD,5,2));
  nDay   := StrToInt(RightStr(YYYYMMDD,2));
  
  Result := IsValidDate(nYear,nMonth,nDay);
end;

function Trims(sValue : string; nStart,nLength: Integer):String;
begin
  Result := Trim( Copy(sValue,nStart,nLength) );
end;

function ConvertStr2Date(strDate : string):TDateTime;
var
  nYear, nMon, nDay, nHour, nMin, nSec : Word;
begin
  strDate := AnsiReplaceText(strDate,'-','');
  strDate := AnsiReplaceText(strDate,':','');
  strDate := AnsiReplaceText(strDate,' ','');

  IF strDate = '' Then
  begin
    Result := Now;
    Exit;
  end;

  Case Length(strDate) of
    8:
    begin
      nYear := StrToInt(LeftStr(strDate,4));
      nMon  := StrToInt(MidStr(strDate,5,2));
      nDay  := StrToInt(MidStr(strDate,7,2));
      nHour := 0;
      nMin  := 0;
      nSec  := 0;
    end;

    14:
    begin
      nYear := StrToInt(LeftStr(strDate,4));
      nMon  := StrToInt(MidStr(strDate,5,2));
      nDay  := StrToInt(MidStr(strDate,7,2));
      nHour := StrToInt(MidStr(strDate,9,2));
      nMin  := StrToInt(MidStr(strDate,11,2));
      nSec  := StrToInt(MidStr(strDate,13,2));
    end;
  end;

  Result := EncodeDateTime(nYear,nMon,nday,nHour,nMin,nSec,0);
end;

procedure ClearControlValue(Owner : TsPanel); overload;
var
  i : integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then (Owner.Controls[i] as TsEdit).Clear;
    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Clear;
    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Clear;    
    If Owner.Controls[i] is TsCurrencyEdit Then (Owner.Controls[i] as TsCurrencyEdit).Value := 0;
    IF Owner.Controls[i] is TsCheckBox then (Owner.Controls[i] as TsCheckBox).Checked := False;
  end;
end;

procedure ClearControlValue(Owner : TsTabSheet); overload;
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then (Owner.Controls[i] as TsEdit).Clear;
    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Clear;
    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Clear;
    If Owner.Controls[i] is TsCurrencyEdit Then (Owner.Controls[i] as TsCurrencyEdit).Value := 0;
    IF Owner.Controls[i] is TsCheckBox then (Owner.Controls[i] as TsCheckBox).Checked := False;
  end;
end;

procedure ClearControlValue(Owner : TsScrollBox); overload;
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then (Owner.Controls[i] as TsEdit).Clear;
    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Clear;
    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Clear;
    If Owner.Controls[i] is TsCurrencyEdit Then (Owner.Controls[i] as TsCurrencyEdit).Value := 0;
    IF Owner.Controls[i] is TsCheckBox then (Owner.Controls[i] as TsCheckBox).Checked := False;
  end;
end;

procedure ClearControlValue(Owner : TsGroupBox); overload;
var
  i : Integer;
begin
    for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then (Owner.Controls[i] as TsEdit).Clear;
    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Clear;
    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Clear;
    If Owner.Controls[i] is TsCurrencyEdit Then (Owner.Controls[i] as TsCurrencyEdit).Value := 0;
    IF Owner.Controls[i] is TsCheckBox then (Owner.Controls[i] as TsCheckBox).Checked := False;
    IF Owner.Controls[i] is TsDBEdit then (Owner.Controls[i] as TsDBEdit).Clear;
    IF Owner.Controls[i] is TsDBMemo then (Owner.Controls[i] as TsDBMemo).Clear;
  end;
end;

procedure ReadOnlyControlValue(Owner : TsPanel;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).Color = clBtnFace Then Continue;
      (Owner.Controls[i] as TsEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then
    begin
       (Owner.Controls[i] as TsMaskEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsMemo Then
    begin
      (Owner.Controls[i] as TsMemo).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsCurrencyEdit Then
    begin
      (Owner.Controls[i] as TsCurrencyEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsDBEdit Then (Owner.Controls[i] as TsDBEdit).ReadOnly := bValue;
    If Owner.Controls[i] is TsDBMemo Then (Owner.Controls[i] as TsDBMemo).ReadOnly := bValue;

    //If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    //If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;


    //App700 체크박스
    IF Owner.Controls[i] is TsCheckBox Then (Owner.Controls[i] as TsCheckBox).ReadOnly := bValue;
  end;
end;

procedure ReadOnlyControlValue(Owner : TsTabSheet;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).Color = clBtnFace Then Continue;
      (Owner.Controls[i] as TsEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).ReadOnly := bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).ReadOnly := bValue;

    If Owner.Controls[i] is TsCurrencyEdit Then
    begin
      (Owner.Controls[i] as TsCurrencyEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsDBEdit Then (Owner.Controls[i] as TsDBEdit).ReadOnly := bValue;
    If Owner.Controls[i] is TsDBMemo Then (Owner.Controls[i] as TsDBMemo).ReadOnly := bValue;

    //If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
   // If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
  end;
end;

procedure ReadOnlyControlValue(Owner : TsScrollBox;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).Color = clBtnFace Then Continue;
      (Owner.Controls[i] as TsEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).ReadOnly := bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).ReadOnly := bValue;

    If Owner.Controls[i] is TsCurrencyEdit Then
    begin
      (Owner.Controls[i] as TsCurrencyEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsDBEdit Then (Owner.Controls[i] as TsDBEdit).ReadOnly := bValue;
    If Owner.Controls[i] is TsDBMemo Then (Owner.Controls[i] as TsDBMemo).ReadOnly := bValue;

    //If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    //If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
  end;
end;

procedure ReadOnlyControlValue(Owner : TsGroupBox;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).Color = clBtnFace Then Continue;
      (Owner.Controls[i] as TsEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).ReadOnly := bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).ReadOnly := bValue;

    If Owner.Controls[i] is TsCurrencyEdit Then
    begin
      (Owner.Controls[i] as TsCurrencyEdit).ReadOnly := bValue;
    end;

    If Owner.Controls[i] is TsDBEdit Then (Owner.Controls[i] as TsDBEdit).ReadOnly := bValue;
    If Owner.Controls[i] is TsDBMemo Then (Owner.Controls[i] as TsDBMemo).ReadOnly := bValue;


    //If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    //If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
  end;
end;

function isEmptyText(sText : string):Boolean;
begin
  Result := Trim(sText) = '';
end;

function CheckEmptyValue(Owner : TsPanel):String;
var
  i : Integer;
begin
  Result := '';
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMaskEdit Then
    begin
      IF (Owner.Controls[i] as TsMaskEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMaskEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsMaskEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMaskEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMaskEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMemo Then
    begin
      IF (Owner.Controls[i] as TsMemo).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMemo).Text) then
        begin
          IF (Owner.Controls[i] as TsMemo).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMemo).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMemo).Name;
        end;
      end;
    end;

  end;
end;

function CheckEmptyValue(Owner : TsScrollBox):String;
var
  i : Integer;
begin
  Result := '';
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMaskEdit Then
    begin
      IF (Owner.Controls[i] as TsMaskEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMaskEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsMaskEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMaskEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMaskEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMemo Then
    begin
      IF (Owner.Controls[i] as TsMemo).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMemo).Text) then
        begin
          IF (Owner.Controls[i] as TsMemo).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMemo).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMemo).Name;
        end;
      end;
    end;

  end;
end;

function CheckEmptyValue(Owner : TsTabSheet):String;
var
  i : Integer;
begin
  Result := '';
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      IF (Owner.Controls[i] as TsEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMaskEdit Then
    begin
      IF (Owner.Controls[i] as TsMaskEdit).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMaskEdit).Text) then
        begin
          IF (Owner.Controls[i] as TsMaskEdit).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMaskEdit).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMaskEdit).Name;
        end;
      end;
    end;

    If Owner.Controls[i] is TsMemo Then
    begin
      IF (Owner.Controls[i] as TsMemo).HelpContext = 0 Then
        Continue
      else
      begin
        IF isEmptyText((Owner.Controls[i] as TsMemo).Text) then
        begin
          IF (Owner.Controls[i] as TsMemo).BoundLabel.Active Then
            Result := (Owner.Controls[i] as TsMemo).BoundLabel.Caption
          else
            Result := (Owner.Controls[i] as TsMemo).Name;
        end;
      end;
    end;

  end;
end;

function TrimMemo(sMemo : TStrings):String;
var
  i : Integer;
begin
  Result := '';
  i := 0;
  while i <= sMemo.Count-1 do
  begin
    IF Trim(sMemo.Strings[i]) = '' Then
      sMemo.Delete(i)
    else
      Inc(i);
  end;
  Result := sMemo.Text;
end;

procedure EnabledControlValue(Owner : TsPanel;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      (Owner.Controls[i] as TsEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then
    begin
       (Owner.Controls[i] as TsMaskEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsMemo Then
    begin
      (Owner.Controls[i] as TsMemo).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsCurrencyEdit Then
    begin
      (Owner.Controls[i] as TsCurrencyEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;

    if Owner.Controls[i] is TsComboBox then (Owner.Controls[i] as TsComboBox).Enabled := not bValue;

    if Owner.Controls[i] is TsDBEdit then (Owner.Controls[i] as TsDBEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBMemo then (Owner.Controls[i] as TsDBMemo).Enabled := not bValue;

    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;

    //App700 체크박스
    IF Owner.Controls[i] is TsCheckBox Then (Owner.Controls[i] as TsCheckBox).Enabled := not bValue;

    IF Owner.Controls[i] is TsLabel Then (Owner.Controls[i] as TsLabel).Enabled := not bValue;
  end;

end;

procedure EnabledControlValue(Owner : TsTabSheet;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      (Owner.Controls[i] as TsEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Enabled := not bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Enabled := not bValue;

    If Owner.Controls[i] is TsCurrencyEdit then (Owner.Controls[i] as TsCurrencyEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBEdit then (Owner.Controls[i] as TsDBEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBMemo then (Owner.Controls[i] as TsDBMemo).Enabled := not bValue;

    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;

    If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;
  end;

end;

procedure EnabledControlValue(Owner : TsScrollBox;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      (Owner.Controls[i] as TsEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Enabled := not bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Enabled := not bValue;

    If Owner.Controls[i] is TsCurrencyEdit then (Owner.Controls[i] as TsCurrencyEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBEdit then (Owner.Controls[i] as TsDBEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBMemo then (Owner.Controls[i] as TsDBMemo).Enabled := not bValue;

    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;

    If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;
    If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;
    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;
  end;

end;

procedure EnabledControlValue(Owner : TsGroupBox;bValue : Boolean = True);
var
  i : Integer;
begin
  for i := 0 to Owner.ControlCount-1 do
  begin
    If Owner.Controls[i] is TsEdit Then
    begin
      (Owner.Controls[i] as TsEdit).Enabled := not bValue;
    end;

    If Owner.Controls[i] is TsMaskEdit Then (Owner.Controls[i] as TsMaskEdit).Enabled := not bValue;

    If Owner.Controls[i] is TsMemo Then (Owner.Controls[i] as TsMemo).Enabled := not bValue;

    If Owner.Controls[i] is TsCurrencyEdit Then(Owner.Controls[i] as TsCurrencyEdit).Enabled := not bValue;

    If Owner.Controls[i] is TsBitBtn Then (Owner.Controls[i] as TsBitBtn).Enabled := not bValue;

    If Owner.Controls[i] is TsButton Then (Owner.Controls[i] as TsButton).Enabled := not bValue;

    if Owner.Controls[i] is TsDBEdit then (Owner.Controls[i] as TsDBEdit).Enabled := not bValue;

    if Owner.Controls[i] is TsDBMemo then (Owner.Controls[i] as TsDBMemo).Enabled := not bValue;

    IF Owner.Controls[i] is TsSpeedButton then (Owner.Controls[i] as TsSpeedButton).Enabled := not bValue;
  end;
end;

function PosMulti(subChars: array of string; text: String): Boolean;
var
  i : Integer;
begin
  Result := False;
  for i := 0 to High(subChars) do
  begin
    Result := Result or (AnsiPos(subChars[i], text) > 0);
    if Result Then Break;
  end;
end;

Const
  Eps : Double = 0.000000000000001;
function CompareExtended(val1, val2 : Extended):TValueRelationship;
begin
  result := 0;
  if Abs(val1-val2) < Eps then
    result := 0
  else
  if val1 > val2 then
    result := 1
  else
    Result := -1;
end;

end.
