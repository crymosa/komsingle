unit MessageDefine;

interface


Const
  MSG_PROGRAM_CLOSE = '프로그램을 종료하시겠습니까?';

  MSG_LOGIN_DISCORD = 'ID 또는 PASSWORD를 확인해주세요.';

  MSG_INPUTAFTERENTER = '입력 후 엔터';

  MSG_ADVICE_DEVELOPER = '개발업체에 문의해주세요';
  MSG_GETCODE_ERROR_EMPTY_HINT = 'ERROR : 코드 참조(HINT)가 없습니다';

  MSG_CustomerReg_CODE_EMPRY = '거래처 코드를 입력해주세요.';

  MSG_NOT_VALID_DATE = '날짜를 확인해주세요';
  //VATBI(세금계산서) 수정사유코드 메세지
  MSG_VATTYPE_NOT_VAILD_DATA = '수정사유코드는 세금계산서종류가 수정일반계산서,수정위수탁계산서,수정수입계산서일 경우에만 사용가능합니다.';
  MSG_QUESTION_SAVE = '입력데이터를 저장하시겠습니까?';
//------------------------------------------------------------------------------
// SYSTEM
//------------------------------------------------------------------------------
  MSG_SYSTEM_LINE = '=============================================='#13#10;
  MSG_SYSTEM_LINE2 = '==============================================';

  MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD = '찾을 데이터를 입력하세요';

  MSG_SYSTEM_SAVETEMP_OK = '임시저장 되었습니다';
  MSG_SYSTEM_SAVE_OK = '저장이 완료되었습니다';
  MSG_SYSTEM_SAVE_ERR = '오류가 발생하여 저장을 완료하지 못했습니다';

  MSG_SYSTEM_DEL_OK = '삭제가 완료되었습니다';
  MSG_SYSTEM_DEL_CONFIRM = '선택하신 데이터를 삭제하시겠습니까?';
  MSG_SYSTEM_DEL_ERR = '오류가 발생하여 삭제를 완료하지 못했습니다';
  MSG_SYSTEM_DEL_CANCEL = '삭제가능한 데이터가 없습니다';
  MSG_SYSTEM_DEL_RECVDOC = '해당 수신문서를 삭제하시겠습니까?';

  MSG_SYSTEM_COPY_OK = '복사가 완료되었습니다';
  MSG_SYSTEM_COPY_ERR = '오류가 발생하여 복사를 완료하지 못했습니다';

  MSG_SYSTEM_NOT_EDIT_BECAUSE_SEND = '전송된 문서입니다. 수정 할 수 없습니다';
  MSG_SYSTEM_NOT_EDIT_BECAUSE_ACCEPT = '접수중인 문서입니다. 수정 할 수 없습니다';
  MSG_SYSTEM_NOT_DEL_BECAUSE_SEND = '전송된 문서입니다. 삭제 할 수 없습니다';
  MSG_SYSTEM_NOT_EDIT_BECAUSE_PERMIT = '이미 승인된 문서 입니다. 수정 할 수 없습니다';
  MSG_SYSTEM_NOT_EDIT = '승인 또는 접수 중인 문서일 경우 수정 할 수 없습니다';
  MSG_SYSTEM_NOT_DEL = '승인 또는 접수 중인 문서일 경우 삭제 할 수 없습니다';

  MSG_SYSTEM_NOT_SEND = '문서가 임시, 접수 또는 승인인 경우는 재전송 할수 없습니다';
  MSG_SYSTEM_SEND_FAIL1 = '전송이 실패하였습니다'#13#10'방화벽(은행 홈페이지 접속중) 또는 WINMATE의 "통신설정"이 잘못 입력되어있는지 확인해주세요';
  MSG_SYSTEM_SEND_FAIL2 = '전송이 실패하였습니다'#13#10'전송창의 상태값을 확인해주세요';
  MSG_SYSTEM_SEND_OK = '전송이 완료되었습니다';

  MSG_SYSTEM_EMPTY_MAINT_NO = '관리번호를 입력해주세요';

  MSG_SYSTEM_RESEND_ALREDYSEND = '이미 전송한 문서입니다. 재전송하시겠습니까?';

  MSG_SYSTEM_CONVERT_ERR = '변환중 오류가 발생하였습니다 - TXOUT ERR'#13#10'WINMATE 로그를 확인하세요';

  MSG_SYSTEM_CANCEL = '취소시 내용이 저장 되지않습니다. 그래도 취소하시겠습니까?';

  MSG_ERR_CALC = '상품내역이 없습니다. 상품내역 입력 후 계산가능합니다.';

  MSG_EXCEL_IMPORT_ALLDATA_DEL = '엑셀가져오기를 실행하면 기존 데이터는 삭제됩니다. 계속 하시겠습니까?';
  MSG_EXCEl_IMPORT_OK = '가져오기를 완료했습니다';

  MSG_QUESTION_DUPLICATE_DATA = '이미 변환되어 수신완료된 문서입니다. 재변환 하시겠습니까?';
//------------------------------------------------------------------------------
// LOCAPP
//------------------------------------------------------------------------------
  MSG_LOCAPP_ERR_EMPTY_DOCNO1 = '[관리번호 - 헤더]를 입력하세요';
  MSG_LOCAPP_ERR_EMPTY_DOCNO2 = '[관리번호 - 은행]를 입력하세요';
  MSG_LOCAPP_ERR_EMPTY_DOCNO3 = '[관리번호 - 일련번호]를 입력하세요';

  MSG_APP707_CLEARTXT = 'REPLACE를 선택하면 현재 입력된 데이터가 초기화 됩니다. 계속 하시겠습니까?';
  MSG_LOCAPP_NOT_IDEN = '수혜자의 "수발신인 식별자" / "상세식별자"를 넣지 않을경우 수혜자쪽으로 동보전송이 가질 않습니다. 무시하시고 진행 하시겠습니까?';
//------------------------------------------------------------------------------
// LOCRCT
//------------------------------------------------------------------------------
  MSG_LOCRCT_DELTAX_QUESTION = '선택하신 세금계산서를 삭제하시겠습니까?';
  MSG_LOCRCT_DELGOODS_QUESTION = '선택하신 물품내역을 삭제하시겠습니까?';
  MSG_COPY_QUESTION = '선택하신 데이터를 복사하여 새로운 문서를 생성하시겠습니까?';
//------------------------------------------------------------------------------
// APPPCR
//------------------------------------------------------------------------------
  MSG_APPPCR_DETAIL_WRITING = '상품내역이 작성중입니다. 완료후 저장하세요';
  MSG_APPPCR_DOCUMENT_WRITING = '근거서류가 작성중입니다. 완료후 저장하세요';
  MSG_APPPCR_TAX_WRITING = '세금계산서가 작성중입니다. 완료후 저장하세요';

  MSG_APPPCR_DETAIL_NO_HS = 'HS부호를 입력해주세요';
  MSG_APPPCR_DETAIL_NO_APPDATE = '구매(공급)일을 입력해주세요';
  MSG_APPPCR_DETAIL_NO_PUM_NM = '품명을 입력해주세요';
  MSG_APPPCR_DETAIL_NOT_QTYC = '수량단위를 입력해주세요';
  MSG_APPPCR_DETAIL_QTY_ZERO = '수량이 0입니다. 계속 진행하시겠습니까?';
  MSG_APPPCR_DETAIL_PRI2_ZERO = '단가가 0입니다. 계속 진행하시겠습니까?';
  MSG_APPPCR_DETAIL_NOT_AMT1C = '통화단위를 입력해주세요';
  MSG_APPPCR_DETAIL_AMT1_ZERO = '금액이 0입니다. 계속 진행하시겠습니까?';
//------------------------------------------------------------------------------
// APP700
//------------------------------------------------------------------------------
  MSG_APP700_DAT_TO_DPU = '2020년 1월 1일부터 "DAT"코드는 "DPU"로 변경됩니다. 변경하시겠습니까?';
//------------------------------------------------------------------------------
// APPRMI
//------------------------------------------------------------------------------
  MSG_APPRMI_DOCUMENT_WRITING = '구비서류가 작성중입니다. 완료후 저장하세요';
implementation

end.
 