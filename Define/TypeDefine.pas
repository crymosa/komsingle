unit TypeDefine;

interface

type
  //Connection Type
  TConnectionType = (ctParadox,ctMssql);
  TConnectionTypes = SET OF TConnectionType;

  //Print Type
  TPrintType = (ptPreView,ptPrint,ptExcel,ptNone);
  TPrintTpyes = set of TPrintType;

  //ProgramControl Type
  TProgramControlType = (ctInsert,ctModify,ctDelete,ctView,ctNone);

  //LoginData
  TLoginData = record
    sID : String;
    sName : String;
    sCompanyNo : String;
    isAdmin : Boolean;
  end;

  //LOCAPP 내국신용장 자동번호 구조
  //LOCRCT 물품인수증 자동번호
  TAutoNumbering = record
    LOCAPP_NO : String;
    LOCAPP_BANK : String;
    LOCRCT_NO : String;
  end;
  //코드반환
  TCodeRecord = record
    sCode : String;
    sName : string;
  end;
  //변경신청서 키값
  TDOCNO = record
    MAINT_NO : String;
    MSEQ : Integer;
    AMD_NO : Integer;
  end;

Const
  TrueValues : array [0..6] of string = ('1', 'True', 'true', 'Y', 'y', 'T', 't');
  FalseValues : array [0..6] of string = ('0', 'False', 'false', 'N', 'n', 'F', 'f');  
implementation

end.
 