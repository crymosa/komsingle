unit ITEM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Grids, DBGrids, acDBGrid, StdCtrls, sComboBox,
  Buttons, sBitBtn, sEdit, sLabel, ExtCtrls, sSplitter, sSpeedButton,
  sPanel, sSkinProvider, DBCtrls, sDBMemo, Mask, sDBEdit;

type
  TITEM_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSplitter2: TsSplitter;
    sLabel1: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sPanel7: TsPanel;
    sEdit1: TsEdit;
    sBitBtn1: TsBitBtn;
    sComboBox1: TsComboBox;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit4: TsDBEdit;
    sSpeedButton6: TsSpeedButton;
    sDBEdit6: TsDBEdit;
    sSpeedButton2: TsSpeedButton;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sSpeedButton7: TsSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ITEM_frm: TITEM_frm;

implementation

{$R *.dfm}

end.
