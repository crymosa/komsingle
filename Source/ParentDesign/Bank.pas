unit Bank;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Buttons, sBitBtn, sEdit,
  dbcgrids, acDBCtrlGrid, sLabel, ExtCtrls, sSplitter, sSpeedButton,
  sPanel, sSkinProvider, Grids, DBGrids, acDBGrid;

type
  TBank_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSplitter2: TsSplitter;
    sLabel1: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sPanel7: TsPanel;
    sEdit1: TsEdit;
    sBitBtn1: TsBitBtn;
    sComboBox1: TsComboBox;
    sDBGrid1: TsDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Bank_frm: TBank_frm;

implementation

{$R *.dfm}

end.
