unit Config;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sEdit, ExtCtrls, sPanel,
  sBevel, sCheckBox, sRadioButton, sGroupBox, sComboBox, sButton;

type
  TConfig_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    edt_SanghoKOR: TsEdit;
    edt_SanghoENG: TsEdit;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    edt_NameKOR: TsEdit;
    edt_NameENG: TsEdit;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    edt_Addr1KOR: TsEdit;
    edt_Addr2KOR: TsEdit;
    edt_Addr3KOR: TsEdit;
    edt_Addr1ENG: TsEdit;
    edt_Addr2ENG: TsEdit;
    edt_Addr3ENG: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    edt_Tel: TsEdit;
    sPanel12: TsPanel;
    Edt_TradeSingoNo: TsEdit;
    sPanel13: TsPanel;
    edt_CompanyNo: TsEdit;
    sPanel14: TsPanel;
    com_SignRef: TsComboBox;
    sPanel15: TsPanel;
    edt_Sign: TsEdit;
    sPanel16: TsPanel;
    Edt_OpenPolicyNo: TsEdit;
    sPanel17: TsPanel;
    com_JeokHa: TsComboBox;
    sGroupBox1: TsGroupBox;
    Rad_0: TsRadioButton;
    Rad_1: TsRadioButton;
    Rad_2: TsRadioButton;
    Rad_3: TsRadioButton;
    Rad_4: TsRadioButton;
    Chk_Added: TsCheckBox;
    sButton1: TsButton;
    sButton2: TsButton;
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Config_frm: TConfig_frm;

implementation

{$R *.dfm}

procedure TConfig_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
