unit LOCADV_N;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, DBCtrls, sDBMemo, Buttons, sSpeedButton,
  sLabel, sDBEdit, ExtCtrls, sSplitter, Grids, DBGrids, acDBGrid,
  sComboBox, sButton, sEdit, Mask, sMaskEdit, sPanel, ComCtrls,
  sPageControl, sSkinProvider, DB, ADODB;

type
  TLOCADV_N_frm = class(TChildForm_frm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sDBGrid1: TsDBGrid;
    sTabSheet3: TsTabSheet;
    sSplitter1: TsSplitter;
    sPanel2: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sPanel3: TsPanel;
    sDBEdit33: TsDBEdit;
    sPanel4: TsPanel;
    sPanel1: TsPanel;
    sSplitter2: TsSplitter;
    sLabel7: TsLabel;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sLabel6: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    Btn_Ready: TsSpeedButton;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Btn_Temp: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    sTabSheet2: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sLabel1: TsLabel;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sLabel4: TsLabel;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sPanel6: TsPanel;
    sDBEdit16: TsDBEdit;
    sLabel2: TsLabel;
    sDBEdit17: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sDBEdit22: TsDBEdit;
    sDBEdit24: TsDBEdit;
    sDBEdit25: TsDBEdit;
    sDBEdit26: TsDBEdit;
    sDBEdit27: TsDBEdit;
    sDBEdit28: TsDBEdit;
    sDBEdit29: TsDBEdit;
    sPanel5: TsPanel;
    sDBEdit30: TsDBEdit;
    sDBEdit31: TsDBEdit;
    sDBEdit32: TsDBEdit;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sLabel3: TsLabel;
    sLabel5: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sPanel8: TsPanel;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sLabel12: TsLabel;
    sDBEdit47: TsDBEdit;
    sLabel13: TsLabel;
    sDBEdit48: TsDBEdit;
    sLabel15: TsLabel;
    sDBEdit49: TsDBEdit;
    sDBEdit50: TsDBEdit;
    sDBEdit51: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit53: TsDBEdit;
    sPanel9: TsPanel;
    sDBEdit54: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBMemo3: TsDBMemo;
    sPanel10: TsPanel;
    sLabel14: TsLabel;
    sDBEdit55: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit63: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    sDBEdit66: TsDBEdit;
    sDBEdit67: TsDBEdit;
    sDBEdit68: TsDBEdit;
    sDBEdit69: TsDBEdit;
    sDBEdit70: TsDBEdit;
    sDBEdit71: TsDBEdit;
    sPanel11: TsPanel;
    sDBMemo4: TsDBMemo;
    sPanel12: TsPanel;
    sDBEdit72: TsDBEdit;
    sDBEdit73: TsDBEdit;
    sDBEdit74: TsDBEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    sDBEdit75: TsDBEdit;
    sDBEdit76: TsDBEdit;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sComboBox1: TsComboBox;
    edt_MAINT_NO3: TsEdit;
    edt_MAINT_NO2: TsEdit;
    edt_MAINT_NO1: TsEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LOCADV_N_frm: TLOCADV_N_frm;

implementation

{$R *.dfm}

end.
