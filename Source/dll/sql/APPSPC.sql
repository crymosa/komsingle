CREATE PROCEDURE [dbo].[CopyAPPSPC]
(
	@OrgMaint_No varchar(35),
	@CpyMaint_No varchar(35)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--공통사항 복사
	INSERT INTO APPSPC_H(MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2, CHK3, BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, CP_ACCOUNTNO, CP_NAME1, CP_NAME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACCOUNTNO1, CP_ADD_NAME1, CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_NAME2, CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAME, APP_SAUPNO, APP_NAME, APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADDR3, CP_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_EMAIL2, DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT)
	SELECT @CpyMaint_No, USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2, CHK3, BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, CP_ACCOUNTNO, CP_NAME1, CP_NAME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACCOUNTNO1, CP_ADD_NAME1, CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_NAME2, CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAME, APP_SAUPNO, APP_NAME, APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADDR3, CP_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_EMAIL2, DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT
	FROM APPSPC_H
	WHERE MAINT_NO = @OrgMaint_No;
	--상세1 복사
	INSERT INTO APPSPC_D1(KEYY, DOC_GUBUN, SEQ, DOC_NO, LR_GUBUN, LR_NO, LR_NO2, ISS_DATE, GET_DATE, EXP_DATE, ACC_DATE, VAL_DATE, BSN_HSCODE, ISS_EXPAMTU, ISS_EXPAMTC, ISS_EXPAMT, EXCH, ISS_AMTU, ISS_AMTC, ISS_AMT, ISS_TOTAMT, ISS_TOTAMTC, TOTCNT, TOTCNTC, VB_RENO, VB_SENO, VB_FSNO, VB_MAINTNO, VB_DMNO, VB_DETAILNO, VB_GUBUN, SE_GUBUN, SE_SAUPNO, SE_CODE, SE_SNAME, SE_NAME, SE_ELEC, SE_ADDR1, SE_ADDR2, SE_ADDR3, SG_UPTAI1_1, SG_UPTAI1_2, SG_UPTAI1_3, HN_ITEM1_1, HN_ITEM1_2, HN_ITEM1_3, BY_GUBUN, BY_SAUPNO, BY_CODE, BY_SNAME, BY_NAME, BY_ELEC, BY_ADDR1, BY_ADDR2, BY_ADDR3, SG_UPTAI2_1, SG_UPTAI2_2, SG_UPTAI2_3, HN_ITEM2_1, HN_ITEM2_2, HN_ITEM2_3, AG_SAUPNO, AG_CODE, AG_SNAME, AG_NAME, AG_ELEC, AG_ADDR1, AG_ADDR2, AG_ADDR3, LA_BANK, LA_BANKNAME, LA_BANKBU, LA_ELEC, PAI_CODE1, PAI_CODE2, PAI_CODE3, PAI_CODE4, PAI_AMTC1, PAI_AMTC2, PAI_AMTC3, PAI_AMTC4, PAI_AMTU1, PAI_AMTU2, PAI_AMTU3, PAI_AMTU4, PAI_AMT1, PAI_AMT2, PAI_AMT3, PAI_AMT4, LA_TYPE, LA_BANKBUCODE, LA_BANKNAMEP, LA_BANKBUP, REMARK1_1, REMARK1_2, REMARK1_3 , REMARK1_4, REMARK1_5, REMARK2_1, REMARK2_2, REMARK2_3, REMARK2_4, REMARK2_5)
	SELECT @CpyMaint_No, DOC_GUBUN, SEQ, DOC_NO, LR_GUBUN, LR_NO, LR_NO2, ISS_DATE, GET_DATE, EXP_DATE, ACC_DATE, VAL_DATE, BSN_HSCODE, ISS_EXPAMTU, ISS_EXPAMTC, ISS_EXPAMT, EXCH, ISS_AMTU, ISS_AMTC, ISS_AMT, ISS_TOTAMT, ISS_TOTAMTC, TOTCNT, TOTCNTC, VB_RENO, VB_SENO, VB_FSNO, VB_MAINTNO, VB_DMNO, VB_DETAILNO, VB_GUBUN, SE_GUBUN, SE_SAUPNO, SE_CODE, SE_SNAME, SE_NAME, SE_ELEC, SE_ADDR1, SE_ADDR2, SE_ADDR3, SG_UPTAI1_1, SG_UPTAI1_2, SG_UPTAI1_3, HN_ITEM1_1, HN_ITEM1_2, HN_ITEM1_3, BY_GUBUN, BY_SAUPNO, BY_CODE, BY_SNAME, BY_NAME, BY_ELEC, BY_ADDR1, BY_ADDR2, BY_ADDR3, SG_UPTAI2_1, SG_UPTAI2_2, SG_UPTAI2_3, HN_ITEM2_1, HN_ITEM2_2, HN_ITEM2_3, AG_SAUPNO, AG_CODE, AG_SNAME, AG_NAME, AG_ELEC, AG_ADDR1, AG_ADDR2, AG_ADDR3, LA_BANK, LA_BANKNAME, LA_BANKBU, LA_ELEC, PAI_CODE1, PAI_CODE2, PAI_CODE3, PAI_CODE4, PAI_AMTC1, PAI_AMTC2, PAI_AMTC3, PAI_AMTC4, PAI_AMTU1, PAI_AMTU2, PAI_AMTU3, PAI_AMTU4, PAI_AMT1, PAI_AMT2, PAI_AMT3, PAI_AMT4, LA_TYPE, LA_BANKBUCODE, LA_BANKNAMEP, LA_BANKBUP, REMARK1_1, REMARK1_2, REMARK1_3, REMARK1_4, REMARK1_5, REMARK2_1, REMARK2_2, REMARK2_3, REMARK2_4, REMARK2_5
	FROM APPSPC_D1
	WHERE KEYY = @OrgMaint_No;
	--상세2 복사
	INSERT INTO APPSPC_D2(KEYY, DOC_GUBUN, SEQ, LINE_NO, HS_NO, DE_DATE, IMD_CODE1, IMD_CODE2, IMD_CODE3, IMD_CODE4, SIZE1, SIZE2, SIZE3, SIZE4, SIZE5, SIZE6, SIZE7, SIZE8, SIZE9, SIZE10, QTY, QTYC, TOTQTY, TOTQTYC, PRICE, PRICE_G, PRICEC, CUX_RATE, SUP_AMT, SUP_AMTC, VB_TAX, VB_TAXC, VB_AMT, VB_AMTC, SUP_TOTAMT, SUP_TOTAMTC, VB_TOTTAX, VB_TOTTAXC, VB_TOTAMT, VB_TOTAMTC, BIGO1, BIGO2, BIGO3, BIGO4, BIGO5)
	SELECT @CpyMaint_No, DOC_GUBUN, SEQ, LINE_NO, HS_NO, DE_DATE, IMD_CODE1, IMD_CODE2, IMD_CODE3, IMD_CODE4, SIZE1, SIZE2, SIZE3, SIZE4, SIZE5, SIZE6, SIZE7, SIZE8, SIZE9, SIZE10, QTY, QTYC, TOTQTY, TOTQTYC, PRICE, PRICE_G, PRICEC, CUX_RATE, SUP_AMT, SUP_AMTC, VB_TAX, VB_TAXC, VB_AMT, VB_AMTC, SUP_TOTAMT, SUP_TOTAMTC, VB_TOTTAX, VB_TOTTAXC, VB_TOTAMT, VB_TOTAMTC, BIGO1, BIGO2, BIGO3, BIGO4, BIGO5
	FROM APPSPC_D2
	WHERE KEYY = @OrgMaint_No;
	
	SET NOCOUNT OFF
	RETURN

END
