IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'DEBADV_HEADER')
BEGIN
    -- ==================================================================================
    -- 헤더 테이블 생성
    -- ==================================================================================
    CREATE TABLE [dbo].[DEBADV_HEADER](
	    [MAINT_NO] [varchar](35) NOT NULL,
	    [USER_ID] [varchar](10) NULL,
	    [DATEE] [varchar](8) NULL,
	    [MESSAGE1] [varchar](3) NULL,
	    [MESSAGE2] [varchar](3) NULL,
	    [ADV_DT] [varchar](8) NULL,
	    [WIT_DT] [varchar](8) NULL,
	    [DEP_DT] [varchar](8) NULL,
	    [EXT_DT] [varchar](8) NULL,
	    [ETC_REFCD] [varchar](2) NULL,
	    [ETC_REFNO] [varchar](35) NULL,
	    [CHK1] [bit] NULL,
	    [CHK2] [varchar](1) NULL,
	    [CHK3] [varchar](10) NULL,
	    [PAY_DTL] [varchar](350) NULL,
	    [ETC_INFO1] [varchar](350) NULL,
	    [ETC_INFO2] [varchar](350) NULL,
	    [CLT_BKCD] [varchar](11) NULL,
	    [CLT_MAN] [varchar](3) NULL,
	    [CLT_BKNM] [varchar](70) NULL,
	    [CLT_BRNM] [varchar](70) NULL,
	    [BEN_BKCD] [varchar](11) NULL,
	    [BEN_BKNM] [varchar](70) NULL,
	    [BEN_BRNM] [varchar](70) NULL,
	    [BEN_ACNT] [varchar](17) NULL,
	    [BEN_ACNTNM] [varchar](35) NULL,
	    [BEN_UNIT] [varchar](3) NULL,
	    [BEN_MAN] [varchar](3) NULL,
	    [BEN_NM1] [varchar](35) NULL,
	    [BEN_NM2] [varchar](35) NULL,
	    [BEN_NM3] [varchar](35) NULL,
	    [BEN_NM4] [varchar](35) NULL,
	    [BEN_NM5] [varchar](35) NULL,
	    [BEN_NM6] [varchar](35) NULL,
	    [CLT_NM1] [varchar](35) NULL,
	    [CLT_NM2] [varchar](35) NULL,
	    [CLT_NM3] [varchar](35) NULL,
	    [CLT_NM4] [varchar](35) NULL,
	    [CLT_NM5] [varchar](35) NULL,
	    [CLT_NM6] [varchar](35) NULL,
	    [SIGN1] [varchar](35) NULL,
	    [SIGN2] [varchar](35) NULL,
	    [SIGN3] [varchar](35) NULL,
     CONSTRAINT [PK_DEBADV_HEADER] PRIMARY KEY CLUSTERED 
    (
	    [MAINT_NO] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    -- ==================================================================================
    -- 디테일 테이블 생성
    -- ==================================================================================
    -- [DEBADV_DTL]
    CREATE TABLE [dbo].[DEBADV_DTL](
	    [MAINT_NO] [varchar](35) NOT NULL,
	    [CD5025] [varchar](2) NOT NULL,
	    [AMT_UNIT] [varchar](3) NULL,
	    [AMT] [decimal](18, 4) NULL,
	    [BASE_UNIT] [varchar](3) NULL,
	    [DEST_UNIT] [varchar](3) NULL,
	    [RATE] [decimal](10, 5) NULL,
	    [RATE_STDT] [date] NULL,
	    [RATE_EDDT] [date] NULL,
	    [D1_RFF_CD] [varchar](3) NULL,
	    [D1_RFF_NO] [varchar](35) NULL,
	    [WIT_ACNT] [varchar](35) NULL,
     CONSTRAINT [PK_DEBADV_DTL] PRIMARY KEY CLUSTERED 
    (
	    [MAINT_NO] ASC,
	    [CD5025] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    -- [DEBADV_DOCLIST]
    CREATE TABLE [dbo].[DEBADV_DOCLIST](
	    [MAINT_NO] [varchar](35) NOT NULL,
	    [SEQ] [int] NOT NULL,
	    [RELIST_TYPE] [varchar](3) NULL,
	    [RELIST_NO] [varchar](35) NULL,
	    [RELIST_ADDNO] [varchar](35) NULL,
	    [RELIST_APPDT] [varchar](8) NULL,
     CONSTRAINT [PK_DEBADV_DOCLIST] PRIMARY KEY CLUSTERED 
    (
	    [MAINT_NO] ASC,
	    [SEQ] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
    
    -- [DEBADV_FC1]
    CREATE TABLE [dbo].[DEBADV_FC1](
	[MAINT_NO] [varchar](35) NOT NULL,
	[FC1CD] [varchar](3) NOT NULL,
	[FC1AMT1] [decimal](18, 4) NULL,
	[FC1AMT1C] [varchar](3) NULL,
	[FC1AMT2] [decimal](18, 4) NULL,
	[FC1AMT2C] [varchar](3) NULL,
     CONSTRAINT [PK_DEBADV_FC1_1] PRIMARY KEY CLUSTERED 
    (
	    [MAINT_NO] ASC,
	    [FC1CD] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    -- [DEBADV_FC2]
    CREATE TABLE [dbo].[DEBADV_FC2](
	    [MAINT_NO] [varchar](35) NOT NULL,
	    [FC1CD] [varchar](3) NOT NULL,
	    [FC1SEQ] [int] NOT NULL,
	    [FC1CH] [varchar](3) NULL,
	    [FC1SA1] [decimal](18, 4) NULL,
	    [FC1SA1C] [varchar](3) NULL,
	    [FC1SA2] [decimal](18, 4) NULL,
	    [FC1SA2C] [varchar](3) NULL,
	    [FC1RATE1] [varchar](3) NULL,
	    [FC1RATE2] [varchar](3) NULL,
	    [FC1RATE] [decimal](18, 4) NULL,
	    [FC1RD1] [varchar](8) NULL,
	    [FC1RD2] [varchar](8) NULL,
     CONSTRAINT [PK_DEBADV_FC2] PRIMARY KEY CLUSTERED 
    (
	    [MAINT_NO] ASC,
	    [FC1CD] ASC,
	    [FC1SEQ] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END