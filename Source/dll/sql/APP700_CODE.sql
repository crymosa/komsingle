-- ===============================================================================
-- 2020-01-01 부터 가격조건 DAT -> DPU로 변경
-- ===============================================================================
IF (getdate() >= '2020-01-01')
BEGIN
    IF NOT EXISTS(SELECT 1 FROM CODE2NDD Where Prefix = '가격조건' AND CODE = 'DPU')
    BEGIN
        INSERT INTO CODE2NDD VALUES('가격조건','DPU','1','','도착지양하인도조건')
				UPDATE CODE2NDD SET REMARK = 0 WHERE Prefix = '가격조건' AND CODE = 'DAT'
    END
END