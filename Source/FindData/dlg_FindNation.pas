unit dlg_FindNation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBTables, ADODB, sSkinProvider, Grids, DBGrids, acDBGrid,
  StdCtrls, sButton, sEdit, sComboBox, ExtCtrls, sPanel, sLabel,TypeDefine,
  Buttons, sBitBtn;

type
  Tdlg_FindNation_frm = class(TForm)
    sLabel1: TsLabel;
    sPanel1: TsPanel;
    sComboBox1: TsComboBox;
    sEdit1: TsEdit;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    sButton2: TsButton;
    sButton3: TsButton;
    sSkinProvider1: TsSkinProvider;
    qryList_MSSQL: TADOQuery;
    qryList_Paradox: TQuery;
    dsList: TDataSource;
    sBitBtn3: TsBitBtn;
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryList_MSSQLAfterOpen(DataSet: TDataSet);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    FPrefix : String;
    FFindAllData : Boolean;
    procedure FindData(bALLDATA : Boolean);
    procedure FindMSSQL;
    procedure FindParadox;
    function GetCODE:String;
    function GetName:String;
    procedure SettingGridTitle;
  public
    { Public declarations }
    property SelectCODE:String  read GetCODE;
    property SelectNAME:String  read GetName;
    property Prefix:String Read FPrefix write FPrefix;
    function RUN(Prefix,CODE : String):TModalResult;
    function isValidData(Prefix,CODE : String):Boolean;
    function GetContent(Prefix,CODE : string):String;

  end;

var
  dlg_FindNation_frm: Tdlg_FindNation_frm;

implementation

uses VarDefine, CODE_MSSQL;

{$R *.dfm}

{ Tdlg_FindNation_frm }

procedure Tdlg_FindNation_frm.FindData(bALLDATA : Boolean);
begin
  FFindAllData := bALLDATA;
  Case ConnectionType of
    ctMSSQL : FindMSSQL;
    ctParadox : FindParadox;
  end;
end;

procedure Tdlg_FindNation_frm.FindMSSQL;
begin
  with qryList_MSSQL do
  begin
    Close;
    SQL.Text := FSQL;
    Parameters.ParamByName('Prefix').Value := FPrefix;
    Case sComboBox1.ItemIndex of
      0: SQL.Add('AND CODE LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
      1: SQL.Add('AND NAME LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;

    IF not FFindAllData Then
    begin
      SQL.Add('AND [Remark] = 1')
    end;

    Open;
  end;
end;

procedure Tdlg_FindNation_frm.FindParadox;
begin
  //
end;

function Tdlg_FindNation_frm.RUN(Prefix,CODE: String): TModalResult;
begin
  Result := mrCancel;
  sEdit1.Text := CODE;
  FPrefix := Prefix;
  IF CODE <> '' Then
  begin
    sComboBox1.ItemIndex := 0;
  end;

  FindData(False);

  IF Self.ShowModal = mrOK Then
  begin
    Result := mrOk;
  end;
end;

procedure Tdlg_FindNation_frm.sButton1Click(Sender: TObject);
begin
  FindData(False);
end;

procedure Tdlg_FindNation_frm.sButton3Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure Tdlg_FindNation_frm.sButton2Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

function Tdlg_FindNation_frm.GetContent(Prefix,CODE: string): String;
begin
  Result := '';

  IF CODE = '' Then EXIT;
  
  IF isValidData(Prefix,CODE) Then
  begin
    Case ConnectionType of
      ctMssql :
      begin
        Result := qryList_MSSQL.FieldByName('NAME').AsString;
      end;
      ctParadox :
      begin
        Result := qryList_Paradox.FieldByName('NAME').AsString;
      end;
    end;
  end
  else
  begin
    Result := 'CODE ['+CODE+'] IS NOT FOUND DATA!';
  end;
end;

function Tdlg_FindNation_frm.isValidData(Prefix,CODE: String): Boolean;
begin
  sEdit1.Text := CODE;
  FPrefix := Prefix;
  FindData(True);
  Result := False;
  Case ConnectionType of
    ctMssql :
    begin
      Result := qryList_MSSQL.RecordCount > 0;
    end;
    ctParadox :
    begin
      Result := qryList_Paradox.RecordCount > 0;
    end;
  end;
end;

procedure Tdlg_FindNation_frm.FormCreate(Sender: TObject);
begin
  FSQL := qryList_MSSQL.SQL.Text;
end;


function Tdlg_FindNation_frm.GetCODE: String;
begin
  Case ConnectionType of
    ctMssql   : Result := qryList_MSSQL.FieldByName('CODE').AsString;
    ctParadox : Result := qryList_Paradox.FieldByName('CODE').AsString;
  end;
end;

function Tdlg_FindNation_frm.GetName: String;
begin
  Case ConnectionType of
    ctMssql   : Result := qryList_MSSQL.FieldByName('NAME').AsString;
    ctParadox : Result := qryList_Paradox.FieldByName('NAME').AsString;
  end;
end;

procedure Tdlg_FindNation_frm.sEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = VK_RETURN Then sButton1Click(nil);
end;

procedure Tdlg_FindNation_frm.sDBGrid1DblClick(Sender: TObject);
begin
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    ModalResult := mrOk;
  end;
end;

procedure Tdlg_FindNation_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = VK_RETURN Then sDBGrid1DblClick(nil);
end;

procedure Tdlg_FindNation_frm.SettingGridTitle;
begin
  sComboBox1.Items.Clear;
  IF FPrefix = '국가' Then
  begin
    Self.Caption := '국가코드 조회';
    sLabel1.Caption := '국가 코드 조회';
    sComboBox1.Items.Add('국가코드');
    sComboBox1.Items.Add('국가명(영문)');
    sCombobox1.ItemIndex := 0;
  end
  else IF FPrefix = '단위' Then
  begin
    Self.Caption := '수량단위 조회';
    sLabel1.Caption := '수량 단위 조회';
    sCombobox1.Items.Add('단위코드');
    sCombobox1.Items.Add('명칭');
    sCombobox1.ItemIndex := 0;    
  end
  else IF FPrefix = '통화' Then
  begin
    Self.Caption := '통화단위 조회';
    sLabel1.Caption := '통화 단위 조회';
    sCombobox1.Items.Add('통화코드');
    sCombobox1.Items.Add('명칭');
    sCombobox1.ItemIndex := 0;
  end;
end;

procedure Tdlg_FindNation_frm.qryList_MSSQLAfterOpen(DataSet: TDataSet);
begin
  SettingGridTitle;
end;

procedure Tdlg_FindNation_frm.sBitBtn3Click(Sender: TObject);
begin
  CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Self);

  try
    if CODE_MSSQL_frm.Run('국가') = mrOk then
    begin
      qryList_MSSQL.Close;
      qryList_MSSQL.Open;
    end;
  finally
    FreeAndNil(CODE_MSSQL_frm);
  end;

end;

end.
