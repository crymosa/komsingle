inherited dlg_FindCity_frm: Tdlg_FindCity_frm
  Left = 1161
  Top = 348
  Caption = #46020#49884#53076#46300' '#51312#54924
  ClientHeight = 397
  ClientWidth = 428
  FormStyle = fsNormal
  Position = poOwnerFormCenter
  Visible = False
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sLabel1: TsLabel [0]
    Left = 0
    Top = 56
    Width = 105
    Height = 12
    Caption = #9654#44397#44032' '#53076#46300' '#51312#54924
    ParentFont = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = [fsBold]
  end
  object sPanel1: TsPanel [1]
    Left = 0
    Top = 0
    Width = 428
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      428
      41)
    object com_SearchKeyword: TsComboBox
      Left = 24
      Top = 10
      Width = 81
      Height = 21
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #46020#49884#53076#46300
      Items.Strings = (
        #46020#49884#53076#46300
        #46020#49884#47749)
    end
    object edt_Search: TsEdit
      Left = 106
      Top = 10
      Width = 159
      Height = 21
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyUp = edt_SearchKeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sButton1: TsButton
      Left = 266
      Top = 10
      Width = 75
      Height = 21
      Caption = #44160#49353
      TabOrder = 2
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 0
      ContentMargin = 14
    end
    object sBitBtn3: TsBitBtn
      Left = 342
      Top = 10
      Width = 75
      Height = 21
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 3
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 2
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 72
    Width = 425
    Height = 281
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #46020#49884#53076#46300
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #46020#49884#47749
        Width = 313
        Visible = True
      end>
  end
  object sButton2: TsButton [3]
    Left = 136
    Top = 362
    Width = 75
    Height = 29
    Caption = #54869#51064
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 2
    SkinData.SkinSection = 'BUTTON'
    Images = DMICON.System18
    ImageIndex = 17
  end
  object sButton3: TsButton [4]
    Left = 216
    Top = 362
    Width = 75
    Height = 29
    Cancel = True
    Caption = #52712#49548
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'BUTTON'
    Images = DMICON.System18
    ImageIndex = 18
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 208
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Prefix'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CODE2NDD'
      'WHERE Prefix = :Prefix'
      'AND Remark = 1')
    Left = 8
    Top = 144
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 144
  end
end
