unit Config_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Config, sSkinProvider, StdCtrls, sButton, sCheckBox,
  sRadioButton, sGroupBox, sComboBox, sEdit, ExtCtrls, sPanel, DB, ADODB, Strutils,TypeDefine;

type
  TConfig_MSSQL_frm = class(TConfig_frm)
    qryConfig: TADOQuery;
    qryConfigNAME1: TStringField;
    qryConfigNAME2: TStringField;
    qryConfigTRAD_NO: TStringField;
    qryConfigSAUP_NO: TStringField;
    qryConfigADDR1: TStringField;
    qryConfigADDR2: TStringField;
    qryConfigADDR3: TStringField;
    qryConfigENAME1: TStringField;
    qryConfigENAME2: TStringField;
    qryConfigENAME3: TStringField;
    qryConfigEADDR1: TStringField;
    qryConfigEADDR2: TStringField;
    qryConfigEADDR3: TStringField;
    qryConfigSIGN_C: TStringField;
    qryConfigSIGN: TStringField;
    qryConfigNMLEN: TStringField;
    qryConfigSZLEN: TStringField;
    qryConfigHDMRG: TStringField;
    qryConfigBDLEN: TStringField;
    qryConfigLFMRG: TStringField;
    qryConfigOP_NO: TStringField;
    qryConfigAPP_SAUP: TStringField;
    qryConfigAPP_NAME1: TStringField;
    qryConfigAPP_NAME2: TStringField;
    qryConfigAPP_NAME3: TStringField;
    qryConfigAPP_NAME4: TStringField;
    qryConfigAPP_NO: TStringField;
    qryConfigAPP_CST: TStringField;
    qryConfigMNF_SAUP: TStringField;
    qryConfigMNF_NAME1: TStringField;
    qryConfigMNF_NAME2: TStringField;
    qryConfigMNF_NAME3: TStringField;
    qryConfigMNF_NAME4: TStringField;
    qryConfigBANK_CD: TStringField;
    qryConfigBANK: TStringField;
    qryConfigBANK_ACC: TStringField;
    qryConfigCST1: TStringField;
    qryConfigCST2: TStringField;
    qryConfigPrtNm: TStringField;
    qryConfigPrtNo: TIntegerField;
    qryConfigAutoGubun: TStringField;
    qryConfigJecw: TStringField;
    qryConfigCST3: TStringField;
    qryIns: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    StringField39: TStringField;
    IntegerField1: TIntegerField;
    StringField40: TStringField;
    StringField41: TStringField;
    StringField42: TStringField;
    qryMod: TADOQuery;
    StringField43: TStringField;
    StringField44: TStringField;
    StringField45: TStringField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    StringField66: TStringField;
    StringField67: TStringField;
    StringField68: TStringField;
    StringField69: TStringField;
    StringField70: TStringField;
    StringField71: TStringField;
    StringField72: TStringField;
    StringField73: TStringField;
    StringField74: TStringField;
    StringField75: TStringField;
    StringField76: TStringField;
    StringField77: TStringField;
    StringField78: TStringField;
    StringField79: TStringField;
    StringField80: TStringField;
    StringField81: TStringField;
    IntegerField2: TIntegerField;
    StringField82: TStringField;
    StringField83: TStringField;
    StringField84: TStringField;
    procedure FormShow(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    //Data Controls
    FWork : TProgramControlType;
    procedure ReadData;
    procedure SetData;
    procedure SaveData;
    procedure UpdateData;

    //SQL Query Controls
    procedure SetParam(qry : TADOQuery);
  public
    { Public declarations }
  end;

var
  Config_MSSQL_frm: TConfig_MSSQL_frm;

implementation

uses MSSQL, Commonlib;    

{$R *.dfm}

{ TConfig_MSSQL_frm }

procedure TConfig_MSSQL_frm.ReadData;
begin
  with qryConfig do
  begin
    try
      Close;
      Open;

      //쿼리를 실행하여 해당 데이터행수가 0일때 Exception발생
      IF qryConfig.RecordCount = 0 Then
      begin
        //Raise Exception NODATA
        FWork := ctInsert;
//        ShowMessage('환경설정 데이터가 존재하지 않습니다'#13#10'데이터를 새로 입력하시기 바랍니다');
      end
      else
      begin
        //데이터가 존재한다면 해당 데이터를 컨트롤에 맞추어 출력
        FWork := ctModify;
        SetData;
      end;
    finally
      qryConfig.Close;
    end;
  end;
end;

procedure TConfig_MSSQL_frm.SetData;
var
  TempInt : Integer;
begin
  edt_SanghoKOR.Text := qryConfigNAME1.AsString;
  edt_SanghoENG.Text := qryConfigENAME1.AsString;

  edt_NameKOR.Text := qryConfigNAME2.AsString;
  edt_NameENG.Text := qryConfigENAME2.AsString;

  edt_Addr1KOR.Text := qryConfigADDR1.AsString;
  edt_Addr2KOR.Text := qryConfigADDR2.AsString;
  edt_Addr3KOR.Text := qryConfigADDR3.AsString;

  edt_Addr1ENG.Text := qryConfigEADDR1.AsString;
  edt_Addr2ENG.Text := qryConfigEADDR2.AsString;
  edt_Addr3ENG.Text := qryConfigEADDR3.AsString;

  edt_Tel.Text := qryConfigENAME3.AsString;

  Edt_TradeSingoNo.Text := qryConfigTRAD_NO.AsString;
  edt_CompanyNo.Text := qryConfigSAUP_NO.AsString;

  Edt_OpenPolicyNo.Text := qryConfigOP_NO.AsString;

  TempInt := StrToIntDef(qryConfigJecw.AsString,-1);
  IF TempInt = -1 Then
  begin
    ShowMessage('적하보험참조 항목을 가져올 수 없습니다. 자동으로 9로 셋팅합니다.'#13#10'확인후 수정바랍니다.');
    com_JeokHa.ItemIndex := 8;
  end
  else
  begin
    com_JeokHa.ItemIndex := TempInt-1;
  end;

  TempInt := StrToIntDef(qryConfigSIGN_C.AsString,-1);
  IF TempInt = -1 then
  begin
    ShowMessage('전자서명참조 항목을 가져올 수 없습니다. 자동으로 1로 셋팅합니다.'#13#10'해당 항목 확인 후에 수정바랍니다.');
  end
  else
  begin
    com_SignRef.ItemIndex := TempInt-1;
  end;

  edt_Sign.Text := qryConfigSIGN.AsString;

  TempInt := StrToIntDef(qryConfigAutoGubun.AsString,-1);
  if TempInt = -1 then
    ShowMessage('관리번호 설정을 가져올 수 없습니다. 자동으로 "년월 + 일련번호"로 셋팅합니다.'#13#10'확인 후 수정바랍니다.');

  Case TempInt of
     0 : Rad_0.Checked := True;
     1 : Rad_1.Checked := True;
     2 : Rad_2.Checked := True;
     3 : Rad_3.Checked := True;
     4 : Rad_4.Checked := True;
  end;

  Chk_Added.Checked := AnsiCompareText(qryConfigCST3.AsString,'1') = 0 ;

end;

procedure TConfig_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadData;
end;

procedure TConfig_MSSQL_frm.SaveData;
begin
  SetParam(qryIns);
end;

procedure TConfig_MSSQL_frm.UpdateData;
begin
  SetParam(qryMod);
end;

procedure TConfig_MSSQL_frm.SetParam(qry: TADOQuery);
begin
  with qry do
  begin
    Parameters.ParamByName('NAME1'    ).Value := edt_SanghoKOR.Text;
    Parameters.ParamByName('NAME2'    ).Value := edt_NameKOR.Text;
    Parameters.ParamByName('TRAD_NO'  ).Value := Edt_TradeSingoNo.Text;
    Parameters.ParamByName('SAUP_NO'  ).Value := edt_CompanyNo.Text;
    Parameters.ParamByName('ADDR1'    ).Value := edt_Addr1KOR.Text;
    Parameters.ParamByName('ADDR2'    ).Value := edt_Addr2KOR.Text;
    Parameters.ParamByName('ADDR3'    ).Value := edt_Addr3KOR.Text;
    Parameters.ParamByName('ENAME1'   ).Value := edt_SanghoENG.Text;
    Parameters.ParamByName('ENAME2'   ).Value := edt_NameENG.Text;
    Parameters.ParamByName('ENAME3'   ).Value := edt_Tel.Text;
    Parameters.ParamByName('EADDR1'   ).Value := edt_Addr1ENG.Text;
    Parameters.ParamByName('EADDR2'   ).Value := edt_Addr2ENG.Text;
    Parameters.ParamByName('EADDR3'   ).Value := edt_Addr3ENG.Text;
    Parameters.ParamByName('SIGN_C'   ).Value := com_SignRef.ItemIndex +1;
    Parameters.ParamByName('SIGN'     ).Value := edt_Sign.Text;
    Parameters.ParamByName('NMLEN'    ).Value := '';
    Parameters.ParamByName('SZLEN'    ).Value := '';
    Parameters.ParamByName('HDMRG'    ).Value := '';
    Parameters.ParamByName('BDLEN'    ).Value := '';
    Parameters.ParamByName('LFMRG'    ).Value := '';
    Parameters.ParamByName('OP_NO'    ).Value := Edt_OpenPolicyNo.Text;
    Parameters.ParamByName('APP_SAUP' ).Value := '';
    Parameters.ParamByName('APP_NAME1').Value := '';
    Parameters.ParamByName('APP_NAME2').Value := '';
    Parameters.ParamByName('APP_NAME3').Value := '';
    Parameters.ParamByName('APP_NAME4').Value := '';
    Parameters.ParamByName('APP_NO'   ).Value := '';
    Parameters.ParamByName('APP_CST'  ).Value := '';
    Parameters.ParamByName('MNF_SAUP' ).Value := '';
    Parameters.ParamByName('MNF_NAME1').Value := '';
    Parameters.ParamByName('MNF_NAME2').Value := '';
    Parameters.ParamByName('MNF_NAME3').Value := '';
    Parameters.ParamByName('MNF_NAME4').Value := '';
    Parameters.ParamByName('BANK_CD'  ).Value := '';
    Parameters.ParamByName('BANK'     ).Value := '';
    Parameters.ParamByName('BANK_ACC' ).Value := '';
    Parameters.ParamByName('CST1'     ).Value := '';
    Parameters.ParamByName('CST2'     ).Value := '';
    Parameters.ParamByName('PrtNm'    ).Value := '';
    Parameters.ParamByName('PrtNo'    ).Value := '0';

    IF Rad_0.Checked Then Parameters.ParamByName('AutoGubun').Value := '0'
    else IF Rad_1.Checked Then Parameters.ParamByName('AutoGubun').Value := '1'
    else IF Rad_2.Checked Then Parameters.ParamByName('AutoGubun').Value := '2'
    else IF Rad_3.Checked Then Parameters.ParamByName('AutoGubun').Value := '3'
    else IF Rad_4.Checked Then Parameters.ParamByName('AutoGubun').Value := '4';

    Parameters.ParamByName('Jecw'     ).Value := com_JeokHa.ItemIndex + 1;
    if Chk_Added.Checked Then Parameters.ParamByName('CST3').Value := 1
    else Parameters.ParamByName('CST3').Value := 0;

    ExecSQL;

  end;
end;

procedure TConfig_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  IF ConfirmMessage('환경설정을 저장하시겠습니까?') then
  begin
    Case FWork of
      ctInsert : SaveData;
      ctModify : UpdateData;
    end;
    Close;
  end;
end;

end.
