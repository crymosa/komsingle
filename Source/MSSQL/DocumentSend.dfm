inherited DocumentSend_frm: TDocumentSend_frm
  Left = 648
  Caption = #47928#49436' '#51204#49569
  ClientHeight = 538
  ClientWidth = 767
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 767
    Height = 53
    Align = alTop
    DoubleBuffered = False
    TabOrder = 0
    object sSpeedButton3: TsSpeedButton
      Left = 340
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 176
      Height = 51
      AutoSnap = False
      Enabled = False
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 287
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #49325#51228
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_DelClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 19
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 700
      Top = 1
      Width = 11
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 711
      Top = 1
      Width = 55
      Height = 51
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      ImageIndex = 20
      Reflected = True
    end
    object sSpeedButton2: TsSpeedButton
      Left = 177
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 155
      Height = 25
      Caption = 'Send Documents'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 76
      Height = 15
      Caption = #51089#49457#47928#49436' '#51204#49569
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton1: TsSpeedButton
      Tag = 2
      Left = 232
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #51204#49569
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton1Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 28
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Tag = 2
      Left = 179
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #49440#53469
      Layout = blGlyphTop
      Spacing = 0
      OnClick = sSpeedButton4Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 17
      Reflected = True
    end
    object sSpeedButton6: TsSpeedButton
      Left = 285
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton7: TsSpeedButton
      Tag = 2
      Left = 342
      Top = 1
      Width = 59
      Height = 51
      Cursor = crHandPoint
      Caption = #50724#47448#54869#51064
      Layout = blGlyphTop
      Spacing = 0
      OnClick = sSpeedButton7Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 37
      Reflected = True
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 53
    Width = 767
    Height = 485
    Align = alClient
    Color = clWhite
    DataSource = dsReadyList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Tag'
        Title.Alignment = taCenter
        Title.Caption = #49440#53469
        Width = 39
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DOCID'
        Title.Alignment = taCenter
        Title.Caption = #47928#49436'ID'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 190
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'MSEQ'
        Title.Alignment = taCenter
        Title.Caption = #52264#49688
        Width = 36
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRCONTROL'
        Title.Alignment = taCenter
        Title.Caption = #48320#44221
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SRVENDOR'
        Title.Alignment = taCenter
        Title.Caption = #49688#49888#52376
        Width = 68
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRDATE'
        Title.Alignment = taCenter
        Title.Caption = #51456#48708#51068#51088
        Width = 99
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRTIME'
        Title.Alignment = taCenter
        Title.Caption = #51456#48708#49884#44036
        Width = 52
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRSTATE'
        Title.Alignment = taCenter
        Title.Caption = #49345#53468
        Width = 74
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'ReadyUser'
        Title.Alignment = taCenter
        Title.Caption = #51456#48708#51088
        Width = 55
        Visible = True
      end>
  end
  object sPanel2: TsPanel [2]
    Left = 267
    Top = 232
    Width = 161
    Height = 129
    DoubleBuffered = False
    Font.Charset = HANGEUL_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object sLabel2: TsLabel
      Left = 16
      Top = 16
      Width = 76
      Height = 15
      Caption = #51089#49457#47928#49436' '#51204#49569
      ParentFont = False
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object lbl_Step1: TsLabel
      Left = 24
      Top = 40
      Width = 153
      Height = 15
      AutoSize = False
      Caption = '> UNH'#54028#51068' '#49373#49457
      ParentFont = False
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object lbl_Step3: TsLabel
      Left = 24
      Top = 72
      Width = 153
      Height = 15
      AutoSize = False
      Caption = '> UPLOAD '#47785#47197' '#49373#49457
      ParentFont = False
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object lbl_Step2: TsLabel
      Left = 24
      Top = 56
      Width = 153
      Height = 15
      AutoSize = False
      Caption = '> UNH'#54028#51068' '#48320#54872
      ParentFont = False
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object lbl_Step4: TsLabel
      Left = 24
      Top = 88
      Width = 153
      Height = 15
      AutoSize = False
      Caption = '> '#47928#49436' '#51204#49569
      ParentFont = False
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 88
    Top = 232
  end
  object qryReadyList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryReadyListAfterScroll
    AfterScroll = qryReadyListAfterScroll
    Parameters = <
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [DOCID]'
      '      ,[MAINT_NO]'
      '      ,[MSEQ]'
      '      ,[SRDATE]'
      '      ,[SRTIME]'
      '      ,[SRVENDOR]'
      '      ,[SRSTATE]'
      '      ,[SRCONTROL]'
      '      ,[SRFLAT]'
      '      ,[Tag]'
      '      ,[TableName]'
      '      ,[ReadyUser]'
      '      ,[ReadyDept]'
      '  FROM [SR_READY]'
      ' WHERE [ReadyUser] = :ReadyUser')
    Left = 96
    Top = 136
    object qryReadyListDOCID: TStringField
      FieldName = 'DOCID'
      Size = 10
    end
    object qryReadyListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryReadyListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryReadyListSRDATE: TStringField
      FieldName = 'SRDATE'
      OnGetText = qryReadyListSRDATEGetText
      Size = 8
    end
    object qryReadyListSRTIME: TStringField
      FieldName = 'SRTIME'
      Size = 6
    end
    object qryReadyListSRVENDOR: TStringField
      FieldName = 'SRVENDOR'
      Size = 17
    end
    object qryReadyListSRSTATE: TStringField
      FieldName = 'SRSTATE'
      Size = 10
    end
    object qryReadyListSRCONTROL: TStringField
      FieldName = 'SRCONTROL'
    end
    object qryReadyListSRFLAT: TMemoField
      FieldName = 'SRFLAT'
      BlobType = ftMemo
    end
    object qryReadyListTag: TBooleanField
      FieldName = 'Tag'
      OnGetText = qryReadyListTagGetText
    end
    object qryReadyListTableName: TStringField
      FieldName = 'TableName'
    end
    object qryReadyListReadyUser: TStringField
      FieldName = 'ReadyUser'
      Size = 10
    end
    object qryReadyListReadyDept: TStringField
      FieldName = 'ReadyDept'
    end
  end
  object dsReadyList: TDataSource
    DataSet = qryReadyList
    Left = 128
    Top = 136
  end
  object qryReadyDel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Size = -1
        Value = Null
      end
      item
        Name = 'DOCID'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35)'
      #9#9',@DOCID varchar(10)'
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT_' +
        'NO')
    Left = 96
    Top = 168
  end
  object qryDelAll: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM SR_READY'
      'WHERE ReadyUser = :ReadyUser'
      'AND Tag =1')
    Left = 128
    Top = 168
  end
  object qryUpdateState: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'STATE_MESSAGE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'AMDNO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE SR_READY'
      'SET SRSTATE = :STATE_MESSAGE'
      'WHERE DOCID = :DOCID'
      'AND '
      'MAINT_NO = :MAINT_NO'
      'AND'
      'MSEQ = :MSEQ'
      'AND'
      'SRCONTROL = :AMDNO')
    Left = 160
    Top = 168
  end
  object qryCompleteList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryReadyListAfterScroll
    AfterScroll = qryReadyListAfterScroll
    Parameters = <
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [DOCID]'
      '      ,[MAINT_NO]'
      '      ,[MSEQ]'
      '      ,[SRDATE]'
      '      ,[SRTIME]'
      '      ,[SRVENDOR]'
      '      ,[SRSTATE]'
      '      ,[SRCONTROL]'
      '      ,[SRFLAT]'
      '      ,[Tag]'
      '      ,[TableName]'
      '      ,[ReadyUser]'
      '      ,[ReadyDept]'
      '  FROM [SR_READY]'
      ' WHERE [ReadyUser] = :ReadyUser'
      'AND Tag = 1')
    Left = 88
    Top = 264
    object StringField1: TStringField
      FieldName = 'DOCID'
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object IntegerField1: TIntegerField
      FieldName = 'MSEQ'
    end
    object StringField3: TStringField
      FieldName = 'SRDATE'
      OnGetText = qryReadyListSRDATEGetText
      Size = 8
    end
    object StringField4: TStringField
      FieldName = 'SRTIME'
      Size = 6
    end
    object StringField5: TStringField
      FieldName = 'SRVENDOR'
      Size = 17
    end
    object StringField6: TStringField
      FieldName = 'SRSTATE'
      Size = 10
    end
    object StringField7: TStringField
      FieldName = 'SRCONTROL'
    end
    object MemoField1: TMemoField
      FieldName = 'SRFLAT'
      BlobType = ftMemo
    end
    object BooleanField1: TBooleanField
      FieldName = 'Tag'
      OnGetText = qryReadyListTagGetText
    end
    object StringField8: TStringField
      FieldName = 'TableName'
    end
    object StringField9: TStringField
      FieldName = 'ReadyUser'
      Size = 10
    end
    object StringField10: TStringField
      FieldName = 'ReadyDept'
    end
  end
end
