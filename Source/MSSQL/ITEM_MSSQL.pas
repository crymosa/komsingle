unit ITEM_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ITEM, sSkinProvider, StdCtrls, DBCtrls, sDBMemo, Mask, sDBEdit,
  Grids, DBGrids, acDBGrid, sComboBox, Buttons, sBitBtn, sEdit, sLabel,
  ExtCtrls, sSplitter, sSpeedButton, sPanel, DB, ADODB, TypeDefine;

type
  TITEM_MSSQL_frm = class(TITEM_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Btn_NewClick(Sender: TObject);
    procedure doRefresh;
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSQL : String;
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ITEM_MSSQL_frm: TITEM_MSSQL_frm;

implementation

uses MSSQL, Dlg_ITEM, ICON;

{$R *.dfm}

procedure TITEM_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TITEM_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    qryList.Close;
    SQL.Text := FSQL;

    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
      1: SQL.Add('WHERE [SNAME] LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;

    Open;
  end;
end;

procedure TITEM_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TITEM_MSSQL_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TITEM_MSSQL_frm.sEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TITEM_MSSQL_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  IF ActiveControl = sEdit1 Then Exit;
  inherited;

end;

procedure TITEM_MSSQL_frm.Btn_NewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;
  Case (sender as TsSpeedButton).Tag of
    0: TempControl := ctInsert;
    1: TempControl := ctModify;
    2: TempControl := ctDelete;
  end;

  Dlg_ITEM_frm := TDlg_ITEM_frm.Create(Self);

  try
    IF Dlg_ITEM_frm.Run(TempControl,qryList.Fields) = mrOk Then
    begin
      IF TempControl IN [ctinsert,ctDelete] Then
      begin
        qryList.Close;
        qrylist.Open;
      end
      else if TempControl = ctModify Then
      begin
        doRefresh;
      end;
    end;
  finally
    FreeAndNil(Dlg_ITEM_frm);
  end;
end;

procedure TITEM_MSSQL_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

procedure TITEM_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TITEM_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  ITEM_MSSQL_frm := nil;
end;

end.
