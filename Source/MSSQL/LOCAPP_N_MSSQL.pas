unit LOCAPP_N_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LOCAPP_N, sSkinProvider, DBCtrls, sDBComboBox, StdCtrls,
  sDBMemo, sDBEdit, sComboBox, sButton, sEdit, Mask, sMaskEdit, Grids,
  DBGrids, acDBGrid, ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel,
  ExtCtrls, sSplitter, sPanel, DB, ADODB, StrUtils,DateUtils, TypeDefine;

type
  TLOCAPP_N_MSSQL_frm = class(TLOCAPP_N_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListDATEE: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListAPP_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListLCNO: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    edt_N4487: TsEdit;
    edt_N4025: TsEdit;
    qryListBUSINESSNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
    edt_TRANSPRT: TsEdit;
    Edt_DOC_DTL: TsEdit;
    edt_PAYMENT: TsEdit;
    edt_NAT: TsEdit;
    sSpeedButton14: TsSpeedButton;
    sComboBox2: TsComboBox;
    qryReady: TADOQuery;
    qryDelete: TADOQuery;
    com_MakeCodeBank: TsComboBox;
    qryMakeCodeBank: TADOQuery;
    sSpeedButton15: TsSpeedButton;
    sSpeedButton16: TsSpeedButton;
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure Btn_NewClick(Sender: TObject);
    procedure sPageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_TempClick(Sender: TObject);
    procedure qryListAfterInsert(DataSet: TDataSet);
    procedure DB_OWNERCODEDblClick(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton6DblClick(Sender: TObject);
    procedure sDBEdit7DblClick(Sender: TObject);
    procedure sSpeedButton7Click(Sender: TObject);
    procedure sDBEdit10DblClick(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure sSpeedButton9Click(Sender: TObject);
    procedure sSpeedButton13Click(Sender: TObject);
    procedure sSpeedButton10Click(Sender: TObject);
    procedure sSpeedButton11Click(Sender: TObject);
    procedure sSpeedButton12Click(Sender: TObject);
    procedure sSpeedButton14Click(Sender: TObject);
    procedure sDBEdit75Enter(Sender: TObject);
    procedure sComboBox2Exit(Sender: TObject);
    procedure Btn_ReadyClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterPost(DataSet: TDataSet);
    procedure com_MakeCodeBankSelect(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sDBEdit10Exit(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Btn_SaveClick(Sender: TObject);
    procedure sDBEdit45Exit(Sender: TObject);
    procedure sSpeedButton16Click(Sender: TObject);
    procedure com_MakeCodeBankExit(Sender: TObject);
    procedure edt_MAINT_NO2Enter(Sender: TObject);
    procedure qryListBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FFIXED_MAINCODE : String;
    FCurrentWork : TProgramControlType;
    FBeforePageIndex : integer;
    FSQL : string;
    arrCodeBank : array of string;
    procedure Serfixed_Maincode;
    procedure MakeCodeBank;
    procedure CreateReady;
    procedure PanelReadOnly(var UserPanel: TsPanel; bValue: Boolean);
    procedure ReadyDoc;
    procedure ReadMstList;
    procedure DeleteDocumnet;
    function FindCode(Sender : TsDBEdit;DM: TADOQuery):string;
    procedure setContents(Sender : TsDBEdit; sValue : String);
    function CHECK_VALIDITY: String;
  public
    { Public declarations }
  end;

var
  LOCAPP_N_MSSQL_frm: TLOCAPP_N_MSSQL_frm;

implementation

uses MSSQL, Commonlib, ICON, CodeContents, AutoNo, Dlg_Customer,
  Dlg_FindBankCode, VarDefine, Dlg_FindDefineCode, Dlg_RecvSelect,
  CreateDocuments, Dlg_ErrorMessage, Dlg_APPPCR_newDocSelect,
  Dlg_LOCAPP_SelectCopy;

{$R *.dfm}

procedure TLOCAPP_N_MSSQL_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  if qryListCHK2.IsNull then Exit;

  CASE qryListCHK2.AsInteger of
    0: Text := '임시';
    1: Text := '문서';
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  sDate : String;
  sState : String;
begin
  sDate := qryListCHK3.AsString;

  IF StringReplace(sDate,' ','',[rfReplaceAll]) <> '' Then
  begin
    CASE StrToInt(RightStr(sDate,1)) OF
      1: sState := '송신준비';
      2: sState := '준비삭제';
      3: sState := '변환오류';
      4: sState := '통신오류';
      5: sState := '송신완료';
    end;
    Text := FormatDateTime('YYYY.MM.DD',decodeCharDate(LeftStr(sDate,8)))+' '+sState;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.CreateReady;
begin
  //관리번호 ReadOnly
//  sDBEdit1.ReadOnly := not (FCurrentWork = ctInsert);

  edt_MAINT_NO3.ReadOnly := not(FCurrentWork = ctInsert);
  IF edt_MAINT_NO3.ReadOnly Then
    edt_MAINT_NO3.Color := clBtnFace
  else
    edt_MAINT_NO3.Color := $00B5F0C0;

  edt_MAINT_NO2.ReadOnly := FCurrentWork in [ctView,ctModify];
  IF edt_MAINT_NO2.ReadOnly Then
    edt_MAINT_NO2.Color := clBtnFace
  else
    edt_MAINT_NO2.Color := $00B5F0C0;

  //Button State
  Btn_New.Enabled    := FCurrentWork = ctView;
  Btn_Modify.Enabled := FCurrentWork = ctView;
  Btn_Del.Enabled    := FCurrentWork = ctView;

  Btn_Temp.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Save.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Cancel.Enabled := FCurrentWork IN [ctInsert,ctModify];

  Btn_Print.Enabled  := FCurrentWork = ctView;
  Btn_Ready.Enabled  := FCurrentWork = ctView;

  //Lock ListGrid
  IF sPageControl1.ActivePageIndex = 0 Then
    sPageControl1.ActivePageIndex := 1;

  sPageControl1.Pages[0].Enabled := FCurrentWork = ctView;
  sDBGrid1.Enabled := FCurrentWork = ctView;

//  PanelReadOnly(sPanel2, (FCurrentWork = ctView));
  PanelReadOnly(sPanel3, (FCurrentWork = ctView));
  PanelReadOnly(sPanel8, (FCurrentWork = ctView));
  PanelReadOnly(sPanel10,(FCurrentWork = ctView));

  edt_N4487.Visible := not (FCurrentWork = ctView);
  edt_N4025.Visible := not (FCurrentWork = ctView);
  edt_TRANSPRT.Visible := not (FCurrentWork = ctView);
  Edt_DOC_DTL.Visible  := not (FCurrentWork = ctView);
  edt_PAYMENT.Visible  := not (FCurrentWork = ctView);
  edt_NAT.Visible      := not (FCurrentWork = ctView);


  IF FCurrentWork = ctModify Then
  begin
    edt_N4487.Text := qryListLOC_TYPENAME.AsString;
    edt_N4025.Text := qryListBUSINESSNAME.AsString;
    edt_TRANSPRT.Text := qryListTRANSPRTNAME.AsString;
    Edt_DOC_DTL.Text  := qryListDOC_DTLNAME.AsString;
    edt_PAYMENT.Text  := qryListPAYMENTNAME.AsString;
    edt_NAT.Text      := qryListDESTNAME.AsString;
  end
  else
  if FCurrentWork = ctInsert Then
  begin
    edt_N4487.Clear;
    edt_N4025.Clear;
    edt_TRANSPRT.Clear;
    Edt_DOC_DTL.Clear;
    edt_PAYMENT.Clear;
    edt_NAT.Clear;
  end;

end;

procedure TLOCAPP_N_MSSQL_frm.PanelReadOnly(var UserPanel: TsPanel;
  bValue: Boolean);
var
  nLoop ,
  MaxCount : integer;
  UserDBEdit : TsDBEdit;
  UserDBMemo : TsDBMemo;
begin
  MaxCount := UserPanel.ControlCount;
  for nLoop := 0 to MaxCount -1 do
  begin
    IF UserPanel.Controls[nLoop] is TsDBEdit Then
    begin
      UserDBEdit := TsDBEdit(UserPanel.Controls[nLoop]);

      IF UserDBEdit.Color = clGray Then
        Continue;

      UserDBEdit.ReadOnly := bValue;

      IF UserDBEdit.Tag = 9 Then
        UserDBEdit.Color := $00B5F0C0
      else
      IF (not bValue) and (UserDBEdit.BoundLabel.Active) and (fsbold in UserDBEdit.BoundLabel.Font.Style) Then
        UserDBEdit.Color := clYellow
      else
        UserDBEdit.Color := clWhite;

    end
    else
    IF UserPanel.Controls[nLoop] is TsDBMemo Then
    begin
      UserDBMemo := TsDBMemo(UserPanel.Controls[nLoop]);
      UserDBMemo.ReadOnly := bValue;
    end;

  end;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_NewClick(Sender: TObject);
var
  FNewDocCopy : Boolean;
begin
  inherited;
  {
  IF not Assigned(dlg_APPPCR_newDocSelect_frm) Then dlg_APPPCR_newDocSelect_frm := Tdlg_APPPCR_newDocSelect_frm.Create(Self);
  dlg_APPPCR_newDocSelect_frm.sButton3.Caption := '물품매도확약서(개설자-수신)에서 선택';
  dlg_APPPCR_newDocSelect_frm.sButton3.CommandLinkHint := '물품매도확약서에서 데이터를 가져옵니다';
  try
    Case dlg_APPPCR_newDocSelect_frm.ShowModal of
      mrOk : FNewDocCopy := False;
      mrYes :
      begin
        FNewDocCopy := True;
        Exit;
      end;
      mrCancel : Exit;
    end;
  finally
    FreeAndNil( dlg_APPPCR_newDocSelect_frm );
  end;
  }

  sPageControl1.ActivePageIndex := 1;
  DB_OWNERCODE.SetFocus;

  Case (Sender as TsSpeedButton).Tag of
    0:
    begin
      DMCodeContents.CODEREFRESH;
      MakeCodeBank;
//      IF DMAutoNo.GetDocumentNo('LOCAPP') = '' Then
//      begin
//        Raise Exception.Create('내국신용장은 관리번호 직접입력이 불가능합니다'#13#10'환경설정에서 변경해주세요');
//      end
//      else

      IF not DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) and (DMCodeContents.GEOLAECHEO.FieldByName('ESU1').AsString = '')  then
      begin
        Raise Exception.Create('거래처관리의 개설업체(00000) 식별자부호가 없습니다'#13#10'입력 후 진행하세요');
      end;

      FCurrentWork := ctInsert;
      CreateReady;
      qryList.Append;
    end;

    1:
    begin
      FCurrentWork := ctModify;
      CreateReady;
      qryList.Edit;
    end;

    2:
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까? 데이터 복구는 불가능합니다.') Then
      begin
        FCurrentWork := ctDelete;
        DeleteDocumnet;
      end;
    end;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sPageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
    FBeforePageIndex := sPageControl1.ActivePageIndex
end;

procedure TLOCAPP_N_MSSQL_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
  begin
    IF sPageControl1.ActivePageIndex = 0 Then
    begin
      sPageControl1.ActivePageIndex := FBeforePageIndex;
      Self.Width := 826;
    end
    else
      Self.Width := 826;
  end
  else
  begin
    IF sPageControl1.ActivePageIndex = 0 Then
    begin
      Self.Width := 919;
    end
    else
      Self.Width := 826;
  end;

end;

procedure TLOCAPP_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCurrentWork := ctView;
  sMaskEdit6.Text := '20000101';
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
  
  MakeCodeBank;
  Serfixed_Maincode;

  FSQL := qryList.SQL.Text;
  ReadMstList;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  IF not ConfirmMessage('작업을 취소하시겠습니까?'#13#10'지금까지 작성했던 데이터들은 삭제됩니다.') Then Exit;

  FCurrentWork := ctView;
//  DMMssql.RollbackTrans;
  qryList.Cancel;
  CreateReady;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_TempClick(Sender: TObject);
begin
  inherited;
  IF Pos('XX',qryListMAINT_NO.AsString) > 0 Then
  begin
    com_MakeCodeBank.SetFocus;
    raise Exception.Create('은행을 선택해주세요')
  end;
  qryListCHK2.AsInteger := 0;
  qryList.Post;
  FCurrentWork := ctView;
  CreateReady;
end;

procedure TLOCAPP_N_MSSQL_frm.qryListAfterInsert(DataSet: TDataSet);
begin
  inherited;
//  IF sPageControl1.ActivePageIndex = 1 Then
//    sDBEdit1.SetFocus;

  DMCodeContents.CODEREFRESH;

  //기본 개설의뢰인
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    qryListAPPLIC.AsString   :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    qryListAPPLIC1.AsString  :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    qrylistAPPLIC2.AsString  :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    qrylistAPPADDR1.AsString := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    qrylistAPPADDR2.AsString := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    qrylistAPPADDR3.AsString := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    qryListAPPLIC3.AsString  := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
  end;

//  qryListMAINT_NO.AsString := DMAutoNo.GetDocumentNo('LOCAPP');
  edt_MAINT_NO3.Text := DMAutoNo.GetDocumentNo('LOCAPP');
  edt_MAINT_NO3.ReadOnly := not(Trim(edt_MAINT_NO3.Text) = '');
  //준비서류
  qryListDOCCOPY1.AsInteger := 1;
  qryListDOCCOPY2.AsInteger := 1;
  qryListDOCCOPY3.AsInteger := 0;
  qryListDOCCOPY4.AsInteger := 1;
  qryListDOCCOPY5.AsInteger := 1;


  qryListUSER_ID.AsString := LoginData.sID;
  qryListDATEE.AsString := FormatDateTime('YYYYMMDD',Now);
  qryListMESSAGE1.AsString := '9';
  qryListMESSAGE2.AsString := 'AB';
  qrylistOPEN_NO.AsInteger := 1;

  qryListEXNAME1.AsString := DMCodeContents.ConfigNAME1.AsString;
  qryListEXNAME2.AsString := DMCodeContents.ConfigNAME2.AsString;
  qryListEXNAME3.AsString := DMCodeContents.ConfigSIGN.AsString;

  edt_MAINT_NO1.Text := FFIXED_MAINCODE;
  edt_MAINT_NO2.Clear;

end;

procedure TLOCAPP_N_MSSQL_frm.DB_OWNERCODEDblClick(Sender: TObject);
begin
  inherited;
  IF not (FCurrentWork in [ctinsert,ctModify]) Then Exit;

  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      IF Run = mrOK Then
      begin
        IF (Sender as TsDBEdit).DataField = 'APPLIC' then
        begin
          qryListAPPLIC.AsString   :=  DataSource1.DataSet.FieldByName('CODE').AsString;
          qryListAPPLIC1.AsString  :=  DataSource1.DataSet.FieldByName('ENAME').AsString;
          qrylistAPPLIC2.AsString  :=  DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          qrylistAPPADDR1.AsString := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          qrylistAPPADDR2.AsString := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          qrylistAPPADDR3.AsString := DataSource1.DataSet.FieldByName('ADDR3').AsString;
          qryListAPPLIC3.AsString  := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        end
        else
        if (Sender as TsDBEdit).DataField = 'BENEFC' then
        begin
          qryListBENEFC.AsString      := DataSource1.DataSet.FieldByName('CODE').AsString;
          qryListBENEFC1.AsString     := DataSource1.DataSet.FieldByName('ENAME').AsString;
          qryListBENEFC2.AsString     := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          qryListBNFEMAILID.AsString  := DataSource1.DataSet.FieldByName('EMAIL_ID').AsString;
          qryListBNFDOMAIN.AsString   := DataSource1.DataSet.FieldByName('EMAIL_DOMAIN').AsString;
          qryListBNFADDR1.AsString    := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          qryListBNFADDR2.AsString    := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          qryListBNFADDR3.AsString    := DataSource1.DataSet.FieldByName('ADDR3').AsString;
          qryListBENEFC3.AsString     := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        end
        else
        if (Sender as TsDBedit).DataField = 'IM_NAME' Then
        begin
          qrylistIM_NAME.AsString :=   DataSource1.DataSet.FieldByName('CODE').AsString;
          qrylistIM_NAME1.AsString :=  DataSource1.DataSet.FieldByName('ENAME').AsString;
          qrylistIM_NAME2.AsString :=  DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          qrylistIM_NAME3.AsString :=  DataSource1.DataSet.FieldByName('ADDR1').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  DB_OWNERCODEDblClick(DB_OWNERCODE);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton6DblClick(Sender: TObject);
begin
  inherited;
  DB_OWNERCODEDblClick(DB_BNFCODE);
end;

procedure TLOCAPP_N_MSSQL_frm.sDBEdit7DblClick(Sender: TObject);
begin
  inherited;
  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      IF Run = mrOK Then
      begin
        qryListAP_BANK.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
        qryListAP_BANK1.AsString := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        qryListAP_BANK2.AsString := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton7Click(Sender: TObject);
begin
  inherited;
  sDBEdit7DblClick(sDBEdit7);
end;

procedure TLOCAPP_N_MSSQL_frm.sDBEdit10DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBEdit).ReadOnly Then Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //내국신4025 - 공급물품명세구분
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['BUSINESS']) Then
      begin
          IF Run('NAEGUKSIN4025') = mrOk Then
          begin
            qryListBUSINESS.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            edt_N4025.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
      else
      //내국신4487
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['LOC_TYPE']) Then
      begin
          IF Run('NAEGUKSIN4487') = mrOk Then
          begin
            qryListLOC_TYPE.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            edt_N4487.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
      else
      //내국신4487
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['DEST']) Then
      begin
          IF Run('NATION') = mrOk Then
          begin
            qryListDEST.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            edt_NAT.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
      else
      //단위
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['LOC_AMTC']) Then
      begin
          IF Run('TONGHWA') = mrOk Then
          begin
            (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          end;
      end
      else
      //분할
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['TRANSPRT']) Then
      begin
          IF Run('PSHIP') = mrOk Then
          begin
            qryListTRANSPRT.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            edt_TRANSPRT.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
      else
      //내국신1001 = 개설근거 서류종류
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['DOC_DTL']) Then
      begin
          IF Run('NAEGUKSIN1001') = mrOk Then
          begin
            qryListDOC_DTL.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            Edt_DOC_DTL.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
      else
      //내국신4277 = 대금결제조건
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['PAYMENT']) Then
      begin
          IF Run('NAEGUKSIN4277') = mrOk Then
          begin
            qryListPAYMENT.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            edt_PAYMENT.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit10);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton9Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit16);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton13Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit66);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton10Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit52);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton11Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit55);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton12Click(Sender: TObject);
begin
  inherited;
  sDBEdit10DblClick(sDBEdit70);
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton14Click(Sender: TObject);
begin
  inherited;
  DB_OWNERCODEDblClick(sDBEdit62);  
end;

procedure TLOCAPP_N_MSSQL_frm.sDBEdit75Enter(Sender: TObject);
begin
  inherited;
  IF FCurrentWork = ctView Then Exit;
  with sComboBox2 do
  begin
    Visible := False;
    Left := (Sender as TsDBEdit).Left;
    Top := (Sender as TsDBEdit).Top;
    Width := (Sender as TsDBEdit).Width;

    Tag := (Sender as TsDBedit).Tag;

    IF (Sender as TsDBEdit).Name = 'DB_MESSAGE1' THEN
    begin
      Items.Clear;
      Items.Add('9 : ORIGINAL');
      Items.Add('7 : DUPLICATE');
      Items.Add('3 : DELETE');

      Perform(CB_SETDROPPEDWIDTH, 150, 0);

      Case qryListMESSAGE1.AsInteger of
        9: ItemIndex := 0;
        7: ItemIndex := 1;
        3: ItemIndex := 2;
      end;
    end
    else
    IF (Sender as TsDBEdit).Name = 'DB_MESSAGE2' Then
    begin
      items.Clear;
      Items.Add('AB : Message Acknowledgement');
      Items.Add('AP : Accepted');
      items.Add('NA : No Acknowledgement Needed');
      Items.Add('RE : Rejected');

      Perform(CB_SETDROPPEDWIDTH, 300, 0);

      ItemIndex := AnsiIndexText(TRIM(qryListMESSAGE2.AsString) ,['AB' , 'AP', 'NA' , 'RE' ]);
    end;

    Visible := True;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sComboBox2Exit(Sender: TObject);
begin
  inherited;
  IF qryList.State in [dsInsert,dsEdit] Then
  begin
    Case sComboBox2.Tag of
      100: qryListMESSAGE1.AsString := LeftStr(sComboBox2.Text,1);
      101: qryListMESSAGE2.AsString := LeftStr(sComboBox2.Text,2);
    end;
  end;
  sComboBox2.Visible := False;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_ReadyClick(Sender: TObject);
begin
  inherited;
  IF qryListCHK2.AsString = '0' Then
    Raise Exception.Create('임시문서는 전송할 수 없습니다');
  IF qryListCHK3.AsString <> '' Then
  begin
    if StrToInt(RightStr(qryListCHK3.AsString,1)) = 5  Then
    begin
      IF not ConfirmMessage('전송완료된 문서입니다. 재전송 하시겠습니까?') Then Exit;
    end;
  end
  else
    ReadyDoc;
end;

procedure TLOCAPP_N_MSSQL_frm.ReadyDoc;
var
  RecvSelect : TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat : String;
  ReadyDateTime : TDateTime;
  DocBookMark : Pointer;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    IF RecvSelect.ShowModal = mrOK Then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc  := qryListMAINT_NO.AsString;
      RecvFlat := APPPCR(RecvDoc,RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Close;
        Parameters.ParamByName('DOCID'    ).Value := 'APPPCR';
        Parameters.ParamByName('MAINT_NO' ).Value := RecvDoc;
        Parameters.ParamByName('MESQ'     ).Value := 0;
        Parameters.ParamByName('SRDATE'   ).Value := FormatDateTime('YYYYMMDD',ReadyDateTime);
        Parameters.ParamByName('SRTIME'   ).Value := FormatDateTime('HHNNSS',ReadyDateTime);
        Parameters.ParamByName('SRVENDOR' ).Value := RecvCode;

        IF StringReplace(qryListCHK3.AsString,' ','',[rfReplaceAll]) = '' Then
          Parameters.ParamByName('SRSTATE'  ).Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE'  ).Value := '재준비';

        Parameters.ParamByName('SRFLAT'   ).Value := RecvFlat;
        Parameters.ParamByName('Tag'      ).Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPPCR_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD',Now)+IntToStr(1);

        DocBookMark := qryList.GetBookmark;

        ExecSQL;


        ReadMstList;

        try
          IF qryList.BookmarkValid(DocBookMark) Then
          begin
            qryList.GotoBookmark(DocBookMark);
          end;
        finally
          qryList.FreeBookmark(DocBookMark);
        end;

      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.ReadMstList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
//    ParamByName('FROMDATE').Value := sMaskEdit6.Text;
//    ParamByName('TODATE'  ).Value := sMaskEdit7.Text;
//    IF sEdit3.Text <> '' Then
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+sEdit3.Text+'%'));
//    SQL.Add('AND USER_ID = '+QuotedStr(LoginData.sID));

      SQL.Add('ORDER BY DATEE DESC');
    Open;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadMstList;
end;

procedure TLOCAPP_N_MSSQL_frm.sEdit3KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  IF KEY = #13 Then ReadMstList;
end;

procedure TLOCAPP_N_MSSQL_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  qryList.Properties['Unique Table'].Value := 'LOCAPP';

  Btn_Modify.Enabled := qryList.RecordCount > 0;
  Btn_Del.Enabled    := qryList.RecordCount > 0;

  qryListAfterScroll(DataSet);  
end;

procedure TLOCAPP_N_MSSQL_frm.qryListAfterPost(DataSet: TDataSet);
var
  TempDocNo : string;
begin
  inherited;
  IF FCurrentWork = ctInsert Then
    DMAutoNo.RenewNo;

  IF FCurrentWork in [ctInsert,ctModify] Then
  begin
    TempDocNo := qryListMAINT_NO.AsString;
    qryList.Close;
    qryList.Open;
    qryList.Locate('MAINT_NO',TempDocNo,[]);
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.DeleteDocumnet;
begin
  with qryDelete do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
    ExecSQL;
  end;

  ReadMstList;

end;

procedure TLOCAPP_N_MSSQL_frm.MakeCodeBank;
begin
  with qryMakeCodeBank do
  begin
    Close;
    Open;
    com_MakeCodeBank.Items.Clear;
    SetLength(arrCodeBank,qryMakeCodeBank.RecordCount);
    while not Eof do
    begin
      arrCodeBank[RecNo-1] := qryMakeCodeBank.FieldByName('CODE').AsString;
      com_MakeCodeBank.Items.Add('['+qryMakeCodeBank.FieldByName('CODE').AsString+']'+qryMakeCodeBank.FieldByName('NAME').AsString);
      qryMakeCodeBank.Next;
    end;
  end;
  com_MakeCodeBank.Perform(CB_SETDROPPEDWIDTH,120,0);
end;

procedure TLOCAPP_N_MSSQL_frm.com_MakeCodeBankSelect(Sender: TObject);
Const
  DOC_CODE : string = 'LOCAPP';
var
  Before_MAINT_NO : String;
  DOC_ID : String;
  Start_POS : integer;
begin
  inherited;
  IF qryList.State = dsInsert then
  begin
    edt_MAINT_NO2.Text := arrCodeBank[com_MakeCodeBank.itemindex];
//    Before_MAINT_NO := qryListMAINT_NO.AsString;
//    DMCodeContents.CODEREFRESH;
//    DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]);
//    DOC_ID := DMCodeContents.GEOLAECHEO.FieldByName('ESU1').AsString;

//    Start_Pos := Length(DOC_CODE+DOC_ID);

//    qryListMAINT_NO.AsString := DOC_CODE+DOC_ID+arrCodeBank[com_MakeCodeBank.itemindex]+MidStr(Before_MAINT_NO,Start_POS+1,Length(Before_MAINT_NO)-Start_POS+1);
//    qryListMAINT_NO.AsString := StuffString(qryListMAINT_NO.AsString,Start_POS+1,2,arrCodeBank[com_MakeCodeBank.itemindex])
  end;
end;


procedure TLOCAPP_N_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
var
  BankCode : string;
  i : Integer;
  Cut_IDX : Integer;
begin
  inherited;
  DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]);
  BankCode := MidStr(qryListMAINT_NO.AsString,Length('LOCAPP'+DMCodeContents.GEOLAECHEO.FieldByName('ESU1').AsString)+1,2);

  for i := 0 to High(arrCodeBank) do
  begin
    IF BankCode = arrCodeBank[i] Then
    begin
      com_MakeCodeBank.ItemIndex := i;
      Break;
    end;
  end;

  Cut_IDX := Pos('-',qryListMAINT_NO.AsString)-1;

  IF qryList.State = dsBrowse then
  begin
    edt_MAINT_NO1.Text := LeftStr(qryListMAINT_NO.AsString,Cut_IDX);
    edt_MAINT_NO2.Text := MidStr(qryListMAINT_NO.AsString,Cut_IDX+2,2);
    edt_MAINT_NO3.Text := MidStr(qryListMAINT_NO.AsString,Cut_IDX+4,Length(qryListMAINT_NO.AsString));
  end;

end;

function TLOCAPP_N_MSSQL_frm.FindCode(Sender : TsDBEdit;DM: TADOQuery):string;
begin
  DMCodeContents.CODEREFRESH;
  Result := '';
  IF DM.Locate('CODE',Sender.Text,[]) Then
    Result := DM.FieldByName('Name').AsString
  else
    sDBEdit10DblClick(Sender);
end;

procedure TLOCAPP_N_MSSQL_frm.sDBEdit10Exit(Sender: TObject);
var
  ResultValue : String;
begin
  inherited;

  IF qryList.State = dsBrowse Then Exit;
  IF Trim((Sender as TsDBEdit).Text) = '' Then Exit; 

  IF (Sender as TsDBEdit).Hint = '내국신 4487' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.NAEGUKSIN4487)
  else
  IF (Sender as TsDBEdit).Hint = '내국신 4025' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.NAEGUKSIN4025);
  IF (Sender as TsDBEdit).Hint = 'PSHIP' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.PSHIP);
  IF (Sender as TsDBEdit).Hint = 'DOC_DTL' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.NAEGUKSIN1001);
  IF (Sender as TsDBEdit).Hint = 'PAYMENT' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.NAEGUKSIN4277);
  IF (Sender as TsDBEdit).Hint = 'DEST' Then
    ResultValue := FindCode((Sender as TsDBedit),DMCodeContents.NATION);


  IF not (ResultValue = '') then
    setContents((Sender as TsDBedit),ResultValue);
end;

procedure TLOCAPP_N_MSSQL_frm.setContents(Sender: TsDBEdit;
  sValue: String);
begin
  IF (Sender as TsDBEdit).Hint = '내국신 4487' Then
    edt_N4487.Text := sValue
  else
  IF (Sender as TsDBEdit).Hint = '내국신 4025' Then
    edt_N4025.Text := sValue
  else
  IF (Sender as TsDBEdit).Hint = 'PSHIP' Then
    edt_TRANSPRT.Text := sValue
  else
  IF (Sender as TsDBEdit).Hint = 'DOC_DTL' Then
    Edt_DOC_DTL.Text := sValue
  else
  IF (Sender as TsDBEdit).Hint = 'PAYMENT' Then
    edt_PAYMENT.Text := sValue
  else
  IF (Sender as TsDBEdit).Hint = 'DEST' Then
    edt_NAT.Text := sValue
end;

procedure TLOCAPP_N_MSSQL_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF (ActiveControl is TsDBEdit) Then
  begin
    IF (Key = VK_F1) AND ((ActiveControl as  TsDBEdit).Tag = 9) Then
    begin
      IF AnsiMatchText( (ActiveControl as TsDBEdit).Name, ['DB_OWNERCODE','DB_BNFCODE'] ) Then
        DB_OWNERCODEDblClick((ActiveControl as TsDBEdit))
      else
      IF AnsiMatchText( (ActiveControl as TsDBEdit).Name, ['sDBEdit7']) Then
        sDBEdit7DblClick((ActiveControl as TsDBEdit))
      else
        sDBEdit10DblClick(ActiveControl)
    end;
  end;

end;

procedure TLOCAPP_N_MSSQL_frm.Btn_SaveClick(Sender: TObject);
begin
  inherited;
  IF Pos('XX',qryListMAINT_NO.AsString) > 0 Then
  begin
    sPageControl1.ActivePageIndex := 1;
    com_MakeCodeBank.SetFocus;
    raise Exception.Create('은행을 선택해주세요');
  end;

  IF AnsiMatchText('',[Trim(edt_MAINT_NO1.Text),Trim(edt_MAINT_NO2.Text),Trim(edt_MAINT_NO3.Text)] ) Then
  begin
    raise Exception.Create('관리번호를 생성할 수 없습니다');
  end;

  //누락항목 확인
  sPanel1.SetFocus;
  Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
  if Dlg_ErrorMessage_frm.Run_ErrorMessage( sLabel7.Caption ,CHECK_VALIDITY ) Then
  begin
    FreeAndNil(Dlg_ErrorMessage_frm);
    Exit;
  end;

  qryListCHK2.AsInteger := 1;
  qryList.Post;


end;

function TLOCAPP_N_MSSQL_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
begin
  Result := '';
  ErrMsg := TStringList.Create;
  try
    IF (Trim(qryListAPPLIC1.AsString) = '') OR qryListAPPLIC1.IsNull then
      ErrMsg.Add('[상세내역] 개설의뢰인의 상호를 입력하세요');

    IF (Trim(qryListAPPLIC2.AsString) = '') OR qryListAPPLIC2.IsNull then
      ErrMsg.Add('[상세내역] 개설의뢰인의 대표자명을 입력하세요');

    IF (Trim(qryListAPPADDR1.AsString) = '') OR qryListAPPADDR1.IsNull then
      ErrMsg.Add('[상세내역] 개설의뢰인의 주소를 입력하세요');

    IF (Trim(qryListAPPLIC3.AsString) = '') OR qryListAPPLIC3.IsNull then
      ErrMsg.Add('[상세내역] 개설의뢰인의 사업자등록번호를 입력하세요');

    IF (Trim(qryListBENEFC1.AsString) = '') OR qryListBENEFC1.IsNull then
      ErrMsg.Add('[상세내역] 수혜자의 상호를 입력하세요');

    IF (Trim(qryListBENEFC2.AsString) = '') OR qryListBENEFC2.IsNull then
      ErrMsg.Add('[상세내역] 수혜자의 대표자명을 입력하세요');

    IF (Trim(qryListBNFADDR1.AsString) = '') OR qryListBNFADDR1.IsNull then
      ErrMsg.Add('[상세내역] 수혜자의 주소를 입력하세요');

    IF (Trim(qryListBENEFC3.AsString) = '') OR qryListBENEFC3.IsNull then
      ErrMsg.Add('[상세내역] 수혜자의 사업자등록번호를 입력하세요');

    IF (Trim(qryListCHKNAME1.AsString) = '') OR qryListCHKNAME1.IsNull then
      ErrMsg.Add('[상세내역] 수발신식별자를 입력하세요');

    IF (Trim(qryListCHKNAME2.AsString) = '') OR qryListCHKNAME2.IsNull then
      ErrMsg.Add('[상세내역] 상세수발신식별자를 입력하세요');


    IF (Trim(qryListAP_BANK1.AsString) = '') OR qryListAP_BANK1.IsNull then
      ErrMsg.Add('[상세내역] 개설은행명을 입력하세요');

    IF (Trim(qryListLOC_TYPE.AsString) = '') OR qryListLOC_TYPE.IsNull then
      ErrMsg.Add('[상세내역] 신용장 종류를 선택하세요');

    IF (Trim(qryListLOC_AMTC.AsString) = '') OR qryListLOC_AMTC.IsNull then
      ErrMsg.Add('[상세내역] 개설금액 통화단위를 입력하세요');
    IF (Trim(qryListLOC_AMT.AsString) = '') OR qryListLOC_AMT.IsNull then
      ErrMsg.Add('[상세내역] 개설금액을 입력하세요');

    IF (Trim(qryListBUSINESS.AsString) = '') OR qryListBUSINESS.IsNull then
      ErrMsg.Add('[상세내역] 개설근거별용도를 선택하세요');

    //--------------------------------------------------------------------------
    IF (Trim(qryListAPP_DATE.AsString) = '') OR qryListAPP_DATE.IsNull then
      ErrMsg.Add('[개설정보] 개설신청일자를 입력하세요')
    else
    IF not isDate_8String(qryListAPP_DATE.AsString) Then
      ErrMsg.Add('[개설정보] 개설신청일자의 날짜가 잘못되었습니다('+qryListAPP_DATE.AsString+')');

    IF (Trim(qryListDELIVERY.AsString) = '') OR qryListDELIVERY.IsNull then
      ErrMsg.Add('[개설정보] 물품인도기일을 입력하세요')
    else
    IF not isDate_8String(qryListDELIVERY.AsString) Then
      ErrMsg.Add('[개설정보] 물품인도기일의 날짜가 잘못되었습니다('+qryListDELIVERY.AsString+')');

    IF (Trim(qryListEXPIRY.AsString) = '') OR qryListEXPIRY.IsNull then
      ErrMsg.Add('[개설정보] 유효기일을 입력하세요')
    else
    IF not isDate_8String(qryListEXPIRY.AsString) Then
      ErrMsg.Add('[개설정보] 유효기일의 날짜가 잘못되었습니다('+qryListEXPIRY.AsString+')');

    IF (Trim(qryListBSN_HSCODE.AsString) = '') OR qryListBSN_HSCODE.IsNull then
      ErrMsg.Add('[개설정보] 대표공급물품 HS부호를 입력하세요')
    else
    IF Length( qryListBSN_HSCODE.AsString ) <> 10 Then
      ErrMsg.Add('[개설정보] 대표공급물품 HS부호는 10자리입니다('+qryListBSN_HSCODE.AsString+')');

    IF (Trim(qryListTRANSPRT.AsString) = '') OR qryListTRANSPRT.IsNull then
      ErrMsg.Add('[개설정보] 분할허용여부를 입력하세요');

    IF (Trim(qryListGOODDES1.AsString) = '') OR qryListGOODDES1.IsNull then
      ErrMsg.Add('[개설정보] 대표공급 물품명을 입력하세요');

    IF (Trim(qryListEXNAME1.AsString) = '') OR qryListEXNAME1.IsNull then
      ErrMsg.Add('[기타정보] 발신기관 전자서명(상호)을 입력하세요');

    IF (Trim(qryListEXNAME2.AsString) = '') OR qryListEXNAME2.IsNull then
      ErrMsg.Add('[기타정보] 발신기관 전자서명(대표자)을 입력하세요');

    IF (Trim(qryListEXNAME3.AsString) = '') OR qryListEXNAME3.IsNull then
      ErrMsg.Add('[기타정보] 발신기관 전자서명을 입력하세요');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sDBEdit45Exit(Sender: TObject);
begin
  inherited;
  IF qryList.State in [dsInsert,dsEdit] Then
  begin
    IF Trim(sDBEdit45.Text) = '' Then qryListOPEN_NO.AsInteger := 1;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sSpeedButton16Click(Sender: TObject);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode,RecvDoc : string;
  RecvFlat : TStringList;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  RecvFlat := TStringList.Create;
  try
    IF RecvSelect.ShowModal = mrOK Then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc  := qryListMAINT_NO.AsString;
//      RecvFlat := LOCAPP(RecvDoc,RecvCode);
      RecvFlat.Text := LOCAPP(RecvDoc,RecvCode);
    end;
  finally
    FreeAndNil( RecvSelect);
    RecvFlat.Free;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.Serfixed_Maincode;
begin
  DMCodeContents.GEOLAECHEO.Close;
  DMCodeContents.GEOLAECHEO.Open;
  DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]);  
  FFIXED_MAINCODE := 'LOCAPP'+DMCodeContents.GEOLAECHEO.FieldByName('ESU1').AsString;
  edt_MAINT_NO1.Text := FFIXED_MAINCODE;
end;

procedure TLOCAPP_N_MSSQL_frm.com_MakeCodeBankExit(Sender: TObject);
begin
  inherited;
  com_MakeCodeBank.Visible := False;
  com_MakeCodeBank.Width := com_MakeCodeBank.Width-100;
  edt_MAINT_NO3.Width := edt_MAINT_NO3.Width+100;
  edt_MAINT_NO3.Left := edt_MAINT_NO3.Left-100;
end;

procedure TLOCAPP_N_MSSQL_frm.edt_MAINT_NO2Enter(Sender: TObject);
begin
  inherited;
  IF not (qryList.State in [dsInsert]) then Exit;
  
  with com_MakeCodeBank do
  begin
    Left := edt_MAINT_NO2.Left;
    Width := edt_MAINT_NO2.Width+100;
    Top  := edt_MAINT_NO2.Top;

    edt_MAINT_NO3.Width := edt_MAINT_NO3.Width-100;
    edt_MAINT_NO3.Left := edt_MAINT_NO3.Left+100;

    Perform(CB_SETDROPPEDWIDTH,200,0);
    Visible := True;
    Enabled := True;
    SetFocus;
    BringToFront;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.qryListBeforePost(DataSet: TDataSet);
begin
  inherited;
  IF qryList.State = dsInsert Then
    qryListMAINT_NO.AsString := edt_MAINT_NO1.Text+'-'+edt_MAINT_NO2.Text+edt_MAINT_NO3.Text;
end;

end.

