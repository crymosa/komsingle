unit Dlg_APPPCR_Documents;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sMemo, sEdit, Buttons, sSpeedButton,
  sLabel, sSkinProvider, ExtCtrls, sPanel, ADODB, DB, StrUtils, TypeDefine;

type
  TDlg_APPPCR_Documents_frm = class(TDialogParent_frm)
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sPanel2: TsPanel;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Edt_Seq: TsEdit;
    Edt_DOC_G: TsEdit;
    Edt_QTY: TsEdit;
    Edt_DOC_D: TsEdit;
    Edt_BNAME: TsEdit;
    Edt_BHS_NO: TsEdit;
    Edt_BAMTC: TsEdit;
    Edt_BAMT1: TsEdit;
    Edt_SHIP_DATE: TsEdit;
    Edt_BAL_CD: TsEdit;
    Edt_BAL_NAME1: TsEdit;
    Edt_BAL_NAME2: TsEdit;
    Edt_NAT: TsEdit;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    Edt_BNAME1: TsMemo;
    Edt_BSIZE1: TsMemo;
    procedure Btn_SaveClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Edt_DOC_GDblClick(Sender: TObject);
    procedure Edt_BAL_CDDblClick(Sender: TObject);
    procedure Edt_DOC_GExit(Sender: TObject);
  private
    { Private declarations }
    FFields : TFields;
    FProgramControls : TProgramControlType;
    FTempDoc : String;
    procedure SetData;
    procedure SetParamAndExec(qry: TADOQuery);
    procedure SetData_Insert;
    procedure Code_contents(EdtBox : TsEdit;BNoDataOpenDlg : Boolean=False);
  public
    function Run(UserWork: TProgramControlType; UserFields: TFields;
      const TempDoc: String=''): TModalResult;
    { Public declarations }
  end;

var
  Dlg_APPPCR_Documents_frm: TDlg_APPPCR_Documents_frm;

implementation

uses Commonlib, CodeContents, Dlg_FindDefineCode, Dlg_FindBankCode;

{$R *.dfm}

{ TDlg_APPPCR_Documents_frm }

function TDlg_APPPCR_Documents_frm.Run(UserWork : TProgramControlType; UserFields : TFields; Const TempDoc : String=''): TModalResult;
begin
  FProgramControls := UserWork;
  FFields := UserFields;

  //문서번호
  Case FProgramControls of
    ctInsert : FTempDoc := TempDoc;
    ctModify : FTempDoc := FFields.FieldByName('KEYY').AsString;
  end;

  Case FProgramControls of
    ctInsert : SetData_Insert;
    ctModify : SetData;
  end;

  IF Self.ShowModal = mrOk Then
  begin
    Case FProgramControls of
      ctInsert : SetParamAndExec(qryIns);
      ctModify : SetParamAndExec(qryMod);
    end;
    Result := mrOk;
  end
  else
    Result := mrCancel;
end;

procedure TDlg_APPPCR_Documents_frm.SetData;
var
  nLoop, MaxCount : integer;
  TempEdit : TsEdit;
  TempMemo : TsMemo;
begin
  MaxCount := Self.ComponentCount;

  for nLoop := 0 to MaxCount - 1 do
  begin
    IF Components[nLoop] is TsEdit Then
    begin
      TempEdit := TsEdit(Components[nLoop]);

      IF TempEdit.Hint <> '' Then
      begin
        Case TempEdit.Tag of
          0: TempEdit.Text := FFields.FieldByName(TempEdit.Hint).AsString;
          1: TempEdit.Text := FormatFloat('#,0',FFields.FieldByName(TempEdit.Hint).AsCurrency);
          2: TempEdit.Text := FormatFloat('#,0.000',FFields.FieldByName(TempEdit.Hint).AsCurrency);
          102 :
          begin
            IF FFields.FieldByName(TempEdit.Hint).AsString <> '' Then
              TempEdit.Text := FormatDateTime('YYYY.MM.DD',decodeCharDate(FFields.FieldByName(TempEdit.Hint).AsString))
            else
              TempEdit.Text := '';
          end;
          103 : TempEdit.Text := HSCodeFormat(FFields.FieldByName(TempEdit.Hint).AsString);
        end;
        Code_contents(TempEdit)
      end;
    end
    else
    IF Components[nLoop] is TsMemo Then
    begin
      TempMemo := TsMemo(Components[nLoop]);

      IF TempMemo.Hint <> '' Then
      begin
        TempMemo.Text := FFields.FieldByName(TempMemo.Hint).AsString;
      end;
    end;
  end
end;

procedure TDlg_APPPCR_Documents_frm.SetParamAndExec(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('KEYY'     ).Value := FTempDoc;
    Parameters.ParamByName('SEQ'      ).Value := Edt_Seq.Text;
    Parameters.ParamByName('DOC_G'    ).Value := Edt_DOC_G.Text;
    Parameters.ParamByName('DOC_D'    ).Value := Edt_DOC_D.Text;
    Parameters.ParamByName('BHS_NO'   ).Value := HSCodeFormat(Edt_BHS_NO.Text,False);
    Parameters.ParamByName('BNAME'    ).Value := Edt_BNAME.Text;
    Parameters.ParamByName('BNAME1'   ).Value := Edt_BNAME1.Text;
    Parameters.ParamByName('BAMT'     ).Value := STC(Edt_BAMT1.Text);
    Parameters.ParamByName('BAMTC'    ).Value := Edt_BAMTC.Text;
    Parameters.ParamByName('EXP_DATE' ).Value := '';
    Parameters.ParamByName('SHIP_DATE').Value := AnsiReplaceText(Edt_SHIP_DATE.Text,'.','');
    Parameters.ParamByName('BSIZE1'   ).Value := Edt_BSIZE1.Text;
    Parameters.ParamByName('BAL_NAME1').Value := Edt_BAL_NAME1.Text;
    Parameters.ParamByName('BAL_NAME2').Value := Edt_BAL_NAME2.Text;
    Parameters.ParamByName('BAL_CD'   ).Value := Edt_BAL_CD.Text;
    Parameters.ParamByName('NAT'      ).Value := Edt_NAT.Text;
    ExecSQL;
  end;
end;

procedure TDlg_APPPCR_Documents_frm.Btn_SaveClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDlg_APPPCR_Documents_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_APPPCR_Documents_frm.SetData_Insert;
begin
  Edt_Seq.Text := IntToStr(FFields.DataSet.RecordCount+1);
end;

procedure TDlg_APPPCR_Documents_frm.Code_contents(EdtBox : TsEdit; BNoDataOpenDlg : Boolean);
begin
  //PRINT Code Contents
  with DMCodeContents do
  begin
    //근거서류
    IF AnsiMatchText(EdtBox.Hint,['DOC_G']) Then
    begin
      IF APPPCR_DOCUMENTS.Locate('CODE',EdtBox.Text,[]) Then
        Edt_QTY.Text := APPPCR_DOCUMENTS.FieldByName('NAME').AsString
      else
      begin
        IF BNoDataOpenDlg Then
          Edt_DOC_GDblClick(EdtBox)
        else
          Edt_QTY.Text := '';

      end;
    end
    else
    //근거서류 발급기관
    IF AnsiMatchText(EdtBox.Hint,['Edt_BAL_CD']) Then
    begin
      IF BANKCODE.Locate('CODE',EdtBox.Text,[]) Then
      begin
        Edt_BAL_NAME1.Text := BANKCODE.FieldByName('BANKNAME').AsString;
        Edt_BAL_NAME2.Text := BANKCODE.FieldByName('BANKBRANCH').AsString;
      end
      else
      begin
        Edt_BAL_NAME1.Clear;
        Edt_BAL_NAME2.Clear;
      end;
    end
  end;
end;

procedure TDlg_APPPCR_Documents_frm.Edt_DOC_GDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //통화
      IF AnsiMatchText((Sender as TsEdit).Hint,['BAMTC']) Then
      begin
          IF Run('TONGHWA') = mrOk Then
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //문서구분
      IF AnsiMatchText((Sender as TsEdit).Hint,['DOC_G']) Then
      begin
          IF Run('APPPCR_DOCUMENTS') = mrOk Then
          begin
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
            Edt_QTY.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end;
      //국가코드
      IF AnsiMatchText((Sender as TsEdit).Hint,['NAT']) Then
      begin
          IF Run('NATION') = mrOk Then
          begin
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
            Edt_QTY.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end;
      //근거서류 발급기관
      
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TDlg_APPPCR_Documents_frm.Edt_BAL_CDDblClick(Sender: TObject);
begin
  inherited;
  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      IF Run = mrOK Then
      begin
        Edt_BAL_CD.Text     := DataSource1.DataSet.FieldByName('CODE').AsString;
        Edt_BAL_NAME1.Text  := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        Edt_BAL_NAME2.Text  := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;
end;

procedure TDlg_APPPCR_Documents_frm.Edt_DOC_GExit(Sender: TObject);
begin
  inherited;
  Code_contents(Sender as TsEdit,True);
end;

end.
