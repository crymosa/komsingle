unit dlg_CreateDocumentChoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Dlg_APPPCR_NewSelect, sSkinProvider, StdCtrls, sButton,
  ExtCtrls, sBevel, sLabel, sPanel;

type
  Tdlg_CreateDocumentChoice_frm = class(TDlg_APPPCR_NewSelect_frm)
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CreateDocumentChoice_frm: Tdlg_CreateDocumentChoice_frm;

implementation

{$R *.dfm}

procedure Tdlg_CreateDocumentChoice_frm.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then ModalResult := mrCancel;
end;

end.
