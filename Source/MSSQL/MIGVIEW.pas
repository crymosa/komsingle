unit MIGVIEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sMemo, ExtCtrls, sPanel;

type
  TMIGVIEW_frm = class(TForm)
    sPanel1: TsPanel;
    sMemo1: TsMemo;
    sBitBtn1: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Run(MIG : String);
  end;

var
  MIGVIEW_frm: TMIGVIEW_frm;

implementation

{$R *.dfm}

procedure TMIGVIEW_frm.Run(MIG: String);
begin
  sMemo1.Text := MIG;
  Self.ShowModal;
end;

procedure TMIGVIEW_frm.sBitBtn1Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
