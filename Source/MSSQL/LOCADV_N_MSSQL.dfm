inherited LOCADV_N_MSSQL_frm: TLOCADV_N_MSSQL_frm
  Left = 313
  Top = 251
  Caption = #45236#44397#49888#50857#51109' '#44060#49444' '#51025#45813#49436'['#44060#49444#50629#52404']'
  ClientWidth = 847
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  DesignSize = (
    847
    660)
  PixelsPerInch = 96
  TextHeight = 12
  object Image1: TImage [0]
    Left = 688
    Top = 20
    Width = 105
    Height = 105
  end
  inherited sPageControl1: TsPageControl
    Width = 847
    ActivePage = sTabSheet2
    TabIndex = 2
    inherited sTabSheet1: TsTabSheet
      inherited sDBGrid1: TsDBGrid
        Width = 839
        OnDblClick = sDBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 138
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ADV_DATE'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51068#51088
            Width = 75
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = 'L/C NO'
            Width = 124
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ISS_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 75
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'EXPIRY'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#51068#51088
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 142
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC1AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
            Width = 137
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC1AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC2AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
            Width = 139
            Visible = True
          end>
      end
      inherited sPanel7: TsPanel
        Width = 839
        inherited sButton1: TsButton
          OnClick = sButton1Click
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter1: TsSplitter
        Width = 839
      end
      inherited sPanel2: TsPanel
        Width = 839
        inherited sDBEdit1: TsDBEdit
          AutoSize = False
          DataSource = dsList
        end
        inherited sDBEdit23: TsDBEdit
          AutoSize = False
          DataSource = dsList
        end
        inherited sDBEdit2: TsDBEdit
          Width = 87
          AutoSize = False
          DataField = 'DATEE'
          DataSource = dsList
        end
        inherited sDBEdit75: TsDBEdit
          AutoSize = False
          DataField = 'MESSAGE1'
          DataSource = dsList
        end
        inherited sDBEdit76: TsDBEdit
          AutoSize = False
          DataField = 'MESSAGE2'
          DataSource = dsList
        end
      end
      inherited sPanel3: TsPanel
        Width = 839
        inherited sLabel8: TsLabel
          Caption = #44277#44553#51088#48156#54665' '#49464#44552#44228#49328#49436' '#49324#48376
        end
        inherited sLabel9: TsLabel
          Caption = #47932#54408#47749#49464#44032' '#44592#51116#46108' '#49569#51109
        end
        inherited sDBEdit33: TsDBEdit
          AutoSize = False
          DataField = 'APPLIC1'
          DataSource = dsList
        end
        inherited sDBEdit3: TsDBEdit
          AutoSize = False
          DataField = 'APPLIC2'
          DataSource = dsList
        end
        inherited sDBEdit4: TsDBEdit
          AutoSize = False
          DataField = 'APPLIC3'
          DataSource = dsList
        end
        inherited sDBEdit5: TsDBEdit
          AutoSize = False
          DataField = 'APPADDR1'
          DataSource = dsList
        end
        inherited sDBEdit6: TsDBEdit
          AutoSize = False
          DataField = 'APPADDR2'
          DataSource = dsList
        end
        inherited sDBEdit7: TsDBEdit
          AutoSize = False
          DataField = 'APPADDR3'
          DataSource = dsList
        end
        inherited sDBEdit8: TsDBEdit
          AutoSize = False
          DataField = 'BENEFC1'
          DataSource = dsList
        end
        inherited sDBEdit9: TsDBEdit
          AutoSize = False
          DataField = 'BENEFC2'
          DataSource = dsList
        end
        inherited sDBEdit10: TsDBEdit
          AutoSize = False
          DataField = 'BENEFC3'
          DataSource = dsList
        end
        inherited sDBEdit11: TsDBEdit
          AutoSize = False
          DataField = 'BNFADDR1'
          DataSource = dsList
        end
        inherited sDBEdit12: TsDBEdit
          AutoSize = False
          DataField = 'BNFADDR2'
          DataSource = dsList
        end
        inherited sDBEdit13: TsDBEdit
          AutoSize = False
          DataField = 'BNFADDR3'
          DataSource = dsList
        end
        inherited sDBEdit14: TsDBEdit
          AutoSize = False
          DataField = 'BNFEMAILID'
          DataSource = dsList
        end
        inherited sDBEdit15: TsDBEdit
          AutoSize = False
          DataField = 'BNFDOMAIN'
          DataSource = dsList
        end
        inherited sDBEdit16: TsDBEdit
          AutoSize = False
          DataField = 'AP_BANK'
          DataSource = dsList
        end
        inherited sDBEdit17: TsDBEdit
          AutoSize = False
          DataField = 'AP_BANK1'
          DataSource = dsList
        end
        inherited sDBEdit18: TsDBEdit
          AutoSize = False
          DataField = 'AP_BANK2'
          DataSource = dsList
        end
        inherited sDBEdit19: TsDBEdit
          AutoSize = False
          DataField = 'LOC_TYPE'
          DataSource = dsList
        end
        inherited sDBEdit20: TsDBEdit
          AutoSize = False
          DataField = 'LOC_TYPENAME'
          DataSource = dsList
        end
        inherited sDBEdit21: TsDBEdit
          AutoSize = False
          DataField = 'LOC1AMTC'
          DataSource = dsList
        end
        inherited sDBEdit22: TsDBEdit
          AutoSize = False
          DataField = 'LOC2AMTC'
          DataSource = dsList
        end
        inherited sDBEdit24: TsDBEdit
          AutoSize = False
          DataField = 'LOC1AMT'
          DataSource = dsList
        end
        inherited sDBEdit25: TsDBEdit
          AutoSize = False
          DataField = 'LOC2AMT'
          DataSource = dsList
        end
        inherited sDBEdit26: TsDBEdit
          AutoSize = False
          DataField = 'CD_PERP'
          DataSource = dsList
        end
        inherited sDBEdit27: TsDBEdit
          AutoSize = False
          DataField = 'CD_PERM'
          DataSource = dsList
        end
        inherited sDBEdit28: TsDBEdit
          AutoSize = False
          DataField = 'EX_RATE'
          DataSource = dsList
        end
        inherited sDBEdit29: TsDBEdit
          Top = 184
          AutoSize = False
          DataField = 'OFFERNO1'
          DataSource = dsList
        end
        inherited sPanel5: TsPanel
          Top = 169
        end
        inherited sDBEdit30: TsDBEdit
          Top = 208
          AutoSize = False
          DataField = 'OFFERNO2'
          DataSource = dsList
        end
        inherited sDBEdit31: TsDBEdit
          Top = 232
          AutoSize = False
          DataField = 'OFFERNO3'
          DataSource = dsList
        end
        inherited sDBEdit32: TsDBEdit
          Top = 256
          AutoSize = False
          DataField = 'OFFERNO4'
          DataSource = dsList
        end
        inherited sDBEdit34: TsDBEdit
          Top = 280
          AutoSize = False
          DataField = 'OFFERNO5'
          DataSource = dsList
        end
        inherited sDBEdit35: TsDBEdit
          Top = 304
          AutoSize = False
          DataField = 'OFFERNO6'
          DataSource = dsList
        end
        inherited sDBEdit36: TsDBEdit
          Top = 328
          AutoSize = False
          DataField = 'OFFERNO7'
          DataSource = dsList
        end
        inherited sDBEdit37: TsDBEdit
          Top = 352
          AutoSize = False
          DataField = 'OFFERNO8'
          DataSource = dsList
        end
        inherited sDBEdit38: TsDBEdit
          Top = 376
          AutoSize = False
          DataField = 'OFFERNO9'
          DataSource = dsList
        end
        inherited sDBEdit40: TsDBEdit
          AutoSize = False
          DataField = 'DOCCOPY1'
          DataSource = dsList
        end
        inherited sDBEdit41: TsDBEdit
          AutoSize = False
          DataField = 'DOCCOPY2'
          DataSource = dsList
        end
        inherited sDBEdit42: TsDBEdit
          AutoSize = False
          DataField = 'DOCCOPY3'
          DataSource = dsList
        end
        inherited sDBEdit43: TsDBEdit
          AutoSize = False
          DataField = 'DOCCOPY4'
          DataSource = dsList
        end
        inherited sDBEdit44: TsDBEdit
          AutoSize = False
          DataField = 'DOCCOPY5'
          DataSource = dsList
        end
        object sDBEdit39: TsDBEdit
          Left = 466
          Top = 133
          Width = 39
          Height = 23
          TabStop = False
          AutoSize = False
          Color = clWhite
          DataField = 'BUSINESS'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 43
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44060#49444#44540#44144#48324#50857#46020
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlue
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit77: TsDBEdit
          Left = 506
          Top = 133
          Width = 151
          Height = 23
          TabStop = False
          AutoSize = False
          Color = clWhite
          DataField = 'BUSINESSNAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      Caption = #44060#49444#51221#48372
      inherited sPanel8: TsPanel
        Width = 839
        inherited sDBEdit45: TsDBEdit
          AutoSize = False
          DataField = 'OPEN_NO'
          DataSource = dsList
        end
        inherited sDBEdit46: TsDBEdit
          Width = 101
          AutoSize = False
          DataField = 'ADV_DATE'
          DataSource = dsList
        end
        inherited sDBEdit47: TsDBEdit
          Width = 101
          AutoSize = False
          DataField = 'ISS_DATE'
          DataSource = dsList
        end
        inherited sDBEdit48: TsDBEdit
          AutoSize = False
          DataField = 'DOC_PRD'
          DataSource = dsList
        end
        inherited sDBEdit49: TsDBEdit
          AutoSize = False
          DataField = 'LC_NO'
          DataSource = dsList
        end
        inherited sDBEdit50: TsDBEdit
          Width = 101
          AutoSize = False
          DataField = 'DELIVERY'
          DataSource = dsList
        end
        inherited sDBEdit51: TsDBEdit
          Width = 101
          AutoSize = False
          DataField = 'EXPIRY'
          DataSource = dsList
        end
        inherited sDBEdit52: TsDBEdit
          AutoSize = False
          DataField = 'TRANSPRT'
          DataSource = dsList
        end
        inherited sDBEdit53: TsDBEdit
          AutoSize = False
          DataField = 'TRANSPRTNAME'
          DataSource = dsList
        end
        inherited sDBEdit54: TsDBEdit
          AutoSize = False
          DataField = 'BSN_HSCODE'
          DataSource = dsList
        end
        inherited sDBMemo1: TsDBMemo
          TabStop = False
          DataField = 'GOODDES1'
          DataSource = dsList
          ReadOnly = True
        end
        inherited sDBMemo2: TsDBMemo
          TabStop = False
          DataField = 'DOC_ETC1'
          DataSource = dsList
          ReadOnly = True
        end
        inherited sDBMemo3: TsDBMemo
          TabStop = False
          DataField = 'REMARK1'
          DataSource = dsList
          ReadOnly = True
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      Caption = #44592#53440#51221#48372
      inherited sPanel10: TsPanel
        Width = 839
        inherited sDBEdit55: TsDBEdit
          AutoSize = False
          DataField = 'DOC_DTL'
          DataSource = dsList
        end
        inherited sDBEdit56: TsDBEdit
          AutoSize = False
          DataSource = dsList
        end
        inherited sDBEdit57: TsDBEdit
          AutoSize = False
          DataField = 'DOC_NO'
          DataSource = dsList
        end
        inherited sDBEdit58: TsDBEdit
          AutoSize = False
          DataField = 'DOC_AMTC'
          DataSource = dsList
          Visible = False
        end
        inherited sDBEdit59: TsDBEdit
          AutoSize = False
          DataField = 'DOC_AMT'
          DataSource = dsList
          Visible = False
        end
        inherited sDBEdit60: TsDBEdit
          AutoSize = False
          DataField = 'LOADDATE'
          DataSource = dsList
        end
        inherited sDBEdit61: TsDBEdit
          AutoSize = False
          DataField = 'EXPDATE'
          DataSource = dsList
        end
        inherited sDBEdit62: TsDBEdit
          Left = 130
          Top = 157
          AutoSize = False
          DataField = 'IM_NAME'
          DataSource = dsList
          Visible = False
          BoundLabel.Font.Color = clBlue
        end
        inherited sDBEdit63: TsDBEdit
          Left = 130
          Top = 181
          AutoSize = False
          DataField = 'IM_NAME1'
          DataSource = dsList
          Visible = False
        end
        inherited sDBEdit64: TsDBEdit
          Left = 130
          Top = 205
          AutoSize = False
          DataField = 'IM_NAME2'
          DataSource = dsList
          Visible = False
        end
        inherited sDBEdit65: TsDBEdit
          Left = 130
          Top = 229
          AutoSize = False
          DataField = 'IM_NAME3'
          DataSource = dsList
          Visible = False
        end
        inherited sDBEdit66: TsDBEdit
          Top = 37
          AutoSize = False
          DataField = 'DEST'
          DataSource = dsList
        end
        inherited sDBEdit67: TsDBEdit
          Top = 37
          AutoSize = False
          DataSource = dsList
        end
        inherited sDBEdit68: TsDBEdit
          Top = 61
          AutoSize = False
          DataField = 'ISBANK1'
          DataSource = dsList
        end
        inherited sDBEdit69: TsDBEdit
          Top = 85
          AutoSize = False
          DataField = 'ISBANK2'
          DataSource = dsList
        end
        inherited sDBEdit70: TsDBEdit
          Top = 109
          AutoSize = False
          DataField = 'PAYMENT'
          DataSource = dsList
        end
        inherited sDBEdit71: TsDBEdit
          Top = 109
          AutoSize = False
          DataSource = dsList
        end
        inherited sDBMemo4: TsDBMemo
          TabStop = False
          DataField = 'EXGOOD1'
          DataSource = dsList
          ReadOnly = True
        end
        inherited sDBEdit72: TsDBEdit
          AutoSize = False
          DataField = 'EXNAME1'
          DataSource = dsList
        end
        inherited sDBEdit73: TsDBEdit
          AutoSize = False
          DataField = 'EXNAME2'
          DataSource = dsList
        end
        inherited sDBEdit74: TsDBEdit
          AutoSize = False
          DataField = 'EXNAME3'
          DataSource = dsList
        end
      end
    end
  end
  inherited sPanel1: TsPanel
    Width = 847
    inherited sSpeedButton5: TsSpeedButton
      Left = 780
    end
    inherited Btn_Close: TsSpeedButton
      Left = 791
      OnClick = Btn_CloseClick
    end
    inherited Btn_Print: TsSpeedButton
      OnClick = Btn_PrintClick
    end
  end
  object sPanel13: TsPanel [3]
    Left = 689
    Top = 59
    Width = 130
    Height = 24
    Anchors = [akRight]
    TabOrder = 2
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
  end
  object sPanel14: TsPanel [4]
    Left = 663
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 3
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton4: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9664
      OnClick = sSpeedButton4Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  object sPanel15: TsPanel [5]
    Left = 820
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 4
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton8: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9654
      OnClick = sSpeedButton8Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  inherited qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCADV.BUSINESS =' +
        ' N4025.CODE'
      
        '                           LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON L' +
        'OCADV.TRANSPRT = PSHIP.CODE'
      
        '                           LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON' +
        ' LOCADV.PAYMENT = N4277.CODE')
    Left = 168
    Top = 200
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      EditMask = '9999.99-9999;0'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
  end
  inherited dsList: TDataSource
    Left = 200
    Top = 200
  end
end
