unit Dlg_Customer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sButton, DB, Grids, DBGrids, acDBGrid,
  sEdit, sComboBox, sSkinProvider, ExtCtrls, sPanel;

type
  TDlg_Customer_frm = class(TDialogParent_frm)
    sComboBox1: TsComboBox;
    sEdit1: TsEdit;
    sDBGrid1: TsDBGrid;
    DataSource1: TDataSource;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sButton4: TsButton;
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Run:TModalResult;
    function isValue(CODE : String):Boolean;
  end;

var
  Dlg_Customer_frm: TDlg_Customer_frm;

implementation

uses MSSQL, CodeContents, dlg_CustomerReg, TypeDefine;

{$R *.dfm}

{ TDlg_Customer_frm }

function TDlg_Customer_frm.Run: TModalResult;
begin
  DataSource1.DataSet.Close;
  DataSource1.DataSet.Open;
  Result := Self.ShowModal;
end;

procedure TDlg_Customer_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF DataSource1.DataSet.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDlg_Customer_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN THEN ModalResult := mrOk;
end;

procedure TDlg_Customer_frm.sButton1Click(Sender: TObject);
var
  FindIndex : String;
begin
  inherited;
  IF sEdit1.Text = '' Then Exit;

  DataSource1.DataSet.First;
  Case sComboBox1.ItemIndex of
    0: FindIndex := 'CODE';
    1: FindIndex := 'ENAME';
    2: FindIndex := 'REP_NAME';
  end;

  IF not DataSource1.DataSet.Locate(FindIndex,sEdit1.Text,[loPartialKey]) Then
    Raise Exception.Create('데이터를 찾지 못했습니다.');

end;

function TDlg_Customer_frm.isValue(CODE: String): Boolean;
begin
  DataSource1.DataSet.First;
  Result := DataSource1.DataSet.Locate('CODE',CODE,[]);
end;

procedure TDlg_Customer_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDlg_Customer_frm.sButton4Click(Sender: TObject);
var
  Custom_Code : String;
begin
  dlg_CustomerReg_frm := Tdlg_CustomerReg_frm.Create(Self);
  try
    if dlg_CustomerReg_frm.Run(ctInsert,nil) = mrOk then
    begin
        Custom_Code := dlg_CustomerReg_frm.edt_CODE.Text;
        DataSource1.DataSet.Close;
        DataSource1.DataSet.Open;
        DataSource1.DataSet.Locate('CODE',Custom_Code,[]);
    end;
  finally
    FreeAndNil(dlg_CustomerReg_frm);
  end;
end;

end.
