unit LOCAM1_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB, ADODB;

type
  TLOCAM1_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QR_MAINTNO: TQRLabel;
    QR_ADVDATE: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel3: TQRLabel;
    ChildBand2: TQRChildBand;
    ChildBand3: TQRChildBand;
    QRLabel31: TQRLabel;
    QR_ISSBANK: TQRLabel;
    QR_ISSBANK1: TQRLabel;
    QR_ISSBANK2: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_AMDDATE: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_LCNO: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel7: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    ChildBand7: TQRChildBand;
    QR_APPADDR1: TQRLabel;
    QR_APPADDR2: TQRLabel;
    QR_APPADDR3: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel8: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    ChildBand9: TQRChildBand;
    QR_BNFADDR1: TQRLabel;
    QR_BNFADDR2: TQRLabel;
    QR_BNFADDR3: TQRLabel;
    ChildBand10: TQRChildBand;
    QR_BNFMAILID: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_LOC1AMT: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_LOC2AMT: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_CDPERPM: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_EXRATE: TQRLabel;
    ChildBand15: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_OFFERNO1: TQRLabel;
    QR_OFFERNO2: TQRLabel;
    QR_OFFERNO3: TQRLabel;
    QR_OFFERNO6: TQRLabel;
    QR_OFFERNO5: TQRLabel;
    QR_OFFERNO4: TQRLabel;
    QR_OFFERNO9: TQRLabel;
    QR_OFFERNO8: TQRLabel;
    QR_OFFERNO7: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_DELIVERY: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_EXPIRY: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel18: TQRLabel;
    QR_AMDNO: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel24: TQRLabel;
    QR_CHGINFO: TQRMemo;
    ChildBand20: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRImage1: TQRImage;
    QRLabel20: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListAMD_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListISSBANK: TStringField;
    qryListISSBANK1: TStringField;
    qryListISSBANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListAMD_NO: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListCHGINFO: TStringField;
    qryListCHGINFO1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListbgm_ref: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    ChildBand21: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    ChildBand22: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel12: TQRLabel;
    QRLabel16: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    FSEQNO : String;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
    property  SEQNO:string  read FSEQNO write FSEQNO;
  end;

var
  LOCAM1_PRINT_frm: TLOCAM1_PRINT_frm;

implementation

uses Commonlib, MSSQL;
{$R *.dfm}

{ TLOCAM1_PRINT_frm }

procedure TLOCAM1_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Parameters.ParamByName('MSEQ').Value := FSEQNO;
  qryList.Open;
end;

procedure TLOCAM1_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
  //전자문서번호
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //조건변경통지일자
  QR_ADVDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListAMD_DATE.AsString));
end;

procedure TLOCAM1_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
   //개설은행
  if Trim(qryListISSBANK.AsString) <> '' then
  begin
    QR_ISSBANK.Enabled := True;
    QR_ISSBANK.Caption := qryListISSBANK.AsString;
  end;

  nIndex := 1;
  for i := 1 to 2 do
  begin
    if Trim(qrylist.FieldByName('ISSBANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('ISSBANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_ISSBANK.Caption) = '') and (Trim(QR_ISSBANK1.Caption) = '') then
  begin
    ChildBand3.Enabled := False;
  end
  else
    ChildBand3.Height := (16 * (nIndex-1))+15;

end;

procedure TLOCAM1_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //조건변경일자
  QR_AMDDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListAMD_DATE.AsString));
  if Trim(QR_AMDDATE.Caption) = '' then
    ChildBand4.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //신용장번호
  QR_LCNO.Caption := qryListLC_NO.AsString;
  if Trim(QR_LCNO.Caption) = '' then
    ChildBand5.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
  inherited;
  //개설의뢰인 상호 대표자명 '''
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qrylist.FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APPLIC'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_APPLIC1.Caption) = '') and (Trim(QR_APPLIC2.Caption) = '') then
  begin
    ChildBand6.Enabled := False;
  end
  else
    ChildBand6.Height := (16 * (nIndex-1));
end;

procedure TLOCAM1_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
  inherited;
  //개설의뢰인 주소
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qrylist.FieldByName('APPADDR'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APPADDR'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_APPADDR1.Caption) = '') and (Trim(QR_APPADDR2.Caption) = '') then
  begin
    ChildBand7.Enabled := False;
  end
  else
    ChildBand7.Height := (16 * (nIndex-1));
end;

procedure TLOCAM1_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
  inherited;
  //수혜자 상호 대표자명 '''
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qrylist.FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BENEFC'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_BENEFC1.Caption) = '') and (Trim(QR_BENEFC2.Caption) = '') then
  begin
    ChildBand8.Enabled := False;
  end
  else
    ChildBand8.Height := (16 * (nIndex-1));
end;

procedure TLOCAM1_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
  inherited;
  //개설의뢰인 주소
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qrylist.FieldByName('BNFADDR'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_BNFADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_BNFADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BNFADDR'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_BNFADDR1.Caption) = '') and (Trim(QR_BNFADDR2.Caption) = '') then
  begin
    ChildBand9.Enabled := False;
  end
  else
    ChildBand9.Height := (16 * (nIndex-1));
end;

procedure TLOCAM1_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_BNFMAILID.Caption := qryListBNFEMAILID.AsString + '@' + qryListBNFDOMAIN.AsString;
  if Trim(qryListBNFEMAILID.AsString) = '' then
    ChildBand10.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //변경후 외화금액
  QR_LOC1AMT.Caption := qryListLOC1AMTC.AsString + ' ' + FormatFloat('#,##0.####' , qryListLOC1AMT.AsFloat);
  if qryListLOC1AMT.AsCurrency = 0 then
    ChildBand11.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //변경후 원화금액
  QR_LOC2AMT.Caption := qryListLOC2AMTC.AsString + ' ' + FormatFloat('#,##0.####' , qryListLOC2AMT.AsFloat);
  if qryListLOC2AMT.AsCurrency = 0 then
    ChildBand12.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //허요오차
  QR_CDPERPM.Caption := '(±) ' + qryListCD_PERP.AsString + '/' + qryListCD_PERM.AsString + '(%)';
  if (qryListCD_PERP.AsInteger = 0) and  (qryListCD_PERM.AsInteger = 0) then
    ChildBand13.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_EXRATE.Caption := FormatFloat('#,##0.####' , qryListEX_RATE.AsFloat);
  if qryListEX_RATE.AsCurrency = 0 then
    ChildBand14.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
//최종물품매도확약서번호
  nIndex := 1;
  for i := 1 to 9 do
  begin
    if Trim(qrylist.FieldByName('OFFERNO'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('OFFERNO'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_OFFERNO1.Caption) = '') and (Trim(QR_OFFERNO2.Caption) = '') and (Trim(QR_OFFERNO3.Caption) = '')
      and (Trim(QR_OFFERNO4.Caption) = '') and (Trim(QR_OFFERNO5.Caption) = '') and (Trim(QR_OFFERNO6.Caption) = '')
      and (Trim(QR_OFFERNO7.Caption) = '') and (Trim(QR_OFFERNO8.Caption) = '') and (Trim(QR_OFFERNO9.Caption) = '')then
  begin
    ChildBand15.Enabled := False;
  end
  else
    ChildBand15.Height := (16 * (nIndex-1));

end;

procedure TLOCAM1_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //변경후 물품인도기일
  QR_DELIVERY.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListDELIVERY.AsString));
  if Trim(QR_DELIVERY.Caption) = '' then
    ChildBand16.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand17BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //변경후 유효기일
  QR_EXPIRY.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListEXPIRY.AsString));
  if Trim(QR_EXPIRY.Caption) = '' then
    ChildBand17.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //조건변경횟수
  inherited;
  QR_AMDNO.Caption := IntToStr(qryListAMD_NO.AsInteger);
  if qryListAMD_NO.AsInteger = 0 then
    ChildBand18.Enabled := False;
end;

procedure TLOCAM1_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //기타조건변경사항
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListCHGINFO1.AsString;
    ChildBand19.Height := ( 13 * memoLines.Count) + 4;
    QR_CHGINFO.Height := ( 13 * memoLines.Count);

    QR_CHGINFO.Lines.Clear;
    QR_CHGINFO.Lines.Text := qryListCHGINFO1.AsString;

    if QR_CHGINFO.Lines.Count = 0 then
    begin
      ChildBand19.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCAM1_PRINT_frm.ChildBand21BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
 nIndex , i : Integer;
begin
  inherited;
  //발신기관 전자사명
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qrylist.FieldByName('EXNAME'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_EXNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_EXNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('EXNAME'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  ChildBand21.Height := (16 * (nIndex));

end;

end.
