inherited VATBI4_PRINT_frm: TVATBI4_PRINT_frm
  BeforePrint = QuickRepBeforePrint
  Functions.DATA = (
    '0'
    '0'
    #39#39)
  OnNeedData = QuickRepNeedData
  Page.Values = (
    100
    2970
    100
    2100
    100
    100
    0)
  inherited TitleBand1: TQRBand
    Size.Values = (
      156.104166666667
      1899.70833333333)
    inherited QRLabel1: TQRLabel
      Size.Values = (
        58.2083333333333
        732.895833333333
        42.3333333333333
        433.916666666667)
      FontSize = 16
    end
  end
  inherited ChildBand1: TQRChildBand
    BeforePrint = ChildBand1BeforePrint
    Size.Values = (
      52.9166666666667
      1899.70833333333)
    inherited QRLabel4: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        13.2291666666667
        320.145833333333)
      FontSize = 9
    end
    inherited QR_MAINT_NO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        13.2291666666667
        177.270833333333)
      FontSize = 9
    end
    inherited QRLabel2: TQRLabel
      Left = 568
      Width = 87
      Size.Values = (
        37.0416666666667
        1502.83333333333
        13.2291666666667
        230.1875)
      Caption = '('#44277#44553#48155#45716#51088#50857')'
      FontSize = 9
    end
  end
  inherited ChildBand2: TQRChildBand
    BeforePrint = ChildBand2BeforePrint
    Size.Values = (
      52.9166666666667
      1899.70833333333)
    inherited QRLabel3: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        29.1041666666667
        320.145833333333)
      FontSize = 9
    end
    inherited QR_RENO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        29.1041666666667
        113.770833333333)
      FontSize = 9
    end
  end
  inherited ChildBand3: TQRChildBand
    BeforePrint = ChildBand3BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel6: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        2.64583333333333
        320.145833333333)
      FontSize = 9
    end
    inherited QR_SENO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        2.64583333333333
        113.770833333333)
      FontSize = 9
    end
  end
  inherited ChildBand23: TQRChildBand
    BeforePrint = ChildBand23BeforePrint
    Size.Values = (
      44.9791666666667
      1899.70833333333)
    inherited QRLabel5: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        2.64583333333333
        320.145833333333)
      FontSize = 9
    end
    inherited QR_FSNO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        2.64583333333333
        113.770833333333)
      FontSize = 9
    end
  end
  inherited ChildBand4: TQRChildBand
    BeforePrint = ChildBand4BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel8: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        2.64583333333333
        320.145833333333)
      FontSize = 9
    end
    inherited QR_ACENO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        2.64583333333333
        129.645833333333)
      FontSize = 9
    end
  end
  inherited ChildBand5: TQRChildBand
    BeforePrint = ChildBand5BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel10: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        2.64583333333333
        320.145833333333)
      FontSize = 9
    end
    inherited QR_RFFNO: TQRLabel
      Size.Values = (
        37.0416666666667
        402.166666666667
        2.64583333333333
        129.645833333333)
      FontSize = 9
    end
  end
  inherited ChildBand6: TQRChildBand
    Size.Values = (
      84.6666666666667
      1899.70833333333)
    inherited QRImage3: TQRImage
      Size.Values = (
        13.2291666666667
        0
        15.875
        1899.70833333333)
    end
    inherited QRLabel12: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        39.6875
        216.958333333333)
      FontSize = 9
    end
  end
  inherited ChildBand7: TQRChildBand
    BeforePrint = ChildBand7BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QR_SESAUP: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        145.520833333333)
      FontSize = 9
    end
    inherited QRLabel13: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand8: TQRChildBand
    BeforePrint = ChildBand8BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QR_SENAME1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QRLabel16: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SENAME2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand10: TQRChildBand
    BeforePrint = ChildBand10BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QR_SEADDR3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        82.0208333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEADDR2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEADDR1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QRLabel19: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEADDR4: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        121.708333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEADDR5: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        161.395833333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand11: TQRChildBand
    BeforePrint = ChildBand11BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel23: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SENAME3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand12: TQRChildBand
    BeforePrint = ChildBand12BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel25: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEUPTA1: TQRMemo
      Size.Values = (
        34.3958333333333
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
  end
  inherited ChildBand13: TQRChildBand
    BeforePrint = ChildBand13BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel29: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_SEITEM1: TQRMemo
      Size.Values = (
        34.3958333333333
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
  end
  inherited ChildBand14: TQRChildBand
    Size.Values = (
      84.6666666666667
      1899.70833333333)
    inherited QRLabel33: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        39.6875
        248.708333333333)
      FontSize = 9
    end
    inherited QRImage1: TQRImage
      Size.Values = (
        13.2291666666667
        0
        15.875
        1899.70833333333)
    end
  end
  inherited ChildBand15: TQRChildBand
    BeforePrint = ChildBand15BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel34: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYSAUP: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        145.520833333333)
      FontSize = 9
    end
  end
  inherited ChildBand18: TQRChildBand
    BeforePrint = ChildBand18BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel40: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYADDR1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYADDR2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYADDR3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        82.0208333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYADDR4: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        121.708333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYADDR5: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        161.395833333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand16: TQRChildBand
    BeforePrint = ChildBand16BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel36: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYNAME1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYNAME2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand19: TQRChildBand
    BeforePrint = ChildBand19BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel44: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYNAME3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand20: TQRChildBand
    BeforePrint = ChildBand20BeforePrint
    Size.Values = (
      52.9166666666667
      1899.70833333333)
    inherited QRLabel46: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYUPTA1: TQRMemo
      Size.Values = (
        34.3958333333333
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
  end
  inherited ChildBand21: TQRChildBand
    BeforePrint = ChildBand21BeforePrint
    Size.Values = (
      52.9166666666667
      1899.70833333333)
    inherited QRLabel50: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_BYITEM1: TQRMemo
      Size.Values = (
        34.3958333333333
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
  end
  inherited ChildBand9: TQRChildBand
    BeforePrint = ChildBand9BeforePrint
    Size.Values = (
      84.6666666666667
      1899.70833333333)
    inherited QRImage10: TQRImage
      Size.Values = (
        13.2291666666667
        0
        15.875
        1899.70833333333)
    end
    inherited QRLabel7: TQRLabel
      Size.Values = (
        37.0416666666667
        21.1666666666667
        39.6875
        216.958333333333)
      FontSize = 9
    end
  end
  inherited ChildBand17: TQRChildBand
    BeforePrint = ChildBand17BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel9: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGSAUP: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        145.520833333333)
      FontSize = 9
    end
  end
  inherited ChildBand29: TQRChildBand
    BeforePrint = ChildBand29BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel15: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGNAME1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGNAME2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand30: TQRChildBand
    BeforePrint = ChildBand30BeforePrint
    Size.Values = (
      26.4583333333333
      1899.70833333333)
    inherited QRLabel20: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGADDR1: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGADDR2: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        42.3333333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGADDR3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        82.0208333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGADDR4: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        121.708333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGADDR5: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        161.395833333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand31: TQRChildBand
    BeforePrint = ChildBand31BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QRLabel28: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_AGNAME3: TQRLabel
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand32: TQRChildBand
    BeforePrint = ChildBand32BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QR_AGUPTA1: TQRMemo
      Size.Values = (
        37.0416666666667
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
    inherited QRLabel31: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand33: TQRChildBand
    BeforePrint = ChildBand33BeforePrint
    Size.Values = (
      39.6875
      1899.70833333333)
    inherited QR_AGITEM1: TQRMemo
      Size.Values = (
        34.3958333333333
        230.1875
        2.64583333333333
        658.8125)
      FontSize = 9
    end
    inherited QRLabel32: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        2.64583333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand22: TQRChildBand
    BeforePrint = ChildBand22BeforePrint
    Size.Values = (
      177.270833333333
      1899.70833333333)
    inherited QRImage2: TQRImage
      Size.Values = (
        13.2291666666667
        0
        13.2291666666667
        1899.70833333333)
    end
    inherited QRImage4: TQRImage
      Size.Values = (
        13.2291666666667
        0
        79.375
        1899.70833333333)
    end
    inherited QRLabel54: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        37.0416666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QRLabel55: TQRLabel
      Size.Values = (
        37.0416666666667
        296.333333333333
        37.0416666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QRLabel56: TQRLabel
      Size.Values = (
        37.0416666666667
        571.5
        37.0416666666667
        129.645833333333)
      FontSize = 9
    end
    inherited QRLabel57: TQRLabel
      Size.Values = (
        37.0416666666667
        889
        37.0416666666667
        129.645833333333)
      FontSize = 9
    end
    inherited QRLabel58: TQRLabel
      Size.Values = (
        37.0416666666667
        1164.16666666667
        37.0416666666667
        66.1458333333333)
      FontSize = 9
    end
    inherited QR_DRAWDAT: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        113.770833333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_DETAILNO: TQRLabel
      Size.Values = (
        37.0416666666667
        174.625
        113.770833333333
        177.270833333333)
      FontSize = 9
    end
    inherited QR_SUPAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        555.625
        113.770833333333
        145.520833333333)
      FontSize = 9
    end
    inherited QR_TAXAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        873.125
        113.770833333333
        145.520833333333)
      FontSize = 9
    end
    inherited QR_REMARK1: TQRMemo
      Size.Values = (
        34.3958333333333
        1164.16666666667
        113.770833333333
        722.3125)
      FontSize = 9
    end
  end
  inherited ChildBand24: TQRChildBand
    Size.Values = (
      137.583333333333
      1899.70833333333)
    inherited QRImage5: TQRImage
      Size.Values = (
        13.2291666666667
        0
        13.2291666666667
        1899.70833333333)
    end
    inherited QRLabel63: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        37.0416666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QRLabel64: TQRLabel
      Size.Values = (
        37.0416666666667
        211.666666666667
        37.0416666666667
        288.395833333333)
      FontSize = 9
    end
    inherited QRLabel65: TQRLabel
      Size.Values = (
        37.0416666666667
        920.75
        37.0416666666667
        161.395833333333)
      FontSize = 9
    end
    inherited QRLabel66: TQRLabel
      Size.Values = (
        37.0416666666667
        1180.04166666667
        37.0416666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QRLabel67: TQRLabel
      Size.Values = (
        37.0416666666667
        1460.5
        37.0416666666667
        129.645833333333)
      FontSize = 9
    end
    inherited QRLabel68: TQRLabel
      Size.Values = (
        37.0416666666667
        1397
        79.375
        193.145833333333)
      FontSize = 9
    end
    inherited QRLabel69: TQRLabel
      Size.Values = (
        37.0416666666667
        1778
        37.0416666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QRLabel70: TQRLabel
      Size.Values = (
        37.0416666666667
        1778
        79.375
        97.8958333333333)
      FontSize = 9
    end
    inherited QRImage6: TQRImage
      Size.Values = (
        13.2291666666667
        0
        121.708333333333
        1899.70833333333)
    end
  end
  inherited DetailBand1: TQRBand
    BeforePrint = DetailBand1BeforePrint
    Size.Values = (
      148.166666666667
      1899.70833333333)
    inherited QR_DEDATED: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        13.2291666666667
        161.395833333333)
      Font.Height = -12
      FontSize = 9
    end
    inherited QR_NAME1D: TQRMemo
      Size.Values = (
        34.3958333333333
        211.666666666667
        13.2291666666667
        658.8125)
      FontSize = 9
    end
    inherited QR_PRICED: TQRLabel
      Size.Values = (
        37.0416666666667
        920.75
        13.2291666666667
        145.520833333333)
      FontSize = 9
    end
    inherited QR_STQTYD: TQRLabel
      Size.Values = (
        37.0416666666667
        1156.22916666667
        13.2291666666667
        145.520833333333)
      FontSize = 9
    end
    inherited QR_SUPSTAMTD: TQRLabel
      Size.Values = (
        37.0416666666667
        1428.75
        13.2291666666667
        193.145833333333)
      FontSize = 9
    end
    inherited QR_USSTAMTD: TQRLabel
      Size.Values = (
        37.0416666666667
        1436.6875
        55.5625
        177.270833333333)
      FontSize = 9
    end
    inherited QR_TAXSTAMTD: TQRLabel
      Size.Values = (
        37.0416666666667
        1688.04166666667
        13.2291666666667
        193.145833333333)
      FontSize = 9
    end
    inherited QR_RATE_D: TQRLabel
      Size.Values = (
        37.0416666666667
        1735.66666666667
        55.5625
        145.520833333333)
      FontSize = 9
    end
    inherited QR_SIZE1D: TQRMemo
      Size.Values = (
        34.3958333333333
        211.666666666667
        50.2708333333333
        658.8125)
      FontSize = 9
    end
    inherited QR_DEREM1D: TQRMemo
      Size.Values = (
        34.3958333333333
        211.666666666667
        87.3125
        658.8125)
      FontSize = 9
    end
  end
  inherited SummaryBand1: TQRBand
    BeforePrint = SummaryBand1BeforePrint
    Size.Values = (
      121.708333333333
      1899.70833333333)
    inherited QRImage7: TQRImage
      Size.Values = (
        13.2291666666667
        0
        13.2291666666667
        1899.70833333333)
    end
    inherited QRLabel78: TQRLabel
      Size.Values = (
        37.0416666666667
        63.5
        52.9166666666667
        95.25)
      FontSize = 9
    end
    inherited QR_SUPTAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        1444.625
        34.3958333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_USTAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        1452.5625
        76.7291666666667
        145.520833333333)
      FontSize = 9
    end
    inherited QR_TAXTAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        1719.79166666667
        34.3958333333333
        161.395833333333)
      FontSize = 9
    end
  end
  inherited ChildBand25: TQRChildBand
    BeforePrint = ChildBand25BeforePrint
    Size.Values = (
      79.375
      1899.70833333333)
    inherited QRImage8: TQRImage
      Size.Values = (
        13.2291666666667
        0
        0
        1899.70833333333)
    end
    inherited QRLabel82: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        23.8125
        304.270833333333)
      FontSize = 9
    end
    inherited QRImage9: TQRImage
      Size.Values = (
        13.2291666666667
        0
        66.1458333333333
        1899.70833333333)
    end
    inherited QR_TAMT: TQRLabel
      Size.Values = (
        37.0416666666667
        378.354166666667
        23.8125
        113.770833333333)
      FontSize = 9
    end
  end
  inherited ChildBand26: TQRChildBand
    BeforePrint = ChildBand26BeforePrint
    Size.Values = (
      391.583333333333
      1899.70833333333)
    inherited QRLabel84: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        13.2291666666667
        304.270833333333)
      FontSize = 9
    end
    inherited label1: TQRLabel
      Size.Values = (
        37.0416666666667
        378.354166666667
        13.2291666666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QR_AMT12: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        13.2291666666667
        129.645833333333)
      FontSize = 9
    end
    inherited QR_AMT11: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        52.9166666666667
        129.645833333333)
      FontSize = 9
    end
    inherited label2: TQRLabel
      Size.Values = (
        37.0416666666667
        378.354166666667
        97.8958333333333
        97.8958333333333)
      FontSize = 9
    end
    inherited QR_AMT22: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        97.8958333333333
        129.645833333333)
      FontSize = 9
    end
    inherited QR_AMT21: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        137.583333333333
        129.645833333333)
      FontSize = 9
    end
    inherited label3: TQRLabel
      Size.Values = (
        37.0416666666667
        378.354166666667
        182.5625
        97.8958333333333)
      FontSize = 9
    end
    inherited QR_AMT32: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        182.5625
        129.645833333333)
      FontSize = 9
    end
    inherited QR_AMT31: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        222.25
        129.645833333333)
      FontSize = 9
    end
    inherited label4: TQRLabel
      Size.Values = (
        37.0416666666667
        378.354166666667
        267.229166666667
        97.8958333333333)
      FontSize = 9
    end
    inherited QR_AMT42: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        267.229166666667
        129.645833333333)
      FontSize = 9
    end
    inherited QR_AMT41: TQRLabel
      Size.Values = (
        37.0416666666667
        542.395833333333
        306.916666666667
        129.645833333333)
      FontSize = 9
    end
  end
  inherited ChildBand27: TQRChildBand
    BeforePrint = ChildBand27BeforePrint
    Size.Values = (
      95.25
      1899.70833333333)
    inherited QRLabel86: TQRLabel
      Size.Values = (
        37.0416666666667
        42.3333333333333
        26.4583333333333
        161.395833333333)
      FontSize = 9
    end
    inherited QR_INDICATOR: TQRLabel
      Size.Values = (
        37.0416666666667
        185.208333333333
        26.4583333333333
        193.145833333333)
      FontSize = 9
    end
    inherited QRLabel88: TQRLabel
      Size.Values = (
        37.0416666666667
        359.833333333333
        26.4583333333333
        34.3958333333333)
      FontSize = 9
    end
    inherited QRImage13: TQRImage
      Size.Values = (
        13.2291666666667
        0
        0
        1899.70833333333)
    end
    inherited QRImage14: TQRImage
      Size.Values = (
        13.2291666666667
        0
        76.7291666666667
        1899.70833333333)
    end
  end
  inherited ChildBand28: TQRChildBand
    Height = 103
    Size.Values = (
      272.520833333333
      1899.70833333333)
    inherited QRShape2: TQRShape
      Size.Values = (
        145.520833333333
        52.9166666666667
        26.4583333333333
        1759.47916666667)
    end
    inherited QRLabel89: TQRLabel
      Size.Values = (
        37.0416666666667
        357.1875
        55.5625
        1209.14583333333)
      FontSize = 9
    end
    inherited QRLabel90: TQRLabel
      Size.Values = (
        37.0416666666667
        357.1875
        97.8958333333333
        574.145833333333)
      FontSize = 9
    end
    inherited QRLabel18: TQRLabel
      Size.Values = (
        37.0416666666667
        1656.29166666667
        185.208333333333
        150.8125)
      FontSize = 10
    end
  end
  inherited PageFooterBand1: TQRBand
    Top = 1068
    Size.Values = (
      76.7291666666667
      1899.70833333333)
    inherited QRLabel14: TQRLabel
      Left = 612
      Size.Values = (
        37.0416666666667
        1619.25
        15.875
        224.895833333333)
      FontSize = 10
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, '
      '             AG_FTX4, AG_FTX5, SE_NAME3, BY_NAME3,'
      ''
      '            VATBI4_NAME.INDICATOR_NAME as INDICATOR_NAME'
      ''
      'From VATBI4_H'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as INDICATOR_NAME FROM CODE2NDD with' +
        '(nolock) WHERE Prefix = '#39#50689#49688'/'#52397#44396#39') VATBI4_NAME ON VATBI4_H.INDICAT' +
        'OR = VATBI4_NAME.CODE'
      ''
      'WHERE MAINT_NO = :MAINT_NO')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
    object qryListINDICATOR_NAME: TStringField
      FieldName = 'INDICATOR_NAME'
      Size = 100
    end
  end
  inherited qryGoods: TADOQuery
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        'KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY, QTY_G,' +
        ' QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, USAMT_G, S' +
        'UPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, RATE'
      'FROM VATBI4_D'
      'WHERE KEYY = :MAINT_NO')
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsDE_DATE: TStringField
      FieldName = 'DE_DATE'
      Size = 8
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsDE_REM1: TMemoField
      FieldName = 'DE_REM1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsSUPAMT: TBCDField
      FieldName = 'SUPAMT'
      Precision = 18
    end
    object qryGoodsTAXAMT: TBCDField
      FieldName = 'TAXAMT'
      Precision = 18
    end
    object qryGoodsUSAMT: TBCDField
      FieldName = 'USAMT'
      Precision = 18
    end
    object qryGoodsUSAMT_G: TStringField
      FieldName = 'USAMT_G'
      Size = 3
    end
    object qryGoodsSUPSTAMT: TBCDField
      FieldName = 'SUPSTAMT'
      Precision = 18
    end
    object qryGoodsTAXSTAMT: TBCDField
      FieldName = 'TAXSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT: TBCDField
      FieldName = 'USSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT_G: TStringField
      FieldName = 'USSTAMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
  end
end
