unit SPCNTC_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TSPCNTC_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QRLabel5: TQRLabel;
    QR_BGMGUBUN: TQRLabel;
    QRLabel2: TQRLabel;
    QR_NTDATE: TQRLabel;
    QRLabel7: TQRLabel;
    QR_CPDATE: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_CPBANKNAME: TQRLabel;
    QRImage5: TQRImage;
    QR_CPBANKBU: TQRLabel;
    QR_CPBANK: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_APPSNAME: TQRLabel;
    QR_APPNAME: TQRLabel;
    QR_APPNO: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_DMNO: TQRLabel;
    ChildBand5: TQRChildBand;
    ChildBand6: TQRChildBand;
    QR_CPNO: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QR_LCNO: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel12: TQRLabel;
    QR_CPFTX1: TQRMemo;
    ChildBand8: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_CPAMTU: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_CPAMT: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel18: TQRLabel;
    QR_CPTOTCHARGE: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_CPTOTAMT: TQRLabel;
    ChildBand12: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRImage2: TQRImage;
    DetailBand1: TQRBand;
    QR_CHARGENO: TQRLabel;
    QR_BASAMT: TQRLabel;
    QR_SESSDATE: TQRLabel;
    QR_SESEDATE: TQRLabel;
    QR_SESDAY: TQRLabel;
    QR_CUXRATE: TQRLabel;
    QR_CHARGERATE: TQRLabel;
    QR_CHAAMT: TQRLabel;
    SummaryBand1: TQRBand;
    ChildBand13: TQRChildBand;
    QRImage3: TQRImage;
    QRImage4: TQRImage;
    QRLabel26: TQRLabel;
    QR_CPBANKNAME2: TQRLabel;
    QR_CPBANKBU2: TQRLabel;
    QR_CPBANKELEC: TQRLabel;
    QRShape2: TQRShape;
    QRLabel133: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel126: TQRLabel;
    qryList: TADOQuery;
    qryGoods: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListNT_DATE: TStringField;
    qryListCP_DATE: TStringField;
    qryListDM_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_NAME: TStringField;
    qryListCP_BANKELEC: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_TOTAMT: TBCDField;
    qryListCP_TOTAMTC: TStringField;
    qryListCP_TOTCHARGE: TBCDField;
    qryListCP_TOTCHARGEC: TStringField;
    qryListCP_FTX: TStringField;
    qryListCP_FTX1: TMemoField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPP_NO: TStringField;
    qryListCP_NO: TStringField;
    qryListRC_NO: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsCHARGE_NO: TStringField;
    qryGoodsCHARGE_TYPE: TStringField;
    qryGoodsCHARGE_RATE: TStringField;
    qryGoodsBAS_AMT: TBCDField;
    qryGoodsBAS_AMTC: TStringField;
    qryGoodsCHA_AMT: TBCDField;
    qryGoodsCHA_AMTC: TStringField;
    qryGoodsCUX_RATE: TBCDField;
    qryGoodsSES_DAY: TStringField;
    qryGoodsSES_SDATE: TStringField;
    qryGoodsSES_EDATE: TStringField;
    PageFooterBand1: TQRBand;
    QRLabel8: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    qryGoodsCHARGE_NM: TStringField;
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
    function StrCase(Selector: string; StrList: array of string): Integer;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  SPCNTC_PRINT_frm: TSPCNTC_PRINT_frm;

implementation

uses Commonlib, MSSQL;
{$R *.dfm}

{ TSPCNTC_PRINT_frm }

function TSPCNTC_PRINT_frm.StrCase(Selector: string;
  StrList: array of string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(StrList) do
  begin
    if Selector = StrList[I] then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TSPCNTC_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;

  qryGoods.Close;
  qryGoods.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryGoods.Open;
end;

procedure TSPCNTC_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TSPCNTC_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //전자문서번호
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //통지일자
  QR_NTDATE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryListNT_DATE.AsString));
  //응답서구분
  case StrCase(Trim(qryListBGM_GUBUN.AsString) , ['2CQ', '2DT', '2CR']) of
    0: QR_BGMGUBUN.Caption := '정상통보';
    1: QR_BGMGUBUN.Caption := '구매자통보';
    2: QR_BGMGUBUN.Caption := '오류통보';
  end;
  //매입일자
  QR_CPDATE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryListCP_DATE.AsString)); 

end;

procedure TSPCNTC_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심(매입)은행
  QR_CPBANK.Caption := qryListCP_BANK.AsString;
  QR_CPBANKNAME.Caption := qryListCP_BANKNAME.AsString;
  QR_CPBANKBU.Caption := qryListCP_BANKBU.AsString;
end;

procedure TSPCNTC_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심(매입)신청인
  ChildBand3.Height := ChildBand3.Tag;
  QR_APPSNAME.Caption := qryListAPP_SNAME.AsString;
  QR_APPNAME.Caption := qryListAPP_NAME.AsString;
  QR_APPNO.Caption := qryListAPP_NO.AsString;
  if (Trim(QR_APPSNAME.Caption) = '') and (Trim(QR_APPNAME.Caption) = '') then
  begin
    ChildBand3.Enabled := False;
  end;
end;

procedure TSPCNTC_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심(매입)의뢰서 번호
  ChildBand4.Height := ChildBand4.Tag;
  QR_DMNO.Caption := qryListDM_NO.AsString;
  if Trim(QR_DMNO.Caption) = '' then
    ChildBand4.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //내국신용장번호
  ChildBand5.Height := ChildBand5.Tag;
  QR_LCNO.Caption := qryListLC_NO.AsString;
  if Trim(QR_LCNO.Caption) = '' then
    ChildBand5.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //매입번호
  ChildBand6.Height := ChildBand6.Tag;
  QR_CPNO.Caption := qryListCP_NO.AsString;
  if Trim(QR_CPNO.Caption) = '' then
    ChildBand6.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //통보내역
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := Trim(qryListCP_FTX1.AsString);
    ChildBand7.Height := ( 14 * memoLines.Count) + 4;
    QR_CPFTX1.Height := ( 14 * memoLines.Count);

    QR_CPFTX1.Lines.Clear;
    QR_CPFTX1.Lines.Text := qryListCP_FTX1.AsString;

//    ShowMessage(IntToStr(QR_CPFTX1.Lines.Count));

    if QR_CPFTX1.Lines.Count = 0 then
    begin
      ChildBand7.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TSPCNTC_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심금액(외화)
  ChildBand8.Height := ChildBand8.Tag;
  QR_CPAMTU.Caption := qryListCP_AMTC.AsString + '   ' + FormatFloat('#,##0.####' , qryListCP_AMTU.AsCurrency);
  if qryListCP_AMTU.AsCurrency = 0 then
    ChildBand8.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심금액(원화)
  ChildBand9.Height := ChildBand9.Tag;
  QR_CPAMT.Caption := 'KRW   ' + FormatFloat('#,##0.####' , qryListCP_AMT.AsCurrency);
  if qryListCP_AMT.AsCurrency = 0 then
    ChildBand9.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //수수료(이자)합계
  ChildBand10.Height := ChildBand10.Tag;
  QR_CPTOTCHARGE.Caption := qryListCP_TOTCHARGEC.AsString + '   ' + FormatFloat('#,##0.####' , qryListCP_TOTCHARGE.AsCurrency);
  if qryListCP_TOTCHARGE.AsCurrency = 0 then
    ChildBand10.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //최종지급(수입)액
  ChildBand11.Height := ChildBand11.Tag;
  QR_CPTOTAMT.Caption := qryListCP_TOTAMTC.AsString + '   ' + FormatFloat('#,##0.####' , qryListCP_TOTAMT.AsCurrency);
  if qryListCP_TOTAMT.AsCurrency = 0 then
    ChildBand11.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if qryGoods.RecordCount = 0 then ChildBand12.Enabled := False;
end;

procedure TSPCNTC_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if qryGoods.RecordCount > 0 then
  begin
    QRLabel28.Caption := qryGoodsCHARGE_NM.AsString;
    //수수료(이자)유형 (계산서번호)
    QR_CHARGENO.Caption := '( ' + qryGoodsCHARGE_NO.AsString + ' )' ;

    //대상금액
    QR_BASAMT.Caption := qryGoodsBAS_AMTC.AsString + '  ' + FormatFloat('#,##0.####' , qryGoodsBAS_AMT.AsCurrency);

    //적용일수
    QR_SESSDATE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryGoodsSES_SDATE.AsString));
    QR_SESEDATE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryGoodsSES_EDATE.AsString));
    //일수
    QR_SESDAY.Caption := qryGoodsSES_DAY.AsString;

    //적용환율
    QR_CUXRATE.Caption := FormatFloat('#,##0.####' , qryGoodsCUX_RATE.AsCurrency);
    //적용요율
    QR_CHARGERATE.Caption := qryGoodsCHARGE_RATE.AsString;

    //산출금액
    QR_CHAAMT.Caption := qryGoodsCHA_AMTC.AsString + '  ' + FormatFloat('#,##0.####' , qryGoodsCHA_AMT.AsCurrency);

    qryGoods.Next;
  end
  else
    DetailBand1.Enabled := False;

end;

procedure TSPCNTC_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryGoods.Eof;
end;

procedure TSPCNTC_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //발신기관 전자서명
  QR_CPBANKNAME2.Caption := qryListCP_BANKNAME.AsString;
  QR_CPBANKBU2.Caption := qryListCP_BANKBU.AsString;
  QR_CPBANKELEC.Caption := qryListCP_BANKELEC.AsString;
end;

end.
