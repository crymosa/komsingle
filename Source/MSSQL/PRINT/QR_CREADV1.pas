unit QR_CREADV1;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, StrUtils, DateUtils, DB,
  ADODB;

type
  TQR_CREADV1_PRN = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel39: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    QR_ADV_DATE: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QR_TRN1NAME1: TQRLabel;
    QR_TRN2NAME1: TQRLabel;
    memo_OD1BANK: TQRMemo;
    memo_BN1BANK: TQRMemo;
    QR_EXE_DATE: TQRLabel;
    QR_POS_DATE: TQRLabel;
    QR_ADREFNO: TQRLabel;
    memo_ADINFO11: TQRMemo;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QR_RELIST_TYPE: TQRLabel;
    QR_RELIST_NO: TQRLabel;
    QR_RELIST_ADDNO: TQRLabel;
    QR_RELIST_APPDT: TQRLabel;
    QRImage2: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRImage3: TQRImage;
    QRLabel22: TQRLabel;
    QRLabel20: TQRLabel;
    QRImage4: TQRImage;
    QRImage5: TQRImage;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRImage6: TQRImage;
    QRImage7: TQRImage;
    QR_ORD1AMT: TQRLabel;
    QR_ORD2AMT: TQRLabel;
    QR_ORD3AMT: TQRLabel;
    QR_ORD4AMT: TQRLabel;
    QR_ORD1AMTC: TQRLabel;
    QR_ORD2AMTC: TQRLabel;
    QR_ORD3AMTC: TQRLabel;
    QR_ORD4AMTC: TQRLabel;
    QR_EXCRATE4: TQRLabel;
    QR_EXCDATE4: TQRLabel;
    DetailBand1: TQRBand;
    ColumnHeaderBand1: TQRBand;
    QRLabel36: TQRLabel;
    QRImage8: TQRImage;
    QRLabel37: TQRLabel;
    QRImage9: TQRImage;
    QRLabel38: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    SummaryBand1: TQRBand;
    QRChildBand1: TQRChildBand;
    QRLabel43: TQRLabel;
    QRImage10: TQRImage;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QR_TRN3NAME1: TQRLabel;
    QR_TRN3NAME2: TQRLabel;
    QR_TRN3NAME3: TQRLabel;
    ChildBand1: TQRChildBand;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRShape1: TQRShape;
    ADOQuery1: TADOQuery;
    QRLabel17: TQRLabel;
    QRLabel24: TQRLabel;
    QR_RATECD41: TQRLabel;
    QR_RATECD42: TQRLabel;
    QR_FC1NM: TQRLabel;
    QR_FC1SA1: TQRLabel;
    QR_FC1RATE: TQRLabel;
    QR_FC1RD: TQRLabel;
    QRImage11: TQRImage;
    QR_FC1SA1C: TQRLabel;
    QR_FC1CH: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ColumnHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure SummaryBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FCOMMON : TADOQuery;
    FDOCLIST : TADOQuery;
    FD1 : TADOQuery;
    FFC1 : TADOQuery;
  public
    property COMMON : TADOQuery read FCOMMON write FCOMMON;
    property DOCLIST : TADOQuery read FDOCLIST write FDOCLIST;
    property D1 : TADOQuery read FD1 write FD1;
    property FC1 : TADOQuery read FFC1 write FFC1;
  end;

var
  QR_CREADV1_PRN: TQR_CREADV1_PRN;

implementation

{$R *.DFM}

procedure TQR_CREADV1_PRN.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  DOCLIST.First;
  D1.First;
  FC1.First;
end;

procedure TQR_CREADV1_PRN.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  QR_MAINT_NO.Caption := FCOMMON.FieldByName('MAINT_NO').AsString;
  TMP_STR := FCOMMON.FieldByName('ADV_DATE').AsString;
  QR_EXE_DATE.Caption := LeftStr(TMP_STR,4)+'.'+MidStr(TMP_STR, 5, 2)+'.'+RightStr(TMP_STR, 2);

  QR_TRN1NAME1.Caption := FCOMMON.FieldByName('TRN1NAME1').AsString;
  QR_TRN2NAME1.Caption := FCOMMON.FieldByName('TRN2NAME1').AsString;

  memo_OD1BANK.Lines.Clear;
  memo_OD1BANK.Lines.Add(FCOMMON.FieldByName('OD1BANK').AsString);
  memo_OD1BANK.Lines.Add(FCOMMON.FieldByName('OD1BANK1').AsString);
  memo_OD1BANK.Lines.Add(FCOMMON.FieldByName('OD1BANK2').AsString);
  memo_OD1BANK.Lines.Add(FCOMMON.FieldByName('OD1FB').AsString);

  memo_BN1BANK.Lines.Clear;
  memo_BN1BANK.Lines.Add(FCOMMON.FieldByName('BN1BANK').AsString);
  memo_BN1BANK.Lines.Add(FCOMMON.FieldByName('BN1BANK1').AsString);
  memo_BN1BANK.Lines.Add(FCOMMON.FieldByName('BN1BANK2').AsString);
  memo_BN1BANK.Lines.Add(FCOMMON.FieldByName('BN1FB').AsString);

  TMP_STR := FCOMMON.FieldByName('EXE_DATE').AsString;
  QR_EXE_DATE.Caption := LeftStr(TMP_STR,4)+'.'+MidStr(TMP_STR, 5, 2)+'.'+RightStr(TMP_STR, 2);
  TMP_STR := FCOMMON.FieldByName('POS_DATE').AsString;
  QR_POS_DATE.Caption := LeftStr(TMP_STR,4)+'.'+MidStr(TMP_STR, 5, 2)+'.'+RightStr(TMP_STR, 2);

  //기타참조번호
  QR_ADREFNO.Caption := FCOMMON.FieldByName('ADREFNO').AsString;

  //입금관련서류 - 맨처음 1건만
  QR_RELIST_TYPE.Caption  := FDOCLIST.FieldByName('ViewType').AsString;
  QR_RELIST_NO.Caption    := FDOCLIST.FieldByName('RELIST_NO').AsString;
  QR_RELIST_ADDNO.Caption := FDOCLIST.FieldByName('RELIST_ADDNO').AsString;
  QR_RELIST_APPDT.Caption := FDOCLIST.FieldByName('RELIST_APPDT').AsString;

  //기타정보
  memo_ADINFO11.Lines.Clear;
  memo_ADINFO11.Lines.Text := FCOMMON.FieldByName('ADINFO11').AsString;

  //금액정보
  QR_ORD1AMTC.Caption := FCOMMON.FieldByName('ORD1AMTC').AsString;
  QR_ORD1AMT.Caption  := FormatFloat('#,#.###', FCOMMON.FieldByName('ORD1AMT').AsCurrency);

  QR_ORD2AMTC.Caption := FCOMMON.FieldByName('ORD2AMTC').AsString;
  QR_ORD2AMT.Caption  := FormatFloat('#,#.###', FCOMMON.FieldByName('ORD2AMT').AsCurrency);

  QR_ORD3AMTC.Caption := FCOMMON.FieldByName('ORD3AMTC').AsString;
  QR_ORD3AMT.Caption  := FormatFloat('#,#.###', FCOMMON.FieldByName('ORD3AMT').AsCurrency);

  QR_ORD4AMTC.Caption := FCOMMON.FieldByName('ORD4AMTC').AsString;
  QR_ORD4AMT.Caption  := FormatFloat('#,#.###', FCOMMON.FieldByName('ORD4AMT').AsCurrency);
  QR_RATECD41.Caption  := FCOMMON.FieldByName('RATECD41').AsString;
  QR_RATECD42.Caption  := FCOMMON.FieldByName('RATECD42').AsString;
  QR_EXCRATE4.Caption := FormatFloat('#,#.###', FCOMMON.FieldByName('EXCRATE4').AsCurrency);
  QR_EXCDATE4.Caption := FCOMMON.FieldByName('EXCDATE41').AsString + ' ~ ' + FCOMMON.FieldByName('EXCDATE42').AsString;
  QR_EXCDATE4.Enabled := Trim(QR_EXCDATE4.Caption) <> '~'; 

end;

procedure TQR_CREADV1_PRN.QRChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_TRN3NAME1.Caption := FCOMMON.FieldByName('TRN3NAME1').AsString;
  QR_TRN3NAME2.Caption := FCOMMON.FieldByName('TRN3NAME2').AsString;
  QR_TRN3NAME3.Caption := FCOMMON.FieldByName('TRN3NAME3').AsString;
end;

procedure TQR_CREADV1_PRN.ColumnHeaderBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRLabel36.Caption := D1.FieldByName('FC1NM').AsString;
end;

procedure TQR_CREADV1_PRN.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
//  MoreData := not FFC1.Eof;
end;

procedure TQR_CREADV1_PRN.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_FC1CH.Enabled := FFC1.RecordCount > 0;
  QR_FC1NM.Enabled := QR_FC1CH.Enabled;
  QR_FC1SA1.Enabled := QR_FC1CH.Enabled;
  QR_FC1SA1C.Enabled := QR_FC1CH.Enabled;
  QR_FC1RATE.Enabled := QR_FC1CH.Enabled;
  QR_FC1RD.Enabled   := QR_FC1CH.Enabled;

  QR_FC1CH.Caption   := FFC1.FieldByName('FC1CH').AsString;
  QR_FC1NM.Caption   := FFC1.FieldByName('FC1NM').AsString;
  QR_FC1SA1.Caption  := FormatFloat('#,0.###', FFC1.FieldByName('FC1SA1').asCurrency);
  QR_FC1SA1C.Caption := FFC1.FieldByName('FC1SA1C').AsString;
  QR_FC1RATE.Caption := FormatFloat('#,0.##', FFC1.FieldByName('FC1RATE').asCurrency);
  QR_FC1RD.Caption := FFC1.FieldByName('FC1RD1').AsString + ' ~ ' + FFC1.FieldByName('FC1RD2').AsString;
  IF Trim(QR_FC1RD.Caption) = '~' Then QR_FC1RD.Caption := '';

  FDOCLIST.Next;
end;

procedure TQR_CREADV1_PRN.SummaryBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //FC1AMT11C
  QRLabel25.Caption := FD1.FieldByName('FC1AMT11C').AsString;
  QRLabel26.Caption := FormatFloat('#,0.##', FD1.FieldByName('FC1AMT11').asCurrency);
  QRLabel27.Caption := FD1.FieldByName('FC1AMT12C').AsString;
  QRLabel28.Caption := FormatFloat('#,0.##', FD1.FieldByName('FC1AMT12').asCurrency);
end;

end.
