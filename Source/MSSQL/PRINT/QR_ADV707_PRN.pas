unit QR_ADV707_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TQR_ADV707_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel88: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRImage1: TQRImage;
    QRLabel4: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QR_MAINT_NO: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_APP_NO: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK_INFO1: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK_INFO: TQRLabel;
    QR_ADBANK_TEL: TQRLabel;
    QR_BENEFC_TEL: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC1: TQRLabel;
    CHILD_01: TQRChildBand;
    QRLabel13: TQRLabel;
    QRImage2: TQRImage;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO_TXT: TStringField;
    qryListMAINT_NO: TStringField;
    qryListAMD_NO: TIntegerField;
    qryListAMD_NO1: TIntegerField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListISS_BANK: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListIBANK_REF: TStringField;
    qryListPRNO: TIntegerField;
    qryListGOODS_DESC: TStringField;
    qryListGOODS_DESC_1: TMemoField;
    qryListDOC_DESC: TStringField;
    qryListDOC_DESC_1: TMemoField;
    qryListADD_DESC: TStringField;
    qryListADD_DESC_1: TMemoField;
    qryListSPECIAL_DESC: TStringField;
    qryListSPECIAL_DESC_1: TMemoField;
    qryListINST_DESC: TStringField;
    qryListINST_DESC_1: TMemoField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListPURP_MSG: TStringField;
    qryListPURP_MSG_NM: TStringField;
    qryListCAN_REQ: TStringField;
    qryListCON_INST: TStringField;
    qryListDOC_TYPE: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_NUM1: TStringField;
    qryListCHARGE_NUM2: TStringField;
    qryListCHARGE_NUM3: TStringField;
    qryListCHARGE_NUM4: TStringField;
    qryListCHARGE_NUM5: TStringField;
    qryListCHARGE_NUM6: TStringField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_NUM1: TStringField;
    qryListAMD_CHARGE_NUM2: TStringField;
    qryListAMD_CHARGE_NUM3: TStringField;
    qryListAMD_CHARGE_NUM4: TStringField;
    qryListAMD_CHARGE_NUM5: TStringField;
    qryListAMD_CHARGE_NUM6: TStringField;
    qryListTSHIP: TStringField;
    qryListPSHIP: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListCO_BANK: TStringField;
    qryListCO_BANK1: TStringField;
    qryListCO_BANK2: TStringField;
    qryListCO_BANK3: TStringField;
    qryListCO_BANK4: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAMD_APPLIC1: TStringField;
    qryListAMD_APPLIC2: TStringField;
    qryListAMD_APPLIC3: TStringField;
    qryListAMD_APPLIC4: TStringField;
    qryListPERIOD: TIntegerField;
    qryListPERIOD_TXT: TStringField;
    qryListNBANK_ISS: TStringField;
    qryListNBANK_ISS1: TStringField;
    qryListNBANK_ISS2: TStringField;
    qryListNBANK_ISS3: TStringField;
    qryListNBANK_ISS4: TStringField;
    QR_APBANK_INFO2: TQRLabel;
    QRMemo1: TQRMemo;
    QR_CDNO: TQRLabel;
    QR_RCVREF: TQRLabel;
    QR_IBANKREF: TQRLabel;
    CHILD_02: TQRChildBand;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QR_AMDNO1: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QR_AMDDATE: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel37: TQRLabel;
    QR_PURP_MSG: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    CHILD_03: TQRChildBand;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    CHILD_04: TQRChildBand;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel51: TQRLabel;
    CHILD_05: TQRChildBand;
    QRLabel50: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    CHILD_06: TQRChildBand;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    CHILD_07: TQRChildBand;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    CHILD_08: TQRChildBand;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    CHILD_09: TQRChildBand;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    CHILD_10: TQRChildBand;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    CHILD_11: TQRChildBand;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    CHILD_12: TQRChildBand;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    CHILD_13: TQRChildBand;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    CHILD_14: TQRChildBand;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    CHILD_15: TQRChildBand;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    CHILD_16: TQRChildBand;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    CHILD_17: TQRChildBand;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    CHILD_18: TQRChildBand;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    CHILD_19: TQRChildBand;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QR44C_4: TQRLabel;
    QRLabel132: TQRLabel;
    QR44C_5: TQRLabel;
    QR44C_6: TQRLabel;
    QR44C_1: TQRLabel;
    QR44C_2: TQRLabel;
    QR44C_3: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_01BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_02BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_03BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FMaint_No : String;
    FAmd_No : Integer;
  public
    property Maint_No:String read FMaint_No write FMaint_No;
    property Amd_No:Integer read FAmd_No write FAmd_No;
  end;

var
  QR_ADV707_PRN_frm: TQR_ADV707_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_ADV707_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  with qryList do
  begin
    Close;
    // MAINT_NO
    Parameters[0].Value := FMaint_No;
    // AMD_NO
    Parameters[1].Value := FAmd_No;
    Open;    
  end;
end;

procedure TQR_ADV707_PRN_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  QR_MAINT_NO.Caption := qryListMAINT_NO.AsString;
  TMP_STR := qryListAPP_DATE.AsString;
  insert('-', TMP_STR, 5);
  Insert('-', TMP_STR, 8);
  QR_APPDATE.Caption  := TMP_STR;
  QR_APP_NO.Caption := qryListAPP_NO.AsString;
  QR_APBANK.Caption := qryListAP_BANK.AsString;
  QR_APBANK_INFO1.Caption := Trim(qryListAP_BANK1.AsString+' '+qryListAP_BANK2.AsString);
  QR_APBANK_INFO2.Caption := Trim(qryListAP_BANK3.AsString+' '+qryListAP_BANK4.AsString);

  QR_ADBANK.Caption := qryListAD_BANK.AsString;
  TMP_STR := Trim(qryListAD_BANK1.AsString+' '+qryListAD_BANK2.AsString+' '+qryListAD_BANK3.AsString+' '+qryListAD_BANK4.AsString);
  QR_ADBANK_INFO.Caption := TMP_STR;
  QR_ADBANK_TEL.Caption := qryListAD_BANK5.AsString;

  QR_BENEFC1.Caption := qryListBENEFC1.AsString;
  TMP_STR := Trim( qryListBENEFC2.AsString+' '+qryListBENEFC3.AsString+' '+qryListBENEFC3.AsString+' '+qryListBENEFC4.AsString+' '+qryListBENEFC5.AsString);
  QR_BENEFC2.Caption := TMP_STR;
  QR_BENEFC_TEL.Caption := qryListBENEFC6.AsString;

  QRMemo1.Lines.Text := qryListADDINFO_1.AsString;
  IF QRmemo1.Lines.Count > 1 Then
  begin
    QRmemo1.Height := QRMemo1.Lines.Count * 12;
    QRShape7.Top := QRMemo1.Top+QRMemo1.Height+3;
  end
  else
  begin
    QRmemo1.Height := 13;
    QRShape7.Top := 426;
  end;
  QRShape13.Height := QRShape7.Top - 163;
  QRShape14.Height := QRShape13.Height;
  QRShape15.Height := QRShape13.Height;

  TitleBand1.Height := QRShape7.Top+7;
end;

procedure TQR_ADV707_PRN_frm.CHILD_01BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := Sender.Tag;

  QR_CDNO.Caption := qryListCD_NO.AsString;
  QR_RCVREF.Caption := qryListRCV_REF.AsString;
  QR_IBANKREF.Caption := qryListIBANK_REF.AsString;

  if Trim(qryListISS_BANK.AsString) <> '' Then
  begin
    QRLabel23.Caption := '52a';
    QRLabel24.Caption := 'Issuing Bank';
    QRLabel32.Caption := qryListISS_BANK.AsString;
  end
  else
  if Trim(qryListNBANK_ISS.AsString) <> '' Then
  begin
    QRLabel23.Caption := '50B';
    QRLabel24.Caption := 'NON-Bank Issuer';
    QRLabel32.Caption := qryListNBANK_ISS.AsString;
  end;

  QR_ISSDATE.Caption := RightStr( qryListISS_DATE.AsString, 6);
  if qryListAMD_NO1.IsNull Then
    QR_AMDNO1.Caption  := qryListAMD_NO.AsString
  else
    QR_AMDNO1.Caption  := qryListAMD_NO1.AsString;

  QR_AMDDATE.Caption  := RightStr( qryListAMD_DATE.AsString, 6);
  QR_PURP_MSG.Caption := qryListPURP_MSG_NM.AsString;

end;

procedure TQR_ADV707_PRN_frm.CHILD_02BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := Sender.Tag;
  PrintBand := qryListCAN_REQ.AsString = 'CR';
end;

procedure TQR_ADV707_PRN_frm.CHILD_03BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  sSwiftCode, sContent, sValue : string;
begin
  //Applicable Rules
  Sender.Height := Sender.Tag;
  sSwiftCode := Sender.Hint;

  if sSwiftCode = '40E' then
  begin
    sContent := 'Applicable Rules';
    PrintBand := not qryListAPPLICABLE_RULES_1.IsNull;
    sValue := qryListAPPLICABLE_RULES_1.AsString;
    if sValue = 'OTHR' then
      sValue := qryListAPPLICABLE_RULES_2.AsString;

    QRLabel42.Caption := sSwiftCode;
    QRLabel43.Caption := sContent;
    QRLabel45.Caption := sValue;
  end
  else
  if sSwiftCode = '31D' Then
  begin
    PrintBand := not qryListEX_DATE.IsNull;
    QRLabel49.Caption := RightStr(qryListEX_DATE.AsString, 6)+' '+qryListEX_PLACE.AsString;
  end
  else
  if sSwiftCode = '50' Then
  begin
    PrintBand := not qryListAMD_APPLIC1.IsNull;
    QRLabel54.Caption := Trim(qryListAMD_APPLIC1.AsString+' '+qryListAMD_APPLIC2.AsString);
    QRLabel55.Caption := Trim(qryListAMD_APPLIC3.AsString+' '+qryListAMD_APPLIC4.AsString);
  end
  else
  if sSwiftCode = '59' then
  begin
    PrintBand := not qryListBENEFC1.IsNull;
    QRLabel59.Caption := qryListBENEFC6.AsString;
    QRLabel60.Caption := Trim(qryListBENEFC1.AsString+' '+qryListBENEFC2.AsString);
    QRLabel61.Caption := Trim(qryListBENEFC3.AsString+' '+qryListBENEFC4.AsString);
  end
  else
  if sSwiftCode = '32B' Then
  begin
    PrintBand := not(qryListINCD_CUR.IsNull AND qryListDECD_CUR.IsNull);
    if not qryListINCD_CUR.IsNull Then
    begin
      sSwiftCode := '32B';
      sContent := 'Increase of Documentary';
      sValue := qryListINCD_CUR.AsString+' '+FormatFloat('#,0.00', qryListINCD_AMT.AsFloat);
    end
    else
    if not qryListDECD_CUR.IsNull Then
    begin
      sSwiftCode := '33B';
      sContent := 'Decrease of Documentary';
      sValue := qryListDECD_CUR.AsString+' '+FormatFloat('#,0.00', qryListINCD_AMT.AsFloat);
    end;
    QRLabel63.Caption := sContent;
    QRLabel65.Caption := sValue;
  end
  else
  if sSwiftCode = '39A' Then
  begin
    PrintBand := not qryListCD_PERP.IsNull;
    QRLabel70.Caption := qryListCD_PERP.AsString+'/'+qryListCD_PERM.AsString;
  end
  else
  if sSwiftCode = '39C' Then
  begin
    PrintBand := not qryListAA_CV1.IsNull;
    QRLabel75.Caption := Trim(qryListAA_CV1.AsString+' '+qryListAA_CV2.AsString);
    QRLabel76.Caption := Trim(qryListAA_CV3.AsString+' '+qryListAA_CV4.AsString);
  end
  else
  if sSwiftCode = '41a' then
  begin
    PrintBand := not qryListAVAIL.IsNull;
    QRLabel80.Caption := qryListAVAIL.AsString;
    QRLabel81.Caption := Trim(qryListAVAIL1.AsString+' '+qryListAVAIL2.AsString);
    QRLabel82.Caption := Trim(qryListAVAIL3.AsString+' '+qryListAVAIL4.AsString);
  end
  else
  if sSwiftCode = '42C' Then
  begin
    PrintBand := not(qryListDRAFT1.IsNull and qryListMIX_PAY1.IsNull and qryListDEF_PAY1.IsNull);
    if not qryListDRAFT1.IsNull then
    begin
      sSwiftCode := '42C';
      QRLabel84.Caption := 'Drafts at...';
      QRLabel90.Enabled := False;

      QRLabel86.Caption := qryListDRAFT1.AsString;
      QRLabel87.Caption := qryListDRAFT2.AsString;
      QRLabel89.Caption := qryListDRAFT3.AsString;
    end
    else
    if not qryListMIX_PAY1.IsNull then
    begin
      sSwiftCode := '42M';
      QRLabel84.Caption := 'Mixed payment Details';
      QRLabel90.Enabled := False;

      QRLabel86.Caption := qryListMIX_PAY1.AsString;
      QRLabel87.Caption := qryListMIX_PAY2.AsString;
      QRLabel89.Caption := Trim(qryListMIX_PAY3.AsString+' '+qryListMIX_PAY4.AsString);
    end
    else
    if not qryListDEF_PAY1.IsNull then
    begin
      sSwiftCode := '42P';
      QRLabel84.Caption := 'Negotiation/Deferred Paym';
      QRLabel90.Caption := 'ent Details';
      QRLabel90.Enabled := True;

      QRLabel86.Caption := qryListDEF_PAY1.AsString;
      QRLabel87.Caption := qryListDEF_PAY2.AsString;
      QRLabel89.Caption := Trim(qryListDEF_PAY3.AsString+' '+qryListDEF_PAY4.AsString);
    end;
  end
  else
  if sSwiftCode = '42a' Then
  begin
    PrintBand := not qryListDRAWEE.IsNull;
    QRLabel95.Caption := qryListDRAWEE.AsString;
    QRLabel96.Caption := Trim(qryListDRAWEE1.AsString+' '+qryListDRAWEE2.AsString);
    QRLabel97.Caption := Trim(qryListDRAWEE3.AsString+' '+qryListDRAWEE4.AsString);
  end
  else
  if sSwiftCode = '43P' Then
  begin
    PrintBand := not qryListPSHIP.IsNull;
    QRLabel101.Caption := qryListPSHIP.AsString;
  end
  else
  if sSwiftCode = '43T' Then
  begin
    PrintBand := not qryListPSHIP.IsNull;
    QRLabel105.Caption := qryListTSHIP.AsString;
  end
  else
  if sSwiftCode = '44A' Then
  begin
    PrintBand := not qryListLOAD_ON.IsNull;
    QRLabel109.Caption := qryListLOAD_ON.AsString;
  end
  else
  if sSwiftCode = '44E' Then
  begin
    PrintBand := not qryListSUNJUCK_PORT.IsNull;
    QRLabel115.Caption := qryListSUNJUCK_PORT.AsString;
  end
  else
  if sSwiftCode = '44F' Then
  begin
    PrintBand := not qryListDOCHACK_PORT.IsNull;
    QRLabel120.Caption := qryListDOCHACK_PORT.AsString;
  end
  else
  if sSwiftCode = '44B' Then
  begin
    PrintBand := not qryListFOR_TRAN.IsNull;
    QRLabel125.Caption := qryListFOR_TRAN.AsString;
  end
  else
  if sSwiftCode = '44C' Then
  begin
    PrintBand := not (qryListLST_DATE.IsNull and qryListSHIP_PD1.IsNull);
    if not qryListLST_DATE.IsNull Then
    begin
      QRLabel128.Caption := '44C';
      QRLabel129.Caption := 'Latest Date of Shipment';
      QRLabel132.Caption := '(YYMMDD)';
      QRLabel132.Enabled := True;

      Sender.Height := 41;

      QR44C_1.Caption := RightStr( qryListLST_DATE.AsString, 6 );
    end
    else
    if not qryListSHIP_PD1.IsNull then
    begin
      QRLabel128.Caption := '44D';
      QRLabel129.Caption := 'Shipment Period';
      QRLabel132.Enabled := False;

      Sender.Height := 100;

      QR44C_1.Caption := qryListSHIP_PD1.AsString;
      QR44C_2.Caption := qryListSHIP_PD2.AsString;
      QR44C_3.Caption := qryListSHIP_PD3.AsString;
      QR44C_4.Caption := qryListSHIP_PD4.AsString;
      QR44C_5.Caption := qryListSHIP_PD5.AsString;
      QR44C_6.Caption := qryListSHIP_PD6.AsString;
    end;
  end;
end;

end.
