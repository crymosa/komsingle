unit QR_ADV710_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TQR_ADV710_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel88: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRImage1: TQRImage;
    QRLabel4: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QR_MAINT_NO: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_APP_NO: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK_INFO1: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK_INFO: TQRLabel;
    QR_ADBANK_TEL: TQRLabel;
    QR_BENEFC_TEL: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_APBANK_INFO2: TQRLabel;
    QRMemo1: TQRMemo;
    qryList: TADOQuery;
    CHILD_01: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel13: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QR_SENDER_REF: TQRLabel;
    QR_LIC_NO: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QR_DOCCD1: TQRLabel;
    QR_DOCCD2: TQRLabel;
    qryListMAINT_NO: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListSE_BANK: TStringField;
    qryListSE_BANK1: TStringField;
    qryListSE_BANK2: TStringField;
    qryListSE_BANK3: TStringField;
    qryListSE_BANK4: TStringField;
    qryListSE_BANK5: TStringField;
    qryListRE_BANK: TStringField;
    qryListRE_BANK1: TStringField;
    qryListRE_BANK2: TStringField;
    qryListRE_BANK3: TStringField;
    qryListRE_BANK4: TStringField;
    qryListRE_BANK5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListBEN_TEL: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListDOC_CD1: TStringField;
    qryListDOC_CD2: TStringField;
    qryListADV_NO: TStringField;
    qryListLIC_NO: TStringField;
    qryListREF_NO: TStringField;
    qryListISS_DATE: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListISS_BANK: TStringField;
    qryListNon_Bank_Issuer1: TStringField;
    qryListAPP_BANK: TStringField;
    qryListAPP_BANK1: TStringField;
    qryListAPP_BANK2: TStringField;
    qryListAPP_BANK3: TStringField;
    qryListAPP_BANK4: TStringField;
    qryListAPP_BANK5: TStringField;
    qryListISS_BANK1: TStringField;
    CHILD_02: TQRChildBand;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel21: TQRLabel;
    QR_PRE_ADV: TQRLabel;
    CHILD_03: TQRChildBand;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel29: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QR_APPLICABLE: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QR_EXDATE: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QR_ISSBANK: TQRLabel;
    CHILD_04: TQRChildBand;
    QRLabel27: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel37: TQRLabel;
    QR_APPBANK: TQRLabel;
    CHILD_05: TQRChildBand;
    QRLabel32: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    CHILD_06: TQRChildBand;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRBen1: TQRLabel;
    QRBen2: TQRLabel;
    QRBen3: TQRLabel;
    QRBen4: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRAMT: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRPCD: TQRLabel;
    QRLabel60: TQRLabel;
    CHILD_07: TQRChildBand;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRAACV1: TQRLabel;
    QRAACV2: TQRLabel;
    qryListCD_CUR: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    CHILD_08: TQRChildBand;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRAV_AIL1: TQRLabel;
    QRAV_AIL2: TQRLabel;
    qryListAV_AIL: TStringField;
    qryListAV_AIL1: TStringField;
    qryListAV_AIL2: TStringField;
    qryListAV_AIL3: TStringField;
    qryListAV_AIL4: TStringField;
    qryListAV_AIL5: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListAV_PAY: TStringField;
    CHILD_09: TQRChildBand;
    QRLabel51: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel59: TQRLabel;
    QRDR_AWEE: TQRLabel;
    qryListDR_AWEE: TStringField;
    CHILD_10: TQRChildBand;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    qryListDRAFT: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListMIX_PAY: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListDEF_PAY: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    QRLabel69: TQRLabel;
    CHILD_11: TQRChildBand;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRPSHIP: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRTSHIP: TQRLabel;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    CHILD_12: TQRChildBand;
    QRLabel73: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLoadon: TQRLabel;
    QRLabel80: TQRLabel;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    CHILD_13: TQRChildBand;
    QRLabel79: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRSUNJUK: TQRLabel;
    QRLabel84: TQRLabel;
    CHILD_14: TQRChildBand;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRDOCHAK: TQRLabel;
    QRLabel90: TQRLabel;
    CHILD_15: TQRChildBand;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRFORTRAN: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    CHILD_16: TQRChildBand;
    QRLabel83: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel95: TQRLabel;
    QR_LSTDATE: TQRLabel;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    DetailBand1: TQRBand;
    qryListDESGOOD_1: TMemoField;
    qryListDOCREQU_1: TMemoField;
    qryListADDCOND_1: TMemoField;
    qryListSPECIAL_DESC_1: TMemoField;
    qryListINSTRCT_1: TMemoField;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    SummaryBand1: TQRBand;
    CHILD_17: TQRChildBand;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRCharges1: TQRLabel;
    QRCharges2: TQRLabel;
    QRCharges3: TQRLabel;
    qryListCHARGE1: TStringField;
    qryListCHARGE2: TStringField;
    qryListCHARGE3: TStringField;
    qryListCHARGE4: TStringField;
    qryListCHARGE5: TStringField;
    qryListCHARGE6: TStringField;
    CHILD_18: TQRChildBand;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRPeriodDays: TQRLabel;
    qryListPERIOD_DAYS: TIntegerField;
    qryListPERIOD_TXT: TStringField;
    CHILD_19: TQRChildBand;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRConfirm: TQRLabel;
    qryListCONFIRMM: TStringField;
    CHILD_20: TQRChildBand;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRConfirmBank: TQRLabel;
    qryListCO_BANK: TStringField;
    qryListREI_BANK: TStringField;
    CHILD_21: TQRChildBand;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRReibank: TQRLabel;
    CHILD_22: TQRChildBand;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRAdvise: TQRLabel;
    qryListAVT_BANK: TStringField;
    CHILD_23: TQRChildBand;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRSND_INFO1: TQRLabel;
    qryListSND_INFO1: TStringField;
    qryListSND_INFO2: TStringField;
    qryListSND_INFO3: TStringField;
    qryListSND_INFO4: TStringField;
    qryListSND_INFO5: TStringField;
    qryListSND_INFO6: TStringField;
    QRSND_INFO2: TQRLabel;
    QRSND_INFO3: TQRLabel;
    QRSND_INFO4: TQRLabel;
    QRSND_INFO5: TQRLabel;
    QRSND_INFO6: TQRLabel;
    CHILD_24: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel123: TQRLabel;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRShape21: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure CHILD_01BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_02BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_03BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_04BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_05BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_06BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_07BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_08BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_09BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepAfterPrint(Sender: TObject);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CHILD_24BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    nDescIdx : integer;
    FDESC : TStringList;
    FMAINT_NO : string;
    sTitleLine : array [0..2] of String;
  public
    property MAINT_NO: String  read FMAINT_NO write FMAINT_NO;
  end;

var
  QR_ADV710_PRN_frm: TQR_ADV710_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_ADV710_PRN_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  QR_MAINT_NO.Caption := qryListMAINT_NO.AsString;
  TMP_STR := qryListAPP_DATE.AsString;
  insert('-', TMP_STR, 5);
  Insert('-', TMP_STR, 8);
  QR_APPDATE.Caption  := TMP_STR;
  QR_APP_NO.Caption := qryListAPP_NO.AsString;
  QR_APBANK.Caption := qryListSE_BANK.AsString;
  QR_APBANK_INFO1.Caption := Trim(qryListSE_BANK1.AsString+' '+qryListSE_BANK2.AsString);
  QR_APBANK_INFO2.Caption := Trim(qryListSE_BANK3.AsString+' '+qryListSE_BANK4.AsString);

  QR_ADBANK.Caption := qryListRE_BANK.AsString;
  TMP_STR := Trim(qryListRE_BANK1.AsString+' '+qryListRE_BANK2.AsString+' '+qryListRE_BANK3.AsString+' '+qryListRE_BANK4.AsString);
  QR_ADBANK_INFO.Caption := TMP_STR;
  QR_ADBANK_TEL.Caption := qryListRE_BANK5.AsString;

  QR_BENEFC1.Caption := qryListBENEFC1.AsString;
  TMP_STR := Trim( qryListBENEFC2.AsString+' '+qryListBENEFC3.AsString+' '+qryListBENEFC3.AsString+' '+qryListBENEFC4.AsString+' '+qryListBENEFC5.AsString);
  QR_BENEFC2.Caption := TMP_STR;
  QR_BENEFC_TEL.Caption := qryListBEN_TEL.AsString;

  QRMemo1.Lines.Text := qryListADDINFO_1.AsString;
  IF QRmemo1.Lines.Count > 1 Then
  begin
    QRmemo1.Height := QRMemo1.Lines.Count * 12;
    QRShape7.Top := QRMemo1.Top+QRMemo1.Height+3;
  end
  else
  begin
    QRmemo1.Height := 13;
    QRShape7.Top := 426;
  end;

  QRShape13.Height := QRShape7.Top - 163;
  QRShape14.Height := QRShape13.Height;
  QRShape15.Height := QRShape13.Height;

  TitleBand1.Height := QRShape7.Top+7;
end;

procedure TQR_ADV710_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FDESC := TStringList.Create;
  nDescIdx := 0;
  with qryList do
  begin
    Close;
    // MAINT_NO
    Parameters[0].Value := FMAINT_NO;
    Open;    
  end;

//------------------------------------------------------------------------------
// Memo ��¹�
//------------------------------------------------------------------------------
  FDESC.Clear;
  IF Trim(AnsiReplaceText(qryListDESGOOD_1.AsString,#13#10,'')) <> '' Then
  begin
    FDESC.Add('#DESC_GOODS');
    FDESC.Text := FDESC.Text+qryListDESGOOD_1.AsString;
//    FDESC.Add(qryListDESGOOD_1.AsString);
  end;
  IF Trim(AnsiReplaceText(qryListDOCREQU_1.AsString,#13#10,'')) <> '' Then
  begin
    FDESC.Add('#DOC_REQ');
    FDESC.Text := FDESC.Text+qryListDOCREQU_1.AsString;
//    FDESC.Add(qryListDOCREQU_1.AsString);
  end;
  IF Trim(AnsiReplaceText(qryListADDCOND_1.AsString,#13#10,'')) <> '' Then
  begin
    FDESC.Add('#ADD_COND');
    FDESC.Text := FDESC.Text+qryListADDCOND_1.AsString;
//    FDESC.Add(qryListADDCOND_1.AsString);
  end;
  IF Trim(AnsiReplaceText(qryListSPECIAL_DESC_1.AsString,#13#10,'')) <> '' Then
  begin
    FDESC.Add('#SPECIAL');
    FDESC.Text := FDESC.Text+qryListSPECIAL_DESC_1.AsString;
//    FDESC.Add(qryListSPECIAL_DESC_1.AsString);
  end;
  IF Trim(AnsiReplaceText(qryListINSTRCT_1.AsString,#13#10,'')) <> '' Then
  begin
    FDESC.Add('#INST');
    FDESC.Text := FDESC.Text+qryListINSTRCT_1.AsString;
//    FDESC.Add(qryListSPECIAL_DESC_1.AsString);
  end;
end;

procedure TQR_ADV710_PRN_frm.CHILD_01BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_DOCCD1.Caption := qryListDOC_CD1.AsString;
  QR_DOCCD2.Caption := qryListDOC_CD2.AsString;
  QR_SENDER_REF.Caption :=  qryListADV_NO.AsString;
  QR_LIC_NO.Caption := qryListLIC_NO.AsString;

  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_02BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_PRE_ADV.Caption := qryListREF_NO.AsString;
  PrintBand := Trim(QR_PRE_ADV.Caption) <> '';
  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_03BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_ISSDATE.Caption := qryListISS_DATE.AsString;
  QR_APPLICABLE.Caption := qryListAPPLICABLE_RULES_1.AsString;
  IF Trim(qryListAPPLICABLE_RULES_1.AsString) = '' Then
    QR_APPLICABLE.Caption := qryListAPPLICABLE_RULES_2.AsString;
  QR_EXDATE.Caption := Trim(qryListEX_DATE.AsString+' '+qryListEX_PLACE.AsString);
  if Trim(qryListISS_BANK.AsString+qryListISS_BANK1.AsString) <> '' then
  begin
    QRLabel34.Caption := '52a';
    QRLabel35.Caption := 'Issuing Bank';
    IF Trim(qryListISS_BANK.AsString) <> '' Then
      QR_ISSBANK.Caption := qryListISS_BANK.AsString
    else
    IF Trim(qryListISS_BANK1.AsString) <> '' Then
      QR_ISSBANK.Caption := qryListISS_BANK1.AsString;
  end
  else
  if Trim(qryListNon_Bank_Issuer1.AsString) <> '' then
  begin
    QRLabel34.Caption := '50B';
    QRLabel35.Caption := 'Non-Bank Issuer';
    QR_ISSBANK.Caption := qryListNon_Bank_Issuer1.AsString;
  end;
  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_04BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_APPBANK.Caption := Trim(qryListAPP_BANK1.AsString+' '+qryListAPP_BANK2.AsString);
  PrintBand := Trim(QR_APPBANK.Caption) <> '';
  Sender.Height := Sender.Tag;  
end;

procedure TQR_ADV710_PRN_frm.CHILD_05BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel43.Caption := Trim(qryListAPPLIC1.AsString+' '+qryListAPPLIC2.AsString);
  QRLabel44.Caption := Trim(qryListAPPLIC3.AsString+' '+qryListAPPLIC4.AsString);

  PrintBand := (QRLabel43.Caption <> '') OR (QRLabel44.Caption <> '');
  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_06BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRBen1.Caption := qryListBENEFC5.AsString;
  QRBen2.Caption := qryListBENEFC1.AsString;
  QRBen3.Caption := Trim(qryListBENEFC2.AsString+' '+qryListBENEFC3.AsString);
  QRBen4.Caption := qryListBENEFC4.AsString;

  QRAMT.Caption := qrylistCD_CUR.AsString+' '+FormatFloat('#,0.####',qryListCD_AMT.AsFloat);
  QRPCD.Caption := qryListCD_PERP.AsString+'/'+qryListCD_PERM.AsString;

  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_07BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRAACV1.Caption := Trim(qryListAA_CV1.AsString+' '+qryListAA_CV2.AsString);
  QRAACV2.Caption := Trim(qryListAA_CV3.AsString+' '+qryListAA_CV4.AsString);
  Sender.Height := Sender.Tag;

  PrintBand := (QRAACV1.Caption <> '') OR (QRAACV2.Caption <> '');   
end;

procedure TQR_ADV710_PRN_frm.CHILD_08BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRAV_AIL1.Caption := Trim(qryListAV_AIL.AsString);
  QRAV_AIL2.Caption := Trim(qryListAV_PAY.AsString);

  Sender.Height := Sender.Tag;
  PrintBand := (QRAV_AIL1.Caption <> '') OR (QRAV_AIL2.Caption <> '');
end;

procedure TQR_ADV710_PRN_frm.CHILD_09BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRDR_AWEE.Caption := Trim(qryListDR_AWEE.AsString);
  Sender.Height := Sender.Tag;
  PrintBand := QRDR_AWEE.Caption <> '';
end;

procedure TQR_ADV710_PRN_frm.CHILD_10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel69.Enabled := False;
  IF Trim(qryListDRAFT.AsString) <> '' Then
  begin
    QRLabel64.Caption := '42C';
    QRLabel65.Caption := 'Drafts at...';
    QRLabel67.Caption := qryListDRAFT.AsString;
    QRLabel68.Caption := Trim(qryListDRAFT1.AsString+' '+qryListDRAFT2.AsString);
  end
  else
  IF Trim(qryListMIX_PAY.AsString) <> '' then
  begin
    QRLabel64.Caption := '42M';
    QRLabel65.Caption := 'Mixed Payment Details';
    QRLabel67.Caption := Trim(qryListMIX_PAY.AsString+' '+qryListMIX_PAY1.AsString);
    QRLabel68.Caption := Trim(qryListMIX_PAY2.AsString+' '+qryListMIX_PAY3.AsString);
  end
  else
  if Trim(qryListDEF_PAY.AsString) <> '' Then
  begin
    QRLabel64.Caption := '42P';
    QRLabel65.Caption := 'Negotiation/Deferred Payment';
    QRLabel69.Caption := 'Details';
    QRLabel69.Enabled := True;
    QRLabel67.Caption := Trim(qryListDEF_PAY.AsString+' '+qryListDEF_PAY1.AsString);
    QRLabel68.Caption := Trim(qryListDEF_PAY2.AsString+' '+qryListDEF_PAY3.AsString);
  end;

  Sender.Height := Sender.Tag;
  PrintBand := (QRLabel67.Caption <> '') or (QRLabel68.Caption <> '');
end;

procedure TQR_ADV710_PRN_frm.CHILD_11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRPSHIP.Caption := Trim(qryListPSHIP.AsString);
  QRTSHIP.Caption := Trim(qryListTSHIP.AsString);
  Sender.Height := Sender.Tag;
end;

procedure TQR_ADV710_PRN_frm.CHILD_12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  sData : String;
begin
  IF Sender.Name = 'CHILD_12' Then
  begin
    sData := Trim(qryListLOAD_ON.AsString);
    QRLoadon.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_13' Then
  begin
    sData := Trim(qryListSUNJUCK_PORT.AsString);
    QRSUNJUK.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_14' Then
  begin
    sData := Trim(qryListDOCHACK_PORT.AsString);
    QRDOCHAK.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_15' Then
  begin
    sData := Trim(qryListFOR_TRAN.AsString);
    QRFORTRAN.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_17' Then
  begin
    sData := Trim(qryListCHARGE1.AsString);
    QRCharges1.Caption := Trim(sData+' '+qryListCHARGE2.AsString);
    QRCharges2.Caption := Trim(qryListCHARGE3.AsString+' '+qryListCHARGE4.AsString);
    QRCharges3.Caption := Trim(qryListCHARGE5.AsString+' '+qryListCHARGE5.AsString);
  end
  else
  IF Sender.Name = 'CHILD_18' Then
  begin
    sData := Trim(qryListPERIOD_DAYS.AsString);
    QRPeriodDays.Caption := sData;
    IF Trim(qryListPERIOD_TXT.AsString) <> '' Then
      QRPeriodDays.Caption := QRPeriodDays.Caption+'/'+Trim(qryListPERIOD_TXT.AsString);
  end
  else
  IF Sender.Name = 'CHILD_19' Then
  begin
    sData := Trim(qryListCONFIRMM.AsString);
    QRConfirm.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_20' Then
  begin
    sData := Trim(qryListCO_BANK.AsString);
    QRConfirmBank.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_21' Then
  begin
    sData := Trim(qryListREI_BANK.AsString);
    QRReibank.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_22' Then
  begin
    sData := Trim(qryListAVT_BANK.AsString);
    QRAdvise.Caption := sData;
  end
  else
  IF Sender.Name = 'CHILD_23' Then
  begin
    sData := Trim(qryListSND_INFO1.AsString);
    QRSND_INFO1.Caption := sData;
    QRSND_INFO2.Caption := qryListSND_INFO2.AsString;
    QRSND_INFO3.Caption := qryListSND_INFO3.AsString;
    QRSND_INFO4.Caption := qryListSND_INFO4.AsString;
    QRSND_INFO5.Caption := qryListSND_INFO5.AsString;
    QRSND_INFO6.Caption := qryListSND_INFO6.AsString;
  end;

  Sender.Height := Sender.Tag;
  PrintBand := sData <> '';
end;

procedure TQR_ADV710_PRN_frm.CHILD_16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  IF Trim(qryListLST_DATE.AsString) <> '' Then
  begin
    QRLabel83.Caption := '44C';
    QRLabel89.Caption := 'Latest Date of Shipment(YYMMDD)';
    QR_LSTDATE.Caption := RightStr(qryListLST_DATE.AsString, 6);
  end
  else
  IF Trim(qryListSHIP_PD1.AsString) <> '' Then
  begin
    QRLabel83.Caption := '44D';
    QRLabel89.Caption := 'Shipment Period';
    QR_LSTDATE.Caption := Trim(qryListSHIP_PD1.AsString+' '+
                          qryListSHIP_PD2.AsString+' '+
                          qryListSHIP_PD3.AsString+' '+
                          qryListSHIP_PD4.AsString+' '+
                          qryListSHIP_PD5.AsString+' '+
                          qryListSHIP_PD6.AsString);
  end;

  Sender.Height := Sender.Tag;
  PrintBand := Trim(QR_LSTDATE.Caption) <> '';
end;

procedure TQR_ADV710_PRN_frm.QuickRepAfterPrint(Sender: TObject);
begin
  IF Assigned(FDESC) Then
    FDESC.Free;
end;

procedure TQR_ADV710_PRN_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := nDescIdx <= FDESC.Count-1;
end;

procedure TQR_ADV710_PRN_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 13;
  IF LeftStr(FDESC.Strings[nDescIdx], 1) = '#' Then
  begin
    sTitleLine[0] := '';
    sTitleLine[1] := '';
    sTitleLine[2] := '';

    IF FDESC.Strings[nDescIdx] = '#DESC_GOODS' Then
    begin
      sTitleLine[0] := '45A';
      sTitleLine[1] := 'Description of Goods and/or';
      sTitleLine[2] := 'Services';
    end
    else
    IF FDESC.Strings[nDescIdx] = '#DOC_REQ' Then
    begin
      sTitleLine[0] := '46A';
      sTitleLine[1] := 'Documents Required';
    end
    else
    IF FDESC.Strings[nDescIdx] = '#ADD_COND' Then
    begin
      sTitleLine[0] := '47A';
      sTitleLine[1] := 'Additional Conditions';
    end
    else
    IF FDESC.Strings[nDescIdx] = '#SPECIAL' Then
    begin
      sTitleLine[0] := '49G';
      sTitleLine[1] := 'Special Payment Conditions for';
      sTitleLine[2] := 'Beneficiary';
    end
    else
    IF FDESC.Strings[nDescIdx] = '#INST' Then
    begin
      sTitleLine[0] := '78';
      sTitleLine[1] := 'Instructions to the';
      sTitleLine[2] := 'Paying/Accepting/Negotiating Bank';
    end;

    QRLabel98.Caption := sTitleLine[0];
    QRLabel98.Enabled := True;
    QRLabel99.Caption := sTitleLine[1];
    QRLabel99.Enabled := QRLabel98.Enabled;
    QRLabel100.Enabled := QRLabel98.Enabled;
    Inc(nDescIdx);
  end
  else
  begin
    QRLabel98.Enabled  := False;
    QRLabel99.Enabled  := False;    
    QRLabel100.Enabled := False;
    If sTitleLine[2] <> '' Then
    begin
      QRLabel99.Enabled := True;
      QRLabel99.Caption := sTitleLine[2];
      sTitleLine[2] := '';
    end

  end;
  QRLabel101.Caption := FDESC.Strings[nDescIdx];
  Inc(nDescIdx);
end;

procedure TQR_ADV710_PRN_frm.CHILD_24BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel135.Caption := qryListEX_NAME1.AsString;
  QRLabel136.Caption := qryListEX_NAME2.AsString;
  QRLabel137.Caption := qryListEX_NAME3.AsString;
  QRLabel138.Caption := Trim(qryListEX_ADDR1.AsString+' '+qryListEX_ADDR2.AsString);
end;

end.
