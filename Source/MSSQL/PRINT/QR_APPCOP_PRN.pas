unit QR_APPCOP_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TQR_APPCOP_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MS_NAME1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QR_MS_NAME2: TQRLabel;
    QR_MS_ADDR1: TQRLabel;
    QR_MS_ADDR2: TQRLabel;
    QR_MS_ADDR3: TQRLabel;
    QR_MS_TEL: TQRLabel;
    QR_AX_NAME3: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRLabel5: TQRLabel;
    QR_OAPP_DATE: TQRLabel;
    QRLabel6: TQRLabel;
    QRImage1: TQRImage;
    QRLabel8: TQRLabel;
    QRChildBand2: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_PAY_AMTC: TQRLabel;
    QRLabel10: TQRLabel;
    QRMemo1: TQRMemo;
    QRChildBand3: TQRChildBand;
    QRLabel13: TQRLabel;
    QRMemo2: TQRMemo;
    QRChildBand4: TQRChildBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRChildBand5: TQRChildBand;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRChildBand6: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRChildBand7: TQRChildBand;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRChildBand8: TQRChildBand;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRChildBand9: TQRChildBand;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCT_NO: TStringField;
    qryListAFY_NO: TStringField;
    qryListMS_CODE: TStringField;
    qryListMS_SAUP: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListMS_ADDR1: TStringField;
    qryListMS_ADDR2: TStringField;
    qryListMS_ADDR3: TStringField;
    qryListMS_TEL: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListAMT: TBCDField;
    qryListAMT_C: TStringField;
    qryListREMARK1: TMemoField;
    qryListGOODS_CD: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListHS_NO: TStringField;
    qryListBUS_G: TStringField;
    qryListBUS_D: TStringField;
    qryListOTRM_DATE: TStringField;
    qryListNTRM_DATE: TStringField;
    qryListOAPP_DATE: TStringField;
    qryListNAPP_DATE: TStringField;
    qryListOCFM_DATE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1_NM: TStringField;
    qryListBUS_G_NM: TStringField;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    FMAINT_NO: String;
  public
    property MAINT_NO : String read FMAINT_NO write FMAINT_NO;
  end;

var
  QR_APPCOP_PRN_frm: TQR_APPCOP_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_APPCOP_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  with qryList do
  begin
    Close;
    Parameters[0].Value := FMAINT_NO;
    Open;
  end;

  IF qryListMESSAGE1.AsString = '2BP' Then
    QRLabel1.Caption := '지 급 확 인 신 청 서'
  else
  IF qryListMESSAGE1.AsString = '2BP' Then
    QRLabel1.Caption := '지 급 변 경 확 인 신 청 서';

  QR_MS_NAME1.Caption := qryListMS_NAME1.AsString;
  QR_MS_NAME2.Caption := qryListMS_NAME2.AsString;
  QR_MS_ADDR1.Caption := qryListMS_ADDR1.AsString;
  QR_MS_ADDR2.Caption := qryListMS_ADDR2.AsString;
  QR_MS_ADDR3.Caption := qryListMS_ADDR3.AsString;
  QR_MS_TEL.Caption := qryListMS_TEL.AsString;
  QR_AX_NAME3.Caption := qryListAX_NAME3.AsString;

  QR_OAPP_DATE.Caption := LeftStr(qryListOAPP_DATE.AsString,4)+'-'+MidStr(qryListOAPP_DATE.AsString,5,2)+'-'+RightStr(qryListOAPP_DATE.AsString,2);
  QRLabel6.Caption := '변경신청일자:'+LeftStr(qryListNAPP_DATE.AsString,4)+'-'+MidStr(qryListNAPP_DATE.AsString,5,2)+'-'+RightStr(qryListNAPP_DATE.AsString,2);
  QR_PAY_AMTC.Caption := qryListAMT_C.AsString+' '+FormatFloat('#,0.####', qryListAMT.AsFloat);
  QRMemo1.Lines.Text := qryListREMARK1.AsString;
  if QRMemo1.Lines.Count = 0 Then
    QRChildBand2.Enabled := False
  else
  begin
    QRChildBand2.Enabled := True;
    QRMemo1.Height := (QRMemo1.Lines.Count * 14);
    QRChildBand2.Height := QRMemo1.Top + QRMemo1.Height + 4;
  end;

  QRMemo2.Lines.Clear;
  if Trim(qryListHS_NO.AsString) <> '' Then
    QRMemo2.Lines.Add(qryListHS_NO.AsString);
  if Trim(qryListGOODS_CD.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS_CD.AsString);
  if Trim(qryListGOODS1.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS1.AsString);
  if Trim(qryListGOODS2.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS2.AsString);
  if Trim(qryListGOODS3.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS3.AsString);
  if Trim(qryListGOODS4.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS4.AsString);
  if Trim(qryListGOODS5.AsString) <> '' Then QRMemo2.Lines.Add(qryListGOODS5.AsString);

  if QRMemo2.Lines.Count = 0 Then
    QRChildBand3.Enabled := False
  else
  begin
    QRChildBand3.Enabled := True;
    QRMemo2.Height := (QRMemo2.Lines.Count * 14);
    QRChildBand3.Height := QRMemo2.Top + QRMemo2.Height + 4;
  end;

  QRLabel12.Caption := qryListBUS_G_NM.AsString;

  QRLabel16.Caption := qryListSE_NAME1.AsString;
  QRLabel17.Caption := qryListSE_NAME2.AsString;
  QRLabel18.Caption := qryListSE_NAME3.AsString;

  QRLabel20.Caption := qryListCT_NO.AsString;

  QRLabel22.Caption := '당초기간:'+LeftStr(qryListOTRM_DATE.AsString,4)+'-'+MidStr(qryListOTRM_DATE.AsString,5,2)+'-'+RightStr(qryListOTRM_DATE.AsString,2);
  QRLabel23.Caption := '변경기간:'+LeftStr(qryListNTRM_DATE.AsString,4)+'-'+MidStr(qryListNTRM_DATE.AsString,5,2)+'-'+RightStr(qryListNTRM_DATE.AsString,2);
end;

end.
