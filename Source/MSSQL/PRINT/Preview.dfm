object Preview_frm: TPreview_frm
  Left = 651
  Top = 22
  Width = 892
  Height = 867
  ActiveControl = sSpinEdit1
  BorderIcons = [biMinimize, biMaximize]
  Caption = #52636#47141#47932' '#48120#47532#48372#44592
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object QRPreview1: TQRPreview
    Left = 0
    Top = 41
    Width = 876
    Height = 787
    HorzScrollBar.Tracking = True
    VertScrollBar.Tracking = True
    Align = alClient
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    OnClick = QRPreview1Click
    PageNumber = 1
    Zoom = 100
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    
    TabOrder = 1
    DesignSize = (
      876
      41)
    object sImage1: TsImage
      Left = 845
      Top = 8
      Width = 24
      Height = 24
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      AutoSize = True
      Picture.Data = {07544269746D617000000000}
      OnClick = sImage1Click
      ImageIndex = 34
      Images = DMICON.System26
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sImage2: TsImage
      Left = 79
      Top = 8
      Width = 24
      Height = 24
      Cursor = crHandPoint
      AutoSize = True
      Picture.Data = {07544269746D617000000000}
      Visible = False
      ImageIndex = 42
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sImage3: TsImage
      Left = 224
      Top = 8
      Width = 24
      Height = 24
      Cursor = crHandPoint
      AutoSize = True
      Picture.Data = {07544269746D617000000000}
      OnClick = sImage3Click
      ImageIndex = 33
      Images = DMICON.System26
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sSpeedButton1: TsSpeedButton
      Left = 206
      Top = 6
      Width = 14
      Height = 29
      ButtonStyle = tbsDivider
    end
    object sImage4: TsImage
      Left = 270
      Top = 8
      Width = 24
      Height = 24
      Cursor = crHandPoint
      Hint = #54253#47582#52644
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {07544269746D617000000000}
      ShowHint = True
      OnClick = sImage4Click
      ImageIndex = 35
      Images = DMICON.System26
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sImage5: TsImage
      Tag = 1
      Left = 298
      Top = 8
      Width = 24
      Height = 24
      Cursor = crHandPoint
      Hint = #54168#51060#51648#47582#52644
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {07544269746D617000000000}
      ShowHint = True
      OnClick = sImage4Click
      ImageIndex = 36
      Images = DMICON.System26
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sSpeedButton2: TsSpeedButton
      Left = 252
      Top = 6
      Width = 14
      Height = 29
      ButtonStyle = tbsDivider
    end
    object sEdit1: TsEdit
      Left = 34
      Top = 9
      Width = 28
      Height = 23
      TabOrder = 0
      Text = '0'
    end
    object sEdit2: TsEdit
      Left = 74
      Top = 9
      Width = 28
      Height = 23
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
      Text = '123'
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = '/'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 8
      Top = 9
      Width = 25
      Height = 23
      Caption = #9664
      TabOrder = 2
      OnClick = sButton1Click
    end
    object sButton2: TsButton
      Left = 103
      Top = 9
      Width = 25
      Height = 23
      Caption = #9654
      TabOrder = 3
      OnClick = sButton2Click
    end
    object sSpinEdit1: TsSpinEdit
      Left = 136
      Top = 9
      Width = 49
      Height = 23
      TabOrder = 4
      Text = '50'
      OnChange = sSpinEdit1Change
      Increment = 20
      MaxValue = 300
      MinValue = 50
      Value = 50
    end
    object sPanel2: TsPanel
      Left = 185
      Top = 9
      Width = 17
      Height = 23
      SkinData.SkinSection = 'TRANSPARENT'
      Caption = '%'
      
      TabOrder = 5
    end
  end
end
