unit LOCAMA_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB;

type
  TLOCAMA_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    QR_APP_DATE: TQRLabel;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    CHILD_BANK: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_ISSBANK: TQRLabel;
    QR_ISSBANK1: TQRLabel;
    QR_ISSBANK2: TQRLabel;
    CHILD_INFO: TQRChildBand;
    QRLabel2: TQRLabel;
    QR_AMD_DATE: TQRLabel;
    QRLabel4: TQRLabel;
    QR_ISS_DATE: TQRLabel;
    QRLabel6: TQRLabel;
    QR_LC_NO: TQRLabel;
    CHILD_APPLIC: TQRBand;
    QRLabel9: TQRLabel;
    QRM_APPLIC: TQRMemo;
    CHILD_BENEFC: TQRChildBand;
    QRLabel8: TQRLabel;
    QRM_BENEFC: TQRMemo;
    CHILD_INFO2: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_LOC_TYPENAME: TQRLabel;
    QRLabel13: TQRLabel;
    QR_LOC1AMT: TQRLabel;
    QRLabel15: TQRLabel;
    QR_CD_PER: TQRLabel;
    QRLabel17: TQRLabel;
    QR_EX_RATE: TQRLabel;
    CHILD_OFFERNO: TQRChildBand;
    QRLabel19: TQRLabel;
    QRM_OFFERNO: TQRMemo;
    CHILD_DELIVERY: TQRChildBand;
    CHILD_EXPIRY: TQRChildBand;
    CHILD_AMD_NO: TQRChildBand;
    CHILD_CHGINFO: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_DELIVERY: TQRLabel;
    QRLabel22: TQRLabel;
    QR_EXPIRY: TQRLabel;
    QRLabel24: TQRLabel;
    QR_AMD_NO: TQRLabel;
    CHILD_REMARK: TQRChildBand;
    QRLabel26: TQRLabel;
    QRM_CHGINFO: TQRMemo;
    QRLabel27: TQRLabel;
    QRM_REMARK: TQRMemo;
    CHILD_EXNAME: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel71: TQRLabel;
    QRLabel28: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    CIHLD_END: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel5: TQRLabel;
    QR_LOC2AMT: TQRLabel;
    QRLabel7: TQRLabel;
  private
    { Private declarations }
  public
    procedure PrintDocument(Fields: TFields; uPreview: Boolean = True); override;
  end;

var
  LOCAMA_PRINT_frm: TLOCAMA_PRINT_frm;

implementation

uses Commonlib;

{$R *.dfm}

{ TPRINT_PARENT_QR1 }

procedure TLOCAMA_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
begin
  inherited;
  FFields := Fields;
  FPreview := uPreview;

  with Fields do
  begin
    //문서번호
    QR_MAINT_NO.Caption := '전자문서번호 : '+FieldByName('MAINT_NO').AsString;
    //통지일자
    QR_APP_DATE.Caption := '조건변경 통지일자 : '+FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FieldByName('ADV_DATE').AsString));
//------------------------------------------------------------------------------
// CHILD_BANK
//------------------------------------------------------------------------------
    QR_ISSBANK.Caption  := getValueFromField('ISSBANK');
    QR_ISSBANK1.Caption := getValueFromField('ISSBANK1');
    QR_ISSBANK2.Caption := getValueFromField('ISSBANK2');
//------------------------------------------------------------------------------
// CHILD_INFO
//------------------------------------------------------------------------------
    QR_AMD_DATE.Caption := getValueFromField('AMD_DATE','YYYY-MM-DD');
    QR_ISS_DATE.Caption := getValueFromField('ISS_DATE','YYYY-MM-DD');
    QR_LC_NO.Caption := getValueFromField('LC_NO');
//------------------------------------------------------------------------------
// CHILD_APPLIC
//------------------------------------------------------------------------------
    QRM_APPLIC.Lines.Clear;
    getValueMemo(QRM_APPLIC,'APPLIC1');
    getValueMemo(QRM_APPLIC,'APPLIC2');
    getValueMemo(QRM_APPLIC,'APPLIC3');
    getValueMemo(QRM_APPLIC,'APPADDR1');
    getValueMemo(QRM_APPLIC,'APPADDR2');
    getValueMemo(QRM_APPLIC,'APPADDR3');
    QRM_APPLIC.Height := QRM_APPLIC.Lines.Count*12;
    CHILD_APPLIC.Height := QRM_APPLIC.Height+1;
//------------------------------------------------------------------------------
// CHILD_BENEFC
//------------------------------------------------------------------------------
    QRM_BENEFC.Lines.Clear;
    getValueMemo(QRM_BENEFC,'BENEFC1');
    getValueMemo(QRM_BENEFC,'BENEFC2');
    getValueMemo(QRM_BENEFC,'BENEFC3');
    getValueMemo(QRM_BENEFC,'BNFADDR1');
    getValueMemo(QRM_BENEFC,'BNFADDR2');
    getValueMemo(QRM_BENEFC,'BNFADDR3');
    getValueMemo(QRM_BENEFC,'BNFEMAILID');
    getValueMemo(QRM_BENEFC,'BNFDOMAIN');
    QRM_BENEFC.Height := QRM_BENEFC.Lines.Count*12;
    CHILD_BENEFC.Height := QRM_BENEFC.Height+1;
//------------------------------------------------------------------------------
// CHILD_INFO2
//------------------------------------------------------------------------------
    QR_LOC_TYPENAME.Caption := getValueFromField('LOC_TYPENAME');
    QR_LOC1AMT.Caption := getValueFromField('LOC1AMTC')+' '+getValueFromField('LOC1AMT','#,0.####');
    IF getValueFromField('LOC2AMTC') = '' Then
    begin
      QRLabel5.Enabled := False;
      QR_LOC2AMT.Enabled := False;

      QRLabel15.Top := QRLabel15.Top-14;
      QR_CD_PER.Top := QR_CD_PER.Top-14;
      QRLabel17.Top := QRLabel17.Top-14;
      QR_EX_RATE.Top := QR_EX_RATE.Top-14;
      CHILD_INFO2.Height := CHILD_INFO2.Height-14;
    end;
    QR_LOC2AMT.Caption := getValueFromField('LOC2AMTC')+' '+getValueFromField('LOC2AMT','#,0.####');
    QR_CD_PER.Caption := '(±) '+FieldByName('CD_PERP').AsString+' / '+FieldByName('CD_PERM').AsString+' (%)';
    QR_EX_RATE.Caption := getValueFromField('EX_RATE','#,0.####');
//------------------------------------------------------------------------------
// CHILD_OFFERNO
//------------------------------------------------------------------------------
    QRM_OFFERNO.Lines.Clear;
    getValueMemo(QRM_OFFERNO,'OFFERNO1');
    getValueMemo(QRM_OFFERNO,'OFFERNO2');
    getValueMemo(QRM_OFFERNO,'OFFERNO3');
    getValueMemo(QRM_OFFERNO,'OFFERNO4');
    getValueMemo(QRM_OFFERNO,'OFFERNO5');
    getValueMemo(QRM_OFFERNO,'OFFERNO6');
    getValueMemo(QRM_OFFERNO,'OFFERNO7');
    getValueMemo(QRM_OFFERNO,'OFFERNO8');
    getValueMemo(QRM_OFFERNO,'OFFERNO9');
    QRM_OFFERNO.Height := QRM_OFFERNO.Lines.Count*12;
    CHILD_OFFERNO.Height := QRM_OFFERNO.Height+1;
//------------------------------------------------------------------------------
// CHILD_DELIVERY
//------------------------------------------------------------------------------
    CHILD_DELIVERY.Enabled := Trim(FieldByName('DELIVERY').AsString) <> '';
    QR_DELIVERY.Caption := getValueFromField('DELIVERY','YYYY-MM-DD');
//------------------------------------------------------------------------------
// CHILD_EXPIRY
//------------------------------------------------------------------------------
    CHILD_EXPIRY.Enabled := Trim(FieldByName('EXPIRY').AsString) <> '';
    QR_EXPIRY.Caption := getValueFromField('EXPIRY','YYYY-MM-DD');
//------------------------------------------------------------------------------
// CHILD_AMD_NO
//------------------------------------------------------------------------------
    QR_AMD_NO.Caption := FieldByName('AMD_NO').AsString;
//------------------------------------------------------------------------------
// CHILD_CHGINFO
//------------------------------------------------------------------------------
    QRM_CHGINFO.Lines.Clear;
    getValueMemo(QRM_CHGINFO,'CHGINFO1');
    QRM_CHGINFO.Height := QRM_CHGINFO.Lines.Count*12;
    CHILD_CHGINFO.Height := QRM_CHGINFO.Height+1;
//------------------------------------------------------------------------------
// CHILD_REMARK
//------------------------------------------------------------------------------
    QRM_REMARK.Lines.Clear;
    getValueMemo(QRM_REMARK,'REMARK1');
    QRM_REMARK.Height := QRM_REMARK.Lines.Count*12;
    CHILD_REMARK.Height := QRM_REMARK.Height+1;
//------------------------------------------------------------------------------
// CHILD_EXNAME
//------------------------------------------------------------------------------
    QR_EXNAME1.Caption := getValueFromField('EXNAME1');
    QR_EXNAME2.Caption := getValueFromField('EXNAME2');
    QR_EXNAME3.Caption := getValueFromField('EXNAME3');
  end;

//  RunPrint;

end;

end.
