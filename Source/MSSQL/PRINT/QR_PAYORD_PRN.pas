unit QR_PAYORD_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TQR_PAYORD_PRN_New = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QR_1_COMMON: TQRChildBand;
    QRLabel5: TQRLabel;
    QRImage1: TQRImage;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QR_BF_CODE: TQRLabel;
    QR_BUS_CD: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QR_PAY_AMT: TQRLabel;
    QR_PAY_AMT2: TQRLabel;
    QR_CHARGE_TO: TQRLabel;
    QR_2_DETAIL: TQRChildBand;
    QRLabel12: TQRLabel;
    QRImage2: TQRImage;
    QRLabel16: TQRLabel;
    QR_APP_NAME1: TQRLabel;
    QR_APP_NAME2: TQRLabel;
    QR_APP_NAME3: TQRLabel;
    QR_APP_STR1: TQRLabel;
    QR_APP_TELE: TQRLabel;
    QRLabel6: TQRLabel;
    QR_BEN_NAME1: TQRLabel;
    QR_BEN_NAME2: TQRLabel;
    QR_BEN_NAME3: TQRLabel;
    QR_BEN_STR1: TQRLabel;
    QR_BEN_STR2: TQRLabel;
    QR_BEN_STR3: TQRLabel;
    QR_BEN_TELE: TQRLabel;
    QRLabel13: TQRLabel;
    QR_OD_BANK: TQRLabel;
    QR_OD_BANK1: TQRLabel;
    QR_OD_NAME1: TQRLabel;
    QR_OD_ACCNT1: TQRLabel;
    QR_3_KRWBANK: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_EC_BANK: TQRLabel;
    QR_EC_BANK1: TQRLabel;
    QR_EC_NAME1: TQRLabel;
    QR_EC_ACCNT1: TQRLabel;
    QR_4_BEN_BANK: TQRChildBand;
    QRLabel22: TQRLabel;
    QR_BN_BANK: TQRLabel;
    QR_BN_BANK1: TQRLabel;
    QR_BN_NAME1: TQRLabel;
    QR_BN_ACCNT1: TQRLabel;
    QR_5_IT_BANK: TQRChildBand;
    QRLabel23: TQRLabel;
    QR_IT_BANK: TQRLabel;
    QR_IT_BANK1: TQRLabel;
    QR_IT_NAME1: TQRLabel;
    QR_IT_ACCNT1: TQRLabel;
    QR_6_DOC: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_DOC_GU: TQRLabel;
    QR_DOC_NO1: TQRLabel;
    QR_DOC_DTE1: TQRLabel;
    QR_7_SND: TQRChildBand;
    QRLabel18: TQRLabel;
    QR_SND_CD: TQRLabel;
    QR_PAYDET1: TQRLabel;
    QR_PAYDET2: TQRLabel;
    QRLabel25: TQRLabel;
    QR_PAYDET3: TQRLabel;
    QR_PAYDET4: TQRLabel;
    QR_8_ETC: TQRChildBand;
    QRLabel19: TQRLabel;
    QR_REMARK1: TQRLabel;
    QR_REMARK2: TQRLabel;
    QR_REMARK3: TQRLabel;
    QR_REMARK4: TQRLabel;
    QR_REMARK5: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_HS_CODE: TQRLabel;
    QR_ADREFNO: TQRLabel;
    QR_BUS_CD1: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRChildBand2: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QR_SND_NAME1: TQRLabel;
    QR_SND_NAME2: TQRLabel;
    QR_SND_NAME3: TQRLabel;
    QRChildBand3: TQRChildBand;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_DATE: TStringField;
    qryListEXE_DATE: TStringField;
    qryListIMP_DATE: TStringField;
    qryListHS_CODE: TStringField;
    qryListADREFNO: TStringField;
    qryListBUS_CD: TStringField;
    qryListBUS_CD1: TStringField;
    qryListSND_CD: TStringField;
    qryListCHARGE_TO: TStringField;
    qryListPAY_AMT: TBCDField;
    qryListPAY_AMTC: TStringField;
    qryListOD_BANK: TStringField;
    qryListOD_BANK1: TStringField;
    qryListOD_BANK2: TStringField;
    qryListOD_NAME1: TStringField;
    qryListOD_NAME2: TStringField;
    qryListOD_ACCNT1: TStringField;
    qryListOD_ACCNT2: TStringField;
    qryListOD_CURR: TStringField;
    qryListOD_NATION: TStringField;
    qryListBN_BANK1: TStringField;
    qryListBN_BANK2: TStringField;
    qryListBN_NAME1: TStringField;
    qryListBN_NAME2: TStringField;
    qryListBN_ACCNT1: TStringField;
    qryListBN_ACCNT2: TStringField;
    qryListBN_CURR: TStringField;
    qryListBN_NATION: TStringField;
    qryListIT_ACCNT: TStringField;
    qryListIT_PCOD4: TStringField;
    qryListIT_BANK: TStringField;
    qryListIT_BANK1: TStringField;
    qryListIT_BANK2: TStringField;
    qryListIT_NAME1: TStringField;
    qryListIT_NAME2: TStringField;
    qryListIT_ACCNT1: TStringField;
    qryListIT_ACCNT2: TStringField;
    qryListIT_CURR: TStringField;
    qryListIT_NATION: TStringField;
    qryListEC_BANK: TStringField;
    qryListEC_BANK1: TStringField;
    qryListEC_BANK2: TStringField;
    qryListEC_NAME1: TStringField;
    qryListEC_NAME2: TStringField;
    qryListEC_ACCNT1: TStringField;
    qryListEC_ACCNT2: TStringField;
    qryListEC_NATION: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_TELE: TStringField;
    qryListAPP_STR1: TStringField;
    qryListBEN_CODE: TStringField;
    qryListBEN_NAME1: TStringField;
    qryListBEN_NAME2: TStringField;
    qryListBEN_NAME3: TStringField;
    qryListBEN_TELE: TStringField;
    qryListBEN_STR1: TStringField;
    qryListBEN_STR2: TStringField;
    qryListBEN_STR3: TStringField;
    qryListBEN_NATION: TStringField;
    qryListSND_CODE: TStringField;
    qryListSND_NAME1: TStringField;
    qryListSND_NAME2: TStringField;
    qryListSND_NAME3: TStringField;
    qryListREMARK: TBooleanField;
    qryListREMARK1: TStringField;
    qryListREMARK2: TStringField;
    qryListREMARK3: TStringField;
    qryListREMARK4: TStringField;
    qryListREMARK5: TStringField;
    qryListDOC_GU: TStringField;
    qryListDOC_NO1: TStringField;
    qryListDOC_DTE1: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPAY_AMT2: TBCDField;
    qryListPAY_AMT2C: TStringField;
    qryListBF_CODE: TStringField;
    qryListPAYDET1: TStringField;
    qryListPAYDET2: TStringField;
    qryListPAYDET3: TStringField;
    qryListPAYDET4: TStringField;
    qryListPAYDET5: TStringField;
    qryListBN_BANK: TStringField;
    qryListBF_CODE_NM: TStringField;
    qryListBUS_CD_NM: TStringField;
    qryListBUS_CD1_NM: TStringField;
    qryListSND_CD_NM: TStringField;
    qryListCHARGE_TO_NM: TStringField;
    qryListDOC_GU_NM: TStringField;
    QR_BN_BANK2: TQRLabel;
    QR_BN_NAME2: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QR_IT_BANK2: TQRLabel;
    QR_IT_NAME2: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    FMAINT_NO: String;
  public
    property MAINT_NO : String read FMAINT_NO write FMAINT_NO;
  end;

var
  QR_PAYORD_PRN_New: TQR_PAYORD_PRN_New;

implementation

uses
  MSSQL;

{$R *.DFM}

procedure TQR_PAYORD_PRN_New.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  with qryList do
  begin
    Close;
    Parameters[0].Value := FMAINT_NO;
    Open;
  end;

  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  QRLabel2.Caption := '신청일자:'+LeftStr(qryListAPP_DATE.AsString,4)+'-'+MidStr(qryListAPP_DATE.AsString,5,2)+'-'+RightStr(qryListAPP_DATE.AsString,2);
  QRLabel4.Caption := '수입예정일:'+LeftStr(qryListIMP_DATE.AsString,4)+'-'+MidStr(qryListIMP_DATE.AsString,5,2)+'-'+RightStr(qryListIMP_DATE.AsString,2);

  QR_BF_CODE.Caption := Format('%s  (%s)',[qryListBF_CODE.AsString,qryListBF_CODE_NM.AsString]);
  QR_BUS_CD.Caption := Trim(qryListBUS_CD_NM.AsString);
  QR_PAY_AMT.Caption := UpperCase(Trim(qryListPAY_AMTC.AsString))+' '+FormatFloat('#,0.###', qryListPAY_AMT.AsFloat);
  QR_PAY_AMT2.Caption := UpperCase(Trim(qryListPAY_AMT2C.AsString))+' '+FormatFloat('#,0.###', qryListPAY_AMT2.AsFloat);
  QR_CHARGE_TO.Caption := qryListCHARGE_TO_NM.AsString;

  QR_APP_NAME1.Caption := qryListAPP_NAME1.AsString;
  QR_APP_NAME2.Caption := qryListAPP_NAME2.AsString;
  QR_APP_NAME3.Caption := qryListAPP_NAME3.AsString;
  QR_APP_STR1.Caption :=  qryListAPP_STR1.AsString;
  QR_APP_TELE.Caption := '[전화번호] '+qryListAPP_TELE.AsString;

  QR_BEN_NAME1.Caption := qryListBEN_NAME1.AsString;
  QR_BEN_NAME2.Caption := qryListBEN_NAME2.AsString;
  QR_BEN_NAME3.Caption := qryListBEN_NAME3.AsString;
  QR_BEN_STR1.Caption := qryListBEN_STR1.AsString;
  QR_BEN_STR2.Caption := qryListBEN_STR2.AsString;
  QR_BEN_STR3.Caption := qryListBEN_STR3.AsString;
  QR_BEN_TELE.Caption := '[전화번호] '+qryListBEN_TELE.AsString;

  QR_OD_BANK.Caption := qryListOD_BANK.AsString;
  QR_OD_BANK1.Caption := Trim(qryListOD_BANK1.AsString+' '+qryListOD_BANK2.AsString);
  QR_OD_NAME1.Caption := Trim(qryListOD_NAME1.AsString+' '+qryListOD_NAME2.AsString);
  QR_OD_ACCNT1.Caption := '[계좌주/계좌번호] '+Trim(qryListOD_ACCNT2.AsString+' '+qryListOD_ACCNT1.AsString);

  QR_3_KRWBANK.Enabled := False;
  IF Trim(qryListEC_BANK.AsString) <> '' then
  begin
    QR_3_KRWBANK.Enabled := True;
    QR_3_KRWBANK.Height := QR_3_KRWBANK.Tag;
    QR_EC_BANK.Caption := qryListEC_BANK.AsString;
    QR_EC_BANK1.Caption := Trim(qryListEC_BANK1.AsString+' '+qryListEC_BANK2.AsString);
    QR_EC_NAME1.Caption := Trim(qryListEC_NAME1.AsString+' '+qryListEC_NAME2.AsString);
    QR_EC_ACCNT1.Caption := '[계좌주/계좌번호] '+Trim(qryListEC_ACCNT2.AsString+' '+qryListEC_ACCNT1.AsString);
  end;

  QR_4_BEN_BANK.Enabled := False;
  IF Trim(qryListBN_BANK.AsString) <> '' Then
  begin
    QR_4_BEN_BANK.Enabled := True;
    QR_4_BEN_BANK.Height := QR_4_BEN_BANK.Tag;
    QR_BN_BANK.Caption := qryListBN_BANK.AsString;
    QR_BN_BANK1.Caption := Trim(qryListBN_BANK1.AsString);
    QR_BN_BANK2.Caption := Trim(qryListBN_BANK2.AsString);
    QR_BN_NAME1.Caption := Trim(qryListBN_NAME1.AsString);
    QR_BN_NAME2.Caption := Trim(qryListBN_NAME2.AsString);
    QR_BN_ACCNT1.Caption := '[계좌주/계좌번호] '+Trim(qryListBN_ACCNT2.AsString+' '+qryListBN_ACCNT1.AsString);
  end;

  QR_5_IT_BANK.Enabled := False;
  IF Trim(qryListIT_BANK.AsString) <> '' Then
  begin
    QR_5_IT_BANK.Enabled := True;
    QR_5_IT_BANK.Height := QR_5_IT_BANK.Tag;
    QR_IT_BANK.Caption := qryListIT_BANK.AsString;
    QR_IT_BANK1.Caption := Trim(qryListIT_BANK1.AsString);
    QR_IT_BANK2.Caption := Trim(qryListIT_BANK2.AsString);
    QR_IT_NAME1.Caption := Trim(qryListIT_NAME1.AsString);
    QR_IT_NAME2.Caption := Trim(qryListIT_NAME2.AsString);
    QR_IT_ACCNT1.Caption := '[계좌주/계좌번호] '+Trim(qryListIT_ACCNT2.AsString+' '+qryListIT_ACCNT1.AsString);
  end;

  QR_6_DOC.Enabled := False;
//  IF AnsiMatchText('',[Trim(qryListDOC_GU.AsString),Trim(qryListDOC_NO1.AsString),Trim(qryListDOC_DTE1.AsString)]) Then
  if (Trim(qryListDOC_GU.AsString) <> '') OR
     (Trim(qryListDOC_NO1.AsString) <> '') OR
     (Trim(qryListDOC_DTE1.AsString) <> '') Then
  begin
    QR_6_DOC.Enabled := True;
    QR_6_DOC.Height := QR_6_DOC.Tag;
    IF Trim(qryListDOC_GU.AsString) <> '' then
      QR_DOC_GU.Caption := Trim(qryListDOC_GU.AsString)+' ('+qryListDOC_GU_NM.AsString+')'
    else
      QR_DOC_GU.Caption := ' ';
    QR_DOC_NO1.Caption := '[문서번호] '+Trim(qryListDOC_NO1.AsString);
    QR_DOC_DTE1.Caption := '[발행일자] '+Trim(qryListDOC_DTE1.AsString);
  end;

  QR_7_SND.Enabled := False;
  if (Trim(qryListSND_CD.AsString) <> '') OR
     (Trim(qryListPAYDET1.AsString) <> '') OR
     (Trim(qryListPAYDET2.AsString) <> '') OR
     (Trim(qryListPAYDET3.AsString) <> '') OR
     (Trim(qryListPAYDET4.AsString) <> '') Then
  begin
    QR_7_SND.Enabled := True;

    QR_SND_CD.Caption := qryListSND_CD_NM.AsString;

    QR_PAYDET1.Caption := qryListPAYDET1.AsString;
    QR_PAYDET2.Caption := qryListPAYDET2.AsString;
    QR_PAYDET3.Caption := qryListPAYDET3.AsString;
    QR_PAYDET4.Caption := qryListPAYDET4.AsString;
    
    IF Trim(qryListPAYDET4.AsString) = '' Then
    begin
      QR_7_SND.Height := QR_PAYDET4.Top;
      IF Trim(qryListPAYDET3.AsString) = '' Then
      begin
        QR_7_SND.Height := QR_PAYDET3.Top;
        IF Trim(qryListPAYDET2.AsString) = '' Then
        begin
          QR_7_SND.Height := QR_PAYDET2.Top;
          IF Trim(qryListPAYDET1.AsString) = '' Then
          begin
            QR_7_SND.Height := QR_PAYDET1.Top;
          end;
        end;
      end;
    end;
  end;


  QR_REMARK1.Caption := qryListREMARK1.AsString;
  QR_REMARK2.Caption := qryListREMARK2.AsString;
  QR_REMARK3.Caption := qryListREMARK3.AsString;
  QR_REMARK4.Caption := qryListREMARK4.AsString;
  QR_REMARK5.Caption := qryListREMARK5.AsString;

  IF Trim(qryListREMARK5.AsString) = '' Then
  begin
    QR_8_ETC.Height := QR_REMARK5.Top;
    IF Trim(qryListREMARK4.AsString) = '' Then
    begin
      QR_8_ETC.Height := QR_REMARK4.Top;
      IF Trim(qryListREMARK3.AsString) = '' Then
      begin
        QR_8_ETC.Height := QR_REMARK3.Top;
        IF Trim(qryListREMARK2.AsString) = '' Then
        begin
          QR_8_ETC.Height := QR_REMARK2.Top;
          IF Trim(qryListREMARK1.AsString) = '' Then
          begin
            QR_8_ETC.Enabled := False;
          end;
        end;
      end;
    end;
  end;

  QR_HS_CODE.Caption := qryListHS_CODE.AsString;
  QR_ADREFNO.Caption := qryListADREFNO.AsString;
  QR_BUS_CD1.Caption := qryListBUS_CD1_NM.AsString;

  QR_SND_NAME1.Caption := qryListSND_NAME1.AsString;
  QR_SND_NAME2.Caption := qryListSND_NAME2.AsString;
  QR_SND_NAME3.Caption := qryListSND_NAME3.AsString;
end;

end.
