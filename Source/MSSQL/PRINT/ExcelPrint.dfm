object ExcelPrint_frm: TExcelPrint_frm
  Left = 652
  Top = 334
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #50641#49472#52636#47141
  ClientHeight = 74
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 74
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sLabel1: TsLabel
      Left = 171
      Top = 29
      Width = 24
      Height = 15
      Caption = #48512#53552
    end
    object sLabel2: TsLabel
      Left = 299
      Top = 29
      Width = 24
      Height = 15
      Caption = #44620#51648
    end
    object sDateEdit1: TsDateEdit
      Left = 72
      Top = 24
      Width = 97
      Height = 25
      AutoSize = False
      Color = clWhite
      EditMask = '!9999/99/99;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '2015-04-24'
      BoundLabel.Active = True
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      Date = 42118
    end
    object sDateEdit2: TsDateEdit
      Left = 200
      Top = 24
      Width = 97
      Height = 25
      AutoSize = False
      Color = clWhite
      EditMask = '!9999/99/99;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '2015-04-24'
      BoundLabel.Caption = 'sDateEdit2'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      Date = 42118
    end
    object sButton1: TsButton
      Left = 336
      Top = 24
      Width = 145
      Height = 25
      Caption = #50641#49472#52636#47141
      TabOrder = 2
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
