unit LOCAPP_PRINT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, ADODB, DB, Dialogs;

type
  TLOCAPP_PRINT_frm = class(TQuickRep)
    PageFooterBand1: TQRBand;
    QRImage2: TQRImage;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QR_SIGN1: TQRLabel;
    QR_SIGN2: TQRLabel;
    QR_SIGN3: TQRLabel;
    QRShape2: TQRShape;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    Band_Title: TQRBand;
    CHILD_SELL: TQRChildBand;
    CHILD_OPENDATA: TQRChildBand;
    CHILD_GOODS: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_Create1: TQRLabel;
    QR_Create2: TQRLabel;
    QR_Create3: TQRLabel;
    QR_Create4: TQRLabel;
    QRLabel26: TQRLabel;
    QR_RECV1: TQRLabel;
    QR_RECV2: TQRLabel;
    QR_RECV3: TQRLabel;
    QR_RECV4: TQRLabel;
    QR_RECV5: TQRLabel;
    QRLabel23: TQRLabel;
    QR_ID: TQRLabel;
    QRLabel25: TQRLabel;
    QR_ID_DETAIL: TQRLabel;
    QRLabel24: TQRLabel;
    QR_BANK1: TQRLabel;
    QR_BANK2: TQRLabel;
    QR_BANK3: TQRLabel;
    QRLabel27: TQRLabel;
    QR_LOC_TYPE: TQRLabel;
    QRLabel28: TQRLabel;
    QR_LOC_AMT: TQRLabel;
    QRLabel29: TQRLabel;
    QR_CD_PER: TQRLabel;
    QRLabel31: TQRLabel;
    QR_BUSINESS: TQRLabel;
    QRLabel12: TQRLabel;
    QR_DOC_SELL1: TQRLabel;
    QR_DOC_SELL2: TQRLabel;
    QR_DOC_SELL3: TQRLabel;
    QR_DOC_SELL4: TQRLabel;
    QR_DOC_SELL5: TQRLabel;
    QR_DOC_SELL6: TQRLabel;
    QR_DOC_SELL7: TQRLabel;
    QR_DOC_SELL8: TQRLabel;
    QR_DOC_SELL9: TQRLabel;
    QRLabel13: TQRLabel;
    QR_OPEN_COUNT: TQRLabel;
    QRLabel30: TQRLabel;
    QR_DOC_PRD: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    QRLabel15: TQRLabel;
    QR_DELIVERY: TQRLabel;
    QRLabel16: TQRLabel;
    QR_EXPIRY: TQRLabel;
    QRLabel17: TQRLabel;
    QR_TRANSPRT: TQRLabel;
    QRLabel14: TQRLabel;
    QR_HS: TQRLabel;
    QRM_GOODS: TQRMemo;
    CHILD_ATTACH: TQRChildBand;
    QRLabel18: TQRLabel;
    QR_ATTACH1: TQRLabel;
    QR_ATTACH2: TQRLabel;
    QR_ATTACH3: TQRLabel;
    QR_ATTACH4: TQRLabel;
    CHILD_ETC_ATTACH: TQRChildBand;
    QRLabel19: TQRLabel;
    QRM_DOC_ETC1: TQRMemo;
    CHILD_REMARK: TQRChildBand;
    QRLabel20: TQRLabel;
    QRM_REMARK1: TQRMemo;
    CHILD_EXPORT1: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QR_DOC_DTL: TQRLabel;
    QRLabel32: TQRLabel;
    QR_DOC_NO: TQRLabel;
    QRLabel33: TQRLabel;
    QR_DOC_AMT: TQRLabel;
    QRLabel34: TQRLabel;
    QR_LOADDATE: TQRLabel;
    QRLabel35: TQRLabel;
    QR_EXPDATE: TQRLabel;
    ChildBand2: TQRChildBand;
    CHILD_EXPORT2: TQRChildBand;
    QRLabel36: TQRLabel;
    QR_EXPORT1: TQRLabel;
    QR_EXPORT2: TQRLabel;
    QR_EXPORT3: TQRLabel;
    CHILD_EXPORT3: TQRChildBand;
    QRLabel37: TQRLabel;
    QR_DEST: TQRLabel;
    QRLabel38: TQRLabel;
    QR_ISBANK1: TQRLabel;
    QR_ISBANK2: TQRLabel;
    QRLabel39: TQRLabel;
    QR_PAYMENT: TQRLabel;
    CHILD_EXPORT4: TQRChildBand;
    QRLabel40: TQRLabel;
    QRM_EXGOOD1: TQRMemo;
    QRLabel41: TQRLabel;
    procedure PageFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  public
    procedure PrintDocument(Fields : TFields ;uPreview : Boolean = False);
    procedure ReadOnlyDocument(Fields : TFields);

  end;

var
  LOCAPP_PRINT_frm: TLOCAPP_PRINT_frm;

implementation

uses MSSQL, Commonlib;

{$R *.DFM}



{ TLOCAPP_PRINT_frm }
Const
  RecordHeight : Integer = 16;
procedure TLOCAPP_PRINT_frm.PrintDocument(Fields: TFields; uPreview: Boolean);
var
  i, nIndex : Integer;
  MSG : String;
begin
  ReadOnlyDocument(Fields);

  Self.Prepare;
  IF uPreview Then Self.Preview
  else
  begin
    Self.PrinterSetup;
//------------------------------------------------------------------------------
// 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
//------------------------------------------------------------------------------
    IF Self.Tag = 0 Then Self.Print;
  end;

end;

procedure TLOCAPP_PRINT_frm.PageFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PageFooterBand1.AlignToBottom := Self.PageNumber = 1;
end;

procedure TLOCAPP_PRINT_frm.ReadOnlyDocument(Fields: TFields);
var
  i, nIndex : Integer;
  MSG : String;
begin
  with Fields do
  begin
//------------------------------------------------------------------------------
// TITLE SECTION
//------------------------------------------------------------------------------
    Band_Title.Height := Band_Title.Tag;
    //------------------------------------------------------------------------------
    // 문서번호
    //------------------------------------------------------------------------------
    QRLabel8.Caption := '전자문서번호 : '+FieldByName('MAINT_NO').AsString;
    //------------------------------------------------------------------------------
    // 개설신청일자
    //------------------------------------------------------------------------------
    QRLabel9.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(FieldByName('APP_DATE').AsString));
    //------------------------------------------------------------------------------
    // 개설의뢰인
    //------------------------------------------------------------------------------
    QR_Create1.Caption := FieldByName('APPLIC1').AsString;
    QR_Create2.Caption := FieldByName('APPLIC2').AsString;
    QR_Create3.Caption := FieldByName('APPLIC3').AsString;
    QR_Create4.Caption := FieldByName('APPADDR1').AsString + FieldByName('APPADDR2').AsString;
    //------------------------------------------------------------------------------
    // 수혜자
    //------------------------------------------------------------------------------
    QR_RECV1.Caption := FieldByName('BENEFC1').AsString;
    QR_RECV2.Caption := FieldByName('BENEFC2').AsString;
    QR_RECV3.Caption := FieldByName('BENEFC3').AsString;
    QR_RECV4.Caption := FieldByName('BNFADDR1').AsString;
    QR_RECV5.Caption := FieldByName('BNFADDR2').AsString;
    //------------------------------------------------------------------------------
    // 수발신인식별자
    //------------------------------------------------------------------------------
    QR_ID.Caption := FieldByName('CHKNAME1').AsString;
    QR_ID_DETAIL.Caption := FieldByName('CHKNAME2').AsString;
    //------------------------------------------------------------------------------
    // 개설은행
    //------------------------------------------------------------------------------
    QR_BANK1.Caption := FieldByName('AP_BANK').AsString;
    QR_BANK2.Caption := FieldByName('AP_BANK1').AsString;
    QR_BANK3.Caption := FieldByName('AP_BANK2').AsString;
    //------------------------------------------------------------------------------
    // 내국신용장 종류
    //------------------------------------------------------------------------------
    QR_LOC_TYPE.Caption := '['+FieldByName('LOC_TYPE').AsString+'] '+FieldByName('LOC_TYPENAME').AsString;
    //------------------------------------------------------------------------------
    // 개설외화(원화)금액
    //------------------------------------------------------------------------------
    QR_LOC_AMT.Caption := FieldByName('LOC_AMTC').AsString +'  '+ FormatFloat('#,0.####',FieldByName('LOC_AMT').AsFloat);
    //------------------------------------------------------------------------------
    // 허용오차
    //------------------------------------------------------------------------------
    QR_CD_PER.Caption := '('+'±'+') '+FieldByName('CD_PERP').AsString+' / '+FieldByName('CD_PERM').AsString+' (%)';
    //------------------------------------------------------------------------------
    // 개설근거별 용도
    //------------------------------------------------------------------------------
    QR_BUSINESS.Caption := '['+FieldByName('BUSINESS').AsString+'] '+FieldByName('BUSINESSNAME').AsString;

//------------------------------------------------------------------------------
// SELL DOC SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 물품매도확약서번호
    //------------------------------------------------------------------------------
    nIndex := 1;
    for i := 1 to 9 do
    begin
      if Trim(FieldByName('OFFERNO'+IntToStr(i)).AsString) = '' Then Continue;

      (FindComponent('QR_DOC_SELL'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('OFFERNO'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    CHILD_SELL.Height := ((nIndex-1)*RecordHeight)+1;
//------------------------------------------------------------------------------
// OPENDATA SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 개설회차
    //------------------------------------------------------------------------------
    QR_OPEN_COUNT.Caption := FieldByName('OPEN_NO').AsString+' 차 내국 신용장';
    //------------------------------------------------------------------------------
    // 서류제시기간
    //------------------------------------------------------------------------------
    QR_DOC_PRD.Caption := '물품수령증명서 발급일로부터  '+FieldByName('DOC_PRD').AsString+' 영업일 이내';
    //------------------------------------------------------------------------------
    // 물품인도기일
    //------------------------------------------------------------------------------
    QR_DELIVERY.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(FieldByName('DELIVERY').AsString));
    //------------------------------------------------------------------------------
    // 유효기일
    //------------------------------------------------------------------------------
    QR_EXPIRY.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(FieldByName('EXPIRY').AsString));
    //------------------------------------------------------------------------------
    // 분할인도허용여부
    //------------------------------------------------------------------------------
    QR_TRANSPRT.Caption := FieldByName('TRANSPRTNAME').AsString;
//------------------------------------------------------------------------------
// GOODS SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 공급물품 HS부호
    //------------------------------------------------------------------------------
    QR_HS.Caption := '[HS 부호 : '+FieldByName('BSN_HSCODE').AsString+']';
    //------------------------------------------------------------------------------
    // 공급물품명
    //------------------------------------------------------------------------------
    QRM_GOODS.Lines.Text := FieldByName('GOODDES1').AsString;
    QRM_GOODS.Height := 13+((QRM_GOODS.Lines.Count-1)*12);
    //------------------------------------------------------------------------------
    // 섹션 높이 변경
    //------------------------------------------------------------------------------
    CHILD_GOODS.Height := (QR_HS.Height+1) + (QRM_GOODS.Height+1);
//------------------------------------------------------------------------------
// ATTACH SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 주요구비서류
    //------------------------------------------------------------------------------
    nIndex := 1;
    for i := 1 to 5 do
    begin
      If FieldByName('DOCCOPY'+IntToStr(i)).AsInteger > 0 Then
      begin
        Case i of
          1: MSG := '물품수령증명서                     ';
          2: MSG := '공급자발행 세금계산서 사본         ';
          3: MSG := '물품명세가 기재된 송장             ';
          4: MSG := '본 내국신용장의 사본               ';
          5: MSG := '공급자발행 물품매도확약서 사본     ';
        end;
        (FindComponent('QR_ATTACH'+IntToStr(nIndex)) as TQRLabel).Caption := MSG + FieldByName('DOCCOPY'+IntToStr(i)).AsString+' 통';
        inc(nIndex);
      end
      else
        Continue;
    end;
//------------------------------------------------------------------------------
// CHILD_ETC_ATTACH SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 기타 구비서류
    //------------------------------------------------------------------------------
    QRM_DOC_ETC1.Lines.Text := FieldByName('DOC_ETC1').AsString;
    IF QRM_DOC_ETC1.Lines.Count > 0 Then
    begin
      QRM_DOC_ETC1.Height := 13+((QRM_DOC_ETC1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_ETC_ATTACH.Height := QRM_DOC_ETC1.Height + 1;
    end
    else
      CHILD_ETC_ATTACH.Enabled := False;
//------------------------------------------------------------------------------
// CHILD_REMARK SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 기타정보
    //------------------------------------------------------------------------------
    QRM_REMARK1.Lines.Text := FieldByName('REMARK1').AsString;
    IF QRM_REMARK1.Lines.Count > 0 Then
    begin
      QRM_REMARK1.Height := 13+((QRM_REMARK1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_REMARK.Height := QRM_REMARK1.Height + 1;
    end
    else
      CHILD_REMARK.Enabled := False;
//------------------------------------------------------------------------------
// CHILD_EXPORT SECTION
//------------------------------------------------------------------------------
    IF Trim(FieldByName('DOC_DTL').AsString) <> '' Then
    begin
      //------------------------------------------------------------------------------
      // 개설근거서류 종류
      //------------------------------------------------------------------------------
      QR_DOC_DTL.Caption := '['+FieldByName('DOC_DTL').AsString+'] '+FieldByName('DOC_DTLNAME').AsString;
      //------------------------------------------------------------------------------
      // 신용장(계약서)번호
      //------------------------------------------------------------------------------
      QR_DOC_NO.Caption := FieldByName('DOC_NO').AsString;
      //------------------------------------------------------------------------------
      // 결제통화 및 금액
      //------------------------------------------------------------------------------
      QR_DOC_AMT.Enabled := Trim(FieldByName('DOC_AMTC').AsString) <> '';
      QR_DOC_AMT.Caption := FieldByName('DOC_AMTC').AsString +'  '+ FormatFloat('#,0.####',FieldByName('DOC_AMT').AsFloat);
      //------------------------------------------------------------------------------
      // 선적(인도)기일
      //------------------------------------------------------------------------------
      QR_LOADDATE.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(FieldByName('LOADDATE').AsString));
      //------------------------------------------------------------------------------
      // 유효기일
      //------------------------------------------------------------------------------
      QR_EXPDATE.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(FieldByName('EXPDATE').AsString));

      QR_ISBANK1.Caption := FieldByName('ISBANK1').AsString;
      QR_ISBANK2.Caption := FieldByName('ISBANK2').AsString;      
    end
    else
      CHILD_EXPORT1.Enabled := False;

//------------------------------------------------------------------------------
// CHILD_EXPORT2 SECTION
//------------------------------------------------------------------------------
    CHILD_EXPORT2.Enabled := CHILD_EXPORT1.Enabled;
    //------------------------------------------------------------------------------
    // 수출(공급)상대방
    //------------------------------------------------------------------------------
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(FieldByName('IM_NAME'+IntToStr(i)).AsString) = '' Then Continue;

      (FindComponent('QR_EXPORT'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('IM_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    CHILD_EXPORT2.Height := ((nIndex-1)*RecordHeight)+1;
//------------------------------------------------------------------------------
// CHILD_EXPORT3 SECTION
//------------------------------------------------------------------------------
    CHILD_EXPORT3.Enabled := CHILD_EXPORT1.Enabled;
    //------------------------------------------------------------------------------
    // 수출지역
    //------------------------------------------------------------------------------
    QR_DEST.Enabled := Trim(FieldByName('DEST').AsString) <> '';
    QR_DEST.Caption := FieldByName('DEST').AsString;
    //------------------------------------------------------------------------------
    // 대금결제조건
    //------------------------------------------------------------------------------
    QR_PAYMENT.Enabled := (FieldByName('PAYMENT').AsString <> '');
    IF FieldByName('PAYMENT').AsString <> '' Then
    begin
      QR_PAYMENT.Caption := '['+FieldByName('PAYMENT').AsString+'] '+FieldByName('PAYMENTNAME').AsString;
    end;
//------------------------------------------------------------------------------
// CHILD_EXPORT4 SECTION
//------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // 대표 수출물품명
    //------------------------------------------------------------------------------
    QRM_EXGOOD1.Lines.Text := FieldByName('EXGOOD1').AsString;
    IF QRM_EXGOOD1.Lines.Count > 0 Then
    begin
      QRM_EXGOOD1.Height := 13+((QRM_EXGOOD1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_EXPORT4.Height := QRM_EXGOOD1.Height + 1;
    end
    else
      CHILD_EXPORT4.Enabled := False;
//------------------------------------------------------------------------------
// SIGN SECTION
//------------------------------------------------------------------------------
    QR_SIGN1.Caption := FieldByName('EXNAME1').AsString;
    QR_SIGN2.Caption := FieldByName('EXNAME2').AsString;
    QR_SIGN3.Caption := FieldByName('EXNAME3').AsString;
  end;
end;

end.
