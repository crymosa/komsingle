unit QR_APP700_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils, DateUtils, Dialogs;

type
  TQR_APP700_PRN_New = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MAINTNO: TQRLabel;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QR_1: TQRLabel;
    QR_APPDATE: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QR_IN_MATHOD: TQRLabel;
    QR_ADPAY: TQRLabel;
    QR_CARRIGE: TQRLabel;
    M_ETCINFO: TQRMemo;
    M_APBANK: TQRMemo;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK_BIC: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListDOC_CD: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListDOC_380: TBooleanField;
    qryListDOC_380_1: TBCDField;
    qryListDOC_705: TBooleanField;
    qryListDOC_705_1: TStringField;
    qryListDOC_705_2: TStringField;
    qryListDOC_705_3: TStringField;
    qryListDOC_705_4: TStringField;
    qryListDOC_740: TBooleanField;
    qryListDOC_740_1: TStringField;
    qryListDOC_740_2: TStringField;
    qryListDOC_740_3: TStringField;
    qryListDOC_740_4: TStringField;
    qryListDOC_530: TBooleanField;
    qryListDOC_530_1: TStringField;
    qryListDOC_530_2: TStringField;
    qryListDOC_271: TBooleanField;
    qryListDOC_271_1: TBCDField;
    qryListDOC_861: TBooleanField;
    qryListDOC_2AA: TBooleanField;
    qryListDOC_2AA_1: TMemoField;
    qryListACD_2AA: TBooleanField;
    qryListACD_2AA_1: TStringField;
    qryListACD_2AB: TBooleanField;
    qryListACD_2AC: TBooleanField;
    qryListACD_2AD: TBooleanField;
    qryListACD_2AE: TBooleanField;
    qryListACD_2AE_1: TMemoField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListPERIOD: TBCDField;
    qryListCONFIRMM: TStringField;
    qryListINSTRCT: TBooleanField;
    qryListINSTRCT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListORIGIN: TStringField;
    qryListORIGIN_M: TStringField;
    qryListPL_TERM: TStringField;
    qryListTERM_PR: TStringField;
    qryListTERM_PR_M: TStringField;
    qryListSRBUHO: TStringField;
    qryListDOC_705_GUBUN: TStringField;
    qryListCARRIAGE: TStringField;
    qryListDOC_760: TBooleanField;
    qryListDOC_760_1: TStringField;
    qryListDOC_760_2: TStringField;
    qryListDOC_760_3: TStringField;
    qryListDOC_760_4: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListCONFIRM_BICCD: TStringField;
    qryListCONFIRM_BANKNM: TStringField;
    qryListCONFIRM_BANKBR: TStringField;
    qryListPERIOD_TXT: TStringField;
    qryListSPECIAL_PAY: TMemoField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListDOC_Name: TStringField;
    qryListdoc705_Name: TStringField;
    qryListdoc740_Name: TStringField;
    qryListdoc760_Name: TStringField;
    qryListCHARGE_Name: TStringField;
    qryListCARRIAGE_NAME: TStringField;
    qryListPERIOD_IDX: TIntegerField;
    qryListMSEQ: TIntegerField;
    M_IL: TQRMemo;
    M_ADBANK: TQRMemo;
    ChildBand2: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel9: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    ChildBand4: TQRChildBand;
    QR_3_1: TQRLabel;
    QR_3_2: TQRLabel;
    QR_3_4: TQRLabel;
    QR_3_5: TQRLabel;
    QR_3_7: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel31: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel32: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel33: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    SummaryBand1: TQRBand;
    QRImage3: TQRImage;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRShape2: TQRShape;
    QR_DOCCD: TQRLabel;
    QR_EXDATE: TQRLabel;
    M_APPLIC: TQRMemo;
    M_BENEFC: TQRMemo;
    QR_CDAMT: TQRLabel;
    QR_PCD: TQRLabel;
    M_AACV: TQRMemo;
    M_DRAFT: TQRMemo;
    M_MIX: TQRMemo;
    M_DEF: TQRMemo;
    QRLabel38: TQRLabel;
    QR_PSHIP: TQRLabel;
    QR_TSHIP: TQRLabel;
    QR_SUNJUK: TQRLabel;
    QR_DOCHAK: TQRLabel;
    M_PERIOD: TQRMemo;
    QRBand1: TQRBand;
    QRLabel43: TQRLabel;
    QR_3_3: TQRLabel;
    QR_3_6: TQRLabel;
    QR_LOADON: TQRLabel;
    QR_FORTRAN: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FSUM_HEIGHT : integer;
    FMAINT_NO : String;
    procedure RecordHeight(nValue : integer);
  public
     property MAINT_NO:string read FMAINT_NO write FMAINT_NO;
  end;

var
  QR_APP700_PRN_New: TQR_APP700_PRN_New;
Const
  RowHeight : integer = 13;
  IndentHeight : integer = 4;
implementation

{$R *.DFM}

procedure TQR_APP700_PRN_New.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FSUM_HEIGHT := 0;
  
  qryList.Close;
  Qrylist.Parameters[0].Value := FMAINT_NO;
  qryList.Open;
end;

procedure TQR_APP700_PRN_New.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
end;

procedure TQR_APP700_PRN_New.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_LIST : TStringList;
  i : integer;
  IL_NO, IL_CUR : String;
  IL_AMT :Currency;
begin
  FSUM_HEIGHT := 0;

  QR_APPDATE.Caption   := LeftStr(qryListAPP_DATE.AsString,4)+'-'+MidStr(qryListAPP_DATE.AsString,5,2)+'-'+RightStr(qryListAPP_DATE.AsString,2);
  QR_IN_MATHOD.Caption := qryListmathod_Name.AsString;
  // 운송방법
  IF UpperCase( qryListCARRIAGE.AsString ) = 'DT' Then
    QR_CARRIGE.Caption   := 'Sea / Air';
  IF UpperCase( qryListCARRIAGE.AsString ) = 'DQ' Then
    QR_CARRIGE.Caption   := 'Complex / Etc';
  // 개설의뢰은행
  M_APBANK.Lines.Clear;
  IF Trim(qryListAP_BANK.AsString) <> '' Then M_APBANK.Lines.Add(qryListAP_BANK.AsString);
  IF Trim(qryListAP_BANK1.AsString) <> '' Then M_APBANK.Lines.Add( Trim(qryListAP_BANK1.AsString)+Trim(qryListAP_BANK2.AsString) );
  IF Trim(qryListAP_BANK3.AsString) <> '' Then M_APBANK.Lines.Add( Trim(qryListAP_BANK3.AsString)+Trim(qryListAP_BANK4.AsString) );
  IF Trim(qryListAP_BANK5.AsString) <> '' Then M_APBANK.Lines.Add( '(Tel No.) '+qryListAP_BANK5.AsString );
  M_APBANK.Height := M_APBANK.Lines.Count * RowHeight;
  RecordHeight(M_APBANK.Height);

  //통지은행
  QRLabel12.Top := 101;
  QRLabel12.Top := QRLabel12.Top + FSUM_HEIGHT;
  M_ADBANK.Top := QRLabel12.Top;
  M_ADBANK.Lines.Clear;
//    IF Trim(qryListAD_BANK.AsString) <> '' Then M_ADBANK.Lines.Add(qryListAD_BANK.AsString);
  IF Trim(qryListAD_BANK1.AsString) <> '' Then M_ADBANK.Lines.Add( Trim(qryListAD_BANK1.AsString)+Trim(qryListAD_BANK2.AsString) );
  IF Trim(qryListAD_BANK3.AsString) <> '' Then M_ADBANK.Lines.Add( Trim(qryListAD_BANK3.AsString)+Trim(qryListAD_BANK4.AsString) );
  IF Trim(qryListAD_BANK_BIC.AsString) <> '' Then M_ADBANK.Lines.Add( Trim(qryListAD_BANK_BIC.AsString));
  M_ADBANK.Height := M_ADBANK.Lines.Count * RowHeight;
  RecordHeight(M_ADBANK.Height);

  //신용공여
  QRLabel13.Top := 118;
  IF Trim(qryListpay_Name.AsString) <> '' Then
  begin
    QRLabel13.Enabled := True;
    QRLabel13.Top := QRLabel13.Top + FSUM_HEIGHT;
    QR_ADPAY.Top := QRLabel13.Top;
    QR_ADPAY.Caption := qryListpay_Name.AsString;
    RecordHeight(13);
  end
  else
  begin
    QRLabel13.Enabled := False;
    QR_ADPAY.Enabled := False;
    RecordHeight(0);
  end;

  //수입승인번호
  QRLabel14.Top := 135;
  QRLabel14.Top := QRLabel14.Top + FSUM_HEIGHT;
  M_IL.Top := QRLabel14.Top;
  M_IL.Lines.Clear;
  for i := 1 to 5 do
  begin
    IL_NO := Trim(qryList.FieldByName('IL_NO'+IntToStr(i)).AsString);
    IF IL_NO  <> '' Then
    begin
      IL_CUR := qryList.FieldByName('IL_CUR'+IntToStr(i)).AsString;
      IL_AMT := qryList.FieldByName('IL_AMT'+IntToStr(i)).AsCurrency;
      M_IL.Lines.Add(Format('%-36s%-4s%-18s',[IL_NO, IL_CUR, formatfloat('#,0.00',IL_AMT)]));
    end;
  end;
  M_IL.Height := M_IL.Lines.Count * RowHeight;
  RecordHeight(M_IL.Height);


  //기타정보
  QRLabel15.Top := 152;
  QRLabel15.Top := QRLabel15.Top + FSUM_HEIGHT;
  M_ETCINFO.Top := QRLabel15.Top;
  M_ETCINFO.Lines.Clear;
  for i := 1 to 5 do
  begin
    IL_NO := Trim(qryList.FieldByName('AD_INFO'+IntToStr(i)).AsString);
    IF IL_NO <> '' Then
      M_ETCINFO.Lines.Add(IL_NO);
  end;
  M_ETCINFO.Height := M_ETCINFO.Lines.Count * RowHeight;
  RecordHeight(M_ETCINFO.Height);

  ChildBand1.Height := 178;
  ChildBand1.Height := ChildBand1.Height + FSUM_HEIGHT;
end;

procedure TQR_APP700_PRN_New.RecordHeight(nValue: integer);
begin
  IF nValue > 0 Then
    FSUM_HEIGHT := FSUM_HEIGHT+nValue-13
  else
    FSUM_HEIGHT := FSUM_HEIGHT-13-IndentHeight;
end;

procedure TQR_APP700_PRN_New.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
  TMP_CUR : Currency;
  i : integer;
begin
  FSUM_HEIGHT := 0;
  QRLabel16.Top := 24;
  QRLabel17.Top := 40;
  QRLabel18.Top := 56;
  QRLabel19.Top := 72;
  QRLabel20.Top := 88;
  QRLabel21.Top := 104;
  QRLabel22.Top := 120;

  QR_DOCCD.Top := QRLabel16.Top;
  QR_DOCCD.Caption := qryListDOC_Name.AsString;

  QR_EXDATE.Top := QRLabel17.Top;
  QR_EXDATE.Caption := qryListEX_DATE.AsString + ' ' + qryListEX_DATE.AsString;

  M_APPLIC.Lines.Clear;
  IF Trim(qryListAPPLIC1.AsString) <> '' Then M_APPLIC.Lines.Add( Trim(qryListAPPLIC1.AsString) );
  IF Trim(qryListAPPLIC2.AsString) <> '' Then M_APPLIC.Lines.Add( Trim(qryListAPPLIC2.AsString) );
  IF Trim(qryListAPPLIC3.AsString) <> '' Then M_APPLIC.Lines.Add( Trim(qryListAPPLIC3.AsString) );
  IF Trim(qryListAPPLIC4.AsString) <> '' Then M_APPLIC.Lines.Add( Trim(qryListAPPLIC4.AsString) );
  IF Trim(qryListAPPLIC5.AsString) <> '' Then M_APPLIC.Lines.Add( '(Tel No.) '+Trim(qryListAPPLIC5.AsString) );
  M_APPLIC.Height := M_APPLIC.Lines.Count * RowHeight;
  RecordHeight(M_APPLIC.Height);

  QRLabel19.Top := QRLabel19.Top + FSUM_HEIGHT;
  M_BENEFC.Top := QRLabel19.Top;
  M_BENEFC.Lines.Clear;
  IF Trim(qryListBENEFC1.AsString) <> '' Then M_BENEFC.Lines.Add( Trim(qryListBENEFC1.AsString) );
  IF Trim(qryListBENEFC2.AsString) <> '' Then M_BENEFC.Lines.Add( Trim(qryListBENEFC2.AsString) );
  IF Trim(qryListBENEFC3.AsString) <> '' Then M_BENEFC.Lines.Add( Trim(qryListBENEFC3.AsString) );
  IF Trim(qryListBENEFC4.AsString) <> '' Then M_BENEFC.Lines.Add( Trim(qryListBENEFC4.AsString) );
  IF Trim(qryListBENEFC5.AsString) <> '' Then M_BENEFC.Lines.Add( '(Account number) '+Trim(qryListBENEFC5.AsString) );
  M_BENEFC.Height := M_BENEFC.Lines.Count * RowHeight;
  RecordHeight(M_BENEFC.Height);

  QRLabel20.Top := QRLabel20.Top + FSUM_HEIGHT;
  QR_CDAMT.Top := QRLabel20.Top;
  TMP_STR := qryListCD_CUR.AsString;
  TMP_CUR := qryListCD_AMT.AsCurrency;
  QR_CDAMT.Caption := Format('%-4s%s',[TMP_STR, formatfloat('#,0.00',TMP_CUR)]);

  QRLabel21.Top := QRLabel21.Top + FSUM_HEIGHT;
  QR_PCD.Top := QRLabel21.Top;
  QR_PCD.Caption := '(+)'+qryListCD_PERP.AsString+'/(-)'+qryListCD_PERM.AsString;

  QRLabel22.Top := QRLabel22.Top + FSUM_HEIGHT;
  M_AACV.Top := QRLabel22.Top;
  M_AACV.Lines.Clear;
  IF Trim(qryListAA_CV1.AsString) <> '' Then M_AACV.Lines.Add( Trim(qryListAA_CV1.AsString) );
  IF Trim(qryListAA_CV2.AsString) <> '' Then M_AACV.Lines.Add( Trim(qryListAA_CV2.AsString) );
  IF Trim(qryListAA_CV3.AsString) <> '' Then M_AACV.Lines.Add( Trim(qryListAA_CV3.AsString) );
  IF Trim(qryListAA_CV4.AsString) <> '' Then M_AACV.Lines.Add( Trim(qryListAA_CV4.AsString) );
  M_AACV.Height := M_AACV.Lines.Count * RowHeight;
  RecordHeight(M_AACV.Height);

  ChildBand2.Height := 135;
  ChildBand2.Height := ChildBand2.Height + FSUM_HEIGHT;

end;

procedure TQR_APP700_PRN_New.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
  TMP_CUR : Currency;
  i : integer;
begin
  FSUM_HEIGHT := 0;
  QRLabel23.Top := 2;
  QRLabel24.Top := 18;
  QRLabel25.Top := 34;
  ChildBand3.Height := 50;

  M_DRAFT.Lines.Clear;
  IF Trim(qryListDRAFT1.AsString) <> '' Then M_DRAFT.Lines.Add( Trim(qryListDRAFT1.AsString) );
  IF Trim(qryListDRAFT2.AsString) <> '' Then M_DRAFT.Lines.Add( Trim(qryListDRAFT2.AsString) );
  IF Trim(qryListDRAFT3.AsString) <> '' Then M_DRAFT.Lines.Add( Trim(qryListDRAFT3.AsString) );
  M_DRAFT.Height := M_DRAFT.Lines.Count * RowHeight;
  RecordHeight(M_DRAFT.Height);
  QRLabel23.Enabled := M_DRAFT.Height > 0;
  M_DRAFT.Enabled := QRLabel23.Enabled;

  M_MIX.Lines.Clear;
  QRLabel24.Top := QRLabel24.Top + FSUM_HEIGHT;
  M_MIX.Top := QRLabel24.Top;
  IF Trim(qryListMIX_PAY1.AsString) <> '' Then M_MIX.Lines.Add( Trim(qryListMIX_PAY1.AsString) );
  IF Trim(qryListMIX_PAY2.AsString) <> '' Then M_MIX.Lines.Add( Trim(qryListMIX_PAY2.AsString) );
  IF Trim(qryListMIX_PAY3.AsString) <> '' Then M_MIX.Lines.Add( Trim(qryListMIX_PAY3.AsString) );
  IF Trim(qryListMIX_PAY4.AsString) <> '' Then M_MIX.Lines.Add( Trim(qryListMIX_PAY4.AsString) );
  M_MIX.Height := M_MIX.Lines.Count * RowHeight;
  RecordHeight(M_MIX.Height);
  QRLabel24.Enabled := M_MIX.Height > 0;
  M_MIX.Enabled := QRLabel24.Enabled;
  
  M_DEF.Lines.Clear;
  QRLabel25.Top := QRLabel25.Top + FSUM_HEIGHT;
  M_DEF.Top := QRLabel25.Top;
  IF Trim(qryListDEF_PAY1.AsString) <> '' Then M_DEF.Lines.Add( Trim(qryListDEF_PAY1.AsString) );
  IF Trim(qryListDEF_PAY2.AsString) <> '' Then M_DEF.Lines.Add( Trim(qryListDEF_PAY2.AsString) );
  IF Trim(qryListDEF_PAY3.AsString) <> '' Then M_DEF.Lines.Add( Trim(qryListDEF_PAY3.AsString) );
  IF Trim(qryListDEF_PAY4.AsString) <> '' Then M_DEF.Lines.Add( Trim(qryListDEF_PAY4.AsString) );
  M_DEF.Height := M_DEF.Lines.Count * RowHeight;
  RecordHeight(M_DEF.Height);
  QRLabel25.Enabled := M_DEF.Height > 0;
  M_DEF.Enabled := QRLabel25.Enabled;

  ChildBand3.Height := ChildBand3.Height + FSUM_HEIGHT;
  ChildBand3.Enabled :=  M_DRAFT.Enabled OR M_MIX.Enabled OR M_DEF.Enabled;

end;

procedure TQR_APP700_PRN_New.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
  TMP_CUR : Currency;
  i : integer;
begin
  FSUM_HEIGHT := 0;
  QR_3_1.Top := 3;
  QR_3_2.Top := QR_3_1.Top + (1 * 16);
  QR_3_3.Top := QR_3_1.Top + (2 * 16);
  QR_3_4.Top := QR_3_1.Top + (3 * 16);
  QR_3_5.Top := QR_3_1.Top + (4 * 16);
  QR_3_6.Top := QR_3_1.Top + (5 * 16);
  QR_3_7.Top := QR_3_1.Top + (6 * 16);
  ChildBand4.Height := 115;

  Case AnsiIndexText(qryListPSHIP.AsString, ['9','10']) of
    0: QR_PSHIP.Caption := 'ALLOWED';
    1: QR_PSHIP.Caption := 'NOT ALLOWED';
  end;
  Case AnsiIndexText(qryListTSHIP.AsString, ['7','8']) of
    0: QR_TSHIP.Caption := 'ALLOWED';
    1: QR_TSHIP.Caption := 'NOT ALLOWED';
  end;

  IF Trim(qryListLOAD_ON.AsString) <> '' Then
    QR_LOADON.Caption := qryListLOAD_ON.AsString
  else
  begin
    QR_3_3.Enabled := False;
    QR_LOADON.Enabled := False;
    RecordHeight(0);
  end;

  QR_3_4.Top := QR_3_4.Top + FSUM_HEIGHT;
  QR_SUNJUK.Top := QR_3_4.Top;
  IF Trim(qryListSUNJUCK_PORT.AsString) <> '' Then
    QR_SUNJUK.Caption := qryListSUNJUCK_PORT.AsString
  else
  begin
    QR_3_4.Enabled := False;
    QR_SUNJUK.Enabled := False;
    RecordHeight(0);
  end;

  QR_3_5.Top := QR_3_5.Top + FSUM_HEIGHT;
  QR_DOCHAK.Top := QR_3_5.Top;
  IF Trim(qryListDOCHACK_PORT.AsString) <> '' Then
    QR_DOCHAK.Caption := qryListDOCHACK_PORT.AsString
  else
  begin
    QR_3_5.Enabled := False;
    QR_DOCHAK.Enabled := False;
    RecordHeight(0);
  end;

  QR_3_6.Top := QR_3_6.Top + FSUM_HEIGHT;
  QR_FORTRAN.Top := QR_3_6.Top;
  IF Trim(qryListFOR_TRAN.AsString) <> '' Then
    QR_FORTRAN.Caption := qryListFOR_TRAN.AsString
  else
  begin
    QR_3_6.Enabled := False;
    QR_FORTRAN.Enabled := False;
    RecordHeight(0);
  end;

  QR_3_7.Top := QR_3_7.Top + FSUM_HEIGHT;
  M_PERIOD.Top := QR_3_7.Top;
  M_PERIOD.Lines.Clear;
  for i := 1 to 5 do
  begin
    TMP_STR := Trim(qryList.FieldByName('SHIP_PD'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_PERIOD.Lines.Add(TMP_STR);
  end;
  M_PERIOD.Height := M_PERIOD.Lines.Count * RowHeight;
  RecordHeight(M_PERIOD.Height);

  ChildBand4.Height := ChildBand4.Height+FSUM_HEIGHT; 
end;

end.
