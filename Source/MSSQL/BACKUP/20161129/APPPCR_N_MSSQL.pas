unit APPPCR_N_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, APPPCR_N, DB, ADODB, sSkinProvider, StdCtrls, sButton, DBCtrls,
  sDBMemo, sLabel, sDBEdit, sGroupBox, ExtCtrls, sSplitter, Grids, DBGrids,
  acDBGrid, sEdit, sComboBox, Mask, sMaskEdit, ComCtrls, sPageControl, sPanel,
  StrUtils, DateUtils, Buttons, sSpeedButton, TypeDefine, sMemo, QuickRpt;

type
  TAPPPCR_N_MSSQL_frm = class(TAPPPCR_N_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSUP_CODE: TStringField;
    qryListSUP_NAME1: TStringField;
    qryListSUP_NAME2: TStringField;
    qryListSUP_NAME3: TStringField;
    qryListSUP_ADDR1: TStringField;
    qryListSUP_ADDR2: TStringField;
    qryListSUP_ADDR3: TStringField;
    qryListITM_G1: TStringField;
    qryListITM_G2: TStringField;
    qryListITM_G3: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListTAMT1: TBCDField;
    qryListTAMT1C: TStringField;
    qryListTAMT2: TBCDField;
    qryListTAMT2C: TStringField;
    qryListCHG_G: TStringField;
    qryListC1_AMT: TBCDField;
    qryListC1_C: TStringField;
    qryListC2_AMT: TBCDField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListPRNO: TIntegerField;
    qryListPCrLic_No: TStringField;
    qryListChgCd: TStringField;
    qryListAPP_DATE: TStringField;
    qryListDOC_G: TStringField;
    qryListDOC_D: TStringField;
    qryListBHS_NO: TStringField;
    qryListBNAME: TStringField;
    qryListBNAME1: TMemoField;
    qryListBAMT: TBCDField;
    qryListBAMTC: TStringField;
    qryListEXP_DATE: TStringField;
    qryListSHIP_DATE: TStringField;
    qryListBSIZE1: TMemoField;
    qryListBAL_CD: TStringField;
    qryListBAL_NAME1: TStringField;
    qryListBAL_NAME2: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListSRBUHO: TStringField;
    qryListITM_GBN: TStringField;
    qryListITM_GNAME: TStringField;
    qryListISSUE_GBN: TStringField;
    qryListDOC_GBN: TStringField;
    qryListDOC_NO: TStringField;
    qryListBAMT2: TBCDField;
    qryListBAMTC2: TStringField;
    qryListBAMT3: TBCDField;
    qryListBAMTC3: TStringField;
    qryListPCRLIC_ISS_NO: TStringField;
    qryListFIN_CODE: TStringField;
    qryListFIN_NAME: TStringField;
    qryListFIN_NAME2: TStringField;
    qryListNEGO_APPDT: TStringField;
    qryListEMAIL_ID: TStringField;
    qryListEMAIL_DOMAIN: TStringField;
    qryListBK_CD: TStringField;
    sPanel4: TsPanel;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailREMARK: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTYC: TStringField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailPRI1: TBCDField;
    qryDetailPRI2: TBCDField;
    qryDetailPRI_BASE: TBCDField;
    qryDetailPRI_BASEC: TStringField;
    qryDetailACHG_G: TStringField;
    qryDetailAC1_AMT: TBCDField;
    qryDetailAC1_C: TStringField;
    qryDetailAC2_AMT: TBCDField;
    qryDetailLOC_TYPE: TStringField;
    qryDetailNAMESS: TStringField;
    qryDetailSIZESS: TStringField;
    qryDetailAPP_DATE: TStringField;
    qryDetailITM_NUM: TStringField;
    sButton1: TsButton;
    qryDocument: TADOQuery;
    dsDocument: TDataSource;
    qryDocumentKEYY: TStringField;
    qryDocumentSEQ: TIntegerField;
    qryDocumentDOC_G: TStringField;
    qryDocumentDOC_D: TStringField;
    qryDocumentBHS_NO: TStringField;
    qryDocumentBNAME: TStringField;
    qryDocumentBNAME1: TMemoField;
    qryDocumentBAMT: TBCDField;
    qryDocumentBAMTC: TStringField;
    qryDocumentEXP_DATE: TStringField;
    qryDocumentSHIP_DATE: TStringField;
    qryDocumentBSIZE1: TMemoField;
    qryDocumentBAL_NAME1: TStringField;
    qryDocumentBAL_NAME2: TStringField;
    qryDocumentBAL_CD: TStringField;
    qryDocumentNAT: TStringField;
    qryTax: TADOQuery;
    dsTax: TDataSource;
    qryTaxKEYY: TStringField;
    qryTaxDATEE: TStringField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxITEM_NAME1: TStringField;
    qryTaxITEM_NAME2: TStringField;
    qryTaxITEM_NAME3: TStringField;
    qryTaxITEM_NAME4: TStringField;
    qryTaxITEM_NAME5: TStringField;
    qryTaxITEM_DESC: TMemoField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxQUANTITY: TBCDField;
    qryTaxQUANTITY_UNIT: TStringField;
    sSpeedButton3: TsSpeedButton;
    sSplitter2: TsSplitter;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    Btn_Ready: TsSpeedButton;
    qryReady: TADOQuery;
    sPanel5: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sPanel6: TsPanel;
    sSpeedButton8: TsSpeedButton;
    qryDocumentDOC_NAME: TStringField;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Btn_Temp: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    Edt_DocNo: TsEdit;
    qryDetailDel: TADOQuery;
    qryTaxSEQ: TIntegerField;
    Btn_TaxOk: TsSpeedButton;
    Btn_TaxCancel: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    Btn_DocOk: TsSpeedButton;
    Btn_DocCancel: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    Btn_DetailOk: TsSpeedButton;
    Btn_DetailCancel: TsSpeedButton;
    sSpeedButton14: TsSpeedButton;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    qryDel: TADOQuery;
    QRCompositeReport1: TQRCompositeReport;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    qryCopy: TADOQuery;
    sLabel2: TsLabel;
    sSpeedButton16: TsSpeedButton;
    qry_UpdateTempNo2NewNo: TADOQuery;
    sLabel8: TsLabel;
    sButton2: TsButton;
    qryCalcTotal: TADOQuery;
    qryMAX_SEQ: TADOQuery;
    procedure qryListCHK2GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure qryListCHK3GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure qryListDATEEGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryDetailNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure sButton1Click(Sender: TObject);
    procedure sDBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryDocumentAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_ReadyClick(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure qryDocumentBNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure Edt_DocumentsSectionExit(Sender: TObject);
    procedure Edt_DocumentsSectionDblClick(Sender: TObject);
    procedure Btn_NewClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure sPageControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure sPageControl1Change(Sender: TObject);
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure Btn_SaveClick(Sender: TObject);
    procedure sEdit39DblClick(Sender: TObject);
    procedure Btn_TaxAddClick(Sender: TObject);
    procedure qryTaxAfterInsert(DataSet: TDataSet);
    procedure qryTaxAfterEdit(DataSet: TDataSet);
    procedure sDBEdit53Enter(Sender: TObject);
    procedure sDBEdit53Exit(Sender: TObject);
    procedure sDBEdit60DblClick(Sender: TObject);
    procedure sDBEdit60Exit(Sender: TObject);
    procedure sDBEdit60KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure qryDocumentAfterEdit(DataSet: TDataSet);
    procedure qryDocumentAfterInsert(DataSet: TDataSet);
    procedure qryDocumentAfterOpen(DataSet: TDataSet);
    procedure sDBEdit48DblClick(Sender: TObject);
    procedure sDBEdit51DblClick(Sender: TObject);
    procedure sDBEdit65DblClick(Sender: TObject);
    procedure Btn_DocAddClick(Sender: TObject);
    procedure Btn_DetailAddClick(Sender: TObject);
    procedure qryDetailAfterEdit(DataSet: TDataSet);
    procedure qryDetailAfterInsert(DataSet: TDataSet);
    procedure Edt_ApplicationCodeDblClick(Sender: TObject);
    procedure sEdit39Exit(Sender: TObject);
    procedure Edt_ApplicationCodeExit(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure Btn_PrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure sPanel1Click(Sender: TObject);
    procedure sSpeedButton6Click(Sender: TObject);
    procedure sSpeedButton7Click(Sender: TObject);
    procedure sDBEdit34DblClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBEdit43Exit(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sDBEdit45Enter(Sender: TObject);
  private
    { Private declarations }
    FSQL: string;
    FCurrentWork: TProgramControlType;
    FSelectedRow: TStringList;

    //beforePageIndex
    FBeforePageIndex: Integer;

    //문서생성 신규 또는 복사
    FNewDocCopy : Boolean;

    //채번여부
    FAutoNumbering : Boolean;

    //신규시 임시 관리번호를 부여하여 세부사항,근거서류,세금계산서를 우선저장한다
    //수정시에는 현재 관리번호를 저장한다.
    FTempDocNo: string;
    procedure ReadMstList;
    procedure ReadDetail(TempDocNo: string = '');
    procedure ReadDocuments(TempDocNo: string = '');
    procedure ReadTax(TempDocNo: string = '');
    procedure SelectData;
    procedure ReadyDoc;

    //마지막 순번
    function MAX_SEQ(TableName : String):Integer;

    //Print RecordData
    procedure PrintRecordData(var GB: TsGroupBox; var qry: TADOQuery; Arow: Integer);

    //CommonData ReadOnly Control
    procedure CreateReady(bValue: Boolean = False);
    procedure GroupReadOnly(var GroupBox: TsGroupBox; bValue: Boolean = False);
    //ClearStringGrid
    procedure SetData;
    procedure AfterSetData;
    procedure Dialog_Detail(Sender: TsSpeedButton);
    procedure Dialog_Documents(Sender: TsSpeedButton);
    procedure Dialog_Tax(Sender: TsSpeedButton);

    //DataInsert, Modify, Del
    procedure INIT_INSERT;
    procedure SetParamExec(var qry: TADOQuery; bTemp: Boolean);

    procedure DeleteDoc;
    //임시관리번호(복사시)를 신규 관리번호로 업데이트
    procedure Update_Temp2NewDocNo;

    //데이터 검증
    function CHECK_VALIDITY:String;

  public
    { Public declarations }
  end;

var
  APPPCR_N_MSSQL_frm: TAPPPCR_N_MSSQL_frm;

implementation

uses
  Commonlib, CodeContents, VarDefine, ICON, CreateDocuments, Dlg_RecvSelect,
  Dlg_APPPCR_CreateDoc, Dlg_FindDefineCode, Dlg_APPPCR_Detail, MSSQL,
  Dlg_APPPCR_Documents, Dlg_FindBankCode, Dlg_FindJepum, Dlg_Customer,
  APPPCR_PRINT, APPPCR_PRINT1, APPPCR_PRINT2, AutoNo,
  Dlg_APPPCR_newDocSelect, Dlg_APPPCR_SelectCopyDoc, Dlg_ErrorMessage;

{$R *.dfm}

procedure TAPPPCR_N_MSSQL_frm.qryListCHK2GetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  if qryListCHK2.IsNull then
    Exit;
  case qryListCHK2.AsInteger of
    0:
      Text := '임시';
    1:
      Text := '문서';
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryListCHK3GetText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  sDate: string;
  sState: string;
begin
  sDate := qryListCHK3.AsString;

  if StringReplace(sDate, ' ', '', [rfReplaceAll]) <> '' then
  begin
    case StrToInt(RightStr(sDate, 1)) of
      1:
        sState := '송신준비';
      2:
        sState := '준비삭제';
      3:
        sState := '변환오류';
      4:
        sState := '통신오류';
      5:
        sState := '송신완료';
    end;
    Text := FormatDateTime('YYYY.MM.DD', decodeCharDate(LeftStr(sDate, 8))) + ' ' + sState;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryListDATEEGetText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  sDate: string;
begin
  inherited;
  sDate := qryListDATEE.AsString;
  if sDate = '' then
    Exit;
  Text := FormatDateTime('YYYY.MM.DD', decodeCharDate(LeftStr(sDate, 8)))
end;

procedure TAPPPCR_N_MSSQL_frm.ReadMstList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
//    ParamByName('FROMDATE').Value := sMaskEdit6.Text;
//    ParamByName('TODATE'  ).Value := sMaskEdit7.Text;
//    IF sEdit3.Text <> '' Then
    SQL.Add('AND MAINT_NO LIKE ' + QuotedStr('%' + sEdit3.Text + '%'));
    SQL.Add('AND USER_ID = ' + QuotedStr(LoginData.sID));
    //CHK2 2 = 삭제
    SQL.Add('AND CHK2 != 2');
    SQL.Add('ORDER BY DATEE DESC');
    Open;

    //체크사항 초기화
    FSelectedRow.Clear;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y > 17 then
  begin
    if qryList.RecordCount > 0 then
    begin
      sPageControl1.ActivePageIndex := 1;
//      SelectData;
    end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  FSelectedRow := TStringList.Create;
  FCurrentWork := ctView;
  sMaskEdit6.Text := '20000101';
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sPageControl1.ActivePageIndex := 0;
  ReadMstList;
end;

procedure TAPPPCR_N_MSSQL_frm.sEdit3KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then
    ReadMstList;
end;

procedure TAPPPCR_N_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sPanel4.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
  Edt_DocNo.Text := qryListMAINT_NO.AsString;

  SetData;

  //세부사항
  ReadDetail;
  //근거서류
  ReadDocuments;
  //세금계산서
  ReadTax;

  //상단패널 정보
  Edt_User.Text := LoginData.sID;
  EdtMk_RegDate.Text := qryListDATEE.AsString;

  // 9: 원본 4: 수정
  Com_DocumentFunc.ItemIndex := AnsiIndexText(qryListMESSAGE1.AsString, ['', '9', '4']);

  //응답유형
  sComboBox2.ItemIndex := AnsiIndexText(qryListMESSAGE2.AsString, ['', 'AB']);
end;

procedure TAPPPCR_N_MSSQL_frm.qryDetailNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString, #13#10, ' ');
end;

procedure TAPPPCR_N_MSSQL_frm.ReadDetail(TempDocNo: string = '');
begin
  if TempDocNo <> '' then
  begin
    with qryDetail do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := TempDocNo;
      open;
    end;
  end
  else if qryList.RecordCount > 0 then
  begin
    with qryDetail do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := qryList.FieldByName('MAINT_NO').AsString;
      open;
    end;
  end
  else
  begin
    qryDetail.Close;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.ReadTax(TempDocNo: string);
begin
  if TempDocNo <> '' then
  begin
    with qryTax do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := TempDocNo;
      open;
    end;
  end
  else if qryList.RecordCount > 0 then
  begin
    with qryTax do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := qryList.FieldByName('MAINT_NO').AsString;
      open;
    end;
  end
  else
  begin
    qryTax.Close;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadMstList;
end;

procedure TAPPPCR_N_MSSQL_frm.SelectData;
begin
  if qryList.RecordCount = 0 then
    Exit;

  if FSelectedRow.IndexOf(qryListMAINT_NO.AsString) = -1 then
  begin
    FSelectedRow.Add(qryListMAINT_NO.AsString);
  end
  else
  begin
    FSelectedRow.Delete(FSelectedRow.IndexOf(qryListMAINT_NO.AsString));
  end;
  sDBGrid1.Repaint;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_SPACE then
  begin
    SelectData;
    qryList.Next;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  DocumentState: string;
begin
  inherited;
  if qryList.RecordCount = 0 then
    exit;

  with Sender as TsDBGrid do
  begin
    if qryList.FieldByName('CHK2').AsInteger = 0 then
    begin
      Canvas.Brush.Color := $00B5F19A;
      Canvas.Font.Color := clBlack;
    end
    else
    begin
      Canvas.Brush.Color := clWhite;
    end;

    if Column.FieldName = 'CHK3' then
    begin
      case AnsiIndexText(RightStr(qryList.FieldByName('CHK3').AsString, 1), ['1', '2', '3', '4']) of
        0:
          begin
            Canvas.Brush.Color := clBlack;
            Canvas.Font.Color := clWhite;
          end;
        1:
          begin
            Canvas.Brush.Color := clBlack;
            Canvas.Font.Color := clYellow;
          end;
        2, 3:
          begin
            Canvas.Brush.Color := clBlack;
            Canvas.Font.Color := clRed;
          end;
      end;
    end;

    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $00CC8A00;
      Canvas.Font.Color := clWhite;
    end;

    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  {IF Column.Title.Caption = '상황' then
  begin
    case qryList.FieldByName('CHK2').AsInteger of
      0: DocumentState := '임시';
      1: DocumentState := '문서';
    end;
    sDBGrid1.Canvas.TextOut(Rect.Left + 5, Rect.Top+1, DocumentState);
  end;}

  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clblack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  if Column.Title.Caption = '선택' then
  begin
    if FSelectedRow.IndexOf(qryListMAINT_NO.AsString) <> -1 then
    begin
      sDBGrid1.Canvas.TextOut(Rect.Left + 10, Rect.Top + 2, '>>');
    end;
  end;

end;

procedure TAPPPCR_N_MSSQL_frm.ReadDocuments(TempDocNo: string = '');
begin
  if TempDocNo <> '' then
  begin
    with qryDocument do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := TempDocNo;
      open;
    end;
  end
  else if qryList.RecordCount > 0 then
  begin
    with qryDocument do
    begin
      Close;
      Parameters.ParamByName('KEY').Value := qryListMAINT_NO.AsString;
      Open;
    end;
  end
  else
  begin
    qryDocument.Close;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryDocumentAfterScroll(DataSet: TDataSet);
begin
  inherited;
  with DMCodeContents do
  begin
    //PAGE 1 신청/변경구분
    if APPPCR_DOCUMENTS.Locate('CODE', qryDocumentDOC_G.AsString, []) then
      Pan_DocNO.Caption := APPPCR_DOCUMENTS.FieldByName('NAME').AsString
    else
      Pan_DocNO.Caption := '';
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
//  ReadMstList;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TAPPPCR_N_MSSQL_frm.FormActivate(Sender: TObject);
var
  BMK: Pointer;
begin
  inherited;
  //사용되는 코드들을 새로입력하고 이폼으로 돌아왔을때 코드 새로고침을 실행
  with DMCodeContents do
  begin
    CODEREFRESH;
  end;
  //리스트 갱신
  BMk := qryList.GetBookmark;
  ReadMstList;
  try
    if qryList.BookmarkValid(BMK) then
      qryList.GotoBookmark(BMK);
  finally
    qryList.FreeBookmark(BMK);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  IF DMMssql.KISConnect.InTransaction Then
    DMMssql.RollbackTrans;
  if Assigned(FSelectedRow) then
    FSelectedRow.Free;
  Action := caFree;
  APPPCR_N_MSSQL_frm := nil;
end;

procedure TAPPPCR_N_MSSQL_frm.ReadyDoc;
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
  DocBookMark: Pointer;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPPCR(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Close;
        Parameters.ParamByName('DOCID').Value := 'APPPCR';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPPCR_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        DocBookMark := qryList.GetBookmark;

        ExecSQL;

        ReadMstList;

        try
          if qryList.BookmarkValid(DocBookMark) then
          begin
            qryList.GotoBookmark(DocBookMark);
          end;
        finally
          qryList.FreeBookmark(DocBookMark);
        end;

      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_ReadyClick(Sender: TObject);
begin
  inherited;
  if qryListCHK2.AsString = '0' then
    raise Exception.Create('임시문서는 전송할 수 없습니다');
  if qryListCHK3.AsString <> '' then
  begin
//    if StrToInt(RightStr(qryListCHK3.AsString, 1)) = 5 then
//    begin
//      if ConfirmMessage('전송완료된 문서입니다. 재전송 하시겠습니까?') then
//      begin
//        Exit;
//      end;
//    end
//    else
      ReadyDoc;
  end
  else
    ReadyDoc;
end;

procedure TAPPPCR_N_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TAPPPCR_N_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qryList.Next;
end;

procedure TAPPPCR_N_MSSQL_frm.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (Sender as TStringGrid).Name = 'StringGrid1' then
  begin
    PrintRecordData(sGroupbox5, qryDetail, Arow);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.PrintRecordData(var GB: TsGroupBox; var qry: TADOQuery; Arow: Integer);
var
  nLoop: integer;
  UserEdit: TsEdit;
  UserMemo: TsMemo;
begin
  qry.First;
  qry.MoveBy(ARow - 1);

  for nLoop := 0 to GB.ControlCount - 1 do
  begin
    if GB.Controls[nLoop] is TsEdit then
    begin
      UserEdit := TsEdit(GB.Controls[nLoop]);
      if UserEdit.Hint <> '' then
      begin
        case UserEdit.Tag of
          0:
            UserEdit.Text := qry.FieldByName(UserEdit.Hint).AsString;
          1:
            UserEdit.Text := FormatFloat('#,0', qry.FieldByName(UserEdit.Hint).AsCurrency);
          2:
            UserEdit.Text := FormatFloat('#,0.000', qry.FieldByName(UserEdit.Hint).AsCurrency);
          102:
            UserEdit.Text := FormatDateTime('YYYY.MM.DD', decodeCharDate(qry.FieldByName(UserEdit.Hint).AsString));
          103:
            UserEdit.Text := HSCodeFormat(qry.FieldByName(UserEdit.Hint).AsString);
        end;
      end;
    end
    else if GB.Controls[nLoop] is TsMemo then
    begin
      UserMemo := TsMemo(GB.Controls[nLoop]);
      if UserMemo.Hint <> '' then
      begin
        UserMemo.Text := qry.FieldByName(UserMemo.Hint).AsString;
      end;
    end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryDocumentBNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString, #13#10, ' ');
end;

procedure TAPPPCR_N_MSSQL_frm.SetData;
var
  nLoop, nGLoop, MaxCount, GroupCount: Integer;
  UserEdit: TsEdit;
begin
  //수정시 필드채우기
  GroupCount := sTabSheet2.ControlCount;
  try
    for nGLoop := 0 to GroupCount - 1 do
    begin
      if sTabSheet2.Controls[nGLoop] is TsGroupBox then
      begin

        MaxCount := (sTabSheet2.Controls[nGLoop] as TsGroupBox).ControlCount;
        for nLoop := 0 to MaxCount - 1 do
        begin
          if (sTabSheet2.Controls[nGLoop] as TsGroupBox).Controls[nLoop] is TsEdit then
          begin
            UserEdit := TsEdit((sTabSheet2.Controls[nGLoop] as TsGroupBox).Controls[nLoop]);

            if UserEdit.Hint <> '' then
            begin
              case UserEdit.Tag of
                0:
                  UserEdit.Text := qryList.FieldByName(UserEdit.Hint).AsString;
                1:
                  UserEdit.Text := FormatFloat('#,0', qryList.FieldByName(UserEdit.Hint).AsCurrency);
                2:
                  UserEdit.Text := FormatFloat('#,0.000', qryList.FieldByName(UserEdit.Hint).AsCurrency);
                102:
                  begin
//                IF UserEdit.Text = '' Then Continue;
                    if qryList.FieldByName(UserEdit.Hint).AsString = '' then
                      Continue;
                    UserEdit.Text := FormatDateTime('YYYY.MM.DD', decodeCharDate(qryList.FieldByName(UserEdit.Hint).AsString));
                  end;
              end;
            end;
            UserEdit.readonly := True;
          end;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message + #13#10 + UserEdit.Name);
    end;
  end;
  AfterSetData;
end;

procedure TAPPPCR_N_MSSQL_frm.AfterSetData;
begin
  with DMCodeContents do
  begin
    //PAGE 1 신청/변경구분
    APPPCR_GUBUN.First;
    if APPPCR_GUBUN.Locate('CODE', Edt_DocumentsSection.Text, []) then
      sEdit17.Text := APPPCR_GUBUN.FieldByName('NAME').AsString
    else
      sEdit17.Text := '';

    //PAGE 1 총할인/할증/변동
    BYEONDONG_GUBUN.First;
    if BYEONDONG_GUBUN.Locate('CODE', sEdit27.Text, []) then
      sEdit28.Text := BYEONDONG_GUBUN.FieldByName('NAME').AsString
    else
      sEdit28.Text := '';

    //PAGE 1 공급물품명세구분
    NAEGUKSIN4025.First;
    if NAEGUKSIN4025.Locate('CODE', sEdit33.Text, []) then
      sEdit34.Text := NAEGUKSIN4025.FieldByName('NAME').AsString
    else
      sEdit34.Text := '';

    //구매확인서번호
    Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Edt_DocumentsSectionExit(Sender: TObject);
begin
  inherited;

  if (Sender as TsEdit).readonly then
    Exit;

  with DMCodeContents do
  begin
    if AnsiMatchText((Sender as TsEdit).Hint, ['C1_C', 'TAMT1C']) then
    begin
      TONGHWA.First;
      if TONGHWA.Locate('CODE', (Sender as TsEdit).Text, []) then
        (Sender as TsEdit).Text := TONGHWA.FieldByName('CODE').AsString
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //단위
 if AnsiMatchText((Sender as TsEdit).Hint, ['TQTYC']) then
    begin
      DANWI.First;
      if DANWI.Locate('CODE', (Sender as TsEdit).Text, []) then
        (Sender as TsEdit).Text := DANWI.FieldByName('CODE').AsString
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //문서구분
 if AnsiMatchText((Sender as TsEdit).Hint, ['ChgCd']) then
    begin
      APPPCR_GUBUN.First;
      if APPPCR_GUBUN.Locate('CODE', (Sender as TsEdit).Text, []) then
      begin
        (Sender as TsEdit).Text := APPPCR_GUBUN.FieldByName('CODE').AsString;
        sEdit17.Text := APPPCR_GUBUN.FieldByName('NAME').AsString;
        Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
      end
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //변동구분
 if AnsiMatchText((Sender as TsEdit).Hint, ['CHG_G']) then
    begin
      if (Sender as TsEdit).Text = '' then
      begin
        sEdit28.Text := '';
        sEdit31.Text := '';
        sEdit32.Text := '';
        sEdit29.Text := '';
      end
      else
      begin
        BYEONDONG_GUBUN.First;
        if BYEONDONG_GUBUN.Locate('CODE', (Sender as TsEdit).Text, []) then
        begin
          (Sender as TsEdit).Text := BYEONDONG_GUBUN.FieldByName('CODE').AsString;
          sEdit28.Text := BYEONDONG_GUBUN.FieldByName('NAME').AsString;
          sEdit31.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
          sEdit29.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
        end
        else
          Edt_DocumentsSectionDblClick(Sender);
      end;
    end
    else
    //내국신4025 - 공급물품명세구분
 if AnsiMatchText((Sender as TsEdit).Hint, ['ITM_GBN']) then
    begin
      NAEGUKSIN4025.First;
      if NAEGUKSIN4025.Locate('CODE', (Sender as TsEdit).Text, []) then
      begin
        (Sender as TsEdit).Text := NAEGUKSIN4025.FieldByName('CODE').AsString;
        sEdit34.Text := NAEGUKSIN4025.FieldByName('NAME').AsString;
      end
      else
        Edt_DocumentsSectionDblClick(Sender);
    end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Edt_DocumentsSectionDblClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsEdit).readonly then
    Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //통화
      if AnsiMatchText((Sender as TsEdit).Hint, ['C1_C', 'TAMT1C']) then
      begin
        if Run('TONGHWA') = mrOk then
          (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //단위
 if AnsiMatchText((Sender as TsEdit).Hint, ['TQTYC']) then
      begin
        if Run('DANWI') = mrOk then
          (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //문서구분
 if AnsiMatchText((Sender as TsEdit).Hint, ['ChgCd']) then
      begin
        if Run('APPPCR_GUBUN') = mrOk then
        begin
          (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          sEdit17.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
        end;
      end;
      //변동구분
      if AnsiMatchText((Sender as TsEdit).Hint, ['CHG_G']) then
      begin
        if Run('BYEONDONG_GUBUN') = mrOk then
        begin
          (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          sEdit28.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          sEdit31.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
          sEdit29.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
        end;
      end
      else
      //내국신4025 - 공급물품명세구분
 if AnsiMatchText((Sender as TsEdit).Hint, ['ITM_GBN']) then
      begin
        if Run('NAEGUKSIN4025') = mrOk then
        begin
          (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          sEdit34.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.CreateReady(bValue: Boolean);
var
  nLoop, nGLoop, MaxCount, GroupCount: Integer;
  UserEdit: TsEdit;
  tempCount : Integer;
begin
  GroupCount := sTabSheet2.ControlCount;

  //관리번호 ReadOnly
  Edt_DocNo.readonly := not (FCurrentWork = ctInsert) AND not FNewDocCopy;

  for nGLoop := 0 to GroupCount - 1 do
  begin
    if sTabSheet2.Controls[nGLoop] is TsGroupBox then
    begin

      MaxCount := (sTabSheet2.Controls[nGLoop] as TsGroupBox).ControlCount;
      for nLoop := 0 to MaxCount - 1 do
      begin
        if (sTabSheet2.Controls[nGLoop] as TsGroupBox).Controls[nLoop] is TsEdit then
        begin
          UserEdit := TsEdit((sTabSheet2.Controls[nGLoop] as TsGroupBox).Controls[nLoop]);

          if (FCurrentWork = ctinsert) and (UserEdit.Tag <> 99) then
          begin
            UserEdit.Clear;
          end;

          if UserEdit.Tag <> 99 then
            UserEdit.readonly := bValue;
        end;
      end;
    end;
  end;

  //Button State
  Btn_New.Enabled := FCurrentWork = ctView;
  Btn_Modify.Enabled := FCurrentWork = ctView;
  Btn_Del.Enabled := FCurrentWork = ctView;

  Btn_Temp.Enabled := FCurrentWork in [ctInsert, ctModify];
  Btn_Save.Enabled := FCurrentWork in [ctInsert, ctModify];
  Btn_Cancel.Enabled := FCurrentWork in [ctInsert, ctModify];

  Btn_Print.Enabled := FCurrentWork = ctView;
  Btn_Ready.Enabled := FCurrentWork = ctView;

  sButton2.Enabled := FCurrentWork in [ctInsert, ctModify];

  //SubTab button
  tempCount := 0;
  if qryDetail.Active Then
    tempCount := qryDetail.RecordCount;
  Btn_DetailAdd.Enabled := FCurrentWork in [ctInsert, ctModify];
  Btn_DetailMod.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_DetailDel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_DetailOk.Enabled     := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_DetailAdd.Enabled);
  Btn_DetailCancel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_DetailAdd.Enabled);

  tempCount := 0;
  if qryDocument.Active Then
    tempCount := qryDocument.RecordCount;
  Btn_DocAdd.Enabled := FCurrentWork in [ctInsert, ctModify];
  Btn_DocMod.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_DocDel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_DocOk.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_DocAdd.Enabled);
  Btn_DocCancel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_DocAdd.Enabled);

  tempCount := 0;
  if qryTax.Active Then
    tempCount := qryTax.RecordCount;
  Btn_TaxAdd.Enabled := FCurrentWork in [ctInsert, ctModify];
  Btn_TaxMod.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_TaxDel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0);
  Btn_TaxOk.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_TaxAdd.Enabled);
  Btn_TaxCancel.Enabled := (FCurrentWork in [ctInsert, ctModify]) and (tempCount > 0) and not(Btn_TaxAdd.Enabled);


  //Lock ListGrid
  if sPageControl1.ActivePageIndex = 0 then
    sPageControl1.ActivePageIndex := 1;

  sPageControl1.Pages[0].Enabled := FCurrentWork = ctView;
  sDBGrid1.Enabled := FCurrentWork = ctView;

  if FCurrentWork = ctInsert then
  begin
    //Set DocNo
    case FCurrentWork of
      ctInsert:
        FTempDocNo := 'APPPCR' + FormatDateTime('YYYYMMDDNNHHSS', Now);

      ctModify:
        FTempDocNo := qryListMAINT_NO.AsString;
    end;

    INIT_INSERT;

    IF DMAutoNo.GetDocumentNo('APPPCR') = '' Then
    begin
      Edt_DocNo.Text := FTempDocNo;
      FAutoNumbering := False;
      sDBEdit1.ReadOnly := False
    end
    else
    begin
      Edt_DocNo.Text  := DMAutoNo.GetDocumentNo('APPPCR');
      FAutoNumbering := True;
      sDBEdit1.ReadOnly := True;
    end;

    //자동채번일 경우에는 관리번호 변경못함
    Edt_DocNo.ReadOnly := FAutoNumbering;

    Edt_DocNo.SetFocus;

    IF FAutoNumbering THen
      Edt_DocNo.Color := clWhite
    else
    begin
      Edt_DocNo.Clear;
      Edt_DocNo.Color := clYellow;
    end;

    ReadDetail(FTempDocNo);
    ReadDocuments(FTempDocNo);
    ReadTax(FTempDocNo);
  end
  else if FCurrentWork = ctModify Then
  begin
    IF FNewDocCopy AND not FAutoNumbering Then
    begin
      Edt_DocNo.Color := clYellow;
      Edt_DocNo.Clear;
    end
    else
    begin
      Edt_DocNo.Color := clWhite;
      Edt_DocNo.ReadOnly := True;
    end;
  end
  else if FCurrentWork = ctview then
  begin
    Edt_DocNo.Color := clWhite;
    qryListAfterScroll(qryList);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.INIT_INSERT;
begin
  EdtMk_RegDate.Text := FormatDateTime('YYYYMMDD', Now);
  Com_DocumentFunc.ItemIndex := 1;
  sComboBox2.ItemIndex := 1;

  Edt_ApplicationCode.Text := '00000';
  Edt_ApplicationCodeExit(Edt_ApplicationCode);

  Edt_DocumentsSection.Text := '2CG';
  Edt_DocumentsSectionExit(Edt_DocumentsSection);
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_NewClick(Sender: TObject);
begin
  inherited;
  DMMssql.BeginTrans;
  IF (sender as TsSpeedButton).Tag = 0 Then
  begin
    dlg_APPPCR_newDocSelect_frm := Tdlg_APPPCR_newDocSelect_frm.Create(Self);
    Case dlg_APPPCR_newDocSelect_frm.ShowModal of
      mrOk : FNewDocCopy := False;
      mrYes : FNewDocCopy := True;
      mrCancel :
      begin
        DMMssql.RollbackTrans;
        Exit;
      end;
    end;

    IF FNewDocCopy Then
    begin
      Dlg_APPPCR_SelectCopyDoc_frm := TDlg_APPPCR_SelectCopyDoc_frm.Create(Self);
      if Dlg_APPPCR_SelectCopyDoc_frm.ShowModal = mrOk Then
      begin
        //제출번호 생성
        IF DMAutoNo.GetDocumentNo('APPPCR') = '' Then
        begin
          FTempDocNo := 'APPPCR' + FormatDateTime('YYYYMMDDNNHHSS', Now);
          FAutoNumbering := False;
          sDBEdit1.ReadOnly := False
        end
        else
        begin
          FTempDocNo := DMAutoNo.GetDocumentNo('APPPCR');
          FAutoNumbering := True;          
//          Edt_DocNo.Text  := DMAutoNo.GetDocumentNo('APPPCR');
          sDBEdit1.ReadOnly := True;
        end;

        //복사
        with qryCopy do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := Dlg_APPPCR_SelectCopyDoc_frm.DocNo;
          Parameters.ParamByName('NEW_DOCNO').Value := FTempDocNo;
          ExecSQL;
        end;
        //수정
        FCurrentWork := ctModify;
        ReadMstList;
        qryList.Locate('MAINT_NO',FTempDocNo,[]);
        CreateReady;
        exit;
      end
      else
      begin
        DMMssql.RollbackTrans;
        Exit;
      end;
    end
    else
    begin
      FCurrentWork := ctInsert;
      CreateReady;
    end;
  end
  else
  begin
    FAutoNumbering := False;
    FNewDocCopy := False;
  end;

  case (Sender as TsSpeedButton).Tag of
//    0:
//      begin
//        FCurrentWork := ctInsert;
//        CreateReady;
//      end;
    1:
      begin
        FCurrentWork := ctModify;
        CreateReady;
      end;
    2:
      begin
        FCurrentWork := ctDelete;
        DeleteDoc;
      end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  if not ConfirmMessage('작업을 취소하시겠습니까?'#13#10'지금까지 작성했던 데이터들은 삭제됩니다.') then
    Exit;

  FCurrentWork := ctView;
  DMMssql.RollbackTrans;
  ReadMstList;
  CreateReady(True);
end;

procedure TAPPPCR_N_MSSQL_frm.sPageControl1Changing(Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  if FCurrentWork in [ctInsert, ctModify] then
    FBeforePageIndex := sPageControl1.ActivePageIndex
end;

procedure TAPPPCR_N_MSSQL_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  if FCurrentWork in [ctInsert, ctModify] then
  begin
    if sPageControl1.ActivePageIndex = 0 then
      sPageControl1.ActivePageIndex := FBeforePageIndex;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryDetailAfterScroll(DataSet: TDataSet);
var
  bValue: boolean;
begin
  inherited;
  bValue := (qryDetail.RecordCount > 0) and (FCurrentWork in [ctInsert, ctModify]);
  Btn_DetailMod.Enabled := bValue;
  Btn_DetailDel.Enabled := bValue;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_SaveClick(Sender: TObject);
var
  LocateString : String;
begin
  inherited;
  //INSERT,MODIFY Common
  //INSERT,MODIFY Detail
  //INSERT,MODIFY Documents
  //INSERT,MODIFY Tax

  if (qryTax.State in [dsInsert, dsEdit]) then
  begin
    sPageControl1.ActivePageIndex := 4;
    raise Exception.Create('세금계산서가 아직 작성중입니다.')
  end
  else if (qryDocument.State in [dsInsert, dsEdit]) then
  begin
    sPageControl1.ActivePageIndex := 3;
    raise Exception.Create('근거서류 아직 작성중입니다.')
  end
  else if (qryDetail.State in [dsInsert, dsEdit]) then
  begin
    sPageControl1.ActivePageIndex := 2;
    raise Exception.Create('세부사항 아직 작성중입니다.')
  end
  else if Trim(Edt_DocNo.Text) = '' then
  begin
    raise Exception.Create('관리번호를 입력해주세요')
  end;

  if (Sender as TsSpeedButton).Tag = 1 then
  begin
    //누락항목 확인
     Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
     if Dlg_ErrorMessage_frm.Run_ErrorMessage( sLabel7.Caption ,CHECK_VALIDITY ) Then
     begin
       FreeAndNil(Dlg_ErrorMessage_frm);
       Exit;
     end;
      
    //임시저장
    case FCurrentWork of
      ctInsert:
        SetParamExec(qryIns, False);
      ctModify:
        SetParamExec(qrymod, False);
    end;
    FCurrentWork := ctView;

    //복사했을때에는 입력한 관리번호로 업데이트
    IF FNewDocCopy Then
      Update_Temp2NewDocNo;

    //자동채번일 경우에는 관리번호 +1
    IF FAutoNumbering Then
      DMAutoNo.RenewNo;

    DMMssql.CommitTrans;
//    IF FNewDocCopy Then
      LocateString := Edt_DocNo.Text;
//    else
//      LocateString := qryListMAINT_NO.AsString;
    ReadMstList;
    qryList.Locate('MAINT_NO',LocateString,[]);
    CreateReady(True);
  end
  else
  begin
    //임시저장
    case FCurrentWork of
      ctInsert:
        SetParamExec(qryIns, True);
      ctModify:
        SetParamExec(qrymod, True);
    end;
    FCurrentWork := ctView;

    //복사했을때에는 입력한 관리번호로 업데이트
    IF FNewDocCopy Then
      Update_Temp2NewDocNo;

    //자동채번일 경우에는 관리번호 +1
    IF FAutoNumbering Then
      DMAutoNo.RenewNo;

    DMMssql.CommitTrans;

//    IF FNewDocCopy Then
      LocateString := Edt_DocNo.Text;
//    else
//      LocateString := qryListMAINT_NO.AsString;

    ReadMstList;
    qryList.Locate('MAINT_NO',LocateString,[]);

    CreateReady(True);
//    DMAutoNo.RenewNo;
  end;

end;

procedure TAPPPCR_N_MSSQL_frm.Dialog_Detail(Sender: TsSpeedButton);
var
  UModalResult: TModalResult;
begin
  Dlg_APPPCR_Detail_frm := TDlg_APPPCR_Detail_frm.Create(Self);
  try
    case Sender.Tag of
      0:
        UModalResult := Dlg_APPPCR_Detail_frm.Run(ctInsert, qryDetail.Fields, FTempDocNo);
      1:
        UModalResult := Dlg_APPPCR_Detail_frm.Run(ctModify, qryDetail.Fields);
      2:
        begin
          if ConfirmMessage('다음 데이터를 삭제하시겠습니까?') then
          begin
            with qryDetailDel do
            begin
              Close;
              Parameters.ParamByName('KEYY').Value := qryDetailKEYY.AsString;
              Parameters.ParamByName('SEQ').Value := qryDetailSEQ.AsString;
              ExecSQL;
            end;
            UModalResult := mrOk;
          end
          else
            UModalResult := mrCancel;
        end;
    end;

    if UModalResult = mrOk then
    begin
      ReadDetail(FTempDocNo);
    end;

  finally
    FreeAndNil(Dlg_APPPCR_Detail_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Dialog_Documents(Sender: TsSpeedButton);
var
  UModalResult: TModalResult;
begin
  Dlg_APPPCR_Documents_frm := TDlg_APPPCR_Documents_frm.Create(Self);
  try
    case Sender.Tag of
      0:
        UModalResult := Dlg_APPPCR_Documents_frm.Run(ctInsert, qryDocument.Fields, FTempDocNo);
      1:
        UModalResult := Dlg_APPPCR_Documents_frm.Run(ctModify, qryDocument.Fields);
      2:
        begin
          if ConfirmMessage('다음 데이터를 삭제하시겠습니까?') then
          begin
            with qryDetailDel do
            begin
              Close;
              Parameters.ParamByName('KEYY').Value := qryDetailKEYY.AsString;
              Parameters.ParamByName('SEQ').Value := qryDetailSEQ.AsString;
              ExecSQL;
            end;
            UModalResult := mrOk;
          end
          else
            UModalResult := mrCancel;
        end;
    end;

    if UModalResult = mrOk then
    begin
      ReadDocuments(FTempDocNo);
    end;

  finally
    FreeAndNil(Dlg_APPPCR_Documents_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Dialog_Tax(Sender: TsSpeedButton);
var
  UModalResult: TModalResult;
begin

end;

procedure TAPPPCR_N_MSSQL_frm.sEdit39DblClick(Sender: TObject);
begin
  inherited;
  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      if Run = mrOK then
      begin
        sEdit39.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        sEdit35.Text := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        sEdit37.Text := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_TaxAddClick(Sender: TObject);
begin
  inherited;
  case AnsiIndexText((Sender as TsSpeedButton).Name, ['Btn_TaxAdd', 'Btn_TaxMod', 'Btn_TaxDel', 'Btn_TaxOk', 'Btn_TaxCancel']) of
    0:
      qryTax.Append;
    1:
      qryTax.Edit;
    2:
      begin
        if ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'삭제후 저장시에는 복구가 불가능합니다.') then
        begin
          qryTax.Delete;
        end;
      end;
    3:
      qryTax.Post;
    4:
      qryTax.Cancel;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryTaxAfterInsert(DataSet: TDataSet);
begin
  inherited;
  qryTaxAfterEdit(DataSet);
  IF FNewDocCopy Then
    DataSet.FieldByName('KEYY').AsString := FTempDocNo
  else
    DataSet.FieldByName('KEYY').AsString := Edt_DocNo.Text;
//  DataSet.FieldByName('SEQ').AsInteger := Dataset.RecordCount + 1;
  DataSet.FieldByName('SEQ').AsInteger := MAX_SEQ('APPPCR_TAX');
end;

procedure TAPPPCR_N_MSSQL_frm.qryTaxAfterEdit(DataSet: TDataSet);
begin
  inherited;
  GroupReadOnly(sGroupbox7, DataSet.State = dsBrowse);

  Btn_TaxAdd.Enabled := DataSet.State = dsBrowse;
  Btn_TaxMod.Enabled := DataSet.State = dsBrowse;
  Btn_TaxDel.Enabled := DataSet.State = dsBrowse;
  Btn_TaxOk.Enabled := DataSet.State in [dsInsert, dsEdit];
  Btn_TaxCancel.Enabled := DataSet.State in [dsInsert, dsEdit];
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit53Enter(Sender: TObject);
begin
  inherited;
  if (Sender as TsDBEdit).DataSource.DataSet.State in [dsInsert, dsEdit] then
  begin
    (Sender as TsDBEdit).Color := clYellow;
  end
  else
    (Sender as TsDBEdit).Color := clWhite;

  (Sender as TsDBedit).Repaint;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit53Exit(Sender: TObject);
begin
  inherited;
  (Sender as TsDBEdit).Color := clWhite;
  (Sender as TsDBedit).Repaint;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit60DblClick(Sender: TObject);
var
  sDataField: string;
begin
  inherited;
  if (Sender as TsDBEdit).readonly or ((Sender as TsDBEdit).DataSource.DataSet.State = dsBrowse) then
    Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //Get FieldName
      sDataField := (Sender as TsDBEdit).DataField;

      //통화
      if AnsiMatchText(sDataField, ['BILL_AMOUNT_UNIT', 'TAX_AMOUNT_UNIT']) then
      begin
        if Run('TONGHWA') = mrOk then
          (Sender as TsDBEdit).DataSource.DataSet.FieldByName(sDataField).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //단위
 if AnsiMatchText((Sender as TsDBEdit).DataField, ['QUANTITY_UNIT']) then
      begin
        if Run('DANWI') = mrOk then
          (Sender as TsDBEdit).DataSource.DataSet.FieldByName(sDataField).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit60Exit(Sender: TObject);
var
  sDataField: string;
  TempField: TField;
begin
  inherited;
  if (Sender as TsDBEdit).readonly or ((Sender as TsDBEdit).DataSource.DataSet.State = dsBrowse) then
    Exit;

  with DMCodeContents do
  begin
    //Get FieldName
    sDataField := (Sender as TsDBEdit).DataField;
    TempField := (Sender as TsDBEdit).DataSource.DataSet.FieldByName(sDataField);

    if AnsiMatchText(sDataField, ['BILL_AMOUNT_UNIT', 'TAX_AMOUNT_UNIT']) then
    begin
      TONGHWA.First;
      if TONGHWA.Locate('CODE', TempField.AsString, []) then
        TempField.AsString := TONGHWA.FieldByName('CODE').AsString
      else
        sDBEdit60DblClick(Sender);
    end
    else if AnsiMatchText((Sender as TsDBEdit).DataField, ['QUANTITY_UNIT']) then
    begin
      DANWI.First;
      if DANWI.Locate('CODE', TempField.AsString, []) then
        TempField.AsString := DANWI.FieldByName('CODE').AsString
      else
        sDBEdit60DblClick(Sender);
    end
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit60KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F8 then
    sDBEdit60DblClick(Sender);
end;

procedure TAPPPCR_N_MSSQL_frm.GroupReadOnly(var GroupBox: TsGroupBox; bValue: Boolean);
var
  nLoop, MaxCount: integer;
  UserDBEdit: TsDBEdit;
  UserDBMemo: TsDBMemo;
begin
  MaxCount := GroupBox.ControlCount;
  for nLoop := 0 to MaxCount - 1 do
  begin
    if GroupBox.Controls[nLoop] is TsDBEdit then
    begin
      UserDBEdit := TsDBEdit(GroupBox.Controls[nLoop]);
      UserDBEdit.readonly := bValue;
    end
    else if GroupBox.Controls[nLoop] is TsDBMemo then
    begin
      UserDBMemo := TsDBMemo(GroupBox.Controls[nLoop]);
      UserDBMemo.readonly := bValue;
    end;

  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryDocumentAfterEdit(DataSet: TDataSet);
begin
  inherited;
  GroupReadOnly(sGroupbox6, DataSet.State = dsBrowse);

  Btn_DocAdd.Enabled := DataSet.State = dsBrowse;
  Btn_DocMod.Enabled := DataSet.State = dsBrowse;
  Btn_DocDel.Enabled := DataSet.State = dsBrowse;
  Btn_DocOk.Enabled := DataSet.State in [dsInsert, dsEdit];
  Btn_DocCancel.Enabled := DataSet.State in [dsInsert, dsEdit];
end;

procedure TAPPPCR_N_MSSQL_frm.qryDocumentAfterInsert(DataSet: TDataSet);
begin
  inherited;
  qryDocumentAfterEdit(DataSet);
  IF FNewDocCopy Then
    DataSet.FieldByName('KEYY').AsString := FTempDocNo
  else
    DataSet.FieldByName('KEYY').AsString := Edt_DocNo.Text;
//  DataSet.FieldByName('SEQ').AsInteger := Dataset.RecordCount + 1;
  DataSet.FieldByName('SEQ').AsInteger := MAX_SEQ('APPPCRD2');
end;

procedure TAPPPCR_N_MSSQL_frm.qryDocumentAfterOpen(DataSet: TDataSet);
begin
  inherited;
  qryDocument.Properties['Unique Table'].Value := 'APPPCRD2';
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit48DblClick(Sender: TObject);
var
  sDataField: string;
  TempField: TField;
begin
  inherited;
  if (Sender as TsDBEdit).readonly or ((Sender as TsDBEdit).DataSource.DataSet.State = dsBrowse) then
    Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //Get FieldName
      sDataField := (Sender as TsDBEdit).DataField;
      TempField := (Sender as TsDBEdit).DataSource.DataSet.FieldByName(sDataField);

      //통화
      if AnsiMatchText(sDataField, ['BAMTC']) then
      begin
        if Run('TONGHWA') = mrOk then
          TempField.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //문서구분
     if AnsiMatchText(sDataField, ['DOC_G']) then
      begin
        if Run('APPPCR_DOCUMENTS') = mrOk then
        begin
          TempField.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          Pan_DocNO.Caption := DataSource1.DataSet.FieldByName('NAME').AsString;
        end;
      end;
      //국가코드
      if AnsiMatchText(sDataField, ['NAT']) then
      begin
        if Run('NATION') = mrOk then
        begin
          TempField.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
        end;
      end;

    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit51DblClick(Sender: TObject);
begin
  inherited;
  if not (qryDocument.State in [dsInsert, dsEdit]) then
    Exit;

  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      if Run = mrOK then
      begin
//        Edt_BAL_CD.Text     := DataSource1.DataSet.FieldByName('CODE').AsString;
//        Edt_BAL_NAME1.Text  := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
//        Edt_BAL_NAME2.Text  := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
        qryDocumentBAL_CD.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
        qryDocumentBAL_NAME1.AsString := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        qryDocumentBAL_NAME2.AsString := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit65DblClick(Sender: TObject);
var
  TempDataSet: TDataSet;
begin
  inherited;

  IF (Sender as TsDBEdit).DataSource = dsDetail Then
  begin
    if not (qryDetail.State in [dsInsert, dsEdit]) then
      Exit;

    Dlg_FindJepum_frm := TDlg_FindJepum_frm.Create(Self);
    try

      if Dlg_FindJepum_frm.Run(qryDetailNAMESS.AsString) = mrOk then
      begin
        TempDataSet := Dlg_FindJepum_frm.dsITEM.DataSet;
        qryDetailHS_NO.AsString := TempDataset.FieldByName('HSCODE').AsString;
        qryDetailNAMESS.AsString := TempDataset.FieldByName('CODE').AsString;
        qryDetailNAME1.AsString := TempDataset.FieldByName('FNAME').AsString;
        qryDetailSIZE.AsString := TempDataset.FieldByName('MSIZE').AsString;
      end;
    finally
      FreeAndNil(Dlg_FindJepum_frm);
    end;
  end
  else
  IF (Sender as TsDBEdit).DataSource = dsDocument Then
  begin
    if not (qryDocument.State in [dsInsert, dsEdit]) then
      Exit;

    Dlg_FindJepum_frm := TDlg_FindJepum_frm.Create(Self);
    try

      if Dlg_FindJepum_frm.Run(qryDocumentBNAME.AsString) = mrOk then
      begin
        TempDataSet := Dlg_FindJepum_frm.dsITEM.DataSet;
        qryDocumentBHS_NO.AsString := TempDataset.FieldByName('HSCODE').AsString;
        qryDocumentBNAME.AsString := TempDataset.FieldByName('CODE').AsString;
        qryDocumentBNAME1.AsString := TempDataset.FieldByName('FNAME').AsString;
        qryDocumentBSIZE1.AsString := TempDataset.FieldByName('MSIZE').AsString;
      end;
    finally
      FreeAndNil(Dlg_FindJepum_frm);
    end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_DocAddClick(Sender: TObject);
begin
  inherited;

  if Edt_DocNo.Text = '' then
    raise Exception.Create('관리번호를 입력하세요');

  case AnsiIndexText((Sender as TsSpeedButton).Name, ['Btn_DocAdd', 'Btn_DocMod', 'Btn_DocDel', 'Btn_DocOk', 'Btn_DocCancel']) of
    0:
      qryDocument.Append;
    1:
      qryDocument.Edit;
    2:
      begin
        if ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'삭제후 저장시에는 복구가 불가능합니다.') then
        begin
          qryDocument.Delete;
        end;
      end;
    3:
      qryDocument.Post;
    4:
      qryDocument.Cancel;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_DetailAddClick(Sender: TObject);
begin
  inherited;

  if Edt_DocNo.Text = '' then
    raise Exception.Create('관리번호를 입력하세요');

  case AnsiIndexText((Sender as TsSpeedButton).Name, ['Btn_DetailAdd', 'Btn_DetailMod', 'Btn_DetailDel', 'Btn_DetailOk', 'Btn_DetailCancel']) of
    0:
      qryDetail.Append;
    1:
      qryDetail.Edit;
    2:
      begin
        if ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'삭제후 저장시에는 복구가 불가능합니다.') then
        begin
          qryDetail.Delete;
        end;
      end;
    3:
      qryDetail.Post;
    4:
      qryDetail.Cancel;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.qryDetailAfterEdit(DataSet: TDataSet);
begin
  inherited;
  GroupReadOnly(sGroupbox5, DataSet.State = dsBrowse);

  Btn_DetailAdd.Enabled := DataSet.State = dsBrowse;
  Btn_DetailMod.Enabled := DataSet.State = dsBrowse;
  Btn_DetailDel.Enabled := DataSet.State = dsBrowse;
  Btn_DetailOk.Enabled := DataSet.State in [dsInsert, dsEdit];
  Btn_DetailCancel.Enabled := DataSet.State in [dsInsert, dsEdit];
end;

procedure TAPPPCR_N_MSSQL_frm.qryDetailAfterInsert(DataSet: TDataSet);
begin
  inherited;
  qryDetailAfterEdit(DataSet);
  IF FNewDocCopy Then
    DataSet.FieldByName('KEYY').AsString := FTempDocNo
  else
    DataSet.FieldByName('KEYY').AsString := Edt_DocNo.Text;
//  DataSet.FieldByName('SEQ').AsInteger := Dataset.RecordCount + 1;
  DataSet.FieldByName('SEQ').AsInteger := MAX_SEQ('APPPCRD1');
end;

procedure TAPPPCR_N_MSSQL_frm.Edt_ApplicationCodeDblClick(Sender: TObject);
begin
  inherited;
  if not (FCurrentWork in [ctinsert, ctModify]) then
    Exit;

  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      if Run = mrOK then
      begin
        if (Sender as TsEdit).Hint = 'APP_CODE' then
        begin
          Edt_ApplicationCode.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          Edt_ApplicationName1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
          Edt_ApplicationName2.Text := '';
          Edt_ApplicationAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          Edt_ApplicationAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          Edt_ApplicationAddr3.Text := DataSource1.DataSet.FieldByName('ADDR3').AsString;
          Edt_ApplicationAXNAME1.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          Edt_ApplicationSAUPNO.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
          Edt_ApplicationElectronicSign.Text := DataSource1.DataSet.FieldByName('Jenja').AsString;
        end
        else if (Sender as TsEdit).Hint = 'SUP_CODE' then
        begin
          Edt_SupplyCode.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          Edt_SupplyName.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
          Edt_SupplyCEO.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          Edt_SupplyEID.Text := DataSource1.DataSet.FieldByName('EMAIL_ID').AsString;
          Edt_SupplyEID2.Text := DataSource1.DataSet.FieldByName('EMAIL_DOMAIN').AsString;
          Edt_SupplyAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          Edt_SupplyAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          Edt_SupplySaupNo.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sEdit39Exit(Sender: TObject);
begin
  inherited;
  if Trim(sEdit39.Text) = '' then
  begin
    sEdit39.Clear;
    sEdit35.Clear;
    sEdit37.Clear;
    Exit;
  end;

  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      if not ExistsValue(sEdit39.Text) then
      begin
        if Run = mrOK then
        begin
          sEdit39.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          sEdit35.Text := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
          sEdit37.Text := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
        end;
      end
      else
      begin
        sEdit39.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        sEdit35.Text := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        sEdit37.Text := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.SetParamExec(var qry: TADOQuery; bTemp: Boolean);
begin
  with qry do
  begin
    IF FNewDocCopy Then
      Parameters.ParamByName('MAINT_NO').Value := FTempDocNo
    else
      Parameters.ParamByName('MAINT_NO').Value := Edt_DocNo.Text;

    if FCurrentWork = ctInsert then
      Parameters.ParamByName('USER_ID').Value := LoginData.sID;

    Parameters.ParamByName('DATEE').Value := FormatDateTime('YYYYMMDD', Now);
    Parameters.ParamByName('CHK1').Value := '';
    //0 임시, 1문서
    if bTemp then
      Parameters.ParamByName('CHK2').Value := 0
    else
      Parameters.ParamByName('CHK2').Value := 1;

    Parameters.ParamByName('CHK3').Value := '';
    Parameters.ParamByName('MESSAGE1').Value := LeftStr(Com_DocumentFunc.Text, 1);
    Parameters.ParamByName('MESSAGE2').Value := sComboBox2.Text;
    Parameters.ParamByName('APP_CODE').Value := Edt_ApplicationCode.Text;
    Parameters.ParamByName('APP_NAME1').Value := Edt_ApplicationName1.Text;
    Parameters.ParamByName('APP_NAME2').Value := Edt_ApplicationName2.Text;
    Parameters.ParamByName('APP_NAME3').Value := '';
    Parameters.ParamByName('APP_ADDR1').Value := Edt_ApplicationAddr1.Text;
    Parameters.ParamByName('APP_ADDR2').Value := Edt_ApplicationAddr2.Text;
    Parameters.ParamByName('APP_ADDR3').Value := Edt_ApplicationAddr3.Text;
    Parameters.ParamByName('AX_NAME1').Value := Edt_ApplicationAXNAME1.Text;
    Parameters.ParamByName('AX_NAME2').Value := Edt_ApplicationSAUPNO.Text;
    Parameters.ParamByName('AX_NAME3').Value := Edt_ApplicationElectronicSign.Text;
    Parameters.ParamByName('SUP_CODE').Value := Edt_SupplyCode.Text;
    Parameters.ParamByName('SUP_NAME1').Value := Edt_SupplyName.Text;
    Parameters.ParamByName('SUP_NAME2').Value := Edt_SupplyCEO.Text;
    Parameters.ParamByName('SUP_NAME3').Value := '';
    Parameters.ParamByName('SUP_ADDR1').Value := Edt_SupplyAddr1.Text;
    Parameters.ParamByName('SUP_ADDR2').Value := Edt_SupplyAddr2.Text;
    Parameters.ParamByName('SUP_ADDR3').Value := Edt_SupplySaupNo.Text;
    Parameters.ParamByName('ITM_G1').Value := '';
    Parameters.ParamByName('ITM_G2').Value := '';
    Parameters.ParamByName('ITM_G3').Value := '';
    Parameters.ParamByName('TQTY').Value := StrToCurrDef(AnsiReplaceText(sEdit41.Text, ',', ''),0);
    Parameters.ParamByName('TQTYC').Value := sEdit36.Text;
    Parameters.ParamByName('TAMT1').Value := StrToCurrDef(AnsiReplaceText(sEdit42.Text, ',', ''), 0);
    Parameters.ParamByName('TAMT1C').Value := sEdit40.Text;
    Parameters.ParamByName('TAMT2').Value := StrToCurrDef(AnsiReplaceText(sEdit43.Text, ',', ''), 0);
    Parameters.ParamByName('TAMT2C').Value := 'USD';
    Parameters.ParamByName('CHG_G').Value := sEdit27.Text;
    Parameters.ParamByName('C1_AMT').Value := StrToCurrDef(sEdit31.Text, 0);
    Parameters.ParamByName('C1_C').Value := sEdit32.Text;
    Parameters.ParamByName('C2_AMT').Value := StrToCurrDef(sEdit29.Text, 0);
    Parameters.ParamByName('BK_NAME1').Value := sEdit35.Text;
    Parameters.ParamByName('BK_NAME2').Value := sEdit37.Text;
    Parameters.ParamByName('PRNO').Value := 0;
    Parameters.ParamByName('PCrLic_No').Value := Edt_BuyConfirmDocuments.Text;
    Parameters.ParamByName('ChgCd').Value := Edt_DocumentsSection.Text;
    Parameters.ParamByName('APP_DATE').Value := AnsiReplaceText( AnsiReplaceText( Edt_ApplicationAPPDATE.Text , '.','') , '-', '');
//    Parameters.ParamByName('DOC_G'        ).Value :=
//    Parameters.ParamByName('DOC_D'        ).Value :=
//    Parameters.ParamByName('BHS_NO'       ).Value :=
//    Parameters.ParamByName('BNAME'        ).Value :=
//    Parameters.ParamByName('BNAME1'       ).Value :=
//    Parameters.ParamByName('BAMT'         ).Value :=
//    Parameters.ParamByName('BAMTC'        ).Value :=
//    Parameters.ParamByName('EXP_DATE'     ).Value :=
//    Parameters.ParamByName('SHIP_DATE'    ).Value :=
//    Parameters.ParamByName('BSIZE1'       ).Value :=
//    Parameters.ParamByName('BAL_CD'       ).Value :=
//    Parameters.ParamByName('BAL_NAME1'    ).Value :=
//    Parameters.ParamByName('BAL_NAME2'    ).Value :=
//    Parameters.ParamByName('F_INTERFACE'  ).Value :=
//    Parameters.ParamByName('SRBUHO'       ).Value :=
    Parameters.ParamByName('ITM_GBN').Value := sEdit33.Text;
    Parameters.ParamByName('ITM_GNAME').Value := '';
//    Parameters.ParamByName('ISSUE_GBN'    ).Value :=
//    Parameters.ParamByName('DOC_GBN'      ).Value :=
//    Parameters.ParamByName('DOC_NO'       ).Value :=
//    Parameters.ParamByName('BAMT2'        ).Value :=
//    Parameters.ParamByName('BAMTC2'       ).Value :=
//    Parameters.ParamByName('BAMT3'        ).Value :=
//    Parameters.ParamByName('BAMTC3'       ).Value :=
//    Parameters.ParamByName('PCRLIC_ISS_NO').Value :=
//    Parameters.ParamByName('FIN_CODE'     ).Value :=
//    Parameters.ParamByName('FIN_NAME'     ).Value :=
//    Parameters.ParamByName('FIN_NAME2'    ).Value :=
//    Parameters.ParamByName('NEGO_APPDT'   ).Value :=
    Parameters.ParamByName('EMAIL_ID').Value := Edt_SupplyEID.Text;
    Parameters.ParamByName('EMAIL_DOMAIN').Value := Edt_SupplyEID2.Text;
    Parameters.ParamByName('BK_CD').Value := sEdit39.Text;
    ExecSQL;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Edt_ApplicationCodeExit(Sender: TObject);
begin
  inherited;
  if not (FCurrentWork in [ctinsert, ctModify]) then
    Exit;

  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      if isValue((Sender as TsEdit).Text) then
      begin
        Edt_ApplicationCode.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        Edt_ApplicationName1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
//        Edt_ApplicationName2.Text := '';
        Edt_ApplicationName2.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        Edt_ApplicationAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
        Edt_ApplicationAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
        Edt_ApplicationAddr3.Text := DataSource1.DataSet.FieldByName('ADDR3').AsString;
        Edt_ApplicationAXNAME1.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        Edt_ApplicationSAUPNO.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        Edt_ApplicationElectronicSign.Text := DataSource1.DataSet.FieldByName('Jenja').AsString;
      end
      else
        Edt_ApplicationCodeDblClick(Edt_ApplicationCode);
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TAPPPCR_N_MSSQL_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;
//  APPPCR_PRINT_FRM.DocNo := qryListMAINT_NO.AsString;
//  APPPCR_PRINT_FRM.Preview;

//  APPPCR_PRINT1_FRM.DocNo := qryListMAINT_NO.AsString;
//  APPPCR_PRINT1_FRM.Preview;
  APPPCR_PRINT_FRM := TAPPPCR_PRINT_FRM.Create(Self);
  APPPCR_PRINT1_FRM := TAPPPCR_PRINT1_FRM.Create(Self);
  APPPCR_PRINT2_FRM := TAPPPCR_PRINT2_FRM.Create(Self);
  try
    APPPCR_PRINT_FRM.DocNo := qryListMAINT_NO.AsString;
    APPPCR_PRINT1_FRM.DocNo := qryListMAINT_NO.AsString;
    APPPCR_PRINT2_FRM.DocNo := qryListMAINT_NO.AsString;
    QRCompositeReport1.Prepare;
    APPPCR_PRINT2_FRM.Totalpage := APPPCR_PRINT2_FRM.QRPrinter.PageCount;
    QRCompositeReport1.Preview;
  finally
    FreeAndNil(APPPCR_PRINT_FRM);
    FreeAndNil(APPPCR_PRINT1_FRM);
    FreeAndNil(APPPCR_PRINT2_FRM);
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(APPPCR_PRINT_FRM);
  QRCompositeReport1.Reports.Add(APPPCR_PRINT1_FRM);
  QRCompositeReport1.Reports.Add(APPPCR_PRINT2_FRM);
end;

procedure TAPPPCR_N_MSSQL_frm.sPanel1Click(Sender: TObject);
var
  Int: Integer;
  Temp: AnsiString;
begin
  inherited;
//  Temp := AnsiString('①');

//  for Int := 1 to Length(Temp) do
//    ShowMessage(IntToHex(Ord(Temp[int]),2));
end;

procedure TAPPPCR_N_MSSQL_frm.sSpeedButton6Click(Sender: TObject);
begin
  inherited;
  sDBEdit65DblClick(sDBedit33);
end;

procedure TAPPPCR_N_MSSQL_frm.sSpeedButton7Click(Sender: TObject);
begin
  inherited;
  sDBEdit65DblClick(sDBEdit65);
end;

procedure TAPPPCR_N_MSSQL_frm.DeleteDoc;
begin
  IF RightStr(qryListCHK3.AsString, 1) = '5' Then
  begin
    DMMssql.CommitTrans;
    Raise Exception.Create('송신완료된 데이터는 삭제 할 수 없습니다');
  end;

  IF ConfirmMessage('해당 데이터를 삭제하시겠습니까? 데이터 복구는 불가능합니다.') Then
  begin
    with qryDel do
    begin
      Close;
      Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
      ExecSQL;
      Close;
    end;
    ReadMstList;
    DMMssql.CommitTrans;    
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit34DblClick(Sender: TObject);
begin
  inherited;
   if (Sender as TsDBEdit).readonly or ((Sender as TsDBEdit).DataSource.DataSet.State = dsBrowse) then Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['QTYC','PRI_BASEC']) Then
      begin
          IF Run('DANWI') = mrOk Then
          begin
            (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          end;
      end
      else
      IF AnsiMatchText((Sender as TsDBEdit).DataField,['AMT1C','AC1_C','BAMTC']) Then
      begin
          IF Run('TONGHWA') = mrOk Then
          begin
            (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          end;
      end
      else
      IF  AnsiMatchText((Sender as TsDBEdit).DataField,['ACHG_G']) Then
      begin
          IF Run('BYEONDONG_GUBUN') = mrOk Then
          begin
            (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          end;
      end
      else
      //문서구분
      IF  AnsiMatchText((Sender as TsDBEdit).DataField,['DOC_G']) Then
      begin
        if Run('APPPCR_DOCUMENTS') = mrOk then
        begin
          (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
          Pan_DocNO.Caption := DataSource1.DataSet.FieldByName('NAME').AsString;
        end;
      end
      else
      //국가코드
      IF  AnsiMatchText((Sender as TsDBEdit).DataField,['NAT']) Then
      begin
        if Run('NATION') = mrOk then
        begin
          (Sender as TsDBEdit).DataSource.DataSet.FieldByName( (Sender as TsDBEdit).DataField ).AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;
procedure TAPPPCR_N_MSSQL_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_F1 then
  begin
    IF (ActiveControl is TsDBEdit) AND Assigned( (ActiveControl as  TsDBEdit).OnDblClick )  then
    begin
        (ActiveControl as  TsDBEdit).OnDblClick((ActiveControl as  TsDBEdit));
    end
    else
    IF (ActiveControl is TsEdit) AND Assigned( (ActiveControl as  TsEdit).OnDblClick ) then
    begin
        (ActiveControl as  TsEdit).OnDblClick((ActiveControl as  TsEdit));
    end;
  end;

  case Key of
    VK_INSERT : Btn_NewClick(Btn_New);
    VK_DELETE : Btn_NewClick(Btn_Del);
  end;

end;

function TAPPPCR_N_MSSQL_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;

begin
  ErrMsg := TStringList.Create;

  Try
    //공통사항 확인
    IF Trim(Edt_DocumentsSection.Text) = '' THEN
      ErrMsg.Add('[공통] 신청/변경 구분을 입력해야 합니다');

    IF (Trim(Edt_DocumentsSection.Text) = '2CH') AND (Trim(Edt_BuyConfirmDocuments.Text) = '') THEN
      ErrMsg.Add('[공통] 구매(공급)확인서 번호가 없습니다');

    IF Trim(Edt_SupplyName.Text) = '' THEN
      ErrMsg.Add('[공통] 공급자가 없습니다');

    if Trim(Edt_SupplySaupNo.Text) = '' Then
      ErrMsg.Add('[공통] 공급자 사업자등록번호가 없습니다');

    if Trim(sEdit33.Text) = '' Then
      ErrMsg.Add('[공통] 공급물품 명세구분이 없습니다');

    if Trim(sEdit39.Text) = '' then
      ErrMsg.Add('[공통] 확인기관(코드)이 없습니다');

    if Trim(sEdit35.Text) = '' then
      ErrMsg.Add('[공통] 확인기관명이 없습니다');

    if Trim(sEdit41.Text) = '' Then
      ErrMsg.Add('[공통] 총 수량이 없습니다');

    if Trim(sEdit36.Text) = '' Then
      ErrMsg.Add('[공통] 총 수량의 단위가 없습니다');

    //구매물품 확인-------------------------------------------------------------
    IF qryDetail.RecordCount = 0 THEN
    begin
      ErrMsg.Add('[세부사항] 상세내역의 구매물품은 1개이상 입력해야 합니다');
    end
    else
    begin
      qryDetail.First;
      //공급물품 데이터 유효성 검사
      while not qryDetail.Eof do
      begin
        IF (Trim(qryDetailNAME1.AsString) = '') OR qryDetailNAME1.IsNull Then
          ErrMsg.Add('[세부사항] ['+IntToStr(qryDetail.RecNo)+']행의 구매물품 품목은 필수 입력 입니다');

        IF (Trim(qryDetailHS_NO.AsString) = '') OR qryDetailHS_NO.IsNull Then
          ErrMsg.Add('[세부사항] ['+IntToStr(qryDetail.RecNo)+']행의 HS부호은 필수 입력 입니다');

        IF (qryDetailQTYC.AsString <> '') AND ( qryDetailQTY.IsNull OR (qryDetailQTY.AsCurrency = 0) ) Then
          ErrMsg.Add('[세부사항] ['+IntToStr(qryDetail.RecNo)+']행의 수량을 입력해야합니다');


        IF (Trim(qryDetailAPP_DATE.AsString) = '') OR qryDetailAPP_DATE.IsNull Then
          ErrMsg.Add('[세부사항] ['+IntToStr(qryDetail.RecNo)+']행의 구매(공급)일은 필수 입력 입니다')
        else
        begin
          nYear := StrToInt(LeftStr(qryDetailAPP_DATE.AsString,4));
          nMonth := StrToInt(MidStr(qryDetailAPP_DATE.AsString,5,2));
          nDay := StrToInt(RightStr(qryDetailAPP_DATE.AsString,2));

          IF not IsValidDate(nYear,nMonth,nDay) then
            ErrMsg.Add('[세부사항] ['+IntToStr(qryDetail.RecNo)+']행의 구매(공급)일을 확인하세요');
        end;
        qryDetail.Next;
      end;
      qryDetail.First;
    end;

    //근거서류 확인-------------------------------------------------------------
    if qryDocument.RecordCount = 0 Then
      ErrMsg.Add('[근거서류] 근거서류는 1개이상 입력해야 합니다')
    else
    begin
      qryDocument.First;
      //공급물품 데이터 유효성 검사
      while not qryDocument.Eof do
      begin
        IF (Trim(qryDocumentBAMTC.AsString) = '') OR qryDocumentBAMTC.IsNull Then
          ErrMsg.Add('[근거서류] ['+IntToStr(qryDocument.RecNo)+']행의 총금액은 필수 입력입니다');

        IF (Trim(qryDocumentSHIP_DATE.AsString) = '') OR qryDocumentSHIP_DATE.IsNull Then
          ErrMsg.Add('[근거서류] ['+IntToStr(qryDocument.RecNo)+']행의 선적기일은 필수 입력입니다');
        qryDocument.Next;
      end;
      qryDocument.First;
    end;
    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.Update_Temp2NewDocNo;
begin
  with qry_UpdateTempNo2NewNo do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := Edt_DocNo.Text;
    Parameters.ParamByName('TEMP_NO' ).Value := FTempDocNo;
    ExecSQL;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit43Exit(Sender: TObject);
begin
  inherited;
  IF qryDetail.State in [dsInsert, dsEdit] Then
    qryDetailAMT1.AsCurrency := qryDetailQTY.AsCurrency * qryDetailPRI2.AsCurrency;  
end;

procedure TAPPPCR_N_MSSQL_frm.sButton2Click(Sender: TObject);
begin
  inherited;

  IF qryDetail.State in [dsInsert, dsEdit] Then
  begin
    ShowMessage('세부사항이 입력중입니다. 저장 후에 진행하세요');
    Exit;
  end;

  with qryCalcTotal do
  begin
    Close;
    Parameters.ParamByName('KEY').Value := qryListMAINT_NO.AsString;
    Open;
    try
      IF qryCalcTotal.RecordCount = 0 Then
      begin
        sEdit41.Text := '0';
        sEdit36.Text := '0';
        sEdit42.Text := '0';
        sEdit40.Text := '0';
      end
      else
      begin
        sEdit41.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TQTY').AsCurrency);
        sEdit36.Text := qryCalcTotal.FieldByName('QTYC').AsString;
        sEdit42.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TAMT1').AsCurrency);
        sEdit40.Text := qryCalcTotal.FieldByName('AMT1C').AsString;

        IF sEdit40.Text = 'USD' Then
          sEdit43.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TAMT1').AsCurrency);

      end;
    finally
      qryCalcTotal.Close;
    end;
  end;
end;

procedure TAPPPCR_N_MSSQL_frm.sDBEdit45Enter(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBEdit).DataSource.DataSet.State in [dsInsert, dsEdit] Then
    (Sender as TsDBEdit).Text := AnsiReplaceText((Sender as TsDBEdit).Text,',','');
end;

function TAPPPCR_N_MSSQL_frm.MAX_SEQ(TableName: String): Integer;
begin
  with qryMAX_SEQ do
  begin
    Close;
    SQL.Text := 'SELECT MAX(SEQ) as MAX_SEQ FROM '+TableName+' WHERE KEYY = :KEYY';
    Parameters.ParamByName('KEYY').Value := qryListMAINT_NO.AsString;
    Open;

    Result := FieldByName('MAX_SEQ').AsInteger + 1;
  end;
end;

end.

