inherited UI_DOMOFB_NEW_frm: TUI_DOMOFB_NEW_frm
  Left = 651
  Top = 210
  BorderWidth = 4
  Caption = '[DOMOFB] ('#44060#49444#51088')'#44397#45236#48156#54665#47932#54408#47588#46020#54869#50557#49436' - '#49688#49888
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 148
      Height = 17
      Caption = #44397#45236#48156#54665#47932#54408' '#47588#46020#54869#50557#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 50
      Height = 13
      Caption = 'DOMOFB'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 160
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 173
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 239
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 315
    Height = 632
    Align = alLeft
    TabOrder = 1
    SkinData.SkinSection = 'TRANSPARENT'
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 313
      Height = 574
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 189
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 90
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 313
      Height = 56
      Align = alTop
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 230
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 57
        Top = 29
        Width = 171
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 57
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 150
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 241
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 315
    Top = 41
    Width = 799
    Height = 632
    Align = alClient
    TabOrder = 2
    SkinData.SkinSection = 'TRANSPARENT'
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      TabOrder = 0
      SkinData.SkinSection = 'TRANSPARENT'
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 797
      Height = 575
      ActivePage = sTabSheet1
      Align = alClient
      TabHeight = 26
      TabIndex = 0
      TabOrder = 1
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 789
          Height = 539
          Align = alClient
          TabOrder = 0
          SkinData.SkinSection = 'TRANSPARENT'
          object sSpeedButton10: TsSpeedButton
            Left = 342
            Top = 5
            Width = 11
            Height = 524
            ButtonStyle = tbsDivider
          end
          object sButton1: TsButton
            Left = 262
            Top = 473
            Width = 67
            Height = 43
            Cursor = crHandPoint
            Caption = #44228#49328
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 50
            TabStop = False
            Visible = False
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object msk_pubdt: TsMaskEdit
            Left = 64
            Top = 8
            Width = 89
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#54665#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object edt_pubno: TsEdit
            Left = 64
            Top = 32
            Width = 265
            Height = 23
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48156#54665#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_pubcd: TsEdit
            Tag = 101
            Left = 64
            Top = 64
            Width = 49
            Height = 23
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#54665#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_tradeno: TsEdit
            Left = 208
            Top = 64
            Width = 121
            Height = 23
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #47924#50669#45824#47532#51216#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_pubnm: TsEdit
            Left = 64
            Top = 88
            Width = 265
            Height = 23
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_pubceo: TsEdit
            Left = 64
            Top = 112
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_pubsign: TsEdit
            Left = 64
            Top = 136
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_pubaddr1: TsEdit
            Left = 64
            Top = 160
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_pubaddr2: TsEdit
            Left = 64
            Top = 184
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 8
            SkinData.CustomColor = True
          end
          object edt_pubaddr3: TsEdit
            Left = 64
            Top = 208
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 9
            SkinData.CustomColor = True
          end
          object edt_demCd: TsEdit
            Tag = 102
            Left = 64
            Top = 240
            Width = 49
            Height = 23
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#50836#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demSaupno: TsEdit
            Left = 208
            Top = 240
            Width = 121
            Height = 23
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demNm: TsEdit
            Left = 64
            Top = 264
            Width = 265
            Height = 23
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demCeo: TsEdit
            Left = 64
            Top = 288
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demSign: TsEdit
            Left = 64
            Top = 312
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demAddr1: TsEdit
            Left = 64
            Top = 336
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_demAddr2: TsEdit
            Left = 64
            Top = 360
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 16
            SkinData.CustomColor = True
          end
          object edt_demAddr3: TsEdit
            Left = 64
            Top = 384
            Width = 265
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 17
            SkinData.CustomColor = True
          end
          object edt_demRef: TsEdit
            Left = 152
            Top = 416
            Width = 177
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #52280#51312#48264#54840'['#49688#50836#51088']'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_HSCD: TsEdit
            Left = 152
            Top = 440
            Width = 177
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            ReadOnly = True
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#44277#44553' HS'#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sPanel8: TsPanel
            Left = 365
            Top = 4
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #50976#54952#44592#44036
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 20
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_eff1: TsEdit
            Left = 365
            Top = 27
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_eff2: TsEdit
            Left = 365
            Top = 47
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 22
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_eff3: TsEdit
            Left = 365
            Top = 67
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_eff4: TsEdit
            Left = 365
            Top = 87
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_eff5: TsEdit
            Left = 365
            Top = 107
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 25
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_chk1: TsEdit
            Left = 365
            Top = 160
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 26
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_chk2: TsEdit
            Left = 365
            Top = 180
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_chk3: TsEdit
            Left = 365
            Top = 200
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 28
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_chk4: TsEdit
            Left = 365
            Top = 220
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 29
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_chk5: TsEdit
            Left = 365
            Top = 240
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 30
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sPanel9: TsPanel
            Left = 365
            Top = 136
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #44160#49324#48169#48277
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 31
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_pkg1: TsEdit
            Left = 365
            Top = 292
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 32
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pkg2: TsEdit
            Left = 365
            Top = 312
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 33
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pkg3: TsEdit
            Left = 365
            Top = 332
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 34
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pkg4: TsEdit
            Left = 365
            Top = 352
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 35
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pkg5: TsEdit
            Left = 365
            Top = 372
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sPanel10: TsPanel
            Left = 365
            Top = 269
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #54252#51109#48169#48277
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 37
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel11: TsPanel
            Left = 365
            Top = 401
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #44208#51228#48169#48277
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 38
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_pay1: TsEdit
            Left = 365
            Top = 424
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 39
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pay2: TsEdit
            Left = 365
            Top = 444
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 40
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pay3: TsEdit
            Left = 365
            Top = 464
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 41
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pay4: TsEdit
            Left = 365
            Top = 484
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 42
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_pay5: TsEdit
            Left = 365
            Top = 504
            Width = 415
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 43
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_TQTYCUR: TsEdit
            Tag = 508
            Left = 132
            Top = 473
            Width = 36
            Height = 21
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 44
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn24: TsBitBtn
            Tag = 15
            Left = 169
            Top = 495
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 45
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_TQTY: TsCurrencyEdit
            Left = 192
            Top = 473
            Width = 137
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 46
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_TAMT: TsCurrencyEdit
            Left = 192
            Top = 495
            Width = 137
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 47
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_TAMTCUR: TsEdit
            Tag = 509
            Left = 132
            Top = 495
            Width = 36
            Height = 21
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 48
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn25: TsBitBtn
            Tag = 14
            Left = 169
            Top = 473
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 49
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = #50896#49328#51648'/'#49440#51201'/'#46020#52265
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 789
          Height = 539
          Align = alClient
          TabOrder = 0
          SkinData.SkinSection = 'TRANSPARENT'
          object edt_ori1: TsEdit
            Left = 5
            Top = 29
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Caption = #50868#49569#48169#48277
          end
          object edt_ori1nm: TsEdit
            Left = 61
            Top = 29
            Width = 239
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
          end
          object sPanel12: TsPanel
            Left = 5
            Top = 4
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #50896#49328#51648#44397#44032'/'#51648#48169#47749
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 2
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object btnOri1: TsBitBtn
            Left = 37
            Top = 29
            Width = 23
            Height = 23
            Cursor = crHandPoint
            TabOrder = 3
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_ori2: TsEdit
            Tag = 1
            Left = 5
            Top = 51
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Caption = #50868#49569#48169#48277
          end
          object edt_ori2nm: TsEdit
            Left = 61
            Top = 51
            Width = 239
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
          end
          object btnOri2: TsBitBtn
            Tag = 1
            Left = 37
            Top = 51
            Width = 23
            Height = 23
            Cursor = crHandPoint
            TabOrder = 6
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_ori3: TsEdit
            Tag = 2
            Left = 5
            Top = 73
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Caption = #50868#49569#48169#48277
          end
          object edt_ori3nm: TsEdit
            Left = 61
            Top = 73
            Width = 239
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 8
          end
          object btnOri3: TsBitBtn
            Tag = 2
            Left = 37
            Top = 73
            Width = 23
            Height = 23
            Cursor = crHandPoint
            TabOrder = 9
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_ori4: TsEdit
            Tag = 3
            Left = 5
            Top = 95
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Caption = #50868#49569#48169#48277
          end
          object edt_ori4nm: TsEdit
            Left = 61
            Top = 95
            Width = 239
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 11
          end
          object btnOri4: TsBitBtn
            Tag = 3
            Left = 37
            Top = 95
            Width = 23
            Height = 23
            Cursor = crHandPoint
            TabOrder = 12
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_ori5: TsEdit
            Tag = 4
            Left = 5
            Top = 117
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Caption = #50868#49569#48169#48277
          end
          object edt_ori5nm: TsEdit
            Left = 61
            Top = 117
            Width = 239
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 17
            ParentFont = False
            ReadOnly = True
            TabOrder = 14
          end
          object btnOri5: TsBitBtn
            Tag = 4
            Left = 37
            Top = 117
            Width = 23
            Height = 23
            Cursor = crHandPoint
            TabOrder = 15
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sPanel13: TsPanel
            Left = 309
            Top = 4
            Width = 118
            Height = 24
            BevelOuter = bvNone
            Caption = #44592#53440#52280#51312#49324#54637
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 16
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object mm_remark1: TsMemo
            Left = 309
            Top = 29
            Width = 468
            Height = 111
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 17
            CharCase = ecUpperCase
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_Trans: TsEdit
            Tag = 401
            Left = 135
            Top = 151
            Width = 34
            Height = 18
            Hint = #50868#49569#49688#45800
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 18
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn14: TsBitBtn
            Tag = 5
            Left = 168
            Top = 151
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 19
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_TransNm: TsEdit
            Left = 191
            Top = 151
            Width = 192
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clBtnFace
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 20
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_shipNat: TsEdit
            Tag = 402
            Left = 135
            Top = 181
            Width = 34
            Height = 18
            Hint = #44397#44032
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 21
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#51201#44397#44032'/'#49440#51201#54637
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn13: TsBitBtn
            Tag = 6
            Left = 168
            Top = 181
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_shipPort: TsEdit
            Left = 191
            Top = 181
            Width = 192
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_LOADTXT1: TsEdit
            Left = 135
            Top = 203
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#51201#49884#44592
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_LOADTXT2: TsEdit
            Left = 135
            Top = 223
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 25
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_LOADTXT3: TsEdit
            Left = 135
            Top = 243
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 26
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_LOADTXT4: TsEdit
            Left = 135
            Top = 263
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_LOADTXT5: TsEdit
            Left = 135
            Top = 283
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 28
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_destNat: TsEdit
            Tag = 403
            Left = 135
            Top = 312
            Width = 34
            Height = 18
            Hint = #44397#44032
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 29
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46020#52265#44397#44032'/'#46020#52265#54637
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn16: TsBitBtn
            Tag = 7
            Left = 170
            Top = 312
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 30
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_destPort: TsEdit
            Left = 193
            Top = 312
            Width = 190
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 31
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_DESTTXT1: TsEdit
            Left = 135
            Top = 335
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 32
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46020#52265#49884#44592
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DESTTXT2: TsEdit
            Left = 135
            Top = 355
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 33
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_DESTTXT3: TsEdit
            Left = 135
            Top = 375
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 34
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_DESTTXT4: TsEdit
            Left = 135
            Top = 395
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 35
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_DESTTXT5: TsEdit
            Left = 135
            Top = 415
            Width = 640
            Height = 18
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sPanel14: TsPanel
            Left = 5
            Top = 151
            Width = 129
            Height = 21
            BevelOuter = bvNone
            Caption = #50868#49569#48169#48277
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 37
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel15: TsPanel
            Left = 5
            Top = 312
            Width = 129
            Height = 21
            BevelOuter = bvNone
            Caption = #46020#52265#44397#44032'/'#46020#52265#54637
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 38
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel18: TsPanel
            Left = 5
            Top = 203
            Width = 129
            Height = 101
            BevelOuter = bvNone
            Caption = #49440#51201#49884#44592
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 39
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel16: TsPanel
            Left = 5
            Top = 181
            Width = 129
            Height = 21
            BevelOuter = bvNone
            Caption = #49440#51201#44397#44032'/'#49440#51201#54637
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 40
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel19: TsPanel
            Left = 5
            Top = 334
            Width = 129
            Height = 101
            BevelOuter = bvNone
            Caption = #46020#52265#49884#44592
            Color = 16765090
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 41
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = #49345#54408#45236#50669
        object sPanel17: TsPanel
          Left = 0
          Top = 299
          Width = 789
          Height = 240
          Align = alClient
          TabOrder = 0
          object sSpeedButton11: TsSpeedButton
            Left = 462
            Top = 9
            Width = 11
            Height = 221
            ButtonStyle = tbsDivider
          end
          object sPanel22: TsPanel
            Left = 272
            Top = 7
            Width = 97
            Height = 23
            Caption = 'HS'#53076#46300
            Color = 16765090
            TabOrder = 1
            SkinData.CustomColor = True
          end
          object sPanel23: TsPanel
            Left = 8
            Top = 119
            Width = 57
            Height = 23
            Caption = #44508#44201
            Color = 16765090
            TabOrder = 2
            SkinData.CustomColor = True
          end
          object sDBEdit1: TsDBEdit
            Tag = 501
            Left = 152
            Top = 7
            Width = 121
            Height = 23
            Hint = #54408#47749
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'NAME_COD'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit2: TsDBEdit
            Left = 368
            Top = 7
            Width = 90
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'HS_NO'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS NO'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBMemo1: TsDBMemo
            Left = 8
            Top = 29
            Width = 450
            Height = 91
            Color = clWhite
            Ctl3D = True
            DataField = 'NAME1'
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeftTop
            CharCase = ecUpperCase
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBMemo2: TsDBMemo
            Left = 8
            Top = 141
            Width = 450
            Height = 90
            Color = clWhite
            Ctl3D = True
            DataField = 'SIZE1'
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 6
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeftTop
            CharCase = ecUpperCase
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit3: TsDBEdit
            Tag = 502
            Left = 558
            Top = 35
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'QTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn17: TsBitBtn
            Tag = 8
            Left = 595
            Top = 35
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 8
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit4: TsDBEdit
            Left = 618
            Top = 35
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 9
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit6: TsDBEdit
            Left = 618
            Top = 59
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'PRICE'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn18: TsBitBtn
            Tag = 9
            Left = 595
            Top = 59
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 20
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit5: TsDBEdit
            Tag = 503
            Left = 558
            Top = 59
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'PRICE_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 10
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sDBEdit7: TsDBEdit
            Tag = 504
            Left = 558
            Top = 83
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'QTYG_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sBitBtn19: TsBitBtn
            Tag = 10
            Left = 595
            Top = 83
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 21
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit8: TsDBEdit
            Left = 618
            Top = 83
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTYG'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 13
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit10: TsDBEdit
            Left = 618
            Top = 107
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 15
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn20: TsBitBtn
            Tag = 11
            Left = 595
            Top = 107
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit9: TsDBEdit
            Tag = 505
            Left = 558
            Top = 107
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'AMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sDBEdit11: TsDBEdit
            Tag = 506
            Left = 558
            Top = 154
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'STQTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 16
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn21: TsBitBtn
            Tag = 12
            Left = 595
            Top = 154
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 23
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit12: TsDBEdit
            Left = 618
            Top = 154
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STQTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 17
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit14: TsDBEdit
            Left = 618
            Top = 178
            Width = 153
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STAMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn23: TsBitBtn
            Tag = 13
            Left = 595
            Top = 178
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 24
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit13: TsDBEdit
            Tag = 507
            Left = 558
            Top = 178
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'STAMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 18
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn26: TsButton
            Left = 558
            Top = 131
            Width = 213
            Height = 21
            Cursor = crHandPoint
            Caption = #44228#49328
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 25
            TabStop = False
            Visible = False
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object sPanel21: TsPanel
            Left = 96
            Top = 7
            Width = 57
            Height = 23
            Caption = #54408#47749
            Color = 16765090
            TabOrder = 0
            SkinData.CustomColor = True
          end
          object sPanel28: TsPanel
            Left = 8
            Top = 7
            Width = 49
            Height = 23
            Caption = #49692#48264
            Color = 16765090
            TabOrder = 26
            SkinData.CustomColor = True
          end
          object sDBEdit15: TsDBEdit
            Tag = 501
            Left = 56
            Top = 7
            Width = 41
            Height = 23
            Hint = #54408#47749
            TabStop = False
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            DataField = 'SEQ'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
        end
        object sPanel25: TsPanel
          Left = 0
          Top = 0
          Width = 789
          Height = 299
          Align = alTop
          TabOrder = 1
          object sDBGrid2: TsDBGrid
            Left = 1
            Top = 30
            Width = 787
            Height = 268
            Align = alClient
            Color = clWhite
            Ctl3D = False
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NAME_COD'
                Title.Alignment = taCenter
                Title.Caption = #54408#47749#53076#46300
                Width = 60
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'HS_NO'
                Title.Alignment = taCenter
                Title.Caption = 'HS'#48264#54840
                Width = 100
                Visible = True
              end
              item
                Color = 15132415
                Expanded = False
                FieldName = 'QTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 15132415
                Expanded = False
                FieldName = 'QTY_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 38
                Visible = True
              end
              item
                Color = 14417919
                Expanded = False
                FieldName = 'PRICE'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 14417919
                Expanded = False
                FieldName = 'PRICE_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 38
                Visible = True
              end
              item
                Color = 16772829
                Expanded = False
                FieldName = 'AMT'
                Title.Alignment = taCenter
                Title.Caption = #44552#50529
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 16772829
                Expanded = False
                FieldName = 'AMT_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 38
                Visible = True
              end
              item
                Color = 15531993
                Expanded = False
                FieldName = 'STQTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049#49548#44228
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 15531993
                Expanded = False
                FieldName = 'STQTY_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 38
                Visible = True
              end
              item
                Color = 15720447
                Expanded = False
                FieldName = 'STAMT'
                Title.Alignment = taCenter
                Title.Caption = #44552#50529#49548#44228
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 15720447
                Expanded = False
                FieldName = 'STAMT_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 38
                Visible = True
              end>
          end
          object sPanel26: TsPanel
            Left = 1
            Top = 1
            Width = 787
            Height = 29
            Align = alTop
            TabOrder = 1
            Visible = False
            SkinData.SkinSection = 'TRANSPARENT'
            object sSpeedButton13: TsSpeedButton
              Left = 162
              Top = 4
              Width = 14
              Height = 22
              ButtonStyle = tbsDivider
            end
            object btnDnew: TsBitBtn
              Left = 0
              Top = 2
              Width = 53
              Height = 25
              Cursor = crHandPoint
              Caption = #51077#47141
              Enabled = False
              TabOrder = 0
              TabStop = False
              ImageIndex = 2
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnDEdit: TsBitBtn
              Tag = 1
              Left = 54
              Top = 2
              Width = 53
              Height = 25
              Cursor = crHandPoint
              Caption = #49688#51221
              Enabled = False
              TabOrder = 1
              TabStop = False
              ImageIndex = 3
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnDDel: TsBitBtn
              Tag = 2
              Left = 108
              Top = 2
              Width = 53
              Height = 25
              Cursor = crHandPoint
              Caption = #49325#51228
              Enabled = False
              TabOrder = 2
              TabStop = False
              ImageIndex = 1
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnDok: TsBitBtn
              Tag = 3
              Left = 177
              Top = 2
              Width = 53
              Height = 25
              Cursor = crHandPoint
              Caption = #51200#51109
              Enabled = False
              TabOrder = 3
              TabStop = False
              ImageIndex = 17
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnDcancel: TsBitBtn
              Tag = 4
              Left = 231
              Top = 2
              Width = 53
              Height = 25
              Cursor = crHandPoint
              Caption = #52712#49548
              Enabled = False
              TabOrder = 4
              TabStop = False
              ImageIndex = 18
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnImport: TsBitBtn
              Tag = 4
              Left = 636
              Top = 2
              Width = 81
              Height = 25
              Cursor = crHandPoint
              Hint = #50641#49472#50640#49436' '#51228#54408#45236#50669#51012' '#44032#51256#50741#45768#45796
              Caption = #44032#51256#50724#44592
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 5
              TabStop = False
              ImageIndex = 32
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
            object btnSample: TsBitBtn
              Tag = 4
              Left = 720
              Top = 2
              Width = 57
              Height = 25
              Cursor = crHandPoint
              Hint = #50641#49472#50640#49436' '#51228#54408#45236#50669#51012' '#44032#51256#50741#45768#45796
              Caption = #49368#54540
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 6
              TabStop = False
              ImageIndex = 33
              Images = DMICON.System18
              SkinData.SkinSection = 'TRANSPARENT'
            end
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 789
          Height = 507
          Align = alClient
          Color = clGray
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = 12582911
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_SQ1'
              Title.Alignment = taCenter
              Title.Caption = #50976#54952#44592#44036
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_DATE'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_NO'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#48264#54840
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'UD_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49688#50836#51088
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TQTY'
              Title.Alignment = taCenter
              Title.Caption = #52509#49688#47049
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TQTYCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TAMT'
              Title.Alignment = taCenter
              Title.Caption = #52509#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TAMTCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51088
              Width = 50
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 789
          Height = 32
          Align = alTop
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 1
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 797
      Height = 55
      Align = alTop
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 334
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 265
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object com_func: TsComboBox
        Left = 400
        Top = 4
        Width = 121
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 3
        ParentFont = False
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 400
        Top = 28
        Width = 211
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 2
        ParentFont = False
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 272
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
    end
    object sPanel30: TsPanel
      Left = 408
      Top = 58
      Width = 388
      Height = 27
      TabOrder = 3
      Visible = False
      object sLabel3: TsLabel
        Left = 3
        Top = 6
        Width = 136
        Height = 15
        Caption = #44397#45236#48156#54665#47932#54408' '#47588#46020#54869#50557#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 0
    Top = 56
  end
  object qryList: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * ,'
      '      TRNS_NAME.DOC_NAME as TRANSNME,'
      '      LOADD_NAME.DOC_NAME as LOADDNAME,'
      '      DEST_NAME.DOC_NAME as DESTNAME'
      #9#9'  '#9#9#9#9'   '
      ' FROM DOMOFB_H1 AS H1'
      ''
      'INNER JOIN DOMOFB_H2 AS H2 ON H1.MAINT_NO = H2.MAINT_NO '
      'INNER JOIN DOMOFB_H3 AS H3 ON H1.MAINT_NO = H3.MAINT_NO'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#50868#49569#49688#45800#39') TRNS_NAME ON H3.TRNS_ID = TRNS_NAME.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADD_NAME ON H3.LOADD = LOADD_NAME.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') DEST_NAME ON H3.DEST = DEST_NAME.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(H1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))')
    Left = 48
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMaint_Rff: TStringField
      FieldName = 'Maint_Rff'
      Size = 35
    end
    object qryListChk1: TStringField
      FieldName = 'Chk1'
      Size = 1
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListSR_CODE: TStringField
      FieldName = 'SR_CODE'
      Size = 10
    end
    object qryListSR_NO: TStringField
      FieldName = 'SR_NO'
      Size = 17
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 10
    end
    object qryListSR_ADDR1: TStringField
      FieldName = 'SR_ADDR1'
      Size = 35
    end
    object qryListSR_ADDR2: TStringField
      FieldName = 'SR_ADDR2'
      Size = 35
    end
    object qryListSR_ADDR3: TStringField
      FieldName = 'SR_ADDR3'
      Size = 35
    end
    object qryListUD_CODE: TStringField
      FieldName = 'UD_CODE'
      Size = 10
    end
    object qryListUD_RENO: TStringField
      FieldName = 'UD_RENO'
      Size = 35
    end
    object qryListUD_NO: TStringField
      FieldName = 'UD_NO'
      Size = 10
    end
    object qryListUD_NAME1: TStringField
      FieldName = 'UD_NAME1'
      Size = 35
    end
    object qryListUD_NAME2: TStringField
      FieldName = 'UD_NAME2'
      Size = 35
    end
    object qryListUD_NAME3: TStringField
      FieldName = 'UD_NAME3'
      Size = 10
    end
    object qryListUD_ADDR1: TStringField
      FieldName = 'UD_ADDR1'
      Size = 35
    end
    object qryListUD_ADDR2: TStringField
      FieldName = 'UD_ADDR2'
      Size = 35
    end
    object qryListUD_ADDR3: TStringField
      FieldName = 'UD_ADDR3'
      Size = 35
    end
    object qryListOFR_NO: TStringField
      FieldName = 'OFR_NO'
      Size = 35
    end
    object qryListOFR_DATE: TStringField
      FieldName = 'OFR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListOFR_SQ: TStringField
      FieldName = 'OFR_SQ'
      Size = 1
    end
    object qryListOFR_SQ1: TStringField
      FieldName = 'OFR_SQ1'
      Size = 70
    end
    object qryListOFR_SQ2: TStringField
      FieldName = 'OFR_SQ2'
      Size = 70
    end
    object qryListOFR_SQ3: TStringField
      FieldName = 'OFR_SQ3'
      Size = 70
    end
    object qryListOFR_SQ4: TStringField
      FieldName = 'OFR_SQ4'
      Size = 70
    end
    object qryListOFR_SQ5: TStringField
      FieldName = 'OFR_SQ5'
      Size = 70
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK_1: TMemoField
      FieldName = 'REMARK_1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListTSTINST: TStringField
      FieldName = 'TSTINST'
      Size = 1
    end
    object qryListTSTINST1: TStringField
      FieldName = 'TSTINST1'
      Size = 70
    end
    object qryListTSTINST2: TStringField
      FieldName = 'TSTINST2'
      Size = 70
    end
    object qryListTSTINST3: TStringField
      FieldName = 'TSTINST3'
      Size = 70
    end
    object qryListTSTINST4: TStringField
      FieldName = 'TSTINST4'
      Size = 70
    end
    object qryListTSTINST5: TStringField
      FieldName = 'TSTINST5'
      Size = 70
    end
    object qryListPCKINST: TStringField
      FieldName = 'PCKINST'
      Size = 1
    end
    object qryListPCKINST1: TStringField
      FieldName = 'PCKINST1'
      Size = 70
    end
    object qryListPCKINST2: TStringField
      FieldName = 'PCKINST2'
      Size = 70
    end
    object qryListPCKINST3: TStringField
      FieldName = 'PCKINST3'
      Size = 70
    end
    object qryListPCKINST4: TStringField
      FieldName = 'PCKINST4'
      Size = 70
    end
    object qryListPCKINST5: TStringField
      FieldName = 'PCKINST5'
      Size = 70
    end
    object qryListPAY: TStringField
      FieldName = 'PAY'
      Size = 3
    end
    object qryListPAY_ETC: TStringField
      FieldName = 'PAY_ETC'
      Size = 1
    end
    object qryListPAY_ETC1: TStringField
      FieldName = 'PAY_ETC1'
      Size = 70
    end
    object qryListPAY_ETC2: TStringField
      FieldName = 'PAY_ETC2'
      Size = 70
    end
    object qryListPAY_ETC3: TStringField
      FieldName = 'PAY_ETC3'
      Size = 70
    end
    object qryListPAY_ETC4: TStringField
      FieldName = 'PAY_ETC4'
      Size = 70
    end
    object qryListPAY_ETC5: TStringField
      FieldName = 'PAY_ETC5'
      Size = 70
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListTERM_DEL: TStringField
      FieldName = 'TERM_DEL'
      Size = 3
    end
    object qryListTERM_NAT: TStringField
      FieldName = 'TERM_NAT'
      Size = 3
    end
    object qryListTERM_LOC: TStringField
      FieldName = 'TERM_LOC'
      Size = 70
    end
    object qryListORGN1N: TStringField
      FieldName = 'ORGN1N'
      Size = 3
    end
    object qryListORGN1LOC: TStringField
      FieldName = 'ORGN1LOC'
      Size = 70
    end
    object qryListORGN2N: TStringField
      FieldName = 'ORGN2N'
      Size = 3
    end
    object qryListORGN2LOC: TStringField
      FieldName = 'ORGN2LOC'
      Size = 70
    end
    object qryListORGN3N: TStringField
      FieldName = 'ORGN3N'
      Size = 3
    end
    object qryListORGN3LOC: TStringField
      FieldName = 'ORGN3LOC'
      Size = 70
    end
    object qryListORGN4N: TStringField
      FieldName = 'ORGN4N'
      Size = 3
    end
    object qryListORGN4LOC: TStringField
      FieldName = 'ORGN4LOC'
      Size = 70
    end
    object qryListORGN5N: TStringField
      FieldName = 'ORGN5N'
      Size = 3
    end
    object qryListORGN5LOC: TStringField
      FieldName = 'ORGN5LOC'
      Size = 70
    end
    object qryListTRNS_ID: TStringField
      FieldName = 'TRNS_ID'
      Size = 3
    end
    object qryListLOADD: TStringField
      FieldName = 'LOADD'
      Size = 3
    end
    object qryListLOADLOC: TStringField
      FieldName = 'LOADLOC'
      Size = 70
    end
    object qryListLOADTXT: TStringField
      FieldName = 'LOADTXT'
      Size = 1
    end
    object qryListLOADTXT1: TStringField
      FieldName = 'LOADTXT1'
      Size = 70
    end
    object qryListLOADTXT2: TStringField
      FieldName = 'LOADTXT2'
      Size = 70
    end
    object qryListLOADTXT3: TStringField
      FieldName = 'LOADTXT3'
      Size = 70
    end
    object qryListLOADTXT4: TStringField
      FieldName = 'LOADTXT4'
      Size = 70
    end
    object qryListLOADTXT5: TStringField
      FieldName = 'LOADTXT5'
      Size = 70
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTLOC: TStringField
      FieldName = 'DESTLOC'
      Size = 70
    end
    object qryListDESTTXT: TStringField
      FieldName = 'DESTTXT'
      Size = 1
    end
    object qryListDESTTXT1: TStringField
      FieldName = 'DESTTXT1'
      Size = 70
    end
    object qryListDESTTXT2: TStringField
      FieldName = 'DESTTXT2'
      Size = 70
    end
    object qryListDESTTXT3: TStringField
      FieldName = 'DESTTXT3'
      Size = 70
    end
    object qryListDESTTXT4: TStringField
      FieldName = 'DESTTXT4'
      Size = 70
    end
    object qryListDESTTXT5: TStringField
      FieldName = 'DESTTXT5'
      Size = 70
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,##0.####;'
      EditFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTQTYCUR: TStringField
      FieldName = 'TQTYCUR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTAMTCUR: TStringField
      FieldName = 'TAMTCUR'
      Size = 3
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      Size = 100
    end
    object qryListCODE_1: TStringField
      FieldName = 'CODE_1'
      Size = 12
    end
    object qryListDOC_NAME_1: TStringField
      FieldName = 'DOC_NAME_1'
      Size = 100
    end
    object qryListCODE_2: TStringField
      FieldName = 'CODE_2'
      Size = 12
    end
    object qryListDOC_NAME_2: TStringField
      FieldName = 'DOC_NAME_2'
      Size = 100
    end
    object qryListTRANSNME: TStringField
      FieldName = 'TRANSNME'
      Size = 100
    end
    object qryListLOADDNAME: TStringField
      FieldName = 'LOADDNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListHS_CODE: TStringField
      FieldName = 'HS_CODE'
      Size = 10
    end
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM DOMOFB_D'
      'WHERE KEYY = :KEYY')
    Left = 48
    Top = 224
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryDetailNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryDetailNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryDetailQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryDetailQTYG: TBCDField
      FieldName = 'QTYG'
      Precision = 18
    end
    object qryDetailQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryDetailPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryDetailPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryDetailAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryDetailAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryDetailSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryDetailSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryDetailSTAMT: TBCDField
      FieldName = 'STAMT'
      Precision = 18
    end
    object qryDetailSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 80
    Top = 224
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 192
  end
end
