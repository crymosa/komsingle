inherited UI_APP700_NEW_frm: TUI_APP700_NEW_frm
  Left = 623
  Top = 114
  BorderWidth = 4
  Caption = '[APP700]'#52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
  ClientHeight = 673
  ClientWidth = 1062
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1062
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 825
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 546
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 6
      Width = 187
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 21
      Width = 40
      Height = 13
      Caption = 'APP700'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 471
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton7: TsSpeedButton
      Left = 750
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton8: TsSpeedButton
      Left = 272
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 986
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 278
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 342
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Tag = 2
      Left = 406
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 759
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 4
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 552
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 5
      TabStop = False
      OnClick = btnTempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 618
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = btnTempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Tag = 2
      Left = 684
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 834
      Top = 3
      Width = 93
      Height = 35
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 8
      TabStop = False
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 920
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 9
      TabStop = False
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 478
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 10
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object sButton13: TsButton
      Left = 205
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = 'ERP'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      TabStop = False
      OnClick = sButton13Click
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 341
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 574
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 175
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 86
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #51652#54665
          Width = 43
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = sMaskEdit1Change
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 1
      Top = 57
      Width = 339
      Height = 574
      Align = alClient
      
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 55
        Top = 257
        Width = 230
        Height = 21
        Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 55
        Top = 281
        Width = 126
        Height = 12
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 41
    Width = 721
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 719
      Height = 575
      ActivePage = sTabSheet3
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabIndex = 1
      TabOrder = 1
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sSpeedButton10: TsSpeedButton
            Left = 352
            Top = 4
            Width = 11
            Height = 352
            ButtonStyle = tbsDivider
          end
          object msk_APP_DATE: TsMaskEdit
            Left = 93
            Top = 30
            Width = 79
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#49888#52397#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object edt_IN_MATHOD: TsEdit
            Tag = 1000
            Left = 93
            Top = 54
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 2
            TabOrder = 1
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#48169#48277
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_IN_MATHOD_NM: TsEdit
            Left = 143
            Top = 54
            Width = 161
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd1: TsEdit
            Tag = 1003
            Left = 2
            Top = 386
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 23
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd1_NM: TsEdit
            Left = 52
            Top = 386
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 43
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd2: TsEdit
            Tag = 1004
            Left = 2
            Top = 408
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 27
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd2_NM: TsEdit
            Left = 52
            Top = 408
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 44
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd3: TsEdit
            Tag = 1005
            Left = 2
            Top = 430
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 31
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd3_NM: TsEdit
            Left = 52
            Top = 430
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 45
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd4: TsEdit
            Tag = 1006
            Left = 2
            Top = 452
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 35
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd4_NM: TsEdit
            Left = 52
            Top = 452
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 46
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd5: TsEdit
            Tag = 1007
            Left = 2
            Top = 474
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 39
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ImpCd5_NM: TsEdit
            Left = 52
            Top = 474
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 47
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdPay: TsEdit
            Tag = 1002
            Left = 93
            Top = 257
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 10
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_IN_MATHODExit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#50857#44277#50668
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdPay1: TsEdit
            Left = 143
            Top = 257
            Width = 201
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 48
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank: TsEdit
            Tag = 1001
            Left = 93
            Top = 78
            Width = 49
            Height = 23
            Hint = '4'#51088#47532'('#48376#51216'+'#51648#51216')'
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 4
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '*'#44060#49444#51032#47280#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ApBank1: TsEdit
            Left = 143
            Top = 78
            Width = 161
            Height = 23
            Color = clWhite
            MaxLength = 70
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank3: TsEdit
            Left = 93
            Top = 102
            Width = 211
            Height = 23
            Color = clWhite
            MaxLength = 70
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank: TsEdit
            Tag = 1100
            Left = 93
            Top = 185
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 6
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '('#55148#47581')'#53685#51648#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank1: TsEdit
            Left = 143
            Top = 185
            Width = 161
            Height = 23
            Color = clWhite
            MaxLength = 70
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank3: TsEdit
            Left = 93
            Top = 209
            Width = 211
            Height = 23
            Color = clWhite
            MaxLength = 70
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank_BIC: TsEdit
            Tag = 1101
            Left = 93
            Top = 233
            Width = 92
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 9
            OnDblClick = edt_IN_MATHODDblClick
            OnExit = edt_AdBank_BICExit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'BIC CODE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo1: TsEdit
            Left = 445
            Top = 30
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44592#53440#51221#48372
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo2: TsEdit
            Left = 445
            Top = 52
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo5: TsEdit
            Left = 445
            Top = 118
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo3: TsEdit
            Left = 445
            Top = 74
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo4: TsEdit
            Left = 445
            Top = 96
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit2: TsEdit
            Left = 186
            Top = 233
            Width = 158
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILno1: TsEdit
            Left = 255
            Top = 386
            Width = 256
            Height = 23
            Color = clWhite
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'I/L'#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILno2: TsEdit
            Left = 255
            Top = 408
            Width = 256
            Height = 23
            Color = clWhite
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILno5: TsEdit
            Left = 255
            Top = 474
            Width = 256
            Height = 23
            Color = clWhite
            TabOrder = 40
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILno3: TsEdit
            Left = 255
            Top = 430
            Width = 256
            Height = 23
            Color = clWhite
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILno4: TsEdit
            Left = 255
            Top = 452
            Width = 256
            Height = 23
            Color = clWhite
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel2: TsPanel
            Left = 2
            Top = 5
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 50
          end
          object sPanel8: TsPanel
            Left = 2
            Top = 160
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #53685#51648#51008#54665'('#51008#54665#47749', BICCODE '#46168#51473' '#54616#45208#49324#50857')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 51
          end
          object sPanel9: TsPanel
            Left = 2
            Top = 361
            Width = 252
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#51077#50857#46020
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 52
          end
          object sPanel10: TsPanel
            Left = 368
            Top = 5
            Width = 328
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44592#53440#51221#48372
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 53
          end
          object sPanel11: TsPanel
            Left = 255
            Top = 361
            Width = 256
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = 'I/L'#48264#54840
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 54
          end
          object edt_ILCur1: TsEdit
            Tag = 1009
            Left = 512
            Top = 386
            Width = 42
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 25
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_ILCur1'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur2: TsEdit
            Tag = 1010
            Left = 512
            Top = 408
            Width = 42
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 29
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur5: TsEdit
            Tag = 1013
            Left = 512
            Top = 474
            Width = 42
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 41
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur3: TsEdit
            Tag = 1011
            Left = 512
            Top = 430
            Width = 42
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 33
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur4: TsEdit
            Tag = 1012
            Left = 512
            Top = 452
            Width = 42
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 37
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILAMT1: TsCurrencyEdit
            Left = 555
            Top = 386
            Width = 141
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 26
            DisplayFormat = '#,0.00;-#,0.00;0'
          end
          object edt_ILAMT2: TsCurrencyEdit
            Left = 555
            Top = 408
            Width = 141
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 30
            DisplayFormat = '#,0.00;-#,0.00;0'
          end
          object edt_ILAMT3: TsCurrencyEdit
            Left = 555
            Top = 430
            Width = 141
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 34
            DisplayFormat = '#,0.00;-#,0.00;0'
          end
          object edt_ILAMT4: TsCurrencyEdit
            Left = 555
            Top = 452
            Width = 141
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 38
            DisplayFormat = '#,0.00;-#,0.00;0'
          end
          object edt_ILAMT5: TsCurrencyEdit
            Left = 555
            Top = 474
            Width = 141
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 42
            DisplayFormat = '#,0.00;-#,0.00;0'
          end
          object sPanel12: TsPanel
            Left = 368
            Top = 159
            Width = 328
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #47749#51032#51064' '#51221#48372
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 55
          end
          object edt_EXName1: TsEdit
            Left = 445
            Top = 184
            Width = 251
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#52397#50629#52404
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_EXName2: TsEdit
            Left = 445
            Top = 208
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXName3: TsEdit
            Left = 445
            Top = 232
            Width = 92
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49885#48324#48512#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr1: TsEdit
            Left = 445
            Top = 256
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr2: TsEdit
            Left = 445
            Top = 280
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel59: TsPanel
            Left = 1
            Top = 305
            Width = 105
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #50868#49569#49688#45800
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 56
          end
          object com_Carriage: TsComboBox
            Left = 107
            Top = 305
            Width = 237
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 11
            Text = 'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
            OnSelect = com_CarriageSelect
            Items.Strings = (
              ''
              'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
              'DT: '#54644#49345#50868#49569'(Sea)/'#54637#44277#50868#49569'(Air)')
          end
          object sPanel89: TsPanel
            Left = 1
            Top = 329
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = '[PAGE2] 44A/44B'#47484' '#51089#49457#54616#49464#50836
            Color = 16576211
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 57
          end
          object edt_ApBank5: TsEdit
            Left = 93
            Top = 126
            Width = 211
            Height = 23
            Color = clWhite
            MaxLength = 70
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'TEL'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel126: TsPanel
            Left = 512
            Top = 361
            Width = 42
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #45800#50948
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 58
          end
          object sPanel127: TsPanel
            Left = 555
            Top = 361
            Width = 141
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 59
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = 'SWIFT PAGE 1'
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sLabel3: TsLabel
            Left = 413
            Top = 232
            Width = 10
            Height = 15
            Caption = '%'
          end
          object sLabel4: TsLabel
            Left = 540
            Top = 304
            Width = 152
            Height = 15
            Caption = #50808#54872#51008#54665#51068#44221#50864' '#51008#54665#47749' '#49325#51228
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            UseSkinColor = False
          end
          object sPanel15: TsPanel
            Left = 4
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '40A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object sPanel1: TsPanel
            Left = 39
            Top = 4
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Form of Documentary Credit'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_Doccd: TsEdit
            Tag = 1014
            Left = 239
            Top = 4
            Width = 26
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 1
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_Doccd1: TsEdit
            Left = 266
            Top = 4
            Width = 184
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel13: TsPanel
            Left = 4
            Top = 28
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sPanel14: TsPanel
            Left = 39
            Top = 28
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Date and Place of Expiry'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object msk_exDate: TsMaskEdit
            Left = 275
            Top = 28
            Width = 80
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 2
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'DATE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object edt_exPlace: TsEdit
            Left = 397
            Top = 28
            Width = 308
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel16: TsPanel
            Left = 4
            Top = 60
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '50'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object sPanel17: TsPanel
            Left = 39
            Top = 60
            Width = 312
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Applicant'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object edt_Applic1: TsEdit
            Left = 4
            Top = 84
            Width = 347
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic2: TsEdit
            Left = 4
            Top = 106
            Width = 347
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic3: TsEdit
            Left = 4
            Top = 128
            Width = 347
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic4: TsEdit
            Left = 4
            Top = 150
            Width = 347
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic5: TsEdit
            Left = 56
            Top = 172
            Width = 295
            Height = 23
            Color = clWhite
            MaxLength = 25
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'TEL'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel18: TsPanel
            Left = 356
            Top = 60
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '59'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object sPanel19: TsPanel
            Left = 391
            Top = 60
            Width = 314
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Beneficiary'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_Benefc1: TsEdit
            Left = 404
            Top = 84
            Width = 301
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc2: TsEdit
            Left = 356
            Top = 106
            Width = 349
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc3: TsEdit
            Left = 356
            Top = 128
            Width = 349
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc4: TsEdit
            Left = 356
            Top = 150
            Width = 349
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc5: TsEdit
            Left = 424
            Top = 172
            Width = 281
            Height = 23
            Color = clWhite
            MaxLength = 34
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel21: TsPanel
            Left = 4
            Top = 204
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '32B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object sPanel22: TsPanel
            Left = 39
            Top = 204
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Currency Code,Amount'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object edt_Cdcur: TsEdit
            Tag = 1016
            Left = 239
            Top = 204
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 15
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_Cdamt: TsCurrencyEdit
            Left = 274
            Top = 204
            Width = 135
            Height = 23
            AutoSize = False
            TabOrder = 16
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object sPanel23: TsPanel
            Left = 4
            Top = 228
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object sPanel25: TsPanel
            Left = 39
            Top = 228
            Width = 234
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Percentage Credit Amount Tolerance'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object edt_cdPerP: TsCurrencyEdit
            Left = 303
            Top = 228
            Width = 42
            Height = 23
            AutoSize = False
            TabOrder = 17
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '('#65291')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object edt_cdPerM: TsCurrencyEdit
            Left = 370
            Top = 228
            Width = 39
            Height = 23
            AutoSize = False
            TabOrder = 18
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '/ ('#65293')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object sPanel26: TsPanel
            Left = 4
            Top = 252
            Width = 34
            Height = 285
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '45A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 37
          end
          object sPanel28: TsPanel
            Left = 39
            Top = 252
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Terms of Price'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
          object sPanel30: TsPanel
            Left = 39
            Top = 276
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Place of Terms of Price'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 39
          end
          object sPanel31: TsPanel
            Left = 39
            Top = 300
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Country of Origin'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 40
          end
          object edt_TermPR: TsEdit
            Tag = 1017
            Left = 239
            Top = 252
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 19
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_TermPR_M: TsEdit
            Left = 336
            Top = 252
            Width = 201
            Height = 23
            Color = 12775866
            MaxLength = 65
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51649#51217#51077#47141
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_plTerm: TsEdit
            Left = 239
            Top = 276
            Width = 298
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Origin: TsEdit
            Tag = 1018
            Left = 239
            Top = 300
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 22
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_Origin_M: TsEdit
            Left = 274
            Top = 300
            Width = 263
            Height = 23
            Color = clWhite
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel32: TsPanel
            Left = 39
            Top = 324
            Width = 588
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Description of Goods and/or Services'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 41
          end
          object sPanel33: TsPanel
            Left = 628
            Top = 324
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '65*300z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 42
          end
          object memo_Desgood1: TsMemo
            Tag = 122
            Left = 39
            Top = 348
            Width = 666
            Height = 189
            Hint = 'DesGood'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 24
            WordWrap = False
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_Benefc: TsEdit
            Tag = 1015
            Left = 356
            Top = 84
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 9
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#48169#48277
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel62: TsPanel
            Left = 4
            Top = 172
            Width = 53
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'TEL'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 43
          end
          object sPanel88: TsPanel
            Left = 356
            Top = 172
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'ACCOUNT'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 44
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = 'PAGE 2'
        object sPanel34: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel36: TsPanel
            Left = 4
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel37: TsPanel
            Left = 39
            Top = 4
            Width = 257
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional Amount Covered'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_aaCcv1: TsEdit
            Left = 4
            Top = 28
            Width = 292
            Height = 23
            Color = clWhite
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv2: TsEdit
            Left = 4
            Top = 50
            Width = 292
            Height = 23
            Color = clWhite
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv3: TsEdit
            Left = 4
            Top = 72
            Width = 292
            Height = 23
            Color = clWhite
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv4: TsEdit
            Left = 4
            Top = 94
            Width = 292
            Height = 23
            Color = clWhite
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel38: TsPanel
            Left = 4
            Top = 124
            Width = 34
            Height = 36
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel39: TsPanel
            Left = 39
            Top = 124
            Width = 257
            Height = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Drafts at...'#13#10'('#54868#54872#50612#51020#51312#44148')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object edt_Draft1: TsEdit
            Left = 4
            Top = 161
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Draft2: TsEdit
            Left = 4
            Top = 183
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Draft3: TsEdit
            Left = 4
            Top = 205
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel40: TsPanel
            Left = 39
            Top = 232
            Width = 257
            Height = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Mixed Payment Detail'#13#10'('#54844#54633#51648#44553#51312#44148')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_mixPay1: TsEdit
            Left = 4
            Top = 269
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_mixPay2: TsEdit
            Left = 4
            Top = 291
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_mixPay3: TsEdit
            Left = 4
            Top = 313
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_mixPay4: TsEdit
            Left = 4
            Top = 335
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel41: TsPanel
            Left = 39
            Top = 363
            Width = 257
            Height = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Negotiation/Deferred Payment Details'#13#10'('#47588#51077'/'#50672#51648#44552#51312#44148#47749#49464')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object edt_defPay1: TsEdit
            Left = 4
            Top = 400
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_defPay2: TsEdit
            Left = 4
            Top = 422
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_defPay3: TsEdit
            Left = 4
            Top = 444
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_defPay4: TsEdit
            Left = 4
            Top = 466
            Width = 291
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel42: TsPanel
            Left = 4
            Top = 363
            Width = 34
            Height = 36
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object sPanel43: TsPanel
            Left = 300
            Top = 156
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object sPanel44: TsPanel
            Left = 335
            Top = 156
            Width = 291
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Latest Date of Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object sPanel45: TsPanel
            Left = 335
            Top = 106
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 
              'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
              'livery'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 37
          end
          object msk_lstDate: TsMaskEdit
            Left = 627
            Top = 156
            Width = 81
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 19
            CheckOnExit = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object sPanel46: TsPanel
            Left = 300
            Top = 182
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
          object sPanel47: TsPanel
            Left = 335
            Top = 182
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Shipment Period'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 39
          end
          object edt_shipPD1: TsEdit
            Left = 300
            Top = 206
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD2: TsEdit
            Left = 300
            Top = 228
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD3: TsEdit
            Left = 300
            Top = 250
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD4: TsEdit
            Left = 300
            Top = 272
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD5: TsEdit
            Left = 300
            Top = 294
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD6: TsEdit
            Left = 300
            Top = 316
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel48: TsPanel
            Left = 300
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 40
          end
          object sPanel49: TsPanel
            Left = 335
            Top = 4
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Partial Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 41
          end
          object sPanel50: TsPanel
            Left = 300
            Top = 30
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43T'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 42
          end
          object sPanel51: TsPanel
            Left = 335
            Top = 30
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Transhipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 43
          end
          object sPanel52: TsPanel
            Left = 300
            Top = 342
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 44
          end
          object sPanel53: TsPanel
            Left = 335
            Top = 342
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Loading/Airport of Departure ['#49440#51201#54637']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 45
          end
          object edt_SunjukPort: TsEdit
            Left = 300
            Top = 366
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel54: TsPanel
            Left = 300
            Top = 392
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44F'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 46
          end
          object sPanel55: TsPanel
            Left = 335
            Top = 392
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Discharge/Airport of Destination ['#46020#52265#54637']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 47
          end
          object edt_dochackPort: TsEdit
            Left = 300
            Top = 416
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel56: TsPanel
            Left = 300
            Top = 56
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 48
          end
          object sPanel57: TsPanel
            Left = 335
            Top = 56
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 
              '     Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receip' +
              't'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 49
          end
          object sPanel58: TsPanel
            Left = 300
            Top = 106
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 50
          end
          object edt_loadOn: TsEdit
            Left = 300
            Top = 80
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_forTran: TsEdit
            Left = 300
            Top = 130
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object com_Pship: TsComboBox
            Left = 466
            Top = 4
            Width = 242
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 15
            Items.Strings = (
              ''
              '9: ALLOWED (Partial shipments are allowed)'
              '10: NOT ALLOWED (Partial shipments are not allowed)')
          end
          object com_TShip: TsComboBox
            Left = 466
            Top = 30
            Width = 242
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 16
            Items.Strings = (
              ''
              '7: ALLOWED (Transhipments are allowed)'
              '8: NOT ALLOWED (Transhipments are not allowed)')
          end
          object sPanel84: TsPanel
            Left = 4
            Top = 232
            Width = 34
            Height = 36
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42M'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 51
          end
        end
      end
      object sTabSheet5: TsTabSheet
        Caption = 'PAGE 3'
        object sPanel35: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sLabel31: TsLabel
            Left = 294
            Top = 36
            Width = 40
            Height = 15
            Caption = 'COPIES'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel8: TsLabel
            Left = 169
            Top = 60
            Width = 435
            Height = 15
            Caption = 'OF CLEAN ON BOARD OCEAN BILLS OF LADING MADE OUT TO THE ORDER OF'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel9: TsLabel
            Left = 261
            Top = 108
            Width = 70
            Height = 15
            Caption = 'AND NOTIFY'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel10: TsLabel
            Left = 261
            Top = 229
            Width = 70
            Height = 15
            Caption = 'AND NOTIFY'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel38: TsLabel
            Left = 62
            Top = 273
            Width = 638
            Height = 15
            Caption = 
              'EXPRESSLY STIPULATING THAT CLAIMS ARE PAYABLE IN KOREA AND IT MU' +
              'ST INCLUDE : INSTITUTE CARGO CLAUSE'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel11: TsLabel
            Left = 195
            Top = 343
            Width = 40
            Height = 15
            Caption = 'COPIES'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel5: TsLabel
            Left = 261
            Top = 159
            Width = 70
            Height = 15
            Caption = 'AND NOTIFY'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sPanel60: TsPanel
            Left = 4
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '46A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel61: TsPanel
            Left = 39
            Top = 4
            Width = 146
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Documents Required :'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object chk_380: TsCheckBox
            Tag = 500
            Left = 39
            Top = 34
            Width = 217
            Height = 19
            Caption = 'SIGNED COMMERCIAL INVOICE IN'
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_380_1: TsEdit
            Tag = 500
            Left = 257
            Top = 33
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel63: TsPanel
            Left = 4
            Top = 32
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '380'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel64: TsPanel
            Left = 4
            Top = 56
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '705'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object com_705Gubun: TsComboBox
            Left = 61
            Top = 56
            Width = 105
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 3
            OnSelect = com_705GubunSelect
            Items.Strings = (
              ''
              '705: FULL SET'
              '706: FULL SET'
              '717: 1/3 SET'
              '718: 2/3 SET'
              '707: COPY')
          end
          object chk_705: TsCheckBox
            Tag = 500
            Left = 39
            Top = 59
            Width = 20
            Height = 16
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_705_1: TsEdit
            Tag = 501
            Left = 61
            Top = 80
            Width = 264
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 4
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_705_2: TsEdit
            Tag = 501
            Left = 326
            Top = 80
            Width = 376
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 5
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object com_705_3: TsComboBox
            Left = 161
            Top = 104
            Width = 96
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = 'MARKED FREIGHT'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 6
            Items.Strings = (
              ''
              '31: PREPAID'
              '32: COLLECT')
          end
          object edt_705_4: TsEdit
            Tag = 501
            Left = 334
            Top = 104
            Width = 368
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 7
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Caption = 'sEdit54'
          end
          object sPanel65: TsPanel
            Left = 4
            Top = 131
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '740'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object chk_740: TsCheckBox
            Tag = 500
            Left = 39
            Top = 133
            Width = 193
            Height = 19
            Caption = 'AIR WAY BILL CONSIGNED TO'
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_740_1: TsEdit
            Tag = 501
            Left = 229
            Top = 131
            Width = 236
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 9
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_740_2: TsEdit
            Tag = 501
            Left = 466
            Top = 131
            Width = 236
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 10
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object com_740_3: TsComboBox
            Left = 161
            Top = 155
            Width = 96
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = 'MARKED FREIGHT'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 11
            Items.Strings = (
              ''
              '31: PREPAID'
              '32: COLLECT')
          end
          object edt_740_4: TsEdit
            Tag = 501
            Left = 334
            Top = 155
            Width = 368
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Caption = 'sEdit54'
          end
          object sPanel66: TsPanel
            Left = 4
            Top = 179
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '760'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object chk_760: TsCheckBox
            Tag = 500
            Left = 39
            Top = 181
            Width = 467
            Height = 19
            Caption = 'FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO THE'
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_760_1: TsEdit
            Tag = 501
            Left = 121
            Top = 201
            Width = 264
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = 'ORDER OF'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_760_2: TsEdit
            Tag = 501
            Left = 386
            Top = 201
            Width = 316
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 15
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object com_760_3: TsComboBox
            Left = 161
            Top = 225
            Width = 96
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = 'MARKED FREIGHT'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 16
            Items.Strings = (
              ''
              '31: PREPAID'
              '32: COLLECT')
          end
          object edt_760_4: TsEdit
            Tag = 501
            Left = 334
            Top = 225
            Width = 368
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 17
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Caption = 'sEdit54'
          end
          object sPanel67: TsPanel
            Left = 4
            Top = 251
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '530'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object chk_530: TsCheckBox
            Tag = 505
            Left = 40
            Top = 253
            Width = 637
            Height = 19
            Caption = 
              'FULL SET OF INSURANCE POLICIES OR CERTIFICATES, ENDORSED'#13#10'IN BLA' +
              'NK FOR 110% OF THE INVOICE VALUE,'
            Color = clBtnFace
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 18
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_530_1: TsEdit
            Tag = 501
            Left = 62
            Top = 289
            Width = 640
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_530_2: TsEdit
            Tag = 501
            Left = 62
            Top = 313
            Width = 640
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 20
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel68: TsPanel
            Left = 4
            Top = 339
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '271'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object sPanel69: TsPanel
            Left = 4
            Top = 363
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '861'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object chk_271: TsCheckBox
            Tag = 500
            Left = 40
            Top = 341
            Width = 120
            Height = 19
            Caption = 'PACKING LIST IN'
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_271_1: TsEdit
            Tag = 500
            Left = 158
            Top = 340
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 22
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object chk_861: TsCheckBox
            Tag = 500
            Left = 40
            Top = 365
            Width = 157
            Height = 19
            Caption = 'CRETIFICATE OF ORIGIN'
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object sPanel70: TsPanel
            Left = 4
            Top = 387
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '2AA'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object chk_2AA: TsCheckBox
            Tag = 508
            Left = 40
            Top = 389
            Width = 200
            Height = 19
            Hint = 'doc2aa'
            Caption = 'OTHER DOCUMENT(S) ( if any )'
            Color = clBtnFace
            Font.Charset = ANSI_CHARSET
            Font.Color = 5197647
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 24
            OnClick = chk_380Click
            SkinData.SkinSection = 'CHECKBOX'
          end
          object memo_2AA_1: TsMemo
            Tag = 122
            Left = 43
            Top = 409
            Width = 662
            Height = 128
            Hint = 'DesGood'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 25
            WordWrap = False
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
        end
      end
      object sTabSheet6: TsTabSheet
        Caption = 'PAGE 4'
        object sPanel71: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sLabel48: TsLabel
            Left = 39
            Top = 205
            Width = 656
            Height = 17
            Caption = 
              'ALL BANKING COMMISSIONS AND CHARGES INCLUDING REIMBURSEMENT CHAR' +
              'GES OUTSIDE KOREA ARE'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sPanel72: TsPanel
            Left = 4
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '47A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object sPanel73: TsPanel
            Left = 39
            Top = 4
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional conditions :'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object chk_ACD2AA: TsCheckBox
            Tag = 601
            Left = 36
            Top = 30
            Width = 104
            Height = 19
            Hint = 'acd2AA'
            Caption = 'SHIPMENT BY'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = chk_ACD2AAClick
            SkinData.SkinSection = 'CHECKBOX'
          end
          object edt_ACD2AA_1: TsEdit
            Tag = 601
            Left = 141
            Top = 29
            Width = 292
            Height = 21
            TabStop = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object chk_ACD2AB: TsCheckBox
            Tag = 602
            Left = 36
            Top = 50
            Width = 463
            Height = 19
            Hint = 'acd2AB'
            Caption = 'ACCEPTANCE COMMISSION DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'CHECKBOX'
          end
          object chk_ACD2AC: TsCheckBox
            Tag = 603
            Left = 36
            Top = 70
            Width = 316
            Height = 19
            Hint = 'acd2AC'
            Caption = 'ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'CHECKBOX'
          end
          object chk_ACD2AD: TsCheckBox
            Tag = 604
            Left = 36
            Top = 90
            Width = 237
            Height = 19
            Hint = 'acd2AD'
            Caption = 'LATE PRESENTATION B/L ACCEPTABLE'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'CHECKBOX'
          end
          object chk_ACD2AE: TsCheckBox
            Tag = 605
            Left = 36
            Top = 110
            Width = 199
            Height = 19
            Hint = 'acd2AE'
            Caption = 'OTHER CONDITION(S) ( if any )'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            OnClick = chk_ACD2AAClick
            SkinData.SkinSection = 'CHECKBOX'
          end
          object memo_ACD2AE_1: TsMemo
            Tag = 122
            Left = 39
            Top = 130
            Width = 663
            Height = 50
            Hint = 'DesGood'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 6
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel74: TsPanel
            Left = 625
            Top = 107
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '65*100z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object sPanel75: TsPanel
            Left = 4
            Top = 181
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '71D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object sPanel76: TsPanel
            Left = 39
            Top = 181
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Charges'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object com_Charge: TsComboBox
            Left = 144
            Top = 223
            Width = 131
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = 'FOR ACCOUNT OF'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 7
            OnSelect = chk_ACD2AAClick
            Items.Strings = (
              ''
              '2AF: APPLICANT'
              '2AG: BENEFICIARY'
              '2AH: '#51649#51217#51077#47141)
          end
          object sPanel77: TsPanel
            Left = 625
            Top = 224
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '35C*6R/z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
          end
          object memo_Charge_1: TsMemo
            Tag = 122
            Left = 39
            Top = 247
            Width = 663
            Height = 63
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 8
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel78: TsPanel
            Left = 4
            Top = 311
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '48'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object sPanel79: TsPanel
            Left = 39
            Top = 311
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Period for Presentation in Days'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_period: TsEdit
            Tag = 500
            Left = 38
            Top = 335
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 9
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object com_PeriodIndex: TsComboBox
            Left = 134
            Top = 335
            Width = 145
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = '(days after'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 10
            Text = 'the Date of Shipment'
            OnSelect = chk_ACD2AAClick
            Items.Strings = (
              'the Date of Shipment'
              'any Other Date')
          end
          object edt_PeriodDetail: TsEdit
            Tag = 601
            Left = 386
            Top = 336
            Width = 316
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            MaxLength = 35
            ParentCtl3D = False
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49436#47448#51228#49884#44592#44036' '#49345#49464
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sPanel80: TsPanel
            Left = 4
            Top = 361
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel81: TsPanel
            Left = 39
            Top = 361
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Confirmation Instructions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object com_Confirm: TsComboBox
            Left = 242
            Top = 361
            Width = 182
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 12
            OnSelect = chk_ACD2AAClick
            Items.Strings = (
              ''
              'DA: WITHOUT'
              'DB: MAY ADD'
              'DC: CONFIRM')
          end
          object sPanel82: TsPanel
            Left = 4
            Top = 385
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '58A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel83: TsPanel
            Left = 39
            Top = 385
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Requested Confirmation Party'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_CONFIRM_BICCD: TsEdit
            Tag = 1019
            Left = 303
            Top = 386
            Width = 121
            Height = 23
            Color = 12775866
            TabOrder = 13
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = 'BICCODE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sEdit70: TsEdit
            Tag = 1020
            Left = 486
            Top = 386
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 14
            OnDblClick = edt_IN_MATHODDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54869#51064#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM_BANKNM: TsEdit
            Left = 536
            Top = 386
            Width = 167
            Height = 23
            Color = clWhite
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM_BANKBR: TsEdit
            Left = 486
            Top = 410
            Width = 217
            Height = 23
            Color = clWhite
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel85: TsPanel
            Left = 4
            Top = 433
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49G'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel86: TsPanel
            Left = 39
            Top = 433
            Width = 202
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Special Payment Conditions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object memo_SPECIAL_PAY: TsMemo
            Tag = 122
            Left = 40
            Top = 458
            Width = 663
            Height = 79
            Hint = 'DesGood'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 17
            WordWrap = False
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel87: TsPanel
            Left = 626
            Top = 435
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '65C*100R/z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_CONFIRM_BICNM: TsEdit
            Left = 242
            Top = 410
            Width = 182
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 711
          Height = 507
          TabStop = False
          Align = alClient
          Color = clGray
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #52376#47532
              Width = 60
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'BENEFC1'
              Title.Alignment = taCenter
              Title.Caption = #49688#54812#51088
              Width = 172
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'AP_BANK1'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51008#54665
              Width = 164
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'CD_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#44552#50529
              Width = 97
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CD_CUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
            OnClick = sBitBtn1Click
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 719
      Height = 55
      Align = alTop
      
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 280
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 178
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object com_func: TsComboBox
        Left = 360
        Top = 4
        Width = 121
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 360
        Top = 28
        Width = 211
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 216
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object edt_MSEQ: TsEdit
        Left = 243
        Top = 4
        Width = 30
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '1'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 160
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20100101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180730'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT A1.MAINT_NO, A1.MSEQ,  MESSAGE1, MESSAGE2, USER_ID, DATEE' +
        ', APP_DATE, IN_MATHOD, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP' +
        '_BANK4, AP_BANK5, AD_BANK, AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK' +
        '4, AD_BANK_BIC, AD_PAY, IL_NO1, IL_NO2, IL_NO3, IL_NO4, IL_NO5, ' +
        'IL_AMT1, IL_AMT2, IL_AMT3, IL_AMT4, IL_AMT5, IL_CUR1, IL_CUR2, I' +
        'L_CUR3, IL_CUR4, IL_CUR5, AD_INFO1, AD_INFO2, AD_INFO3, AD_INFO4' +
        ', AD_INFO5, DOC_CD, EX_DATE, EX_PLACE, CHK1, CHK2, CHK3, prno, F' +
        '_INTERFACE, IMP_CD1, IMP_CD2, IMP_CD3, IMP_CD4, IMP_CD5,'
      
        'APPLIC1, APPLIC2, APPLIC3, APPLIC4, APPLIC5, BENEFC, BENEFC1, BE' +
        'NEFC2, BENEFC3, BENEFC4, BENEFC5, CD_AMT, CD_CUR, CD_PERP, CD_PE' +
        'RM, CD_MAX, AA_CV1, AA_CV2, AA_CV3, AA_CV4, DRAFT1, DRAFT2, DRAF' +
        'T3, MIX_PAY1, MIX_PAY2, MIX_PAY3, MIX_PAY4, DEF_PAY1, DEF_PAY2, ' +
        'DEF_PAY3, DEF_PAY4, PSHIP, TSHIP, LOAD_ON, FOR_TRAN, LST_DATE, S' +
        'HIP_PD1, SHIP_PD2, SHIP_PD3, SHIP_PD4, SHIP_PD5, SHIP_PD6, DESGO' +
        'OD_1,'
      
        'DOC_380, DOC_380_1, DOC_705, DOC_705_1, DOC_705_2, DOC_705_3, DO' +
        'C_705_4, DOC_740, DOC_740_1, DOC_740_2, DOC_740_3, DOC_740_4, DO' +
        'C_530, DOC_530_1, DOC_530_2, DOC_271, DOC_271_1, DOC_861, DOC_2A' +
        'A, DOC_2AA_1, ACD_2AA, ACD_2AA_1, ACD_2AB, ACD_2AC, ACD_2AD, ACD' +
        '_2AE, ACD_2AE_1, CHARGE, CHARGE_1, PERIOD, CONFIRMM, INSTRCT, IN' +
        'STRCT_1, EX_NAME1, EX_NAME2, EX_NAME3, EX_ADDR1, EX_ADDR2, ORIGI' +
        'N, ORIGIN_M, PL_TERM, TERM_PR, TERM_PR_M, SRBUHO, DOC_705_GUBUN,' +
        ' CARRIAGE, DOC_760, DOC_760_1, DOC_760_2, DOC_760_3, DOC_760_4, ' +
        'SUNJUCK_PORT, DOCHACK_PORT, CONFIRM_BICCD, CONFIRM_BANKNM, CONFI' +
        'RM_BANKBR, PERIOD_IDX,  PERIOD_TXT, SPECIAL_PAY,'
      'Mathod700.DOC_NAME as mathod_Name'
      ',Pay700.DOC_NAME as pay_Name'
      ',IMPCD700_1.DOC_NAME as Imp_Name_1'
      ',IMPCD700_2.DOC_NAME as Imp_Name_2'
      ',IMPCD700_3.DOC_NAME as Imp_Name_3'
      ',IMPCD700_4.DOC_NAME as Imp_Name_4'
      ',IMPCD700_5.DOC_NAME as Imp_Name_5'
      ',DocCD700.DOC_NAME as DOC_Name'
      ',doc705_700.DOC_NAME as doc705_Name'
      ',doc740_700.DOC_NAME as doc740_Name'
      ',doc760_700.DOC_NAME as doc760_Name'
      ',CHARGE700.DOC_NAME as CHARGE_Name'
      ',CARRIAGE700.DOC_NAME as CARRIAGE_NAME'
      'FROM [APP700_1] AS A1'
      'INNER JOIN [APP700_2] AS A2 ON A1.MAINT_NO = A2.MAINT_NO'
      'INNER JOIN [APP700_3] AS A3 ON A1.MAINT_NO = A3.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON A1.IN_MATHOD = Mathod700.' +
        'CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON A1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON A1.IMP_CD1 = IMPCD700_' +
        '1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON A1.IMP_CD2 = IMPCD700_' +
        '2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON A1.IMP_CD3 = IMPCD700_' +
        '3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON A1.IMP_CD4 = IMPCD700_' +
        '4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON A1.IMP_CD5 = IMPCD700_' +
        '5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON A1.DOC_CD = DocCD700.COD' +
        'E'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON A3.DOC_705_3 = doc705' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON A3.DOC_740_3 = doc740' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON A3.DOC_760_3 = doc760' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON A3.CHARGE = CHARGE700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') CARRIAGE700 ON A3.CARRIAGE = CARRI' +
        'AGE700.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(A1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 56
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK_BIC: TStringField
      FieldName = 'AD_BANK_BIC'
      Size = 11
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = CHK3GetText
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListCHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListCARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListCONFIRM_BICCD: TStringField
      FieldName = 'CONFIRM_BICCD'
      Size = 11
    end
    object qryListCONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 70
    end
    object qryListCONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 70
    end
    object qryListPERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryListSPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCARRIAGE_NAME: TStringField
      FieldName = 'CARRIAGE_NAME'
      Size = 100
    end
    object qryListPERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 88
    Top = 192
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      
        'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10), @MSEQ varchar' +
        '(2)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      'SET @MSEQ = :MSEQ'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO AND MSEQ = @MSEQ)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO  AND MSEQ = @MSEQ'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , @MSEQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APP700_1'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO'
      'AND MSEQ = @MSEQ')
    Left = 56
    Top = 224
  end
  object PopupMenu1: TPopupMenu
    Left = 56
    Top = 256
    object x1: TMenuItem
      Caption = #53580#49828#53944' '#51025#45813#49436#51089#49457'(INF700)'
      OnClick = x1Click
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CreateINF700;1'
    Parameters = <>
    Left = 56
    Top = 296
  end
end
