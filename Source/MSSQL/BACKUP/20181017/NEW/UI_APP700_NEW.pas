unit UI_APP700_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sButton, sLabel, Buttons, sSpeedButton,
  ExtCtrls, sPanel, sSkinProvider, DB, ADODB, sComboBox, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sGroupBox,
  sCheckBox, sRadioButton, StrUtils, DateUtils, Clipbrd, Menus;

type
  TUI_APP700_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    msk_APP_DATE: TsMaskEdit;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    edt_IN_MATHOD: TsEdit;
    edt_IN_MATHOD_NM: TsEdit;
    edt_ImpCd1: TsEdit;
    edt_ImpCd1_NM: TsEdit;
    edt_ImpCd2: TsEdit;
    edt_ImpCd2_NM: TsEdit;
    edt_ImpCd3: TsEdit;
    edt_ImpCd3_NM: TsEdit;
    edt_ImpCd4: TsEdit;
    edt_ImpCd4_NM: TsEdit;
    edt_ImpCd5: TsEdit;
    edt_ImpCd5_NM: TsEdit;
    edt_AdPay: TsEdit;
    edt_AdPay1: TsEdit;
    edt_ApBank: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank3: TsEdit;
    edt_AdBank: TsEdit;
    edt_AdBank1: TsEdit;
    edt_AdBank3: TsEdit;
    edt_AdBank_BIC: TsEdit;
    edt_AdInfo1: TsEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo5: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    sEdit2: TsEdit;
    edt_ILno1: TsEdit;
    edt_ILno2: TsEdit;
    edt_ILno5: TsEdit;
    edt_ILno3: TsEdit;
    edt_ILno4: TsEdit;
    sPanel2: TsPanel;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    edt_ILCur1: TsEdit;
    edt_ILCur2: TsEdit;
    edt_ILCur5: TsEdit;
    edt_ILCur3: TsEdit;
    edt_ILCur4: TsEdit;
    edt_ILAMT1: TsCurrencyEdit;
    edt_ILAMT2: TsCurrencyEdit;
    edt_ILAMT3: TsCurrencyEdit;
    edt_ILAMT4: TsCurrencyEdit;
    edt_ILAMT5: TsCurrencyEdit;
    sPanel12: TsPanel;
    edt_EXName1: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXAddr2: TsEdit;
    sPanel15: TsPanel;
    sPanel1: TsPanel;
    edt_Doccd: TsEdit;
    edt_Doccd1: TsEdit;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    msk_exDate: TsMaskEdit;
    edt_exPlace: TsEdit;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    edt_Applic1: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic4: TsEdit;
    edt_Applic5: TsEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    edt_Benefc1: TsEdit;
    edt_Benefc2: TsEdit;
    edt_Benefc3: TsEdit;
    edt_Benefc4: TsEdit;
    edt_Benefc5: TsEdit;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    edt_Cdcur: TsEdit;
    edt_Cdamt: TsCurrencyEdit;
    sPanel23: TsPanel;
    sPanel25: TsPanel;
    edt_cdPerP: TsCurrencyEdit;
    edt_cdPerM: TsCurrencyEdit;
    sLabel3: TsLabel;
    sPanel26: TsPanel;
    sPanel28: TsPanel;
    sPanel30: TsPanel;
    sPanel31: TsPanel;
    edt_TermPR: TsEdit;
    edt_TermPR_M: TsEdit;
    edt_plTerm: TsEdit;
    edt_Origin: TsEdit;
    edt_Origin_M: TsEdit;
    sPanel32: TsPanel;
    sPanel33: TsPanel;
    memo_Desgood1: TsMemo;
    sTabSheet2: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sPanel34: TsPanel;
    sPanel35: TsPanel;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    edt_aaCcv1: TsEdit;
    edt_aaCcv2: TsEdit;
    edt_aaCcv3: TsEdit;
    edt_aaCcv4: TsEdit;
    sPanel38: TsPanel;
    sPanel39: TsPanel;
    edt_Draft1: TsEdit;
    edt_Draft2: TsEdit;
    edt_Draft3: TsEdit;
    sPanel40: TsPanel;
    edt_mixPay1: TsEdit;
    edt_mixPay2: TsEdit;
    edt_mixPay3: TsEdit;
    edt_mixPay4: TsEdit;
    sPanel41: TsPanel;
    edt_defPay1: TsEdit;
    edt_defPay2: TsEdit;
    edt_defPay3: TsEdit;
    edt_defPay4: TsEdit;
    sPanel42: TsPanel;
    sPanel43: TsPanel;
    sPanel44: TsPanel;
    sPanel45: TsPanel;
    msk_lstDate: TsMaskEdit;
    sPanel46: TsPanel;
    sPanel47: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    sPanel48: TsPanel;
    sPanel49: TsPanel;
    sPanel50: TsPanel;
    sPanel51: TsPanel;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel54: TsPanel;
    sPanel55: TsPanel;
    edt_dochackPort: TsEdit;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sPanel58: TsPanel;
    edt_loadOn: TsEdit;
    edt_forTran: TsEdit;
    sLabel4: TsLabel;
    sPanel60: TsPanel;
    sPanel61: TsPanel;
    chk_380: TsCheckBox;
    edt_380_1: TsEdit;
    sLabel31: TsLabel;
    sPanel63: TsPanel;
    sPanel64: TsPanel;
    com_705Gubun: TsComboBox;
    chk_705: TsCheckBox;
    sLabel8: TsLabel;
    edt_705_1: TsEdit;
    edt_705_2: TsEdit;
    com_705_3: TsComboBox;
    edt_705_4: TsEdit;
    sLabel9: TsLabel;
    sPanel65: TsPanel;
    chk_740: TsCheckBox;
    edt_740_1: TsEdit;
    edt_740_2: TsEdit;
    com_740_3: TsComboBox;
    edt_740_4: TsEdit;
    sPanel66: TsPanel;
    chk_760: TsCheckBox;
    edt_760_1: TsEdit;
    edt_760_2: TsEdit;
    com_760_3: TsComboBox;
    sLabel10: TsLabel;
    edt_760_4: TsEdit;
    sPanel67: TsPanel;
    chk_530: TsCheckBox;
    sLabel38: TsLabel;
    edt_530_1: TsEdit;
    edt_530_2: TsEdit;
    sPanel68: TsPanel;
    sPanel69: TsPanel;
    chk_271: TsCheckBox;
    sLabel11: TsLabel;
    edt_271_1: TsEdit;
    chk_861: TsCheckBox;
    sPanel70: TsPanel;
    chk_2AA: TsCheckBox;
    memo_2AA_1: TsMemo;
    sTabSheet6: TsTabSheet;
    sPanel71: TsPanel;
    sPanel72: TsPanel;
    sPanel73: TsPanel;
    chk_ACD2AA: TsCheckBox;
    edt_ACD2AA_1: TsEdit;
    chk_ACD2AB: TsCheckBox;
    chk_ACD2AC: TsCheckBox;
    chk_ACD2AD: TsCheckBox;
    chk_ACD2AE: TsCheckBox;
    memo_ACD2AE_1: TsMemo;
    sPanel74: TsPanel;
    sPanel75: TsPanel;
    sPanel76: TsPanel;
    sLabel48: TsLabel;
    com_Charge: TsComboBox;
    sPanel77: TsPanel;
    memo_Charge_1: TsMemo;
    sPanel78: TsPanel;
    sPanel79: TsPanel;
    edt_period: TsEdit;
    com_PeriodIndex: TsComboBox;
    edt_PeriodDetail: TsEdit;
    sPanel80: TsPanel;
    sPanel81: TsPanel;
    com_Confirm: TsComboBox;
    sPanel82: TsPanel;
    sPanel83: TsPanel;
    edt_CONFIRM_BICCD: TsEdit;
    sEdit70: TsEdit;
    edt_CONFIRM_BANKNM: TsEdit;
    edt_CONFIRM_BANKBR: TsEdit;
    com_Pship: TsComboBox;
    com_TShip: TsComboBox;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    sPanel86: TsPanel;
    memo_SPECIAL_PAY: TsMemo;
    sPanel87: TsPanel;
    sPanel59: TsPanel;
    edt_Benefc: TsEdit;
    sLabel5: TsLabel;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK_BIC: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListDOC_CD: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListDOC_380: TBooleanField;
    qryListDOC_380_1: TBCDField;
    qryListDOC_705: TBooleanField;
    qryListDOC_705_1: TStringField;
    qryListDOC_705_2: TStringField;
    qryListDOC_705_3: TStringField;
    qryListDOC_705_4: TStringField;
    qryListDOC_740: TBooleanField;
    qryListDOC_740_1: TStringField;
    qryListDOC_740_2: TStringField;
    qryListDOC_740_3: TStringField;
    qryListDOC_740_4: TStringField;
    qryListDOC_530: TBooleanField;
    qryListDOC_530_1: TStringField;
    qryListDOC_530_2: TStringField;
    qryListDOC_271: TBooleanField;
    qryListDOC_271_1: TBCDField;
    qryListDOC_861: TBooleanField;
    qryListDOC_2AA: TBooleanField;
    qryListDOC_2AA_1: TMemoField;
    qryListACD_2AA: TBooleanField;
    qryListACD_2AA_1: TStringField;
    qryListACD_2AB: TBooleanField;
    qryListACD_2AC: TBooleanField;
    qryListACD_2AD: TBooleanField;
    qryListACD_2AE: TBooleanField;
    qryListACD_2AE_1: TMemoField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListPERIOD: TBCDField;
    qryListCONFIRMM: TStringField;
    qryListINSTRCT: TBooleanField;
    qryListINSTRCT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListORIGIN: TStringField;
    qryListORIGIN_M: TStringField;
    qryListPL_TERM: TStringField;
    qryListTERM_PR: TStringField;
    qryListTERM_PR_M: TStringField;
    qryListSRBUHO: TStringField;
    qryListDOC_705_GUBUN: TStringField;
    qryListCARRIAGE: TStringField;
    qryListDOC_760: TBooleanField;
    qryListDOC_760_1: TStringField;
    qryListDOC_760_2: TStringField;
    qryListDOC_760_3: TStringField;
    qryListDOC_760_4: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListCONFIRM_BICCD: TStringField;
    qryListCONFIRM_BANKNM: TStringField;
    qryListCONFIRM_BANKBR: TStringField;
    qryListPERIOD_TXT: TStringField;
    qryListSPECIAL_PAY: TMemoField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListDOC_Name: TStringField;
    qryListdoc705_Name: TStringField;
    qryListdoc740_Name: TStringField;
    qryListdoc760_Name: TStringField;
    qryListCHARGE_Name: TStringField;
    qryListCARRIAGE_NAME: TStringField;
    sPanel62: TsPanel;
    sPanel88: TsPanel;
    qryListPERIOD_IDX: TIntegerField;
    com_Carriage: TsComboBox;
    edt_CONFIRM_BICNM: TsEdit;
    sPanel89: TsPanel;
    edt_ApBank5: TsEdit;
    btnCopy: TsButton;
    sPanel126: TsPanel;
    sPanel127: TsPanel;
    qryReady: TADOQuery;
    sButton13: TsButton;
    PopupMenu1: TPopupMenu;
    x1: TMenuItem;
    ADOStoredProc1: TADOStoredProc;
    edt_MSEQ: TsEdit;
    qryListMSEQ: TIntegerField;
    procedure com_705GubunSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure chk_380Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure edt_IN_MATHODDblClick(Sender: TObject);
    procedure com_CarriageSelect(Sender: TObject);
    procedure chk_ACD2AAClick(Sender: TObject);
    procedure btnTempClick(Sender: TObject);
    procedure edt_AdBank_BICExit(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure sButton13Click(Sender: TObject);
    procedure x1Click(Sender: TObject);
    procedure edt_IN_MATHODExit(Sender: TObject);
  private
    { Private declarations }
    procedure InitData;
    function CHECK_VALIDITY:String;
    function DataProcess(nType: integer): String;
    procedure CopyData;
    procedure ReadyDocument(DocNo: String);
  protected
    procedure ReadList(SortField: TColumn=nil); virtual;
    procedure ReadData; virtual;    
    procedure ButtonEnable; override;
  public
    { Public declarations }
  end;

var
  UI_APP700_NEW_frm: TUI_APP700_NEW_frm;

Const
  MSG_705 :String = 'OF CLEAN ON BOARD OCEAN BILLS OF LADING MADE OUT TO THE ORDER OF';
  MSG_706 :string = 'OF CLEAN ON BOARD OCEAN BILLS OF LADING MADE OUT TO';
implementation

uses
  TypeDefine, MSSQL, AutoNo, VarDefine, CodeContents, CodeDialogParent, CD_OPENDOC, CD_BANK, CD_BIC, CD_CREDITEX, CD_IMP_CD, CD_AMT_UNIT, CD_DOC_CD, CD_CUST, CD_TERM, CD_NATION, Dlg_ErrorMessage, SQLCreator, Commonlib, MessageDefine, dlg_CopyAPP700fromAPP700,
  dlg_SelectCopyDocument, DocumentSend, Dlg_RecvSelect, CreateDocuments,
  LivartData, LivartModule;

{$R *.dfm}

procedure TUI_APP700_NEW_frm.com_705GubunSelect(Sender: TObject);
begin
  inherited;
  case com_705Gubun.ItemIndex of
    1 : sLabel8.Caption := MSG_705;
  else
    sLabel8.Caption := MSG_706;
  end;


end;

procedure TUI_APP700_NEW_frm.ReadData;
begin
  IF not qryList.Active Then Exit;

  //------------------------------------------------------------------------------
  // 헤더
  //------------------------------------------------------------------------------
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  edt_MSEQ.Text := qryListMSEQ.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;
  //문서기능
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

//------------------------------------------------------------------------------
// 문서공통
//------------------------------------------------------------------------------
  msk_APP_DATE.Text := qryListAPP_DATE.AsString;
  edt_IN_MATHOD.Text := qryListIN_MATHOD.AsString;
  edt_IN_MATHOD_NM.Text := qryListmathod_Name.AsString;
  //개설은행
  edt_ApBank.Text := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString + qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString + qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;
  //통지은행
  edt_AdBank.Text := qryListAD_BANK.AsString;
  edt_AdBank1.Text := qryListAd_BANK1.AsString + qryListAd_BANK2.AsString;
  edt_ADBank3.Text := qryListAd_BANK3.AsString + qryListAd_BANK4.AsString;
  //통지은행 BIC코드(추가)
  edt_AdBank_BIC.Text := qryListAD_BANK_BIC.AsString;
  IF Trim(edt_AdBank_BIC.Text) = '' Then sEdit2.Clear;

  edt_AdPay.Text := qryListAD_PAY.AsString;
  edt_AdPay1.Text := qrylistpay_Name.AsString;
  //수입용도
  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd1_NM.Text := qryListImp_Name_1.AsString;
  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd2_NM.Text := qryListImp_Name_2.AsString;
  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd3_NM.Text := qryListImp_Name_3.AsString;
  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd4_NM.Text := qryListImp_Name_4.AsString;
  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd5_NM.Text := qryListImp_Name_5.AsString;
  //기타정보
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;
  //운송수단
  CM_INDEX(com_Carriage, [':'], 0, qryListCARRIAGE.AsString, 0);
//  edt_Carriage.Text := qryListCARRIAGE.AsString;
//  edt_Carriage1.Text := qryListCARRIAGE_NAME.AsString;
  //IL번호
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILAMT1.Value := qryListIL_AMT1.AsCurrency;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILAMT2.Value := qryListIL_AMT2.AsCurrency;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILAMT3.Value := qryListIL_AMT3.AsCurrency;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILAMT4.Value := qryListIL_AMT4.AsCurrency;
  edt_ILno5.Text := qryListIL_NO5.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;
  edt_ILAMT5.Value := qryListIL_AMT5.AsCurrency;

  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE1
//------------------------------------------------------------------------------
  edt_Doccd.Text   := qryListDOC_CD.AsString;
  edt_Doccd1.Text  := qryListDOC_Name.AsString;
  msk_exDate.Text  := qryListEX_DATE.AsString;
  edt_exPlace.Text := qryListEX_PLACE.AsString;

  edt_Applic1.Text := qryListAPPLIC1.AsString;
  edt_Applic2.Text := qryListAPPLIC2.AsString;
  edt_Applic3.Text := qryListAPPLIC3.AsString;
  edt_Applic4.Text := qryListAPPLIC4.AsString;
  edt_Applic5.Text := qryListAPPLIC5.AsString;

  edt_Benefc.Text  := qryListBENEFC.AsString;
  edt_Benefc1.Text := qryListBENEFC1.AsString;
  edt_Benefc2.Text := qryListBENEFC2.AsString;
  edt_Benefc3.Text := qryListBENEFC3.AsString;
  edt_Benefc4.Text := qryListBENEFC4.AsString;
  edt_Benefc5.Text := qryListBENEFC5.AsString;

  edt_Cdcur.Text := qryListCD_CUR.AsString;
  edt_Cdamt.Value := qrylistcd_amt.AsCurrency;
  edt_cdPerP.Value := qryListCD_PERP.AsCurrency;
  edt_cdPerM.Value := qryListCD_PERM.AsCurrency;
  edt_TermPR.Text := qryListTERM_PR.AsString;
  edt_TermPR_M.Text := qryListTERM_PR_M.AsString;
  edt_plTerm.Text := qryListPL_TERM.AsString;
  edt_Origin.Text := qryListORIGIN.AsString;
  edt_Origin_M.Text := qryListORIGIN_M.AsString;
  memo_Desgood1.Text := qryListDESGOOD_1.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE2
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := qryListAA_CV1.AsString;
  edt_aaCcv2.Text := qryListAA_CV2.AsString;
  edt_aaCcv3.Text := qryListAA_CV3.AsString;
  edt_aaCcv4.Text := qryListAA_CV4.AsString;

  edt_Draft1.Text := qryListDRAFT1.AsString;
  edt_Draft2.Text := qryListDRAFT2.AsString;
  edt_Draft3.Text := qryListDRAFT3.AsString;

  edt_mixPay1.Text := qryListMIX_PAY1.AsString;
  edt_mixPay2.Text := qryListMIX_PAY2.AsString;
  edt_mixPay3.Text := qryListMIX_PAY3.AsString;
  edt_mixPay4.Text := qryListMIX_PAY4.AsString;

  edt_defPay1.Text := qryListDEF_PAY1.AsString;
  edt_defPay2.Text := qryListDEF_PAY2.AsString;
  edt_defPay3.Text := qryListDEF_PAY3.AsString;
  edt_defPay4.Text := qryListDEF_PAY4.AsString;

//  7: ALLOWED (Transhipments are allowed)
//  8: NOT ALLOWED (Transhipments are not allowed)
  com_Pship.ItemIndex := AnsiIndexText(Trim(qryListPSHIP.AsString), ['', '9', '10']);
//9: ALLOWED (Partial shipments are allowed)
//10: NOT ALLOWED (Partial shipments are not allowed)
  com_TShip.ItemIndex := AnsiIndexText(Trim(qryListTSHIP.AsString), ['', '7', '8']);

  edt_loadOn.Text := qryListLOAD_ON.AsString;
  edt_forTran.Text := qryListFOR_TRAN.AsString;
  msk_lstDate.Text := qryListLST_DATE.AsString;

  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;

  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE3
//------------------------------------------------------------------------------
  chk_380.Checked := qryListDOC_380.AsBoolean;
  edt_380_1.Text := qryListDOC_380_1.AsString;

  chk_705.Checked := qryListDOC_705.AsBoolean;
  com_705Gubun.ItemIndex := AnsiIndexText( qryListDOC_705_GUBUN.AsString, ['','705','706','717','718','707'] );
  com_705GubunSelect(com_705Gubun);
  edt_705_1.Text := qryListDOC_705_1.AsString;
  edt_705_2.Text := qryListDOC_705_2.AsString;
  com_705_3.ItemIndex := AnsiIndexText(qryListDOC_705_3.AsString, ['','31','32']);
  edt_705_4.Text := qryListDOC_705_4.AsString;

  chk_740.Checked := qryListDOC_740.AsBoolean;
  edt_740_1.Text  := qryListDOC_740_1.AsString;
  edt_740_2.Text  := qryListDOC_740_2.AsString;
  com_740_3.ItemIndex := AnsiIndexText(qryListDOC_740_3.AsString, ['','31','32']);
  edt_740_4.Text  := qryListDOC_740_4.AsString;

  chk_760.Checked := qryListDOC_760.AsBoolean;
  edt_760_1.Text  := qryListDOC_760_1.AsString;
  edt_760_2.Text  := qryListDOC_760_2.AsString;
  com_760_3.ItemIndex := AnsiIndexText(qryListDOC_760_3.AsString, ['','31','32']);
  edt_760_4.Text  := qryListDOC_760_4.AsString;

  chk_530.Checked := qryListDOC_530.AsBoolean;
  edt_530_1.Text := qryListDOC_530_1.AsString;
  edt_530_2.Text := qryListDOC_530_2.AsString;

  chk_271.Checked := qryListDOC_271.AsBoolean;
  edt_271_1.Text := qryListDOC_271_1.AsString;

  chk_861.Checked := qryListDOC_861.AsBoolean;

  chk_2AA.Checked := qryListDOC_2AA.AsBoolean;
  memo_2AA_1.Text := qryListDOC_2AA_1.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE4
//------------------------------------------------------------------------------
  chk_ACD2AA.Checked := qryListACD_2AA.AsBoolean;
  edt_ACD2AA_1.Text := qryListACD_2AA_1.AsString;

  chk_ACD2AB.Checked := qryListACD_2AB.AsBoolean;
  chk_ACD2AC.Checked := qryListACD_2AC.AsBoolean;
  chk_ACD2AD.Checked := qryListACD_2AD.AsBoolean;
  chk_ACD2AE.Checked := qryListACD_2AE.AsBoolean;
  memo_ACD2AE_1.Text := qryListACD_2AE_1.AsString;

//2AF: APPLICANT
//2AG: BENEFICIARY
//2AH: 직접입력
  com_Charge.ItemIndex := AnsiIndexText(UpperCase(Trim(qryListCHARGE.AsString)), ['','2AF','2AG','2AH']);
  memo_Charge_1.Text := qryListCHARGE_1.AsString;

  edt_period.Text := qryListPERIOD.AsString;
  if qryListPERIOD_IDX.IsNull then
    com_PeriodIndex.ItemIndex := 0
  else
    com_PeriodIndex.ItemIndex := qryListPERIOD_IDX.AsInteger;
  edt_PeriodDetail.Text := qryListPERIOD_TXT.AsString;

  com_Confirm.ItemIndex := AnsiIndexText(qryListCONFIRMM.AsString, ['','DA','DB','DC'] );
  edt_CONFIRM_BICCD.Text := qryListCONFIRM_BICCD.AsString;
  edt_CONFIRM_BANKNM.Text := qryListCONFIRM_BANKNM.AsString;
  edt_CONFIRM_BANKBR.Text := qryListCONFIRM_BANKBR.AsString;

  memo_SPECIAL_PAY.Text := qryListSPECIAL_PAY.AsString;

end;

procedure TUI_APP700_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAMR.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_APP700_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_APP700_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_APP700_NEW_frm.chk_380Click(Sender: TObject);
var
  CMPNM : String;
  CMPCK : Boolean;
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  CMPCK := ProgramControlType <> ctView;

  edt_380_1.Enabled := chk_380.Checked AND CMPCK;

  com_705Gubun.Enabled := chk_705.Checked AND CMPCK;
  edt_705_1.Enabled := com_705Gubun.Enabled;
  edt_705_2.Enabled := com_705Gubun.Enabled;
  com_705_3.Enabled := com_705Gubun.Enabled;
  edt_705_4.Enabled := com_705Gubun.Enabled;

  edt_740_1.Enabled := chk_740.Checked AND CMPCK;
  edt_740_2.Enabled := edt_740_1.Enabled;
  com_740_3.Enabled := edt_740_1.Enabled;
  edt_740_4.Enabled := edt_740_1.Enabled;

  edt_760_1.Enabled := chk_760.Checked AND CMPCK;
  edt_760_2.Enabled := edt_760_1.Enabled;
  com_760_3.Enabled := edt_760_1.Enabled;
  edt_760_4.Enabled := edt_760_1.Enabled;

  edt_530_1.Enabled := chk_530.Checked AND CMPCK;
  edt_530_2.Enabled := edt_530_1.Enabled;

  edt_271_1.Enabled := chk_271.Checked AND CMPCK;

  memo_2AA_1.Enabled := chk_2AA.Checked AND CMPCK;

end;

procedure TUI_APP700_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);
  EnableControl(sPanel34, False);
  EnableControl(sPanel35, False);
  EnableControl(sPanel71, False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APP700_NEW_frm.btnCancelClick(Sender: TObject);
begin
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);
  EnableControl(sPanel34, False);
  EnableControl(sPanel35, False);
  EnableControl(sPanel71, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
end;

procedure TUI_APP700_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
  btnCopy.Enabled := btnNew.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_APP700_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsButton).Tag = 0 Then
  begin
    InitData;
  end;

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  EnableControl(sPanel6);
  EnableControl(sPanel7);
  EnableControl(sPanel27);
  EnableControl(sPanel34);
  EnableControl(sPanel35);
  EnableControl(sPanel71);

  chk_ACD2AAClick(chk_ACD2AA);
  chk_380Click(chk_380);
  com_CarriageSelect(nil);

  if ProgramControlType = ctmodify then
  begin
    EnableControl(sPanel6, false);
  end;

  //------------------------------------------------------------------------------
  // 버튼활성화
  //------------------------------------------------------------------------------

  sPanel29.Visible := ProgramControlType = ctInsert;

  ButtonEnable;
  IF ProgramControlType = ctInsert Then
    sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APP700_NEW_frm.InitData;
begin
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  ClearControl(sPanel34);
  ClearControl(sPanel35);
  ClearControl(sPanel71);

  //관리번호
  edt_MAINT_NO.Text  := DMAutoNo.GetDocumentNoAutoInc('APP700');
  edt_MSEQ.Text := '1';

  //헤더
  msk_Datee.Text     := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text    := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  //개설신청일
  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD',Now);

  //------------------------------------------------------------------------------
  // 발신기관 전자서명
  //------------------------------------------------------------------------------
  edt_EXName1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_EXName2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_EXName3.Text := DMCodeContents.ConfigSIGN.AsString;
  edt_EXAddr1.Text := DMCodeContents.ConfigADDR1.Text;
  edt_EXAddr2.Text := DMCodeContents.ConfigADDR2.Text;

  edt_Applic1.Text := DMCodeContents.ConfigENAME1.AsString;
  edt_Applic2.Text := DMCodeContents.ConfigEADDR1.Text;
  edt_Applic3.Text := DMCodeContents.ConfigEADDR2.Text;
  edt_Applic4.Text := DMCodeContents.ConfigEADDR3.Text;
  edt_Applic5.Text := DMCodeContents.ConfigENAME3.AsString;

//  edt_EXName1.Text := DMCodeContents.ConfigENAME1.AsString;
//  edt_EXName2.Text := DMCodeContents.ConfigENAME2.AsString;
//  edt_EXName3.Text := DMCodeContents.ConfigSAUP_NO.AsString;
//  edt_EXAddr1.Text := DMCodeContents.ConfigEADDR1.Text;
//  edt_EXAddr2.Text := DMCodeContents.ConfigEADDR2.Text;
end;

procedure TUI_APP700_NEW_frm.edt_IN_MATHODDblClick(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;
  uDialog := nil;
  IF ProgramControlType = ctView Then Exit;

  Case (Sender as TsEdit).Tag of
    //개설방법
    1000: uDialog := TCD_OPENDOC_frm.Create(Self);
    //은행
    1001, 1100, 1020: uDialog := TCD_BANK_frm.Create(Self);
    1101, 1019:
      uDialog := TCD_BIC_frm.Create(Self);
    //신용공여
    1002: uDialog := TCD_CREDITEX_frm.Create(Self);
    //수입용도
    1003, 1004, 1005, 1006, 1007: uDialog := TCD_IMP_CD_frm.Create(Self);
    1009, 1010, 1011, 1012, 1013, 1016: uDialog := TCD_AMT_UNIT_frm.Create(Self);
    1014: uDialog := TCD_DOC_CD_frm.Create(Self);
    //수익자(BEN)
    1015: uDialog := TCD_CUST_frm.Create(Self);
    //가격조건
    1017: uDialog := TCD_TERM_frm.Create(Self);
    //원산지
    1018: uDialog := TCD_NATION_frm.Create(Self);
  end;


  try
    if uDialog = nil Then Exit;

    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        //개설방법
        1000 :
        begin
          edt_IN_MATHOD_NM.Text := uDialog.Values[1];
        end;
        //개설의뢰은행
        1001 :
        begin
          edt_ApBank1.Text := uDialog.Values[1];
          edt_ApBank3.Text := uDialog.Values[2];
        end;
        //(희망)통지은행 - 은행명
        1100 :
        begin
          edt_AdBank_BIC.Clear;
          sEdit2.Clear;

          edt_AdBank1.Text := uDialog.Values[1];
          edt_AdBank3.Text := uDialog.Values[2];
        end;
        //(희망)통지은행 - BIC
        1101 :
        begin
          edt_AdBank.Clear;
          edt_AdBank1.Clear;
          edt_AdBank3.Clear;
          sEdit2.Text := uDialog.Values[1];
        end;
        //신용공여
        1002:
        begin
          edt_AdPay1.Text := uDialog.Values[1];
        end;
        //수입용도
        1003: edt_ImpCd1_NM.Text := uDialog.Values[1];
        1004: edt_ImpCd2_NM.Text := uDialog.Values[1];
        1005: edt_ImpCd3_NM.Text := uDialog.Values[1];
        1006: edt_ImpCd4_NM.Text := uDialog.Values[1];
        1007: edt_ImpCd5_NM.Text := uDialog.Values[1];
        1014: edt_Doccd1.Text := uDialog.Values[1];

        //수익자(BEN)
        1015:
        begin
          edt_Benefc1.Text := uDialog.FieldValues('ENAME');
          edt_Benefc2.Text := uDialog.FieldValues('REP_NAME');
          edt_Benefc3.Text := uDialog.FieldValues('ADDR1');
          edt_Benefc4.Text := uDialog.FieldValues('ADDR2');
        end;

//        1017: edt_TermPR_M.Text := uDialog.Values[1];

        1018: edt_Origin_M.Text := uDialog.Values[1];

        //확인은행 BIC
        1019: edt_CONFIRM_BICNM.Text := uDialog.Values[1];
        //확인은행 은행평문
        1020:
        begin
          edt_CONFIRM_BANKNM.Text := uDialog.Values[1];
          edt_CONFIRM_BANKBR.Text := uDialog.Values[2];          
        end;
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APP700_NEW_frm.com_CarriageSelect(Sender: TObject);
begin
  inherited;
  edt_loadOn.Enabled  := (com_Carriage.ItemIndex = 1) AND (ProgramControlType <> ctView);
  edt_forTran.Enabled := edt_loadOn.Enabled;
  IF edt_loadOn.Enabled Then
    sPanel89.Caption := '[PAGE2] 44A/44B를 입력하세요'
  else
    sPanel89.Caption := '';
end;

procedure TUI_APP700_NEW_frm.chk_ACD2AAClick(Sender: TObject);
var
  B_TEMP : Boolean;
begin
  inherited;
  B_TEMP := ProgramControlType <> ctView;
  edt_ACD2AA_1.Enabled := chk_ACD2AA.Checked AND B_TEMP;
  memo_Charge_1.Enabled := (com_Charge.ItemIndex = 3) AND B_TEMP;
  edt_PeriodDetail.Enabled := (com_PeriodIndex.ItemIndex = 1) AND B_TEMP;
  memo_ACD2AE_1.Enabled := chk_ACD2AE.Checked;  
//  memo_ACD2AE_1.Enabled := chk_ACD2AE.Checked AND B_TEMP;

  edt_CONFIRM_BICCD.Enabled := (com_Confirm.ItemIndex in [2,3]) AND B_TEMP;
  sEdit70.Enabled := edt_CONFIRM_BICCD.Enabled;
  edt_CONFIRM_BICNM.Enabled := edt_CONFIRM_BICCD.Enabled;
  edt_CONFIRM_BANKNM.Enabled := edt_CONFIRM_BICCD.Enabled;
  edt_CONFIRM_BANKBR.Enabled := edt_CONFIRM_BICCD.Enabled;

end;

procedure TUI_APP700_NEW_frm.btnTempClick(Sender: TObject);
var
  DOCNO : String;
begin
  inherited;
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '취소불능화환신용장 개설 신청서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK+MB_ICONERROR);
      IF DMMssql.inTrans Then DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);
  EnableControl(sPanel34, False);
  EnableControl(sPanel35, False);
  EnableControl(sPanel71, False);

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
  qryList.Locate('MAINT_NO',DOCNO,[]);

end;

function TUI_APP700_NEW_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  TempStr : String;
begin
  ErrMsg := TStringList.Create;
  try
    //------------------------------------------------------------------------------
    // 공통
    //------------------------------------------------------------------------------
    AddErrMsg(ErrMsg, msk_datee, '[공통 PAGE] 등록일자를 입력하세요');
    AddErrMsg(ErrMsg, edt_IN_MATHOD, '[공통 PAGE] 개설방법을 입력해야 합니다');
    AddErrMsg(ErrMsg, edt_ApBank, '[공통 PAGE] 개설(의뢰)은행을 입력해야합니다');
    AddErrMsg(ErrMsg, com_Carriage, '[공통 PAGE] 운송수단을 입력해야합니다');

    IF (Trim(edt_AdBank.Text+edt_AdBank1.Text+edt_AdBank3.Text) <> '') AND (Trim(edt_AdBank_BIC.Text+sEdit2.Text) <> '') Then
      ErrMsg.Add('[공통 PAGE] 희망통지은행은 BIC코드 또는 은행명중 하나만 입력 할 수 있습니다');
    //------------------------------------------------------------------------------
    // PAGE1
    //------------------------------------------------------------------------------
    AddErrMsg(ErrMsg, edt_Doccd, '[SWIFT PAGE1] Form of Documentary Credit를 입력해야 합니다');
    if Trim(msk_exDate.Text) = '' then
      ErrMsg.Add('[SWIFT PAGE1] Date of Expiry를 입력해야 합니다')
    else
    if (Trim(msk_exDate.Text) <> '') AND (StrToInt(msk_exDate.Text) < StrToInt(msk_Datee.Text)) Then
      ErrMsg.Add('[SWIFT PAGE1] 신청일자가 Date of Expiry보다 크면 안됩니다');

    if Trim(edt_exPlace.Text) = '' then
      ErrMsg.Add('[SWIFT PAGE1] Place of Expiry를 입력해야 합니다');

    if (Trim(edt_Benefc1.Text) = '') and (Trim(edt_Benefc2.Text) = '') and (Trim(edt_Benefc3.Text) = '') and (Trim(edt_Benefc4.Text) = '')  then
      ErrMsg.Add('[SWIFT PAGE1] Beneficiary를 입력해야 합니다');

    if (Trim(edt_Cdcur.Text) = '') or (Trim(edt_Cdamt.Text) = '') then
      ErrMsg.Add('[SWIFT PAGE1] Currency Code,Amount를입력해야 합니다');

    if (Trim(edt_TermPR.Text) = '') AND (Trim(edt_TermPR_M.Text) = '') then
      ErrMsg.Add('[SWIFT PAGE1] Terms of Price를입력해야 합니다')
    else
    begin
      IF not Assigned( CD_TERM_frm ) Then CD_TERM_frm := TCD_TERM_frm.Create(Self);
      try
        IF (Trim(edt_TermPR.Text) <> '') AND (CD_TERM_frm.SearchData(edt_TermPR.Text) = 0) AND (Trim(edt_TermPR_M.Text) = '') then
        begin
          ErrMsg.Add('[SWIFT PAGE1] Terms of Price 11개의 코드 외 다른코드 사용시에는 직접입력하셔야합니다')
        end;
      finally
        FreeAndNil(CD_TERM_frm);
      end;
    end;

    //------------------------------------------------------------------------------
    // PAGE2
    //------------------------------------------------------------------------------
//    if (Trim(msk_lstDate.Text) = '') Then
//      ErrMsg.Add('[SWIFT PAGE2] Latest Date of Shipment를 입력해야 합니다')
//    else

    if (Trim(msk_lstDate.Text) <> '') AND (StrToInt(msk_lstDate.Text) < StrToInt(msk_Datee.Text)) Then
      ErrMsg.Add('[SWIFT PAGE2] 신청일자가 Latest Date of Shipment 보다 클 수 없습니다');

    if (Trim(msk_lstDate.Text) <> '') AND (Trim(edt_shipPD1.Text)+Trim(edt_shipPD2.Text)+Trim(edt_shipPD3.Text)+Trim(edt_shipPD4.Text)+Trim(edt_shipPD5.Text) <> '') Then
      ErrMsg.Add('[SWIFT PAGE2] 최종선적일자(44C) 또는 선적기간(44D)는 둘 중 하나만 입력하셔야 합니다');

    //대금지불조건은 42C, 42M, 42P 중 하나만 입력되어야함
    IF ((Trim(edt_Draft1.Text)+Trim(edt_Draft2.Text)+Trim(edt_Draft3.Text)) = '') AND
       ((Trim(edt_mixPay1.Text)+Trim(edt_mixPay2.Text)+Trim(edt_mixPay3.Text)+Trim(edt_mixPay4.Text)) = '') AND
       ((Trim(edt_defPay1.Text)+Trim(edt_defPay2.Text)+Trim(edt_defPay3.Text)+Trim(edt_defPay4.Text)) = '')
    Then
    begin
      ErrMsg.Add('[SWIFT PAGE2] 대금 지불조건을 입력하세요');
    end;

    AddErrMsg(ErrMsg, com_Pship, '[Page2] Partial Shipment를 입력해야 합니다');
    AddErrMsg(ErrMsg, com_Tship, '[Page2] Transhipment를 입력해야 합니다');

    TempStr := Trim(CM_TEXT(com_Carriage,[':']));
    if (TempStr = 'DT') and (Trim(edt_loadOn.Text) <> '') AND edt_loadOn.Enabled then
      ErrMsg.Add('[Page2] 수탁(발송)지는 운송수단이 DQ 일때만 입력가능합니다.');
    if (TempStr = 'DT') and (Trim(edt_forTran.Text) <> '') AND edt_forTran.Enabled then
      ErrMsg.Add('[Page2] 최종목적지는 운송수단이 DQ 일때만 입력가능합니다.');
    if (TempStr = 'DT') and (AnsiMatchText('',[Trim(edt_SunjukPort.Text), Trim(edt_dochackPort.Text)])) then
      ErrMsg.Add('[Page2] 운송수단이 "DT"일때 선적항/도착항은 필수입력입니다');

    //------------------------------------------------------------------------------
    // PAGE3
    //------------------------------------------------------------------------------
    IF not(chk_380.Checked OR chk_705.Checked OR chk_740.Checked OR chk_760.Checked OR chk_530.Checked OR chk_271.Checked OR chk_861.Checked OR chk_2AA.Checked) then
      ErrMsg.Add('[Page3] Documents Required를 입력해야 합니다');

    IF chk_380.Checked and (Trim(edt_380_1.Text) = '') Then
      ErrMsg.Add('[Page3] COMMERCIAL INVOICE를 입력하세요');

    IF chk_705.Checked AND ((com_705Gubun.ItemIndex = 0) OR (com_705_3.ItemIndex = 0)) Then
      ErrMsg.Add('[Page3] BILL OF LANDING을 입력해주세요');

    IF chk_740.Checked AND (com_740_3.ItemIndex = 0) then
      ErrMsg.Add('[Page3] AIR WAY BILL를 입력해주세요');

    IF chk_760.Checked AND (com_760_3.ItemIndex = 0) then
      ErrMsg.Add('[Page3] Multimodal/combined transport Document(generic)을 입력하세요');     

    IF (chk_271.Checked) AND (Trim(edt_271_1.Text) = '') Then
      ErrMsg.Add('[Page3] PACKING LIST 를 입력하세요');

    TempStr := Trim(CM_TEXT(com_Carriage,[':']));
    IF (TempStr = 'DT') and chk_760.Checked Then
      ErrMsg.Add('[Page3] 운송수단이 DT(해상/항공)일때는 주요구비서류의 복합운송관련부분을 입력할 수 없습니다');
    IF (TempStr = 'DQ') and chk_705.Checked Then
      ErrMsg.Add('[Page3] 운송수단이 DQ(복합운송/기타)일때는 주요구비서류의 B/L부분을 입력할 수 없습니다');
    IF (TempStr = 'DQ') and chk_740.Checked Then
      ErrMsg.Add('[Page3] 운송수단이 DQ(복합운송/기타)일때는 주요구비서류의 Air Waybill부분을 입력할 수 없습니다');

//------------------------------------------------------------------------------
// PAGE4
//------------------------------------------------------------------------------
    IF (Trim(edt_CONFIRM_BANKNM.Text+edt_CONFIRM_BANKBR.Text) <> '') AND (Trim(edt_CONFIRM_BICCD.Text) <> '') Then
      ErrMsg.Add('[PAGE4] 확인은행은 BIC코드 또는 은행명중 하나만 입력 할 수 있습니다');

    IF com_PeriodIndex.ItemIndex = -1 Then
      ErrMsg.Add('[PAGE4] 서류제시기간을 선택하세요');

    IF (com_PeriodIndex.ItemIndex = 1) AND (Trim(edt_PeriodDetail.Text) = '') Then
      ErrMsg.Add('[PAGE4] 서류제시기간 상세내역을 입력하세요');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

function TUI_APP700_NEW_frm.DataProcess(nType : integer): String;
var
  MAINT_NO : string;
  SC : TSQLCreate;
  TempStr : String;
  nIdx : integer;
begin
  SC := TSQLCreate.Create;
  try
    Result := edt_MAINT_NO.Text;

    with SC do
    begin
      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',Result);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',Result);
        end;
      end;

      SQLHeader('APP700_1');

      //문서저장구분(0:임시, 1:저장)
      ADDValue('CHK2',nType);
      //등록일자
      ADDValue('DATEE',msk_Datee.Text);
      //유저ID
      ADDValue('USER_ID',edt_userno.Text);
      //메세지1
      ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
      //메세지2
      ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));
      //개설신청일
      ADDValue('APP_DATE', msk_APP_DATE.Text);

    //----------------------------------------------------------------------
    // 개설은행
    //----------------------------------------------------------------------
      //개설방법
      ADDValue('IN_MATHOD',edt_In_Mathod.Text);

      //개설은행
      ADDValue('AP_BANK',edt_ApBank.Text);
      ADDValue('AP_BANK1',CopyK(edt_ApBank1.Text, 1,35));
      ADDValue('AP_BANK2',CopyK(edt_ApBank1.Text,36,35));
      ADDValue('AP_BANK3',CopyK(edt_ApBank3.Text, 1,35));
      ADDValue('AP_BANK4',CopyK(edt_ApBank3.Text,36,35));
      ADDValue('AP_BANK5',edt_ApBank5.Text);

    //----------------------------------------------------------------------
    //통지은행
    //----------------------------------------------------------------------
      //평문
      ADDValue('AD_BANK',edt_AdBank.Text);
      ADDValue('AD_BANK1',CopyK(edt_AdBank1.Text, 1 ,35));
      ADDValue('AD_BANK2',CopyK(edt_AdBank1.Text,36, 35));
      ADDValue('AD_BANK3',CopyK(edt_AdBank3.Text, 1, 35));
      ADDValue('AD_BANK4',CopyK(edt_AdBank3.Text,36, 35));
      //BIC코드
      ADDValue('AD_BANK_BIC', Trim(edt_AdBank_BIC.Text));
      //신용공여
      ADDValue('AD_PAY',edt_AdPay.Text);

    //----------------------------------------------------------------------
    //수입용도
    //----------------------------------------------------------------------
      ADDValue('IMP_CD1',edt_ImpCd1.Text);
      ADDValue('IMP_CD2',edt_ImpCd2.Text);
      ADDValue('IMP_CD3',edt_ImpCd3.Text);
      ADDValue('IMP_CD4',edt_ImpCd4.Text);
      ADDValue('IMP_CD5',edt_ImpCd5.Text);

    //----------------------------------------------------------------------
    //I/L번호
    //----------------------------------------------------------------------
      ADDValue('IL_NO1',edt_ILno1.Text);
      ADDValue('IL_NO2',edt_ILno2.Text);
      ADDValue('IL_NO3',edt_ILno3.Text);
      ADDValue('IL_NO4',edt_ILno4.Text);
      ADDValue('IL_NO5',edt_ILno5.Text);

      ADDValue('IL_CUR1',edt_ILCur1.Text);
      ADDValue('IL_CUR2',edt_ILCur2.Text);
      ADDValue('IL_CUR3',edt_ILCur3.Text);
      ADDValue('IL_CUR4',edt_ILCur4.Text);
      ADDValue('IL_CUR5',edt_ILCur5.Text);

      ADDValue('IL_AMT1',edt_ILAMT1.Text);
      ADDValue('IL_AMT2',edt_ILAMT2.Text);
      ADDValue('IL_AMT3',edt_ILAMT3.Text);
      ADDValue('IL_AMT4',edt_ILAMT4.Text);
      ADDValue('IL_AMT5',edt_ILAMT5.Text);

    //----------------------------------------------------------------------
    //기타정보
    //----------------------------------------------------------------------
      ADDValue('AD_INFO1',edt_AdInfo1.Text);
      ADDValue('AD_INFO2',edt_AdInfo2.Text);
      ADDValue('AD_INFO3',edt_AdInfo3.Text);
      ADDValue('AD_INFO4',edt_AdInfo4.Text);
      ADDValue('AD_INFO5',edt_AdInfo5.Text);

    //----------------------------------------------------------------------
    //Form of Documentary Credit
    //----------------------------------------------------------------------
      ADDValue('DOC_CD',edt_Doccd.Text);

    //----------------------------------------------------------------------
    //Date and Place of Expiry
    //----------------------------------------------------------------------
      ADDValue('EX_DATE', msk_exDate.Text);
      ADDValue('EX_PLACE',edt_exPlace.Text);
    end;

    ExecSQL(SC.CreateSQL);

    with SC do
    begin
      SQLHeader('APP700_2');

      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO', Result);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO', Result);
        end;
      end;

      //----------------------------------------------------------------------
      //Applicant
      //----------------------------------------------------------------------
        ADDValue('APPLIC1',edt_Applic1.Text);
        ADDValue('APPLIC2',edt_Applic2.Text);
        ADDValue('APPLIC3',edt_Applic3.Text);
        ADDValue('APPLIC4',edt_Applic4.Text);
        ADDValue('APPLIC5',edt_Applic5.Text);

      //----------------------------------------------------------------------
      //Beneficiary
      //----------------------------------------------------------------------
        ADDValue('BENEFC',edt_Benefc.Text);
        ADDValue('BENEFC1',edt_Benefc1.Text);
        ADDValue('BENEFC2',edt_Benefc2.Text);
        ADDValue('BENEFC3',edt_Benefc3.Text);
        ADDValue('BENEFC4',edt_Benefc4.Text);
        ADDValue('BENEFC5',edt_Benefc5.Text);

      //----------------------------------------------------------------------
      //Currency Code,Amount
      //----------------------------------------------------------------------
        ADDValue('CD_CUR',edt_Cdcur.Text);
        ADDValue('CD_AMT',edt_Cdamt.Text);

      //----------------------------------------------------------------------
      //Percentage Credit Amount Tolerance
      //----------------------------------------------------------------------
       ADDValue('CD_PERP',edt_cdPerP.Text);
       ADDValue('CD_PERM',edt_cdPerM.Text);

      //----------------------------------------------------------------------
      //Maximum Credit Amount
      // 11월 스위프트변경에서 삭제
      //----------------------------------------------------------------------
//        ADDValue('CD_MAX',edt_CdMax.Text);

      //----------------------------------------------------------------------
      //Additional Amount Covered
      //----------------------------------------------------------------------
        ADDValue('AA_CV1',edt_aaCcv1.Text);
        ADDValue('AA_CV2',edt_aaCcv2.Text);
        ADDValue('AA_CV3',edt_aaCcv3.Text);
        ADDValue('AA_CV4',edt_aaCcv4.Text);

      //----------------------------------------------------------------------
      //Drafts at ... (화환어음조건)
      //----------------------------------------------------------------------
        ADDValue('DRAFT1',edt_Draft1.Text);
        ADDValue('DRAFT2',edt_Draft2.Text);
        ADDValue('DRAFT3',edt_Draft3.Text);          

      //----------------------------------------------------------------------
      //Mixed Payment Detail(혼합지급조건)
      //----------------------------------------------------------------------
        ADDValue('MIX_PAY1',edt_mixPay1.Text);
        ADDValue('MIX_PAY2',edt_MixPay2.Text);
        ADDValue('MIX_PAY3',edt_MixPay3.Text);
        ADDValue('MIX_PAY4',edt_MixPay4.Text);

      //----------------------------------------------------------------------
      //Deferred Payment Details(연지급조건명세)
      //----------------------------------------------------------------------
        ADDValue('DEF_PAY1',edt_defPay1.Text);
        ADDValue('DEF_PAY2',edt_defPay2.Text);
        ADDValue('DEF_PAY3',edt_defPay3.Text);
        ADDValue('DEF_PAY4',edt_defPay4.Text);

      //----------------------------------------------------------------------
      //Description of Goods and/or Services
      //----------------------------------------------------------------------
        ADDValue('DESGOOD_1',memo_Desgood1.Lines.Text);

      //----------------------------------------------------------------------
      //Latest Date of Shipment
      //----------------------------------------------------------------------
        ADDValue('LST_DATE',msk_lstDate.Text);

      //----------------------------------------------------------------------
      //Shipment Period
      //----------------------------------------------------------------------
        ADDValue('SHIP_PD1',edt_shipPD1.Text);
        ADDValue('SHIP_PD2',edt_shipPD2.Text);
        ADDValue('SHIP_PD3',edt_shipPD3.Text);
        ADDValue('SHIP_PD4',edt_shipPD4.Text);
        ADDValue('SHIP_PD5',edt_shipPD5.Text);
        ADDValue('SHIP_PD6',edt_shipPD6.Text);

      //----------------------------------------------------------------------
      //Partial Shipment
      //----------------------------------------------------------------------
        ADDValue('PSHIP',CM_TEXT(com_Pship,[':']));

      //----------------------------------------------------------------------
      //Transhipment
      //----------------------------------------------------------------------
        ADDValue('TSHIP',CM_TEXT(com_tship,[':']));

        ADDValue('LOAD_ON',edt_loadOn.Text);
        ADDValue('FOR_TRAN',edt_forTran.Text);

//      IF com_Carriage.ItemIndex <> 1 Then
//      begin
//        //----------------------------------------------------------------------
//        //Place of Taking in Charge/Dispatch from ···/Place of Receipt
//        //----------------------------------------------------------------------
//          ADDValue('LOAD_ON','');
//
//        //----------------------------------------------------------------------
//        //Place of Final Destination/For Transportation to ···/Place of Delivery
//        //----------------------------------------------------------------------
//          ADDValue('FOR_TRAN','');
//      end
//      else
//      IF com_Carriage.ItemIndex = 1 Then      
//      begin
//      //----------------------------------------------------------------------
//      //Place of Taking in Charge/Dispatch from ···/Place of Receipt
//      //----------------------------------------------------------------------
//        ADDValue('LOAD_ON',edt_loadOn.Text);
//
//      //----------------------------------------------------------------------
//      //Place of Final Destination/For Transportation to ···/Place of Delivery
//      //----------------------------------------------------------------------
//        ADDValue('FOR_TRAN',edt_forTran.Text);
//      end;
    end;

    ExecSQL(SC.CreateSQL);

    with SC do
    begin
      SQLHeader('APP700_3');

      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',Result);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',Result);
        end;
      end;

      //----------------------------------------------------------------------
      //명의인정보
      //----------------------------------------------------------------------
      //명의인
        ADDValue('EX_NAME1',edt_EXName1.Text);
        ADDValue('EX_NAME2',edt_EXName2.Text);
      //식별부호
        ADDValue('EX_NAME3',edt_EXName3.Text);
      //주소
        ADDValue('EX_ADDR1',edt_EXAddr1.Text);
        ADDValue('EX_ADDR2',edt_EXAddr2.Text);

      //----------------------------------------------------------------------
      //Terms of Price
      //----------------------------------------------------------------------
        ADDValue('TERM_PR',edt_TermPR.Text);
        ADDValue('TERM_PR_M',edt_TermPR_M.Text);

      //----------------------------------------------------------------------
      //Place of Terms of Price
      //----------------------------------------------------------------------
        ADDValue('PL_TERM',edt_plTerm.Text);

      //----------------------------------------------------------------------
      //운송수단
      //----------------------------------------------------------------------
        ADDValue('CARRIAGE',CM_TEXT(com_Carriage,[':']));

        ADDValue('SUNJUCK_PORT',edt_SunjukPort.Text);
        ADDValue('DOCHACK_PORT',edt_dochackPort.Text);
        
//      IF com_Carriage.ItemIndex = 2 Then
//      begin
//      //----------------------------------------------------------------------
//      //선적항 Port of Loading/Airport of Departure
//      //----------------------------------------------------------------------
//        ADDValue('SUNJUCK_PORT',edt_SunjukPort.Text);
//
//      //----------------------------------------------------------------------
//      //도착항 Port of Discharge/Airport of Destination
//      //----------------------------------------------------------------------
//        ADDValue('DOCHACK_PORT',edt_dochackPort.Text);
//      end
//      else
//      IF com_Carriage.ItemIndex <> 2 Then
//      begin
//      //----------------------------------------------------------------------
//      //선적항 Port of Loading/Airport of Departure
//      //----------------------------------------------------------------------
//        ADDValue('SUNJUCK_PORT','');
//
//      //----------------------------------------------------------------------
//      //도착항 Port of Discharge/Airport of Destination
//      //----------------------------------------------------------------------
//        ADDValue('DOCHACK_PORT','');
//      end;

      //----------------------------------------------------------------------
      //Country of Origin
      //----------------------------------------------------------------------
        ADDValue('ORIGIN',edt_Origin.Text);
        ADDValue('ORIGIN_M',edt_Origin_M.Text);

      //----------------------------------------------------------------------
      //Documents Required ...
      //----------------------------------------------------------------------
        //ASSIGNED COMMERCIAL
        if chk_380.Checked = True then
        begin
          ADDValue('DOC_380','1');
          ADDValue('DOC_380_1',edt_380_1.Text);
        end
        else
        begin
          ADDValue('DOC_380','0');
          ADDValue('DOC_380_1',0);
        end;

        //--------------------------------------------------------------------
        //FULL SET OF CLEAN ON BOARD OCEAN
        //--------------------------------------------------------------------
        if chk_705.Checked Then
        begin
          ADDValue('DOC_705', '1');
          ADDValue('DOC_705_GUBUN',CM_TEXT( com_705Gubun,[':'] ));
          ADDValue('DOC_705_1',edt_705_1.Text);
          ADDValue('DOC_705_2',edt_705_2.Text);
          ADDValue('DOC_705_3',CM_TEXT( com_705_3, [':'] ));
          ADDValue('DOC_705_4',edt_705_4.Text);
        end
        else
        begin
          ADDValue('DOC_705', '0');
          ADDValue('DOC_705_GUBUN','');
          ADDValue('DOC_705_1','');
          ADDValue('DOC_705_2','');
          ADDValue('DOC_705_3','');
          ADDValue('DOC_705_4','');
        end;

        //--------------------------------------------------------------------
        //AIRWAY VILL CONSIGNED TO
        //--------------------------------------------------------------------
        if chk_740.Checked then
        begin
          ADDValue('DOC_740','1');
          ADDValue('DOC_740_1',edt_740_1.Text);
          ADDValue('DOC_740_2',edt_740_2.Text);
          ADDValue('DOC_740_3',CM_TEXT(com_740_3,[':']));
          ADDValue('DOC_740_4',edt_740_4.Text);
        end
        else
        begin
          ADDValue('DOC_740','0');
          ADDValue('DOC_740_1','');
          ADDValue('DOC_740_2','');
          ADDValue('DOC_740_3','');
          ADDValue('DOC_740_4','');
        end;

        //--------------------------------------------------------------------
        //FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO
        //--------------------------------------------------------------------
        if chk_760.Checked = True then
        begin
          ADDValue('DOC_760','1');
          ADDValue('DOC_760_1',edt_760_1.Text);
          ADDValue('DOC_760_2',edt_760_2.Text);
          ADDValue('DOC_760_3',CM_TEXT(com_760_3, [':']));
          ADDValue('DOC_760_4',edt_760_4.Text);
        end
        else
        begin
          ADDValue('DOC_760','0');
          ADDValue('DOC_760_1','');
          ADDValue('DOC_760_2','');
          ADDValue('DOC_760_3','');
          ADDValue('DOC_760_4','');
        end;

        //--------------------------------------------------------------------
        //FULL SET OF INSURANCE POLICIES
        //--------------------------------------------------------------------
        if chk_530.Checked then
        begin
          ADDValue('DOC_530','1');
          ADDValue('DOC_530_1',edt_530_1.Text);
          ADDValue('DOC_530_2',edt_530_2.Text);
        end
        else
        begin
          ADDValue('DOC_530','0');
          ADDValue('DOC_530_1','');
          ADDValue('DOC_530_2','');
        end;

        //--------------------------------------------------------------------
        //PACKING LIST IN
        //--------------------------------------------------------------------
        if chk_271.Checked then
        begin
          ADDValue('DOC_271','1');
          ADDValue('DOC_271_1',edt_271_1.Text);
        end
        else
        begin
          ADDValue('DOC_271','0');
          ADDValue('DOC_271_1',0);
        end;

        //--------------------------------------------------------------------
        //CRETIFICATE OF ORIGIN
        //--------------------------------------------------------------------
        if chk_861.Checked = True then
          ADDValue('DOC_861','1')
        else
          ADDValue('DOC_861','0');

        //--------------------------------------------------------------------
        //OTHER DOCUMENT(S) ( if any )
        //--------------------------------------------------------------------
        if chk_2AA.Checked = True then
        begin
          ADDValue('DOC_2AA','1');
          ADDValue('DOC_2AA_1',memo_2AA_1.Lines.Text);
        end
        else
        begin
          ADDValue('DOC_2AA','0');
          ADDValue('DOC_2AA_1','');
        end;

      //----------------------------------------------------------------------
      //(C)Additional conditions..
      //----------------------------------------------------------------------
        //SHIPMENT BY
        if chk_ACD2AA.Checked then
        begin
          ADDValue('ACD_2AA','1');
          ADDValue('ACD_2AA_1',edt_ACD2AA_1.Text);
        end
        else
        begin
          ADDValue('ACD_2AA','0');
          ADDValue('ACD_2AA_1','');
        end;

        //--------------------------------------------------------------------
        //ACCEPTANCE COMMISSION  DISCOUN
        //--------------------------------------------------------------------
        if chk_ACD2AB.Checked then
          ADDValue('ACD_2AB','1')
        else
          ADDValue('ACD_2AB','0');

        //--------------------------------------------------------------------
        //ALL DOCUMENTS MUST BEAR OUR CR
        //--------------------------------------------------------------------
        if chk_ACD2AC.Checked then
          ADDValue('ACD_2AC','1')
        else
          ADDValue('ACD_2AC','0');

       //--------------------------------------------------------------------
       //LATE PRESENTATION B/L ACCEPTAB
       //--------------------------------------------------------------------
       if chk_ACD2AD.Checked = True then
         ADDValue('ACD_2AD','1')
       else
         ADDValue('ACD_2AD','0');

       //--------------------------------------------------------------------
       //OTHER CONDITION(S)
       //--------------------------------------------------------------------
       if chk_ACD2AE.Checked then
       begin
         ADDValue('ACD_2AE','1');
         ADDValue('ACD_2AE_1',memo_ACD2AE_1.Text);
       end
       else
       begin
         ADDValue('ACD_2AE','0');
         ADDValue('ACD_2AE_1','');
       end;

       //--------------------------------------------------------------------
       //CHARGE
       //--------------------------------------------------------------------
       ADDValue('CHARGE',CM_TEXT(com_Charge,[':']));
       IF CM_TEXT(com_Charge,[':']) = '2AH' Then
         ADDValue('CHARGE_1',memo_Charge_1.Text)
       else
         ADDValue('CHARGE_1','');

       //--------------------------------------------------------------------
       //Period for Presentation
       //--------------------------------------------------------------------
       ADDValue('PERIOD', StrToIntDef(edt_period.Text,0));
       ADDValue('PERIOD_IDX', com_PeriodIndex.ItemIndex);
       IF com_PeriodIndex.ItemIndex = 1 Then
         ADDValue('PERIOD_TXT', edt_PeriodDetail.Text)
       else
         ADDValue('PERIOD_TXT', '');

       //--------------------------------------------------------------------
       //Confirmation Instructions
       //--------------------------------------------------------------------
       ADDValue('CONFIRMM',CM_TEXT(com_Confirm,[':']));

       //--------------------------------------------------------------------
       //Requested Confirmation Party  58A
       //--------------------------------------------------------------------
       ADDValue('CONFIRM_BICCD', edt_CONFIRM_BICCD.Text);
       ADDValue('CONFIRM_BANKNM', edt_CONFIRM_BANKNM.Text);
       ADDValue('CONFIRM_BANKBR', edt_CONFIRM_BANKBR.Text);

       //--------------------------------------------------------------------
       //Special Payment Conditions   49G
       //--------------------------------------------------------------------
       ADDValue('SPECIAL_PAY', memo_SPECIAL_PAY.Text);

    end;
    ExecSQL(SC.CreateSQL);

  finally
    SC.Free;
  end;
end;

procedure TUI_APP700_NEW_frm.edt_AdBank_BICExit(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  uDialog := TCD_BIC_frm.Create(Self);
  sEdit2.Text := uDialog.GetCodeText(edt_AdBank_BIC.Text);
end;

procedure TUI_APP700_NEW_frm.btnDelClick(Sender: TObject);
var
  MAINT_NO : string;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;

    MAINT_NO := qryListMAINT_NO.AsString;

    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+'취소불능화신용장 개설신청서'#13#10+MAINT_NO+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text :=  'DELETE FROM APP700_1 WHERE MAINT_NO =' + QuotedStr(MAINT_NO)+' AND MSEQ = '+qryListMSEQ.AsString;
          ExecSQL;

          SQL.Text :=  'DELETE FROM APP700_2 WHERE MAINT_NO =' + QuotedStr(MAINT_NO)+' AND MSEQ = '+qryListMSEQ.AsString;
          ExecSQL;

          SQL.Text :=  'DELETE FROM APP700_3 WHERE MAINT_NO =' + QuotedStr(MAINT_NO)+' AND MSEQ = '+qryListMSEQ.AsString;

          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;

          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.Last;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans; 
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APP700_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  dlg_CopyAPP700fromAPP700_frm := Tdlg_CopyAPP700fromAPP700_frm.Create(Self);
  try
    IF dlg_CopyAPP700fromAPP700_frm.OpenDialog(0) Then
    begin
      ClearControl(sPanel7);
      ClearControl(sPanel27);
      ClearControl(sPanel34);
      ClearControl(sPanel35);
      ClearControl(sPanel71);

      CopyData;

      ProgramControlType := ctInsert;      

      EnableControl(sPanel6);
      EnableControl(sPanel7);
      EnableControl(sPanel27);
      EnableControl(sPanel34);
      EnableControl(sPanel35);
      EnableControl(sPanel71);

      chk_ACD2AAClick(chk_ACD2AA);
      chk_380Click(chk_380);
      com_CarriageSelect(nil);

      //------------------------------------------------------------------------------
      // 버튼활성화
      //------------------------------------------------------------------------------

      sPanel29.Visible := ProgramControlType = ctInsert;

      ButtonEnable;
      IF ProgramControlType = ctInsert Then
        sPageControl1.ActivePageIndex := 0;

    end;

  finally
    FreeAndNil(dlg_CopyAPP700fromAPP700_frm);
  end;
end;

procedure TUI_APP700_NEW_frm.CopyData;
begin
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  ClearControl(sPanel34);
  ClearControl(sPanel35);
  ClearControl(sPanel71);

  //관리번호
  edt_MAINT_NO.Text  := DMAutoNo.GetDocumentNoAutoInc('APP700');
  edt_MSEQ.Text := '1';

  //헤더
  msk_Datee.Text     := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text    := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  //개설신청일
  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD',Now);


  edt_IN_MATHOD.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IN_MATHOD');
  edt_IN_MATHOD_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('mathod_Name');
  //개설은행
  edt_ApBank.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK');
  edt_ApBank1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK1') + dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK2');
  edt_ApBank3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK3') + dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK4');
  edt_ApBank5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AP_BANK5');
  //통지은행
  edt_AdBank.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_BANK');
  edt_AdBank1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Ad_BANK1') + dlg_CopyAPP700fromAPP700_frm.FieldString('Ad_BANK2');
  edt_ADBank3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Ad_BANK3') + dlg_CopyAPP700fromAPP700_frm.FieldString('Ad_BANK4');
  //통지은행 BIC코드(추가)
  edt_AdBank_BIC.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_BANK_BIC');
  IF Trim(edt_AdBank_BIC.Text) = '' Then sEdit2.Clear;

  edt_AdPay.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_PAY');
  edt_AdPay1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('pay_Name');
  //수입용도
  edt_ImpCd1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IMP_CD1');
  edt_ImpCd1_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Imp_Name_1');
  edt_ImpCd2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IMP_CD2');
  edt_ImpCd2_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Imp_Name_2');
  edt_ImpCd3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IMP_CD3');
  edt_ImpCd3_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Imp_Name_3');
  edt_ImpCd4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IMP_CD4');
  edt_ImpCd4_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Imp_Name_4');
  edt_ImpCd5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IMP_CD5');
  edt_ImpCd5_NM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('Imp_Name_5');
  //기타정보
  edt_AdInfo1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_INFO1');
  edt_AdInfo2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_INFO2');
  edt_AdInfo3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_INFO3');
  edt_AdInfo4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_INFO4');
  edt_AdInfo5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AD_INFO5');
  //운송수단
  CM_INDEX(com_Carriage, [':'], 0, dlg_CopyAPP700fromAPP700_frm.FieldString('CARRIAGE'), 0);
//  edt_Carriage.Text := qryListCARRIAGE.AsString;
//  edt_Carriage1.Text := qryListCARRIAGE_NAME.AsString;
  //IL번호
  edt_ILno1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_NO1');
  edt_ILCur1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_CUR1');
  edt_ILAMT1.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('IL_AMT1');
  edt_ILno2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_NO2');
  edt_ILCur2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_CUR2');
  edt_ILAMT2.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('IL_AMT2');
  edt_ILno3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_NO3');
  edt_ILCur3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_CUR3');
  edt_ILAMT3.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('IL_AMT3');
  edt_ILno4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_NO4');
  edt_ILCur4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_CUR4');
  edt_ILAMT4.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('IL_AMT4');
  edt_ILno5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_NO5');
  edt_ILCur5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('IL_CUR5');
  edt_ILAMT5.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('IL_AMT5');

  edt_EXName1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_NAME1');
  edt_EXName2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_NAME2');
  edt_EXName1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_NAME3');
  edt_EXAddr1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_ADDR1');
  edt_EXAddr2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_ADDR2');

//------------------------------------------------------------------------------
// SWIFT PAGE1
//------------------------------------------------------------------------------
  edt_Doccd.Text   := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_CD');
  edt_Doccd1.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_Name');
  msk_exDate.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_DATE');
  edt_exPlace.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('EX_PLACE');

  edt_Applic1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('APPLIC1');
  edt_Applic2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('APPLIC2');
  edt_Applic3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('APPLIC3');
  edt_Applic4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('APPLIC4');
  edt_Applic5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('APPLIC5');

  edt_Benefc.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC');
  edt_Benefc1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC1');
  edt_Benefc2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC2');
  edt_Benefc3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC3');
  edt_Benefc4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC4');
  edt_Benefc5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('BENEFC5');

  edt_Cdcur.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('CD_CUR');
  edt_Cdamt.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('cd_amt');
  edt_cdPerP.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('CD_PERP');
  edt_cdPerM.Value := dlg_CopyAPP700fromAPP700_frm.FieldCurr('CD_PERM');
  edt_TermPR.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('TERM_PR');
  edt_TermPR_M.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('TERM_PR_M');
  edt_plTerm.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('PL_TERM');
  edt_Origin.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('ORIGIN');
  edt_Origin_M.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('ORIGIN_M');
  memo_Desgood1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DESGOOD_1');

//------------------------------------------------------------------------------
// SWIFT PAGE2
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AA_CV1');
  edt_aaCcv2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AA_CV2');
  edt_aaCcv3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AA_CV3');
  edt_aaCcv4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('AA_CV4');

  edt_Draft1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DRAFT1');
  edt_Draft2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DRAFT2');
  edt_Draft3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DRAFT3');

  edt_mixPay1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('MIX_PAY1');
  edt_mixPay2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('MIX_PAY2');
  edt_mixPay3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('MIX_PAY3');
  edt_mixPay4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('MIX_PAY4');

  edt_defPay1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DEF_PAY1');
  edt_defPay2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DEF_PAY2');
  edt_defPay3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DEF_PAY3');
  edt_defPay4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DEF_PAY4');

//  7: ALLOWED (Transhipments are allowed)
//  8: NOT ALLOWED (Transhipments are not allowed)
  com_Pship.ItemIndex := AnsiIndexText(Trim(dlg_CopyAPP700fromAPP700_frm.FieldString('PSHIP')), ['', '9', '10']);
//9: ALLOWED (Partial shipments are allowed)
//10: NOT ALLOWED (Partial shipments are not allowed)
  com_TShip.ItemIndex := AnsiIndexText(Trim(dlg_CopyAPP700fromAPP700_frm.FieldString('TSHIP')), ['', '7', '8']);

  edt_loadOn.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('LOAD_ON');
  edt_forTran.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('FOR_TRAN');
  msk_lstDate.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('LST_DATE');

  edt_shipPD1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SHIP_PD1');
  edt_shipPD2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SHIP_PD2');
  edt_shipPD3.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SHIP_PD3');
  edt_shipPD4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SHIP_PD4');
  edt_shipPD5.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SHIP_PD5');

  edt_SunjukPort.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SUNJUCK_PORT');
  edt_dochackPort.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOCHACK_PORT');

//------------------------------------------------------------------------------
// SWIFT PAGE3
//------------------------------------------------------------------------------
  chk_380.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_380');
  edt_380_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_380_1');

  chk_705.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_705');
  com_705Gubun.ItemIndex := AnsiIndexText( dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_705_GUBUN'), ['','705','706','717','718','707'] );
  com_705GubunSelect(com_705Gubun);
  edt_705_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_705_1');
  edt_705_2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_705_2');
  com_705_3.ItemIndex := AnsiIndexText(dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_705_3'), ['','31','32']);
  edt_705_4.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_705_4');

  chk_740.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_740');
  edt_740_1.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_740_1');
  edt_740_2.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_740_2');
  com_740_3.ItemIndex := AnsiIndexText(dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_740_3'), ['','31','32']);
  edt_740_4.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_740_4');

  chk_760.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_760');
  edt_760_1.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_760_1');
  edt_760_2.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_760_2');
  com_760_3.ItemIndex := AnsiIndexText(dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_760_3'), ['','31','32']);
  edt_760_4.Text  := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_760_4');

  chk_530.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_530');
  edt_530_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_530_1');
  edt_530_2.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_530_2');

  chk_271.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_271');
  edt_271_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_271_1');

  chk_861.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_861');

  chk_2AA.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('DOC_2AA');
  memo_2AA_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('DOC_2AA_1');

//------------------------------------------------------------------------------
// SWIFT PAGE4
//------------------------------------------------------------------------------
  chk_ACD2AA.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('ACD_2AA');
  edt_ACD2AA_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('ACD_2AA_1');

  chk_ACD2AB.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('ACD_2AB');
  chk_ACD2AC.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('ACD_2AC');
  chk_ACD2AD.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('ACD_2AD');
  chk_ACD2AE.Checked := dlg_CopyAPP700fromAPP700_frm.FieldBool('ACD_2AE');
  memo_ACD2AE_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('ACD_2AE_1');

  com_Charge.ItemIndex := AnsiIndexText(UpperCase(Trim(dlg_CopyAPP700fromAPP700_frm.FieldString('CHARGE'))), ['','2AF','2AG','2AH']);
  memo_Charge_1.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('CHARGE_1');

  edt_period.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('PERIOD');
  if dlg_CopyAPP700fromAPP700_frm.qryListPERIOD.IsNull then
    com_PeriodIndex.ItemIndex := 0
  else
    com_PeriodIndex.ItemIndex := dlg_CopyAPP700fromAPP700_frm.FieldInt('PERIOD_IDX');
    
  edt_PeriodDetail.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('PERIOD_TXT');

  com_Confirm.ItemIndex := AnsiIndexText(dlg_CopyAPP700fromAPP700_frm.FieldString('CONFIRMM'), ['','DA','DB','DC'] );
  edt_CONFIRM_BICCD.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('CONFIRM_BICCD');
  edt_CONFIRM_BANKNM.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('CONFIRM_BANKNM');
  edt_CONFIRM_BANKBR.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('CONFIRM_BANKBR');

  memo_SPECIAL_PAY.Text := dlg_CopyAPP700fromAPP700_frm.FieldString('SPECIAL_PAY');

end;

procedure TUI_APP700_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_APP700_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_APP700_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APP700_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APP700_NEW_frm := nil;
end;

procedure TUI_APP700_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_APP700_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvSeq, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
  sKEY : String;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvSeq := qryListMSEQ.AsString;
//      RecvFlat := APP700(RecvDoc, RecvCode);
      RecvFlat := APP700_NEW(RecvDoc, RecvSeq, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APP700';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MSEQ').Value := RecvSeq;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APP700_1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        sKEY := qryListMAINT_NO.AsString;
        qryList.Close;
        qryList.Open;
        qryList.Locate('MAINT_NO', sKEY, []);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APP700_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application);
end;

procedure TUI_APP700_NEW_frm.sButton13Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(LivartModule_frm) Then
    LivartModule_frm := TLivartModule_frm.Create(Self);
  try
    LivartModule_frm.sPageControl1.ActivePageIndex := 0;
    LivartModule_frm.ShowModal;

    qryList.Close;
    qrylist.Open;

  finally
    FreeAndNil(LivartModule_frm);
  end;
end;

procedure TUI_APP700_NEW_frm.x1Click(Sender: TObject);
begin
  inherited;
  
  with ADOStoredProc1 do
  begin
    Close;
    Parameters.Refresh;
    Parameters[1].Value := qryListMAINT_NO.AsString;
    try
      DMMssql.BeginTrans;
      ExecProc;
      DMMssql.CommitTrans;
      ShowMessage('응답서 작성완료');
    except
      on E:Exception do
      begin
        DMMssql.RollbackTrans;
        ShowMessage('에러가 발생하여 작업이 취소되었습니다'#13#10+E.Message);
      end;
    end;
  end;
end;

procedure TUI_APP700_NEW_frm.edt_IN_MATHODExit(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;
  uDialog := nil;
  IF ProgramControlType = ctView Then Exit;

  Case (Sender as TsEdit).Tag of
    //개설방법
    1000: uDialog := TCD_OPENDOC_frm.Create(Self);
    //은행
    1001, 1100, 1020: uDialog := TCD_BANK_frm.Create(Self);
    1101, 1019:
      uDialog := TCD_BIC_frm.Create(Self);
    //신용공여
    1002: uDialog := TCD_CREDITEX_frm.Create(Self);
    //수입용도
    1003, 1004, 1005, 1006, 1007: uDialog := TCD_IMP_CD_frm.Create(Self);
    1009, 1010, 1011, 1012, 1013, 1016: uDialog := TCD_AMT_UNIT_frm.Create(Self);
    1014: uDialog := TCD_DOC_CD_frm.Create(Self);
    //수익자(BEN)
    1015: uDialog := TCD_CUST_frm.Create(Self);
    //가격조건
    1017: uDialog := TCD_TERM_frm.Create(Self);
    //원산지
    1018: uDialog := TCD_NATION_frm.Create(Self);
  end;

  try
    if uDialog = nil Then Exit;

    Case (Sender as TsEdit).Tag of
      1000: edt_IN_MATHOD_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1002: edt_AdPay1.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1003: edt_ImpCd1_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1004: edt_ImpCd2_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1005: edt_ImpCd3_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1006: edt_ImpCd4_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      1007: edt_ImpCd5_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
    end;

  finally
    FreeAndNil(uDialog);
  end;

end;

end.
