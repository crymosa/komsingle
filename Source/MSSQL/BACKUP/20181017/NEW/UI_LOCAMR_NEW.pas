unit UI_LOCAMR_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sButton, sLabel, Buttons,
  sSpeedButton, ExtCtrls, sPanel, sComboBox, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl, sBitBtn, Mask,
  sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, DB, ADODB, StrUtils, DateUtils, TypeDefine;

type  
  TUI_LOCAMR_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    sLabel3: TsLabel;
    edt_wcd: TsEdit;
    edt_wsangho: TsEdit;
    edt_wceo: TsEdit;
    edt_waddr1: TsEdit;
    edt_waddr2: TsEdit;
    edt_waddr3: TsEdit;
    edt_rcd: TsEdit;
    edt_rsaupno: TsEdit;
    edt_rsangho: TsEdit;
    edt_rceo: TsEdit;
    edt_remail: TsEdit;
    edt_raddr1: TsEdit;
    edt_raddr2: TsEdit;
    edt_raddr3: TsEdit;
    edt_iden1: TsEdit;
    edt_iden2: TsEdit;
    edt_doc1: TsEdit;
    edt_doc2: TsEdit;
    edt_doc3: TsEdit;
    edt_doc4: TsEdit;
    edt_doc5: TsEdit;
    edt_wsaupno: TsEdit;
    edt_cbank: TsEdit;
    edt_cbanknm: TsEdit;
    edt_cbrunch: TsEdit;
    edt_cType: TsEdit;
    sEdit8: TsEdit;
    edt_LOCAMT_UNIT: TsEdit;
    edt_perr: TsEdit;
    cur_LOCAMT: TsCurrencyEdit;
    edt_merr: TsEdit;
    edt_lcno: TsEdit;
    edt_doc6: TsEdit;
    edt_doc7: TsEdit;
    edt_doc8: TsEdit;
    edt_doc9: TsEdit;
    cur_chasu: TsCurrencyEdit;
    msk_IndoDt: TsMaskEdit;
    msk_ExpiryDT: TsMaskEdit;
    sPanel8: TsPanel;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    memo_Etc: TsMemo;
    memo_etcamd: TsMemo;
    edt_sign1: TsEdit;
    edt_sign2: TsEdit;
    edt_sign3: TsEdit;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sMaskEdit5: TsMaskEdit;
    sMaskEdit6: TsMaskEdit;
    sPanel2: TsPanel;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    sPanel1: TsPanel;
    sPanel13: TsPanel;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListAPP_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListISSBANK: TStringField;
    qryListISSBANK1: TStringField;
    qryListISSBANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListCHGINFO: TStringField;
    qryListCHGINFO1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    sPanel14: TsPanel;
    qryListDOCNO: TStringField;
    edt_MSEQ: TsEdit;
    qryReady: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edt_wcdDblClick(Sender: TObject);
    procedure edt_cbankDblClick(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure btnTempClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ButtonEnable;

    procedure InitData;
    procedure NewDoc;

    procedure InitDataFromLOCADV;
    procedure InitDataFromLOCAMA;
    function DataProcess(nType: integer): TDOCNO;
    function CHECK_VALIDITY: String;
    procedure ReadyDocument(DocNo: String; MSEQ : integer);
    { Private declarations }
  protected
    function getCompareDocno(DOCNO : String):Integer;  
    procedure ReadList(SortField: TColumn=nil); virtual;
    procedure ReadData; virtual;
  public
    { Public declarations }
  end;

var
  UI_LOCAMR_NEW_frm: TUI_LOCAMR_NEW_frm;

implementation

uses
  MSSQL, dlg_CDC_LOCAMR, VarDefine, AutoNo, CodeContents,
  CD_CUST, CodeDialogParent, CD_BANK, CD_LOCTYPE, CD_AMT_UNIT, dlg_CopyLOCAMRfromLOCADV,
  dlg_SelectCopyDocument, dlg_CopyLOCAMRfromLOCAMA, Dlg_ErrorMessage, SQLCreator, MessageDefine, Dlg_RecvSelect, CreateDocuments,
  DocumentSend, Preview, LOCAMR_PRINT;

{$R *.dfm}

procedure TUI_LOCAMR_NEW_frm.ReadList(SortField : TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAMR.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.FormShow(Sender: TObject);
begin
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOCAMR_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_LOCAMR_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.ReadData;
var
  TempStr : String;
begin
  ClearControl(sPanel6);
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  
  with qryList do
  begin
    if RecordCount > 0 then
    begin
      edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
      edt_MSEQ.Text := qryListMSEQ.AsString;

      msk_Datee.Text := qryListDATEE.AsString;
      edt_userno.Text := qryListUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
      CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);      

      //개설일자
      sMaskEdit5.Text := qryListISS_DATE.AsString;
      //조건변경신청일
      sMaskEdit6.Text := qryListAPP_DATE.AsString;
      //조건변경차수
      cur_chasu.Text := qryListAMD_NO.AsString;

      //개설의뢰인
      edt_wcd.Text     := qryListAPPLIC.AsString;
      edt_wsangho.Text := qryListAPPLIC1.AsString;
      edt_wceo.Text    := qryListAPPLIC2.AsString;
      edt_waddr1.Text  := qryListAPPADDR1.AsString;
      edt_waddr2.Text  := qryListAPPADDR2.AsString;
      edt_waddr3.Text  := qryListAPPADDR3.AsString;
      edt_wsaupno.Text := qryListAPPLIC3.AsString;

      //수혜자
      edt_rcd.Text     := qryListBENEFC.AsString;
      edt_rsangho.Text := qryListBENEFC1.AsString;
      edt_rceo.Text    := qryListBENEFC2.AsString;
      edt_raddr1.Text  := qryListBNFADDR1.AsString;
      edt_raddr2.Text  := qryListBNFADDR2.AsString;
      edt_raddr3.Text  := qryListBNFADDR3.AsString;
      edt_rsaupno.Text := qryListBENEFC3.AsString;
      
      TempStr := qryListBNFEMAILID.AsString+ '@' +qryListBNFDOMAIN.AsString;
      IF TempStr = '@' Then
        edt_remail.Clear
      else
        edt_remail.Text  := TempStr;

      //수발신인식별자
      edt_iden1.Text := qryListCHKNAME1.AsString;
      edt_iden2.Text := qryListCHKNAME2.AsString;

      //개설은행
      edt_cbank.Text   := qryListISSBANK.AsString;
      edt_cbanknm.Text := qryListISSBANK1.AsString;
      edt_cbrunch.Text := qryListISSBANK2.AsString;

      //신용장종류
      edt_cType.Text := qryListLOC_TYPE.AsString;
      sEdit8.Text := qryListLOC_TYPENAME.AsString;

      //개설금액
      edt_LOCAMT_UNIT.Text := qryListLOC_AMTC.AsString;
      cur_LOCAMT.Value := qryListLOC_AMT.AsCurrency;

      //허용오차
      //+
      edt_perr.Text := qryListCD_PERP.AsString;
      //-
      edt_merr.Text := qryListCD_PERM.AsString;

      //LC번호
      edt_lcno.Text := qryListLC_NO.AsString;

      //매도확약서
      edt_doc1.Text := qryListOFFERNO1.AsString;
      edt_doc2.Text := qryListOFFERNO2.AsString;
      edt_doc3.Text := qryListOFFERNO3.AsString;
      edt_doc4.Text := qryListOFFERNO4.AsString;
      edt_doc5.Text := qryListOFFERNO5.AsString;
      edt_doc6.Text := qryListOFFERNO6.AsString;
      edt_doc7.Text := qryListOFFERNO7.AsString;
      edt_doc8.Text := qryListOFFERNO8.AsString;
      edt_doc9.Text := qryListOFFERNO9.AsString;

      memo_Etc.Text    := qryListREMARK1.AsString;
      memo_etcamd.Text := qryListCHGINFO1.AsString;

      //물품인도기일
      msk_IndoDt.Text := qryListDELIVERY.AsString;
      //유효기일
      msk_ExpiryDT.Text := qryListEXPIRY.AsString;

      //발신기관 전자서명
      edt_Sign1.Text := qrylistEXNAME1.AsString;
      edt_Sign2.Text := qrylistEXNAME2.AsString;
      edt_Sign3.Text := qrylistEXNAME3.AsString;
    end;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_LOCAMR_NEW_frm.btnNewClick(Sender: TObject);
begin
//------------------------------------------------------------------------------
// 데이터 선택
//------------------------------------------------------------------------------
  IF (Sender as TsButton).Tag = 0 Then
  begin
    dlg_CDC_LOCAMR_frm := Tdlg_CDC_LOCAMR_frm.Create(Self);
    try
      Case dlg_CDC_LOCAMR_frm.ShowModal of
        //내국신용장 개설응답서 가져오기
        mrOk     :
        begin
          dlg_CopyLOCAMRfromLOCADV_frm := Tdlg_CopyLOCAMRfromLOCADV_frm.Create(Self);
          try
            if dlg_CopyLOCAMRfromLOCADV_frm.ShowModal = mrCancel Then Abort;

            InitDataFromLOCADV;
          
          finally
            FreeAndNil(dlg_CopyLOCAMRfromLOCADV_frm);
          end;
        end;
        //내국신용장 조건변경 통지서 가져오기
        mrYes    :
        begin
          dlg_CopyLOCAMRfromLOCAMA_frm := Tdlg_CopyLOCAMRfromLOCAMA_frm.Create(Self);
          try
            if dlg_CopyLOCAMRfromLOCAMA_frm.ShowModal = mrCancel Then Abort;

            InitDataFromLOCAMA;

          finally
            FreeAndNil(dlg_CopyLOCAMRfromLOCAMA_frm);
          end;
        end;
        //신규작성
        mrIgnore :
        begin
          InitData;
          NewDoc;
        end;
        //취소
        mrCancel : Abort;
      end;
    finally
      FreeAndNil( dlg_CDC_LOCAMR_frm );
    end;
  end;

  EnableControl(sPanel6);
  EnableControl(sPanel7);
  EnableControl(sPanel27);

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  if ProgramControlType = ctmodify then
  begin
    EnableControl(sPanel6, false);
  end;

  //------------------------------------------------------------------------------
  // 버튼활성화
  //------------------------------------------------------------------------------

  sPanel29.Visible := ProgramControlType = ctInsert;

  ButtonEnable;
  sPageControl1.ActivePageIndex := 0;

end;

procedure TUI_LOCAMR_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
//  btnCopy.Enabled := btnNew.Enabled;
  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_LOCAMR_NEW_frm.btnCancelClick(Sender: TObject);
begin
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
end;

procedure TUI_LOCAMR_NEW_frm.InitData;
begin
  ClearControl(sPanel7);
  ClearControl(sPanel27);

  //관리번호
  edt_MAINT_NO.Text  := DMAutoNo.GetDocumentNoAutoInc('LOCAMR');
  edt_MSEQ.Text := '1';

  //헤더
  msk_Datee.Text     := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text    := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  //------------------------------------------------------------------------------
  // 발신기관 전자서명
  //------------------------------------------------------------------------------
  edt_Sign1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_Sign2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_Sign3.Text := DMCodeContents.ConfigSIGN.AsString;
end;

procedure TUI_LOCAMR_NEW_frm.NewDoc;
begin
  //조건변경 신청일
  sMaskEdit6.Text := FormatDateTime('YYYYMMDD', Now);
  cur_chasu.Value := 1;
  //------------------------------------------------------------------------------
  // 개설의뢰인 셋팅
  //------------------------------------------------------------------------------
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    edt_wcd.Text     :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    edt_wsangho.Text :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_wceo.Text    :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_waddr1.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    edt_waddr2.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    edt_waddr3.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    edt_wsaupno.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
  end;


end;

procedure TUI_LOCAMR_NEW_frm.edt_wcdDblClick(Sender: TObject);
begin
//  inherited;
  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        101:
          begin
            edt_wsaupno.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_wsangho.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_wceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_waddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_waddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_waddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
          end;

        102:
          begin
            edt_rsaupno.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_rsangho.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_rceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_raddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_raddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_raddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
            IF CD_CUST_frm.FieldValues('EMAIL_ID')+'@'+CD_CUST_frm.FieldValues('EMAIL_DOMAIN') <> '@' Then
              edt_remail.Text := CD_CUST_frm.FieldValues('EMAIL_ID')+'@'+CD_CUST_frm.FieldValues('EMAIL_DOMAIN')
            else
              edt_remail.Clear;
          end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_LOCAMR_NEW_frm.edt_cbankDblClick(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  Case (Sender as TsEdit).Tag of
    //개설은행
    1000: uDialog := TCD_BANK_frm.Create(Self);
    //신용장종류
    1001: uDialog := TCD_LOCTYPE_frm.Create(Self);
    //개설금액통화
    1002: uDialog := TCD_AMT_UNIT_frm.Create(Self);
//    1003: uDialog := TCD_LOCBASIC_frm.Create(Self);
  end;

  try
    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        //개설은행
        1000 :
        begin
          edt_cbanknm.Text := uDialog.Values[1];
          edt_cbrunch.Text := uDialog.Values[2];
        end;
        //신용장종류
        1001:
        begin
          sEdit8.Text := uDialog.Values[1];
        end;
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_LOCAMR_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_LOCAMR_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  if Column.FieldName = 'DOCNO' Then
  begin
    Column.Title.Caption := 'L/C번호';
    Column.FieldName := 'LC_NO';
  end
  else
  if Column.FieldName = 'LC_NO' Then
  begin
    Column.Title.Caption := '관리번호';
    Column.FieldName := 'DOCNO';
  end;
end;

procedure TUI_LOCAMR_NEW_frm.InitDataFromLOCADV;
var
  TempStr : string;
  nPos : Integer;
begin
  ClearControl(sPanel7);
  ClearControl(sPanel27);

  //관리번호
  edt_MAINT_NO.Text  := dlg_CopyLOCAMRfromLOCADV_frm.FieldString('MAINT_NO');
  edt_MSEQ.Text := IntToStr(getCompareDocno(edt_MAINT_NO.Text));

  //헤더
  msk_Datee.Text     := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text    := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  with dlg_CopyLOCAMRfromLOCADV_frm do
  begin
    //개설일자
    sMaskEdit5.Text := FieldString('ISS_DATE');
    //조건변경신청일
    sMaskEdit6.Text := FormatDateTime('YYYYMMDD', Now);
    //조건변경차수
    cur_chasu.Text := '1';

    //개설의뢰인
//    edt_wcd.Text     := FieldString('APPLIC');
    edt_wcd.Clear;
    edt_wsangho.Text := FieldString('APPLIC1');
    edt_wceo.Text    := FieldString('APPLIC2');
    edt_waddr1.Text  := FieldString('APPADDR1');
    edt_waddr2.Text  := FieldString('APPADDR2');
    edt_waddr3.Text  := FieldString('APPADDR3');
    edt_wsaupno.Text := FieldString('APPLIC3');

    //수혜자
//    edt_rcd.Text     := FieldString('BENEFC');
    edt_rcd.Clear;
    edt_rsangho.Text := FieldString('BENEFC1');
    edt_rceo.Text    := FieldString('BENEFC2');
    edt_raddr1.Text  := FieldString('BNFADDR1');
    edt_raddr2.Text  := FieldString('BNFADDR2');
    edt_raddr3.Text  := FieldString('BNFADDR3');
    TempStr := FieldString('BENEFC3');

    edt_rsaupno.Clear;
    edt_iden1.Clear;
    edt_iden2.Clear;
    IF LeftStr(TempStr, 1) <> '/' Then
    begin
      edt_rsaupno.Text := LeftStr( TempStr , 10);
      TempStr := MidStr(TempStr, 11, 100);
      nPos := Pos('/',TempStr);
      edt_iden1.Text := Trim(LeftStr(TempStr, nPos-1));
      edt_iden2.Text := Trim(MidStr(TempStr, nPos+1, 100));
    end;

    TempStr := Trim(FieldString('BNFEMAILID'))+'@'+Trim(FieldString('BNFDOMAIN'));
    IF TempStr = '@' Then
      edt_remail.Clear
    else
      edt_remail.Text  := TempStr;

    //내국신용장번호
    edt_lcno.Text := FieldString('LC_NO');
    //개설은행
    edt_cbank.Text   := FieldString('AP_BANK');
    edt_cbanknm.Text := FieldString('AP_BANK1');
    edt_cbrunch.Text := FieldString('AP_BANK2');

    //매도확약서
    edt_doc1.Text := FieldString('OFFERNO1');
    edt_doc2.Text := FieldString('OFFERNO2');
    edt_doc3.Text := FieldString('OFFERNO3');
    edt_doc4.Text := FieldString('OFFERNO4');
    edt_doc5.Text := FieldString('OFFERNO5');
    edt_doc6.Text := FieldString('OFFERNO6');
    edt_doc7.Text := FieldString('OFFERNO7');
    edt_doc8.Text := FieldString('OFFERNO8');
    edt_doc9.Text := FieldString('OFFERNO9');

  end;

  //------------------------------------------------------------------------------
  // 발신기관 전자서명
  //------------------------------------------------------------------------------
  edt_Sign1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_Sign2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_Sign3.Text := DMCodeContents.ConfigSIGN.AsString;
end;

function TUI_LOCAMR_NEW_frm.getCompareDocno(DOCNO: String): Integer;
begin
  Result := 1;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(MSEQ),0) as MAX_SEQ FROM LOCAMR WHERE MAINT_NO = '+QuotedStr(DOCNO);
      Open;

      Result := FieldByName('MAX_SEQ').AsInteger+1;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.InitDataFromLOCAMA;
var
  TempStr : string;
  nPos : Integer;
begin
  ClearControl(sPanel7);
  ClearControl(sPanel27);

  //관리번호
  edt_MAINT_NO.Text  := dlg_CopyLOCAMRfromLOCAMA_frm.FieldString('MAINT_NO');
  edt_MSEQ.Text := IntToStr(getCompareDocno(edt_MAINT_NO.Text));

  //헤더
  msk_Datee.Text     := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text    := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  with dlg_CopyLOCAMRfromLOCAMA_frm do
  begin
    //개설일자
    sMaskEdit5.Text := FieldString('ISS_DATE');
    //조건변경신청일
    sMaskEdit6.Text := FormatDateTime('YYYYMMDD', Now);
    //조건변경차수
    cur_chasu.Text := '1';

    //개설의뢰인
//    edt_wcd.Text     := FieldString('APPLIC');
    edt_wcd.Clear;
    edt_wsangho.Text := FieldString('APPLIC1');
    edt_wceo.Text    := FieldString('APPLIC2');
    edt_waddr1.Text  := FieldString('APPADDR1');
    edt_waddr2.Text  := FieldString('APPADDR2');
    edt_waddr3.Text  := FieldString('APPADDR3');
    edt_wsaupno.Text := FieldString('APPLIC3');

    //수혜자
//    edt_rcd.Text     := FieldString('BENEFC');
    edt_rcd.Clear;
    edt_rsangho.Text := FieldString('BENEFC1');
    edt_rceo.Text    := FieldString('BENEFC2');
    edt_raddr1.Text  := FieldString('BNFADDR1');
    edt_raddr2.Text  := FieldString('BNFADDR2');
    edt_raddr3.Text  := FieldString('BNFADDR3');
    TempStr := FieldString('BENEFC3');

    edt_rsaupno.Clear;
    edt_iden1.Clear;
    edt_iden2.Clear;
    IF LeftStr(TempStr, 1) <> '/' Then
    begin
      edt_rsaupno.Text := LeftStr( TempStr , 10);
      TempStr := MidStr(TempStr, 11, 100);
      nPos := Pos('/',TempStr);
      edt_iden1.Text := Trim(LeftStr(TempStr, nPos-1));
      edt_iden2.Text := Trim(MidStr(TempStr, nPos+1, 100));
    end;

    TempStr := Trim(FieldString('BNFEMAILID'))+'@'+Trim(FieldString('BNFDOMAIN'));
    IF TempStr = '@' Then
      edt_remail.Clear
    else
      edt_remail.Text  := TempStr;

    //내국신용장번호
    edt_lcno.Text := FieldString('LC_NO');
    //개설은행
    edt_cbank.Text   := FieldString('ISSBANK');
    edt_cbanknm.Text := FieldString('ISSBANK1');
    edt_cbrunch.Text := FieldString('ISSBANK2');

    //매도확약서
    edt_doc1.Text := FieldString('OFFERNO1');
    edt_doc2.Text := FieldString('OFFERNO2');
    edt_doc3.Text := FieldString('OFFERNO3');
    edt_doc4.Text := FieldString('OFFERNO4');
    edt_doc5.Text := FieldString('OFFERNO5');
    edt_doc6.Text := FieldString('OFFERNO6');
    edt_doc7.Text := FieldString('OFFERNO7');
    edt_doc8.Text := FieldString('OFFERNO8');
    edt_doc9.Text := FieldString('OFFERNO9');

  end;

  //------------------------------------------------------------------------------
  // 발신기관 전자서명
  //------------------------------------------------------------------------------
  edt_Sign1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_Sign2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_Sign3.Text := DMCodeContents.ConfigSIGN.AsString;
end;

procedure TUI_LOCAMR_NEW_frm.btnTempClick(Sender: TObject);
var
  DOCNO : TDOCNO;
begin
  inherited;
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '내국신용장 조건변경 신청서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK+MB_ICONERROR);
      IF DMMssql.inTrans Then DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
  qryList.Locate('MAINT_NO;MSEQ;AMD_NO',VarArrayOf([DOCNO.MAINT_NO, DOCNO.MSEQ, DOCNO.AMD_NO]),[]);
end;

function TUI_LOCAMR_NEW_frm.DataProcess(nType : integer): TDOCNO;
var
  MAINT_NO : string;
  SQLCreate : TSQLCreate;
  TempStr : String;
  nIdx : integer;
begin
  IF nType = 2 Then Exit;
  //------------------------------------------------------------------------------
  // SQL생성기
  //------------------------------------------------------------------------------
  SQLCreate := TSQLCreate.Create;
  try
    Result.MAINT_NO := edt_MAINT_NO.Text;
    Result.MSEQ     := StrToInt(edt_MSEQ.Text);
    Result.AMD_NO   := StrToInt(cur_chasu.Text);

    with SQLCreate do
    begin
      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',Result.MAINT_NO);
          ADDValue('MSEQ',Result.MSEQ);
          ADDValue('AMD_NO',Result.AMD_NO);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',Result.MAINT_NO);
          ADDWhere('MSEQ',Result.MSEQ);
          ADDWhere('AMD_NO',Result.AMD_NO);
        end;
      end;

      SQLHeader('LOCAMR');

      //문서저장구분(0:임시, 1:저장)
      ADDValue('CHK2',nType);
      //등록일자
      ADDValue('DATEE',msk_Datee.Text);
      //유저ID
      ADDValue('USER_ID',edt_userno.Text);
      //메세지1
      ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
      //메세지2
      ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));

      //내국신용장번호
      ADDValue('LC_NO', edt_lcno.Text);
      //물품매도확약서번호
      ADDValue('OFFERNO1', Trim(edt_doc1.Text));
      ADDValue('OFFERNO2', Trim(edt_doc2.Text));
      ADDValue('OFFERNO3', Trim(edt_doc3.Text));
      ADDValue('OFFERNO4', Trim(edt_doc4.Text));
      ADDValue('OFFERNO5', Trim(edt_doc5.Text));
      ADDValue('OFFERNO6', Trim(edt_doc6.Text));
      ADDValue('OFFERNO7', Trim(edt_doc7.Text));
      ADDValue('OFFERNO8', Trim(edt_doc8.Text));
      ADDValue('OFFERNO9', Trim(edt_doc9.Text));      

      //변경신청일
      ADDValue('APP_DATE', sMaskEdit6.Text);
      //개설일자
      ADDValue('ISS_DATE', sMaskEdit5.Text);
      //기타정보
      IF (memo_Etc.Lines.Count = 0) AND (Trim(memo_Etc.Text) = '') Then
        ADDValue('REMARK', 'N')
      else
        ADDValue('REMARK', 'Y');
      ADDValue('REMARK1', memo_Etc.Text);

      //개설은행
      ADDValue('ISSBANK',  Trim(edt_cbank.Text));
      ADDValue('ISSBANK1', Trim(edt_cbanknm.Text));
      ADDValue('ISSBANK2', Trim(edt_cbrunch.Text));

      //개설의뢰인
      ADDValue('APPLIC', edt_wcd.Text);
      ADDValue('APPLIC1', edt_wsangho.Text);
      ADDValue('APPLIC2', edt_wceo.Text);
      ADDValue('APPLIC3', edt_wsaupno.Text);
      //수혜자
      ADDValue('BENEFC', edt_rcd.Text);
      ADDValue('BENEFC1', edt_rsangho.Text);
      ADDValue('BENEFC2', edt_rceo.Text);
      ADDValue('BENEFC3', edt_rsaupno.Text);
      //발신기관 전자서명
      ADDValue('EXNAME1', edt_sign1.Text);
      ADDValue('EXNAME2', edt_sign2.Text);
      ADDValue('EXNAME3', edt_sign3.Text);
      //물품인도기일
      ADDValue('DELIVERY', msk_IndoDt.Text);
      //유효기일
      ADDValue('EXPIRY', msk_ExpiryDT.Text);

      //기타조건 변경사항
      IF (memo_etcamd.Lines.Count = 0) AND (Trim(memo_etcamd.Text) = '') Then
        ADDValue('CHGINFO', 'N')
      else
        ADDValue('CHGINFO', 'Y');
      ADDValue('CHGINFO1', memo_etcamd.Text);

      //신용장종류
      ADDValue('LOC_TYPE', edt_cType.Text);
      //개설금액
      ADDValue('LOC_AMT', cur_LOCAMT.Text, vtvariant );
      ADDValue('LOC_AMTC', edt_LOCAMT_UNIT.Text);

      //수발신인식별자
      ADDValue('CHKNAME1', edt_iden1.Text);
      //상세수발신인식별자
      ADDValue('CHKNAME2', edt_iden2.Text);

      //개설자 주소
      ADDValue('APPADDR1', edt_waddr1.Text);
      ADDValue('APPADDR2', edt_waddr2.Text);
      ADDValue('APPADDR3', edt_waddr3.Text);
      //수혜자 주소
      ADDValue('BNFADDR1', edt_raddr1.Text);
      ADDValue('BNFADDR2', edt_raddr2.Text);
      ADDValue('BNFADDR3', edt_raddr3.Text);
      //이메일 주소
      TempStr := edt_remail.Text;
      IF Trim(TempStr) <> '' Then
      begin
        nIdx := Pos('@', TempStr);
        ADDValue('BNFEMAILID', LeftStr(TempStr,nIdx-1));
        ADDValue('BNFDOMAIN', MidStr(TempStr, nIdx+1, Length(TempStr)-nIdx));
      end
      else
      begin
        ADDValue('BNFEMAILID', '');
        ADDValue('BNFDOMAIN', '');
      end;
      
      //허용오차
      ADDValue('CD_PERP', edt_perr.Text);
      ADDValue('CD_PERM', edt_merr.Text);
    end;

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate.CreateSQL;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;
  finally
    SQLCreate.Free;
  end;
end;

function TUI_LOCAMR_NEW_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;
begin
  ErrMsg := TStringList.Create;

  Try
    IF Trim(edt_wsaupno.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 사업자등록번호를 입력하세요');
    IF Trim(edt_wsangho.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 상호를 입력하세요');

    IF Trim(edt_rsaupno.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 사업자등록번호를 입력하세요');
    IF Trim(edt_rsangho.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 상호를 입력하세요');

    IF Trim(edt_cbank.Text) = '' THEN
      ErrMsg.Add('[공통] 개설은행을 입력하세요');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;


procedure TUI_LOCAMR_NEW_frm.btnDelClick(Sender: TObject);
var
  DocNo : TDOCNO;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;

    DocNo.MAINT_NO := qryListMAINT_NO.AsString;
    DocNo.MSEQ := qryListMSEQ.AsInteger;
    DocNo.AMD_NO := qryListAMD_NO.AsInteger;

    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+'내국신용장 조건변경통지서'#13#10+DocNo.MAINT_NO+'/'+inttostr(DocNo.MSEQ)+'/'+inttostr(DocNo.AMD_NO)+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCAMR WHERE MAINT_NO = '+QuotedStr(DocNo.MAINT_NO)+' AND MSEQ = '+IntToStr(DocNo.MSEQ)+' AND AMD_NO = '+IntToStr(DocNo.AMD_NO);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;

          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.Last;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans; 
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString, qryListMSEQ.AsInteger)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_LOCAMR_NEW_frm.ReadyDocument(DocNo: String; MSEQ : integer);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
  RecvSeq, RecvAMD_NO : integer;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := DocNo;
      RecvSeq := MSEQ;
      RecvAMD_NO := qryListAMD_NO.AsInteger;

      RecvFlat := LOCAMR(RecvDoc,RecvSeq, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCAMR';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := MSEQ;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCAMR';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        ReadList;
        qryList.Locate('MAINT_NO;MSEQ;AMD_NO',VarArrayOf([RecvDoc, RecvSeq, RecvAMD_NO]),[]);

//        ReadListBetween(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_LOCAMR_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCAMR_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;  
  Preview_frm := TPreview_frm.Create(Self);
  LOCAMR_PRINT_frm := TLOCAMR_PRINT_frm.Create(Self);
  try
    LOCAMR_PRINT_frm.PrintDocument(qryList.Fields);
    Preview_frm.Report := LOCAMR_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCAMR_PRINT_frm);
  end;

end;

procedure TUI_LOCAMR_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then
    sBitBtn1Click(nil);
end;

procedure TUI_LOCAMR_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCAMR_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;  
  UI_LOCAMR_NEW_frm := nil;
end;

end.

