inherited UI_LOCAMA_NEW_frm: TUI_LOCAMA_NEW_frm
  Left = 703
  Top = 145
  Caption = '[LOCAMA]'#45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#51025#45813#49436
  PixelsPerInch = 96
  TextHeight = 15
  inherited btn_Panel: TsPanel
    inherited sSpeedButton4: TsSpeedButton
      Left = 689
      Top = 29
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 858
      Top = 29
      Visible = False
    end
    inherited sLabel7: TsLabel
      Width = 161
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221#51025#45813#49436
    end
    inherited sLabel6: TsLabel
      Width = 49
      Caption = 'LOCAMA'
    end
    inherited sSpeedButton6: TsSpeedButton
      Left = 407
      Top = 27
      Visible = False
    end
    inherited sSpeedButton7: TsSpeedButton
      Left = 331
    end
    inherited sSpeedButton8: TsSpeedButton
      Left = 181
    end
    inherited btnNew: TsButton
      Left = 209
      Top = 35
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 275
      Top = 35
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 193
    end
    inherited btnPrint: TsButton
      Left = 262
    end
    inherited btnTemp: TsButton
      Left = 416
      Top = 27
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 482
      Top = 27
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 548
      Top = 27
      Visible = False
    end
    inherited btnReady: TsButton
      Left = 698
      Top = 29
      Visible = False
    end
    inherited btnSend: TsButton
      Left = 792
      Top = 29
      Visible = False
    end
  end
  inherited sPanel4: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DOCNO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 215
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'AMD_NO'
          Title.Alignment = taCenter
          Title.Caption = #48320#44221#52264#49688
          Width = 63
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 86
          Visible = True
        end>
    end
    inherited sPanel29: TsPanel
      Left = 408
      Top = 592
      Width = 47
      Height = 31
      Align = alNone
    end
  end
  inherited sPanel3: TsPanel
    inherited sPageControl1: TsPageControl
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          inherited sSpeedButton10: TsSpeedButton
            Left = 344
          end
          inherited edt_wcd: TsEdit
            Left = -21
            Visible = False
          end
          inherited edt_wsangho: TsEdit
            Left = 74
          end
          inherited edt_wceo: TsEdit
            Left = 74
          end
          inherited edt_waddr1: TsEdit
            Left = 74
          end
          inherited edt_waddr2: TsEdit
            Left = 74
          end
          inherited edt_waddr3: TsEdit
            Left = 74
          end
          inherited edt_rcd: TsEdit
            Left = -21
            Visible = False
          end
          inherited edt_rsaupno: TsEdit
            Left = 218
          end
          inherited edt_rsangho: TsEdit
            Left = 74
          end
          inherited edt_rceo: TsEdit
            Left = 74
          end
          inherited edt_remail: TsEdit
            Left = 74
          end
          inherited edt_raddr1: TsEdit
            Left = 74
          end
          inherited edt_raddr2: TsEdit
            Left = 74
          end
          inherited edt_raddr3: TsEdit
            Left = 74
          end
          inherited edt_iden1: TsEdit
            Left = -157
            Top = 487
            Visible = False
          end
          inherited edt_iden2: TsEdit
            Left = -157
            Top = 511
            Visible = False
          end
          inherited edt_doc1: TsEdit
            Left = 479
            Top = 319
            Width = 198
          end
          inherited edt_doc2: TsEdit
            Left = 479
            Top = 343
            Width = 198
          end
          inherited edt_doc3: TsEdit
            Left = 479
            Top = 367
            Width = 198
          end
          inherited edt_doc4: TsEdit
            Left = 479
            Top = 391
            Width = 198
          end
          inherited edt_doc5: TsEdit
            Left = 479
            Top = 415
            Width = 198
          end
          inherited edt_wsaupno: TsEdit
            Left = 218
          end
          inherited edt_LOCAMT_UNIT: TsEdit
            BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50808#54868')'
          end
          inherited edt_perr: TsEdit
            Top = 197
          end
          inherited edt_merr: TsEdit
            Top = 197
          end
          inherited edt_doc6: TsEdit
            Left = 479
            Top = 439
            Width = 198
          end
          inherited edt_doc7: TsEdit
            Left = 479
            Top = 463
            Width = 198
          end
          inherited edt_doc8: TsEdit
            Left = 479
            Top = 487
            Width = 198
          end
          inherited edt_doc9: TsEdit
            Left = 479
            Top = 511
            Width = 198
          end
          inherited cur_chasu: TsCurrencyEdit
            Left = 83
          end
          inherited msk_IndoDt: TsMaskEdit
            Top = 242
          end
          inherited msk_ExpiryDT: TsMaskEdit
            Top = 266
          end
          inherited sPanel8: TsPanel
            Left = 358
            Width = 319
          end
          inherited sMaskEdit5: TsMaskEdit
            Left = 83
          end
          inherited sMaskEdit6: TsMaskEdit
            Left = 260
          end
          inherited sPanel2: TsPanel
            Width = 339
          end
          inherited sPanel9: TsPanel
            Left = 358
            Top = 220
            Width = 319
          end
          inherited sPanel10: TsPanel
            Left = 358
            Top = 297
            Width = 319
          end
          inherited sPanel11: TsPanel
            Width = 339
          end
          inherited sPanel12: TsPanel
            Width = 339
          end
          object edt_LOC2AMTC: TsEdit
            Tag = 1002
            Left = 466
            Top = 149
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 47
            OnDblClick = edt_cbankDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50896#54868')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object cur_LOC2AMT: TsCurrencyEdit
            Left = 516
            Top = 149
            Width = 161
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 48
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_EX_RATE: TsEdit
            Left = 466
            Top = 173
            Width = 121
            Height = 23
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47588#47588#44592#51456#50984
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
    end
  end
  inherited qryList: TADOQuery
    Left = 8
    Top = 640
  end
  inherited dsList: TDataSource
    DataSet = qryLOCAMA
    Left = 40
    Top = 640
  end
  inherited qryReady: TADOQuery
    Left = 72
    Top = 640
  end
  object qryLOCAMA: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '19990101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180731'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ,MAINT_NO+'#39'-'#39'+CONVERT(varchar,MSEQ) as DOCN' +
        'O,  AMD_NO, [USER_ID], DATEE, BGM_REF, MESSAGE1, MESSAGE2, RFF_N' +
        'O, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFE' +
        'RNO6, OFFERNO7, OFFERNO8, OFFERNO9, AMD_DATE, ADV_DATE, ISS_DATE' +
        ', REMARK, REMARK1, ISSBANK, ISSBANK1, ISSBANK2, APPLIC1, APPLIC2' +
        ', APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNAME1, EXNAME2, EXNAME3,' +
        ' DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYPE, LOC1AMT, LOC1AMT' +
        'C, LOC2AMT, LOC2AMTC, EX_RATE, CHK1, CHK2, CHK3, PRNO, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM,'
      '        ISNULL(N4487.DOC_NAME,'#39#39') as LOC_TYPENAME'
      
        'FROM [LOCAMA] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAMA.LOC_TYPE = N4487.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(LOCAMA.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 40
    Top = 520
    object qryLOCAMAMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryLOCAMAMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryLOCAMADOCNO: TStringField
      FieldName = 'DOCNO'
      ReadOnly = True
      Size = 66
    end
    object qryLOCAMAAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryLOCAMAUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryLOCAMADATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryLOCAMABGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryLOCAMAMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryLOCAMAMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryLOCAMARFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryLOCAMALC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryLOCAMAOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryLOCAMAOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryLOCAMAOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryLOCAMAOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryLOCAMAOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryLOCAMAOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryLOCAMAOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryLOCAMAOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryLOCAMAOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryLOCAMAAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryLOCAMAADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryLOCAMAISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryLOCAMAREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryLOCAMAREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryLOCAMAISSBANK: TStringField
      FieldName = 'ISSBANK'
      Size = 4
    end
    object qryLOCAMAISSBANK1: TStringField
      FieldName = 'ISSBANK1'
      Size = 35
    end
    object qryLOCAMAISSBANK2: TStringField
      FieldName = 'ISSBANK2'
      Size = 35
    end
    object qryLOCAMAAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryLOCAMAAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryLOCAMAAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryLOCAMABENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryLOCAMABENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryLOCAMABENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryLOCAMAEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryLOCAMAEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryLOCAMAEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryLOCAMADELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryLOCAMAEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryLOCAMACHGINFO: TStringField
      FieldName = 'CHGINFO'
      Size = 1
    end
    object qryLOCAMACHGINFO1: TMemoField
      FieldName = 'CHGINFO1'
      BlobType = ftMemo
    end
    object qryLOCAMALOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryLOCAMALOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryLOCAMALOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryLOCAMALOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryLOCAMALOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryLOCAMAEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryLOCAMACHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryLOCAMACHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryLOCAMACHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryLOCAMAPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryLOCAMAAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryLOCAMAAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryLOCAMAAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryLOCAMABNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryLOCAMABNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryLOCAMABNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryLOCAMABNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryLOCAMABNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryLOCAMACD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryLOCAMACD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryLOCAMALOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      ReadOnly = True
      Size = 100
    end
  end
end
