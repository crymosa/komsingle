inherited UI_INF707_NEW_frm: TUI_INF707_NEW_frm
  Left = 505
  Top = 143
  BorderWidth = 4
  Caption = '[INF707] '#52712#49548#48520#45733#54868#54872#49888#50857#51109' '#51312#44148#48320#44221' '#51025#45813#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 1027
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 6
      Width = 213
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#51312#44148#48320#44221#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 21
      Width = 36
      Height = 13
      Caption = 'INF707'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 376
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1034
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 241
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 307
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton13: TsButton
      Left = 388
      Top = 3
      Width = 164
      Height = 35
      Cursor = crHandPoint
      Hint = #49440#53469#54620' '#45936#51060#53552#47484' ERP'#50640' '#50732#47549#45768#45796
      Caption = #49440#53469#45936#51060#53552' UPLOAD'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = sButton13Click
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 341
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 574
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 175
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MSEQ'
          Title.Alignment = taCenter
          Title.Caption = #49692#48264
          Width = 32
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = sMaskEdit1Change
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 41
    Width = 773
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sHeader: TsPanel
      Left = 1
      Top = 1
      Width = 771
      Height = 55
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton1: TsSpeedButton
        Left = 280
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 183
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object com_func: TsComboBox
        Left = 360
        Top = 4
        Width = 121
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 360
        Top = 28
        Width = 211
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 216
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object edt_MSEQ: TsEdit
        Left = 248
        Top = 4
        Width = 25
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentFont = False
        TabOrder = 5
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 771
      Height = 575
      ActivePage = sTabSheet7
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabIndex = 5
      TabOrder = 2
      OnChange = sMaskEdit1Change
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #51312#44148#48320#44221' PAGE1'
        object sPage1: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel2: TsPanel
            Left = 98
            Top = 28
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51068#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 42
          end
          object sPanel10: TsPanel
            Left = 98
            Top = 100
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#48169#48277
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 43
          end
          object msk_IssDate: TsMaskEdit
            Left = 178
            Top = 28
            Width = 96
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 2
            Text = '20180621'
            CheckOnExit = True
          end
          object sPanel11: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 44
          end
          object sPanel12: TsPanel
            Left = 98
            Top = 4
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #51312#44148#48320#44221' '#49888#52397#51068#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 45
          end
          object edt_APPDATE: TsMaskEdit
            Left = 229
            Top = 4
            Width = 96
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
          end
          object edt_In_Mathod1: TsEdit
            Left = 217
            Top = 100
            Width = 192
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 46
            SkinData.CustomColor = True
          end
          object edt_In_Mathod: TsEdit
            Tag = 1000
            Left = 178
            Top = 100
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sPanel8: TsPanel
            Left = 98
            Top = 124
            Width = 118
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = '*'#44060#49444'('#51032#47280')'#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 47
          end
          object edt_ApBank: TsEdit
            Tag = 1001
            Left = 217
            Top = 124
            Width = 56
            Height = 23
            Hint = '4'#51088#47532'('#48376#51216'+'#51648#51216')'
            Color = 12775866
            MaxLength = 4
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank1: TsEdit
            Left = 98
            Top = 148
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank2: TsEdit
            Left = 98
            Top = 172
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank3: TsEdit
            Left = 98
            Top = 196
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank4: TsEdit
            Left = 98
            Top = 220
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank5: TsEdit
            Left = 168
            Top = 244
            Width = 241
            Height = 23
            Color = clWhite
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel9: TsPanel
            Tag = -1
            Left = 98
            Top = 244
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'TEL'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 48
          end
          object sPanel59: TsPanel
            Tag = -1
            Left = 63
            Top = 28
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 49
          end
          object sPanel89: TsPanel
            Left = 98
            Top = 52
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #51312#44148#48320#44221#54943#49688
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 50
          end
          object sPanel90: TsPanel
            Tag = -1
            Left = 63
            Top = 52
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '26E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 51
          end
          object curr_AMD_NO: TsCurrencyEdit
            Left = 229
            Top = 52
            Width = 45
            Height = 23
            AutoSize = False
            TabOrder = 3
            DecimalPlaces = 0
            DisplayFormat = '0'
            MinValue = 1
            Value = 1
          end
          object sPanel91: TsPanel
            Tag = -1
            Left = 20
            Top = 100
            Width = 77
            Height = 167
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 52
          end
          object sPanel92: TsPanel
            Left = 426
            Top = 124
            Width = 119
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = '('#55148#47581')'#53685#51648#51008#54665' '#48320#44221
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 53
          end
          object edt_AdBank1: TsEdit
            Left = 426
            Top = 148
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank2: TsEdit
            Left = 426
            Top = 172
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank3: TsEdit
            Left = 426
            Top = 196
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdBank4: TsEdit
            Left = 426
            Top = 220
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel95: TsPanel
            Tag = -1
            Left = 546
            Top = 124
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BICCODE'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 54
          end
          object edt_AdBank: TsEdit
            Tag = 1002
            Left = 616
            Top = 124
            Width = 121
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel94: TsPanel
            Tag = -1
            Left = 63
            Top = 76
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '23S'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 55
          end
          object sPanel96: TsPanel
            Left = 98
            Top = 76
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49888#50857#51109' '#52712#49548#50836#52397
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 56
          end
          object com_isCancel: TsComboBox
            Left = 229
            Top = 76
            Width = 180
            Height = 23
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 4
            Items.Strings = (
              ''
              'CR:CANCEL')
          end
          object sPanel97: TsPanel
            Tag = -1
            Left = 20
            Top = 271
            Width = 77
            Height = 143
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 57
          end
          object sPanel98: TsPanel
            Left = 98
            Top = 271
            Width = 215
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#51077#50857#46020
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 58
          end
          object edt_ImpCd1: TsEdit
            Tag = 1003
            Left = 98
            Top = 295
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ImpCd1_1: TsEdit
            Left = 137
            Top = 295
            Width = 176
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 59
            SkinData.CustomColor = True
          end
          object sPanel99: TsPanel
            Left = 314
            Top = 271
            Width = 215
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = 'I/L('#49688#51077#49849#51064#49436' '#48264#54840')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 60
          end
          object edt_ILno1: TsEdit
            Left = 314
            Top = 295
            Width = 215
            Height = 23
            Color = clWhite
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel100: TsPanel
            Left = 530
            Top = 271
            Width = 39
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #45800#50948
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 61
          end
          object edt_ILCur1: TsEdit
            Left = 530
            Top = 295
            Width = 39
            Height = 23
            Color = clWhite
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel101: TsPanel
            Left = 570
            Top = 271
            Width = 167
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#51077#49849#51064#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 62
          end
          object curr_ILAMT1: TsCurrencyEdit
            Left = 570
            Top = 295
            Width = 167
            Height = 23
            AutoSize = False
            TabOrder = 32
          end
          object edt_ImpCd2: TsEdit
            Tag = 1004
            Left = 98
            Top = 319
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ImpCd1_2: TsEdit
            Left = 137
            Top = 319
            Width = 176
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 63
            SkinData.CustomColor = True
          end
          object edt_ILno2: TsEdit
            Left = 314
            Top = 319
            Width = 215
            Height = 23
            Color = clWhite
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur2: TsEdit
            Left = 530
            Top = 319
            Width = 39
            Height = 23
            Color = clWhite
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_ILAMT2: TsCurrencyEdit
            Left = 570
            Top = 319
            Width = 167
            Height = 23
            AutoSize = False
            TabOrder = 33
          end
          object edt_ImpCd3: TsEdit
            Tag = 1005
            Left = 98
            Top = 343
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ImpCd1_3: TsEdit
            Left = 137
            Top = 343
            Width = 176
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 64
            SkinData.CustomColor = True
          end
          object edt_ILno3: TsEdit
            Left = 314
            Top = 343
            Width = 215
            Height = 23
            Color = clWhite
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur3: TsEdit
            Left = 530
            Top = 343
            Width = 39
            Height = 23
            Color = clWhite
            TabOrder = 29
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_ILAMT3: TsCurrencyEdit
            Left = 570
            Top = 343
            Width = 167
            Height = 23
            AutoSize = False
            TabOrder = 34
          end
          object edt_ImpCd4: TsEdit
            Tag = 1006
            Left = 98
            Top = 367
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ImpCd1_4: TsEdit
            Left = 137
            Top = 367
            Width = 176
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 65
            SkinData.CustomColor = True
          end
          object edt_ILno4: TsEdit
            Left = 314
            Top = 367
            Width = 215
            Height = 23
            Color = clWhite
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur4: TsEdit
            Left = 530
            Top = 367
            Width = 39
            Height = 23
            Color = clWhite
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_ILAMT4: TsCurrencyEdit
            Left = 570
            Top = 367
            Width = 167
            Height = 23
            AutoSize = False
            TabOrder = 35
          end
          object edt_ImpCd5: TsEdit
            Tag = 1007
            Left = 98
            Top = 391
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_ImpCd1_5: TsEdit
            Left = 137
            Top = 391
            Width = 176
            Height = 23
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 66
            SkinData.CustomColor = True
          end
          object edt_ILno5: TsEdit
            Left = 314
            Top = 391
            Width = 215
            Height = 23
            Color = clWhite
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ILCur5: TsEdit
            Left = 530
            Top = 391
            Width = 39
            Height = 23
            Color = clWhite
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_ILAMT5: TsCurrencyEdit
            Left = 570
            Top = 391
            Width = 167
            Height = 23
            AutoSize = False
            TabOrder = 36
          end
          object sPanel102: TsPanel
            Tag = -1
            Left = 20
            Top = 418
            Width = 77
            Height = 119
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 67
          end
          object sPanel103: TsPanel
            Left = 98
            Top = 418
            Width = 95
            Height = 119
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44592#53440#51221#48372
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 68
          end
          object edt_AdInfo1: TsEdit
            Left = 194
            Top = 418
            Width = 543
            Height = 23
            Color = clWhite
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo2: TsEdit
            Left = 194
            Top = 442
            Width = 543
            Height = 23
            Color = clWhite
            TabOrder = 38
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo3: TsEdit
            Left = 194
            Top = 466
            Width = 543
            Height = 23
            Color = clWhite
            TabOrder = 39
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo4: TsEdit
            Left = 194
            Top = 490
            Width = 543
            Height = 23
            Color = clWhite
            TabOrder = 40
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AdInfo5: TsEdit
            Left = 194
            Top = 514
            Width = 543
            Height = 23
            Color = clWhite
            TabOrder = 41
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel1: TsPanel
            Tag = -1
            Left = 426
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '20'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 69
          end
          object sPanel13: TsPanel
            Left = 461
            Top = 4
            Width = 144
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #48156#49888#51008#54665' '#52280#51312#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 70
          end
          object edt_cdNo: TsEdit
            Left = 606
            Top = 4
            Width = 151
            Height = 23
            Color = clWhite
            TabOrder = 1
            Text = 'ABCDEABCDEABCDE1'
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel6: TsPanel
            Tag = -1
            Left = 426
            Top = 28
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '21'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 71
          end
          object sPanel7: TsPanel
            Left = 461
            Top = 28
            Width = 144
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#49888#51008#54665' '#52280#51312#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 72
          end
          object edt_RecvRef: TsEdit
            Left = 606
            Top = 28
            Width = 151
            Height = 23
            Color = clWhite
            TabOrder = 73
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel27: TsPanel
            Tag = -1
            Left = 426
            Top = 52
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '23'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 74
          end
          object sPanel34: TsPanel
            Left = 461
            Top = 52
            Width = 144
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51008#54665' '#52280#51312#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 75
          end
          object edt_IssRef: TsEdit
            Left = 606
            Top = 52
            Width = 151
            Height = 23
            Color = clWhite
            TabOrder = 76
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = 'PAGE2'
        object sPage2: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sLabel3: TsLabel
            Left = 570
            Top = 256
            Width = 10
            Height = 15
            Caption = '%'
          end
          object sPanel14: TsPanel
            Left = 98
            Top = 4
            Width = 126
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51032#47280#51064' '#48320#44221
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object edt_Applic1: TsEdit
            Left = 98
            Top = 28
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic2: TsEdit
            Left = 98
            Top = 52
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic3: TsEdit
            Left = 98
            Top = 76
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic4: TsEdit
            Left = 98
            Top = 100
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel16: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object sPanel15: TsPanel
            Left = 426
            Top = 4
            Width = 118
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#51061#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object edt_Benefc1: TsEdit
            Left = 426
            Top = 28
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc2: TsEdit
            Left = 426
            Top = 52
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc3: TsEdit
            Left = 426
            Top = 76
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Benefc4: TsEdit
            Left = 426
            Top = 100
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel17: TsPanel
            Tag = -1
            Left = 426
            Top = 124
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 11
          end
          object edt_Benefc5: TsEdit
            Left = 496
            Top = 124
            Width = 241
            Height = 23
            Color = clWhite
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel18: TsPanel
            Tag = -1
            Left = 63
            Top = 204
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '32B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object sPanel19: TsPanel
            Left = 98
            Top = 204
            Width = 327
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49888#50857#51109' '#51613#50529#48516'(Increase of Documentary Credit Amount)'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object sPanel21: TsPanel
            Tag = -1
            Left = 63
            Top = 228
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '33B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel22: TsPanel
            Left = 98
            Top = 228
            Width = 327
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49888#50857#51109' '#44048#50529#48516'(Decrease of Documentary Credit Amount)'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sPanel23: TsPanel
            Tag = -1
            Left = 63
            Top = 252
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel25: TsPanel
            Left = 98
            Top = 252
            Width = 327
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44284#48512#51313#54728#50857#50984'(Percentage Credit Amount Tolerance)'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_CdPerp: TsCurrencyEdit
            Left = 461
            Top = 252
            Width = 42
            Height = 23
            AutoSize = False
            TabOrder = 20
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '('#65291')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object edt_CdPerm: TsCurrencyEdit
            Left = 528
            Top = 252
            Width = 39
            Height = 23
            AutoSize = False
            TabOrder = 21
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '/ ('#65293')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object sPanel26: TsPanel
            Tag = -1
            Left = 63
            Top = 399
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '71N'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel28: TsPanel
            Left = 98
            Top = 399
            Width = 291
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49888#50857#51109' '#48320#44221#49688#49688#47308' '#48512#45812#51088'(Amendment Charge)'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object com_amd_charge: TsComboBox
            Left = 390
            Top = 399
            Width = 299
            Height = 23
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 23
            Items.Strings = (
              ''
              '2AC: APPL('#44060#49444#51088#48512#45812')'
              '2AD: BENE('#49688#51061#51088#48512#45812')'
              '2AE: OTHR ('#51228' 3'#51088#48512#45812' '#49345#49464#45236#50669' '#51649#51217#51077#47141')')
          end
          object sPanel33: TsPanel
            Tag = -1
            Left = 692
            Top = 399
            Width = 57
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '6*35z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object sPanel30: TsPanel
            Tag = -1
            Left = 63
            Top = 284
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '71D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object sPanel31: TsPanel
            Left = 98
            Top = 284
            Width = 291
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#49688#47308' '#48512#45812#51088' '#48320#44221'(Charge)'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object com_charge: TsComboBox
            Left = 390
            Top = 284
            Width = 299
            Height = 23
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 22
            Items.Strings = (
              ''
              '2AF: '#44397#50808#49688#49688#47308' '#44060#49444#51088#48512#45812
              '2AG: '#44397#50808#49688#49688#47308' '#49688#51061#51088#48512#45812
              '2AH: '#49345#49464' '#45236#50857' '#51649#51217#51077#47141)
          end
          object sPanel32: TsPanel
            Tag = -1
            Left = 692
            Top = 284
            Width = 57
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '6*35z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object edt_IncdCur: TsEdit
            Tag = 1010
            Left = 426
            Top = 204
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_IncdAmt: TsCurrencyEdit
            Left = 461
            Top = 204
            Width = 135
            Height = 23
            AutoSize = False
            TabOrder = 17
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object edt_DecdCur: TsEdit
            Tag = 1011
            Left = 426
            Top = 228
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_DecdAmt: TsCurrencyEdit
            Left = 461
            Top = 228
            Width = 135
            Height = 23
            AutoSize = False
            TabOrder = 19
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object sPanel77: TsPanel
            Tag = -1
            Left = 63
            Top = 180
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object sPanel78: TsPanel
            Left = 98
            Top = 180
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Date and Place of Expiry'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 37
          end
          object msk_exDate: TsMaskEdit
            Left = 332
            Top = 180
            Width = 86
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 14
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'DATE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object edt_exPlace: TsEdit
            Left = 463
            Top = 180
            Width = 289
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel105: TsPanel
            Tag = -1
            Left = 63
            Top = 156
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '40A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
          object sPanel106: TsPanel
            Left = 98
            Top = 156
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Form of Documentary Credit'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 39
          end
          object edt_Doccd: TsEdit
            Tag = 1008
            Left = 298
            Top = 156
            Width = 26
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_Doccd1: TsEdit
            Left = 325
            Top = 156
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 40
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApplicOrg1: TsEdit
            Left = 2
            Top = 436
            Width = 40
            Height = 23
            Color = clWhite
            TabOrder = 41
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApplicOrg2: TsEdit
            Left = 2
            Top = 460
            Width = 40
            Height = 23
            Color = clWhite
            TabOrder = 42
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApplicOrg3: TsEdit
            Left = 2
            Top = 484
            Width = 40
            Height = 23
            Color = clWhite
            TabOrder = 43
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApplicOrg4: TsEdit
            Left = 2
            Top = 508
            Width = 40
            Height = 23
            Color = clWhite
            TabOrder = 44
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_amd_charge_1: TsMemo
            Left = 390
            Top = 423
            Width = 359
            Height = 86
            ScrollBars = ssVertical
            TabOrder = 45
          end
          object memo_charge_1: TsMemo
            Left = 390
            Top = 308
            Width = 359
            Height = 86
            ScrollBars = ssVertical
            TabOrder = 46
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = 'PAGE 3'
        object sPage3: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object com_Pship: TsComboBox
            Left = 186
            Top = 4
            Width = 351
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 0
            Items.Strings = (
              ''
              '9: ALLOWED (Partial shipments are allowed)'
              '10: NOT ALLOWED (Partial shipments are not allowed)')
          end
          object sPanel43: TsPanel
            Tag = -1
            Left = 20
            Top = 156
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object sPanel44: TsPanel
            Left = 55
            Top = 156
            Width = 291
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Latest Date of Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 15
          end
          object sPanel45: TsPanel
            Left = 55
            Top = 106
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 
              'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
              'livery'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 16
          end
          object msk_lstDate: TsMaskEdit
            Left = 347
            Top = 156
            Width = 81
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 4
            CheckOnExit = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object sPanel48: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object sPanel49: TsPanel
            Left = 55
            Top = 4
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Partial Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object sPanel50: TsPanel
            Tag = -1
            Left = 20
            Top = 28
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43T'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object sPanel51: TsPanel
            Left = 55
            Top = 28
            Width = 130
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Transhipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object sPanel52: TsPanel
            Tag = -1
            Left = 20
            Top = 182
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object sPanel53: TsPanel
            Left = 55
            Top = 182
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Loading/Airport of Departure ['#49440#51201#54637']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object edt_SunjukPort: TsEdit
            Left = 20
            Top = 206
            Width = 717
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel54: TsPanel
            Tag = -1
            Left = 20
            Top = 232
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44F'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
          end
          object sPanel55: TsPanel
            Left = 55
            Top = 232
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Discharge/Airport of Destination ['#46020#52265#54637']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object edt_dochackPort: TsEdit
            Left = 20
            Top = 256
            Width = 717
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel56: TsPanel
            Tag = -1
            Left = 20
            Top = 56
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object sPanel57: TsPanel
            Left = 55
            Top = 56
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel58: TsPanel
            Tag = -1
            Left = 20
            Top = 106
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object edt_loadOn: TsEdit
            Left = 20
            Top = 80
            Width = 717
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_forTran: TsEdit
            Left = 20
            Top = 130
            Width = 717
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object com_TShip: TsComboBox
            Left = 186
            Top = 28
            Width = 351
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = -1
            TabOrder = 1
            Items.Strings = (
              ''
              '7: ALLOWED (Transhipments are allowed)'
              '8: NOT ALLOWED (Transhipments are not allowed)')
          end
          object sPanel46: TsPanel
            Tag = -1
            Left = 20
            Top = 280
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel47: TsPanel
            Left = 55
            Top = 280
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Shipment Period'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_shipPD1: TsEdit
            Left = 20
            Top = 304
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD2: TsEdit
            Left = 20
            Top = 326
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD3: TsEdit
            Left = 20
            Top = 348
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD4: TsEdit
            Left = 20
            Top = 370
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD5: TsEdit
            Left = 20
            Top = 392
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_shipPD6: TsEdit
            Left = 20
            Top = 414
            Width = 408
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_SPECIAL_PAY: TsMemo
            Left = 20
            Top = 464
            Width = 701
            Height = 72
            ScrollBars = ssVertical
            TabOrder = 13
            WordWrap = False
          end
          object sPanel64: TsPanel
            Tag = -1
            Left = 20
            Top = 440
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49M'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel65: TsPanel
            Left = 55
            Top = 440
            Width = 314
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Special Payment Conditions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object sPanel66: TsPanel
            Left = 624
            Top = 440
            Width = 97
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '100'#54665'*65'#51088'z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
        end
      end
      object sTabSheet5: TsTabSheet
        Caption = 'PAGE 4'
        object sPage4: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel71: TsPanel
            Tag = -1
            Left = 20
            Top = 120
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object sPanel72: TsPanel
            Left = 55
            Top = 120
            Width = 313
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Drafts at...('#54868#54872#50612#51020#51312#44148')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object edt_DRAFT1: TsEdit
            Left = 20
            Top = 144
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DRAFT2: TsEdit
            Left = 20
            Top = 166
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DRAFT3: TsEdit
            Left = 20
            Top = 188
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel73: TsPanel
            Left = 407
            Top = 4
            Width = 313
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Mixed Payment Detail ('#54844#54633#51648#44553#51312#44148')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object edt_MIX1: TsEdit
            Left = 372
            Top = 28
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_MIX2: TsEdit
            Left = 372
            Top = 50
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_MIX3: TsEdit
            Left = 372
            Top = 72
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_MIX4: TsEdit
            Left = 372
            Top = 94
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel74: TsPanel
            Left = 407
            Top = 120
            Width = 313
            Height = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Negotiation/Deferred Payment Details'#13#10'('#47588#51077'/'#50672#51648#44552#51312#44148#47749#49464')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object edt_DEFPAY1: TsEdit
            Left = 372
            Top = 157
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DEFPAY2: TsEdit
            Left = 372
            Top = 179
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DEFPAY3: TsEdit
            Left = 372
            Top = 201
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_DEFPAY4: TsEdit
            Left = 372
            Top = 223
            Width = 348
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel75: TsPanel
            Tag = -1
            Left = 372
            Top = 120
            Width = 34
            Height = 36
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object sPanel76: TsPanel
            Tag = -1
            Left = 372
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42M'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object sPanel36: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object sPanel37: TsPanel
            Left = 55
            Top = 4
            Width = 313
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional Amount Covered'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
          end
          object edt_aaCcv1: TsEdit
            Left = 20
            Top = 28
            Width = 348
            Height = 23
            Color = clWhite
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv2: TsEdit
            Left = 20
            Top = 50
            Width = 348
            Height = 23
            Color = clWhite
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv3: TsEdit
            Left = 20
            Top = 72
            Width = 348
            Height = 23
            Color = clWhite
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_aaCcv4: TsEdit
            Left = 20
            Top = 94
            Width = 348
            Height = 23
            Color = clWhite
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_GoodsDesc: TsMemo
            Left = 20
            Top = 404
            Width = 701
            Height = 125
            ScrollBars = ssVertical
            TabOrder = 16
            WordWrap = False
          end
          object sPanel38: TsPanel
            Tag = -1
            Left = 20
            Top = 380
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '45B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object sPanel39: TsPanel
            Left = 55
            Top = 380
            Width = 314
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Description of Goods and/or Services'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object sPanel41: TsPanel
            Tag = -1
            Left = 624
            Top = 380
            Width = 97
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '100'#54665'*65'#51088'z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel79: TsPanel
            Left = 55
            Top = 248
            Width = 314
            Height = 36
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 
              'Instructions to the Paying/Acception/Negotiating Bank'#13#10#51648#44553'/'#51064#49688'/'#47588#51077#51008 +
              #54665#50640' '#45824#54620' '#51648#49884#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sPanel93: TsPanel
            Tag = -1
            Left = 20
            Top = 248
            Width = 34
            Height = 36
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '78'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object memo_TXT78: TsMemo
            Left = 20
            Top = 285
            Width = 701
            Height = 92
            ScrollBars = ssVertical
            TabOrder = 29
            WordWrap = False
          end
          object sPanel107: TsPanel
            Tag = -1
            Left = 624
            Top = 260
            Width = 97
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '12'#54665'*65'#51088'x'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
        end
      end
      object sTabSheet6: TsTabSheet
        Caption = 'PAGE 5'
        object sPage5: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object memo_DOCREQ: TsMemo
            Left = 20
            Top = 28
            Width = 701
            Height = 501
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
          end
          object sPanel67: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '46B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object sPanel68: TsPanel
            Left = 55
            Top = 4
            Width = 314
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Documents Required'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object sPanel42: TsPanel
            Tag = -1
            Left = 624
            Top = 4
            Width = 97
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '100'#54665'*65'#51088'z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
        end
      end
      object sTabSheet7: TsTabSheet
        Caption = 'PAGE 6'
        object sPage6: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel82: TsPanel
            Left = 98
            Top = 390
            Width = 153
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51032#47280#51064' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 15
          end
          object edt_EXName1: TsEdit
            Left = 98
            Top = 414
            Width = 251
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_EXName2: TsEdit
            Left = 98
            Top = 438
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXName3: TsEdit
            Left = 98
            Top = 462
            Width = 92
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr1: TsEdit
            Left = 98
            Top = 486
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr2: TsEdit
            Left = 98
            Top = 510
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel83: TsPanel
            Tag = -1
            Left = 20
            Top = 390
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 16
          end
          object sPanel84: TsPanel
            Tag = -1
            Left = 20
            Top = 414
            Width = 77
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #47749#51032#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object sPanel85: TsPanel
            Tag = -1
            Left = 20
            Top = 462
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49885#48324#48512#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object sPanel86: TsPanel
            Tag = -1
            Left = 20
            Top = 486
            Width = 77
            Height = 46
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51452#49548
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object sPanel87: TsPanel
            Tag = -1
            Left = 20
            Top = 206
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '48'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object sPanel104: TsPanel
            Left = 55
            Top = 206
            Width = 240
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Period for Presentation in Days'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object edt_period: TsEdit
            Tag = 500
            Left = 54
            Top = 230
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object com_PeriodIndex: TsComboBox
            Left = 150
            Top = 230
            Width = 145
            Height = 23
            BoundLabel.Active = True
            BoundLabel.Caption = '(days after'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 2
            Items.Strings = (
              ''
              'the Date of Shipment'
              'any Other Date:')
          end
          object edt_PeriodDetail: TsEdit
            Tag = 601
            Left = 402
            Top = 230
            Width = 316
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            MaxLength = 35
            ParentCtl3D = False
            TabOrder = 3
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49436#47448#51228#49884#44592#44036' '#49345#49464
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sPanel80: TsPanel
            Tag = -1
            Left = 19
            Top = 262
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object sPanel81: TsPanel
            Left = 54
            Top = 262
            Width = 162
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Confirmation Instructions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
          end
          object com_Confirm: TsComboBox
            Left = 217
            Top = 262
            Width = 184
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 2
            TabOrder = 4
            Text = 'DB: MAY ADD'
            Items.Strings = (
              ''
              'DA: WITHOUT'
              'DB: MAY ADD'
              'DC: CONFIRM')
          end
          object sPanel69: TsPanel
            Left = 402
            Top = 262
            Width = 119
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #54869#51064#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object sPanel70: TsPanel
            Tag = -1
            Left = 522
            Top = 262
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BICCODE'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_CONFIRM_BIC: TsEdit
            Left = 592
            Top = 262
            Width = 126
            Height = 23
            Color = clWhite
            MaxLength = 11
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM1: TsEdit
            Left = 402
            Top = 286
            Width = 316
            Height = 23
            Color = clWhite
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM2: TsEdit
            Left = 402
            Top = 310
            Width = 316
            Height = 23
            Color = clWhite
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM4: TsEdit
            Left = 402
            Top = 358
            Width = 316
            Height = 23
            Color = clWhite
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CONFIRM3: TsEdit
            Left = 402
            Top = 334
            Width = 316
            Height = 23
            Color = clWhite
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_ADD_CONDITION: TsMemo
            Left = 20
            Top = 28
            Width = 701
            Height = 176
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
          end
          object sPanel60: TsPanel
            Tag = -1
            Left = 20
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '47B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel61: TsPanel
            Left = 55
            Top = 4
            Width = 314
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional Conditions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sPanel63: TsPanel
            Tag = -1
            Left = 624
            Top = 4
            Width = 97
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '100'#54665'*65'#51088'z'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel108: TsPanel
            Left = 482
            Top = 390
            Width = 153
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51008#54665' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_OP_BANK1: TsEdit
            Left = 482
            Top = 414
            Width = 251
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_OP_BANK2: TsEdit
            Left = 482
            Top = 438
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OP_BANK3: TsEdit
            Left = 482
            Top = 462
            Width = 92
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OP_ADDR1: TsEdit
            Left = 482
            Top = 486
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OP_ADDR2: TsEdit
            Left = 482
            Top = 510
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel109: TsPanel
            Tag = -1
            Left = 404
            Top = 390
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object sPanel110: TsPanel
            Tag = -1
            Left = 404
            Top = 414
            Width = 77
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #47749#51032#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object sPanel111: TsPanel
            Tag = -1
            Left = 404
            Top = 462
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49885#48324#48512#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 37
          end
          object sPanel112: TsPanel
            Tag = -1
            Left = 404
            Top = 486
            Width = 77
            Height = 46
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51452#49548
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 763
          Height = 507
          TabStop = False
          Align = alClient
          Color = clGray
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #52376#47532
              Width = 60
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MSeq'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 32
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'AMD_NO'
              Title.Alignment = taCenter
              Title.Caption = #48320#44221#52264#49688
              Width = 58
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'APPLIC1'
              Title.Caption = #44060#49444'('#51032#47280')'#51008#54665
              Width = 246
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 40
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20100101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180816'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT I707_1.MAINT_NO,I707_1.MSEQ,I707_1.AMD_NO,I707_1.MESSAGE1' +
        ',I707_1.MESSAGE2,I707_1.[USER_ID],I707_1.DATEE,I707_1.APP_DATE,I' +
        '707_1.IN_MATHOD,'
      
        #9'   I707_1.AP_BANK,I707_1.AP_BANK1,I707_1.AP_BANK2,I707_1.AP_BAN' +
        'K3,I707_1.AP_BANK4,I707_1.AP_BANK5,'
      
        #9'   I707_1.AD_BANK,I707_1.AD_BANK1,I707_1.AD_BANK2,I707_1.AD_BAN' +
        'K3,I707_1.AD_BANK4,'
      
        #9'   I707_1.IL_NO1,I707_1.IL_NO2,I707_1.IL_NO3,I707_1.IL_NO4,I707' +
        '_1.IL_NO5,'
      
        #9'   I707_1.IL_AMT1,I707_1.IL_AMT2,I707_1.IL_AMT3,I707_1.IL_AMT4,' +
        'I707_1.IL_AMT5,'
      
        #9'   I707_1.IL_CUR1,I707_1.IL_CUR2,I707_1.IL_CUR3,I707_1.IL_CUR4,' +
        'I707_1.IL_CUR5,'
      
        #9'   I707_1.AD_INFO1,I707_1.AD_INFO2,I707_1.AD_INFO3,I707_1.AD_IN' +
        'FO4,I707_1.AD_INFO5,'
      
        #9'   I707_1.CD_NO,I707_1.RCV_REF,I707_1.IBANK_REF,I707_1.ISS_BANK' +
        '1,I707_1.ISS_BANK2,I707_1.ISS_BANK3,I707_1.ISS_BANK4,I707_1.ISS_' +
        'BANK5,I707_1.ISS_ACCNT,'
      
        '                   I707_1.ISS_DATE,I707_1.AMD_DATE,I707_1.EX_DAT' +
        'E,I707_1.EX_PLACE,I707_1.CHK1,I707_1.CHK2,I707_1.CHK3,'
      
        #9'   I707_1.F_INTERFACE,I707_1.IMP_CD1,I707_1.IMP_CD2,I707_1.IMP_' +
        'CD3,I707_1.IMP_CD4,I707_1.IMP_CD5,I707_1.Prno,'
      ''
      
        #9'   I707_2.MAINT_NO,I707_2.MSEQ,I707_2.AMD_NO,I707_2.APPLIC1,I70' +
        '7_2.APPLIC2,I707_2.APPLIC3,I707_2.APPLIC4,I707_2.APPLIC5,'
      
        #9'   I707_2.BENEFC1,I707_2.BENEFC2,I707_2.BENEFC3,I707_2.BENEFC4,' +
        'I707_2.BENEFC5,I707_2.INCD_CUR,I707_2.INCD_AMT,I707_2.DECD_CUR,I' +
        '707_2.DECD_AMT,'
      
        #9'   I707_2.NWCD_CUR,I707_2.NWCD_AMT,I707_2.CD_PERP,I707_2.CD_PER' +
        'M,I707_2.CD_MAX,I707_2.AA_CV1,I707_2.AA_CV2,I707_2.AA_CV3,I707_2' +
        '.AA_CV4,'
      
        #9'   I707_2.LOAD_ON,I707_2.FOR_TRAN,I707_2.LST_DATE,I707_2.SHIP_P' +
        'D,'
      
        #9'   I707_2.SHIP_PD1,I707_2.SHIP_PD2,I707_2.SHIP_PD3,I707_2.SHIP_' +
        'PD4,I707_2.SHIP_PD5,I707_2.SHIP_PD6,'
      
        '                   I707_2.NARRAT,I707_2.NARRAT_1,I707_2.SR_INFO1' +
        ',I707_2.SR_INFO2,I707_2.SR_INFO3,I707_2.SR_INFO4,I707_2.SR_INFO5' +
        ',I707_2.SR_INFO6,'
      
        #9'   I707_2.EX_NAME1,I707_2.EX_NAME2,I707_2.EX_NAME3,I707_2.EX_AD' +
        'DR1,I707_2.EX_ADDR2,'
      
        #9'   I707_2.OP_BANK1,I707_2.OP_BANK2,I707_2.OP_BANK3,I707_2.OP_AD' +
        'DR1,I707_2.OP_ADDR2,I707_2.BFCD_AMT,I707_2.BFCD_CUR,I707_2.SUNJU' +
        'CK_PORT,I707_2.DOCHACK_PORT,'
      #9'   '
      'IS_CANCEL, DOC_CD, PSHIP, TSHIP,'
      
        'CHARGE, CHARGE_1, AMD_CHARGE, AMD_CHARGE_1, SPECIAL_PAY, GOODS_D' +
        'ESC, DOC_REQ, ADD_CONDITION, DRAFT1, DRAFT2, DRAFT3, MIX1, MIX2,' +
        ' MIX3, MIX4, DEFPAY1, DEFPAY2, DEFPAY3, DEFPAY4, PERIOD_DAYS, PE' +
        'RIOD_IDX, PERIOD_DETAIL, CONFIRM, CONFIRM_BIC, CONFIRM1, CONFIRM' +
        '2, CONFIRM3, CONFIRM4, TXT_78,'
      'APPLIC_CHG1, APPLIC_CHG2, APPLIC_CHG3, APPLIC_CHG4'
      ''
      #9'  ,Mathod707.DOC_NAME as mathod_Name'
      #9'  ,IMPCD707_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD707_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD707_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD707_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD707_5.DOC_NAME as Imp_Name_5'
      #9'  ,CDMAX707.DOC_NAME as CDMAX_Name'
      '                  ,DOC_CD.DOC_NAME as DOC_CDNM'
      '    '
      'FROM [dbo].[INF707_1] AS I707_1'
      
        'INNER JOIN [dbo].[INF707_2] AS I707_2 ON I707_1.MAINT_NO = I707_' +
        '2.MAINT_NO AND I707_1.MSEQ = I707_2.MSEQ'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod707 ON I707_1.IN_MATHOD = Mathod' +
        '707.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_1 ON I707_1.IMP_CD1 = IMPCD' +
        '707_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_2 ON I707_1.IMP_CD2 = IMPCD' +
        '707_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_3 ON I707_1.IMP_CD3 = IMPCD' +
        '707_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_4 ON I707_1.IMP_CD4 = IMPCD' +
        '707_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_5 ON I707_1.IMP_CD5 = IMPCD' +
        '707_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX707 ON I707_2.CD_MAX = CDMAX707' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39' AND CODE != '#39'9'#39' ) DOC_CD ON I707_1.DO' +
        'C_CD = DOC_CD.CODE'
      ''
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(I707_1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 56
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListRCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryListIBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryListISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryListISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryListISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryListISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryListISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryListISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListPrno: TIntegerField
      FieldName = 'Prno'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListMSEQ_1: TIntegerField
      FieldName = 'MSEQ_1'
    end
    object qryListAMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListSR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryListSR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryListSR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryListSR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryListSR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryListSR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListOP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryListOP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryListOP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryListOP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryListOP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListIS_CANCEL: TStringField
      FieldName = 'IS_CANCEL'
      Size = 65
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 2
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 2
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 2
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 4
    end
    object qryListCHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryListAMD_CHARGE: TStringField
      FieldName = 'AMD_CHARGE'
      Size = 4
    end
    object qryListAMD_CHARGE_1: TMemoField
      FieldName = 'AMD_CHARGE_1'
      BlobType = ftMemo
    end
    object qryListSPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
    object qryListGOODS_DESC: TMemoField
      FieldName = 'GOODS_DESC'
      BlobType = ftMemo
    end
    object qryListDOC_REQ: TMemoField
      FieldName = 'DOC_REQ'
      BlobType = ftMemo
    end
    object qryListADD_CONDITION: TMemoField
      FieldName = 'ADD_CONDITION'
      BlobType = ftMemo
    end
    object qryListMIX1: TStringField
      FieldName = 'MIX1'
      Size = 35
    end
    object qryListMIX2: TStringField
      FieldName = 'MIX2'
      Size = 35
    end
    object qryListMIX3: TStringField
      FieldName = 'MIX3'
      Size = 35
    end
    object qryListMIX4: TStringField
      FieldName = 'MIX4'
      Size = 35
    end
    object qryListDEFPAY1: TStringField
      FieldName = 'DEFPAY1'
      Size = 35
    end
    object qryListDEFPAY2: TStringField
      FieldName = 'DEFPAY2'
      Size = 35
    end
    object qryListDEFPAY3: TStringField
      FieldName = 'DEFPAY3'
      Size = 35
    end
    object qryListDEFPAY4: TStringField
      FieldName = 'DEFPAY4'
      Size = 35
    end
    object qryListPERIOD_DAYS: TIntegerField
      FieldName = 'PERIOD_DAYS'
    end
    object qryListPERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryListPERIOD_DETAIL: TStringField
      FieldName = 'PERIOD_DETAIL'
      Size = 35
    end
    object qryListCONFIRM: TStringField
      FieldName = 'CONFIRM'
      Size = 2
    end
    object qryListCONFIRM_BIC: TStringField
      FieldName = 'CONFIRM_BIC'
      Size = 11
    end
    object qryListCONFIRM1: TStringField
      FieldName = 'CONFIRM1'
      Size = 35
    end
    object qryListCONFIRM2: TStringField
      FieldName = 'CONFIRM2'
      Size = 35
    end
    object qryListCONFIRM3: TStringField
      FieldName = 'CONFIRM3'
      Size = 35
    end
    object qryListCONFIRM4: TStringField
      FieldName = 'CONFIRM4'
      Size = 35
    end
    object qryListTXT_78: TMemoField
      FieldName = 'TXT_78'
      BlobType = ftMemo
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListAPPLIC_CHG1: TStringField
      FieldName = 'APPLIC_CHG1'
      Size = 35
    end
    object qryListAPPLIC_CHG2: TStringField
      FieldName = 'APPLIC_CHG2'
      Size = 35
    end
    object qryListAPPLIC_CHG3: TStringField
      FieldName = 'APPLIC_CHG3'
      Size = 35
    end
    object qryListAPPLIC_CHG4: TStringField
      FieldName = 'APPLIC_CHG4'
      Size = 35
    end
    object qryListDOC_CDNM: TStringField
      FieldName = 'DOC_CDNM'
      Size = 100
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 88
    Top = 192
  end
end
