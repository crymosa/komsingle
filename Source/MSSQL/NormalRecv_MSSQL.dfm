inherited NormalRecv_MSSQL_frm: TNormalRecv_MSSQL_frm
  Left = 510
  Top = 65
  ClientWidth = 888
  Position = poMainFormCenter
  OnClose = FormClose
  DesignSize = (
    888
    629)
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    Width = 888
    inherited sSpeedButton5: TsSpeedButton
      Left = 821
    end
    inherited Btn_Close: TsSpeedButton
      Left = 832
      OnClick = Btn_CloseClick
    end
    inherited Btn_New: TsSpeedButton
      Visible = False
    end
    inherited Btn_Modify: TsSpeedButton
      Visible = False
    end
    inherited Btn_Del: TsSpeedButton
      OnClick = Btn_DelClick
    end
    inherited Btn_Print: TsSpeedButton
      Left = 447
    end
    inherited Btn_Ready: TsSpeedButton
      Left = 394
      Visible = False
    end
    inherited Btn_Save: TsSpeedButton
      Left = 502
      Visible = False
    end
    inherited Btn_Cancel: TsSpeedButton
      Left = 557
      Visible = False
    end
    inherited Btn_Temp: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton26: TsSpeedButton
      Left = 610
      Visible = False
    end
    inherited sSpeedButton27: TsSpeedButton
      Left = 555
      Visible = False
    end
  end
  inherited sPageControl1: TsPageControl
    Width = 888
    inherited sTabSheet1: TsTabSheet
      inherited sPanel7: TsPanel
        Width = 880
        inherited sEdit3: TsEdit
          Left = 306
          Width = 159
        end
        inherited sButton1: TsButton
          Left = 466
          OnClick = sButton1Click
        end
        object sComboBox1: TsComboBox
          Left = 680
          Top = 6
          Width = 105
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#54596#53552
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 4276545
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ImeName = 'Microsoft IME 2010'
          ItemHeight = 14
          ItemIndex = -1
          TabOrder = 4
          OnSelect = sComboBox1Select
        end
      end
      inherited sDBGrid1: TsDBGrid
        Width = 880
        DataSource = dsList
        OnDblClick = sDBGrid1DblClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Datee'
            Title.Alignment = taCenter
            Title.Caption = #48156#49888#51068
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Maint_No'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Caption = #44288#47532#48264#54840
            Width = 231
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'User_Id'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EX_Id'
            Title.Caption = #48156#49888#51088#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'XX_Id'
            Title.Caption = #44592#53440#52280#51312#48264#54840
            Width = 144
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Doc1'
            Title.Alignment = taCenter
            Title.Caption = #44288#47144#47928#49436
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BGM_NAME'
            Title.Alignment = taCenter
            Title.Caption = #47928#49436#49345#49464
            Width = 122
            Visible = True
          end>
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter1: TsSplitter
        Width = 880
      end
      inherited sSplitter4: TsSplitter
        Width = 880
      end
      inherited sPanel2: TsPanel
        Width = 880
        inherited sDBEdit1: TsDBEdit
          TabStop = False
          DataField = 'Maint_No'
          DataSource = dsList
          Enabled = False
        end
        inherited sDBEdit23: TsDBEdit
          TabStop = False
          DataField = 'User_Id'
          DataSource = dsList
          Enabled = False
        end
        inherited sEdit1: TsEdit
          Left = 832
          Enabled = False
          Visible = False
        end
      end
      inherited sPanel3: TsPanel
        Width = 880
        inherited sDBEdit33: TsDBEdit
          TabStop = False
          DataField = 'EX_Name1'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit34: TsDBEdit
          TabStop = False
          DataField = 'EX_Name2'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit35: TsDBEdit
          TabStop = False
          DataField = 'EX_Name3'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit36: TsDBEdit
          Width = 85
          TabStop = False
          DataField = 'Datee'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit37: TsDBEdit
          TabStop = False
          DataField = 'EX_Id'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit38: TsDBEdit
          TabStop = False
          DataField = 'SR_Id'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit39: TsDBEdit
          TabStop = False
          DataField = 'XX_Id'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit40: TsDBEdit
          TabStop = False
          DataField = 'SR_Name1'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit41: TsDBEdit
          TabStop = False
          DataField = 'SR_Name2'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit42: TsDBEdit
          TabStop = False
          DataField = 'Sr_Name3'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit43: TsDBEdit
          TabStop = False
          DataField = 'BNFEMAILID'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit44: TsDBEdit
          TabStop = False
          DataField = 'BNFDOMAIN'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sPanel5: TsPanel
          Enabled = False
        end
        inherited sDBEdit45: TsDBEdit
          TabStop = False
          DataField = 'Doc1'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit46: TsDBEdit
          TabStop = False
          DataField = 'Doc2'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit47: TsDBEdit
          TabStop = False
          DataField = 'Doc3'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit48: TsDBEdit
          TabStop = False
          DataField = 'Doc4'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit49: TsDBEdit
          TabStop = False
          DataField = 'Doc5'
          DataSource = dsList
          Enabled = False
          ReadOnly = False
        end
      end
      inherited sPanel6: TsPanel
        Width = 880
        inherited sDBMemo1: TsDBMemo
          DataField = 'Desc_1'
          DataSource = dsList
          Enabled = False
          Font.Style = [fsBold]
          ReadOnly = True
          ScrollBars = ssVertical
        end
      end
    end
  end
  object sPanel8: TsPanel [2]
    Left = 730
    Top = 59
    Width = 130
    Height = 24
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    Anchors = [akRight]
    DoubleBuffered = False
    TabOrder = 2
  end
  object sPanel9: TsPanel [3]
    Left = 704
    Top = 59
    Width = 25
    Height = 24
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    Anchors = [akRight]
    DoubleBuffered = False
    TabOrder = 3
    object sSpeedButton4: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9664
      OnClick = sSpeedButton4Click
      Align = alClient
    end
  end
  object sPanel10: TsPanel [4]
    Left = 861
    Top = 59
    Width = 25
    Height = 24
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    Anchors = [akRight]
    DoubleBuffered = False
    TabOrder = 4
    object sSpeedButton8: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9654
      OnClick = sSpeedButton8Click
      Align = alClient
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [Maint_No]'
      '      ,[User_Id]'
      '      ,[Datee]'
      '      ,[EX_Name1]'
      '      ,[EX_Name2]'
      '      ,[EX_Name3]'
      '      ,[SR_Name1]'
      '      ,[SR_Name2]'
      '      ,[Sr_Name3]'
      '      ,[EX_Id]'
      '      ,[SR_Id]'
      '      ,[XX_Id]'
      '      ,[Doc1]'
      '      ,[Doc2]'
      '      ,[Doc3]'
      '      ,[Doc4]'
      '      ,[Doc5]'
      '      ,[Descc]'
      '      ,[Desc_1]'
      '      ,[Chk1]'
      '      ,[Chk2]'
      '      ,[Chk3]'
      '      ,[PRNO]'
      '      ,[BGM_CODE]'
      '      ,[BNFEMAILID]'
      '      ,[BNFDOMAIN]'
      #9'  ,CASE BGM_CODE '
      #9'   WHEN '#39'2AO'#39' THEN '#39#45236#44397#49888#50857#51109#50724#47448#53685#48372#49436#39
      '                   WHEN '#39'2CY'#39' THEN '#39#44396#47588#54869#51064#50724#47448#53685#48372#49436#39' '
      #9'   WHEN '#39'2CZ'#39' THEN '#39#44396#47588#54869#51064#52712#49548#53685#48372#49436#39
      #9'   WHEN '#39'2DO'#39' THEN '#39#45236#44397#49888#50857#51109#52712#49548#53685#48372#49436#39
      #9'   WHEN '#39'961'#39' THEN '#39#51068#48152#51025#45813#49436#39
      'WHEN '#39'RCV'#39' THEN  '#39'['#51217#49688']'#49569#44552#49888#52397'/'#47588#51077#51032#47280' '#39
      'WHEN '#39'SLT'#39' THEN '#39'['#44208#44284']'#49569#44552#49888#52397'/'#47588#51077#51032#47280#39
      #9'   ELSE '#39'UnknwonDocument'#39'+BGM_CODE'
      #9'   END as BGM_NAME'
      '  FROM [GENRESS]')
    Left = 32
    Top = 168
    object qryListMaint_No: TStringField
      FieldName = 'Maint_No'
      Size = 35
    end
    object qryListUser_Id: TStringField
      FieldName = 'User_Id'
      Size = 10
    end
    object qryListDatee: TStringField
      FieldName = 'Datee'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListEX_Name1: TStringField
      FieldName = 'EX_Name1'
      Size = 35
    end
    object qryListEX_Name2: TStringField
      FieldName = 'EX_Name2'
      Size = 35
    end
    object qryListEX_Name3: TStringField
      FieldName = 'EX_Name3'
      Size = 10
    end
    object qryListSR_Name1: TStringField
      FieldName = 'SR_Name1'
      Size = 35
    end
    object qryListSR_Name2: TStringField
      FieldName = 'SR_Name2'
      Size = 35
    end
    object qryListSr_Name3: TStringField
      FieldName = 'Sr_Name3'
      Size = 35
    end
    object qryListEX_Id: TStringField
      FieldName = 'EX_Id'
      Size = 25
    end
    object qryListSR_Id: TStringField
      FieldName = 'SR_Id'
      Size = 25
    end
    object qryListXX_Id: TStringField
      FieldName = 'XX_Id'
      Size = 25
    end
    object qryListDoc1: TStringField
      FieldName = 'Doc1'
      Size = 70
    end
    object qryListDoc2: TStringField
      FieldName = 'Doc2'
      Size = 70
    end
    object qryListDoc3: TStringField
      FieldName = 'Doc3'
      Size = 70
    end
    object qryListDoc4: TStringField
      FieldName = 'Doc4'
      Size = 70
    end
    object qryListDoc5: TStringField
      FieldName = 'Doc5'
      Size = 70
    end
    object qryListDescc: TBooleanField
      FieldName = 'Descc'
    end
    object qryListDesc_1: TMemoField
      FieldName = 'Desc_1'
      BlobType = ftMemo
    end
    object qryListChk1: TBooleanField
      FieldName = 'Chk1'
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBGM_CODE: TStringField
      FieldName = 'BGM_CODE'
      Size = 3
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListBGM_NAME: TStringField
      FieldName = 'BGM_NAME'
      ReadOnly = True
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 176
  end
  object qryDocList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DISTINCT [Doc1]'
      '  FROM [GENRESS]')
    Left = 32
    Top = 200
  end
  object qryDel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [GENRESS]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 80
    Top = 208
    object StringField1: TStringField
      FieldName = 'Maint_No'
      Size = 35
    end
    object StringField2: TStringField
      FieldName = 'User_Id'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'Datee'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object StringField4: TStringField
      FieldName = 'EX_Name1'
      Size = 35
    end
    object StringField5: TStringField
      FieldName = 'EX_Name2'
      Size = 35
    end
    object StringField6: TStringField
      FieldName = 'EX_Name3'
      Size = 10
    end
    object StringField7: TStringField
      FieldName = 'SR_Name1'
      Size = 35
    end
    object StringField8: TStringField
      FieldName = 'SR_Name2'
      Size = 35
    end
    object StringField9: TStringField
      FieldName = 'Sr_Name3'
      Size = 35
    end
    object StringField10: TStringField
      FieldName = 'EX_Id'
      Size = 25
    end
    object StringField11: TStringField
      FieldName = 'SR_Id'
      Size = 25
    end
    object StringField12: TStringField
      FieldName = 'XX_Id'
      Size = 25
    end
    object StringField13: TStringField
      FieldName = 'Doc1'
      Size = 70
    end
    object StringField14: TStringField
      FieldName = 'Doc2'
      Size = 70
    end
    object StringField15: TStringField
      FieldName = 'Doc3'
      Size = 70
    end
    object StringField16: TStringField
      FieldName = 'Doc4'
      Size = 70
    end
    object StringField17: TStringField
      FieldName = 'Doc5'
      Size = 70
    end
    object BooleanField1: TBooleanField
      FieldName = 'Descc'
    end
    object MemoField1: TMemoField
      FieldName = 'Desc_1'
      BlobType = ftMemo
    end
    object BooleanField2: TBooleanField
      FieldName = 'Chk1'
    end
    object StringField18: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object StringField19: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object IntegerField1: TIntegerField
      FieldName = 'PRNO'
    end
    object StringField20: TStringField
      FieldName = 'BGM_CODE'
      Size = 3
    end
    object StringField21: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object StringField22: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object StringField23: TStringField
      FieldName = 'BGM_NAME'
      ReadOnly = True
    end
  end
end
