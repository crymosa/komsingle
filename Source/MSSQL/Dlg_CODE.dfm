inherited Dlg_CODE_frm: TDlg_CODE_frm
  Left = 1086
  Top = 428
  Caption = #51068#48152#53076#46300' '#49888#44508'/'#49688#51221
  ClientHeight = 313
  ClientWidth = 334
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 334
    Height = 313
    object sLabel1: TsLabel
      Left = 16
      Top = 24
      Width = 4
      Height = 15
    end
    object sBevel1: TsBevel
      Left = 5
      Top = 245
      Width = 324
      Height = 10
      Shape = bsBottomLine
    end
    object sPanel2: TsPanel
      Left = 16
      Top = 16
      Width = 137
      Height = 25
      Caption = #51068#48152#53076#46300' - '#48516#47448
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object Edt_Gubun: TsEdit
      Left = 64
      Top = 49
      Width = 121
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #44396#48516#53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_Name: TsEdit
      Left = 64
      Top = 76
      Width = 249
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #47749#52845
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sPanel3: TsPanel
      Left = 16
      Top = 128
      Width = 137
      Height = 25
      Caption = #51068#48152#53076#46300' - '#49345#49464#45936#51060#53552
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object edt_CODE: TsEdit
      Left = 64
      Top = 161
      Width = 121
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_Contents: TsEdit
      Left = 64
      Top = 188
      Width = 249
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #54637#47785
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object chk_Use: TsCheckBox
      Left = 62
      Top = 216
      Width = 84
      Height = 20
      Caption = #51312#54924#49884' '#49324#50857
      Checked = True
      State = cbChecked
      TabOrder = 6
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object Btn_ok: TsButton
      Left = 90
      Top = 272
      Width = 75
      Height = 25
      Caption = #54869#51064
      TabOrder = 7
      OnClick = Btn_okClick
      SkinData.SkinSection = 'BUTTON'
    end
    object Btn_Cancel: TsButton
      Left = 169
      Top = 272
      Width = 75
      Height = 25
      Caption = #52712#49548
      TabOrder = 8
      OnClick = Btn_CancelClick
      SkinData.SkinSection = 'BUTTON'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 0
    Top = 200
  end
  object qryGroupIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'Gubun'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [CODE2NDM]([Prefix],[Name],[Gubun])'
      'VALUES (:Prefix, :Name, :Gubun)')
    Left = 232
    Top = 104
  end
  object qryGroupMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'Gubun'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [CODE2NDM]'
      'SET [Name] = :Name'
      '   ,[Gubun] = :Gubun'
      'WHERE Prefix = :Prefix')
    Left = 264
    Top = 104
  end
  object qryCodeIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'Remark'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Map'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [CODE2NDD]([Prefix],[CODE],[Remark],[Map],[NAME])'
      'VALUES ( :Prefix, :CODE, :Remark, :Map, :NAME)')
    Left = 232
    Top = 136
  end
  object qryCodeMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Remark'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Map'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Code'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [CODE2NDD]'
      'SET [Remark] = :Remark'
      #9',[Map]    = :Map'
      #9',[NAME]   = :Name'
      'WHERE [Prefix]  = :Prefix'
      'AND [CODE]   = :Code')
    Left = 264
    Top = 136
  end
end
