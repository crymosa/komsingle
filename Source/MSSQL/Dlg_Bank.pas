unit Dlg_Bank;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sButton, sEdit, sSkinProvider, ExtCtrls,
  sPanel,TypeDefine, ADODB, DB, StrUtils;

type
  TDlg_Bank_frm = class(TDialogParent_frm)
    edt_BankCode: TsEdit;
    edt_BankName1: TsEdit;
    edt_BankBranch1: TsEdit;
    edt_BankName2: TsEdit;
    edt_BankBranch2: TsEdit;
    sButton2: TsButton;
    sButton3: TsButton;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    qryDel: TADOQuery;
    Shape1: TShape;
    procedure sButton2Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
  private
    { Private declarations }
    FProgramControl : TProgramControlType;
    FFields : TFields;
    procedure SetData;
    procedure SetParam(qry : TADOQuery);
    function  CheckCode:Boolean;
    procedure DeleteData(CODE: String);
  public
    { Public declarations }
    function Run(ProgramControl : TProgramControlType; UserFields : TFields):TModalResult;
  end;

var
  Dlg_Bank_frm: TDlg_Bank_frm;

implementation

uses MSSQL, Commonlib;

{$R *.dfm}

{ TDlg_Bank_frm }

function TDlg_Bank_frm.Run(ProgramControl: TProgramControlType;
  UserFields: TFields): TModalResult;
begin
  FProgramControl := ProgramControl;
  FFields := UserFields;

  edt_BankCode.Enabled := FProgramControl = ctInsert;

  Result := mrCancel;

  Case FProgramControl of
    ctInsert :
    begin
      Self.Caption := '은행코드 - [신규]';
    end;

    ctModify :
    begin
      Self.Caption := '은행코드 - [수정]';
      SetData;
    end;

    ctDelete :
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'[코드] : '+FFields.FieldByName('CODE').AsString+#13#10'삭제한 데이터를 복구가 불가능 합니다') Then
      begin
        DeleteData(FFields.FieldByName('CODE').AsString);
        Result := mrOk;
        Exit;
      end
      else
      begin
        Result := mrCancel;
        Exit;
      end;
    end;

  end;

  if Self.ShowModal = mrOk Then
  begin
    Case FProgramControl of
      ctInsert : SetParam(qryIns);
      ctModify : SetParam(qryMod);
    end;
    Result := mrOk;
  end;

end;

procedure TDlg_Bank_frm.SetData;
begin
  edt_BankCode.Text     := FFields.FieldByName('CODE').AsString;
  edt_BankName1.Text    := FFields.FieldByName('ENAME1').AsString;
  edt_BankBranch1.Text  := FFields.FieldByName('ENAME3').AsString;
  edt_BankName2.Text    := FFields.FieldByName('ENAME2').AsString;
  edt_BankBranch2.Text  := FFields.FieldByName('ENAME4').AsString;
//  edt_BANKACCOUNT.Text := FFields.FieldByname('BankAccount').AsString;
//  edt_BANKPW.Text := FFields.FieldByName('BankPW').AsString;
end;

procedure TDlg_Bank_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  IF (FProgramControl = ctInsert) AND (Trim(edt_BankCode.Text) = '') Then
  begin
    edt_BankCode.SetFocus;
    Raise Exception.Create('은행코드를 입력하세요');
  end;

  IF AnsiMatchText('',[ Trim(edt_BankName1.Text) , Trim(edt_BankBranch1.Text) ]) then
  begin
    edt_BankName1.SetFocus;
    Raise Exception.Create('은행명과 지점을 입력하세요');
  end;

  IF CheckCode AND (FProgramControl = ctInsert) Then
  begin
    raise Exception.Create('해당 은행코드는 이미 등록되어 있습니다.');
  end;

  ModalResult := mrOk;
end;

procedure TDlg_Bank_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_Bank_frm.SetParam(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('CODE'  ).Value := edt_BankCode.Text;
    Parameters.ParamByName('ENAME1').Value := edt_BankName1.Text;
    Parameters.ParamByName('ENAME2').Value := edt_BankName2.Text;
    Parameters.ParamByName('ENAME3').Value := edt_BankBranch1.Text;
    Parameters.ParamByName('ENAME4').Value := edt_BankBranch2.Text;
//    Parameters.ParamByName('ENAME5').Value := edt_BANKACCOUNT.Text;
//    Parameters.ParamByName('ENAME6').Value := edt_BANKPW.Text;
    ExecSQL;
  end;
end;

function TDlg_Bank_frm.CheckCode: Boolean;
var
  qry : TADOQuery;
begin
  Result := False;

  qry := TADOQuery.Create(Self);

  try
    with qry do
    begin
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT 1 FROM BANKCODE WHERE CODE = :CODE';
      Parameters.ParamByName('CODE').Value := edt_BankCode.Text;
      Open;

      Result := qry.RecordCount > 0;
    end;
  finally
    qry.Free;
  end;

end;

procedure TDlg_Bank_frm.DeleteData(CODE: String);
begin
  with qryDel do
  begin
    Close;
    Parameters.ParamByName('CODE').Value := CODE;
    ExecSQL;
  end;
end;

end.
