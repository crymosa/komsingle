inherited Dlg_APPPCR_Documents_frm: TDlg_APPPCR_Documents_frm
  Left = 1010
  Top = 198
  Caption = ''
  ClientHeight = 445
  ClientWidth = 469
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 469
    Height = 445
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 73
      Height = 25
      Caption = 'APPPCR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 237
      Height = 15
      Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49888#52397#49436' - '#44540#44144#49436#47448' '#51077#47141
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_Save: TsSpeedButton
      Tag = 2
      Left = 359
      Top = 3
      Width = 56
      Height = 50
      Cursor = crHandPoint
      Caption = #51200#51109
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_SaveClick
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 7
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Cancel: TsSpeedButton
      Tag = 1
      Left = 412
      Top = 3
      Width = 52
      Height = 50
      Cursor = crHandPoint
      Caption = #52712#49548
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_CancelClick
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 18
      Images = DMICON.System24
      Reflected = True
    end
    object sLabel4: TsLabel
      Left = 119
      Top = 350
      Width = 236
      Height = 15
      Caption = #50756#51228#54408#51068' '#44221#50864#45716' '#44552#50529#51077#47141#51012' '#49373#47029#54624' '#49688' '#51080#51020
      Color = clGreen
      ParentColor = False
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sLabel5: TsLabel
      Left = 158
      Top = 415
      Width = 296
      Height = 15
      Caption = #44540#44144#49436#47448#44032' '#45817#49324#51088#44036#51032' '#44228#50557#49436#46321' '#51088#52404#48156#44553' '#49436#47448#51064' '#44221#50864
      Color = clGreen
      ParentColor = False
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sPanel2: TsPanel
      Left = 0
      Top = 52
      Width = 660
      Height = 2
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
    end
    object Edt_Seq: TsEdit
      Left = 120
      Top = 60
      Width = 35
      Height = 21
      Hint = 'SEQ'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49692#48264
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_DOC_G: TsEdit
      Left = 120
      Top = 82
      Width = 35
      Height = 21
      Hint = 'DOC_G'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnDblClick = Edt_DOC_GDblClick
      OnExit = Edt_DOC_GExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44540#44144#49436#47448
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_QTY: TsEdit
      Left = 156
      Top = 82
      Width = 191
      Height = 21
      TabStop = False
      Color = clSkyBlue
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_DOC_D: TsEdit
      Left = 120
      Top = 104
      Width = 227
      Height = 21
      Hint = 'DOC_D'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44540#44144#49436#47448#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BNAME: TsEdit
      Left = 120
      Top = 132
      Width = 89
      Height = 21
      Hint = 'BNAME'
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #54408#47749
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BHS_NO: TsEdit
      Tag = 103
      Left = 120
      Top = 298
      Width = 105
      Height = 21
      Hint = 'BHS_NO'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = 'HS'#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BAMTC: TsEdit
      Left = 120
      Top = 326
      Width = 35
      Height = 21
      Hint = 'BAMTC'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      OnDblClick = Edt_DOC_GDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509' '#44552#50529
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BAMT1: TsEdit
      Tag = 2
      Left = 156
      Top = 326
      Width = 137
      Height = 21
      Hint = 'BAMT'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SHIP_DATE: TsEdit
      Tag = 102
      Left = 120
      Top = 368
      Width = 105
      Height = 21
      Hint = 'SHIP_DATE'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49440#51201#44592#51068
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BAL_CD: TsEdit
      Left = 120
      Top = 390
      Width = 35
      Height = 21
      Hint = 'BAL_CD'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      OnDblClick = Edt_BAL_CDDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44540#44144#49436#47448' '#48156#44553#44592#44288
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BAL_NAME1: TsEdit
      Left = 156
      Top = 390
      Width = 137
      Height = 21
      Hint = 'BAL_NAME1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BAL_NAME2: TsEdit
      Left = 294
      Top = 390
      Width = 137
      Height = 21
      Hint = 'BAL_NAME2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_NAT: TsEdit
      Left = 120
      Top = 412
      Width = 35
      Height = 21
      Hint = 'NAT'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
      OnDblClick = Edt_DOC_GDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49688#52636#45824#49345' '#44397#44032#53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BNAME1: TsMemo
      Left = 120
      Top = 154
      Width = 281
      Height = 69
      Hint = 'BNAME1'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 14
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object Edt_BSIZE1: TsMemo
      Left = 120
      Top = 224
      Width = 281
      Height = 73
      Hint = 'BSIZE1'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 15
      BoundLabel.Active = True
      BoundLabel.Caption = #44508#44201
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeftTop
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'KEYY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'DOC_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DOC_D'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BHS_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'BNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'BNAME1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BAMTC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'EXP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SHIP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'BSIZE1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAL_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BAL_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BAL_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'NAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO APPPCRD2(KEYY, SEQ, DOC_G, DOC_D, BHS_NO, BNAME, BNA' +
        'ME1, BAMT, BAMTC, EXP_DATE, SHIP_DATE, BSIZE1, BAL_NAME1, BAL_NA' +
        'ME2, BAL_CD, NAT)'
      
        'VALUES ( :KEYY, :SEQ, :DOC_G, :DOC_D, :BHS_NO, :BNAME, :BNAME1, ' +
        ':BAMT, :BAMTC, :EXP_DATE, :SHIP_DATE, :BSIZE1, :BAL_NAME1, :BAL_' +
        'NAME2, :BAL_CD, :NAT)')
    Left = 312
    Top = 24
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'DOC_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DOC_D'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BHS_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'BNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'BNAME1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BAMTC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'EXP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SHIP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'BSIZE1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAL_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BAL_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BAL_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'NAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'KEYY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE APPPCRD2'
      'SET '
      ' DOC_G'#9#9'= :DOC_G'
      ',DOC_D'#9#9'= :DOC_D'
      ',BHS_NO'#9#9'= :BHS_NO'
      ',BNAME'#9#9'= :BNAME'
      ',BNAME1'#9#9'= :BNAME1'
      ',BAMT'#9#9'= :BAMT'
      ',BAMTC'#9#9'= :BAMTC'
      ',EXP_DATE'#9'= :EXP_DATE'
      ',SHIP_DATE'#9'= :SHIP_DATE'
      ',BSIZE1'#9#9'= :BSIZE1'
      ',BAL_NAME1'#9'= :BAL_NAME1'
      ',BAL_NAME2'#9'= :BAL_NAME2'
      ',BAL_CD'#9#9'= :BAL_CD'
      ',NAT'#9#9'= :NAT'
      'WHERE KEYY = :KEYY'
      'AND SEQ  = :SEQ')
    Left = 344
    Top = 24
  end
end
