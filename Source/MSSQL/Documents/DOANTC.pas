unit DOANTC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TDOANTC = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      DOANTCInsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;

var
  DOANTC_DOC: TDOANTC;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;
{ TDOANTC }

constructor TDOANTC.Create(Mig: TStringList);
begin
FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  DOANTCInsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TDOANTC.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  DOANTCInsQuery.Free;
  DuplicateQuery.Free;
end;

function TDOANTC.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('DOANTC')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TDOANTC.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TDOANTC.getString(idx: Integer): String;
begin
 Result := Trim(FMig.Strings[idx]);
end;

function TDOANTC.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
begin
  Result := false;

    DOANTCInsQuery.Close;
    DOANTCInsQuery.Connection := DMMssql.KISConnect;
    DOANTCInsQuery.SQL.Text := SQL_DOANTC;
    DOANTCInsQuery.Open;

    //APPEND
    try
      try

        DOANTCInsQuery.Append;

        //Default Values
        DOANTCInsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        DOANTCInsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        DOANTCInsQuery.FieldByName('CHK1').AsString := '';
        DOANTCInsQuery.FieldByName('CHK2').AsString := '';
        DOANTCInsQuery.FieldByName('CHK3').AsString := '';

        DOANTCInsQuery.FieldByName('AMT').AsString := '0';
        DOANTCInsQuery.FieldByName('CHRG').AsString := '0';

        DOANTCInsQuery.FieldByName('PRNO').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          IF SEGMENT('BGM','1','10') Then
          begin
            DOANTCInsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            DOANTCInsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
            DOANTCInsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          else if SEGMENT('NAD','','11') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'MR' then //수신업체
            begin
              DOANTCInsQuery.FieldByName('APP_NAME1').AsString := getString(i+2);
              DOANTCInsQuery.FieldByName('APP_NAME2').AsString := getString(i+3);
              DOANTCInsQuery.FieldByName('APP_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'AX' then //발신기관
            begin
              DOANTCInsQuery.FieldByName('BK_NAME1').AsString := getString(i+2);
              DOANTCInsQuery.FieldByName('BK_NAME2').AsString := getString(i+3);
              DOANTCInsQuery.FieldByName('BK_NAME3').AsString := getString(i+4);
            end;
          end
          else if SEGMENT('RFF','','12') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAC' then // 신용장 구분 및 번호
            begin
              DOANTCInsQuery.FieldByName('LC_G').AsString := getString(i+1);
              DOANTCInsQuery.FieldByName('LC_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'CT' then // 계약서 구분 및 번호
            begin
              DOANTCInsQuery.FieldByName('LC_G').AsString := getString(i+1);
              DOANTCInsQuery.FieldByName('LC_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'BM' then // 선하증권 구분 및 번호
            begin
              DOANTCInsQuery.FieldByName('BL_G').AsString := getString(i+1);
              DOANTCInsQuery.FieldByName('BL_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'AWB' then // 항공화물 운송장 구분 및 번호
            begin
              DOANTCInsQuery.FieldByName('BL_G').AsString := getString(i+1);
              DOANTCInsQuery.FieldByName('BL_NO').AsString := getString(i+2);
            end;
          end
          else if SEGMENT('MOA','','13') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '154' then  // 어음금액
            begin
              DOANTCInsQuery.FieldByName('AMT').AsString := getString(i+2);
              DOANTCInsQuery.FieldByName('AMTC').AsString := getString(i+3);
            end;
            if Qualifier = '304' then //기타수수료
            begin
              DOANTCInsQuery.FieldByName('CHRG').AsString := getString(i+2);
              DOANTCInsQuery.FieldByName('CHRGC').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('DTM','','14') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '184' then // 통지일자
            begin
              DOANTCInsQuery.FieldByName('RES_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '265' then //최종결제일
            begin
              DOANTCInsQuery.FieldByName('SET_DATE').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('FTX','','15') then  // 기타사항
          begin
            if getString(i+1) = 'OSI' then
            begin
              if DOANTCInsQuery.FieldByName('REMARK1').AsString = '' then
              begin
                DOANTCInsQuery.FieldByName('REMARK1').AsString := getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);

              end
              else
              begin
                DOANTCInsQuery.FieldByName('REMARK1').AsString := DOANTCInsQuery.FieldByName('REMARK1').AsString+#1310+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
              end;
            end;
          end
          //통지은행 (사용하지 않는다고 표시되지만 값이 저장되는경우도있음)
          else if SEGMENT('FII','','16') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'BI'  then
            begin
              DOANTCInsQuery.FieldByName('BANK1').AsString := getString(i+2);
              DOANTCInsQuery.FieldByName('BANK2').AsString := getString(i+3);
            end;
          end;
        end;
        
        DOANTCInsQuery.Post;

        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      DOANTCInsQuery.Close;
    end;
end;

procedure TDOANTC.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'DOANTC';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'DOANTC';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TDOANTC.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO);

end;

end.
