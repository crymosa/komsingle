unit PCRLIC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TDOC_PCRLIC = class(TForm)
    sMemo1: TsMemo;
    sEdit1: TsEdit;
    qryFinMaintNo: TADOQuery;
    qryMIns: TADOQuery;
    qryDIns: TADOQuery;
  private
    StartIDX , EndIDX: Integer;
    FMAINT_NO : String;
    procedure MParam(ParamName : String; Value : Variant);
    procedure DParam(ParamName : String; Value : Variant);
    PROCEDURE XParam(ParamName : String; Value : Variant);
    function GetNo(MAINT_NO : string):String;

    function FindGroup(ASEG, BSEG : String):String;
    procedure LINEINSERT(var strList : TStringList);
    procedure TAXINSERT(var strList : TStringList);
    { Private declarations }
  public
    procedure Run;
    procedure Run2;
    { Public declarations }
  end;

var
  DOC_PCRLIC: TDOC_PCRLIC;
Const
  //사용 세그먼트
  USE_SEG : array [0..18] of String = ('UNH','BGM','NAD','BUS','DTM','LIN','PIA','IMD','QTY','FTX','MOA','PRI','ALC','CNT','RFF','UNS','FII','AUT','UNT');
  //중복 세그먼트
  //NAD,DTM,IMD,QTY,FTX,MOA,ALC,RFF
implementation

uses VarDefine, Commonlib, MSSQL;

{$R *.dfm}

{ TDOC_PCRLIC }

procedure TDOC_PCRLIC.DParam(ParamName: String; Value: Variant);
begin
  //Detail Parameter
  qryDIns.Parameters.ParamByName(ParamName).Value := Value;
end;

function TDOC_PCRLIC.GetNo(MAINT_NO: string): String;
begin
  Result := '';
  with qryFinMaintNo do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
    Open;
    IF qryFinMaintNo.RecordCount > 0 Then
      Result := '_'+FormatFloat('000',qryFinMaintNo.RecordCount);
    Close;
  end;
end;

procedure TDOC_PCRLIC.MParam(ParamName: String; Value: Variant);
begin
  qryMIns.Parameters.ParamByName(ParamName).Value := Value;
end;

procedure TDOC_PCRLIC.XParam(ParamName: String; Value: Variant);
begin

end;

procedure TDOC_PCRLIC.Run;
var
  DetailList : TStringList;
  nLoop, nSubLoop : integer;
  SEGMENT : string;
  MAINT_NO, IDX_STR : String;
  DetailSEQ, TaxSEQ, CNTSEQ  ,IDX_INT : Integer;
begin
  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';

  DetailSEQ := 0;
  TaxSEQ := 0;
  CNTSEQ := 0;

  try
    MParam('USER_ID',LoginData.sID);
    MParam('DATEE',FormatDateTime('YYYYMMDD',Now));
    MParam('Sunbun',sEdit1.Text);

    for nLoop := 0 to sMemo1.Lines.Count - 1 do
    begin
      DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop];
      SEGMENT := UpperCase(DetailList.Strings[0]);

      //------------------------------------------------------------------------
      //해당 세그먼트가 아니면 오류
      IF not AnsiMatchText(SEGMENT, USE_SEG) Then
        Raise Exception.Create('SEG NOT FOUND'#13#10+SEGMENT);
      //------------------------------------------------------------------------
      //중복 세그먼트 확인
      //NAD,DTM,IMD,QTY,FTX,MOA,ALC

      IF SEGMENT = 'BGM' THEN
      begin
        //BGM+2CJ:::PD1U250417544+YAPPPCR2015000042389         120649+9
        MAINT_NO := DetailList.Strings[2];
        MAINT_NO := MAINT_NO+GetNo(MAINT_NO);
        MParam('MAINT_NO',MAINT_NO);

        Case DetailList.Count of
          4:
          begin
            MParam('MESSAGE1',DetailList.Strings[3]);
            MParam('MESSAGE1','NA');
          end;

          5:
          begin
            MParam('MESSAGE1',DetailList.Strings[3]);
            MParam('MESSAGE1',DetailList.Strings[4]);
          end;
        end;
      end
      else
      IF SEGMENT = 'NAD' then
      begin
        CASE AnsiIndexText(DetailList.Strings[1],['MS','SU']) of
          //NAD+MS+++삼성전자(주):권오현:1248100998+수원시 영통구 삼성로 129(매탄동):P1C2
          //신청인
          0:
          begin
            //상호
            MParam('APP_NAME1', DivideColon(DetailList.Strings[4], 0 ) );
            MParam('APP_NAME2', DivideColon(DetailList.Strings[4], 1 ) );

            //주소
            MParam('APP_ADDR1', DivideColon(DetailList.Strings[5], 0 ) );
            MParam('APP_ADDR2', DivideColon(DetailList.Strings[5], 1 ) );
            MParam('APP_ADDR3', DivideColon(DetailList.Strings[5], 2 ) );

            //사업자등록번호
            MParam('AX_NAME2', DivideColon(DetailList.Strings[4], 2 ) );
          end;

          //NAD+SU+++삼성전기:최치준:1248100979SEMMLB/SEMMLB+경기도 수원시 영통 매영로150
          //공급자
          1:
          begin
            //상호
            MParam('SUP_NAME1', DivideColon(DetailList.Strings[4], 0 ) );
            MParam('SUP_NAME2', DivideColon(DetailList.Strings[4], 1 ) );

            //주소
            MParam('SUP_ADDR1', DivideColon(DetailList.Strings[5], 0 ) );
            MParam('SUP_ADDR2', DivideColon(DetailList.Strings[5], 1 ) );

            //사업자등록번호
            MParam('SUP_ADDR3', DivideColon(DetailList.Strings[4], 2 ) );
          end;
        end;
      end
      ELSE
      IF SEGMENT = 'BUS' THEN
      begin
        //BUS+1:2AA:::원자재
        //구매(공급)원료/물품등의 용도명세 구분
        MParam('ITM_GBN',DivideColon(DetailList.Strings[1] , 1 ) );
      end
      ELSE
      IF SEGMENT = 'DTM' THEN
      begin
        IDX_STR := DivideColon( DetailList.Strings[1] , 0 );
        CASE AnsiIndexText(IDX_STR,['1AG','1AF']) of
          //DTM+1AG:20150407:102
          //확인일자
          0:
          begin
            MParam('APP_DATE', DivideColon( DetailList.Strings[1] , 1 ) );
          end;

          //DTM+1AF:20010402:102
          //구매(공급)일
          1:
          begin
            DParam('APP_DATE',DivideColon( DetailList.Strings[1] , 1 ) );
          end;
        end;
      end
      ELSE
      IF SEGMENT = 'LIN' THEN
      begin
        IF DetailSEQ > 0 Then
        begin
          //품목 INSERT 및 파라미터 리셋
          qryDIns.ExecSQL;
          ParameterReset( qryDIns );
        end;
        //일련품목
        //LIN+1
        DParam('KEYY', MAINT_NO);
        DParam('SEQ', DetailList.Strings[1] );
        Inc(DetailSEQ);
      end
      ELSE
      if SEGMENT = 'PIA' THen
      begin
        //HSCODE
        //PIA+1+9507102000
        DParam('HS_NO', DetailList.Strings[2] );
      end
      Else
      IF SEGMENT = 'IMD' THEN
      begin
        IDX_STR := DivideColon( DetailList.Strings[3] , 0 );
        IF IDX_STR = '1AA' Then
        begin
          IF DetailSEQ > 0 Then
            DParam('NAME1', DivideColon( DetailList.Strings[3] , 3 ) )
          ELSE
          IF TaxSEQ > 0 Then
            //
        end;
      end
      ELSE
      IF SEGMENT = 'QTY' THEN
      begin
        //수량
        //QTY+1:1000:PCE
        IDX_STR := DivideColon( DetailList.Strings[1] , 0 );

        IF IDX_STR = '1' Then
        begin
          IF DetailSEQ > 0 then
          begin
            DParam('QTY', DivideColon( DetailList.Strings[1] , 1 ) );
            DParam('QTYC', DivideColon( DetailList.Strings[1] , 2 ) );
          end
          else
          IF TaxSEQ > 0 Then
          begin
            //
          end;
        end;
      end
      Else
      IF SEGMENT = 'FTX' THEN
      begin
        IF DetailSEQ > 0 Then
        begin
          Case AnsiIndexText(DetailList.Strings[1],['AAA','AAI']) of
            //품목 규격
            //FTX+AAA+++253FBGA,3L,T0.10,100,ADS60170:5158473074 10 LA41-08775A
            0: DParam('SIZE1' , DivideColon( DetailList.Strings[4] , -1 ) );
            //비고
            //FTX+AAI+++순수원화
            1: DParam('REMARK' , DivideColon( DetailList.Strings[4] , -1 ) );
          end;
        end
        Else
        IF TaxSEQ > 0 Then
        begin

        end;
      end
      Else
      IF SEGMENT = 'MOA' THEN
      begin
        IF (DetailSEQ = 0) AND (TaxSEQ = 0) Then
        begin
          Case AnsiIndexText( DivideColon( DetailList.Strings[1] , 0 ) , ['128','2AD','131']) of
            //총금액
            //MOA+128:8500.00:USD
            0:
            begin
              MParam('TAMT1', DivideColon( DetailList.Strings[1] , 1 ) );
              MParam('TAMT1C', DivideColon( DetailList.Strings[1] , 2 ) );
            end;

            //총금액(USD금액 부기)
            //MOA+2AD:8500.00:USD
            1:
            begin
              IF CNTSEQ = 0 Then
                MParam('TAMT2', DivideColon( DetailList.Strings[1] , 1 ) )
              else
                MParam('C2_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
            end;

            2:
            begin
              MParam('C1_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
              MParam('C1_C', DivideColon( DetailList.Strings[1] , 2 ) );
            end;

          end;
        end;
        IF DetailSEQ > 0 Then
        begin
          Case AnsiIndexText( DivideColon( DetailList.Strings[1] , 0 ) , ['203','2AD','146','8']) of
            //금액
            //MOA+203:2676000:KRW
            0:
            begin
              DParam('AMT1', DivideColon( DetailList.Strings[1] , 1 ) );
              DParam('AMT1C', DivideColon( DetailList.Strings[1] , 2 ) );
            end;

            //금액(USD 금액 부기)
            //MOA+2AD:2464.09:USD
            1:
            begin
              IF qryDIns.Parameters.ParamByName('ACHG_G').Value = Null Then
                DParam('AMT2', DivideColon( DetailList.Strings[1] , 1 ) )
              else
                DParam('AC2_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
            end;

            //단가(USD 금액 부기)
            //MOA+146:8.50:USD
            2: DParam('PRI1', DivideColon( DetailList.Strings[1] , 1 ) );

            //변동금액
            3:
            begin
              DParam('AC1_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
              DParam('AC1_C', DivideColon( DetailList.Strings[1] , 2 ) );
            end;
          end
        end
        Else
        if TaxSEQ > 0 Then
        begin

        end;
      end
      ELSE
      IF SEGMENT = 'PRI' THEN
      begin
        //단가
        //PRI+CAL:223:PE:CUP:1:PCE
        IDX_STR := DetailList.Strings[1];
        DParam('PRI2' , DivideColon(IDX_STR,1) );
        DParam('PRI_BASEC' , DivideColon(IDX_STR,5) );
        DParam('PRI_BASE' , DivideColon(IDX_STR,4) );
      end
      Else
      IF SEGMENT = 'ALC' Then
      begin
        //품목별 할인/할증/변동 내역
        //ALC+Q+:2AZ
        IF DetailSEQ > 0 Then
          DParam('ACHG_G',DetailList.Strings[1])
        else
          MParam('CHG_G',DetailList.Strings[1]);
      end
      Else
      IF SEGMENT = 'CNT' Then
      begin
        //마지막 세부사항 INSERT
        IF DetailSEQ > 0 Then
        begin
          qryDIns.ExecSQL;
          DetailSEQ := 0;
        end;
        //총수량
        //CNT+1AA:1000:PCE
        MParam('TQTY', DivideColon( DetailList.Strings[1] , 1 ) );
        MParam('TQTYC', DivideColon( DetailList.Strings[1] , 2 ) );
        inc(CNTSEQ);
      end;


    end;

    qryMIns.ExecSQL;

  finally
    DetailList.Free;
  end;

end;

procedure TDOC_PCRLIC.Run2;
var
  DetailList , GroupList : TStringList;
  nLoop, nSubLoop : integer;
  SEGMENT, IDX_STR : String;
begin
  GroupList := TStringList.Create;

  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';

  DMMssql.BeginTrans;
  try
  try
    MParam('USER_ID',LoginData.sID);
    MParam('DATEE',FormatDateTime('YYYYMMDD',Now));
    MParam('Sunbun',sEdit1.Text);

    for nLoop := 0 to sMemo1.Lines.Count - 1 do
    begin
      DetailList.DelimitedText := AnsiReplaceText(sMemo1.Lines.Strings[nLoop],' ','|');
      SEGMENT := UpperCase(DetailList.Strings[0]);

      //------------------------------------------------------------------------
      //해당 세그먼트가 아니면 오류
      IF not AnsiMatchText(SEGMENT, USE_SEG) Then
        Raise Exception.Create('SEG NOT FOUND'#13#10+SEGMENT);
      //------------------------------------------------------------------------
      //중복 세그먼트 확인
      //NAD,DTM,IMD,QTY,FTX,MOA,ALC
      IF SEGMENT = 'RFF' THEN
      begin
        //RFF+2BD:PD1U250417544
        //RFF+DM:YAPPPCR2015000042389
        Case AnsiIndexText( DivideColon(DetailList.Strings[1] , 0), ['2BD','DM'] ) of
          0: //구매확인서(발급번호)
            MParam('LIC_NO',DivideColon( DetailList.Strings[1] , 1 ) );

          1: //관리번호
          begin
            FMAINT_NO := DivideColon(DetailList.Strings[1] , 1);
            FMAINT_NO := FMAINT_NO+GetNo(FMAINT_NO+'%');
            MParam('MAINT_NO',FMAINT_NO);
          end;
        end;

      end
      else
      IF SEGMENT = 'BGM' THEN
      begin
        //BGM+2CJ:::PD1U250417544+YAPPPCR2015000042389         120649+9
        MParam('BankSend_No',AnsiReplaceText(DetailList.Strings[2],'|',' '));
        MParam('ChgCd', DivideColon(DetailList.Strings[1] , 0) );
        MParam('PCrLic_No', DivideColon(DetailList.Strings[1] , 3) );

        Case DetailList.Count of
          4:
          begin
            MParam('MESSAGE1',DetailList.Strings[3]);
            MParam('MESSAGE2','NA');
          end;

          5:
          begin
            MParam('MESSAGE1',DetailList.Strings[3]);
            MParam('MESSAGE2',DetailList.Strings[4]);
          end;
        end;
      end
      else
      IF SEGMENT = 'BUS' THEN
      begin
        //BUS+1:2AA:::원자재
        //구매(공급)원료/물품등의 용도명세 구분
        MParam('ITM_GBN',DivideColon(DetailList.Strings[1] , 1 ) );
        MParam('ITM_GNAME',DivideColon(DetailList.Strings[1] , 4 ) );
      end
      else
      IF SEGMENT = 'CNT' Then
      begin
        MParam('TQTY', DivideColon( DetailList.Strings[1] , 1 ) );
        MParam('TQTYC', DivideColon( DetailList.Strings[1] , 2 ) );
      end;
    end;

    //LIN찾기
    GroupList.Text := FindGroup('LIN','CNT');
    LINEINSERT(GroupList);

    //세금계산서 찾기
    GroupList.Text := FindGroup('RFF','UNS');
    TAXINSERT(GroupList);

    for nLoop := 0 to sMemo1.Lines.Count - 1 do 
    begin
      DetailList.DelimitedText := AnsiReplaceText(sMemo1.Lines.Strings[nLoop],' ','|');
      SEGMENT := UpperCase(DetailList.Strings[0]);

      //MOA의 2AD한정어는 Continue
      IF (SEGMENT = 'MOA') Then
      begin
        IF DivideColon(DetailList.Strings[1], 0 ) = '2AD' Then Continue;
      end;

      //------------------------------------------------------------------------
      //해당 세그먼트가 아니면 오류
      IF not AnsiMatchText(SEGMENT, USE_SEG) Then
        Raise Exception.Create('SEG NOT FOUND'#13#10+SEGMENT);
      //------------------------------------------------------------------------
      //중복 세그먼트 확인
      //NAD,DTM,IMD,QTY,FTX,MOA,ALC

      IF SEGMENT = 'NAD' then
      begin
        CASE AnsiIndexText(DetailList.Strings[1],['MS','SU','AX']) of
          //NAD+MS+++삼성전자(주):권오현:1248100998+수원시 영통구 삼성로 129(매탄동):P1C2
          //신청인
          0:
          begin
            //상호
            MParam('APP_NAME1', DivideColon(DetailList.Strings[4], 0 ) );
            MParam('APP_NAME2', DivideColon(DetailList.Strings[4], 1 ) );

            //주소
            MParam('APP_ADDR1', DivideColon(DetailList.Strings[5], 0 ) );
            MParam('APP_ADDR2', DivideColon(DetailList.Strings[5], 1 ) );
            MParam('APP_ADDR3', DivideColon(DetailList.Strings[5], 2 ) );

            //사업자등록번호
            MParam('AX_NAME2', DivideColon(DetailList.Strings[4], 2 ) );
          end;

          //NAD+SU+++삼성전기:최치준:1248100979SEMMLB/SEMMLB+경기도 수원시 영통 매영로150
          //공급자
          1:
          begin
            //상호
            MParam('SUP_NAME1', DivideColon(DetailList.Strings[4], 0 ) );
            MParam('SUP_NAME2', DivideColon(DetailList.Strings[4], 1 ) );

            //주소
            MParam('SUP_ADDR1', DivideColon(DetailList.Strings[5], 0 ) );
            MParam('SUP_ADDR2', DivideColon(DetailList.Strings[5], 1 ) );

            //사업자등록번호
            MParam('SUP_ADDR3', DivideColon(DetailList.Strings[4], 2 ) );
          end;

          //확인 기관 전자서명
          //NAD+AX+++KT BANK:은행장 홍길동:1234567890;
          2:
          begin
            MParam('BK_NAME3' , DivideColon( DetailList.Strings[4] , 0 ));
            MParam('BK_NAME4' , DivideColon( DetailList.Strings[4] , 1 ));
            MParam('BK_NAME5' , DivideColon( DetailList.Strings[4] , 2 ));
          end;
        end;
      end
      else
      IF SEGMENT = 'DTM' THEN
      begin
        //확인일자
        //DTM+1AG:20010422:102
        IF DivideColon(DetailList.Strings[1],0) =  '1AG' Then
          MParam('APP_DATE', DivideColon( DetailList.Strings[1] , 1 ) );
      end
      else
      IF SEGMENT = 'MOA' THEN
      begin
        IDX_STR := DivideColon( DetailList.Strings[1] , 0 );

        IF IDX_STR = '128' Then
        begin
          //총금액
          //MOA+128:2676000:KRW
          MParam('TAMT1', DivideColon( DetailList.Strings[1] , 1 ) );
          MParam('TAMT1C', DivideColon( DetailList.Strings[1] , 2 ) );
          IF LeftStr(sMemo1.Lines.Strings[nLoop+1],3) = 'MOA' Then
          begin
            //총금액(USD 부기)
            //MOA+2AD:8500.00:USD
            DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop+1];
            MParam('TAMT2', DivideColon( DetailList.Strings[1] , 1 ) );
            Continue;
          end;
        end
        else
        if IDX_STR = '131' Then
        begin
          //총 할인/할증/변동금액
          //MOA+131:100:USD
          MParam('C1_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
          MParam('C1_C', DivideColon( DetailList.Strings[1] , 2 ) );
          IF LeftStr(sMemo1.Lines.Strings[nLoop+1],3) = 'MOA' Then
          begin
            //총 할인/할증/변동금액(USD 부기)
            //MOA+2AD:8500.00:USD
            DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop+1];
            MParam('C2_AMT', DivideColon( DetailList.Strings[1] , 1 ) );
            Continue;
          end;
        end;
      end
      else
      if SEGMENT = 'ALC' THen
      begin
        //품목별 할인/할증/변동 내역
        //ALC+Q+:2AZ
        MParam('CHG_G',DetailList.Strings[1]);
      end
      else
      IF SEGMENT = 'FTX' Then
      begin
        //발급조건 및 기타사항
        //FTX+AAX+++MESSAGE:MESSAGE
        MParam('LIC_INFO', DivideColon( DetailList.Strings[4] , -1 ));
      end
      else
      IF SEGMENT = 'FII' Then
      begin
        //확인기관
        //FII+B4++0101:25:BOK::::KT BANK:SAMSUNG-DONG BRANCH
        MParam('BK_CD', DivideColon( DetailList.Strings[3] , 0 ) );
        MParam('BK_NAME1', DivideColon( DetailList.Strings[3] , 6 ) );
        MParam('BK_NAME2',DivideColon( DetailList.Strings[3] , 7 ) );
      end;
    end;

    qryMIns.ExecSQL;
//    Self.ShowModal;
    DMMssql.CommitTrans;
  except
    on E:Exception do
    begin
      ShowMessage('에러가 발생하였습니다.'#13#10+E.Message);
      DMMssql.RollbackTrans;
    end;
  end;
  finally
    GroupList.Free;
  end;
end;

function TDOC_PCRLIC.FindGroup(ASEG, BSEG : String):String;
var
  nLoop ,
  DelIDX : Integer;
begin
  StartIDX := 0;
  EndIDX := 0;
  Result := '';
  for nLoop := 0 to sMemo1.Lines.Count - 1 do
  begin
    Case AnsiIndexText(LeftStr(sMemo1.Lines.Strings[nLoop],3) , [ASEG,BSEG]) OF
      0: StartIDX := nLoop;
      1: EndIDX := nLoop-1;
    end;
  end;

  DelIDX := StartIDX;

  for nLoop := StartIDX to EndIDX do
  begin
    Result := Result + sMemo1.Lines.Strings[DelIDX] + #13#10;
    sMemo1.Lines.Delete(DelIDX);
  end;
end;

procedure TDOC_PCRLIC.LINEINSERT(var strList: TStringList);
var
  nLoop,
  InsertCount : Integer;
  SEGMENT ,IDX_STR , Danwi : String;
  DetailList : TStringList;
begin
  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';
  InsertCount := 0;

  try
    for nLoop := 0 to strList.Count - 1 do
    begin
      DetailList.DelimitedText := strList.Strings[nLoop];
      SEGMENT := UpperCase(DetailList.Strings[0]);

      IF SEGMENT = 'LIN' THEN
      begin
        IF InsertCount > 0 Then
        begin
          qryDIns.ExecSQL;
          ParameterReset(qryDIns);
        end;

        DParam('KEYY', FMAINT_NO );
        DParam('SEQ', DetailList.Strings[1]);
        inc(InsertCount);
      end
      else
      IF SEGMENT = 'PIA' Then
      begin
        //HSCODE
        //PIA+1+9507102000
        DParam('HS_NO', LeftStr(DetailList.Strings[2] , 10) );
      end
      Else
      IF SEGMENT = 'IMD' Then
      begin
        //품목
        //IMD+++1AA:::CARBON FISHING RODS
        DParam('NAME1', DivideColon( DetailList.Strings[3] , 3 ) )
      end
      else
      IF SEGMENT = 'QTY' then
      begin
         //수량
         //QTY+1:1000:PCE
        DParam('QTY', DivideColon( DetailList.Strings[1] , 1 ) );
        DParam('QTYC', DivideColon( DetailList.Strings[1] , 2 ) );
      end
      else
      IF SEGMENT = 'FTX' then
      begin
        //FTX+AAA+++253FBGA,3L,T0.10,100,ADS60170:5158473074 10 LA41-08775A
        Case AnsiIndexText(DetailList.Strings[1],['AAA','AAI']) of
          //품목 규격
          //FTX+AAA+++253FBGA,3L,T0.10,100,ADS60170:5158473074 10 LA41-08775A
          0: DParam('SIZE1' , DivideColon( DetailList.Strings[4] , -1 ) );
          //비고
          //FTX+AAI+++순수원화
          1: DParam('REMARK' , DivideColon( DetailList.Strings[4] , -1 ) );
        end;
      end
      else
      IF SEGMENT = 'MOA' THEN
      begin
        //MOA+203:8500:USD
        IDX_STR := DivideColon( DetailList.Strings[1], 0 );

        IF AnsiMatchText(IDX_STR , ['2AD','146']) Then Continue;

        Case AnsiIndexText( IDX_STR , ['203','8']) of
          0:
          begin
            //금액
            Danwi := DivideColon( DetailList.Strings[1], 2 );
            DParam('AMT1' , DivideColon( DetailList.Strings[1], 1 ) );
            DParam('AMT1C', Danwi );
            IF not AnsiMatchText(Danwi , ['KRW','USD'] ) Then
            begin
              //금액(USD부기)
              //MOA+2AD:8500.00:USD
              DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop+1];
              IF DivideColon( DetailList.Strings[1], 0 )  = '2AD' Then
                DParam('AMT2' , DivideColon( DetailList.Strings[1], 1 ) );

              //단가(USD부기)
              //MOA+146:8.50:USD
              DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop+2];
              IF DivideColon( DetailList.Strings[1], 0 )  = '146' Then
                DParam('PRI1' , DivideColon( DetailList.Strings[1], 1 ) );
            end;
          end;

          1:
          begin
            //품목별 할인/할증/변동 금액
            Danwi := DivideColon( DetailList.Strings[1], 2 );
            DParam('AC1_AMT' , DivideColon( DetailList.Strings[1], 1 ) );
            DParam('AC1_C', Danwi );
            IF not AnsiMatchText(Danwi , ['KRW','USD'] ) Then
            begin
              //금액(USD부기)
              //MOA+2AD:8500.00:USD
              DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop+1];
              IF DivideColon( DetailList.Strings[1], 0 )  = '2AD' Then
                DParam('AC2_AMT' , DivideColon( DetailList.Strings[1], 1 ) );
            end;
          end;
        end;
      end
      else
      IF SEGMENT = 'PRI' Then
      begin
        //단가
        //PRI+CAL:8.50:PE:CUP:1:EA
        DParam('PRI2' , DivideColon( DetailList.Strings[1], 1 ) );
        //단가기준수량
        DParam('PRI_BASE' , DivideColon( DetailList.Strings[1], 4 ) );
        DParam('PRI_BASEC' , DivideColon( DetailList.Strings[1], 5 ) );
      end
      else
      IF SEGMENT = 'DTM' THen
      begin
        //구매(공급일)
        //DTM+1AF:20010402:102
        DParam('APP_DATE' ,DivideColon( DetailList.Strings[1], 1 ) );
      end
      else
      if SEGMENT = 'ALC' Then
      begin
        //품목별 할인/할증/변동 내역
        //ALC+Q+:2AZ
        DParam('ACHG_G' ,DetailList.Strings[1]);
      end;
    end;

    IF InsertCount > 0 Then
    begin
      qryDIns.ExecSQL;
    end;
    
  finally
    DetailList.Free;
  end;
end;

procedure TDOC_PCRLIC.TAXINSERT(var strList: TStringList);
var
  nLoop,
  InsertCount : Integer;
  SEGMENT ,IDX_STR , Danwi : String;
  DetailList : TStringList;
begin
  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';
  InsertCount := 0;

  try
    try
      for nLoop := 0 to strList.Count - 1 do
      begin
        DetailList.DelimitedText := strList.Strings[nLoop];
        SEGMENT := UpperCase(DetailList.Strings[0]);

        IF SEGMENT = 'RFF' THEN
        begin
          //RFF+FE:200104050000-12345
          IF DivideColon( DetailList.Strings[1] , 0) = 'FE' Then
          begin
            IF InsertCount > 0 Then
            begin
              qryDIns.ExecSQL;
              ParameterReset(qryDIns);
            end;

            DParam('KEYY', FMAINT_NO );
            inc(InsertCount);
            DParam('SEQ', InsertCount);
            DParam('BILL_NO' , DivideColon( DetailList.Strings[1], 1 ));
          end;
        end
        else
        if SEGMENT = 'DTM' Then
        begin
          //DTM+1AH:20010422:102
          IF DivideColon( DetailList.Strings[1] , 0) = '1AH' Then
          begin
            DParam('BILL_DATE' , DivideColon( DetailList.Strings[1], 1 ));
          end;
        end
        else
        if SEGMENT = 'MOA' Then
        begin
          IDX_STR := DivideColon( DetailList.Strings[1] , 0);
          CAse AnsiIndexText(IDX_STR,['11','150']) of
            //MOA+11:10000000:KRW
            0:
            begin
              //공급가액
              DParam( 'BILL_AMOUNT' , DivideColon( DetailList.Strings[1] , 1) );
              //단위
              DParam( 'BILL_AMOUNT_UNIT' , DivideColon( DetailList.Strings[1] , 2) );
            end;

            //MOA+150:1000000:KRW
            1:
            begin
              //세액
              DParam( 'TAX_AMOUNT' , DivideColon( DetailList.Strings[1] , 1) );
              //단위
              DParam( 'TAX_AMOUNT_UNIT' , DivideColon( DetailList.Strings[1] , 1) );
            end;
          end;
        end
        else
        if SEGMENT = 'IMD' THEN
        begin
          //품목
          //IMD+++1AA:::CARBON FISHING RODS
          DParam('ITEM_NAME1' , DivideColon( DetailList.Strings[3] , 3) );
        end
        else
        IF SEGMENT = 'FTX' Then
        begin
          //규격
          //FTX+AAA+++QPC501
          DParam('ITEM_DESC' , DivideColon( DetailList.Strings[4] , -1 ));
        end
        else
        IF SEGMENT = 'QTY' then
        begin
          //수량
          //QTY+1:1000:PCR
          DParam('QUANTITY' , DivideColon( DetailList.Strings[1] , 1 ) );
          DParam('QUANTITY_UNIT' , DivideColon( DetailList.Strings[1] , 2 ) );
        end;
      end;

      IF InsertCount > 0 Then
      begin
        qryDIns.ExecSQL;
      end;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    DetailList.Free;
  end;
end;

end.
