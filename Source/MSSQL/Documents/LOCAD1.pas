unit LOCAD1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TLOCAD1 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      LOCAD1_InsQuery  : TADOQuery;
      LOCAD1NEGO_InsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  LOCAD1_DOC: TLOCAD1;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;
{ TLOCAD1 }

constructor TLOCAD1.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  LOCAD1_InsQuery := TADOQuery.Create(nil);
  LOCAD1NEGO_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TLOCAD1.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;  
  LOCAD1_InsQuery.Free;
  LOCAD1NEGO_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TLOCAD1.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCNAME = '+QuotedStr('LOCAD1')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TLOCAD1.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TLOCAD1.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TLOCAD1.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount : integer;
  Qualifier : string;
begin
  Result := false;
  with LOCAD1_InsQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := SQL_LOCAD1;
    Open;

    //APPEND
    try
      try
        Append;

        //Default Values
        FieldByName('User_Id').AsString := LoginData.sID;
        FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        FieldByName('CHK1').AsString := '';
        FieldByName('CHK2').AsString := '';
        FieldByName('CHK3').AsString := '';
        FieldByName('DOCCOPY1').AsString := '0';
        FieldByName('DOCCOPY2').AsString := '0';
        FieldByName('DOCCOPY3').AsString := '0';
        FieldByName('DOCCOPY4').AsString := '0';
        FieldByName('DOCCOPY5').AsString := '0';
        FieldByName('DOC_ETC').AsString := '';
        FieldByName('OFFERNO1').AsString := '';
        FieldByName('OFFERNO2').AsString := '';
        FieldByName('OFFERNO3').AsString := '';
        FieldByName('OFFERNO4').AsString := '';
        FieldByName('OFFERNO5').AsString := '';
        FieldByName('OFFERNO6').AsString := '';
        FieldByName('OFFERNO7').AsString := '';
        FieldByName('OFFERNO8').AsString := '';
        FieldByName('OFFERNO9').AsString := '';
        FieldByName('GOODDES').AsString := 'Y';
        FieldByName('REMARK').AsString := 'N';
        FieldByName('RFF_NO').AsString := '';
        FieldByName('PRNO').AsString := '0';
        FieldByName('CD_PERP').AsString := '0';
        FieldByName('CD_PERM').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          if SEGMENT('BGM','1','10') Then
          begin
            FieldByName('BGM_REF').AsString := getString(i+2);
            FieldByName('MESSAGE1').AsString := getString(i+3);
            FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          else if SEGMENT('BUS','1','11') then
          begin //거래기능
            FieldByName('BUSINESS').AsString := getString(i+2);
          end
          else if SEGMENT('RFF','','12') Then
          begin
            Qualifier := getString(i+1);
            IF Qualifier = 'DM'  Then FieldByName('MAINT_NO').AsString := getString(i+2);  //관리번호
            IF Qualifier = 'LC'  Then FieldByName('LC_NO'   ).AsString := getString(i+2);  //LC번호
            IF Qualifier = 'HS'  Then FieldByName('BSN_HSCODE').AsString := getString(i+2);//대표공급물품 HS부호
            IF Qualifier = 'AAG' Then
            begin
              //물품매도확약서번호 9개까지 사용가능
              FieldByName('OFFERNO'+IntToStr(DocumentCount)).AsString := getString(i+2);
              Inc(DocumentCount);
            end;
            IF Qualifier = '2AG' Then FieldByName('OPEN_NO'   ).AsString := getString(i+2); //개설회차
          end
          else if  SEGMENT('DTM','','13') then
          begin
            Qualifier := getString(i+1);
            //통지일자
            IF Qualifier = '137' Then FieldByName('ISS_DATE').AsString := '20'+getString(i+2);
            //개설일자
            IF Qualifier = '182' Then FieldByName('ADV_DATE').AsString := '20'+getString(i+2);
            //서류제시기간
            IF Qualifier = '272' Then FieldByName('DOC_PRD' ).AsString := getString(i+2);
            //물품인도기일
            IF Qualifier = '2'   Then FieldByName('DELIVERY').AsString := '20'+getString(i+2);
            //유효기일
            IF Qualifier = '123' Then FieldByName('EXPIRY'  ).AsString := '20'+getString(i+2);
          end
          else IF SEGMENT('TSR','1','14') Then
          begin
            //분할허용여부
            FieldByName('TRANSPRT').AsString := getString(i+1);
          end
          else if SEGMENT('FTX','','15') Then
          begin
            Qualifier := getString(i+1);
            //대표 공급물품명
            IF Qualifier = 'AAA' then
            begin
              FieldByName('GOODDES1').AsString := getString(i+2)+#13#10+
                                                  getString(i+3)+#13#10+
                                                  getString(i+4)+#13#10+
                                                  getString(i+5)+#13#10+
                                                  getString(i+6);
            end;
            //기타 정보
            IF Qualifier = 'ACB' then
            begin
              FieldByName('REMARK').AsString := 'Y';
              FieldByName('REMARK1').AsString :=  getString(i+2)+#13#10+
                                                  getString(i+3)+#13#10+
                                                  getString(i+4)+#13#10+
                                                  getString(i+5)+#13#10+
                                                  getString(i+6);
            end;
          end
          else if SEGMENT('FII','1','16') then
          begin
            //금융기관 정보 - 개설은행
            FieldByName('AP_BANK' ).AsString := getString(i+2);
            FieldByName('AP_BANK1').AsString := getString(i+5);
            FieldByName('AP_BANK2').AsString := getString(i+6);
          end
          else if SEGMENT('NAD','','17') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DF' then  //개설의뢰인
            begin
              FieldByName('APPLIC1' ).AsString := getString(i+7);
              FieldByName('APPLIC2' ).AsString := getString(i+8);
              FieldByName('APPLIC3' ).AsString := getString(i+9);
              FieldByName('APPADDR1').AsString := getString(i+10);
              FieldByName('APPADDR2').AsString := getString(i+11);
              FieldByName('APPADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'DG' then  //수혜자
            begin
              FieldByName('BNFEMAILID').AsString := getString(i+5);
              FieldByName('BNFDOMAIN' ).AsString := getString(i+6);
              FieldByName('BENEFC1' ).AsString := getString(i+7);
              FieldByName('BENEFC2' ).AsString := getString(i+8);
              FieldByName('BENEFC3' ).AsString := getString(i+9);
              FieldByName('BNFADDR1').AsString := getString(i+10);
              FieldByName('BNFADDR2').AsString := getString(i+11);
              FieldByName('BNFADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'AX' then  //전자서명
            begin
              FieldByName('EXNAME1').AsString := getString(i+7);
              FieldByName('EXNAME2').AsString := getString(i+8);
              FieldByName('EXNAME3').AsString := getString(i+9);
            end;
          end
          else if SEGMENT('DOC','','18') then //주요 구비서류를 표시
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AH' then //물품수령증명서
            begin
              FieldByName('DOCCOPY1').AsString := getString(i+2);
            end;
            if Qualifier = '2AJ' then //공급자발행 세금계산서 사본
            begin
              FieldByName('DOCCOPY2').AsString := getString(i+2);
              FieldByName('DOCCOPY3').AsString := '0';
            end;
            if Qualifier = '2AK' then //물품명세서가 기재된 송장
            begin
              FieldByName('DOCCOPY2').AsString := '0';
              FieldByName('DOCCOPY3').AsString := getString(i+2);
            end;
            if Qualifier = '2AP' then //본 내국신용장의 사본
            begin
              FieldByName('DOCCOPY4').AsString := getString(i+2);
            end;
            if Qualifier = '310' then //공급자발행 물품매도확약서 사본
            begin
              FieldByName('DOCCOPY5').AsString := getString(i+2);
            end;
          end
          else if SEGMENT('FTX','1','') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'ABX' then //기타구비서류
            begin
              FieldByName('DOC_ETC1').AsString :=  getString(i+2)+#13#10+
                                                   getString(i+3)+#13#10+
                                                   getString(i+4)+#13#10+
                                                   getString(i+5)+#13#10+
                                                   getString(i+6);
            end;
          end
          else if SEGMENT('BUS','1','19') then  //내국신용장을 표시
          begin
            //2AA 원화표시외화부기 내국신용장
            //2AB 외화표시 내국신용장
            //2AC 원화표시 내국신용장
            FieldByName('LOC_TYPE').AsString := getString(i+1);
          end
          else if SEGMENT('MOA','','21') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AD' then //개설외화금액
            begin
              FieldByName('LOC1AMTC').AsString := getString(i+3);
              FieldByName('LOC1AMT' ).AsCurrency := StrToCurr(getString(i+2));
            end;
            if Qualifier = '2AE' then //개설원화금액
            begin
              FieldByName('LOC2AMTC').AsString := getString(i+3);
              FieldByName('LOC2AMT' ).AsCurrency := StrToCurr(getString(i+2));
            end;
          end
          else if SEGMENT('PCD','1','22') then  //허용오차
          begin
            FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos(',',getString(i+2))-1);
            FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos(',',getString(i+2))+1,3);
          end
          else IF SEGMENT('CUX','1','') then //매매기준율
          begin
            FieldByName('EX_RATE').AsString := getString(i+1);
          end
          else if SEGMENT('DOC','1','1A') Then  //개설근거서류 종류
          begin
            FieldByName('DOC_DTL').AsString := getString(i+1);
          end
          else if SEGMENT('RFF','1','') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAC' then //신용장 계약서번호
              FieldByName('DOC_NO').AsString := getString(i+2);
          end
          else if SEGMENT('DTM','','') then
          begin
            Qualifier := getString(i+1);
            //선적(인도)기일을 표시
            IF Qualifier = '38'  then
              FieldByName('LOADDATE').AsString := getString(i+2);
            //유효기일을 표시
            IF Qualifier = '123' then
              FieldByName('EXPDATE').AsString := getString(i+2);
          end
          else if SEGMENT('LOC','1','') then
          begin //수출지역표시
            Qualifier := getString(i+1);
            if Qualifier = '28' then
              FieldByName('DEST').AsString := getString(i+2);
          end
          else if SEGMENT('FII','1','') then
          begin //발행은행(발급기관)을 표시
            Qualifier := getString(i+1);
            if Qualifier = 'AZ' then
            begin
              FieldByName('ISBANK1').AsString := getString(i+8);
              FieldByName('ISBANK2').AsString := getString(i+9);
            end;
          end
          else if SEGMENT('PAT','1','') then //대금결제조건을 표시
          begin
            Qualifier := getString(i+1);
            if Qualifier = '1' then
              FieldByName('PAYMENT').AsString := getString(i+2);
          end
          else if SEGMENT('FTX','1','') Then //대표수출품목
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAA' then
            begin
              FieldByName('PAYMENT').AsString := getString(i+2)+#13#10+
                                                 getString(i+3)+#13#10+
                                                 getString(i+4)+#13#10+
                                                 getString(i+5)+#13#10+
                                                 getString(i+6);
            end;
          end;

        end;
        LOCAD1_InsQuery.Post;
        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      LOCAD1_InsQuery.Close;
    end;

  end;
end;

procedure TLOCAD1.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'LOCADV';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        //개설업자와 수혜업자에 구분을 위해서 DOCNAME에 값을 문서명으로  작성
        Parameters.ParamByName('DOCNAME').Value := 'LOCAD1';


        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TLOCAD1.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
 