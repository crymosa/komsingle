unit EDIFACT;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Dialogs,
  StdCtrls, ExtCtrls, FileCtrl, StrUtils;

type
  TEDIFACT = class
    private
      FSENDER : String;
      FRECVER : String;
      FDOCCD : String;
      FDOCNO : String;
      FSEGMENT : String;
      FID : String;
      FCOUNT : Integer;
      FITEMS : array [0..29] of String;
      function getItems(index: Integer): String;
      procedure setItems(index: Integer; const Value: String);
    protected
    public
     constructor Create(SEGMENT, ID : String; CNT : integer = 1); overload;
     constructor Create; overload;
     property Items[index:Integer] : String read getItems write setItems;
     property SEGMENT : String read FSEGMENT write FSEGMENT;
     property ID : String read FID write FID;
     property COUNT : Integer read FCOUNT write FCOUNT;
     property SENDER : String read FSENDER write FSENDER;
     property RECVER : String read FRECVER write FRECVER;
     property DOCCD : String read FDOCCD write FDOCCD;
     property DOCNO : String read FDOCNO write FDOCNO;
     function Text(MaxLine : Integer=6):String;
     function Header:String; overload;
     function Header(VerNo:String):String; overload; //전자문서 개정번호가 문서에 따라 달라지기 때문에 생성.
     function Footer:String;
     procedure ItemClear;
  end;

implementation

{ TEDIFACT }


constructor TEDIFACT.Create(SEGMENT, ID: String; CNT: integer);
begin
  FSEGMENT := SEGMENT;
  FID := ID;
  FCOUNT := CNT;
end;

constructor TEDIFACT.Create;
begin
  inherited;
end;

function TEDIFACT.Footer: String;
begin
  Result := 'UNT';
end;

function TEDIFACT.getItems(index: Integer): String;
begin
  Result := FITEMS[index];
end;

function TEDIFACT.Header: String;
begin
  Result := Format('%-18s%-18s%-7s%-4s%-4s%-50s',[FSENDER, FRECVER, FDOCCD, '2', '911', DOCNO])+#13#10'UNH';
end;

function TEDIFACT.Header(VerNo: String): String;
begin
  Result := Format('%-18s%-18s%-7s%-4s%-4s%-50s',[FSENDER, FRECVER, FDOCCD, VerNo, '911', DOCNO])+#13#10'UNH';
end;

procedure TEDIFACT.ItemClear;
var
  i : Integer;
begin
  for i := 0 to High(FITEMS) do
  begin
    FITEMS[i] := '';
  end;
end;

procedure TEDIFACT.setItems(index: Integer; const Value: String);
begin
  FITEMS[index] := Value;
end;

const
  BLANK_CHAR : String = '    ';
function TEDIFACT.Text(MaxLine : Integer=6): String;
var
  i : integer;
begin
  if FCOUNT = 0 Then FCOUNT := 1;
  
  Result := Format('%-4s%-7d%2s',[Uppercase(FSEGMENT), FCOUNT, FID])+#13#10;

  for i := 0 to MaxLine-1 do
  begin
    IF i = (MaxLine-1) Then
      Result := Result+BLANK_CHAR+FITEMS[i]
    else
      Result := Result+BLANK_CHAR+FITEMS[i]+#13#10;
  end;

  FSEGMENT := '';
  FCOUNT := FCOUNT + 1;
  FID := '';

  ItemClear;
end;

end.
