unit ADV700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TADV700 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      ADV700_INSQUERY  : TADOQuery;
      ADV7001_InsQuery : TADOQuery;
      ADV7002_InsQuery : TADOQuery;
      ADV7003_InsQuery : TADOQuery;
      ADV700D_InsQuery : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  ADV700_DOC : TADV700;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TADV700 }

constructor TADV700.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;

  ADV700_INSQUERY  := TADOQuery.Create(nil);
  ADV7001_InsQuery := TADOQuery.Create(nil);
  ADV7002_InsQuery := TADOQuery.Create(nil);
  ADV7003_InsQuery := TADOQuery.Create(nil);
  ADV700D_InsQuery := TADOQuery.Create(nil);

  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TADV700.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  ADV700_INSQUERY.Free;
  ADV7001_InsQuery.Free;
  ADV7002_InsQuery.Free;
  ADV7003_InsQuery.Free;
  ADV700D_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TADV700.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('ADV700')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TADV700.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TADV700.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TADV700.Run_Convert: Boolean;
var
  i, DocumentCount  : Integer;
  Qualifier : string;
  FMAINT_NO : String;
  MEMOString : TStringList;
  sComHeader : string;
begin
  Result := false;

  ADV700_INSQUERY.Close;
  ADV700_InsQuery.Connection := DMMssql.KISConnect;
  ADV700_INSQUERY.SQL.Text := SQL_ADV700;
  ADV700_INSQUERY.Open;

  ADV7001_InsQuery.Close;
  ADV7001_InsQuery.Connection := DMMssql.KISConnect;
  ADV7001_INSQUERY.SQL.Text := SQL_ADV7001;
  ADV7001_INSQUERY.Open;

  ADV7002_INSQUERY.Close;
  ADV7002_InsQuery.Connection := DMMssql.KISConnect;
  ADV7002_INSQUERY.SQL.Text := SQL_ADV7002;
  ADV7002_INSQUERY.Open;


  ADV7003_INSQUERY.Close;
  ADV7003_InsQuery.Connection := DMMssql.KISConnect;
  ADV7003_INSQUERY.SQL.Text := SQL_ADV7003;
  ADV7003_INSQUERY.Open;

  ADV700D_InsQuery.Close;
  ADV700D_InsQuery.Connection := DMMssql.KISConnect;
  ADV700D_INSQUERY.SQL.Text := SQL_ADV700_D;
  ADV700D_INSQUERY.Open;


  MEMOString := TStringList.Create;
  //APPEND
  try
    try
      ADV700_INSQUERY.Append;
      ADV7001_INSQUERY.Append;
      ADV7002_INSQUERY.Append;
      ADV7003_INSQUERY.Append;

      //Default Values
      ADV700_INSQUERY.FieldByName('User_Id').AsString := LoginData.sID;
      ADV700_INSQUERY.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
      ADV700_INSQUERY.FieldByName('CHK1').AsString := '';
      ADV700_INSQUERY.FieldByName('CHK2').AsString := '';
      ADV700_INSQUERY.FieldByName('CHK3').AsString := '';

      //값 초기화
      DocumentCount := 1;

      //분석시작
      for i:= 1 to FMig.Count-1 do
      begin
       // ShowMessage(IntToStr(i));
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;
        IF SEGMENT('BGM','1','10') Then
        begin
          FMAINT_NO := getString(i+2);
          ADV700_INSQUERY.FieldByName('MAINT_NO').AsString := FMAINT_NO;
          ADV700_INSQUERY.FieldByName('MESSAGE1').AsString := getString(i+3);
          if Trim(getString(i+4)) = '' then
            ADV700_INSQUERY.FieldByName('MESSAGE2').AsString := 'NA'
          else
            ADV700_INSQUERY.FieldByName('MESSAGE2').AsString := getString(i+4);
        end
        else if SEGMENT('RFF','','11') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AA' then //통지번호
          begin
            ADV700_INSQUERY.FieldByName('APP_NO').AsString := getString(i+2);
          end;
          if Qualifier = 'AAC' then //신용장번호
          begin
            ADV700_INSQUERY.FieldByName('CD_NO').AsString := getString(i+2);
          end;
          if Qualifier = '2AF' then //선통지참조사항
          begin
            ADV700_INSQUERY.FieldByName('REE_PRE').AsString := getString(i+2);
          end;
        end
        else if SEGMENT('DTM','','12') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '184' then //통지일자
          begin
            ADV700_INSQUERY.FieldByName('APP_DATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '182' then //개설일자
          begin
            ADV700_INSQUERY.FieldByName('ISS_DATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '272' then //서류제시기간
          begin
            if ADV7002_InsQuery.FieldByName('PD_PRSNT1').AsString = '' then
            begin
              ADV7002_InsQuery.FieldByName('PD_PRSNT1').AsString := getString(i+2);
            end
            else
            begin
             if ADV7002_InsQuery.FieldByName('PD_PRSNT2').AsString = '' then
             begin
               ADV7002_InsQuery.FieldByName('PD_PRSNT2').AsString := getString(i+2);
             end
             else
             begin
              if ADV7002_InsQuery.FieldByName('PD_PRSNT3').AsString = '' then
              begin
                ADV7002_InsQuery.FieldByName('PD_PRSNT3').AsString := getString(i+2);
              end
              else
              begin
                if ADV7002_InsQuery.FieldByName('PD_PRSNT4').AsString = '' then
                begin
                  ADV7002_InsQuery.FieldByName('PD_PRSNT4').AsString := getString(i+2);
                end;
              end;
             end;
            end;
          end;
        end
        else if SEGMENT('PAT','','13') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AB' then //화환어음 조건(Draft at...)
          begin
            if ADV7001_InsQuery.FieldByName('DRAFT1').AsString = '' then
            begin
              ADV7001_InsQuery.FieldByName('DRAFT1').AsString := getString(i+3);
            end
            else
            begin
             if ADV7001_InsQuery.FieldByName('DRAFT2').AsString = '' then
             begin
               ADV7001_InsQuery.FieldByName('DRAFT2').AsString := getString(i+3);
             end
             else
             begin
              if ADV7001_InsQuery.FieldByName('DRAFT3').AsString = '' then
              begin
                ADV7001_InsQuery.FieldByName('DRAFT3').AsString := getString(i+3);
              end
              else
              begin
                if ADV7001_InsQuery.FieldByName('DRAFT4').AsString = '' then
                begin
                  ADV7001_InsQuery.FieldByName('DRAFT4').AsString := getString(i+3);
                end;
              end;
             end;
            end;
          end;
          if Qualifier = '6' then //혼합지급조건명세(Mixed Payment Details)
          begin
            if ADV7001_InsQuery.FieldByName('MIX_PAY1').AsString = '' then
            begin
              ADV7001_InsQuery.FieldByName('MIX_PAY1').AsString := getString(i+3);
            end
            else
            begin
             if ADV7001_InsQuery.FieldByName('MIX_PAY2').AsString = '' then
             begin
               ADV7001_InsQuery.FieldByName('MIX_PAY2').AsString := getString(i+3);
             end
             else
             begin
              if ADV7001_InsQuery.FieldByName('MIX_PAY3').AsString = '' then
              begin
                ADV7001_InsQuery.FieldByName('MIX_PAY3').AsString := getString(i+3);
              end
              else
              begin
                if ADV7001_InsQuery.FieldByName('MIX_PAY4').AsString = '' then
                begin
                  ADV7001_InsQuery.FieldByName('MIX_PAY4').AsString := getString(i+3);
                end;
              end;
             end;
            end;
          end;
          if Qualifier = '4' then //연지급조건명세(Deferred Payment Details)
          begin
            if ADV7001_InsQuery.FieldByName('DEF_PAY1').AsString = '' then
            begin
              ADV7001_InsQuery.FieldByName('DEF_PAY1').AsString := getString(i+3);
            end
            else
            begin
             if ADV7001_InsQuery.FieldByName('DEF_PAY2').AsString = '' then
             begin
               ADV7001_InsQuery.FieldByName('DEF_PAY2').AsString := getString(i+3);
             end
             else
             begin
              if ADV7001_InsQuery.FieldByName('DEF_PAY3').AsString = '' then
              begin
                ADV7001_InsQuery.FieldByName('DEF_PAY3').AsString := getString(i+3);
              end
              else
              begin
                if ADV7001_InsQuery.FieldByName('DEF_PAY4').AsString = '' then
                begin
                  ADV7001_InsQuery.FieldByName('DEF_PAY4').AsString := getString(i+3);
                end;
              end;
             end;
            end;
          end;
        end
        else if SEGMENT('FTX','','14') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'ACB' then //기타정보
          begin
            if Trim(ADV700_INSQUERY.FieldByName('ADDINFO_1').AsString) = '' then
            begin
              ADV700_InsQuery.FieldByName('ADDINFO').AsString := 'T';
              ADV700_INSQUERY.FieldByName('ADDINFO_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV700_INSQUERY.FieldByName('ADDINFO_1').AsString := ADV700_INSQUERY.FieldByName('ADDINFO_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = '3FC' then //Applicable Rules
          begin
            ADV7003_InsQuery.FieldByName('APPLICABLE_RULES_1').AsString := getString(i+2);
            ADV7003_InsQuery.FieldByName('APPLICABLE_RULES_2').AsString := getString(i+3);
          end;
          if Qualifier = '2AA' then//신용장종류
          begin
            ADV700_INSQUERY.FieldByName('DOC_CD1').AsString := getString(i+2);
            ADV700_INSQUERY.FieldByName('DOC_CD2').AsString := getString(i+3);
            ADV700_INSQUERY.FieldByName('DOC_CD3').AsString := getString(i+4);
          end;
          if Qualifier = 'ABT' then //부가금액부담(Additional Amounts Covered)
          begin
            ADV700_INSQUERY.FieldByName('AA_CV1').AsString := getString(i+2);
            ADV700_INSQUERY.FieldByName('AA_CV2').AsString := getString(i+3);
            ADV700_INSQUERY.FieldByName('AA_CV3').AsString := getString(i+4);
            ADV700_INSQUERY.FieldByName('AA_CV4').AsString := getString(i+5);
          end;
          if Qualifier = '2AG' then //분할선적(Partial Shipment) 허용여부
          begin
            ADV7003_InsQuery.FieldByName('PSHIP').AsString := getString(i+2);
          end;
          if Qualifier = '2AH' then //환적(Transhipment) 허용여부
          begin
            ADV7003_InsQuery.FieldByName('TSHIP').AsString := getString(i+2);
          end;
          if Qualifier = 'AAA' then //상품(용역)명세
          begin
            if Trim(ADV7002_InsQuery.FieldByName('DESGOOD_1').AsString) = '' then
            begin
              ADV7002_InsQuery.FieldByName('DESGOOD_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('DESGOOD_1').AsString := ADV7002_InsQuery.FieldByName('DESGOOD_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ABX' then //구비서류
          begin
            if Trim(ADV7002_InsQuery.FieldByName('DOCREQU_1').AsString) = '' then
            begin
              ADV7002_InsQuery.FieldByName('DOCREQU_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('DOCREQU_1').AsString := ADV7002_InsQuery.FieldByName('DOCREQU_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ABS' then //부가조건(Additional Conditions)
          begin
            if Trim(ADV7002_InsQuery.FieldByName('ADDCOND_1').AsString) = '' then
            begin
              ADV7002_InsQuery.FieldByName('ADDCOND_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('ADDCOND_1').AsString := ADV7002_InsQuery.FieldByName('ADDCOND_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ALC' then //수수료부담자(Charges)
          begin
            if (Trim(ADV7002_InsQuery.FieldByName('CHARGE1').AsString) = '') and (Trim(ADV7002_InsQuery.FieldByName('CHARGE2').AsString) = '') and
               (Trim(ADV7002_InsQuery.FieldByName('CHARGE3').AsString) = '') then
            begin
              ADV7002_InsQuery.FieldByName('CHARGE1').AsString := getString(i+2);
              ADV7002_InsQuery.FieldByName('CHARGE2').AsString := getString(i+3);
              ADV7002_InsQuery.FieldByName('CHARGE3').AsString := getString(i+4);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('CHARGE4').AsString := getString(i+2);
              ADV7002_InsQuery.FieldByName('CHARGE5').AsString := getString(i+3);
              ADV7002_InsQuery.FieldByName('CHARGE6').AsString := getString(i+4);
            end;
          end;
          if Qualifier = 'ABP' then //확인지시문언(Confirmation Instructions)
          begin
           ADV7002_InsQuery.FieldByName('CONFIRMM').AsString := getString(i+2);
          end;
          if Qualifier = '2AB' then //매입/지급/인수은행에 대한 지시사항(Instructions to the Paying/Accepting/Negotiating Bank)
          begin
            if Trim(ADV7002_InsQuery.FieldByName('INSTRCT_1').AsString) = '' then
            begin
              ADV7002_InsQuery.FieldByName('INSTRCT_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('INSTRCT_1').AsString := ADV7002_InsQuery.FieldByName('INSTRCT_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5);
            end;
          end;
          if Qualifier = '2AE' then //수신은행앞 정보(Sender to Receiver Information)
          begin
            if (Trim(ADV7002_InsQuery.FieldByName('SND_INFO1').AsString) = '') and (Trim(ADV7002_InsQuery.FieldByName('SND_INFO2').AsString) = '') and
               (Trim(ADV7002_InsQuery.FieldByName('SND_INFO3').AsString) = '') then
            begin
              ADV7002_InsQuery.FieldByName('SND_INFO1').AsString := getString(i+2);
              ADV7002_InsQuery.FieldByName('SND_INFO2').AsString := getString(i+3);
              ADV7002_InsQuery.FieldByName('SND_INFO3').AsString := getString(i+4);
            end
            else
            begin
              ADV7002_InsQuery.FieldByName('SND_INFO4').AsString := getString(i+2);
              ADV7002_InsQuery.FieldByName('SND_INFO5').AsString := getString(i+3);
              ADV7002_InsQuery.FieldByName('SND_INFO6').AsString := getString(i+4);
            end;
          end;
        end
        else if SEGMENT('FII','','15') then
        begin
          Qualifier := getString(i+1);
          sComHeader := Qualifier;          
          // 전문발신은행
          if Qualifier = 'AZ' Then
          begin
            ADV700_INSQUERY.FieldByName('AP_BANK').AsString := getString(i+3); //코드
            ADV700_INSQUERY.FieldByName('AP_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV700_INSQUERY.FieldByName('AP_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV700_INSQUERY.FieldByName('AP_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV700_INSQUERY.FieldByName('AP_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          // 전문수신은행
          if Qualifier = '2AA' Then
          begin
            ADV700_INSQUERY.FieldByName('AD_BANK').AsString := getString(i+3); //코드
            ADV700_INSQUERY.FieldByName('AD_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV700_INSQUERY.FieldByName('AD_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV700_INSQUERY.FieldByName('AD_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV700_INSQUERY.FieldByName('AD_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;

          if Qualifier = '2AC' then //개설의뢰인은행(Applicant Bank)
          begin
            ADV700_INSQUERY.FieldByName('APP_ACCNT').AsString := getString(i+2); //계좌번호
            ADV700_INSQUERY.FieldByName('APP_BANK').AsString := getString(i+3); //코드
            ADV700_INSQUERY.FieldByName('APP_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV700_INSQUERY.FieldByName('APP_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV700_INSQUERY.FieldByName('APP_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV700_INSQUERY.FieldByName('APP_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          if Qualifier = 'DA' then //신용장지급방식 및 은행(Available with…by…)
          begin
            ADV7001_InsQuery.FieldByName('AV_ACCNT').AsString := getString(i+2);
            ADV7001_InsQuery.FieldByName('AVAIL').AsString := getString(i+3);
            ADV7001_InsQuery.FieldByName('AV_PAY').AsString := getString(i+6);
            ADV7001_INSQUERY.FieldByName('AVAIL1').AsString := copy(getString(i+7) , 1 ,35);
            ADV7001_INSQUERY.FieldByName('AVAIL2').AsString := copy(getString(i+7) , 36 ,70);
            ADV7001_INSQUERY.FieldByName('AVAIL3').AsString := copy(getString(i+8) , 1 ,35);
            ADV7001_INSQUERY.FieldByName('AVAIL4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          if Qualifier = 'DW' then //어음지급인(Drawee)
          begin
            ADV7001_InsQuery.FieldByName('DR_ACCNT').AsString := getString(i+2);
            ADV7001_InsQuery.FieldByName('DRAWEE').AsString := getString(i+3);
            ADV7001_INSQUERY.FieldByName('DRAWEE1').AsString := copy(getString(i+7) , 1 ,35);
            ADV7001_INSQUERY.FieldByName('DRAWEE2').AsString := copy(getString(i+7) , 36 ,70);
            ADV7001_INSQUERY.FieldByName('DRAWEE3').AsString := copy(getString(i+8) , 1 ,35);
            ADV7001_INSQUERY.FieldByName('DRAWEE4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          if Qualifier = '4AE' then  //확인은행
          begin
            ADV7003_InsQuery.FieldByName('REQ_BANK').AsString := getString(i+3);
            ADV7003_INSQUERY.FieldByName('REQ_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV7003_INSQUERY.FieldByName('REQ_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV7003_INSQUERY.FieldByName('REQ_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV7003_INSQUERY.FieldByName('REQ_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          if Qualifier = 'BD' then  //상환은행(Reimbursement Bank)
          begin
            ADV7002_InsQuery.FieldByName('REI_ACCNT').AsString := getString(i+2);
            ADV7002_InsQuery.FieldByName('REI_BANK').AsString := getString(i+3);
            ADV7002_INSQUERY.FieldByName('REI_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV7002_INSQUERY.FieldByName('REI_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV7002_INSQUERY.FieldByName('REI_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV7002_INSQUERY.FieldByName('REI_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;
          if Qualifier = '2AB' then //최종통지은행("Advise Through" Bank)
          begin
            ADV7002_InsQuery.FieldByName('AVT_ACCNT').AsString := getString(i+2);
            ADV7002_InsQuery.FieldByName('AVT_BANK').AsString := getString(i+3);
            ADV7002_INSQUERY.FieldByName('AVT_BANK1').AsString := copy(getString(i+7) , 1 ,35);
            ADV7002_INSQUERY.FieldByName('AVT_BANK2').AsString := copy(getString(i+7) , 36 ,70);
            ADV7002_INSQUERY.FieldByName('AVT_BANK3').AsString := copy(getString(i+8) , 1 ,35);
            ADV7002_INSQUERY.FieldByName('AVT_BANK4').AsString := copy(getString(i+8) , 36 ,70);
          end;
        end
        else if SEGMENT('COM', '', '20') then
        begin
          // MT700 수신은행 전화번호
          if sComHeader = '2AA' Then
          begin
            ADV700_INSQUERY.FieldByName('AD_BANK5').AsString := getString(i+1); //코드
          end;
          // 최종통지은행 전화번호
//          if sComHeader = '2AB' Then
//          begin
//
//          end;
        end
        else if SEGMENT('NAD','','16') then
        begin
          Qualifier := getString(i+1);
          sComHeader := Qualifier;
          if Qualifier = 'DF' then //개설의뢰인(Applicant)
          begin
            ADV700_INSQUERY.FieldByName('APPLIC1').AsString := getString(i+2);
            ADV700_INSQUERY.FieldByName('APPLIC2').AsString := getString(i+3);
            ADV700_INSQUERY.FieldByName('APPLIC3').AsString := getString(i+4);
            ADV700_INSQUERY.FieldByName('APPLIC4').AsString := getString(i+5);
          end;
          if Qualifier = 'DG' then //수익자(Beneficiary)
          begin
            ADV700_INSQUERY.FieldByName('BENEFC1').AsString := getString(i+2);
            ADV700_INSQUERY.FieldByName('BENEFC2').AsString := getString(i+3);
            ADV700_INSQUERY.FieldByName('BENEFC3').AsString := getString(i+4);
            ADV700_INSQUERY.FieldByName('BENEFC4').AsString := getString(i+5);
            ADV700_INSQUERY.FieldByName('BENEFC5').AsString := getString(i+6);
          end;
          if Qualifier = '2AG' then //통지은행
          begin
            ADV7002_InsQuery.FieldByName('EX_NAME1').AsString := getString(i+7);
            ADV7002_InsQuery.FieldByName('EX_NAME2').AsString := getString(i+8);
            ADV7002_InsQuery.FieldByName('EX_NAME3').AsString := getString(i+9);
            ADV7002_InsQuery.FieldByName('EX_ADDR1').AsString := getString(i+10);
            ADV7002_InsQuery.FieldByName('EX_ADDR2').AsString := getString(i+11);
          end;
        end
        else if SEGMENT('COM','1','21') then
        begin //수익자 전화번호를 표시
          if sComHeader = 'DG' Then
            ADV700_INSQUERY.FieldByName('BENEFC6').AsString := getString(i+1);
        end
        else if SEGMENT('DTM','1','17') then //유효기일(Date of Expiry)
        begin
          ADV700_INSQUERY.FieldByName('EX_DATE').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('LOC','1','22') then //유효장소(Place of Expiry)
        begin
          ADV700_INSQUERY.FieldByName('EX_PLACE').AsString := getString(i+2);
        end
        //------------------------------------------------------------------------------
        // 2019-11-25
        // DTM 19 추가 서류제시기간
        //------------------------------------------------------------------------------
        else if SEGMENT('DTM','1','19') then
        begin
          ADV7003_INSQUERY.FieldByName('PERIOD_IN_DAYS').AsString := getString(i+2);
        end
        //------------------------------------------------------------------------------
        // 2019-11-25
        // FTX 23 서류제시기간 상세기준
        //------------------------------------------------------------------------------
        else if SEGMENT('FTX','1','23') then
        begin
          ADV7003_INSQUERY.FieldByName('PERIOD_DETAIL').AsString := getString(i+2);
        end
        else if SEGMENT('MOA','1','1A') then //개설금액(Currency Code, Amount)
        begin
          ADV700_INSQUERY.FieldByName('CD_AMT').AsCurrency := StrToCurr(getString(i+2));
          ADV700_INSQUERY.FieldByName('CD_CUR').AsString := getString(i+3);
        end
        else if SEGMENT('PCD','1','24') then //과부족허용율(Percentage Credit Amount Tolerance)
        begin
          ADV700_INSQUERY.FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
          ADV700_INSQUERY.FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
        end
        // 2019-11-25
        // 해당 세그먼트 삭제
//        else if SEGMENT('FTX','1','24') then //최대신용장금액(Maximum Credit Amount)
//        begin
//          ADV700_INSQUERY.FieldByName('CD_MAX').AsString := getString(i+6);
//        end
        else if SEGMENT('LOC','','1B') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '149' then //수탁(발송)지(Place of Taking in Charge/Dispatch from …/Place of Receipt)
          begin
            ADV7003_InsQuery.FieldByName('LOAD_ON').AsString := getString(i+2);
          end;
          if Qualifier = '76' then  //선적항(Port of Loading/Airport of Departure)
          begin
            ADV7003_InsQuery.FieldByName('SUNJUCK_PORT').AsString := getString(i+2);
          end;
          if Qualifier = '12' then //도착항(Port of Discharge/Airport of Destination)을 표시
          begin
            ADV7003_InsQuery.FieldByName('DOCHACK_PORT').AsString := getString(i+2);
          end;
          if Qualifier = '148' then //최종 목적지(Place of Final Destination/For Transportation to…/Place of Delivery)
          begin
            ADV7003_InsQuery.FieldByName('FOR_TRAN').AsString := getString(i+2);
          end;
          if Qualifier = 'ZZZ' then
          begin
            //선적항등 정보 없이 최종선적일자나 선적기간만을 사용하고자 할 때 사용
          end;
        end
        else if SEGMENT('DTM','1','25') then //최종선적일자(Latest Date of Shipment)
        begin
          ADV7001_InsQuery.FieldByName('LST_DATE').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('FTX','','26') then //선적기간
        begin
          if (Trim(ADV7001_InsQuery.FieldByName('SHIP_PD1').AsString) = '') and (Trim(ADV7001_InsQuery.FieldByName('SHIP_PD2').AsString) = '') and
             (Trim(ADV7002_InsQuery.FieldByName('SHIP_PD3').AsString) = '') then
          begin
            ADV7001_InsQuery.FieldByName('SHIP_PD1').AsString := getString(i+2);
            ADV7001_InsQuery.FieldByName('SHIP_PD2').AsString := getString(i+3);
            ADV7001_InsQuery.FieldByName('SHIP_PD3').AsString := getString(i+4);
          end
          else
          begin
            ADV7001_InsQuery.FieldByName('SHIP_PD4').AsString := getString(i+2);
            ADV7001_InsQuery.FieldByName('SHIP_PD5').AsString := getString(i+3);
            ADV7001_InsQuery.FieldByName('SHIP_PD6').AsString := getString(i+4);
          end;
        end;
      end; //for end
      ADV7001_INSQUERY.FieldByName('MAINT_NO').AsString := FMAINT_NO;
      ADV7002_INSQUERY.FieldByName('MAINT_NO').AsString := FMAINT_NO;
      ADV7003_InsQuery.FieldByName('MAINT_NO').AsString := FMAINT_NO;

      ADV700_INSQUERY.Post;
      ADV7001_INSQUERY.Post;
      ADV7002_INSQUERY.Post;
      ADV7003_InsQuery.Post;

      Result := True;

    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    ADV700_INSQUERY.Close;
    ADV7001_InsQuery.Close;
    ADV7002_InsQuery.Close;
    ADV7003_InsQuery.Close;
  end;
end;

procedure TADV700.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'ADV700';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'ADV700';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;

end;

function TADV700.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
