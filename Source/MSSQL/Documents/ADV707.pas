unit ADV707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TADV707 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      ADV707_InsQuery  : TADOQuery;
      ADV7072_InsQuery : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  ADV707_DOC : TADV707;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TADV707 }

constructor TADV707.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;

  ADV707_InsQuery  := TADOQuery.Create(nil);
  ADV7072_InsQuery := TADOQuery.Create(nil);

  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TADV707.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  ADV707_InsQuery.Free;
  ADV7072_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TADV707.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('ADV707')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TADV707.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TADV707.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TADV707.Run_Convert: Boolean;
var
  i, DocumentCount  : Integer;
  Qualifier : string;
  FMAINT_NO : String;
  MEMOString : TStringList;
begin
  Result := false;

  ADV707_InsQuery.Close;
  ADV707_InsQuery.Connection := DMMssql.KISConnect;
  ADV707_InsQuery.SQL.Text := SQL_ADV707;
  ADV707_InsQuery.Open;

  ADV7072_InsQuery.Close;
  ADV7072_InsQuery.Connection := DMMssql.KISConnect;
  ADV7072_InsQuery.SQL.Text := SQL_adv7072;
  ADV7072_InsQuery.Open;

  MEMOString := TStringList.Create;
  //APPEND
  try
    try
      ADV707_InsQuery.Append;
      ADV7072_InsQuery.Append;

      //Default Values
      ADV707_InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
      ADV707_InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
      ADV707_InsQuery.FieldByName('CHK1').AsString := '';
      ADV707_InsQuery.FieldByName('CHK2').AsString := '';
      ADV707_InsQuery.FieldByName('CHK3').AsString := '';
      ADV707_InsQuery.FieldByName('prno').AsString := '0';

      //값 초기화
      DocumentCount := 1;

      //분석시작
      for i:= 1 to FMig.Count-1 do
      begin
       // ShowMessage(IntToStr(i));
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;
        IF SEGMENT('BGM','1','10') Then
        begin
          FMAINT_NO := getString(i+2);
          ADV707_InsQuery.FieldByName('MAINT_NO').AsString := FMAINT_NO;
          ADV7072_InsQuery.FieldByName('MAINT_NO').AsString := FMAINT_NO;
          ADV707_InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
          if Trim(getString(i+4)) = '' then
            ADV707_InsQuery.FieldByName('MESSAGE2').AsString := 'NA'
          else
            ADV707_InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
        end
        else if SEGMENT('RFF','','11') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AA' then
          begin
            ADV707_InsQuery.FieldByName('APP_NO').AsString := getString(i+2);
          end;
          if Qualifier = '2AD' then //발신은행 참조사항(Sender's Reference)
          begin
            ADV707_InsQuery.FieldByName('CD_NO').AsString := getString(i+2);
          end;
          if Qualifier = '2AE' then //수신은행 참조사항(Receiver's Reference)
          begin
            if Trim(getString(i+2)) = '' then
              ADV707_InsQuery.FieldByName('RCV_REF').AsString := 'NONREF'
            else
              ADV707_InsQuery.FieldByName('RCV_REF').AsString := getString(i+2);
          end;
          if Qualifier = '2AC' then //개설은행 참조사항(Issuing Bank's Reference)
          begin
            ADV707_InsQuery.FieldByName('IBANK_REF').AsString := getString(i+2);
          end;
          if Qualifier = '2AB' then //조건변경횟수(Number of Amendment)
          begin
            ADV707_InsQuery.FieldByName('AMD_NO').AsString := '1';
            ADV7072_InsQuery.FieldByName('AMD_NO').AsString := '1';
            ADV707_InsQuery.FieldByName('AMD_NO1').AsString := getString(i+2);
          end;
        end
        else if SEGMENT('DTM','','12') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '184' then //조건변경통지일자를 표시
          begin
            ADV707_InsQuery.FieldByName('APP_DATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '182' then //개설일자
          begin
            ADV707_InsQuery.FieldByName('ISS_DATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '2AB' then //조건변경일자(Date of Amendment)
          begin
            ADV707_InsQuery.FieldByName('AMD_DATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '38' then //최종선적일자(Latest Date of Shipment)
          begin
            ADV7072_InsQuery.FieldByName('LST_DATE').AsString := '20' + getString(i+2);
          end;
        end
        else if SEGMENT('INP','','13') then
        begin
          Qualifier := getString(i+2);
          // 문서목적을 표시
          IF Qualifier = '30' Then
            ADV7072_InsQuery.FieldByName('PURP_MSG').AsString := getString(i+3);
          // 신용장취소요청 표시
          IF Qualifier = '29' Then
            ADV7072_InsQuery.FieldByName('CAN_REQ').AsString := getString(i+3);
        end
        else if SEGMENT('ALC','','14') then
        begin
          // 신용장 변경수수료 부담자
          Qualifier := getString(i+1);
          ADV7072_InsQuery.FieldByName('AMD_CHARGE').AsString := Qualifier;

          if Trim(ADV7072_InsQuery.FieldByName('AMD_CHARGE').AsString) = '' then
          begin
            ADV7072_InsQuery.FieldByName('AMD_CHARGE').AsString := getString(i+2);
          end
          else
          begin
            ADV7072_InsQuery.FieldByName('AMD_CHARGE').AsString := ADV7072_InsQuery.FieldByName('AMD_CHARGE').AsString+#13#10+getString(i+2);
          end;
        end
        else if SEGMENT('MOA','','15') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AA' then //신용장 증액분(Increase of Documentary Credit Amount)
          begin
            ADV707_InsQuery.FieldByName('INCD_AMT').AsCurrency := StrToCurr(getString(i+2));
            ADV707_InsQuery.FieldByName('INCD_CUR').AsString := getString(i+3);
          end;
          if Qualifier = '2AB' then //신용장 감액분(Decrease of Documentary Credit Amount)
          begin
            ADV707_InsQuery.FieldByName('DECD_AMD').AsCurrency := StrToCurr(getString(i+2));
            ADV707_InsQuery.FieldByName('DECD_CUR').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('PCD','1','16') then
        begin
          ADV707_InsQuery.FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
          ADV707_InsQuery.FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
        end
        else if SEGMENT('LOC','','17') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '149' then //수탁(발송)지(Place of Taking in Charge/Dispatch from …/Place of Receipt)
          begin
            ADV7072_InsQuery.FieldByName('LOAD_ON').AsString := getString(i+2);
          end;
          if Qualifier = '76' then //선적항(Port of Loading/Airport of Departure)
          begin
            ADV7072_InsQuery.FieldByName('SUNJUCK_PORT').AsString := getString(i+2);
          end;
          if Qualifier = '12' then //도착항(Port of Discharge/Airport of Destination)
          begin
            ADV7072_InsQuery.FieldByName('DOCHACK_PORT').AsString := getString(i+2);
          end;
          if Qualifier = '148' then //최종 목적지(Place of Final Destination/For Transportation to…/Place of Delivery)
          begin
            ADV7072_InsQuery.FieldByName('FOR_TRAN').AsString := getString(i+2);
          end;
        end
        else if SEGMENT('PAT','','18') then
        begin
         Qualifier := getString(i+1);
          if Qualifier = '2AB' then //화환어음 조건(Draft at...)
          begin
            if ADV7072_InsQuery.FieldByName('DRAFT1').AsString = '' then
            begin
              ADV7072_InsQuery.FieldByName('DRAFT1').AsString := getString(i+3);
            end
            else
            begin
             if ADV7072_InsQuery.FieldByName('DRAFT2').AsString = '' then
             begin
               ADV7072_InsQuery.FieldByName('DRAFT2').AsString := getString(i+3);
             end
             else
             begin
              if ADV7072_InsQuery.FieldByName('DRAFT3').AsString = '' then
              begin
                ADV7072_InsQuery.FieldByName('DRAFT3').AsString := getString(i+3);
              end
              else
              begin
                if ADV7072_InsQuery.FieldByName('DRAFT4').AsString = '' then
                begin
                  ADV7072_InsQuery.FieldByName('DRAFT4').AsString := getString(i+3);
                end;
              end;
             end;
            end;
          end;
          if Qualifier = '6' then //혼합지급조건명세(Mixed Payment Details)
          begin
            if ADV7072_InsQuery.FieldByName('MIX_PAY1').AsString = '' then
            begin
              ADV7072_InsQuery.FieldByName('MIX_PAY1').AsString := getString(i+3);
            end
            else
            begin
             if ADV7072_InsQuery.FieldByName('MIX_PAY2').AsString = '' then
             begin
               ADV7072_InsQuery.FieldByName('MIX_PAY2').AsString := getString(i+3);
             end
             else
             begin
              if ADV7072_InsQuery.FieldByName('MIX_PAY3').AsString = '' then
              begin
                ADV7072_InsQuery.FieldByName('MIX_PAY3').AsString := getString(i+3);
              end
              else
              begin
                if ADV7072_InsQuery.FieldByName('MIX_PAY4').AsString = '' then
                begin
                  ADV7072_InsQuery.FieldByName('MIX_PAY4').AsString := getString(i+3);
                end;
              end;
             end;
            end;
          end;
          if Qualifier = '4' then //연지급조건명세(Deferred Payment Details)
          begin
            if ADV7072_InsQuery.FieldByName('DEF_PAY1').AsString = '' then
            begin
              ADV7072_InsQuery.FieldByName('DEF_PAY1').AsString := getString(i+3);
            end
            else
            begin
              if ADV7072_InsQuery.FieldByName('DEF_PAY2').AsString = '' then
              begin
                ADV7072_InsQuery.FieldByName('DEF_PAY2').AsString := getString(i+3);
              end
              else
              begin
                if ADV7072_InsQuery.FieldByName('DEF_PAY3').AsString = '' then
                begin
                  ADV7072_InsQuery.FieldByName('DEF_PAY3').AsString := getString(i+3);
                end
                else
                begin
                  if ADV7072_InsQuery.FieldByName('DEF_PAY4').AsString = '' then
                  begin
                    ADV7072_InsQuery.FieldByName('DEF_PAY4').AsString := getString(i+3);
                  end;
                end;
              end;
            end;
          end;
        end
        else if SEGMENT('FTX','','19') then
        begin
        Qualifier := getString(i+1);
          if Qualifier = 'ACB' then //기타정보
          begin
            if Trim(ADV707_InsQuery.FieldByName('ADDINFO_1').AsString) = '' then
            begin
              ADV707_InsQuery.FieldByName('ADDINFO').AsString := 'T';
              ADV707_InsQuery.FieldByName('ADDINFO_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('ADDINFO_1').AsString := ADV707_InsQuery.FieldByName('ADDINFO_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          IF Qualifier = '3FC' then //신용장 적용규칙
          begin
            ADV7072_INSQUERY.FieldByName('APPLICABLE_RULES_1').AsString := getString(i+2);
            ADV7072_INSQUERY.FieldByName('APPLICABLE_RULES_2').AsString := getString(i+3);
          end;
          IF Qualifier = '2AA' then // 신용장종류변경
          begin
            ADV7072_INSQUERY.FieldByName('DOC_TYPE').AsString := getString(i+2);
          end;
          if Qualifier = 'ABT' then //부가금액부담(Additional Amounts Covered)
          begin
            ADV707_INSQUERY.FieldByName('AA_CV1').AsString := getString(i+2);
            ADV707_INSQUERY.FieldByName('AA_CV2').AsString := getString(i+3);
            ADV707_INSQUERY.FieldByName('AA_CV3').AsString := getString(i+4);
            ADV707_INSQUERY.FieldByName('AA_CV4').AsString := getString(i+5);
          end;
          IF Qualifier = '2AG' then
          begin
            // 분할선적 허용여부 변경표시
            ADV7072_INSQUERY.FieldByName('PSHIP').AsString := getString(i+2);
          end;
          IF Qualifier = '2AH' then
          begin
            // 환적 허용여부 변경표시
            ADV7072_INSQUERY.FieldByName('TSHIP').AsString := getString(i+2);
          end;
          if Qualifier = '2AF' then //선적기간
          begin
            if (Trim(ADV7072_InsQuery.FieldByName('SHIP_PD1').AsString) = '') and (Trim(ADV7072_InsQuery.FieldByName('SHIP_PD2').AsString) = '') and
             (Trim(ADV7072_InsQuery.FieldByName('SHIP_PD3').AsString) = '') then
            begin
              ADV7072_InsQuery.FieldByName('SHIP_PD1').AsString := getString(i+6);
              ADV7072_InsQuery.FieldByName('SHIP_PD2').AsString := getString(i+7);
              ADV7072_InsQuery.FieldByName('SHIP_PD3').AsString := getString(i+8);
            end
            else
            begin
              ADV7072_InsQuery.FieldByName('SHIP_PD4').AsString := getString(i+6);
              ADV7072_InsQuery.FieldByName('SHIP_PD5').AsString := getString(i+7);
              ADV7072_InsQuery.FieldByName('SHIP_PD6').AsString := getString(i+8);
            end;
          end;
          if Qualifier = 'AAA' then
          begin
            // 상품(용역)명세 변경을 표시
            if Trim(ADV707_InsQuery.FieldByName('GOODS_DESC_1').asString) = '' then
            begin
              ADV707_InsQuery.FieldByName('GOODS_DESC').AsString := 'T';
              ADV707_InsQuery.FieldByName('GOODS_DESC_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('GOODS_DESC_1').AsString := ADV707_InsQuery.FieldByName('GOODS_DESC_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ABX' then
          begin
            // 구비서류 변경을 표시
            if Trim(ADV707_InsQuery.FieldByName('DOC_DESC_1').asString) = '' then
            begin
              ADV707_InsQuery.FieldByName('DOC_DESC').AsString := 'T';
              ADV707_InsQuery.FieldByName('DOC_DESC_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('DOC_DESC_1').AsString := ADV707_InsQuery.FieldByName('DOC_DESC_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ABS' then
          begin
            // 부가조건 변경을 표시
            if Trim(ADV707_InsQuery.FieldByName('ADD_DESC_1').asString) = '' then
            begin
              ADV707_InsQuery.FieldByName('ADD_DESC').AsString := 'T';
              ADV707_InsQuery.FieldByName('ADD_DESC_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('ADD_DESC_1').AsString := ADV707_InsQuery.FieldByName('ADD_DESC_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'AEF' then
          begin
            // 수익자에 대한 특별지급조건 변경을 표시
            if Trim(ADV707_InsQuery.FieldByName('SPECIAL_DESC_1').asString) = '' then
            begin
              ADV707_InsQuery.FieldByName('SPECIAL_DESC').AsString := 'T';
              ADV707_InsQuery.FieldByName('SPECIAL_DESC_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('SPECIAL_DESC_1').AsString := ADV707_InsQuery.FieldByName('SPECIAL_DESC_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ALC' then
          begin
            // 수수료부담자 변경을 표시
            ADV7072_InsQuery.FieldByName('CHARGE_NUM1').AsString := getString(i+2);
            ADV7072_InsQuery.FieldByName('CHARGE_NUM2').AsString := getString(i+3);
            ADV7072_InsQuery.FieldByName('CHARGE_NUM3').AsString := getString(i+4);
          end;
          if Qualifier = 'ABP' Then
          begin
            // 확인지시문언
            ADV7072_InsQuery.FieldByName('CON_INST').AsString := getString(i+2);
          end;
          if Qualifier = '2AB' Then
          begin
            // 지급/인수/매입은행에 대한 지시사항
            if Trim(ADV707_InsQuery.FieldByName('INST_DESC_1').asString) = '' then
            begin
              ADV707_InsQuery.FieldByName('INST_DESC_').AsString := 'T';
              ADV707_InsQuery.FieldByName('INST_DESC_1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
            end
            else
            begin
              ADV707_InsQuery.FieldByName('INST_DESC_1').AsString := ADV707_InsQuery.FieldByName('INST_DESC_1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = '2AE' then //수신은행앞 정보(Sender to Receiver Information)
          begin
            if (Trim(ADV7072_InsQuery.FieldByName('SR_INFO1').AsString) = '') and (Trim(ADV7072_InsQuery.FieldByName('SR_INFO2').AsString) = '') and
               (Trim(ADV7072_InsQuery.FieldByName('SR_INFO3').AsString) = '') then
            begin
              ADV7072_InsQuery.FieldByName('SR_INFO1').AsString := getString(i+2);
              ADV7072_InsQuery.FieldByName('SR_INFO2').AsString := getString(i+3);
              ADV7072_InsQuery.FieldByName('SR_INFO3').AsString := getString(i+4);
            end
            else
            begin
              ADV7072_InsQuery.FieldByName('SR_INFO4').AsString := getString(i+2);
              ADV7072_InsQuery.FieldByName('SR_INFO5').AsString := getString(i+3);
              ADV7072_InsQuery.FieldByName('SR_INFO6').AsString := getString(i+4);
            end;
          end;
        end
        else if SEGMENT('FII','','1A') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'AZ' then //전문발신은행(MT707 Sender)을 표시
          begin
            ADV707_INSQUERY.FieldByName('AP_BANK').AsString := getString(i+4); //코드
            ADV707_INSQUERY.FieldByName('AP_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV707_INSQUERY.FieldByName('AP_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV707_INSQUERY.FieldByName('AP_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV707_INSQUERY.FieldByName('AP_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = '2AA' then //전문수신은행(MT707 Receiver)을 표시
          begin
            ADV707_INSQUERY.FieldByName('AD_BANK1').AsString := getString(i+4); //코드
            ADV707_INSQUERY.FieldByName('AD_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV707_INSQUERY.FieldByName('AD_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV707_INSQUERY.FieldByName('AD_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV707_INSQUERY.FieldByName('AD_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = 'AW' then //개설은행(Issuing Bank)
          begin
            ADV707_InsQuery.FieldByName('ISS_ACCNT').AsString := getString(i+2);
            ADV707_InsQuery.FieldByName('ISS_BANK').AsString := getString(i+4);
            ADV707_INSQUERY.FieldByName('ISS_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV707_INSQUERY.FieldByName('ISS_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV707_INSQUERY.FieldByName('ISS_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV707_INSQUERY.FieldByName('ISS_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = 'NB' then  // 비은행개설자
          begin
            ADV7072_InsQuery.FieldByName('NBANK_ISS1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('NBANK_ISS2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('NBANK_ISS3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('NBANK_ISS4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = 'DA' then  // 신용장지급방식 및 은행
          begin
            ADV7072_InsQuery.FieldByName('AVAIL').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('AVAIL1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('AVAIL2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('AVAIL3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('AVAIL4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = 'DW' then  // 어음지급인
          begin
            ADV7072_InsQuery.FieldByName('DRAWEE').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('DRAWEE1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('DRAWEE2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('DRAWEE3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('DRAWEE4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = '4AE' then // 확인은행
          begin
            ADV7072_InsQuery.FieldByName('CO_BANK').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('CO_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('CO_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('CO_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('CO_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = 'BD' then  // 상환은행
          begin
            ADV7072_InsQuery.FieldByName('REI_BANK').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('REI_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('REI_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('REI_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('REI_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
          if Qualifier = '2AB' then // 최종통지은행
          begin
            ADV7072_InsQuery.FieldByName('AVT_BANK').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('AVT_BANK1').AsString := copy(getString(i+8) , 1 ,35);
            ADV7072_InsQuery.FieldByName('AVT_BANK2').AsString := copy(getString(i+8) , 36 ,70);
            ADV7072_InsQuery.FieldByName('AVT_BANK3').AsString := copy(getString(i+9) , 1 ,35);
            ADV7072_InsQuery.FieldByName('AVT_BANK4').AsString := copy(getString(i+9) , 36 ,70);
          end;
        end
        else
        if SEGMENT('COM','','20') then
        begin
          // 전문수신은행 전화번호
          if Qualifier = '2AA' then
          begin
            ADV707_INSQUERY.FieldByName('AD_BANK5').AsString := getString(i+1);
          end;
          if Qualifier = '2AB' then
          begin
//            ADV707_INSQUERY.FieldByName('AD_BANK5').AsString := getString(i+1);
          end;
        end
        else
        if SEGMENT('DTM','1','1B') then //유효기일(Date of Expiry)
        begin
          ADV707_INSQUERY.FieldByName('EX_DATE').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('LOC','1','21') then //유효장소(Place of Expiry)
        begin
          ADV707_INSQUERY.FieldByName('EX_PLACE').AsString := getString(i+2);
        end
        else
        if SEGMENT('NAD','','1C') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'DG' then //원수익자(Beneficiary before this Amendment)
          begin
            ADV707_InsQuery.FieldByName('BENEFC1').AsString := getString(i+2);
            ADV707_InsQuery.FieldByName('BENEFC2').AsString := getString(i+3);
            ADV707_InsQuery.FieldByName('BENEFC3').AsString := getString(i+4);
            ADV707_InsQuery.FieldByName('BENEFC4').AsString := getString(i+5);
            ADV707_InsQuery.FieldByName('BENEFC5').AsString := getString(i+6);
          end;
          if Qualifier = '2AG' then //통지은행 전자서명
          begin
            ADV7072_InsQuery.FieldByName('EX_NAME1').AsString := getString(i+7);
            ADV7072_InsQuery.FieldByName('EX_NAME2').AsString := getString(i+8);
            ADV7072_InsQuery.FieldByName('EX_NAME3').AsString := getString(i+9);
            ADV7072_InsQuery.FieldByName('EX_ADDR1').AsString := getString(i+10);
            ADV7072_InsQuery.FieldByName('EX_ADDR2').AsString := getString(i+11);
          end;
          if Qualifier = 'WX' Then //개설의뢰인 정보변경 표시
          begin
            ADV7072_InsQuery.FieldByName('AMD_APPLIC1').AsString := getString(i+2);
            ADV7072_InsQuery.FieldByName('AMD_APPLIC2').AsString := getString(i+3);
            ADV7072_InsQuery.FieldByName('AMD_APPLIC3').AsString := getString(i+4);
            ADV7072_InsQuery.FieldByName('AMD_APPLIC4').AsString := getString(i+5);
          end;
        end
        else if SEGMENT('COM','1','22') then  //원수익자 전화번호를 표시
        begin
          ADV707_InsQuery.FieldByName('BENEFC6').AsString := getString(i+1);
        end
        else if SEGMENT('DTM','','1D') then //서류제시기간 변경
        begin
          ADV7072_INSQUERY.FieldByName('PERIOD').AsString := getString(i+2);
        end
        else if SEGMENT('FTX','','23') then //서류제시기간 상세기준 변경
        begin
          ADV7072_INSQUERY.FieldByName('PERIOD_TXT').AsString := getString(i+2);
        end;
      end; //for end

      ADV707_InsQuery.Post;
      ADV7072_InsQuery.Post;

      Result := True;

    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    ADV707_InsQuery.Close;
    ADV7072_InsQuery.Close;
  end;
end;

procedure TADV707.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'ADV707';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'ADV707';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TADV707.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
