inherited CustomerAdminMSSQL_frm: TCustomerAdminMSSQL_frm
  Left = 907
  Top = 141
  Caption = #44144#47000#52376#44288#47532' - [MSSQL]'
  ClientHeight = 617
  ClientWidth = 725
  KeyPreview = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter1: TsSplitter
    Top = 42
    Width = 725
    Height = 29
  end
  inherited sPanel1: TsPanel
    Width = 725
    Height = 42
    inherited Btn_New: TsSpeedButton
      Left = 4
      Top = 6
      Width = 65
      Height = 32
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Layout = blGlyphLeft
      OnClick = Btn_NewClick
      Align = alNone
      Images = DMICON.System26
      ImageIndex = 37
    end
    inherited Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 70
      Top = 6
      Width = 65
      Height = 32
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Layout = blGlyphLeft
      OnClick = Btn_NewClick
      Align = alNone
      Images = DMICON.System26
      ImageIndex = 3
    end
    inherited Btn_Del: TsSpeedButton
      Tag = 2
      Left = 136
      Top = 6
      Width = 65
      Height = 32
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Layout = blGlyphLeft
      OnClick = Btn_NewClick
      Align = alNone
      Images = DMICON.System26
      ImageIndex = 27
    end
    inherited sSpeedButton1: TsSpeedButton
      Left = 202
      Top = 3
      Width = 10
      Height = 35
      Align = alNone
    end
    inherited Btn_Close: TsSpeedButton
      Left = 537
      Top = 3
      Width = 62
      Height = 35
      Font.Name = #47569#51008' '#44256#46357
      Layout = blGlyphLeft
      ParentFont = False
      Visible = False
      Align = alNone
      Images = DMICON.System26
    end
    inherited sSpeedButton4: TsSpeedButton
      Tag = 3
      Left = 213
      Top = 6
      Width = 65
      Height = 32
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Layout = blGlyphLeft
      Visible = False
      Align = alNone
      Images = DMICON.System26
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 640
      Top = 3
      Height = 35
      Visible = False
      Align = alNone
    end
    object sButton1: TsButton
      Left = 502
      Top = 4
      Width = 133
      Height = 34
      Caption = #44592#51316#44144#47000#52376' '#44032#51256#50724#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = sButton1Click
    end
    object btnExit: TsButton
      Left = 647
      Top = 3
      Width = 74
      Height = 35
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 1
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
  end
  inherited edt_Find: TsEdit
    Left = 100
    Top = 45
    Height = 23
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    OnKeyUp = edt_FindKeyUp
  end
  inherited sComboBox1: TsComboBox
    Left = 2
    Top = 45
    Height = 23
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    ItemHeight = 17
  end
  inherited sDBGrid1: TsDBGrid
    Top = 71
    Width = 326
    Height = 546
    Color = clGray
    DataSource = dsList
    Font.Name = #47569#51008' '#44256#46357
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TitleFont.Name = #47569#51008' '#44256#46357
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    SkinData.SkinSection = 'PAGECONTROL'
    Columns = <
      item
        Color = clWhite
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #44144#47000#52376#53076#46300
        Width = 74
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'ENAME'
        Title.Caption = #49345#54840
        Width = 217
        Visible = True
      end>
  end
  inherited sPanel2: TsPanel
    Left = 326
    Top = 71
    Height = 546
    Font.Name = #47569#51008' '#44256#46357
    ParentFont = False
    inherited sDBEdit1: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'CODE'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit2: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'TRAD_NO'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit3: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'SAUP_NO'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit4: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ESU1'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit5: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ZIPCD'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit6: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ADDR1'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit7: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ADDR2'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit8: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ADDR3'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit9: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'CITY'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit10: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'NAT'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit11: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'NationName'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit12: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'REP_NAME'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit13: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'REP_TEL'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit14: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ESU2'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit15: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'REP_FAX'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit16: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'DDAN_NAME'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit17: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'DDAN_DEPT'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit18: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'DDAN_TEL'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit19: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'DDAN_FAX'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit20: TsDBEdit
      Width = 89
      Height = 23
      Color = clBtnFace
      DataField = 'Jenja'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit21: TsDBEdit
      Width = 113
      Height = 23
      Color = clBtnFace
      DataField = 'EMAIL_ID'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    inherited sDBEdit22: TsDBEdit
      Height = 23
      Color = clBtnFace
      DataField = 'ENAME'
      DataSource = dsList
      SkinData.CustomColor = True
    end
    object sDBEdit23: TsDBEdit
      Left = 248
      Top = 504
      Width = 129
      Height = 23
      Color = clBtnFace
      DataField = 'EMAIL_DOMAIN'
      DataSource = dsList
      ReadOnly = True
      TabOrder = 22
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = '@'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -13
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.ParentFont = False
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CUSTOM.[CODE]'
      '      ,[TRAD_NO]'
      '      ,[SAUP_NO]'
      '      ,[ENAME]'
      '      ,[REP_NAME]'
      '      ,[REP_TEL]'
      '      ,[REP_FAX]'
      '      ,[DDAN_DEPT]'
      '      ,[DDAN_NAME]'
      '      ,[DDAN_TEL]'
      '      ,[DDAN_FAX]'
      '      ,[ZIPCD]'
      '      ,[ADDR1]'
      '      ,[ADDR2]'
      '      ,[ADDR3]'
      '      ,[CITY]'
      '      ,[NAT]'
      #9'  ,ISNULL(NationName,'#39#39') as NationName'
      '      ,[ESU1]'
      '      ,[ESU2]'
      '      ,[ESU3]'
      '      ,[Jenja]'
      '      ,[ADDR4]'
      '      ,[ADDR5]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '  FROM [CUSTOM] LEFT JOIN (SELECT CODE,NAME as NationName'
      #9#9#9#9#9#9#9'FROM CODE2NDD'
      #9#9#9#9#9#9#9'WHERE Prefix = '#39#44397#44032#39') Nation on Custom.NAT = nation.CODE')
    Left = 80
    Top = 152
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 112
    Top = 152
  end
  object qryCopy: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CUSTOM.[CODE]'
      '      ,[TRAD_NO]'
      '      ,[SAUP_NO]'
      '      ,[ENAME]'
      '      ,[REP_NAME]'
      '      ,[REP_TEL]'
      '      ,[REP_FAX]'
      '      ,[DDAN_DEPT]'
      '      ,[DDAN_NAME]'
      '      ,[DDAN_TEL]'
      '      ,[DDAN_FAX]'
      '      ,[ZIPCD]'
      '      ,[ADDR1]'
      '      ,[ADDR2]'
      '      ,[ADDR3]'
      '      ,[CITY]'
      '      ,[NAT]'
      #9'  ,ISNULL(NationName,'#39#39') as NationName'
      '      ,[ESU1]'
      '      ,[ESU2]'
      '      ,[ESU3]'
      '      ,[Jenja]'
      '      ,[ADDR4]'
      '      ,[ADDR5]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '  FROM [CUSTOM] LEFT JOIN (SELECT CODE,NAME as NationName'
      #9#9#9#9#9#9#9'FROM CODE2NDD'
      #9#9#9#9#9#9#9'WHERE Prefix = '#39#44397#44032#39') Nation on Custom.NAT = nation.CODE')
    Left = 144
    Top = 152
  end
  object Query1: TQuery
    Left = 80
    Top = 184
  end
end
