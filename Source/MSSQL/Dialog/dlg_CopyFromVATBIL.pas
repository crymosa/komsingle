unit dlg_CopyFromVATBIL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_SelectCopyDocument, DB, ADODB, Grids, DBGrids, acDBGrid,
  Mask, sMaskEdit, StdCtrls, sEdit, sComboBox, sButton, Buttons,
  sSpeedButton, ExtCtrls, sPanel;

type
  Tdlg_CopyFromVATBIL_frm = class(Tdlg_SelectCopyDocument_frm)
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    sSQLQuery:String;
    { Private declarations }
  protected
    procedure ReadList; override;
  public
    { Public declarations }
  end;

var
  dlg_CopyFromVATBIL_frm: Tdlg_CopyFromVATBIL_frm;

implementation

{$R *.dfm}

procedure Tdlg_CopyFromVATBIL_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure Tdlg_CopyFromVATBIL_frm.ReadList;
begin
  inherited;   
  with qryList do
  Begin
    Close;
    SQL.Text := sSQLQuery;
//    ShowMessage(sSQLQuery);
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;
    Open;

    if edt_find.Text <> '' then
    begin
      Case com_find.ItemIndex of
        0: SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+edt_find.Text+'%'));
      end;
    end;
//
    SQL.Add('ORDER BY DATEE DESC');
//
    Open;
  end;

end;

procedure Tdlg_CopyFromVATBIL_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('yyyymmdd',Now);
  sMaskEdit2.Text := FormatDateTime('yyyymmdd',Now);
  sSQLQuery := qryList.SQL.Text;
//  ShowMessage(sSQLQuery);
  qryList.Close;
  qryList.Parameters[0].Value := sMaskEdit1.Text;
  qryList.Parameters[1].Value := sMaskEdit2.Text;
  qryList.Open;
end;

procedure Tdlg_CopyFromVATBIL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

end.
