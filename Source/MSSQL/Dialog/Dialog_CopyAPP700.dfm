object Dialog_CopyAPP700_frm: TDialog_CopyAPP700_frm
  Left = 926
  Top = 169
  Width = 937
  Height = 680
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 921
    Height = 35
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      921
      35)
    object edt_SearchText: TsEdit
      Left = 82
      Top = 5
      Width = 229
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 77
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #46321#47197#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #46321#47197#51068#51088
        #44288#47532#48264#54840
        #49688#54812#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 82
      Top = 5
      Width = 80
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 315
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 163
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 208
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 907
      Left = 289
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = sBitBtn23Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 184
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 846
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 776
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 35
    Width = 921
    Height = 606
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATEE'
        Title.Alignment = taCenter
        Title.Caption = #46321#47197#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BENEFC1'
        Title.Alignment = taCenter
        Title.Caption = #49688#54812#51088
        Width = 193
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AP_BANK1'
        Title.Alignment = taCenter
        Title.Caption = #44060#49444#51008#54665
        Width = 164
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'APP_DATE'
        Title.Alignment = taCenter
        Title.Caption = #44060#49444#51068#51088
        Width = 82
        Visible = True
      end>
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      ''
      
        'SELECT A700_1.MAINT_NO,A700_1.MESSAGE1,A700_1.MESSAGE2,A700_1.[U' +
        'SER_ID],A700_1.DATEE,A700_1.APP_DATE,A700_1.IN_MATHOD,A700_1.AP_' +
        'BANK,A700_1.AP_BANK1,A700_1.AP_BANK2,A700_1.AP_BANK3,A700_1.AP_B' +
        'ANK4,A700_1.AP_BANK5,A700_1.AD_BANK,A700_1.AD_BANK1,A700_1.AD_BA' +
        'NK2,A700_1.AD_BANK3,A700_1.AD_BANK4,A700_1.AD_PAY'
      
        '      ,A700_1.IL_NO1,A700_1.IL_NO2,A700_1.IL_NO3,A700_1.IL_NO4,A' +
        '700_1.IL_NO5,A700_1.IL_AMT1,A700_1.IL_AMT2,A700_1.IL_AMT3,A700_1' +
        '.IL_AMT4,A700_1.IL_AMT5,A700_1.IL_CUR1,A700_1.IL_CUR2,A700_1.IL_' +
        'CUR3,A700_1.IL_CUR4,A700_1.IL_CUR5,A700_1.AD_INFO1,A700_1.AD_INF' +
        'O2,A700_1.AD_INFO3,A700_1.AD_INFO4,A700_1.AD_INFO5'
      
        '      ,A700_1.DOC_CD,A700_1.EX_DATE,A700_1.EX_PLACE,A700_1.CHK1,' +
        'A700_1.CHK2,A700_1.CHK3,A700_1.prno,A700_1.F_INTERFACE,A700_1.IM' +
        'P_CD1,A700_1.IMP_CD2,A700_1.IMP_CD3,A700_1.IMP_CD4,A700_1.IMP_CD' +
        '5'
      ''
      
        #9'  ,A700_2.MAINT_NO,A700_2.APPLIC1,A700_2.APPLIC2,A700_2.APPLIC3' +
        ',A700_2.APPLIC4,A700_2.APPLIC5,A700_2.BENEFC,A700_2.BENEFC1,A700' +
        '_2.BENEFC2,A700_2.BENEFC3,A700_2.BENEFC4,A700_2.BENEFC5,A700_2.C' +
        'D_AMT,A700_2.CD_CUR,A700_2.CD_PERP,A700_2.CD_PERM,A700_2.CD_MAX,' +
        'A700_2.AA_CV1,A700_2.AA_CV2,A700_2.AA_CV3'
      
        '      ,A700_2.AA_CV4,A700_2.DRAFT1,A700_2.DRAFT2,A700_2.DRAFT3,A' +
        '700_2.MIX_PAY1,A700_2.MIX_PAY2,A700_2.MIX_PAY3,A700_2.MIX_PAY4,A' +
        '700_2.DEF_PAY1,A700_2.DEF_PAY2,A700_2.DEF_PAY3,A700_2.DEF_PAY4,A' +
        '700_2.PSHIP,A700_2.TSHIP,A700_2.LOAD_ON,A700_2.FOR_TRAN,A700_2.L' +
        'ST_DATE,A700_2.SHIP_PD1,A700_2.SHIP_PD2'
      
        '      ,A700_2.SHIP_PD3,A700_2.SHIP_PD4,A700_2.SHIP_PD5,A700_2.SH' +
        'IP_PD6,A700_2.DESGOOD_1'
      ''
      
        #9'  ,A700_3.MAINT_NO,A700_3.DOC_380,A700_3.DOC_380_1,A700_3.DOC_7' +
        '05,A700_3.DOC_705_1,A700_3.DOC_705_2,A700_3.DOC_705_3,A700_3.DOC' +
        '_705_4,A700_3.DOC_740,A700_3.DOC_740_1,A700_3.DOC_740_2,A700_3.D' +
        'OC_740_3,A700_3.DOC_740_4,A700_3.DOC_530,A700_3.DOC_530_1,A700_3' +
        '.DOC_530_2,A700_3.DOC_271'
      
        '      ,A700_3.DOC_271_1,A700_3.DOC_861,A700_3.DOC_2AA,A700_3.DOC' +
        '_2AA_1,A700_3.ACD_2AA,A700_3.ACD_2AA_1,A700_3.ACD_2AB,A700_3.ACD' +
        '_2AC,A700_3.ACD_2AD,A700_3.ACD_2AE,A700_3.ACD_2AE_1,A700_3.CHARG' +
        'E,A700_3.PERIOD,A700_3.CONFIRMM,A700_3.INSTRCT,A700_3.INSTRCT_1,' +
        'A700_3.EX_NAME1,A700_3.EX_NAME2'
      
        '      ,A700_3.EX_NAME3,A700_3.EX_ADDR1,A700_3.EX_ADDR2,A700_3.OR' +
        'IGIN,A700_3.ORIGIN_M,A700_3.PL_TERM,A700_3.TERM_PR,A700_3.TERM_P' +
        'R_M,A700_3.SRBUHO,A700_3.DOC_705_GUBUN,A700_3.CARRIAGE,A700_3.DO' +
        'C_760,A700_3.DOC_760_1,A700_3.DOC_760_2,A700_3.DOC_760_3,A700_3.' +
        'DOC_760_4,A700_3.SUNJUCK_PORT,A700_3.DOCHACK_PORT'
      ''
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      #9'  ,CARRIAGE700.DOC_NAME as CARRIAGE_NAME'
      ''
      'FROM [dbo].[APP700_1] AS A700_1'
      
        'INNER JOIN [dbo].[APP700_2] AS A700_2 ON A700_1.MAINT_NO = A700_' +
        '2.MAINT_NO'
      
        'INNER JOIN [dbo].[APP700_3] AS A700_3 ON A700_1.MAINT_NO = A700_' +
        '3.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON A700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON A700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON A700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON A700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON A700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON A700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON A700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON A700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON A700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON A700_2.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON A700_2.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON A700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON A700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON A700_3.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON A700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON A700_3.CONFIRMM = CON' +
        'FIRM700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') CARRIAGE700 ON A700_3.CARRIAGE = C' +
        'ARRIAGE700.CODE'
      ''
      ''
      ''
      '')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListCARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListpship_Name: TStringField
      FieldName = 'pship_Name'
      Size = 100
    end
    object qryListtship_Name: TStringField
      FieldName = 'tship_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCONFIRM_Name: TStringField
      FieldName = 'CONFIRM_Name'
      Size = 100
    end
    object qryListCARRIAGE_NAME: TStringField
      FieldName = 'CARRIAGE_NAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
