unit Dialog_BANK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, ExtCtrls, sSplitter, StdCtrls, sEdit, Buttons,
  sBitBtn, sComboBox, sSkinProvider, sPanel, Grids, DBGrids, acDBGrid, DB,
  ADODB, sSpeedButton;

type
  TDialog_BANK_frm = class(TDialogParent_frm)
    sSplitter3: TsSplitter;
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    sDBGrid1: TsDBGrid;
    dsBank: TDataSource;
    qryBank: TADOQuery;
    sPanel2: TsPanel;
    sSplitter1: TsSplitter;
    sBitBtn3: TsBitBtn;
    sBitBtn6: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn1: TsBitBtn;
    sSpeedButton5: TsSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
    procedure edt_SearchKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog(KeyWord : String):TModalResult;
    function ExistsValue(Code : string):Boolean; 
  end;

var
  Dialog_BANK_frm: TDialog_BANK_frm;

implementation

uses CodeContents, Dlg_Bank, TypeDefine, Bank_MSSQL, Bank;

{$R *.dfm}

procedure TDialog_BANK_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryBank.SQL.Text;
end;

procedure TDialog_BANK_frm.ReadList;
begin
  with qryBank do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE CODE LIKE '+QuotedStr('%'+edt_Search.Text+'%'));
      1: SQL.Add('WHERE ENAME1 LIKE '+QuotedStr('%'+edt_Search.Text+'%'));
      2: SQL.Add('WHERE ENAME3 LIKE '+QuotedStr('%'+edt_Search.Text+'%'));
    end;
    SQL.Add('ORDER BY CODE');
    Open;
  end;
end;

procedure TDialog_BANK_frm.btn_SearchClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_BANK_frm.edt_SearchKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ReadList;
  end;
end;

procedure TDialog_BANK_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

function TDialog_BANK_frm.openDialog(KeyWord: String): TModalResult;
begin
  ReadList;
  If Trim(KeyWord) <> '' Then
  begin
    qryBank.Locate('CODE',KeyWord,[]);
  end;
  Result := ShowModal;
end;

procedure TDialog_BANK_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDialog_BANK_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDialog_BANK_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_BANK_frm.sBitBtn3Click(Sender: TObject);
var
  BANK_CODE : String;
begin
  inherited;
  Dlg_Bank_frm := TDlg_Bank_frm.Create(Self);
  try
    case (Sender as TsBitBtn).Tag of
      0 :
      begin
        IF Dlg_Bank_frm.Run(ctInsert,nil) = mrOK Then
        begin
          BANK_CODE := Dlg_Bank_frm.edt_BankCode.Text;
          qryBank.Close;
          qryBank.Open;
          qryBank.Locate('CODE',BANK_CODE,[]);
        end;
      end;
      1 :
      begin
        IF Dlg_Bank_frm.Run(ctModify,dsBank.DataSet.Fields) = mrOK Then
        begin
          BANK_CODE := Dlg_Bank_frm.edt_BankCode.Text;
          qryBank.Close;
          qryBank.Open;
          qryBank.Locate('CODE',BANK_CODE,[]);
        end;
      end;
      2 :
      begin
        IF Dlg_Bank_frm.Run(ctDelete,dsBank.DataSet.Fields) = mrOK Then
        begin
          qryBank.Close;
          qryBank.Open;
        end;
      end;
    end;

  Finally
     FreeAndNil( Dlg_Bank_frm );
  end;
end;

function TDialog_BANK_frm.ExistsValue(Code: string): Boolean;
begin
  dsBank.DataSet.Close;
  dsBank.DataSet.Open;
  dsBank.DataSet.First;
  Result := dsBank.DataSet.Locate('CODE',Code,[]);
end;

end.
