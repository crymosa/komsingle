unit Dialog_CopyAPP700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls, Buttons, sBitBtn,
  Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls, sPanel;

type
  TDialog_CopyAPP700_frm = class(TForm)
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListDOC_CD: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListMAINT_NO_2: TStringField;
    qryListDOC_380: TBooleanField;
    qryListDOC_380_1: TBCDField;
    qryListDOC_705: TBooleanField;
    qryListDOC_705_1: TStringField;
    qryListDOC_705_2: TStringField;
    qryListDOC_705_3: TStringField;
    qryListDOC_705_4: TStringField;
    qryListDOC_740: TBooleanField;
    qryListDOC_740_1: TStringField;
    qryListDOC_740_2: TStringField;
    qryListDOC_740_3: TStringField;
    qryListDOC_740_4: TStringField;
    qryListDOC_530: TBooleanField;
    qryListDOC_530_1: TStringField;
    qryListDOC_530_2: TStringField;
    qryListDOC_271: TBooleanField;
    qryListDOC_271_1: TBCDField;
    qryListDOC_861: TBooleanField;
    qryListDOC_2AA: TBooleanField;
    qryListDOC_2AA_1: TMemoField;
    qryListACD_2AA: TBooleanField;
    qryListACD_2AA_1: TStringField;
    qryListACD_2AB: TBooleanField;
    qryListACD_2AC: TBooleanField;
    qryListACD_2AD: TBooleanField;
    qryListACD_2AE: TBooleanField;
    qryListACD_2AE_1: TMemoField;
    qryListCHARGE: TStringField;
    qryListPERIOD: TBCDField;
    qryListCONFIRMM: TStringField;
    qryListINSTRCT: TBooleanField;
    qryListINSTRCT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListORIGIN: TStringField;
    qryListORIGIN_M: TStringField;
    qryListPL_TERM: TStringField;
    qryListTERM_PR: TStringField;
    qryListTERM_PR_M: TStringField;
    qryListSRBUHO: TStringField;
    qryListDOC_705_GUBUN: TStringField;
    qryListCARRIAGE: TStringField;
    qryListDOC_760: TBooleanField;
    qryListDOC_760_1: TStringField;
    qryListDOC_760_2: TStringField;
    qryListDOC_760_3: TStringField;
    qryListDOC_760_4: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListDOC_Name: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListpship_Name: TStringField;
    qryListtship_Name: TStringField;
    qryListdoc705_Name: TStringField;
    qryListdoc740_Name: TStringField;
    qryListdoc760_Name: TStringField;
    qryListCHARGE_Name: TStringField;
    qryListCONFIRM_Name: TStringField;
    qryListCARRIAGE_NAME: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
    
  public
    { Public declarations }
    function openDialog:String;
  end;

var
  Dialog_CopyAPP700_frm: TDialog_CopyAPP700_frm;

implementation

uses KISCalendar,MSSQL,Commonlib,DateUtils,AutoNo ;

{$R *.dfm}

procedure TDialog_CopyAPP700_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CopyAPP700_frm.ReadList;
begin
  inherited;
  with qryList do
  begin
    Close;
    sql.Text := FSQL;
    case com_SearchKeyword.ItemIndex of
      0 : SQL.Add(' WHERE APP_DATE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1 : SQL.Add(' WHERE A700_1.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

      SQL.Add('ORDER BY APP_DATE');

    Open;
  end;
end;

procedure TDialog_CopyAPP700_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_CopyAPP700_frm.sBitBtn1Click(Sender: TObject);
begin
  ReadList;
end;

procedure TDialog_CopyAPP700_frm.FormShow(Sender: TObject);
begin
  sDBGrid1.SetFocus;
end;

procedure TDialog_CopyAPP700_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_CopyAPP700_frm.sBitBtn21Click(Sender: TObject);
begin
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TDialog_CopyAPP700_frm.sBitBtn23Click(Sender: TObject);
begin
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TDialog_CopyAPP700_frm.sDBGrid1DblClick(Sender: TObject);
begin
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;
  
end;

function TDialog_CopyAPP700_frm.openDialog: String;
begin
  Result := '';
//------------------------------------------------------------------------------
//개설일자
//관리번호
//수혜자
//------------------------------------------------------------------------------
  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadList;

  IF ShowModal = mrOk then
    Result :=  qryListMAINT_NO.AsString;
    
end;

procedure TDialog_CopyAPP700_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

end.
