inherited Dialog_CopyVATBI1_frm: TDialog_CopyVATBI1_frm
  Left = 825
  Top = 226
  Caption = #48373#49324#54624' '#50896#47928' '#47928#49436' '#49440#53469
  ClientHeight = 620
  ClientWidth = 920
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 920
    Height = 620
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 918
      Height = 35
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      DesignSize = (
        918
        35)
      object edt_SearchText: TsEdit
        Left = 122
        Top = 5
        Width = 229
        Height = 23
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = False
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object com_SearchKeyword: TsComboBox
        Left = 4
        Top = 5
        Width = 117
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = #46321#47197#51068#51088
        OnSelect = com_SearchKeywordSelect
        Items.Strings = (
          #46321#47197#51068#51088
          #44288#47532#48264#54840
          #44277#44553#48155#45716#51088)
      end
      object Mask_SearchDate1: TsMaskEdit
        Left = 122
        Top = 5
        Width = 80
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn1: TsBitBtn
        Left = 355
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Caption = #44160#49353
        TabOrder = 3
        OnClick = sBitBtn1Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 6
        Images = DMICON.System18
      end
      object sBitBtn21: TsBitBtn
        Tag = 900
        Left = 203
        Top = 5
        Width = 22
        Height = 23
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = sBitBtn21Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object Mask_SearchDate2: TsMaskEdit
        Left = 248
        Top = 5
        Width = 82
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 5
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn23: TsBitBtn
        Tag = 907
        Left = 329
        Top = 5
        Width = 24
        Height = 23
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = sBitBtn23Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object sPanel25: TsPanel
        Left = 224
        Top = 5
        Width = 25
        Height = 23
        Caption = '~'
        TabOrder = 7
        SkinData.SkinSection = 'PANEL'
      end
      object sBitBtn2: TsBitBtn
        Left = 846
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #52712#49548
        ModalResult = 2
        TabOrder = 8
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 18
        Images = DMICON.System18
      end
      object sBitBtn3: TsBitBtn
        Left = 776
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #49440#53469
        ModalResult = 1
        TabOrder = 9
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 17
        Images = DMICON.System18
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 36
      Width = 918
      Height = 583
      Align = alClient
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Caption = #44288#47532#48264#54840
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RFF_NO'
          Title.Caption = #49464#44552#44228#49328#49436#48264#54840
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATEE'
          Title.Caption = #46321#47197#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DRAW_DAT'
          Title.Caption = #51089#49457#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SE_NAME1'
          Title.Caption = #44277#44553#51088#49345#54840
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BY_NAME1'
          Title.Caption = #44277#44553#48155#45716#51088#49345#54840
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TAMT'
          Title.Caption = #52509#44552#50529
          Width = 100
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 128
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      #9', VATBI1_NAME.INDICATOR_NAME as INDICATOR_NAME'
      ''
      'From VATBI1_H '
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as INDICATOR_NAME FROM CODE2NDD with' +
        '(nolock) WHERE Prefix = '#39#50689#49688'/'#52397#44396#39') VATBI1_NAME ON VATBI1_H.INDICAT' +
        'OR = VATBI1_NAME.CODE')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListINDICATOR_NAME: TStringField
      FieldName = 'INDICATOR_NAME'
      Size = 100
    end
    object qryListINDICATOR_NAME_1: TStringField
      FieldName = 'INDICATOR_NAME_1'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
