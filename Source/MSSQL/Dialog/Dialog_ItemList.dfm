inherited Dialog_ItemList_frm: TDialog_ItemList_frm
  Left = 816
  Top = 188
  Caption = #51228#54408#47785#47197
  ClientHeight = 370
  ClientWidth = 608
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Top = 33
    Width = 608
    Height = 337
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 606
      Height = 34
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object com_SearchKeyword: TsComboBox
        Left = 4
        Top = 5
        Width = 81
        Height = 23
        Alignment = taLeftJustify
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 0
        ParentFont = False
        TabOrder = 0
        Text = #53076#46300
        Items.Strings = (
          #53076#46300
          #50557#52845)
      end
      object btn_Search: TsBitBtn
        Left = 266
        Top = 5
        Width = 70
        Height = 23
        Cursor = crHandPoint
        Caption = #44160#49353
        TabOrder = 1
        OnClick = btn_SearchClick
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object edt_Search: TsEdit
        Left = 86
        Top = 5
        Width = 179
        Height = 23
        Color = clWhite
        Font.Charset = HANGEUL_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 35
      Width = 263
      Height = 301
      Align = alLeft
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SNAME'
          Width = 200
          Visible = True
        end>
    end
    object sMemo1: TsMemo
      Left = 264
      Top = 35
      Width = 343
      Height = 301
      Align = alClient
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Lines.Strings = (
        'sMemo1')
      ParentFont = False
      TabOrder = 2
      Text = 'sMemo1'
      SkinData.SkinSection = 'EDIT'
    end
  end
  object sPanel3: TsPanel [1]
    Left = 0
    Top = 0
    Width = 608
    Height = 33
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      608
      33)
    object sBitBtn6: TsBitBtn
      Tag = 1
      Left = 77
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49688#51221
      TabOrder = 0
      OnClick = sBitBtn3Click
      ImageIndex = 3
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn5: TsBitBtn
      Tag = 2
      Left = 148
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49325#51228
      TabOrder = 1
      OnClick = sBitBtn3Click
      ImageIndex = 27
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn3: TsBitBtn
      Left = 6
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 2
      OnClick = sBitBtn3Click
      ImageIndex = 2
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn2: TsBitBtn
      Left = 456
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 3
      ImageIndex = 17
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn1: TsBitBtn
      Left = 527
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 4
      ImageIndex = 18
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 152
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '      ,[HSCODE]'
      '      ,[FNAME]'
      '      ,[SNAME]'
      '      ,[MSIZE]'
      '      ,[QTYC]'
      '      ,[PRICEG]'
      '      ,[PRICE]'
      '      ,[QTY_U]'
      '      ,[QTY_UG]'
      '  FROM [dbo].[ITEM_CODE]')
    Left = 16
    Top = 120
    object qryListCODE: TStringField
      FieldName = 'CODE'
    end
    object qryListHSCODE: TStringField
      FieldName = 'HSCODE'
      Size = 10
    end
    object qryListFNAME: TMemoField
      FieldName = 'FNAME'
      BlobType = ftMemo
    end
    object qryListSNAME: TStringField
      FieldName = 'SNAME'
      Size = 35
    end
    object qryListMSIZE: TMemoField
      FieldName = 'MSIZE'
      BlobType = ftMemo
    end
    object qryListQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryListPRICEG: TStringField
      FieldName = 'PRICEG'
      Size = 3
    end
    object qryListPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryListQTY_U: TBCDField
      FieldName = 'QTY_U'
      Precision = 18
    end
    object qryListQTY_UG: TStringField
      FieldName = 'QTY_UG'
      Size = 3
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 120
  end
end
