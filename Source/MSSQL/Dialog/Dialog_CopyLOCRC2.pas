unit Dialog_CopyLOCRC2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, sSkinProvider,
  ExtCtrls, sPanel;

type
  TDialog_CopyLOCRC2_frm = class(TDialogParent_frm)
    sPanel2: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    qrylist: TADOQuery;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAMAINT_NO: TStringField;
    qrylistBSN_HSCODE: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistAPPLIC7: TStringField;
    dslist: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordChange(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog:String;
  end;

var
  Dialog_CopyLOCRC2_frm: TDialog_CopyLOCRC2_frm;

implementation

{$R *.dfm}

{ TDialog_CopyLOCRC2_frm }

uses MSSQL, ICON, KISCalendar, Commonlib, DateUtils;

function TDialog_CopyLOCRC2_frm.openDialog: String;
begin
    Result := '';
//------------------------------------------------------------------------------
//개설일자
//관리번호
//수혜자
//------------------------------------------------------------------------------
  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadList;

  IF ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;
end;

procedure TDialog_CopyLOCRC2_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE UDNAME1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY DATEE');

    Open;
  end;
end;

procedure TDialog_CopyLOCRC2_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CopyLOCRC2_frm.com_SearchKeywordChange(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_CopyLOCRC2_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_CopyLOCRC2_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDialog_CopyLOCRC2_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_CopyLOCRC2_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;
end;

procedure TDialog_CopyLOCRC2_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_CopyLOCRC2_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TDialog_CopyLOCRC2_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate2);
end;

procedure TDialog_CopyLOCRC2_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
