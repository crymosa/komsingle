unit Dialog_CopyAPPLOG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls, sPanel,
  sSkinProvider, MSSQL;

type
  TDialog_CopyAPPLOG_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    dsList: TDataSource;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCR_NAME1: TStringField;
    qryListCR_NAME2: TStringField;
    qryListCR_NAME3: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListINV_AMT: TBCDField;
    qryListINV_AMTC: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListCARRIER1: TStringField;
    qryListCARRIER2: TStringField;
    qryListAR_DATE: TStringField;
    qryListBL_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListLOAD_LOC: TStringField;
    qryListLOAD_TXT: TStringField;
    qryListARR_LOC: TStringField;
    qryListARR_TXT: TStringField;
    qryListSPMARK1: TStringField;
    qryListSPMARK2: TStringField;
    qryListSPMARK3: TStringField;
    qryListSPMARK4: TStringField;
    qryListSPMARK5: TStringField;
    qryListSPMARK6: TStringField;
    qryListSPMARK7: TStringField;
    qryListSPMARK8: TStringField;
    qryListSPMARK9: TStringField;
    qryListSPMARK10: TStringField;
    qryListPAC_QTY: TBCDField;
    qryListPAC_QTYC: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListGOODS6: TStringField;
    qryListGOODS7: TStringField;
    qryListGOODS8: TStringField;
    qryListGOODS9: TStringField;
    qryListGOODS10: TStringField;
    qryListTRM_PAYC: TStringField;
    qryListTRM_PAY: TStringField;
    qryListBANK_CD: TStringField;
    qryListBANK_TXT: TStringField;
    qryListBANK_BR: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListCN_NAME1: TStringField;
    qryListCN_NAME2: TStringField;
    qryListCN_NAME3: TStringField;
    qryListB5_NAME1: TStringField;
    qryListB5_NAME2: TStringField;
    qryListB5_NAME3: TStringField;
    qryListMSG1NAME: TStringField;
    qryListLCGNAME: TStringField;
    qryListBLGNAME: TStringField;
    qryListLOADNAME: TStringField;
    qryListARRNAME: TStringField;
    qryListTRMPAYCNAME: TStringField;
    qryListCRNAEM3NAME: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog:String;
  end;

var
  Dialog_CopyAPPLOG_frm: TDialog_CopyAPPLOG_frm;

implementation

{$R *.dfm}

uses KISCalendar, Commonlib, DateUtils;

{ TDialog_CopyAPPLOG_frm }

function TDialog_CopyAPPLOG_frm.openDialog: String;
begin
  Result := '';

  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadList;

  IF ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;
end;

procedure TDialog_CopyAPPLOG_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE LC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY DATEE ASC');

    Open;
  end;
end;

procedure TDialog_CopyAPPLOG_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CopyAPPLOG_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_CopyAPPLOG_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_CopyAPPLOG_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_CopyAPPLOG_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
    IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;
end;

procedure TDialog_CopyAPPLOG_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_CopyAPPLOG_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    0 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    1 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end
end;

procedure TDialog_CopyAPPLOG_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
