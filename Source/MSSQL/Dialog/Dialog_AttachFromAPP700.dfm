inherited Dialog_AttachFromAPP700_frm: TDialog_AttachFromAPP700_frm
  Left = 924
  Top = 306
  Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444' '#51025#45813#49436' '#49440#53469
  ClientHeight = 582
  ClientWidth = 853
  Font.Name = #47569#51008' '#44256#46357
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 853
    Height = 35
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      853
      35)
    object edt_SearchText: TsEdit
      Left = 78
      Top = 5
      Width = 225
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 73
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #53685#51648#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #53685#51648#51068#51088
        #44288#47532#48264#54840
        #49688#54812#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 78
      Top = 5
      Width = 78
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 304
      Top = 5
      Width = 66
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 155
      Top = 5
      Width = 20
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = btnCalendar
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 198
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 901
      Left = 279
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = btnCalendar
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 174
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 781
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 711
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      OnClick = sDBGrid1DblClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 35
    Width = 853
    Height = 547
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USER_ID'
        Title.Caption = #49324#50857#51088
        Width = 109
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATEE'
        Title.Caption = #46321#47197'('#49688#49888')'#51068#51088
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = #47928#49436#51221#48372
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = #48708#44256
        Width = 170
        Visible = True
      end>
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      
        'SELECT I700_1.MAINT_NO,I700_1.MESSAGE1,I700_1.MESSAGE2,I700_1.[U' +
        'SER_ID],I700_1.DATEE,I700_1.APP_DATE,I700_1.IN_MATHOD,I700_1.AP_' +
        'BANK,I700_1.AP_BANK1,I700_1.AP_BANK2,I700_1.AP_BANK3,I700_1.AP_B' +
        'ANK4,I700_1.AP_BANK5,I700_1.AD_BANK,I700_1.AD_BANK1'
      
        '      ,I700_1.AD_BANK2,I700_1.AD_BANK3,I700_1.AD_BANK4,I700_1.AD' +
        '_PAY,I700_1.IL_NO1,I700_1.IL_NO2,I700_1.IL_NO3,I700_1.IL_NO4,I70' +
        '0_1.IL_NO5,I700_1.IL_AMT1,I700_1.IL_AMT2,I700_1.IL_AMT3,I700_1.I' +
        'L_AMT4,I700_1.IL_AMT5,I700_1.IL_CUR1,I700_1.IL_CUR2,I700_1.IL_CU' +
        'R3'
      
        '      ,I700_1.IL_CUR4,I700_1.IL_CUR5,I700_1.AD_INFO1,I700_1.AD_I' +
        'NFO2,I700_1.AD_INFO3,I700_1.AD_INFO4,I700_1.AD_INFO5,I700_1.DOC_' +
        'CD,I700_1.CD_NO,I700_1.REF_PRE,I700_1.ISS_DATE,I700_1.EX_DATE,I7' +
        '00_1.EX_PLACE,I700_1.CHK1,I700_1.CHK2,I700_1.CHK3,I700_1.prno'
      
        '      ,I700_1.F_INTERFACE,I700_1.IMP_CD1,I700_1.IMP_CD2,I700_1.I' +
        'MP_CD3,I700_1.IMP_CD4,I700_1.IMP_CD5,'
      ''
      
        '       I700_2.MAINT_NO,I700_2.APP_BANK,I700_2.APP_BANK1,I700_2.A' +
        'PP_BANK2,I700_2.APP_BANK3,I700_2.APP_BANK4,I700_2.APP_BANK5,I700' +
        '_2.APP_ACCNT,I700_2.APPLIC1,I700_2.APPLIC2,I700_2.APPLIC3,I700_2' +
        '.APPLIC4,I700_2.APPLIC5,I700_2.BENEFC1'
      
        '      ,I700_2.BENEFC2,I700_2.BENEFC3,I700_2.BENEFC4,I700_2.BENEF' +
        'C5,I700_2.CD_AMT,I700_2.CD_CUR,I700_2.CD_PERP,I700_2.CD_PERM,I70' +
        '0_2.CD_MAX,I700_2.AA_CV1,I700_2.AA_CV2,I700_2.AA_CV3,I700_2.AA_C' +
        'V4,I700_2.AVAIL,I700_2.AVAIL1,I700_2.AVAIL2,I700_2.AVAIL3'
      
        '      ,I700_2.AVAIL4,I700_2.AV_ACCNT,I700_2.AV_PAY,I700_2.DRAFT1' +
        ',I700_2.DRAFT2,I700_2.DRAFT3,I700_2.DRAWEE,I700_2.DRAWEE1,I700_2' +
        '.DRAWEE2,I700_2.DRAWEE3,I700_2.DRAWEE4,I700_2.DR_ACCNT,'
      ''
      
        '       I700_3.MAINT_NO,I700_3.PSHIP,I700_3.TSHIP,I700_3.LOAD_ON,' +
        'I700_3.FOR_TRAN,I700_3.LST_DATE,I700_3.SHIP_PD,I700_3.SHIP_PD1,I' +
        '700_3.SHIP_PD2,I700_3.SHIP_PD3,I700_3.SHIP_PD4,I700_3.SHIP_PD5,I' +
        '700_3.SHIP_PD6,I700_3.DESGOOD,I700_3.DESGOOD_1,I700_3.TERM_PR'
      
        '      ,I700_3.TERM_PR_M,I700_3.PL_TERM,I700_3.ORIGIN,I700_3.ORIG' +
        'IN_M,I700_3.DOC_380,I700_3.DOC_380_1,I700_3.DOC_705,I700_3.DOC_7' +
        '05_1,I700_3.DOC_705_2,I700_3.DOC_705_3,I700_3.DOC_705_4,I700_3.D' +
        'OC_740,I700_3.DOC_740_1,I700_3.DOC_740_2,I700_3.DOC_740_3'
      
        '      ,I700_3.DOC_740_4,I700_3.DOC_530,I700_3.DOC_530_1,I700_3.D' +
        'OC_530_2,I700_3.DOC_271,I700_3.DOC_271_1,I700_3.DOC_861,I700_3.D' +
        'OC_2AA,I700_3.DOC_2AA_1,I700_3.ACD_2AA,I700_3.ACD_2AA_1,I700_3.A' +
        'CD_2AB,I700_3.ACD_2AC,I700_3.ACD_2AD,I700_3.ACD_2AE'
      
        '      ,I700_3.ACD_2AE_1,I700_3.CHARGE,I700_3.PERIOD,I700_3.CONFI' +
        'RMM,I700_3.DEF_PAY1,I700_3.DEF_PAY2,I700_3.DEF_PAY3,I700_3.DEF_P' +
        'AY4,I700_3.DOC_705_GUBUN,'
      ''
      
        '       I700_4.MAINT_NO,I700_4.REI_BANK,I700_4.REI_BANK1,I700_4.R' +
        'EI_BANK2,I700_4.REI_BANK3,I700_4.REI_BANK4,I700_4.REI_BANK5,I700' +
        '_4.REI_ACNNT,I700_4.INSTRCT,I700_4.INSTRCT_1,I700_4.AVT_BANK,I70' +
        '0_4.AVT_BANK1,I700_4.AVT_BANK2,I700_4.AVT_BANK3'
      
        '      ,I700_4.AVT_BANK4,I700_4.AVT_BANK5,I700_4.AVT_ACCNT,I700_4' +
        '.SND_INFO1,I700_4.SND_INFO2,I700_4.SND_INFO3,I700_4.SND_INFO4,I7' +
        '00_4.SND_INFO5,I700_4.SND_INFO6,I700_4.EX_NAME1,I700_4.EX_NAME2,' +
        'I700_4.EX_NAME3,I700_4.EX_ADDR1,I700_4.EX_ADDR2'
      
        '      ,I700_4.EX_ADDR3,I700_4.OP_BANK1,I700_4.OP_BANK2,I700_4.OP' +
        '_BANK3,I700_4.OP_ADDR1,I700_4.OP_ADDR2,I700_4.OP_ADDR3,I700_4.MI' +
        'X_PAY1,I700_4.MIX_PAY2,I700_4.MIX_PAY3,I700_4.MIX_PAY4'
      
        '      ,I700_4.APPLICABLE_RULES_1,I700_4.APPLICABLE_RULES_2,I700_' +
        '4.DOC_760,I700_4.DOC_760_1,I700_4.DOC_760_2,I700_4.DOC_760_3,I70' +
        '0_4.DOC_760_4,I700_4.SUNJUCK_PORT,I700_4.DOCHACK_PORT'
      ''
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      ''
      ''
      'FROM [dbo].[INF700_1] AS I700_1'
      
        'INNER JOIN [dbo].[INF700_2] AS I700_2 ON I700_1.MAINT_NO = I700_' +
        '2.MAINT_NO'
      
        'INNER JOIN [dbo].[INF700_3] AS I700_3 ON I700_1.MAINT_NO = I700_' +
        '3.MAINT_NO'
      
        'INNER JOIN [dbo].[INF700_4] AS I700_4 ON I700_1.MAINT_NO = I700_' +
        '4.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON I700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON I700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON I700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON I700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON I700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON I700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON I700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON I700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON I700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON I700_3.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON I700_3.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON I700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON I700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON I700_4.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON I700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON I700_3.CONFIRMM = CON' +
        'FIRM700.CODE')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListREF_PRE: TStringField
      FieldName = 'REF_PRE'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAPP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryListAPP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryListAPP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryListAPP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryListAPP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryListAPP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryListAPP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListAVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryListAVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryListAVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryListAVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryListAVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryListAV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryListAV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListDRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryListDRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryListDRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryListDRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryListDRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryListDR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD: TBooleanField
      FieldName = 'DESGOOD'
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListMAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryListREI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryListREI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryListREI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryListREI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryListREI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryListREI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryListREI_ACNNT: TStringField
      FieldName = 'REI_ACNNT'
      Size = 35
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListAVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryListAVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryListAVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryListAVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryListAVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryListAVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryListAVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryListSND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryListSND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryListSND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryListSND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryListSND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryListSND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListEX_ADDR3: TStringField
      FieldName = 'EX_ADDR3'
      Size = 35
    end
    object qryListOP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryListOP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryListOP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryListOP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryListOP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryListOP_ADDR3: TStringField
      FieldName = 'OP_ADDR3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryListAPPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListpship_Name: TStringField
      FieldName = 'pship_Name'
      Size = 100
    end
    object qryListtship_Name: TStringField
      FieldName = 'tship_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCONFIRM_Name: TStringField
      FieldName = 'CONFIRM_Name'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
