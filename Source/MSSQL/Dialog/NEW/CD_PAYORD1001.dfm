inherited CD_PAYORD1001_frm: TCD_PAYORD1001_frm
  Caption = 'CD_PAYORD1001_frm'
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sDBGrid1: TsDBGrid
      Color = clWhite
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #51077#44552#44288#47144#49436#47448
          Width = 374
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39'PAYORD1001'#39)
  end
end
