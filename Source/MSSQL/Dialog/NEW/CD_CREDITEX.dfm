inherited CD_CREDITEX_frm: TCD_CREDITEX_frm
  Caption = 'CD_CREDITEX_frm'
  ClientHeight = 308
  ClientWidth = 379
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 379
    inherited sImage1: TsImage
      Left = 356
    end
  end
  inherited sPanel3: TsPanel
    Width = 379
    Height = 283
    inherited sPanel2: TsPanel
      Width = 369
      inherited sImage2: TsImage
        Left = 345
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 369
      Height = 239
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 56
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #49888#50857#44277#50668
          Width = 288
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39#49888#50857#44277#50668#39' AND Remark' +
        ' = 1')
  end
end
