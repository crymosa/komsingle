unit CD_C_METHOD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_C_METHOD_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_C_METHOD_frm: TCD_C_METHOD_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

procedure TCD_C_METHOD_frm.ReadList;
begin
//  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE,NAME FROM CODE2NDD with(nolock) WHERE prefix = ''C_METHOD''';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    SQL.Add('ORDER BY CODE');
    Open;
  end;
end;

procedure TCD_C_METHOD_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '��ۼ���';
end;

function TCD_C_METHOD_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT NAME FROM CODE2NDD with(nolock) WHERE prefix = ''C_METHOD'' AND CODE = '+QuotedStr(value);
    Open;

    IF RecordCount > 0 Then
      Result := FieldByName('NAME').AsString;
  end;
end;

end.
