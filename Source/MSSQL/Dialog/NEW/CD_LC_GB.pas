unit CD_LC_GB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_LC_GB_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_LC_GB_frm: TCD_LC_GB_frm;

implementation

{$R *.dfm}

procedure TCD_LC_GB_frm.FormCreate(Sender: TObject);
begin
  FCaption := '신용장/계약서';
  inherited;
end;

function TCD_LC_GB_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''LC구분''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    Open;
    IF RecordCount > 0 Then
    begin
      Result := FieldByName('NAME').AsString;
    end;
  end;
end;

procedure TCD_LC_GB_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''LC구분'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
