unit CodeDialogParent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, Buttons, sSpeedButton, StdCtrls, sButton,
  sComboBox, sEdit, Grids, DBGrids, acDBGrid, acImage, sLabel, DB, ADODB;

type
  TCodeDialogParent_frm = class(TForm)
    sPanel1: TsPanel;
    sImage1: TsImage;
    sPanel3: TsPanel;
    sPanel2: TsPanel;
    sSpeedButton1: TsSpeedButton;
    sEdit1: TsEdit;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    sImage2: TsImage;
    sLabel1: TsLabel;
    qryList: TADOQuery;
    dsList: TDataSource;
    procedure sImage1Click(Sender: TObject);
    procedure sPanel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    function getValues(index: integer): Variant;
    { Private declarations }
  protected
    FCaption : String;
    FPrefix : String;
    procedure ReadList; virtual; abstract;
  public
    { Public declarations }
    function OpenDialog(KeyIndex : Integer;KeyValue : String=''):Boolean; virtual;
    property Values[index : integer] : Variant read getValues;
    function FieldValues(FieldName : String): Variant;
    function Field(FieldName : String):TField;
    function SearchData(KeyValues : string):Integer; virtual; abstract;
    function GetCodeText(value : String):String; virtual; abstract;
  end;

var
  CodeDialogParent_frm: TCodeDialogParent_frm;

implementation

uses
  ICON, MSSQL;

{$R *.dfm}

procedure TCodeDialogParent_frm.sImage1Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TCodeDialogParent_frm.sPanel1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  Self.Perform(WM_SYSCOMMAND, $f012, 0);
end;

procedure TCodeDialogParent_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;


procedure TCodeDialogParent_frm.sDBGrid1DblClick(Sender: TObject);
begin
  IF (Sender as TsDBGrid).ScreenToClient(Mouse.CursorPos).Y > 17 then
    ModalResult := mrOk;
end;

function TCodeDialogParent_frm.getValues(index: integer): Variant;
begin
  Result := qryList.Fields[index].AsVariant;
end;

function TCodeDialogParent_frm.FieldValues(FieldName: String): Variant;
begin
  IF not qryList.active Then qryList.open;
  Result := qryList.FieldByName(FieldName).AsVariant;
end;

function TCodeDialogParent_frm.OpenDialog(KeyIndex: Integer;
  KeyValue: String): Boolean;
begin
  sLabel1.Caption := FCaption;
  sComboBox1.ItemIndex := KeyIndex;

  IF Trim(KeyValue) <> '' Then
  begin
    sEdit1.Clear;
    ReadList;
    qryList.Locate(sDBGrid1.Columns[KeyIndex].FieldName, KeyValue,[]);
  end
  else
    ReadList;

  Result := Self.ShowModal = mrOK;
end;

procedure TCodeDialogParent_frm.FormCreate(Sender: TObject);
begin
  FCaption := '코드타이틀'
end;

procedure TCodeDialogParent_frm.sButton1Click(Sender: TObject);
begin
  ReadList;
end;

procedure TCodeDialogParent_frm.FormDestroy(Sender: TObject);
begin
  IF qryList.Active Then qryList.Close;
end;

procedure TCodeDialogParent_frm.sEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF KEY = VK_RETURN THEN ReadList;
end;

function TCodeDialogParent_frm.Field(FieldName: String): TField;
begin
  Result := qryList.FieldByName(FieldName);
end;

procedure TCodeDialogParent_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = VK_ESCAPE Then Self.Close;
end;

procedure TCodeDialogParent_frm.FormShow(Sender: TObject);
var
  FORM_POS : TPoint;
begin
  inherited;
  IF Self.Position = poDesigned Then
  begin
    FORM_POS.X := TForm(Owner).Left;
    FORM_POS.Y := TForm(Owner).Top;
    FORM_POS := Application.MainForm.ClientToScreen(FORM_POS);
    Self.Left := FORM_POS.X+(TForm(Owner).Width div 2)-(Self.Width div 2);
    Self.Top  := FORM_POS.Y+(TForm(Owner).Height div 2)-(Self.Height div 2);
  end;
end;

end.
