unit CD_UNIT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_UNIT_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CD_UNIT_frm: TCD_UNIT_frm;

implementation

{$R *.dfm}

procedure TCD_UNIT_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '단위선택';
end;

procedure TCD_UNIT_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''단위'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
