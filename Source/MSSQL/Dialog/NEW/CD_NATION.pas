unit CD_NATION;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_NATION_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }

  end;

var
  CD_NATION_frm: TCD_NATION_frm;

implementation

{$R *.dfm}

procedure TCD_NATION_frm.FormCreate(Sender: TObject);
begin
  FCaption := '국가 선택';
  inherited;
end;

function TCD_NATION_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''국가''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    Open;
    IF RecordCount > 0 Then
      Result := FieldByName('NAME').AsString;
  end;
end;

procedure TCD_NATION_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''국가'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
