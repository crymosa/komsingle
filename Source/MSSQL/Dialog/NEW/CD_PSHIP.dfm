inherited CD_PSHIP_frm: TCD_PSHIP_frm
  Left = 1061
  Top = 187
  Caption = 'CD_PSHIP_frm'
  ClientHeight = 228
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 203
    inherited sDBGrid1: TsDBGrid
      Height = 159
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 73
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #48516#54624#54728#50857#44396#48516
          Width = 390
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'PSHIP'#39)
  end
end
