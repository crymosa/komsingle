unit CD_BANK_SWIFT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_BANK_SWIFT_frm = class(TCodeDialogParent_frm)
    qryListBANK_CD: TStringField;
    qryListBANK_NM1: TStringField;
    qryListBANK_NM2: TStringField;
    qryListBANK_BNCH_NM1: TStringField;
    qryListBANK_BNCH_NM2: TStringField;
    qryListACCT_NM: TStringField;
    qryListBANK_NAT_CD: TStringField;
    qryListIS_USE: TBooleanField;
    qryListBANK_UNIT: TStringField;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
  public
    function SearchData(keys: array of String): boolean; overload;
    { Public declarations }
  end;

var
  CD_BANK_SWIFT_frm: TCD_BANK_SWIFT_frm;

implementation

{$R *.dfm}

{ TCD_BANK_SWIFT_frm }

procedure TCD_BANK_SWIFT_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT BANK_CD, BANK_NM1, BANK_NM2, BANK_BNCH_NM1,'#13+
                'BANK_BNCH_NM2, ACCT_NM, BANK_NAT_CD, BANK_UNIT, IS_USE'#13+
                'FROM BANKCODE_SWIFT'#13;
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('WHERE '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_BANK_SWIFT_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '해외은행 선택';
end;

function TCD_BANK_SWIFT_frm.SearchData(keys: array of String): boolean;
begin
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT 1 FROM BANKCODE_SWIFT WHERE BANK_CD = '+QuotedStr(keys[0]);
    try
      Open;
      Result := RecordCount > 0;      
    finally
      Close;
    end;
  end;
end;


end.
