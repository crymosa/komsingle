unit CD_NK4025;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_NK4025_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_NK4025_frm: TCD_NK4025_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

function TCD_NK4025_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''내국신1001''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    Open;
    IF RecordCount > 0 Then
    begin
      Result := FieldByName('NAME').AsString;
    end;
  end;
end;

procedure TCD_NK4025_frm.FormCreate(Sender: TObject);
begin
  FCaption := '공급물품 명세구분';
//  inherited;
end;

procedure TCD_NK4025_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''내국신4025'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
