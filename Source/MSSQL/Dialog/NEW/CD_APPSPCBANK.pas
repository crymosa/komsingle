unit CD_APPSPCBANK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_APPSPCBANK_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CD_APPSPCBANK_frm: TCD_APPSPCBANK_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

procedure TCD_APPSPCBANK_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE prefix = ''APPSPC은행''';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_APPSPCBANK_frm.FormCreate(Sender: TObject);
begin
  FCaption := '문서번호에 쓸 은행 선택 - 내국신용장(LOCAPP)';
  inherited;
end;

end.
