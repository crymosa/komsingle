inherited CD_APPSPCBANK_frm: TCD_APPSPCBANK_frm
  Caption = 'CD_APPSPCBANK_frm'
  ClientHeight = 533
  ClientWidth = 357
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 357
    inherited sImage1: TsImage
      Left = 334
    end
  end
  inherited sPanel3: TsPanel
    Width = 357
    Height = 508
    inherited sPanel2: TsPanel
      Width = 347
      inherited sImage2: TsImage
        Left = 323
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 347
      Height = 464
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 58
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #51008#54665#47749
          Width = 265
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD WHERE prefix = '#39'APPSPC'#51008#54665#39)
  end
end
