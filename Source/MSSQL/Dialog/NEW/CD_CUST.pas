unit CD_CUST;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_CUST_frm = class(TCodeDialogParent_frm)
    procedure sButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function SearchData(KeyValues: String): Integer; override;
    { Public declarations }
  end;

var
  CD_CUST_frm: TCD_CUST_frm;

implementation

{$R *.dfm}

{ TCD_CUST_frm }

procedure TCD_CUST_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT * FROM CUSTOM';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('WHERE '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_CUST_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TCD_CUST_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '거래처선택'
end;


function TCD_CUST_frm.SearchData(KeyValues: String): Integer;
begin
  Result := -1;
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT * FROM CUSTOM WHERE CODE = '+QuotedStr(KeyValues);
    try
      Open;
      Result := RecordCount;      
    finally
      Close;
    end;
  end;
end;

end.
