inherited dlg_CopyAPPSPCfromVATBI2_frm: Tdlg_CopyAPPSPCfromVATBI2_frm
  Left = 444
  Top = 344
  Caption = #49464#44552#44228#49328#49436' '#49440#53469
  ClientWidth = 987
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 987
    inherited sPanel2: TsPanel
      Width = 985
      inherited btnExit: TsButton
        Left = 914
      end
      inherited sButton1: TsButton
        Left = 841
      end
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #49464#44552#44228#49328#49436#48264#54840)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 985
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #51089#49457#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RE_NO'
          Title.Alignment = taCenter
          Title.Caption = #44428#48264#54840
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SE_NO'
          Title.Alignment = taCenter
          Title.Caption = #54840#48264#54840
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FS_NO'
          Title.Alignment = taCenter
          Title.Caption = #51068#47144#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SE_NAME1'
          Title.Alignment = taCenter
          Title.Caption = #44277#44553#51088#49345#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BY_NAME1'
          Title.Alignment = taCenter
          Title.Caption = #44277#44553#48155#45716#51088#49345#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SUP_AMT'
          Title.Alignment = taCenter
          Title.Caption = #44277#44553#44032#50529
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RFF_NO'
          Title.Alignment = taCenter
          Title.Caption = #49464#44552#44228#49328#49436#48264#54840
          Width = 140
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'DATEE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATEE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, '
      'AG_FTX4, AG_FTX5, SE_NAME3, BY_NAME3'
      'FROM VATBI2_H'
      'WHERE DATEE BETWEEN :DATEE1 AND :DATEE2')
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'DATEE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATEE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, '
      'AG_FTX4, AG_FTX5, SE_NAME3, BY_NAME3'
      'FROM VATBI2_H'
      'WHERE DATEE BETWEEN :DATEE1 AND :DATEE2')
  end
end
