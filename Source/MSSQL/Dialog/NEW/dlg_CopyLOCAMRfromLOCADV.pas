unit dlg_CopyLOCAMRfromLOCADV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_SelectCopyDocument, DB, ADODB, Grids, DBGrids, acDBGrid,
  Mask, sMaskEdit, StdCtrls, sEdit, sComboBox, sButton, Buttons,
  sSpeedButton, ExtCtrls, sPanel;

type
  Tdlg_CopyLOCAMRfromLOCADV_frm = class(Tdlg_SelectCopyDocument_frm)
    qryListMAINT_NO: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_REF: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CopyLOCAMRfromLOCADV_frm: Tdlg_CopyLOCAMRfromLOCADV_frm;

implementation

{$R *.dfm}

{ Tdlg_CopyLOCAMRfromLOCADV_frm }

procedure Tdlg_CopyLOCAMRfromLOCADV_frm.ReadList;
var
  TempField : String;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := qryCopy.SQL.Text;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;

    IF (edt_find.Text <> '') Then
    begin
      TempField := sDBGrid1.Columns[com_find.ItemIndex].FieldName;
      IF TempField = 'MAINT_NO' Then TempField := 'LOCADV.MAINT_NO';

      SQL.Add('AND '+TempField+' LIKE '+QuotedStr('%'+edt_find.Text+'%'));
    end;

    SQL.Add('ORDER BY ADV_DATE DESC');

    Open;
  end;
end;

end.
