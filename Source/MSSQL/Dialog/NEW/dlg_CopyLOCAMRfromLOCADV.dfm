inherited dlg_CopyLOCAMRfromLOCADV_frm: Tdlg_CopyLOCAMRfromLOCADV_frm
  Caption = #45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436' '#49440#53469
  ClientWidth = 979
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 979
    inherited sPanel2: TsPanel
      Width = 977
      inherited btnExit: TsButton
        Left = 909
        Top = 9
        Width = 67
        Anchors = [akTop, akRight]
      end
      inherited sButton1: TsButton
        Left = 841
        Top = 9
        Width = 67
        Anchors = [akTop, akRight]
      end
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #53685#51648#51068#51088
          'L/C'#48264#54840
          #44060#49444#51068#51088)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 977
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 213
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'ADV_DATE'
          Title.Alignment = taCenter
          Title.Caption = #53685#51648#51068#51088
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LC_NO'
          Title.Alignment = taCenter
          Title.Caption = 'L/C'#48264#54840
          Width = 130
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'ISS_DATE'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Alignment = taCenter
          Title.Caption = #49688#54812#51088
          Width = 176
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK1'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51008#54665
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMT'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
          Width = -1
          Visible = False
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOC1AMTC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = -1
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'LOC2AMT'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
          Width = 100
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOC2AMTC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 38
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      '      ,N1001.DOC_NAME as DOC_DTLNAME '
      '      ,N4277.DOC_NAME as PAYMENTNAME'
      '      ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCADV.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'ADV.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCADV.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCADV.DOC_DTL = N1001.CODE'#9
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCADV.DEST = NAT.CODE'
      'WHERE (ADV_DATE BETWEEN :FDATE AND :TDATE)')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      Alignment = taCenter
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      DisplayFormat = '#,0.###'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      '      ,N1001.DOC_NAME as DOC_DTLNAME '
      '      ,N4277.DOC_NAME as PAYMENTNAME'
      '      ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCADV.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'ADV.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCADV.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCADV.DOC_DTL = N1001.CODE'#9
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCADV.DEST = NAT.CODE'
      'WHERE (ADV_DATE BETWEEN :FDATE AND :TDATE)')
  end
end
