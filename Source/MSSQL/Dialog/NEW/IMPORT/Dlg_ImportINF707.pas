unit Dlg_ImportINF707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OKBottomDlg, DB, ADODB, StdCtrls, sComboBox, sRadioButton,
  sButton, sCheckBox, sEdit, sLabel, ExtCtrls, DBTables, ComCtrls,
  acProgressBar, Clipbrd;

type
  TDlg_ImportINF707_frm = class(TOKBottomDlg_frm)
    Query3: TQuery;
    Query3MAINT_NO: TStringField;
    Query3MSEQ: TIntegerField;
    Query3AMD_NO: TIntegerField;
    Query3MESSAGE1: TStringField;
    Query3MESSAGE2: TStringField;
    Query3USER_ID: TStringField;
    Query3DATEE: TStringField;
    Query3APP_DATE: TStringField;
    Query3IN_MATHOD: TStringField;
    Query3AP_BANK: TStringField;
    Query3AP_BANK1: TStringField;
    Query3AP_BANK2: TStringField;
    Query3AP_BANK3: TStringField;
    Query3AP_BANK4: TStringField;
    Query3AP_BANK5: TStringField;
    Query3AD_BANK: TStringField;
    Query3AD_BANK1: TStringField;
    Query3AD_BANK2: TStringField;
    Query3AD_BANK3: TStringField;
    Query3AD_BANK4: TStringField;
    Query3IL_NO1: TStringField;
    Query3IL_NO2: TStringField;
    Query3IL_NO3: TStringField;
    Query3IL_NO4: TStringField;
    Query3IL_NO5: TStringField;
    Query3IL_AMT1: TFloatField;
    Query3IL_AMT2: TFloatField;
    Query3IL_AMT3: TFloatField;
    Query3IL_AMT4: TFloatField;
    Query3IL_AMT5: TFloatField;
    Query3IL_CUR1: TStringField;
    Query3IL_CUR2: TStringField;
    Query3IL_CUR3: TStringField;
    Query3IL_CUR4: TStringField;
    Query3IL_CUR5: TStringField;
    Query3AD_INFO1: TStringField;
    Query3AD_INFO2: TStringField;
    Query3AD_INFO3: TStringField;
    Query3AD_INFO4: TStringField;
    Query3AD_INFO5: TStringField;
    Query3CD_NO: TStringField;
    Query3RCV_REF: TStringField;
    Query3IBANK_REF: TStringField;
    Query3ISS_BANK1: TStringField;
    Query3ISS_BANK2: TStringField;
    Query3ISS_BANK3: TStringField;
    Query3ISS_BANK4: TStringField;
    Query3ISS_BANK5: TStringField;
    Query3ISS_ACCNT: TStringField;
    Query3ISS_DATE: TStringField;
    Query3AMD_DATE: TStringField;
    Query3EX_DATE: TStringField;
    Query3EX_PLACE: TStringField;
    Query3CHK1: TStringField;
    Query3CHK2: TStringField;
    Query3CHK3: TStringField;
    Query3IMP_CD1: TStringField;
    Query3IMP_CD2: TStringField;
    Query3IMP_CD3: TStringField;
    Query3IMP_CD4: TStringField;
    Query3IMP_CD5: TStringField;
    Query3F_INTERFACE: TStringField;
    Query3prno: TIntegerField;
    Query3PURP_MSG: TStringField;
    Query3CAN_REQ: TStringField;
    Query3CON_INST: TStringField;
    Query3DOC_TYPE: TStringField;
    Query3CHARGE: TStringField;
    Query3CHARGE_NUM1: TStringField;
    Query3CHARGE_NUM2: TStringField;
    Query3CHARGE_NUM3: TStringField;
    Query3CHARGE_NUM4: TStringField;
    Query3CHARGE_NUM5: TStringField;
    Query3CHARGE_NUM6: TStringField;
    Query3AMD_CHARGE: TStringField;
    Query3AMD_CHARGE_NUM1: TStringField;
    Query3AMD_CHARGE_NUM2: TStringField;
    Query3AMD_CHARGE_NUM3: TStringField;
    Query3AMD_CHARGE_NUM4: TStringField;
    Query3AMD_CHARGE_NUM5: TStringField;
    Query3AMD_CHARGE_NUM6: TStringField;
    Query3TSHIP: TStringField;
    Query3PSHIP: TStringField;
    Query3DRAFT1: TStringField;
    Query3DRAFT2: TStringField;
    Query3DRAFT3: TStringField;
    Query3MIX_PAY1: TStringField;
    Query3MIX_PAY2: TStringField;
    Query3MIX_PAY3: TStringField;
    Query3MIX_PAY4: TStringField;
    Query3DEF_PAY1: TStringField;
    Query3DEF_PAY2: TStringField;
    Query3DEF_PAY3: TStringField;
    Query3DEF_PAY4: TStringField;
    Query3APPLICABLE_RULES_1: TStringField;
    Query3APPLICABLE_RULES_2: TStringField;
    Query3AVAIL: TStringField;
    Query3AVAIL1: TStringField;
    Query3AVAIL2: TStringField;
    Query3AVAIL3: TStringField;
    Query3AVAIL4: TStringField;
    Query3AV_ACCNT: TStringField;
    Query3DRAWEE: TStringField;
    Query3DRAWEE1: TStringField;
    Query3DRAWEE2: TStringField;
    Query3DRAWEE3: TStringField;
    Query3DRAWEE4: TStringField;
    Query3CO_BANK: TStringField;
    Query3CO_BANK1: TStringField;
    Query3CO_BANK2: TStringField;
    Query3CO_BANK3: TStringField;
    Query3CO_BANK4: TStringField;
    Query3REI_BANK: TStringField;
    Query3REI_BANK1: TStringField;
    Query3REI_BANK2: TStringField;
    Query3REI_BANK3: TStringField;
    Query3REI_BANK4: TStringField;
    Query3AVT_BANK: TStringField;
    Query3AVT_BANK1: TStringField;
    Query3AVT_BANK2: TStringField;
    Query3AVT_BANK3: TStringField;
    Query3AVT_BANK4: TStringField;
    Query3AMD_APPLIC1: TStringField;
    Query3AMD_APPLIC2: TStringField;
    Query3AMD_APPLIC3: TStringField;
    Query3AMD_APPLIC4: TStringField;
    Query3PERIOD: TFloatField;
    Query3PERIOD_TXT: TStringField;
    Query3MAINT_NO_1: TStringField;
    Query3MSEQ_1: TIntegerField;
    Query3AMD_NO_1: TIntegerField;
    Query3APPLIC1: TStringField;
    Query3APPLIC2: TStringField;
    Query3APPLIC3: TStringField;
    Query3APPLIC4: TStringField;
    Query3APPLIC5: TStringField;
    Query3BENEFC1: TStringField;
    Query3BENEFC2: TStringField;
    Query3BENEFC3: TStringField;
    Query3BENEFC4: TStringField;
    Query3BENEFC5: TStringField;
    Query3INCD_CUR: TStringField;
    Query3INCD_AMT: TFloatField;
    Query3DECD_CUR: TStringField;
    Query3DECD_AMT: TFloatField;
    Query3NWCD_CUR: TStringField;
    Query3NWCD_AMT: TFloatField;
    Query3CD_PERP: TFloatField;
    Query3CD_PERM: TFloatField;
    Query3CD_MAX: TStringField;
    Query3AA_CV1: TStringField;
    Query3AA_CV2: TStringField;
    Query3AA_CV3: TStringField;
    Query3AA_CV4: TStringField;
    Query3LOAD_ON: TStringField;
    Query3FOR_TRAN: TStringField;
    Query3LST_DATE: TStringField;
    Query3SHIP_PD: TBooleanField;
    Query3SHIP_PD1: TStringField;
    Query3SHIP_PD2: TStringField;
    Query3SHIP_PD3: TStringField;
    Query3SHIP_PD4: TStringField;
    Query3SHIP_PD5: TStringField;
    Query3SHIP_PD6: TStringField;
    Query3NARRAT: TBooleanField;
    Query3NARRAT_1: TMemoField;
    Query3SR_INFO1: TStringField;
    Query3SR_INFO2: TStringField;
    Query3SR_INFO3: TStringField;
    Query3SR_INFO4: TStringField;
    Query3SR_INFO5: TStringField;
    Query3SR_INFO6: TStringField;
    Query3EX_NAME1: TStringField;
    Query3EX_NAME2: TStringField;
    Query3EX_NAME3: TStringField;
    Query3EX_ADDR1: TStringField;
    Query3EX_ADDR2: TStringField;
    Query3OP_BANK1: TStringField;
    Query3OP_BANK2: TStringField;
    Query3OP_BANK3: TStringField;
    Query3OP_ADDR1: TStringField;
    Query3OP_ADDR2: TStringField;
    Query3BFCD_AMT: TFloatField;
    Query3BFCD_CUR: TStringField;
    Query3SUNJUCK_PORT: TStringField;
    Query3DOCHACK_PORT: TStringField;
    Query3GOODS_DESC: TStringField;
    Query3GOODS_DESC_1: TMemoField;
    Query3DOC_DESC: TStringField;
    Query3DOC_DESC_1: TMemoField;
    Query3ADD_DESC: TStringField;
    Query3ADD_DESC_1: TMemoField;
    Query3SPECIAL_DESC: TStringField;
    Query3SPECIAL_DESC_1: TMemoField;
    Query3INST_DESC: TStringField;
    Query3INST_DESC_1: TMemoField;
    sProgressBar1: TsProgressBar;
    procedure sButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function ConvertData: Boolean; override;
    { Public declarations }
  end;

var
  Dlg_ImportINF707_frm: TDlg_ImportINF707_frm;

implementation

uses
  SQLCreator, VarDefine;

{$R *.dfm}

{ TDlg_ImportINF707_frm }

function TDlg_ImportINF707_frm.ConvertData: Boolean;
var
  SC : array [0..1] of TSQLCreate;
  nCount, nMax : Integer;
  TMP_STR : string;
begin
  try
    Query3.Close;
    Query3.SQL.Text := FSQL;
    Query3.Open;

    if Query3.RecordCount = 0 Then
      raise Exception.Create('데이터가 없습니다');

    SC[0] := TSQLCreate.Create;
    SC[1] := TSQLCreate.Create;

    //------------------------------------------------------------------------------
    // 초기화 섹션
    //------------------------------------------------------------------------------
    with SC[0] do begin DMLType := dmlDelete; SQLHeader('INF707_1'); end;
    with SC[1] do begin DMLType := dmlDelete; SQLHeader('INF707_2'); end;
    RunSQL(SC[0].CreateSQL);
    RunSQL(SC[1].CreateSQL);

    nCount := 0;
    sProgressBar1.Max := Query3.RecordCount;
    sProgressBar1.Position := 0;    

    while not Query3.Eof do
    begin
      Application.ProcessMessages;
      with SC[0] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF707_1');
        ADDValue('MAINT_NO', Query3.Fields[0].AsString);
        ADDValue('MSEQ',     Query3.Fields[1].AsString);
        ADDValue('AMD_NO',   Query3.Fields[2].AsString);
        ADDValue('MESSAGE1', Query3MESSAGE1.AsString);
        ADDValue('MESSAGE2', Query3MESSAGE2.AsString);
        ADDValue('USER_ID',  Query3USER_ID.AsString);
        ADDValue('DATEE',    Query3DATEE.AsString);
        ADDValue('APP_DATE', Query3APP_DATE.AsString);
        ADDValue('IN_MATHOD',  Query3IN_MATHOD.AsString);
        ADDValue('AP_BANK',    Query3AP_BANK.AsString);
        ADDValue('AP_BANK1',   Query3AP_BANK1.AsString);
        ADDValue('AP_BANK2',   Query3AP_BANK2.AsString);
        ADDValue('AP_BANK3',   Query3AP_BANK3.AsString);
        ADDValue('AP_BANK4',   Query3AP_BANK4.AsString);
        ADDValue('AP_BANK5',   Query3AP_BANK5.AsString);
        ADDValue('AD_BANK',    Query3AD_BANK.AsString);
        ADDValue('AD_BANK1',   Query3AD_BANK1.AsString);
        ADDValue('AD_BANK2',   Query3AD_BANK2.AsString);
        ADDValue('AD_BANK3',   Query3AD_BANK3.AsString);
        ADDValue('AD_BANK4',   Query3AD_BANK4.AsString);
        ADDValue('IL_NO1',     Query3IL_NO1.AsString);
        ADDValue('IL_NO2',     Query3IL_NO2.AsString);
        ADDValue('IL_NO3',     Query3IL_NO3.AsString);
        ADDValue('IL_NO4',     Query3IL_NO4.AsString);
        ADDValue('IL_NO5',     Query3IL_NO5.AsString);
        ADDValue('IL_AMT1',    Query3IL_AMT1.asFloat);
        ADDValue('IL_AMT2',    Query3IL_AMT2.asFloat);
        ADDValue('IL_AMT3',    Query3IL_AMT3.asFloat);
        ADDValue('IL_AMT4',    Query3IL_AMT4.asFloat);
        ADDValue('IL_AMT5',    Query3IL_AMT5.asFloat);
        ADDValue('IL_CUR1',    Query3IL_CUR1.AsString);
        ADDValue('IL_CUR2',    Query3IL_CUR2.AsString);
        ADDValue('IL_CUR3',    Query3IL_CUR3.AsString);
        ADDValue('IL_CUR4',    Query3IL_CUR4.AsString);
        ADDValue('IL_CUR5',    Query3IL_CUR5.AsString);
        ADDValue('AD_INFO1',   Query3AD_INFO1.AsString);
        ADDValue('AD_INFO2',   Query3AD_INFO2.AsString);
        ADDValue('AD_INFO3',   Query3AD_INFO3.AsString);
        ADDValue('AD_INFO4',   Query3AD_INFO4.AsString);
        ADDValue('AD_INFO5',   Query3AD_INFO5.AsString);
        ADDValue('CD_NO',      Query3CD_NO.AsString);
        ADDValue('RCV_REF',    Query3RCV_REF.AsString);
        ADDValue('IBANK_REF',  Query3IBANK_REF.AsString);
        ADDValue('ISS_BANK1',  Query3ISS_BANK1.AsString);
        ADDValue('ISS_BANK2',  Query3ISS_BANK2.AsString);
        ADDValue('ISS_BANK3',  Query3ISS_BANK3.AsString);
        ADDValue('ISS_BANK4',  Query3ISS_BANK4.AsString);
        ADDValue('ISS_BANK5',  Query3ISS_BANK5.AsString);
        ADDValue('ISS_ACCNT',  Query3ISS_ACCNT.AsString);
        ADDValue('ISS_DATE',   Query3ISS_DATE.AsString);
        ADDValue('AMD_DATE',   Query3AMD_DATE.AsString);
        ADDValue('EX_DATE',    Query3EX_DATE.AsString);
        ADDValue('EX_PLACE',   Query3EX_PLACE.AsString);
        ADDValue('CHK1',       Query3CHK1.AsString);
        ADDValue('CHK2',       Query3CHK2.AsString);
        ADDValue('CHK3',       Query3CHK3.AsString);
        ADDValue('F_INTERFACE',Query3F_INTERFACE.AsString);
        ADDValue('IMP_CD1',    Query3IMP_CD1.AsString);
        ADDValue('IMP_CD2',    Query3IMP_CD2.AsString);
        ADDValue('IMP_CD3',    Query3IMP_CD3.AsString);
        ADDValue('IMP_CD4',    Query3IMP_CD4.AsString);
        ADDValue('IMP_CD5',    Query3IMP_CD5.AsString);
        ADDValue('IS_CANCEL',  Query3CAN_REQ.AsString);
        ADDValue('DOC_CD',     Query3DOC_TYPE.AsString);
        ADDValue('PSHIP',      Query3PSHIP.AsString);
        ADDValue('TSHIP',      Query3TSHIP.AsString);
        ADDValue('APP_RULE1',  Query3APPLICABLE_RULES_1.AsString);
        ADDValue('APP_RULE2',  Query3APPLICABLE_RULES_2.AsString);
        ADDValue('REIM_BANK_BIC',  Query3REI_BANK.AsString);
        ADDValue('REIM_BANK1',     Query3REI_BANK1.AsString);
        ADDValue('REIM_BANK2',     Query3REI_BANK2.AsString);
        ADDValue('REIM_BANK3',     Query3REI_BANK3.AsString);
        ADDValue('REIM_BANK4',     Query3REI_BANK4.AsString);
        ADDValue('AVT_BANK_BIC',   Query3AVT_BANK.AsString);
        ADDValue('AVT_BANK1',      Query3AVT_BANK1.AsString);
        ADDValue('AVT_BANK2',      Query3AVT_BANK2.AsString);
        ADDValue('AVT_BANK3',      Query3AVT_BANK3.AsString);
        ADDValue('AVT_BANK4',      Query3AVT_BANK4.AsString);
      end;

      with SC[1] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF707_2');
        ADDValue('MAINT_NO', Query3.Fields[0].AsString);
        ADDValue('MSEQ',     Query3.Fields[1].AsString);
        ADDValue('AMD_NO',   Query3.Fields[2].AsString);
        ADDValue('APPLIC1',  Query3APPLIC1.AsString);
        ADDValue('APPLIC2',  Query3APPLIC2.AsString);
        ADDValue('APPLIC3',  Query3APPLIC3.AsString);
        ADDValue('APPLIC4',  Query3APPLIC4.AsString);
        ADDValue('APPLIC5',  Query3APPLIC5.AsString);
        ADDValue('BENEFC1',  Query3BENEFC1.AsString);
        ADDValue('BENEFC2',  Query3BENEFC2.AsString);
        ADDValue('BENEFC3',  Query3BENEFC3.AsString);
        ADDValue('BENEFC4',  Query3BENEFC4.AsString);
        ADDValue('BENEFC5',  Query3BENEFC5.AsString);
        ADDValue('INCD_CUR', Query3INCD_CUR.AsString);
        ADDValue('INCD_AMT', Query3INCD_AMT.asfloat);
        ADDValue('DECD_CUR', Query3DECD_CUR.AsString);
        ADDValue('DECD_AMT', Query3DECD_AMT.asfloat);
        ADDValue('NWCD_CUR', Query3NWCD_CUR.asString);
        ADDValue('NWCD_AMT', Query3NWCD_AMT.asFloat);
        ADDValue('CD_PERP',  Query3CD_PERP.asfloat);
        ADDValue('CD_PERM',  Query3CD_PERM.asfloat);
        ADDValue('CD_MAX',   Query3CD_MAX.AsString);
        ADDValue('AA_CV1',   Query3AA_CV1.AsString);
        ADDValue('AA_CV2',   Query3AA_CV2.AsString);
        ADDValue('AA_CV3',   Query3AA_CV3.AsString);
        ADDValue('AA_CV4',   Query3AA_CV4.AsString);
        ADDValue('LOAD_ON',  Query3LOAD_ON.AsString);
        ADDValue('FOR_TRAN', Query3FOR_TRAN.AsString);
        ADDValue('LST_DATE', Query3LST_DATE.AsString);
        ADDValue('SHIP_PD',  Query3SHIP_PD.asBoolean);
        ADDValue('SHIP_PD1', Query3SHIP_PD1.AsString);
        ADDValue('SHIP_PD2', Query3SHIP_PD2.AsString);
        ADDValue('SHIP_PD3', Query3SHIP_PD3.AsString);
        ADDValue('SHIP_PD4', Query3SHIP_PD4.AsString);
        ADDValue('SHIP_PD5', Query3SHIP_PD5.AsString);
        ADDValue('SHIP_PD6', Query3SHIP_PD6.AsString);
        ADDValue('NARRAT',   Query3NARRAT.asBoolean);
        ADDValue('NARRAT_1', Query3NARRAT_1.AsString);
        ADDValue('SR_INFO1', Query3SR_INFO1.AsString);
        ADDValue('SR_INFO2', Query3SR_INFO2.AsString);
        ADDValue('SR_INFO3', Query3SR_INFO3.AsString);
        ADDValue('SR_INFO4', Query3SR_INFO4.AsString);
        ADDValue('SR_INFO5', Query3SR_INFO5.AsString);
        ADDValue('SR_INFO6', Query3SR_INFO6.AsString);
        ADDValue('EX_NAME1', Query3EX_NAME1.AsString);
        ADDValue('EX_NAME2', Query3EX_NAME2.AsString);
        ADDValue('EX_NAME3', Query3EX_NAME3.AsString);
        ADDValue('EX_ADDR1', Query3EX_ADDR1.AsString);
        ADDValue('EX_ADDR2', Query3EX_ADDR2.AsString);
        ADDValue('OP_BANK1', Query3OP_BANK1.AsString);
        ADDValue('OP_BANK2', Query3OP_BANK2.AsString);
        ADDValue('OP_BANK3', Query3OP_BANK3.AsString);
        ADDValue('OP_ADDR1', Query3OP_ADDR1.AsString);
        ADDValue('OP_ADDR2', Query3OP_ADDR2.AsString);
        ADDValue('BFCD_AMT', Query3BFCD_AMT.asFloat);
        ADDValue('BFCD_CUR', Query3BFCD_CUR.AsString);
        ADDValue('SUNJUCK_PORT', Query3SUNJUCK_PORT.AsString);
        ADDValue('DOCHACK_PORT', Query3DOCHACK_PORT.AsString);
        ADDValue('CHARGE', Query3CHARGE.AsString);
        TMP_STR := Query3CHARGE_NUM1.AsString;
        if Length(Trim(Query3CHARGE_NUM2.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3CHARGE_NUM2.AsString;
        if Length(Trim(Query3CHARGE_NUM3.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3CHARGE_NUM3.AsString;
        if Length(Trim(Query3CHARGE_NUM4.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3CHARGE_NUM4.AsString;
        if Length(Trim(Query3CHARGE_NUM5.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3CHARGE_NUM5.AsString;
        if Length(Trim(Query3CHARGE_NUM6.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3CHARGE_NUM6.AsString;
        ADDValue('CHARGE_1', Trim(TMP_STR));
        ADDValue('AMD_CHARGE', Query3AMD_CHARGE.AsString);
        TMP_STR := Query3AMD_CHARGE_NUM1.AsString;
        if Length(Trim(Query3AMD_CHARGE_NUM2.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3AMD_CHARGE_NUM2.AsString;
        if Length(Trim(Query3AMD_CHARGE_NUM3.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3AMD_CHARGE_NUM3.AsString;
        if Length(Trim(Query3AMD_CHARGE_NUM4.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3AMD_CHARGE_NUM4.AsString;
        if Length(Trim(Query3AMD_CHARGE_NUM5.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3AMD_CHARGE_NUM5.AsString;
        if Length(Trim(Query3AMD_CHARGE_NUM6.AsString)) > 0 Then
          TMP_STR := TMP_STR + #13#10 + Query3AMD_CHARGE_NUM6.AsString;
        ADDValue('AMD_CHARGE_1', Trim(TMP_STR));
        ADDValue('SPECIAL_PAY', Query3SPECIAL_DESC.AsString);
        ADDValue('GOODS_DESC',  Query3SPECIAL_DESC_1.AsString);
        ADDValue('DOC_REQ',     Query3DOC_DESC.AsString);
        ADDValue('ADD_CONDITION', Query3ADD_DESC_1.AsString);
        ADDValue('DRAFT1',      Query3DRAFT1.AsString);
        ADDValue('DRAFT2',      Query3DRAFT2.AsString);
        ADDValue('DRAFT3',      Query3DRAFT3.AsString);
        ADDValue('MIX1',        Query3MIX_PAY1.AsString);
        ADDValue('MIX2',        Query3MIX_PAY2.AsString);
        ADDValue('MIX3',        Query3MIX_PAY3.AsString);
        ADDValue('MIX4',        Query3MIX_PAY4.AsString);
        ADDValue('DEFPAY1',     Query3DEF_PAY1.AsString);
        ADDValue('DEFPAY2',     Query3DEF_PAY2.AsString);
        ADDValue('DEFPAY3',     Query3DEF_PAY3.AsString);
        ADDValue('DEFPAY4',     Query3DEF_PAY4.AsString);
        if Query3PERIOD.IsNull Then
          ADDValue('PERIOD_DAYS', 'NULL', vtInteger)
        else
          ADDValue('PERIOD_DAYS', Query3PERIOD.asfloat);
        if Length(Trim(Query3PERIOD_TXT.AsString)) > 0 Then
          ADDValue('PERIOD_IDX', 1)
        else
          ADDValue('PERIOD_IDX', 0);
        ADDValue('PERIOD_DETAIL', Query3PERIOD_TXT.AsString);
        ADDValue('CONFIRM', Query3CON_INST.AsString);
        ADDValue('CONFIRM_BIC', Query3CO_BANK.AsString);
        ADDValue('CONFIRM1', Query3CO_BANK1.AsString);
        ADDValue('CONFIRM2', Query3CO_BANK2.AsString);
        ADDValue('CONFIRM3', Query3CO_BANK3.AsString);
        ADDValue('CONFIRM4', Query3CO_BANK4.AsString);
        ADDValue('TXT_78',   Query3INST_DESC_1.AsString);
        ADDValue('APPLIC_CHG1', Query3AMD_APPLIC1.AsString);
        ADDValue('APPLIC_CHG2', Query3AMD_APPLIC2.AsString);
        ADDValue('APPLIC_CHG3', Query3AMD_APPLIC3.AsString);
        ADDValue('APPLIC_CHG4', Query3AMD_APPLIC4.AsString);
      end;

      RunSQL(SC[0]);
      RunSQL(SC[1]);

//      RunSQL(SC[0].CreateSQL);
//      RunSQL(SC[1].CreateSQL);

      sProgressBar1.Position := Query3.RecNo;
      Query3.Next;
    end;
  finally
    SC[0].Free;
    SC[1].Free;
    Query3.Close;
  end;
end;

procedure TDlg_ImportINF707_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  try
    ConvertData;
    ShowMessage('전환이 완료되었습니다');
  except
    on E:Exception do
    begin
//      Clipboard.AsText := qryExec.SQL.Text;
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TDlg_ImportINF707_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := Query3.SQL.Text;
end;

end.
