inherited CD_UNIT_frm: TCD_UNIT_frm
  Caption = 'CD_UNIT_frm'
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sPanel2: TsPanel
      inherited sComboBox1: TsComboBox
        Text = #45800#50948
        Items.Strings = (
          #45800#50948
          #49444#47749)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #49444#47749
          Width = 373
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39#45800#50948#39' AND Remark = 1')
  end
end
