inherited dlg_CopyAPPSPCfromLOCRC1_frm: Tdlg_CopyAPPSPCfromLOCRC1_frm
  Left = 683
  Top = 273
  Caption = #47932#54408#49688#47161#51613#47749#49436' '#49440#53469
  DockSite = True
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sPanel2: TsPanel
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #45236#44397#49888#50857#51109#48264#54840)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC_NO'
          Title.Alignment = taCenter
          Title.Caption = #45236#44397#49888#50857#51109#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#50629#52404#49345#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISS_DAT'
          Title.Alignment = taCenter
          Title.Caption = #48156#44553#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GET_DAT'
          Title.Alignment = taCenter
          Title.Caption = #51064#49688#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RFF_NO'
          Title.Alignment = taCenter
          Title.Caption = #48156#44553#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMT'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMTC'
          Title.Alignment = taCenter
          Title.Caption = #53685#54868#45800#50948
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC2AMT'
          Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
          Width = 120
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'DATE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RFF_NO, GET' +
        '_DAT, ISS_DAT, EXP_DAT, BENEFC, BENEFC1, APPLIC, APPLIC1, APPLIC' +
        '2, APPLIC3, APPLIC4, APPLIC5, APPLIC6, RCT_AMT1, RCT_AMT1C, RCT_' +
        'AMT2, RCT_AMT2C, RATE, REMARK, REMARK1, TQTY, TQTY_G, TAMT, TAMT' +
        '_G, LOC_NO, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP_' +
        'NAME, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, LOADDATE, E' +
        'XPDATE, LOC_REM, LOC_REM1, CHK1, CHK2, CHK3, NEGODT, NEGOAMT, BS' +
        'N_HSCODE, PRNO, APPLIC7, BENEFC2, CK_S'
      'FROM LOCRC1_H'
      'WHERE DATEE BETWEEN :DATE1 AND :DATE2')
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'DATE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RFF_NO, GET' +
        '_DAT, ISS_DAT, EXP_DAT, BENEFC, BENEFC1, APPLIC, APPLIC1, APPLIC' +
        '2, APPLIC3, APPLIC4, APPLIC5, APPLIC6, RCT_AMT1, RCT_AMT1C, RCT_' +
        'AMT2, RCT_AMT2C, RATE, REMARK, REMARK1, TQTY, TQTY_G, TAMT, TAMT' +
        '_G, LOC_NO, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP_' +
        'NAME, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, LOADDATE, E' +
        'XPDATE, LOC_REM, LOC_REM1, CHK1, CHK2, CHK3, NEGODT, NEGOAMT, BS' +
        'N_HSCODE, PRNO, APPLIC7, BENEFC2, CK_S'
      'FROM LOCRC1_H'
      'WHERE DATEE BETWEEN :DATE1 AND :DATE2')
  end
end
