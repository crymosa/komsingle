inherited CD_SUNSA_frm: TCD_SUNSA_frm
  Caption = 'CD_SUNSA_frm'
  ClientHeight = 431
  ClientWidth = 493
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 493
    inherited sImage1: TsImage
      Left = 470
    end
  end
  inherited sPanel3: TsPanel
    Width = 493
    Height = 406
    inherited sPanel2: TsPanel
      Width = 483
      inherited sImage2: TsImage
        Left = 459
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 483
      Height = 362
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #49440#49324#47749
          Width = 372
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'SUNSA'#39)
  end
end
