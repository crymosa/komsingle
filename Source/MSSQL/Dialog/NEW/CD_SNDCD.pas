// 价陛规过
unit CD_SNDCD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_SNDCD_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList; override;
  public
    { Public declarations }
    function GetCodeText(value: String): String; override;
  end;

var
  CD_SNDCD_frm: TCD_SNDCD_frm;

implementation

{$R *.dfm}

procedure TCD_SNDCD_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '价陛规过';
end;

function TCD_SNDCD_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''价陛规过''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    try
      Open;
      IF RecordCount > 0 Then
        Result := FieldByName('NAME').AsString;
    finally
      Close;
    end;
  end;
end;

procedure TCD_SNDCD_frm.ReadList;
begin
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''价陛规过'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
