object CodeDialogParent_frm: TCodeDialogParent_frm
  Left = 1115
  Top = 154
  BorderStyle = bsNone
  Caption = #53076#46300#54268
  ClientHeight = 559
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 25
    SkinData.CustomColor = True
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    Color = 16495715
    DoubleBuffered = False
    TabOrder = 0
    OnMouseDown = sPanel1MouseDown
    DesignSize = (
      496
      25)
    object sImage1: TsImage
      Left = 473
      Top = 3
      Width = 18
      Height = 18
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      AutoSize = True
      Picture.Data = {07544269746D617000000000}
      OnClick = sImage1Click
      ImageIndex = 0
      Images = DMICON.img_sysicon
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sLabel1: TsLabel
      Left = 5
      Top = 2
      Width = 86
      Height = 21
      Caption = #53076#46300' '#53440#51060#53952
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 25
    Width = 496
    Height = 534
    Align = alClient
    BorderWidth = 4
    DoubleBuffered = False
    TabOrder = 1
    object sPanel2: TsPanel
      Left = 5
      Top = 5
      Width = 486
      Height = 34
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alTop
      Color = 16708841
      DoubleBuffered = False
      TabOrder = 0
      DesignSize = (
        486
        34)
      object sSpeedButton1: TsSpeedButton
        Left = 328
        Top = 4
        Width = 23
        Height = 22
        ButtonStyle = tbsDivider
      end
      object sImage2: TsImage
        Left = 462
        Top = 3
        Width = 24
        Height = 24
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        AutoSize = True
        Picture.Data = {07544269746D617000000000}
        ImageIndex = 3
        Images = DMICON.System26
        Reflected = True
        SkinData.SkinSection = 'CHECKBOX'
      end
      object sEdit1: TsEdit
        Left = 106
        Top = 3
        Width = 145
        Height = 23
        TabOrder = 0
        OnKeyUp = sEdit1KeyUp
      end
      object sComboBox1: TsComboBox
        Left = 0
        Top = 3
        Width = 105
        Height = 23
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 1
        Text = #53076#46300
        Items.Strings = (
          #53076#46300
          #51060#47492)
      end
      object sButton1: TsButton
        Left = 252
        Top = 3
        Width = 75
        Height = 23
        Caption = #51312#54924
        TabOrder = 2
        OnClick = sButton1Click
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 5
      Top = 39
      Width = 486
      Height = 490
      Align = alClient
      Color = clGray
      Ctl3D = True
      DataSource = dsList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
      SkinData.CustomColor = True
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 24
    Top = 80
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 80
  end
end
