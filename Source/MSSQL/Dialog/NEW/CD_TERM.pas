unit CD_TERM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_TERM_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function SearchData(KeyValues: String): Integer; override;
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_TERM_frm: TCD_TERM_frm;

implementation

{$R *.dfm}

procedure TCD_TERM_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '가격조건';
end;

function TCD_TERM_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT NAME FROM CODE2NDD with(nolock) WHERE prefix = ''가격조건'' AND CODE = '+QuotedStr(value);
    Open;

    IF RecordCount > 0 Then
      Result := FieldByName('NAME').AsString;
  end;
end;

procedure TCD_TERM_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''가격조건'' AND Remark = 1 ORDER BY CODE';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

function TCD_TERM_frm.SearchData(KeyValues: String): Integer;
begin
  Result := -1;
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT * FROM CODE2NDD WHERE Prefix = ''가격조건'' AND CODE = '+QuotedStr(KeyValues);
    try
      Open;
      Result := RecordCount;
    finally
      Close;
    end;
  end;
end;

end.
