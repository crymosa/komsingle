unit Dialog_ItemList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, DBCtrls, sDBMemo, Grids, DBGrids,
  acDBGrid, sEdit, Buttons, sBitBtn, sComboBox, sSkinProvider, ExtCtrls,
  sPanel, DB, ADODB, sMemo, MSSQL;

type
  TDialog_ItemList_frm = class(TDialogParent_frm)
    sPanel2: TsPanel;
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    sDBGrid1: TsDBGrid;
    sMemo1: TsMemo;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListCODE: TStringField;
    qryListHSCODE: TStringField;
    qryListFNAME: TMemoField;
    qryListSNAME: TStringField;
    qryListMSIZE: TMemoField;
    qryListQTYC: TStringField;
    qryListPRICEG: TStringField;
    qryListPRICE: TBCDField;
    qryListQTY_U: TBCDField;
    qryListQTY_UG: TStringField;
    sPanel3: TsPanel;
    sBitBtn6: TsBitBtn;
    sBitBtn5: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure sBitBtn3Click(Sender: TObject);

  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog(): TModalResult;
end;

var
  Dialog_ItemList_frm: TDialog_ItemList_frm;

implementation

uses Dlg_ITEM, TypeDefine;

{$R *.dfm}

function TDialog_ItemList_frm.openDialog: TModalResult;
begin
  ReadList;
  Result := ShowModal;
end;

procedure TDialog_ItemList_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

   sMemo1.Text := qryListFNAME.AsString;
end;

procedure TDialog_ItemList_frm.ReadList;
begin

  with qryList do
  begin

    Close;
    sql.Clear;
    SQL.Text := FSQL;

    if com_SearchKeyword.ItemIndex = 0 then
    begin
      SQL.Add(' WHERE [CODE] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end
    else if com_SearchKeyword.ItemIndex = 1 then
    begin
      SQL.Add(' WHERE [SNAME] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end;

    Open;
  end;

end;

procedure TDialog_ItemList_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_ItemList_frm.btn_SearchClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_ItemList_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDialog_ItemList_frm.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  //
end;

procedure TDialog_ItemList_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  Dlg_ITEM_frm := TDlg_ITEM_frm.Create(Self);

  try
    case (Sender as TsBitBtn).Tag of
      0 :
      begin
        if Dlg_ITEM_frm.Run(ctInsert,nil) = mrOK then
        begin
          qryList.Close;
          qryList.Open;
          qryList.Locate('CODE',Dlg_ITEM_frm.edt_CODE.Text,[]);
        end;
      end;

      1 :
      begin
        if Dlg_ITEM_frm.Run(ctModify,dsList.DataSet.Fields) = mrOK then
        begin
          qryList.Close;
          qryList.Open;
          qryList.Locate('CODE',Dlg_ITEM_frm.edt_CODE.Text,[]);
        end;
      end;

      2 :
      begin
        if Dlg_ITEM_frm.Run(ctDelete,dsList.DataSet.Fields) = mrOK then
        begin
          qryList.Close;
          qryList.Open;
        end;
      end;

    end;
  finally
    FreeAndNil(Dlg_ITEM_frm);
  end;

end;

end.
