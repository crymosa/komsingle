inherited Dialog_CopyDOMOFC_frm: TDialog_CopyDOMOFC_frm
  Left = 797
  Top = 126
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  ClientHeight = 641
  ClientWidth = 921
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 921
    Height = 641
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 919
      Height = 35
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      DesignSize = (
        919
        35)
      object edt_SearchText: TsEdit
        Left = 82
        Top = 5
        Width = 229
        Height = 23
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = False
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object com_SearchKeyword: TsComboBox
        Left = 4
        Top = 5
        Width = 77
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = #46321#47197#51068#51088
        OnChange = com_SearchKeywordChange
        Items.Strings = (
          #46321#47197#51068#51088
          #44288#47532#48264#54840
          #49688#50836#51088)
      end
      object Mask_SearchDate1: TsMaskEdit
        Left = 82
        Top = 5
        Width = 80
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn1: TsBitBtn
        Left = 315
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Caption = #44160#49353
        TabOrder = 3
        OnClick = sBitBtn1Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 6
        Images = DMICON.System18
      end
      object sBitBtn21: TsBitBtn
        Tag = 900
        Left = 163
        Top = 5
        Width = 22
        Height = 23
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = sBitBtn21Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object Mask_SearchDate2: TsMaskEdit
        Left = 208
        Top = 5
        Width = 82
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 5
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn23: TsBitBtn
        Tag = 907
        Left = 289
        Top = 5
        Width = 24
        Height = 23
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = sBitBtn23Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object sPanel25: TsPanel
        Left = 184
        Top = 5
        Width = 25
        Height = 23
        Caption = '~'
        TabOrder = 7
        SkinData.SkinSection = 'PANEL'
      end
      object sBitBtn2: TsBitBtn
        Left = 846
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #52712#49548
        ModalResult = 2
        TabOrder = 8
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 18
        Images = DMICON.System18
      end
      object sBitBtn3: TsBitBtn
        Left = 776
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #49440#53469
        ModalResult = 1
        TabOrder = 9
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 17
        Images = DMICON.System18
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 36
      Width = 919
      Height = 604
      Align = alClient
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Caption = #44288#47532#48264#54840
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFR_DATE'
          Title.Caption = #48156#54665#51068#51088
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SR_NAME1'
          Title.Caption = #48156#54665#51088
          Width = 190
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UD_NAME1'
          Title.Caption = #49688#50836#51088
          Width = 190
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HS_CODE'
          Title.Caption = #45824#54364#44277#44553#47932#54408'HS'#48512#54840
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQTY'
          Title.Caption = #52509#49688#47049
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TAMT'
          Title.Caption = #52509#54633#44228
          Width = 100
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 128
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      'SELECT *  ,'
      '      TRNS_NAME.DOC_NAME as TRANSNME,'
      '      LOADD_NAME.DOC_NAME as LOADDNAME,'
      '      DEST_NAME.DOC_NAME as DESTNAME'
      #9#9'  '#9#9#9#9'   '
      ' FROM DOMOFC_H1 AS H1'
      ''
      'INNER JOIN DOMOFC_H2 AS H2 ON H1.MAINT_NO = H2.MAINT_NO '
      'INNER JOIN DOMOFC_H3 AS H3 ON H1.MAINT_NO = H3.MAINT_NO'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#50868#49569#49688#45800#39') TRNS_NAME ON H3.TRNS_ID = TRNS_NAME.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADD_NAME ON H3.LOADD = LOADD_NAME.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') DEST_NAME ON H3.DEST = DEST_NAME.CODE')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListChk1: TStringField
      FieldName = 'Chk1'
      Size = 1
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListMaint_Rff: TStringField
      FieldName = 'Maint_Rff'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListD_CHK: TStringField
      FieldName = 'D_CHK'
      Size = 6
    end
    object qryListSR_CODE: TStringField
      FieldName = 'SR_CODE'
      Size = 10
    end
    object qryListSR_NO: TStringField
      FieldName = 'SR_NO'
      Size = 10
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 10
    end
    object qryListSR_ADDR1: TStringField
      FieldName = 'SR_ADDR1'
      Size = 35
    end
    object qryListSR_ADDR2: TStringField
      FieldName = 'SR_ADDR2'
      Size = 35
    end
    object qryListSR_ADDR3: TStringField
      FieldName = 'SR_ADDR3'
      Size = 35
    end
    object qryListUD_CODE: TStringField
      FieldName = 'UD_CODE'
      Size = 10
    end
    object qryListUD_NO: TStringField
      FieldName = 'UD_NO'
      Size = 10
    end
    object qryListUD_NAME1: TStringField
      FieldName = 'UD_NAME1'
      Size = 35
    end
    object qryListUD_NAME2: TStringField
      FieldName = 'UD_NAME2'
      Size = 35
    end
    object qryListUD_NAME3: TStringField
      FieldName = 'UD_NAME3'
      Size = 10
    end
    object qryListUD_ADDR1: TStringField
      FieldName = 'UD_ADDR1'
      Size = 35
    end
    object qryListUD_ADDR2: TStringField
      FieldName = 'UD_ADDR2'
      Size = 35
    end
    object qryListUD_ADDR3: TStringField
      FieldName = 'UD_ADDR3'
      Size = 35
    end
    object qryListUD_RENO: TStringField
      FieldName = 'UD_RENO'
      Size = 35
    end
    object qryListOFR_NO: TStringField
      FieldName = 'OFR_NO'
      Size = 35
    end
    object qryListOFR_DATE: TStringField
      FieldName = 'OFR_DATE'
      Size = 8
    end
    object qryListOFR_SQ: TStringField
      FieldName = 'OFR_SQ'
      Size = 1
    end
    object qryListOFR_SQ1: TStringField
      FieldName = 'OFR_SQ1'
      Size = 70
    end
    object qryListOFR_SQ2: TStringField
      FieldName = 'OFR_SQ2'
      Size = 70
    end
    object qryListOFR_SQ3: TStringField
      FieldName = 'OFR_SQ3'
      Size = 70
    end
    object qryListOFR_SQ4: TStringField
      FieldName = 'OFR_SQ4'
      Size = 70
    end
    object qryListOFR_SQ5: TStringField
      FieldName = 'OFR_SQ5'
      Size = 70
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK_1: TMemoField
      FieldName = 'REMARK_1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListHS_CODE: TStringField
      FieldName = 'HS_CODE'
      Size = 10
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListTSTINST: TStringField
      FieldName = 'TSTINST'
      Size = 1
    end
    object qryListTSTINST1: TStringField
      FieldName = 'TSTINST1'
      Size = 70
    end
    object qryListTSTINST2: TStringField
      FieldName = 'TSTINST2'
      Size = 70
    end
    object qryListTSTINST3: TStringField
      FieldName = 'TSTINST3'
      Size = 70
    end
    object qryListTSTINST4: TStringField
      FieldName = 'TSTINST4'
      Size = 70
    end
    object qryListTSTINST5: TStringField
      FieldName = 'TSTINST5'
      Size = 70
    end
    object qryListPCKINST: TStringField
      FieldName = 'PCKINST'
      Size = 1
    end
    object qryListPCKINST1: TStringField
      FieldName = 'PCKINST1'
      Size = 70
    end
    object qryListPCKINST2: TStringField
      FieldName = 'PCKINST2'
      Size = 70
    end
    object qryListPCKINST3: TStringField
      FieldName = 'PCKINST3'
      Size = 70
    end
    object qryListPCKINST4: TStringField
      FieldName = 'PCKINST4'
      Size = 70
    end
    object qryListPCKINST5: TStringField
      FieldName = 'PCKINST5'
      Size = 70
    end
    object qryListPAY: TStringField
      FieldName = 'PAY'
      Size = 3
    end
    object qryListPAY_ETC: TStringField
      FieldName = 'PAY_ETC'
      Size = 1
    end
    object qryListPAY_ETC1: TStringField
      FieldName = 'PAY_ETC1'
      Size = 70
    end
    object qryListPAY_ETC2: TStringField
      FieldName = 'PAY_ETC2'
      Size = 70
    end
    object qryListPAY_ETC3: TStringField
      FieldName = 'PAY_ETC3'
      Size = 70
    end
    object qryListPAY_ETC4: TStringField
      FieldName = 'PAY_ETC4'
      Size = 70
    end
    object qryListPAY_ETC5: TStringField
      FieldName = 'PAY_ETC5'
      Size = 70
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListTERM_DEL: TStringField
      FieldName = 'TERM_DEL'
      Size = 3
    end
    object qryListTERM_NAT: TStringField
      FieldName = 'TERM_NAT'
      Size = 3
    end
    object qryListTERM_LOC: TStringField
      FieldName = 'TERM_LOC'
      Size = 70
    end
    object qryListORGN1N: TStringField
      FieldName = 'ORGN1N'
      Size = 3
    end
    object qryListORGN1LOC: TStringField
      FieldName = 'ORGN1LOC'
      Size = 70
    end
    object qryListORGN2N: TStringField
      FieldName = 'ORGN2N'
      Size = 3
    end
    object qryListORGN2LOC: TStringField
      FieldName = 'ORGN2LOC'
      Size = 70
    end
    object qryListORGN3N: TStringField
      FieldName = 'ORGN3N'
      Size = 3
    end
    object qryListORGN3LOC: TStringField
      FieldName = 'ORGN3LOC'
      Size = 70
    end
    object qryListORGN4N: TStringField
      FieldName = 'ORGN4N'
      Size = 3
    end
    object qryListORGN4LOC: TStringField
      FieldName = 'ORGN4LOC'
      Size = 70
    end
    object qryListORGN5N: TStringField
      FieldName = 'ORGN5N'
      Size = 3
    end
    object qryListORGN5LOC: TStringField
      FieldName = 'ORGN5LOC'
      Size = 70
    end
    object qryListTRNS_ID: TStringField
      FieldName = 'TRNS_ID'
      Size = 3
    end
    object qryListLOADD: TStringField
      FieldName = 'LOADD'
      Size = 3
    end
    object qryListLOADLOC: TStringField
      FieldName = 'LOADLOC'
      Size = 70
    end
    object qryListLOADTXT: TStringField
      FieldName = 'LOADTXT'
      Size = 1
    end
    object qryListLOADTXT1: TStringField
      FieldName = 'LOADTXT1'
      Size = 70
    end
    object qryListLOADTXT2: TStringField
      FieldName = 'LOADTXT2'
      Size = 70
    end
    object qryListLOADTXT3: TStringField
      FieldName = 'LOADTXT3'
      Size = 70
    end
    object qryListLOADTXT4: TStringField
      FieldName = 'LOADTXT4'
      Size = 70
    end
    object qryListLOADTXT5: TStringField
      FieldName = 'LOADTXT5'
      Size = 70
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTLOC: TStringField
      FieldName = 'DESTLOC'
      Size = 70
    end
    object qryListDESTTXT: TStringField
      FieldName = 'DESTTXT'
      Size = 1
    end
    object qryListDESTTXT1: TStringField
      FieldName = 'DESTTXT1'
      Size = 70
    end
    object qryListDESTTXT2: TStringField
      FieldName = 'DESTTXT2'
      Size = 70
    end
    object qryListDESTTXT3: TStringField
      FieldName = 'DESTTXT3'
      Size = 70
    end
    object qryListDESTTXT4: TStringField
      FieldName = 'DESTTXT4'
      Size = 70
    end
    object qryListDESTTXT5: TStringField
      FieldName = 'DESTTXT5'
      Size = 70
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListTQTYCUR: TStringField
      FieldName = 'TQTYCUR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListTAMTCUR: TStringField
      FieldName = 'TAMTCUR'
      Size = 3
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      Size = 100
    end
    object qryListCODE_1: TStringField
      FieldName = 'CODE_1'
      Size = 12
    end
    object qryListDOC_NAME_1: TStringField
      FieldName = 'DOC_NAME_1'
      Size = 100
    end
    object qryListCODE_2: TStringField
      FieldName = 'CODE_2'
      Size = 12
    end
    object qryListDOC_NAME_2: TStringField
      FieldName = 'DOC_NAME_2'
      Size = 100
    end
    object qryListTRANSNME: TStringField
      FieldName = 'TRANSNME'
      Size = 100
    end
    object qryListLOADDNAME: TStringField
      FieldName = 'LOADDNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
