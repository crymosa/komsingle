inherited Dialog_CopyLocapp_frm: TDialog_CopyLocapp_frm
  Left = 860
  Top = 222
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  ClientHeight = 641
  ClientWidth = 921
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 35
    Width = 921
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Width = 921
    Height = 35
    Align = alTop
    DesignSize = (
      921
      35)
    object edt_SearchText: TsEdit
      Left = 82
      Top = 5
      Width = 229
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 77
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #46321#47197#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #46321#47197#51068#51088
        #44288#47532#48264#54840
        #49688#54812#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 82
      Top = 5
      Width = 80
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 315
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 163
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 208
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 907
      Left = 289
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = sBitBtn23Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 184
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 846
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 776
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 38
    Width = 921
    Height = 603
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATEE'
        Title.Alignment = taCenter
        Title.Caption = #46321#47197#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BENEFC1'
        Title.Alignment = taCenter
        Title.Caption = #49688#54812#51088
        Width = 193
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AP_BANK1'
        Title.Alignment = taCenter
        Title.Caption = #44060#49444#51008#54665
        Width = 164
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'APP_DATE'
        Title.Alignment = taCenter
        Title.Caption = #44060#49444#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC_AMT'
        Title.Alignment = taCenter
        Title.Caption = #44060#49444#44552#50529
        Width = 97
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'LOC_AMTC'
        Title.Alignment = taCenter
        Title.Caption = #45800#50948
        Width = 38
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 968
    Top = 40
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,SUBSTRING(MAINT_NO,16,100) as MAINT_NO2, DATEE, ' +
        'USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMAINT_NO2: TStringField
      FieldName = 'MAINT_NO2'
      ReadOnly = True
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListGOODDES1: TStringField
      FieldName = 'GOODDES1'
      Size = 350
    end
    object qryListREMARK1: TStringField
      FieldName = 'REMARK1'
      Size = 350
    end
    object qryListDOC_ETC1: TStringField
      FieldName = 'DOC_ETC1'
      Size = 350
    end
    object qryListEXGOOD1: TStringField
      FieldName = 'EXGOOD1'
      Size = 350
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
