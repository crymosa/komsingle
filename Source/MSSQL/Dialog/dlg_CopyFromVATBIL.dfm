inherited dlg_CopyFromVATBIL_frm: Tdlg_CopyFromVATBIL_frm
  Left = 708
  Top = 303
  Caption = #48373#49324#54624' '#49464#44552#44228#49328#49436' '#51025#45813#49436' '#49440#53469
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sPanel2: TsPanel
      inherited btnExit: TsButton
        OnClick = btnExitClick
      end
      inherited sButton1: TsButton
        OnClick = sButton1Click
      end
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840)
      end
    end
    inherited sDBGrid1: TsDBGrid
      OnDblClick = sButton1Click
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'SDATEE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'EDATEE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      'SELECT*'
      'FROM VATBI4_H'
      'WHERE DATEE BETWEEN :SDATEE AND :EDATEE')
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'INSERTKEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'COPYKEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO VATBI1_D(KEYY,SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, ' +
        'DE_REM1, QTY, QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAM' +
        'T, USAMT, USAMT_G, SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY' +
        ', STQTY_G, RATE)'
      
        'SELECT :INSERTKEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM' +
        ', QTY, QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAM' +
        'T, USAMT_G, SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY' +
        '_G, RATE'
      'FROM VATBI4_D'
      'WHERE KEYY = :COPYKEYY')
  end
end
