inherited Dialog_CopyLOCRC2_frm: TDialog_CopyLOCRC2_frm
  Left = 848
  Top = 236
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  ClientHeight = 641
  ClientWidth = 921
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 921
    Height = 641
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 919
      Height = 35
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      DesignSize = (
        919
        35)
      object edt_SearchText: TsEdit
        Left = 82
        Top = 5
        Width = 229
        Height = 23
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = False
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object com_SearchKeyword: TsComboBox
        Left = 4
        Top = 5
        Width = 77
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = #46321#47197#51068#51088
        OnChange = com_SearchKeywordChange
        Items.Strings = (
          #46321#47197#51068#51088
          #44288#47532#48264#54840
          #49688#50836#51088)
      end
      object Mask_SearchDate1: TsMaskEdit
        Left = 82
        Top = 5
        Width = 80
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn1: TsBitBtn
        Left = 315
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Caption = #44160#49353
        TabOrder = 3
        OnClick = sBitBtn1Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 6
        Images = DMICON.System18
      end
      object sBitBtn21: TsBitBtn
        Tag = 900
        Left = 163
        Top = 5
        Width = 22
        Height = 23
        Cursor = crHandPoint
        TabOrder = 4
        OnClick = sBitBtn21Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object Mask_SearchDate2: TsMaskEdit
        Left = 208
        Top = 5
        Width = 82
        Height = 23
        AutoSize = False
        Color = clWhite
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 5
        Text = '20161125'
        OnDblClick = Mask_SearchDate1DblClick
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn23: TsBitBtn
        Tag = 907
        Left = 289
        Top = 5
        Width = 24
        Height = 23
        Cursor = crHandPoint
        TabOrder = 6
        OnClick = sBitBtn23Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 9
        Images = DMICON.System18
      end
      object sPanel25: TsPanel
        Left = 184
        Top = 5
        Width = 25
        Height = 23
        Caption = '~'
        TabOrder = 7
        SkinData.SkinSection = 'PANEL'
      end
      object sBitBtn2: TsBitBtn
        Left = 846
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #52712#49548
        ModalResult = 2
        TabOrder = 8
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 18
        Images = DMICON.System18
      end
      object sBitBtn3: TsBitBtn
        Left = 776
        Top = 5
        Width = 68
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #49440#53469
        ModalResult = 1
        TabOrder = 9
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 17
        Images = DMICON.System18
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 36
      Width = 919
      Height = 604
      Align = alClient
      Color = clWhite
      DataSource = dslist
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Caption = #44288#47532#48264#54840
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATEE'
          Title.Caption = #46321#47197#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GET_DAT'
          Title.Caption = #51064#49688#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISS_DAT'
          Title.Caption = #48156#44553#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Caption = #49688#54812#51088
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC1'
          Title.Caption = #44060#49444#50629#52404
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK1'
          Title.Caption = #44060#49444#51008#54665
          Width = 150
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 56
  end
  object qrylist: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, [USER_ID], DATEE, MESSAGE1, MESSAGE2, RFF_NO, G' +
        'ET_DAT, ISS_DAT, EXP_DAT, BENEFC, BENEFC1, APPLIC, APPLIC1, APPL' +
        'IC2, APPLIC3, APPLIC4, APPLIC5, APPLIC6, RCT_AMT1, RCT_AMT1C, RC' +
        'T_AMT2, RCT_AMT2C, RATE, REMARK, REMARK1, TQTY, TQTY_G, TAMT, TA' +
        'MT_G, LOC_NO, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, A' +
        'P_NAME, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, LOADDATE,' +
        ' EXPDATE, LOC_REM, LOC_REM1, CHK1, CHK2, CHK3, PRNO, AMAINT_NO, ' +
        'BSN_HSCODE, BENEFC2, APPLIC7'
      'FROM LOCRC2_H')
    Left = 16
    Top = 104
    object qrylistMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qrylistUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qrylistDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qrylistMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qrylistMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qrylistRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qrylistGET_DAT: TStringField
      FieldName = 'GET_DAT'
      Size = 8
    end
    object qrylistISS_DAT: TStringField
      FieldName = 'ISS_DAT'
      Size = 8
    end
    object qrylistEXP_DAT: TStringField
      FieldName = 'EXP_DAT'
      Size = 8
    end
    object qrylistBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qrylistBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qrylistAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qrylistAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qrylistAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qrylistAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 10
    end
    object qrylistAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qrylistAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qrylistAPPLIC6: TStringField
      FieldName = 'APPLIC6'
      Size = 35
    end
    object qrylistRCT_AMT1: TBCDField
      FieldName = 'RCT_AMT1'
      Precision = 18
    end
    object qrylistRCT_AMT1C: TStringField
      FieldName = 'RCT_AMT1C'
      Size = 3
    end
    object qrylistRCT_AMT2: TBCDField
      FieldName = 'RCT_AMT2'
      Precision = 18
    end
    object qrylistRCT_AMT2C: TStringField
      FieldName = 'RCT_AMT2C'
      Size = 3
    end
    object qrylistRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qrylistREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qrylistREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qrylistTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qrylistTQTY_G: TStringField
      FieldName = 'TQTY_G'
      Size = 3
    end
    object qrylistTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qrylistTAMT_G: TStringField
      FieldName = 'TAMT_G'
      Size = 3
    end
    object qrylistLOC_NO: TStringField
      FieldName = 'LOC_NO'
      Size = 35
    end
    object qrylistAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qrylistAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qrylistAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qrylistAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qrylistAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qrylistAP_NAME: TStringField
      FieldName = 'AP_NAME'
      Size = 10
    end
    object qrylistLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qrylistLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 3
    end
    object qrylistLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qrylistLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 3
    end
    object qrylistEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qrylistLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qrylistEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qrylistLOC_REM: TStringField
      FieldName = 'LOC_REM'
      Size = 1
    end
    object qrylistLOC_REM1: TMemoField
      FieldName = 'LOC_REM1'
      BlobType = ftMemo
    end
    object qrylistCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qrylistCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qrylistCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qrylistPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qrylistAMAINT_NO: TStringField
      FieldName = 'AMAINT_NO'
      Size = 35
    end
    object qrylistBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qrylistBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 10
    end
    object qrylistAPPLIC7: TStringField
      FieldName = 'APPLIC7'
      Size = 10
    end
  end
  object dslist: TDataSource
    DataSet = qrylist
    Left = 48
    Top = 104
  end
end
