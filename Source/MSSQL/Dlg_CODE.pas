unit Dlg_CODE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, sSkinProvider, ExtCtrls, sPanel, StdCtrls,
  sButton, sCheckBox, sEdit, sLabel, sBevel, TypeDefine,ADODB,DB, StrUtils;

type
  TGroupType = (gtGubun,gtCode);
  TDlg_CODE_frm = class(TDialogParent_frm)
    sLabel1: TsLabel;
    sPanel2: TsPanel;
    Edt_Gubun: TsEdit;
    edt_Name: TsEdit;
    sPanel3: TsPanel;
    edt_CODE: TsEdit;
    edt_Contents: TsEdit;
    chk_Use: TsCheckBox;
    Btn_ok: TsButton;
    Btn_Cancel: TsButton;
    sBevel1: TsBevel;
    qryGroupIns: TADOQuery;
    qryGroupMod: TADOQuery;
    qryCodeIns: TADOQuery;
    qryCodeMod: TADOQuery;
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_okClick(Sender: TObject);
  private
    { Private declarations }
    FGroupType : TGroupType;
    FProgramControlType : TProgramControlType;
    FFieldsMaster : TFields;
    FFieldsDetail : TFields;
    FCreateGubun : String;
    procedure SetData;
    procedure SetParam_Gubun(qry : TADOQuery);
    procedure SetParam_Code(qry : TADOQuery);
  public
    { Public declarations }
    function Run(GroupType : TGroupType; ProgramControlType : TProgramControlType;MasterFields,DetailFields : TFields):TModalResult;
    property CreateGubun:String read FCreateGubun;
  end;

var
  Dlg_CODE_frm: TDlg_CODE_frm;

implementation

uses MSSQL;

{$R *.dfm}

{ TDlg_CODE_frm }

function TDlg_CODE_frm.Run(GroupType : TGroupType;
                           ProgramControlType : TProgramControlType;
                           MasterFields,DetailFields : TFields):TModalResult;
begin
  Result := mrCancel;
  FGroupType := GroupType;
  FProgramControlType := ProgramControlType;
  FFieldsMaster := MasterFields;
  FFieldsDetail := DetailFields;

  Edt_Gubun.Enabled    := (FGroupType = gtGubun);
  edt_Name.Enabled     := (FGroupType = gtGubun);
  edt_CODE.Enabled     := (FGroupType = gtCode) AND (FProgramControlType = ctInsert);
  edt_Contents.Enabled := (FGroupType = gtCode);
  chk_Use.Enabled := (FGroupType = gtCode);

  Case FProgramControlType of
    ctInsert :
    begin
      IF FGroupType = gtCODE Then
      begin
        Edt_Gubun.Text := FFieldsMaster.FieldByName('Prefix').AsString;
        edt_Name.Text := FFieldsMaster.FieldByName('Name').AsString;
      end;
    end;
    ctModify :
    begin
      SetData;
    end;
  end;

  IF Self.ShowModal = mrOK Then
  begin
    Case FGroupType of
      gtGubun :
      begin
        Case FProgramControlType of
          ctInsert : SetParam_Gubun(qryGroupIns);
          ctModify : SetParam_Gubun(qryGroupMod);
        end;
      end;

      gtCode :
      begin
        Case FProgramControlType of
          ctInsert : SetParam_Code(qryCodeIns);
          ctModify : SetParam_Code(qryCodeMod);
        end;
      end;
    end;

    Result := mrOk;
  end
end;

procedure TDlg_CODE_frm.SetData;
begin
  Case FGroupType of
    gtGubun :
    begin
      Edt_Gubun.Text := FFieldsMaster.FieldByName('Prefix').AsString;
      edt_Name.Text  := FFieldsMaster.FieldByName('Name').AsString;
    end;

    gtCode :
    begin
      Edt_Gubun.Text := FFieldsMaster.FieldByName('Prefix').AsString;
      edt_Name.Text := FFieldsMaster.FieldByName('Name').AsString;
      edt_CODE.Text := FFieldsDetail.FieldByName('Code').AsString;
      edt_Contents.Text := FFieldsDetail.FieldByName('Name').AsString;
      chk_Use.Checked := AnsiMatchText(FFieldsDetail.FieldByName('Remark').AsString,['1']);
    end;

  end;

end;

procedure TDlg_CODE_frm.SetParam_Code(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('Prefix').Value := Edt_Gubun.Text;
    Parameters.ParamByName('CODE').Value := edt_CODE.Text;
    IF chk_Use.Checked Then
      Parameters.ParamByName('Remark').Value := '1'
    else
      Parameters.ParamByName('Remark').Value := '0';
    Parameters.ParamByName('Map').Value := edt_CODE.Text;
    Parameters.ParamByName('Name').Value := edt_Contents.Text;
    ExecSQL;
  end;
end;

procedure TDlg_CODE_frm.SetParam_Gubun(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('Prefix').Value := Edt_Gubun.Text;
    Parameters.ParamByName('Name').Value := edt_Name.Text;
    Parameters.ParamByName('Gubun').Value := '일반';
    ExecSQL;
  end;
end;

procedure TDlg_CODE_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_CODE_frm.Btn_okClick(Sender: TObject);
var
  qryCheck : TADOQuery;
  bOverlab : Boolean;
  TempStr : String;
begin
  inherited;
  qryCheck := TADOQuery.Create(Self);
  qryCheck.Connection := DMMssql.KISConnect;
  bOverlab := False;
  try
    IF FProgramControlType = ctInsert then
    begin
      Case FGroupType of
        gtGubun:
        begin
          //Prefix중복확인
          qryCheck.SQL.Text := 'SELECT Prefix FROM CODE2NDM WHERE [Prefix] = :Prefix';
          qryCheck.Parameters.ParamByName('Prefix').Value := Edt_Gubun.Text;
          qryCheck.Open;

          bOverlab := qryCheck.RecordCount > 0;

          if bOverlab Then
          begin
            TempStr := '중복되는 구분코드 : '+qryCheck.FieldByName('Prefix').AsString;
          end;
        end;

        gtCode :
        begin
          //CODE중복확인
          qryCheck.SQL.Text := 'SELECT CODE2NDM.Prefix,CODE2NDM.Name,LTRIM(CODE2NDD.Name) as Name2,Gubun,CODE'#13#10+
                               'FROM CODE2NDM LEFT JOIN CODE2NDD ON CODE2NDM.Prefix = CODE2NDD.Prefix'#13#10+
                               'WHERE CODE2NDM.[Prefix] = :Prefix AND [CODE] = :CODE';
          qryCheck.Parameters.ParamByName('Prefix').Value := Edt_Gubun.Text;
          qryCheck.Parameters.ParamByName('CODE').Value := edt_CODE.Text;
          qryCheck.Open;

          bOverlab := qryCheck.RecordCount > 0;

          if bOverlab Then
          begin
            TempStr := '중복되는 구분코드 : ['+qryCheck.FieldByName('Prefix').AsString+']['+qryCheck.FieldByName('Name').AsString+']'#13#10+
                       '코드 : ['+qryCheck.FieldByName('CODE').AsString+']['+qryCheck.FieldByName('Name2').AsString+']';
          end;
        end;
      end;
    end;

    CASE AnsiIndexText('',[Trim(edt_Name.Text),Trim(edt_contents.Text),Trim(Edt_Gubun.Text),Trim(edt_CODE.Text)]) OF
      0: begin
           TempStr := '[명칭]을 입력하세요';
           edt_Name.SetFocus;
           bOverlab := True;
         end;
      1: begin
           IF FGroupType = gtCODE then
           begin
             Tempstr := '[항목]을 입력하세요';
             edt_Contents.SetFocus;
             bOverlab := True;
           end
           else
             bOverlab := False;
         end;
      2: begin
           TempStr := '[구분코드]를 입력하세요';
           Edt_Gubun.SetFocus;
           bOverlab := True;
         end;
      3: begin
           IF FGroupType = gtCODE then
           begin
             TempStr := '[코드]를 입력하세요';
             edt_CODE.SetFocus;
             bOverlab := True;
           end
           else
            bOverlab := False;
         end;
    end;

    IF not bOverlab Then
    begin
      if FProgramControlType = ctInsert Then
      begin
        Case FGroupType of
          gtGubun : FCreateGubun := Edt_Gubun.Text;
          gtCode : FCreateGubun := edt_CODE.Text;
        end;
      end
      else
        FCreateGubun := '';

      ModalResult := mrOk
    end
    else
    begin
      ShowMessage(TempStr);
    end;
  finally
    qryCheck.Close;
    qryChecK.Free;
  end;
end;

end.
