inherited ITEM_MSSQL_frm: TITEM_MSSQL_frm
  Left = 1019
  Top = 191
  Caption = #54408#47749' '#48143' '#44508#44201#53076#46300
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    Height = 43
    inherited sSpeedButton3: TsSpeedButton
      Left = 366
      Height = 41
    end
    inherited Btn_Close: TsSpeedButton
      Height = 41
      OnClick = Btn_CloseClick
      Images = DMICON.System18
      ImageIndex = 34
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 377
      Height = 41
      Visible = False
      Images = DMICON.System18
    end
    inherited sSpeedButton5: TsSpeedButton
      Height = 41
    end
    inherited sSplitter2: TsSplitter
      Height = 41
      SkinData.SkinSection = 'TRANSPARENT'
    end
    inherited sLabel1: TsLabel
      Top = 11
    end
    inherited Btn_New: TsSpeedButton
      Left = 201
      Height = 41
      OnClick = Btn_NewClick
      Images = DMICON.System18
    end
    inherited Btn_Modify: TsSpeedButton
      Left = 256
      Height = 41
      OnClick = Btn_NewClick
      Images = DMICON.System18
    end
    inherited Btn_Del: TsSpeedButton
      Left = 311
      Height = 41
      OnClick = Btn_NewClick
      Images = DMICON.System18
    end
    inherited sSpeedButton1: TsSpeedButton
      Height = 41
      Align = alNone
    end
  end
  inherited sPanel7: TsPanel
    Top = 43
    inherited sEdit1: TsEdit
      OnKeyUp = sEdit1KeyUp
    end
    inherited sBitBtn1: TsBitBtn
      OnClick = sBitBtn1Click
    end
  end
  inherited sDBGrid1: TsDBGrid
    Top = 75
    Height = 431
    DataSource = dsList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #53076#46300
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SNAME'
        Title.Caption = #54408#47749
        Width = 162
        Visible = True
      end>
  end
  inherited sPanel2: TsPanel
    Top = 75
    Height = 431
    inherited sDBEdit1: TsDBEdit
      DataSource = dsList
    end
    inherited sDBEdit2: TsDBEdit
      DataField = 'HSCODE'
      DataSource = dsList
    end
    inherited sDBEdit3: TsDBEdit
      DataField = 'SNAME'
      DataSource = dsList
    end
    inherited sDBMemo1: TsDBMemo
      DataField = 'FNAME'
      DataSource = dsList
    end
    inherited sDBMemo2: TsDBMemo
      DataField = 'MSIZE'
      DataSource = dsList
    end
    inherited sDBEdit4: TsDBEdit
      DataField = 'QTYC'
      DataSource = dsList
    end
    inherited sDBEdit6: TsDBEdit
      DataField = 'PRICEG'
      DataSource = dsList
    end
    inherited sDBEdit8: TsDBEdit
      DataField = 'PRICE'
      DataSource = dsList
    end
    inherited sDBEdit9: TsDBEdit
      DataField = 'QTY_U'
      DataSource = dsList
    end
    inherited sDBEdit10: TsDBEdit
      DataField = 'QTY_UG'
      DataSource = dsList
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '      ,[HSCODE]'
      '      ,[FNAME]'
      '      ,[SNAME]'
      '      ,[MSIZE]'
      '      ,[QTYC]'
      '      ,[PRICEG]'
      '      ,[PRICE]'
      '      ,[QTY_U]'
      '      ,[QTY_UG]'
      '  FROM [ITEM_CODE]')
    Left = 96
    Top = 168
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 128
    Top = 168
  end
end
