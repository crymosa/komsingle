unit Dlg_HSCODE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sEdit, sButton, ExtCtrls, sBevel, sPanel,
  sSkinProvider, DB, ADODB, TypeDefine;

type
  TDlg_HSCODE_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sBevel1: TsBevel;
    sButton2: TsButton;
    sButton3: TsButton;
    edt_CODE: TsEdit;
    edt_REPName: TsEdit;
    edt_Name: TsEdit;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    qryDel: TADOQuery;
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
    FProgramControl : TProgramControlType;
    FFields : TFields;
    procedure SetData;
    procedure SetParam(qry : TADOQuery);
    function  CheckCode:Boolean;
    procedure DeleteData(CODE: String);
  public
    { Public declarations }
    function Run(ProgramControl : TProgramControlType; UserFields : TFields):TModalResult;
  end;

var
  Dlg_HSCODE_frm: TDlg_HSCODE_frm;

implementation

{$R *.dfm}

{ TDlg_HSCODE_frm }

uses MSSQL, Commonlib ;

function TDlg_HSCODE_frm.Run(ProgramControl: TProgramControlType;
  UserFields: TFields): TModalResult;
begin
  FProgramControl := ProgramControl;
  FFields := UserFields;

  edt_CODE.Enabled := FProgramControl = ctInsert;

  Result := mrCancel;


  Case FProgramControl of
    ctInsert :
    begin
      Self.Caption := 'HS코드 - [신규]';
    end;

    ctModify :
    begin
      Self.Caption := 'HS코드 - [수정]';
      SetData;
    end;

    ctDelete :
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'[코드] : '+FFields.FieldByName('CODE').AsString+#13#10'삭제한 데이터를 복구가 불가능 합니다') Then
      begin
        DeleteData(FFields.FieldByName('CODE').AsString);
        Result := mrOk;
        Exit;
      end
      else
      begin
        Result := mrCancel;
        Exit;
      end;
    end;

  end;

  if Self.ShowModal = mrOk Then
  begin
    Case FProgramControl of
      ctInsert : SetParam(qryIns);
      ctModify : SetParam(qryMod);
    end;
    Result := mrOk;
  end;

end;

procedure TDlg_HSCODE_frm.SetData;
begin
  edt_CODE.Text     := FFields.FieldByName('CODE').AsString;
  edt_REPName.Text    := FFields.FieldByName('REPNAME').AsString;
  edt_Name.Text  := FFields.FieldByName('NAME').AsString;
end;

procedure TDlg_HSCODE_frm.SetParam(qry: TADOQuery);
begin
  with qry do
  begin

    Close;
    Parameters.ParamByName('CODE').Value := edt_CODE.Text;
    Parameters.ParamByName('REPNAME').Value := edt_REPName.Text;
    Parameters.ParamByName('NAME').Value := edt_Name.Text;
    ExecSQL;
    
  end;

end;

procedure TDlg_HSCODE_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_HSCODE_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  IF (FProgramControl = ctInsert) AND (Trim(edt_CODE.Text) = '') Then
  begin
    edt_CODE.SetFocus;
    Raise Exception.Create('코드를 입력하세요');
  end;

  IF (FProgramControl = ctInsert) AND (Trim(edt_REPName.Text) = '') Then
  begin
    edt_CODE.SetFocus;
    Raise Exception.Create('대표품명을 입력하세요');
  end;

  IF CheckCode AND (FProgramControl = ctInsert) Then
  begin
    raise Exception.Create('해당 코드는 이미 등록되어 있습니다.');
  end;

  ModalResult := mrOk;

end;

function TDlg_HSCODE_frm.CheckCode: Boolean;
var
  qry : TADOQuery;
begin
  Result := False;

  qry := TADOQuery.Create(Self);

  try
    with qry do
    begin
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT 1 FROM HS_CODE WHERE CODE = :CODE';
      Parameters.ParamByName('CODE').Value := edt_CODE.Text;
      Open;

      Result := qry.RecordCount > 0;
    end;
  finally
    qry.Free;
  end;
end;

procedure TDlg_HSCODE_frm.DeleteData(CODE: String);
begin
  with qryDel do
  begin
    Close;
    Parameters.ParamByName('CODE').Value := CODE;
    ExecSQL;
  end;
end;

end.
