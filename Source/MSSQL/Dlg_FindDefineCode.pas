unit Dlg_FindDefineCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sEdit, sComboBox, Grids, DBGrids,
  acDBGrid, sSkinProvider, ExtCtrls, sPanel, DB, StrUtils, sButton;

type
  TDlg_FindDefineCode_frm = class(TDialogParent_frm)
    sDBGrid1: TsDBGrid;
    sComboBox1: TsComboBox;
    sEdit1: TsEdit;
    DataSource1: TDataSource;
    sButton2: TsButton;
    sButton3: TsButton;
    sButton1: TsButton;
    sButton4: TsButton;
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Run(Gubun : String):TModalResult;
  end;

var
  Dlg_FindDefineCode_frm: TDlg_FindDefineCode_frm;

implementation

uses CodeContents, CODE_MSSQL;

{$R *.dfm}

{ TDlg_FindDefineCode_frm }

function TDlg_FindDefineCode_frm.Run(Gubun: String): TModalResult;
begin
  DMCodeContents.CODEREFRESH;
  Case AnsiIndexText(Gubun,['DANWI',
                            'TONGHWA',
                            'APPPCR_GUBUN',
                            'APPPCR_DOCUMENTS',
                            'BYEONDONG_GUBUN',
                            'NAEGUKSIN4025',
                            'NATION',
                            'NAEGUKSIN4487',
                            'PSHIP',
                            'NAEGUKSIN1001',
                            'NAEGUKSIN4277'])
  of
    0: DataSource1.DataSet := DMCodeContents.DANWI;
    1: DataSource1.DataSet := DMCodeContents.TONGHWA;
    2: DataSource1.DataSet := DMCodeContents.APPPCR_GUBUN;
    3: DataSource1.DataSet := DMCodeContents.APPPCR_DOCUMENTS;
    4: DataSource1.DataSet := DMCodeContents.BYEONDONG_GUBUN;
    5: DataSource1.DataSet := DMCodeContents.NAEGUKSIN4025;
    6: DataSource1.DataSet := DMCodeContents.NATION;
    7: DataSource1.DataSet := DMCodeContents.NAEGUKSIN4487;
    8: DataSource1.DataSet := DMCodeContents.PSHIP;
    9: DataSource1.DataSet := DMCodeContents.NAEGUKSIN1001;
    10: DataSource1.DataSet := DMCodeContents.NAEGUKSIN4277;
  end;

  IF DataSource1.DataSet <> nil Then
  begin
    DataSource1.DataSet.Open;
    Result := Self.ShowModal;
  end;
end;

procedure TDlg_FindDefineCode_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF DataSource1.DataSet.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDlg_FindDefineCode_frm.sDBGrid1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN THEN ModalResult := mrOk;
end;

procedure TDlg_FindDefineCode_frm.sButton4Click(Sender: TObject);
begin
  inherited;

//  ShowMessage( DataSource1.DataSet.FieldByName('Prefix').AsString);
  CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Self);
  try
    if CODE_MSSQL_frm.Run(DataSource1.DataSet.FieldByName('Prefix').AsString) = mrOk then
    begin
      DataSource1.DataSet.Close;
      DataSource1.DataSet.Open;
    end;
  finally
    FreeAndNil(CODE_MSSQL_frm);
  end;

end;

end.
