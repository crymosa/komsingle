inherited LOCAPP_N_MSSQL_frm: TLOCAPP_N_MSSQL_frm
  Left = 948
  Top = 95
  Caption = #45236#44397#49888#50857#51109#44060#49444#49888#52397#49436'['#44060#49444#50629#52404']'
  ClientHeight = 662
  ClientWidth = 810
  OnKeyPress = nil
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 12
  inherited sDBEdit1: TsDBEdit
    Left = 562
    Top = 27
    Visible = False
  end
  inherited sPageControl1: TsPageControl
    Width = 810
    Height = 605
    ActivePage = sTabSheet4
    TabIndex = 3
    OnChange = sPageControl1Change
    OnChanging = sPageControl1Changing
    inherited sTabSheet1: TsTabSheet
      inherited sDBGrid1: TsDBGrid
        Width = 802
        Height = 538
        DataSource = dsList
        TabOrder = 1
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #47928#49436
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 188
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 142
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 149
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 107
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #49569#49888#54788#54889
            Width = 127
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = 'USER'
            Width = 35
            Visible = True
          end>
      end
      inherited sPanel7: TsPanel
        Width = 802
        TabOrder = 0
        inherited sEdit3: TsEdit
          TabOrder = 3
          OnKeyPress = sEdit3KeyPress
        end
        inherited sButton1: TsButton
          TabOrder = 4
          OnClick = sButton1Click
        end
        inherited sComboBox1: TsComboBox
          TabOrder = 2
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter1: TsSplitter
        Width = 802
      end
      inherited sPanel2: TsPanel
        Width = 802
        object sComboBox2: TsComboBox [0]
          Left = 584
          Top = 8
          Width = 25
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = -1
          TabOrder = 4
          Visible = False
          OnExit = sComboBox2Exit
        end
        inherited sDBEdit23: TsDBEdit
          Left = 408
          Color = clGray
          DataField = 'USER_ID'
          DataSource = dsList
          Font.Color = clWhite
          SkinData.CustomFont = True
        end
        inherited sDBEdit2: TsDBEdit
          Left = 504
          DataSource = dsList
        end
        inherited DB_MESSAGE1: TsDBEdit
          Tag = 100
          DataSource = dsList
          OnEnter = sDBEdit75Enter
        end
        inherited DB_MESSAGE2: TsDBEdit
          Tag = 101
          Width = 41
          DataSource = dsList
          OnEnter = sDBEdit75Enter
        end
        inherited edt_MAINT_NO1: TsEdit
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
        end
        inherited edt_MAINT_NO2: TsEdit
          TabStop = False
          Color = clBtnFace
          MaxLength = 2
          ReadOnly = True
          TabOrder = 7
          OnEnter = edt_MAINT_NO2Enter
          SkinData.CustomColor = True
        end
        inherited edt_MAINT_NO3: TsEdit
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
        end
        object com_MakeCodeBank: TsComboBox
          Left = 443
          Top = 5
          Width = 30
          Height = 23
          BoundLabel.Caption = #51008#54665
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          Enabled = False
          ItemHeight = 17
          ItemIndex = -1
          TabOrder = 5
          Visible = False
          OnSelect = com_MakeCodeBankSelect
          OnExit = com_MakeCodeBankExit
        end
      end
      inherited sPanel3: TsPanel
        Width = 802
        Height = 533
        inherited sLabel3: TsLabel
          Left = 500
        end
        inherited sLabel5: TsLabel
          Left = 420
        end
        inherited sLabel8: TsLabel
          Left = 500
        end
        inherited sLabel9: TsLabel
          Left = 500
          Caption = #47932#54408#47749#49464#44032' '#44592#51116#46108' '#49569#51109
        end
        inherited sLabel10: TsLabel
          Left = 500
        end
        inherited sLabel11: TsLabel
          Left = 500
        end
        object sSpeedButton4: TsSpeedButton [6]
          Left = 214
          Top = 13
          Width = 23
          Height = 23
          OnClick = sSpeedButton4Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton6: TsSpeedButton [7]
          Left = 214
          Top = 189
          Width = 23
          Height = 23
          OnClick = sSpeedButton6DblClick
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton7: TsSpeedButton [8]
          Left = 198
          Top = 453
          Width = 23
          Height = 23
          OnClick = sSpeedButton7Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton8: TsSpeedButton [9]
          Left = 550
          Top = 37
          Width = 23
          Height = 23
          OnClick = sSpeedButton8Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton9: TsSpeedButton [10]
          Left = 550
          Top = 109
          Width = 23
          Height = 23
          OnClick = sSpeedButton9Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton15: TsSpeedButton [11]
          Left = 550
          Top = 61
          Width = 23
          Height = 23
          OnClick = sSpeedButton8Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        inherited sPanel4: TsPanel
          Left = 380
          TabOrder = 29
        end
        inherited DB_OWNERCOMPANYNAME: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'APPLIC1'
          DataSource = dsList
        end
        inherited DB_OWNERNAME: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'APPLIC2'
          DataSource = dsList
        end
        inherited DB_OWNERADDR1: TsDBEdit
          Left = 134
          TabStop = True
          DataSource = dsList
        end
        inherited DB_OWNERADDR2: TsDBEdit
          Left = 134
          TabStop = True
          DataSource = dsList
        end
        inherited DB_OWNERADDR3: TsDBEdit
          Left = 134
          TabStop = True
          DataSource = dsList
        end
        inherited sDBEdit40: TsDBEdit
          Left = 708
          TabStop = True
          DataSource = dsList
          TabOrder = 40
        end
        inherited sDBEdit41: TsDBEdit
          Left = 708
          TabStop = True
          DataSource = dsList
          TabOrder = 41
        end
        inherited sDBEdit42: TsDBEdit
          Left = 708
          TabStop = True
          DataSource = dsList
          TabOrder = 42
        end
        inherited sDBEdit43: TsDBEdit
          Left = 708
          TabStop = True
          DataSource = dsList
          TabOrder = 43
        end
        inherited sDBEdit44: TsDBEdit
          Left = 708
          TabStop = True
          DataSource = dsList
          TabOrder = 44
        end
        inherited DB_OWNERSAUPNO: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'APPLIC3'
          DataSource = dsList
          TabOrder = 6
        end
        inherited DB_OWNERCODE: TsDBEdit
          Tag = 9
          Left = 134
          Width = 79
          TabStop = True
          Color = clYellow
          DataField = 'APPLIC'
          DataSource = dsList
          TabOrder = 0
          OnDblClick = DB_OWNERCODEDblClick
        end
        inherited DB_BNFCOMPANYNAME: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BENEFC1'
          DataSource = dsList
          TabOrder = 8
        end
        inherited DB_BNFNAME: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BENEFC2'
          DataSource = dsList
          TabOrder = 9
        end
        inherited DB_BNFADDR1: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BNFADDR1'
          DataSource = dsList
          TabOrder = 10
        end
        inherited DB_BNFADDR2: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BNFADDR2'
          DataSource = dsList
          TabOrder = 11
        end
        inherited DB_BNFADDR3: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BNFADDR3'
          DataSource = dsList
          TabOrder = 12
        end
        inherited DB_BNFSAUPNO: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BENEFC3'
          DataSource = dsList
          TabOrder = 13
        end
        inherited DB_BNFCODE: TsDBEdit
          Tag = 9
          Left = 134
          Width = 79
          TabStop = True
          Color = 11923648
          DataField = 'BENEFC'
          DataSource = dsList
          TabOrder = 7
          OnDblClick = DB_OWNERCODEDblClick
        end
        inherited sDBEdit3: TsDBEdit
          Left = 166
          TabStop = True
          DataField = 'CHKNAME1'
          DataSource = dsList
          TabOrder = 14
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit4: TsDBEdit
          Left = 166
          TabStop = True
          DataField = 'CHKNAME2'
          DataSource = dsList
          TabOrder = 15
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit5: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'BNFEMAILID'
          DataSource = dsList
          TabOrder = 16
        end
        inherited sDBEdit6: TsDBEdit
          Left = 246
          TabStop = True
          DataField = 'BNFDOMAIN'
          DataSource = dsList
          TabOrder = 17
        end
        inherited sDBEdit8: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'AP_BANK1'
          DataSource = dsList
          TabOrder = 19
        end
        inherited sDBEdit9: TsDBEdit
          Left = 134
          TabStop = True
          DataField = 'AP_BANK2'
          DataSource = dsList
          TabOrder = 20
        end
        inherited sDBEdit7: TsDBEdit
          Tag = 9
          Left = 134
          TabStop = True
          Color = 11923648
          DataField = 'AP_BANK'
          DataSource = dsList
          TabOrder = 18
          OnDblClick = sDBEdit7DblClick
        end
        inherited sDBEdit10: TsDBEdit
          Tag = 9
          Left = 510
          Hint = #45236#44397#49888' 4487'
          TabStop = True
          CharCase = ecUpperCase
          Color = 11923648
          DataField = 'LOC_TYPE'
          DataSource = dsList
          TabOrder = 21
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
        end
        inherited sDBEdit11: TsDBEdit
          Tag = 9
          Left = 510
          TabStop = True
          CharCase = ecUpperCase
          DataField = 'LOC_AMTC'
          DataSource = dsList
          TabOrder = 22
          OnDblClick = sDBEdit10DblClick
        end
        inherited sDBEdit12: TsDBEdit
          Left = 532
          TabStop = True
          DataField = 'CD_PERP'
          DataSource = dsList
          TabOrder = 24
        end
        inherited sDBEdit13: TsDBEdit
          Left = 603
          TabStop = True
          DataField = 'CD_PERM'
          DataSource = dsList
          TabOrder = 25
        end
        inherited sDBEdit14: TsDBEdit
          Left = 574
          Width = 159
          DataField = 'LOC_TYPENAME'
          DataSource = dsList
          TabOrder = 38
        end
        inherited sDBEdit15: TsDBEdit
          Left = 574
          Width = 159
          TabStop = True
          Color = clWhite
          DataField = 'LOC_AMT'
          DataSource = dsList
          Font.Color = 4013373
          TabOrder = 23
        end
        inherited sDBEdit16: TsDBEdit
          Tag = 9
          Left = 510
          Hint = #45236#44397#49888' 4025'
          TabStop = True
          CharCase = ecUpperCase
          Color = 11923648
          DataField = 'BUSINESS'
          DataSource = dsList
          TabOrder = 26
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
        end
        inherited sDBEdit17: TsDBEdit
          Left = 574
          Width = 159
          DataField = 'BUSINESSNAME'
          DataSource = dsList
          TabOrder = 39
        end
        inherited sDBEdit18: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'LCNO'
          DataSource = dsList
          TabOrder = 27
        end
        inherited sDBEdit19: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO1'
          DataSource = dsList
          TabOrder = 28
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit20: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO2'
          DataSource = dsList
          TabOrder = 30
        end
        inherited sDBEdit21: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO3'
          DataSource = dsList
          TabOrder = 31
        end
        inherited sDBEdit22: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO4'
          DataSource = dsList
          TabOrder = 32
        end
        inherited sDBEdit24: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO5'
          DataSource = dsList
          TabOrder = 33
        end
        inherited sDBEdit25: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO6'
          DataSource = dsList
          TabOrder = 34
        end
        inherited sDBEdit26: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO7'
          DataSource = dsList
          TabOrder = 35
        end
        inherited sDBEdit27: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO8'
          DataSource = dsList
          TabOrder = 36
        end
        inherited sDBEdit28: TsDBEdit
          Left = 510
          TabStop = True
          DataField = 'OFFERNO9'
          DataSource = dsList
          TabOrder = 37
        end
        object edt_N4487: TsEdit
          Left = 574
          Top = 37
          Width = 159
          Height = 23
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 45
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_N4025: TsEdit
          Left = 574
          Top = 109
          Width = 159
          Height = 23
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 46
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sPanel8: TsPanel
        Width = 802
        Height = 570
        inherited sLabel13: TsLabel
          Font.Style = [fsBold]
        end
        inherited sLabel15: TsLabel
          Left = 310
          Top = 65
        end
        object sSpeedButton10: TsSpeedButton [3]
          Left = 512
          Top = 61
          Width = 23
          Height = 23
          OnClick = sSpeedButton10Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        inherited sDBEdit45: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 1
          OnExit = sDBEdit45Exit
        end
        inherited sDBEdit46: TsDBEdit
          TabStop = True
          DataField = 'APP_DATE'
          DataSource = dsList
          TabOrder = 2
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit50: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 8
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit51: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 9
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit52: TsDBEdit
          Tag = 9
          Hint = 'PSHIP'
          TabStop = True
          Color = 11923648
          DataSource = dsList
          TabOrder = 10
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit53: TsDBEdit
          Left = 536
          DataField = 'TRANSPRTNAME'
          DataSource = dsList
          TabOrder = 11
        end
        inherited sPanel9: TsPanel
          TabOrder = 0
        end
        inherited sDBEdit54: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 3
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBMemo1: TsDBMemo
          DataSource = dsList
          TabOrder = 4
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBMemo2: TsDBMemo
          DataSource = dsList
          TabOrder = 5
        end
        inherited sDBMemo3: TsDBMemo
          DataSource = dsList
          TabOrder = 6
        end
        inherited sDBComboBox1: TsDBComboBox
          Left = 268
          Top = 62
          DataField = 'DOC_PRD'
          DataSource = dsList
          Items.Strings = (
            '5'
            '7')
          TabOrder = 7
        end
        object edt_TRANSPRT: TsEdit
          Left = 536
          Top = 61
          Width = 200
          Height = 23
          TabStop = False
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sPanel10: TsPanel
        Width = 802
        Height = 570
        object sSpeedButton11: TsSpeedButton [1]
          Left = 162
          Top = 37
          Width = 23
          Height = 23
          OnClick = sSpeedButton11Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton12: TsSpeedButton [2]
          Left = 162
          Top = 157
          Width = 23
          Height = 23
          OnClick = sSpeedButton12Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton13: TsSpeedButton [3]
          Left = 514
          Top = 133
          Width = 23
          Height = 23
          OnClick = sSpeedButton13Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sSpeedButton14: TsSpeedButton [4]
          Left = 586
          Top = 37
          Width = 23
          Height = 23
          OnClick = sSpeedButton14Click
          SkinData.SkinSection = 'SPEEDBUTTON'
          ImageIndex = 0
        end
        inherited sDBEdit55: TsDBEdit
          Tag = 9
          Hint = #45236#44397#49888'1001'
          TabStop = True
          CharCase = ecUpperCase
          Color = 11923648
          DataSource = dsList
          TabOrder = 2
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
        end
        inherited sDBEdit56: TsDBEdit
          Left = 186
          Width = 191
          DataField = 'DOC_DTLNAME'
          DataSource = dsList
          TabOrder = 13
        end
        inherited sDBEdit57: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 3
        end
        inherited sDBEdit58: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 4
        end
        inherited sDBEdit60: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 5
        end
        inherited sDBEdit61: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 6
        end
        inherited sDBEdit62: TsDBEdit
          Tag = 9
          TabStop = True
          Color = 11923648
          DataSource = dsList
          TabOrder = 15
          OnDblClick = DB_OWNERCODEDblClick
        end
        inherited sDBEdit63: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 16
        end
        inherited sDBEdit64: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 17
        end
        inherited sDBEdit65: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 18
        end
        inherited sDBEdit66: TsDBEdit
          Tag = 9
          TabStop = True
          CharCase = ecUpperCase
          Color = 11923648
          DataSource = dsList
          TabOrder = 19
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
        end
        inherited sDBEdit67: TsDBEdit
          Left = 538
          Width = 239
          DataField = 'DESTNAME'
          DataSource = dsList
          TabOrder = 22
        end
        inherited sDBEdit68: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 20
        end
        inherited sDBEdit69: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 21
        end
        inherited sDBEdit70: TsDBEdit
          Tag = 9
          Hint = #45236#44397#49888'4277'
          TabStop = True
          CharCase = ecUpperCase
          Color = 11923648
          DataSource = dsList
          TabOrder = 7
          OnDblClick = sDBEdit10DblClick
          OnExit = sDBEdit10Exit
        end
        inherited sDBEdit71: TsDBEdit
          Left = 186
          Width = 191
          DataField = 'PAYMENTNAME'
          DataSource = dsList
          TabOrder = 14
        end
        inherited sPanel11: TsPanel
          TabOrder = 0
        end
        inherited sDBMemo4: TsDBMemo
          DataSource = dsList
          TabOrder = 8
        end
        inherited sPanel12: TsPanel
          TabOrder = 1
        end
        inherited sDBEdit72: TsDBEdit
          DataSource = dsList
          TabOrder = 9
          SkinData.CustomFont = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Font.Color = clBlue
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit73: TsDBEdit
          DataSource = dsList
          TabOrder = 10
        end
        inherited sDBEdit74: TsDBEdit
          DataSource = dsList
          TabOrder = 11
          SkinData.CustomFont = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Font.Style = [fsBold]
        end
        inherited sDBEdit59: TsDBEdit
          TabStop = True
          DataSource = dsList
          TabOrder = 12
        end
        object Edt_DOC_DTL: TsEdit
          Left = 186
          Top = 37
          Width = 191
          Height = 23
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_PAYMENT: TsEdit
          Left = 186
          Top = 157
          Width = 191
          Height = 23
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_NAT: TsEdit
          Left = 538
          Top = 133
          Width = 239
          Height = 23
          Color = clGray
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          Visible = False
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
  end
  inherited sPanel1: TsPanel
    Width = 810
    inherited sSpeedButton5: TsSpeedButton
      Left = 743
    end
    inherited Btn_Close: TsSpeedButton
      Left = 754
    end
    inherited Btn_New: TsSpeedButton
      Visible = True
      OnClick = Btn_NewClick
    end
    inherited Btn_Modify: TsSpeedButton
      Visible = True
      OnClick = Btn_NewClick
    end
    inherited Btn_Del: TsSpeedButton
      OnClick = Btn_NewClick
    end
    inherited Btn_Print: TsSpeedButton
      Left = 518
    end
    inherited Btn_Ready: TsSpeedButton
      Left = 573
      Visible = True
      OnClick = Btn_ReadyClick
    end
    inherited Btn_Save: TsSpeedButton
      Left = 410
      Visible = True
      OnClick = Btn_SaveClick
    end
    inherited Btn_Cancel: TsSpeedButton
      Left = 463
      Visible = True
      OnClick = Btn_CancelClick
    end
    inherited Btn_Temp: TsSpeedButton
      Visible = True
      OnClick = Btn_TempClick
    end
    inherited sSpeedButton26: TsSpeedButton
      Left = 571
      Visible = True
    end
    inherited sSpeedButton27: TsSpeedButton
      Left = 626
      Visible = True
    end
    object sSpeedButton16: TsSpeedButton
      Left = 628
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #51456#48708
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton16Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 22
      Reflected = True
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 56
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterInsert = qryListAfterInsert
    BeforePost = qryListBeforePost
    AfterPost = qryListAfterPost
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT MAINT_NO, DATEE, USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE'#9
      '  ')
    Left = 288
    Top = 200
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      EditMask = '9999.99-9999;0;'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 320
    Top = 200
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPPCR_H '
      'SET CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 288
    Top = 264
  end
  object qryDelete: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterInsert = qryListAfterInsert
    AfterPost = qryListAfterPost
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [LOCAPP]'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 288
    Top = 232
  end
  object qryMakeCodeBank: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,NAME FROM [CODE2NDD]'
      'WHERE Prefix = '#39'APPSPC'#51008#54665#39
      'AND Remark = 1'
      '--ORDER BY [NAME]'
      'ORDER BY CODE')
    Left = 320
    Top = 264
  end
end
