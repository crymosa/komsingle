unit HSCODE_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Mask, DBCtrls, sDBEdit, Grids, DBGrids,
  acDBGrid, StdCtrls, sComboBox, Buttons, sBitBtn, sEdit, sLabel, ExtCtrls,
  sSplitter, sSpeedButton, sPanel, sSkinProvider;

type
  THSCODE_MSSQL_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSplitter2: TsSplitter;
    sLabel1: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sPanel7: TsPanel;
    sEdit1: TsEdit;
    sBitBtn1: TsBitBtn;
    sComboBox1: TsComboBox;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    sSpeedButton7: TsSpeedButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_NewClick(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
    procedure doRefresh;
  public
    { Public declarations }
  end;

var
  HSCODE_MSSQL_frm: THSCODE_MSSQL_frm;

implementation

{$R *.dfm}
uses Dlg_HSCODE, TypeDefine;

procedure THSCODE_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure THSCODE_MSSQL_frm.ReadList;
begin

  with qryList do
  begin

    Close;
    SQL.Text := FSQL;

    case sComboBox1.ItemIndex of
      0 : SQL.Add(' WHERE [CODE] LIKE '+ QuotedStr('%'+sEdit1.Text+'%'));
      1 : SQL.Add(' WHERE [REPNAME] LIKE ' + QuotedStr('%'+sEdit1.Text+'%'));
    end;

    Open;

  end;

end;

procedure THSCODE_MSSQL_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure THSCODE_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure THSCODE_MSSQL_frm.sEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
   if Key = VK_RETURN then ReadList;
end;

procedure THSCODE_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  HSCODE_MSSQL_frm := nil;
end;

procedure THSCODE_MSSQL_frm.Btn_NewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;
  Dlg_HSCODE_frm := TDlg_HSCODE_frm.Create(Self);

  case (Sender as TSpeedButton).Tag of

    0 : TempControl := ctInsert;
    1 : TempControl := ctModify;
    2 : TempControl := ctDelete;

  end;

  try
    if Dlg_HSCODE_frm.Run(TempControl,qryList.Fields) = mrOK then
    begin

        Case TempControl of
          ctInsert,ctDelete :
          begin
            qryList.Close;
            qryList.Open;
          end;

          ctModify :
          begin
            doRefresh;
          end;

        end;
    end;
  finally
     FreeAndNil(Dlg_HSCODE_frm);
  end;


end;

procedure THSCODE_MSSQL_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

procedure THSCODE_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
