inherited PERSON_MSSQL_frm: TPERSON_MSSQL_frm
  Left = 792
  Top = 170
  BorderWidth = 4
  Caption = #49324#50857#51088' '#44288#47532
  ClientHeight = 569
  ClientWidth = 545
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPageControl1: TsPageControl [0]
    Left = 0
    Top = 0
    Width = 545
    Height = 569
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 28
    TabIndex = 0
    TabOrder = 0
    TabPadding = 8
    object sTabSheet1: TsTabSheet
      Caption = #49324#50857#51088' '#47532#49828#53944
      object sPanel1: TsPanel
        Left = 0
        Top = 0
        Width = 537
        Height = 38
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        DesignSize = (
          537
          38)
        object sButton2: TsButton
          Left = 0
          Top = 3
          Width = 105
          Height = 32
          Caption = #49888#44508' '#49324#50857#51088
          TabOrder = 0
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 2
        end
        object sButton3: TsButton
          Left = 106
          Top = 3
          Width = 84
          Height = 32
          Caption = #48708#54876#49457#54868
          TabOrder = 1
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 1
        end
        object sButton4: TsButton
          Left = 453
          Top = 3
          Width = 84
          Height = 32
          Anchors = [akTop, akRight]
          Caption = #49352#47196#44256#52840
          TabOrder = 2
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 31
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 38
        Width = 537
        Height = 493
        Align = alClient
        Color = clGray
        Ctl3D = False
        DataSource = dsPerson
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'USERID'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = 'ID'
            Width = 73
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'NAME'
            Title.Alignment = taCenter
            Title.Caption = #50976#51200#47749
            Width = 140
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'DEPT'
            Title.Alignment = taCenter
            Title.Caption = #48512#49436
            Width = 88
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'ACTIVE'
            Title.Alignment = taCenter
            Title.Caption = #49849#51064#50668#48512
            Width = 60
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'JOIN_DT'
            Title.Alignment = taCenter
            Title.Caption = #44032#51077#51068#49884
            Width = 130
            Visible = True
          end>
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #49324#50857#51088#48324' '#54532#47196#44536#47016' '#44428#54620
      object sPanel2: TsPanel
        Left = 0
        Top = 0
        Width = 537
        Height = 38
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object sComboBox1: TsComboBox
          Left = 56
          Top = 8
          Width = 97
          Height = 22
          BoundLabel.Active = True
          BoundLabel.Caption = 'USER ID'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          Style = csOwnerDrawFixed
          ItemHeight = 16
          ItemIndex = -1
          TabOrder = 0
        end
        object sButton5: TsButton
          Left = 454
          Top = 3
          Width = 83
          Height = 32
          Caption = #51201#50857
          TabOrder = 1
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 17
        end
      end
    end
  end
  object sButton1: TsButton [1]
    Left = 457
    Top = 0
    Width = 88
    Height = 28
    Anchors = [akTop, akRight]
    Caption = #45803#44592
    TabOrder = 1
    OnClick = sButton1Click
    Images = DMICON.System18
    ImageIndex = 18
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 512
    Top = 0
  end
  object qryPerson: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT  USERID, NAME, DEPT, PASSWORD, DEPTCODE, CASE ACTIVATE WH' +
        'EN 0 THEN '#39#49849#51064#45824#44592#39' WHEN 1 THEN '#39#49849#51064#39' END as ACTIVE, JOIN_DT'
      'FROM PERSON')
    Left = 56
    Top = 256
    object qryPersonUSERID: TStringField
      FieldName = 'USERID'
      Size = 15
    end
    object qryPersonNAME: TStringField
      FieldName = 'NAME'
      Size = 10
    end
    object qryPersonDEPT: TStringField
      FieldName = 'DEPT'
      Size = 40
    end
    object qryPersonPASSWORD: TStringField
      FieldName = 'PASSWORD'
      Size = 64
    end
    object qryPersonDEPTCODE: TStringField
      FieldName = 'DEPTCODE'
      Size = 8
    end
    object qryPersonACTIVE: TStringField
      FieldName = 'ACTIVE'
      ReadOnly = True
      Size = 8
    end
    object qryPersonJOIN_DT: TDateTimeField
      Alignment = taCenter
      FieldName = 'JOIN_DT'
      DisplayFormat = 'yyyy-mm-dd hh:nn:ss'
    end
  end
  object dsPerson: TDataSource
    DataSet = qryPerson
    Left = 88
    Top = 256
  end
end
