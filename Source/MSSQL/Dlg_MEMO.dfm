inherited Dlg_MEMO_frm: TDlg_MEMO_frm
  Left = 1100
  Top = 343
  Caption = #47700' '#47784' '#53076' '#46300
  ClientHeight = 340
  ClientWidth = 445
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Visible = False
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 445
    Height = 340
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sBevel1: TsBevel
      Left = -3
      Top = 261
      Width = 452
      Height = 10
      Shape = bsBottomLine
    end
    object sButton2: TsButton
      Left = 145
      Top = 290
      Width = 75
      Height = 29
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 3
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
    object sButton3: TsButton
      Left = 225
      Top = 290
      Width = 75
      Height = 29
      Cancel = True
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 4
      TabStop = False
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 18
    end
    object edt_CODE: TsEdit
      Left = 72
      Top = 16
      Width = 121
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_SUBJ: TsEdit
      Left = 72
      Top = 49
      Width = 121
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 12
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #47749#52845
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Memo_content: TsMemo
      Left = 72
      Top = 82
      Width = 337
      Height = 159
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
      BoundLabel.Active = True
      BoundLabel.Caption = #45236#50857
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeftTop
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'PREFIX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'SUBJECT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'D_MEMO'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [MEMOD]'
      '           ([PREFIX]'
      '            ,[CODE]'
      '           ,[SUBJECT]'
      '           ,[D_MEMO])'
      'VALUES('
      ' :PREFIX'
      ', :CODE'
      ', :SUBJECT'
      ', :D_MEMO'
      ')')
    Left = 264
    Top = 72
  end
  object qryDel: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [MEMOD]'
      'WHERE [CODE] = :CODE')
    Left = 328
    Top = 72
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'PREFIX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUBJECT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'D_MEMO'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [MEMOD]'
      'SET        [PREFIX] = :PREFIX'
      '             ,[SUBJECT] =  :SUBJECT'
      '             ,[D_MEMO] = :D_MEMO'
      'WHERE [CODE] = :CODE')
    Left = 296
    Top = 72
  end
end
