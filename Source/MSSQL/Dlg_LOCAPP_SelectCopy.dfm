inherited Dlg_LOCAPP_SelectCopy_frm: TDlg_LOCAPP_SelectCopy_frm
  Left = 447
  Top = 304
  Caption = '[LOCAPP]'#45236#44397#49888#50857#44060#49444' '#49888#52397#49436' '#48373#49324#54624' '#50896#48376' '#49440#53469
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sPanel7: TsPanel
      inherited sButton1: TsButton
        OnClick = sButton1Click
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATEE'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'USER_ID'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MESSAGE1'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MESSAGE2'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BUSINESS'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO4'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO5'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO6'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO7'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO8'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OFFERNO9'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OPEN_NO'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APP_DATE'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_PRD'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DELIVERY'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXPIRY'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TRANSPRT'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GOODDES'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GOODDES1'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REMARK'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REMARK1'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXNAME1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXNAME2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXNAME3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOCCOPY1'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOCCOPY2'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOCCOPY3'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOCCOPY4'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOCCOPY5'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_ETC'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_ETC1'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC_TYPE'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC_AMT'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC_AMTC'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_DTL'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_NO'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_AMT'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC_AMTC'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOADDATE'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXPDATE'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IM_NAME'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IM_NAME1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IM_NAME2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IM_NAME3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DEST'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISBANK1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISBANK2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAYMENT'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXGOOD'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXGOOD1'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHKNAME1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHKNAME2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHK1'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHK2'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHK3'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRNO'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LCNO'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BSN_HSCODE'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPADDR1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPADDR2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPADDR3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BNFADDR1'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BNFADDR2'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BNFADDR3'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BNFEMAILID'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BNFDOMAIN'
          Width = 249
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CD_PERP'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CD_PERM'
          Width = 74
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    CursorType = ctStatic
    SQL.Strings = (
      '/****** SSMS'#51032' SelectTopNRows '#47749#47161' '#49828#53356#47549#53944' ******/'
      'SELECT [MAINT_NO]'
      '      ,[DATEE]'
      '      ,[USER_ID]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[BUSINESS]'
      '      ,[OFFERNO1]'
      '      ,[OFFERNO2]'
      '      ,[OFFERNO3]'
      '      ,[OFFERNO4]'
      '      ,[OFFERNO5]'
      '      ,[OFFERNO6]'
      '      ,[OFFERNO7]'
      '      ,[OFFERNO8]'
      '      ,[OFFERNO9]'
      '      ,[OPEN_NO]'
      '      ,[APP_DATE]'
      '      ,[DOC_PRD]'
      '      ,[DELIVERY]'
      '      ,[EXPIRY]'
      '      ,[TRANSPRT]'
      '      ,[GOODDES]'
      '      ,[GOODDES1]'
      '      ,[REMARK]'
      '      ,[REMARK1]'
      '      ,[AP_BANK]'
      '      ,[AP_BANK1]'
      '      ,[AP_BANK2]'
      '      ,[APPLIC]'
      '      ,[APPLIC1]'
      '      ,[APPLIC2]'
      '      ,[APPLIC3]'
      '      ,[BENEFC]'
      '      ,[BENEFC1]'
      '      ,[BENEFC2]'
      '      ,[BENEFC3]'
      '      ,[EXNAME1]'
      '      ,[EXNAME2]'
      '      ,[EXNAME3]'
      '      ,[DOCCOPY1]'
      '      ,[DOCCOPY2]'
      '      ,[DOCCOPY3]'
      '      ,[DOCCOPY4]'
      '      ,[DOCCOPY5]'
      '      ,[DOC_ETC]'
      '      ,[DOC_ETC1]'
      '      ,[LOC_TYPE]'
      '      ,[LOC_AMT]'
      '      ,[LOC_AMTC]'
      '      ,[DOC_DTL]'
      '      ,[DOC_NO]'
      '      ,[DOC_AMT]'
      '      ,[DOC_AMTC]'
      '      ,[LOADDATE]'
      '      ,[EXPDATE]'
      '      ,[IM_NAME]'
      '      ,[IM_NAME1]'
      '      ,[IM_NAME2]'
      '      ,[IM_NAME3]'
      '      ,[DEST]'
      '      ,[ISBANK1]'
      '      ,[ISBANK2]'
      '      ,[PAYMENT]'
      '      ,[EXGOOD]'
      '      ,[EXGOOD1]'
      '      ,[CHKNAME1]'
      '      ,[CHKNAME2]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[PRNO]'
      '      ,[LCNO]'
      '      ,[BSN_HSCODE]'
      '      ,[APPADDR1]'
      '      ,[APPADDR2]'
      '      ,[APPADDR3]'
      '      ,[BNFADDR1]'
      '      ,[BNFADDR2]'
      '      ,[BNFADDR3]'
      '      ,[BNFEMAILID]'
      '      ,[BNFDOMAIN]'
      '      ,[CD_PERP]'
      '      ,[CD_PERM]'
      '  FROM [dbo].[LOCAPP]')
    Left = 40
    Top = 8
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
  end
  inherited dsList: TDataSource
    Left = 72
    Top = 8
  end
end
