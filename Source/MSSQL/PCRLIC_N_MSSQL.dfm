inherited PCRLIC_N_MSSQL_frm: TPCRLIC_N_MSSQL_frm
  Left = 695
  Top = 257
  Caption = #50808#54868#54925#46301#50857#50896#47308'('#47932#54408' '#46321') '#44396#47588'('#44277#44553')'#54869#51064#49436'('#49688#49888')'
  ClientHeight = 681
  ClientWidth = 1112
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter2: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1112
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter1: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1112
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Width = 1112
    object sSpeedButton3: TsSpeedButton
      Left = 452
      Top = 3
      Width = 2
      Height = 35
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 183
      Top = 4
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      Spacing = 0
      OnClick = Btn_DelClick
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 27
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 1036
      Top = 4
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      Spacing = 0
      OnClick = Btn_CloseClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 20
      Reflected = True
    end
    object Btn_Print: TsSpeedButton
      Left = 250
      Top = 4
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = ' '#48148#47196#52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      Spacing = 0
      OnClick = Btn_PrintClick
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 21
      Reflected = True
    end
    object sSpeedButton27: TsSpeedButton
      Left = 525
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton2: TsSpeedButton
      Left = 456
      Top = 4
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #50641#49472
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton2Click
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      ImageIndex = 30
      Reflected = True
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 7
      Height = 25
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 4
      Height = 15
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 37
      Height = 13
      Caption = 'PCRLIC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 161
      Height = 17
      Caption = #50808#54868#54925#46301#50857#50896#47308' '#44396#47588#54869#51064#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton4: TsSpeedButton
      Left = 177
      Top = 3
      Width = 2
      Height = 35
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      OnClick = sSpeedButton4Click
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton6: TsSpeedButton
      Tag = 1
      Left = 349
      Top = 4
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = ' '#48120#47532#48372#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      Spacing = 0
      OnClick = Btn_PrintClick
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 15
      Reflected = True
    end
  end
  inherited sPanel3: TsPanel
    Top = 44
    Width = 1112
    SkinData.CustomColor = False
    SkinData.SkinSection = 'PANEL'
    object sDBEdit49: TsDBEdit
      Left = 64
      Top = 5
      Width = 211
      Height = 23
      TabStop = False
      Color = clBtnFace
      DataField = 'MAINT_NO'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = 4013373
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit50: TsDBEdit
      Left = 336
      Top = 5
      Width = 49
      Height = 23
      TabStop = False
      Color = clBtnFace
      DataField = 'USER_ID'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = 4013373
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit63: TsDBEdit
      Left = 440
      Top = 5
      Width = 73
      Height = 23
      TabStop = False
      Color = clBtnFace
      DataField = 'DATEE'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = 4013373
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit64: TsDBEdit
      Left = 568
      Top = 5
      Width = 41
      Height = 23
      TabStop = False
      Color = clBtnFace
      DataField = 'MESSAGE1'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = 4013373
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit65: TsDBEdit
      Left = 664
      Top = 5
      Width = 41
      Height = 23
      TabStop = False
      Color = clBtnFace
      DataField = 'MESSAGE2'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = 4013373
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
  end
  inherited sPageControl1: TsPageControl
    Top = 79
    Width = 1112
    Height = 602
    TabHeight = 30
    TabWidth = 110
    OnChange = sPageControl1Change
    SkinData.CustomColor = True
    inherited sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685
      inherited sSplitter5: TsSplitter
        Left = 246
        Height = 554
      end
      inherited sPanel4: TsPanel
        Width = 246
        Height = 554
      end
      inherited sPanel5: TsPanel
        Left = 250
        Width = 846
        Height = 554
        inherited sSpeedButton1: TsSpeedButton
          Left = 412
          Height = 562
        end
        inherited sBevel3: TsBevel
          Left = 7
          Top = 268
        end
        inherited sDBEdit2: TsDBEdit
          Top = 48
          DataSource = dsList
        end
        inherited sDBEdit3: TsDBEdit
          Top = 70
          DataSource = dsList
        end
        inherited sDBEdit4: TsDBEdit
          Top = 92
          DataField = 'APP_ADDR1'
          DataSource = dsList
        end
        inherited sDBEdit5: TsDBEdit
          Top = 114
          DataField = 'APP_ADDR2'
          DataSource = dsList
        end
        inherited sDBEdit6: TsDBEdit
          Top = 136
          DataField = 'APP_ADDR3'
          DataSource = dsList
        end
        inherited sDBEdit7: TsDBEdit
          Top = 158
          DataSource = dsList
        end
        inherited sDBEdit8: TsDBEdit
          Top = 180
          DataSource = dsList
        end
        inherited sDBEdit9: TsDBEdit
          Top = 202
          DataSource = dsList
        end
        inherited sDBEdit10: TsDBEdit
          Top = 224
          DataField = 'APP_DATE'
          DataSource = dsList
        end
        inherited sDBEdit11: TsDBEdit
          Top = 48
          DataField = 'ChgCd'
          DataSource = dsList
        end
        inherited sEdit17: TsEdit
          Top = 48
        end
        inherited sDBEdit12: TsDBEdit
          Top = 70
          DataField = 'PCrLic_No'
          DataSource = dsList
        end
        inherited sDBEdit13: TsDBEdit
          Top = 92
          DataField = 'BankSend_No'
          DataSource = dsList
        end
        inherited sDBEdit14: TsDBEdit
          Top = 114
          DataField = 'EMAIL_ID'
          DataSource = dsList
        end
        inherited sDBEdit15: TsDBEdit
          Top = 114
          DataField = 'EMAIL_DOMAIN'
          DataSource = dsList
        end
        inherited sDBEdit16: TsDBEdit
          Top = 136
          DataField = 'SUP_NAME1'
          DataSource = dsList
        end
        inherited sDBEdit17: TsDBEdit
          Top = 158
          DataField = 'SUP_NAME2'
          DataSource = dsList
        end
        inherited sDBEdit18: TsDBEdit
          Top = 180
          DataField = 'SUP_ADDR1'
          DataSource = dsList
        end
        inherited sDBEdit19: TsDBEdit
          Top = 202
          DataField = 'SUP_ADDR2'
          DataSource = dsList
        end
        inherited sDBEdit20: TsDBEdit
          Top = 224
          DataField = 'SUP_ADDR3'
          DataSource = dsList
        end
        inherited sPanel8: TsPanel
          Top = 114
        end
        inherited sDBEdit25: TsDBEdit
          Top = 320
          DataField = 'ITM_GBN'
          DataSource = dsList
        end
        inherited sEdit34: TsEdit
          Top = 320
        end
        inherited sDBEdit66: TsDBEdit
          Top = 342
          DataField = 'ITM_GNAME'
          DataSource = dsList
        end
        inherited sDBEdit26: TsDBEdit
          Top = 364
          DataField = 'BK_CD'
          DataSource = dsList
        end
        inherited sDBEdit27: TsDBEdit
          Top = 364
          DataField = 'BK_NAME1'
          DataSource = dsList
        end
        inherited sDBEdit28: TsDBEdit
          Top = 386
          DataField = 'BK_NAME2'
          DataSource = dsList
        end
        inherited sDBEdit29: TsDBEdit
          Top = 408
          DataField = 'BK_NAME3'
          DataSource = dsList
        end
        inherited sDBEdit30: TsDBEdit
          Top = 430
          DataField = 'BK_NAME4'
          DataSource = dsList
        end
        inherited sDBEdit31: TsDBEdit
          Top = 452
          Width = 113
          DataField = 'BK_NAME5'
          DataSource = dsList
        end
        inherited sDBEdit21: TsDBEdit
          Left = 600
          Top = 320
          DataField = 'CHG_G'
          DataSource = dsList
        end
        inherited sEdit28: TsEdit
          Left = 634
          Top = 320
        end
        inherited sDBEdit22: TsDBEdit
          Left = 600
          Top = 342
          DataField = 'C2_AMT'
          DataSource = dsList
        end
        inherited sDBEdit24: TsDBEdit
          Left = 634
          Top = 364
          DataField = 'C1_AMT'
          DataSource = dsList
          BoundLabel.Active = False
          BoundLabel.Caption = 'sDBEdit24'
        end
        inherited sDBEdit48: TsDBEdit
          Left = 600
          Top = 364
          DataField = 'C1_C'
          DataSource = dsList
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52509#54624#51064'/'#54624#51613'/'#48320#46041#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
        end
        object sPanel23: TsPanel
          Left = 8
          Top = 9
          Width = 384
          Height = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49888#52397#51064
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 35
        end
        object sPanel16: TsPanel
          Left = 442
          Top = 9
          Width = 399
          Height = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #48320#44221#54869#51064'/'#44277#44553#51088
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 36
        end
        object sPanel17: TsPanel
          Left = 8
          Top = 281
          Width = 384
          Height = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #54869#51064#44592#44288
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 37
        end
        object sPanel18: TsPanel
          Left = 442
          Top = 281
          Width = 399
          Height = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #48320#44221#54869#51064'/'#44277#44553#51088
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 38
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter3: TsSplitter
        Left = 250
        Height = 562
      end
      inherited sPanel6: TsPanel
        Width = 250
        Height = 562
      end
      inherited sPanel9: TsPanel
        Left = 254
        Width = 850
        Height = 562
        inherited sDBGrid2: TsDBGrid
          Width = 848
          Height = 270
          DataSource = dsDetail
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48264#54840
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44396#47588'('#44277#44553')'#51068#51088
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'LOC_TYPE'
              Title.Alignment = taCenter
              Title.Caption = #49888#50857#51109#51333#47448
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 92
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTYC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT1'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 109
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT1C'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRI2'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 88
              Visible = True
            end
            item
              Alignment = taLeftJustify
              Expanded = False
              FieldName = 'PRI1'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032'(USD)'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ACHG_G'
              Title.Alignment = taCenter
              Title.Caption = #48320#46041#44396#48516
              Width = -1
              Visible = False
            end
            item
              Alignment = taLeftJustify
              Expanded = False
              FieldName = 'AC1_AMT'
              Title.Alignment = taCenter
              Title.Caption = #48320#46041#44552#50529'(USD)'
              Width = -1
              Visible = False
            end>
        end
        inherited sGroupBox5: TsGroupBox
          Top = 271
          Width = 848
          object sSpeedButton19: TsSpeedButton [0]
            Left = 419
            Top = 8
            Width = 10
            Height = 254
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          inherited sDBEdit33: TsDBEdit
            Top = 61
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit36: TsDBEdit
            Left = 317
            Top = 37
            DataSource = dsDetail
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBMemo1: TsDBMemo
            Left = 61
            Top = 85
            Width = 332
            DataSource = dsDetail
          end
          inherited sDBMemo2: TsDBMemo
            Left = 61
            Top = 140
            Width = 332
            DataField = 'SIZE1'
            DataSource = dsDetail
          end
          inherited sDBMemo3: TsDBMemo
            Left = 61
            Top = 195
            Width = 332
            DataField = 'REMARK'
            DataSource = dsDetail
          end
          inherited sDBEdit34: TsDBEdit
            Top = 45
            Color = clWhite
            DataSource = dsDetail
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit35: TsDBEdit
            Top = 69
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit37: TsDBEdit
            Top = 93
            DataSource = dsDetail
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit38: TsDBEdit
            Top = 117
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit39: TsDBEdit
            Top = 141
            Color = clWhite
            DataSource = dsDetail
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit40: TsDBEdit
            Top = 165
            DataSource = dsDetail
          end
          inherited sDBEdit41: TsDBEdit
            Top = 189
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit42: TsDBEdit
            Top = 213
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit43: TsDBEdit
            Top = 45
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit44: TsDBEdit
            Left = 642
            Top = 69
            Width = 143
            DataSource = dsDetail
          end
          inherited sDBEdit45: TsDBEdit
            Left = 642
            Top = 141
            Width = 143
            Color = clWhite
            DataSource = dsDetail
          end
          inherited sDBEdit46: TsDBEdit
            Left = 642
            Top = 189
            Width = 143
            DataSource = dsDetail
          end
          inherited sDBEdit47: TsDBEdit
            Left = 642
            Top = 213
            Width = 143
            DataSource = dsDetail
          end
          inherited sDBEdit1: TsDBEdit
            Left = 317
            Top = 61
            DataField = 'APP_DATE'
            DataSource = dsDetail
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit32: TsDBEdit
            Top = 249
            Color = clWhite
            DataField = 'LOC_TYPE'
            DataSource = dsDetail
            Visible = False
            BoundLabel.Font.Color = clWindowText
          end
          object sPanel22: TsPanel
            Left = 15
            Top = 11
            Width = 389
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49345#54408' '#54408#47749
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 20
          end
          object sPanel20: TsPanel
            Left = 444
            Top = 11
            Width = 389
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49688#47049'/'#45800#44032'/'#44552#50529
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 21
          end
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      Caption = #44592#53440#49324#54637
      inherited sSplitter4: TsSplitter
        Left = 250
        Height = 562
      end
      inherited sPanel10: TsPanel
        Width = 250
        Height = 562
      end
      inherited sPanel11: TsPanel
        Left = 254
        Width = 850
        Height = 562
        inherited sDBMemo4: TsDBMemo
          Left = 86
          Top = 58
          Width = 683
          DataField = 'LIC_INFO'
          DataSource = dsList
          ScrollBars = ssVertical
          BoundLabel.Active = False
          BoundLabel.Caption = ''
          BoundLabel.Layout = sclLeft
        end
        inherited sDBEdit51: TsDBEdit
          Left = 134
          Top = 266
          Color = clWhite
          DataField = 'TQTYC'
          DataSource = dsList
        end
        inherited sDBEdit69: TsDBEdit
          Left = 179
          Top = 266
          Width = 143
          DataField = 'TQTY'
          DataSource = dsList
        end
        inherited sDBEdit52: TsDBEdit
          Left = 134
          Top = 290
          Color = clWhite
          DataField = 'TAMT1C'
          DataSource = dsList
        end
        inherited sDBEdit71: TsDBEdit
          Left = 179
          Top = 290
          Width = 143
          DataField = 'TAMT1'
          DataSource = dsList
        end
        inherited sDBEdit59: TsDBEdit
          Left = 486
          Top = 266
          DataField = 'TAMT2'
          DataSource = dsList
        end
        inherited sDBEdit58: TsDBEdit
          Left = 486
          Top = 290
          DataField = 'LIC_NO'
          DataSource = dsList
        end
        object sPanel32: TsPanel
          Left = 28
          Top = 22
          Width = 796
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #48156#44553#51312#44148#46321' '#44592#53440#49324#54637
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 7
        end
        object sPanel19: TsPanel
          Left = 28
          Top = 230
          Width = 796
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #52509#54633#44228' / '#44396#47588#54869#51064#49436' '#48264#54840
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 8
        end
      end
    end
    inherited sTabSheet5: TsTabSheet
      inherited sSplitter6: TsSplitter
        Left = 250
        Height = 562
      end
      inherited sPanel12: TsPanel
        Width = 250
        Height = 562
      end
      inherited sPanel13: TsPanel
        Left = 254
        Width = 850
        Height = 562
        inherited sDBGrid4: TsDBGrid
          Width = 848
          Height = 234
          DataSource = dsTax
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Caption = #51089#49457#51068#51088
              Width = 77
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Caption = #49464#44552#44228#49328#49436#48264#54840
              Width = 224
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME1'
              Title.Caption = #54408#47785'1'
              Width = 172
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME2'
              Title.Caption = #54408#47785'2'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME3'
              Title.Caption = #54408#47785'3'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME4'
              Title.Caption = #54408#47785'4'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME5'
              Title.Caption = #54408#47785'5'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Caption = #44277#44553#44032#50529
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Caption = #49464#50529
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTITY'
              Title.Caption = #49688#47049
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTITY_UNIT'
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end>
        end
        inherited sGroupBox7: TsGroupBox
          Top = 235
          Width = 848
          inherited sDBEdit53: TsDBEdit
            Top = 51
            Height = 23
            DataField = 'BILL_NO'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
          end
          inherited sDBMemo7: TsDBMemo
            Top = 219
            DataField = 'ITEM_DESC'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit55: TsDBEdit
            Left = 626
            Top = 51
            Height = 23
            DataField = 'BILL_AMOUNT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = False
            BoundLabel.Caption = ''
          end
          inherited sDBEdit60: TsDBEdit
            Left = 584
            Top = 51
            Height = 23
            Color = clWhite
            DataField = 'BILL_AMOUNT_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
          end
          inherited sDBEdit68: TsDBEdit
            Top = 99
            Height = 23
            DataField = 'ITEM_NAME1'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
          end
          inherited sDBEdit54: TsDBEdit
            Top = 123
            Height = 23
            DataField = 'ITEM_NAME2'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit62: TsDBEdit
            Top = 147
            Height = 23
            DataField = 'ITEM_NAME3'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit70: TsDBEdit
            Top = 171
            Height = 23
            DataField = 'ITEM_NAME4'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit56: TsDBEdit
            Left = 626
            Top = 75
            Height = 23
            DataField = 'TAX_AMOUNT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = False
            BoundLabel.Caption = 'sDBEdit56'
          end
          inherited sDBEdit57: TsDBEdit
            Left = 584
            Top = 75
            Height = 23
            Color = clWhite
            DataField = 'TAX_AMOUNT_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
          end
          inherited sDBEdit61: TsDBEdit
            Left = 626
            Top = 99
            Height = 23
            DataField = 'QUANTITY'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = False
            BoundLabel.Caption = 'sDBEdit61'
          end
          inherited sDBEdit67: TsDBEdit
            Left = 584
            Top = 99
            Height = 23
            Color = clWhite
            DataField = 'QUANTITY_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
          end
          inherited sDBEdit23: TsDBEdit
            Top = 195
            Height = 23
            DataField = 'ITEM_NAME5'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit72: TsDBEdit
            Top = 75
            Height = 23
            DataField = 'BILL_DATE'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ParentCtl3D = False
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
          end
          object sPanel25: TsPanel
            Left = 12
            Top = 14
            Width = 821
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49464#44552#44228#49328#49436
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 14
          end
        end
      end
    end
    inherited sTabSheet1: TsTabSheet
      inherited sDBGrid1: TsDBGrid [0]
        Top = 33
        Width = 1104
        Height = 529
        Color = clWhite
        DataSource = dsList
        Font.Name = #47569#51008' '#44256#46357
        TitleFont.Name = #47569#51008' '#44256#46357
        OnDrawColumnCell = sDBGrid3DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 154
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LIC_NO'
            Title.Alignment = taCenter
            Title.Caption = #48156#44553#48264#54840
            Width = 115
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BankSend_No'
            Title.Alignment = taCenter
            Title.Caption = #49688#49888#44288#47532#48264#54840
            Width = 115
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUP_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#51088
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APP_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#49888#52397#51064
            Width = 119
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TQTY'
            Title.Alignment = taCenter
            Title.Caption = #52509#49688#47049
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TQTYC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 42
            Visible = True
          end>
      end
      inherited sPanel7: TsPanel [1]
        Width = 1104
        Height = 33
        SkinData.CustomColor = False
        inherited sEdit3: TsEdit [0]
          Left = 90
          Width = 219
          Visible = False
        end
        inherited sComboBox5: TsComboBox
          Left = 8
          Width = 81
          BoundLabel.Active = False
          BoundLabel.Caption = 'sComboBox5'
          Text = #46321#47197#51068#51088
          OnSelect = sComboBox5Select
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #44277#44553#51088)
        end
        inherited sMaskEdit6: TsMaskEdit [2]
          Left = 90
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited sMaskEdit7: TsMaskEdit [3]
          Left = 212
          BoundLabel.Active = False
          BoundLabel.Caption = 'sMaskEdit7'
        end
        object sButton1: TsButton
          Left = 309
          Top = 6
          Width = 75
          Height = 20
          Caption = #44160#49353
          TabOrder = 4
          OnClick = sButton1Click
          SkinData.SkinSection = 'BUTTON'
          Images = DMICON.System18
          ImageIndex = 6
        end
        object sBitBtn24: TsBitBtn
          Tag = 1
          Left = 163
          Top = 6
          Width = 24
          Height = 20
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn25: TsBitBtn
          Tag = 2
          Left = 285
          Top = 6
          Width = 24
          Height = 20
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel26: TsPanel
          Left = 187
          Top = 6
          Width = 25
          Height = 20
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
    end
  end
  object sPanel2: TsPanel [5]
    Left = 612
    Top = 8
    Width = 497
    Height = 89
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    Visible = False
    object sLabel1: TsLabel
      Left = 171
      Top = 45
      Width = 24
      Height = 12
      Caption = #48512#53552
    end
    object sLabel2: TsLabel
      Left = 299
      Top = 45
      Width = 24
      Height = 12
      Caption = #44620#51648
    end
    object sLabel3: TsLabel
      Left = 16
      Top = 8
      Width = 64
      Height = 21
      Caption = #50641#49472#52636#47141
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sDateEdit1: TsDateEdit
      Left = 72
      Top = 40
      Width = 97
      Height = 25
      AutoSize = False
      EditMask = '!9999/99/99;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '2015-04-24'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      Date = 42118
    end
    object sDateEdit2: TsDateEdit
      Left = 200
      Top = 40
      Width = 97
      Height = 25
      AutoSize = False
      EditMask = '!9999/99/99;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '2015-04-24'
      BoundLabel.Caption = 'sDateEdit2'
      SkinData.SkinSection = 'EDIT'
      Date = 42118
    end
    object sButton2: TsButton
      Left = 336
      Top = 24
      Width = 145
      Height = 25
      Caption = #50641#49472#52636#47141
      TabOrder = 2
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton3: TsButton
      Left = 336
      Top = 49
      Width = 145
      Height = 25
      Caption = #45803#44592
      TabOrder = 3
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPanel14: TsPanel [6]
    Left = 4
    Top = 118
    Width = 251
    Height = 560
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 4
    object sPanel15: TsPanel
      Left = 1
      Top = 1
      Width = 249
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      
      TabOrder = 0
      DesignSize = (
        249
        33)
      object Mask_fromDate: TsMaskEdit
        Left = 71
        Top = 5
        Width = 74
        Height = 23
        Anchors = [akTop, akRight]
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object Mask_toDate: TsMaskEdit
        Left = 146
        Top = 5
        Width = 74
        Height = 23
        Anchors = [akTop, akRight]
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 221
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object sDBGrid3: TsDBGrid
      Left = 1
      Top = 34
      Width = 249
      Height = 525
      Align = alClient
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid3DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 71
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 155
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 296
    Top = 232
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[DATEE]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[APP_CODE]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_ADDR1]'
      '      ,[APP_ADDR2]'
      '      ,[APP_ADDR3]'
      '      ,[AX_NAME1]'
      '      ,[AX_NAME2]'
      '      ,[AX_NAME3]'
      '      ,[SUP_CODE]'
      '      ,[SUP_NAME1]'
      '      ,[SUP_NAME2]'
      '      ,[SUP_NAME3]'
      '      ,[SUP_ADDR1]'
      '      ,[SUP_ADDR2]'
      '      ,[SUP_ADDR3]'
      '      ,[ITM_G1]'
      '      ,[ITM_G2]'
      '      ,[ITM_G3]'
      '      ,[TQTY]'
      '      ,[TQTYC]'
      '      ,[TAMT1]'
      '      ,[TAMT1C]'
      '      ,[TAMT2]'
      '      ,[TAMT2C]'
      '      ,[CHG_G]'
      '      ,[C1_AMT]'
      '      ,[C1_C]'
      '      ,[C2_AMT]'
      '      ,[LIC_INFO]'
      '      ,[LIC_NO]'
      '      ,[BK_CD]'
      '      ,[BK_NAME1]'
      '      ,[BK_NAME2]'
      '      ,[BK_NAME3]'
      '      ,[BK_NAME4]'
      '      ,[BK_NAME5]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[Comf_dat]'
      '      ,[PRNO]'
      '      ,[PCrLic_No]'
      '      ,[ChgCd]'
      '      ,[BankSend_No]'
      '      ,[APP_DATE]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_CD]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[F_INTERFACE]'
      '      ,[ITM_GBN]'
      '      ,[ITM_GNAME]'
      '      ,[ISSUE_GBN]'
      '      ,[DOC_GBN]'
      '      ,[DOC_NO]'
      '      ,[BAMT2]'
      '      ,[BAMTC2]'
      '      ,[BAMT3]'
      '      ,[BAMTC3]'
      '      ,[PCRLIC_ISS_NO]'
      '      ,[BK_CD2]'
      '      ,[BAL_CD2]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '  FROM [PCRLIC_H]')
    Left = 8
    Top = 416
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      OnGetText = qryListDATEEGetText
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 10
    end
    object qryListSUP_CODE: TStringField
      FieldName = 'SUP_CODE'
      Size = 10
    end
    object qryListSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryListSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryListSUP_NAME3: TStringField
      FieldName = 'SUP_NAME3'
      Size = 35
    end
    object qryListSUP_ADDR1: TStringField
      FieldName = 'SUP_ADDR1'
      Size = 35
    end
    object qryListSUP_ADDR2: TStringField
      FieldName = 'SUP_ADDR2'
      Size = 35
    end
    object qryListSUP_ADDR3: TStringField
      FieldName = 'SUP_ADDR3'
      Size = 35
    end
    object qryListITM_G1: TStringField
      FieldName = 'ITM_G1'
      Size = 3
    end
    object qryListITM_G2: TStringField
      FieldName = 'ITM_G2'
      Size = 3
    end
    object qryListITM_G3: TStringField
      FieldName = 'ITM_G3'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListTAMT1: TBCDField
      FieldName = 'TAMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryListTAMT2: TBCDField
      FieldName = 'TAMT2'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTAMT2C: TStringField
      FieldName = 'TAMT2C'
      Size = 3
    end
    object qryListCHG_G: TStringField
      FieldName = 'CHG_G'
      Size = 3
    end
    object qryListC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryListC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryListC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryListLIC_INFO: TMemoField
      FieldName = 'LIC_INFO'
      BlobType = ftMemo
    end
    object qryListLIC_NO: TStringField
      FieldName = 'LIC_NO'
      Size = 35
    end
    object qryListBK_CD: TStringField
      FieldName = 'BK_CD'
      Size = 4
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 70
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 70
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListBK_NAME4: TStringField
      FieldName = 'BK_NAME4'
      Size = 35
    end
    object qryListBK_NAME5: TStringField
      FieldName = 'BK_NAME5'
      Size = 10
    end
    object qryListAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryListAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryListAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryListComf_dat: TStringField
      FieldName = 'Comf_dat'
      Size = 8
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListPCrLic_No: TStringField
      FieldName = 'PCrLic_No'
      Size = 35
    end
    object qryListChgCd: TStringField
      FieldName = 'ChgCd'
      Size = 3
    end
    object qryListBankSend_No: TStringField
      FieldName = 'BankSend_No'
      Size = 35
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryListDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryListBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryListBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryListBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryListBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryListBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryListEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryListSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryListBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryListBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 4
    end
    object qryListBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryListBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryListITM_GNAME: TStringField
      FieldName = 'ITM_GNAME'
      Size = 70
    end
    object qryListISSUE_GBN: TStringField
      FieldName = 'ISSUE_GBN'
      Size = 3
    end
    object qryListDOC_GBN: TStringField
      FieldName = 'DOC_GBN'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListBAMT2: TBCDField
      FieldName = 'BAMT2'
      Precision = 18
    end
    object qryListBAMTC2: TStringField
      FieldName = 'BAMTC2'
      Size = 3
    end
    object qryListBAMT3: TBCDField
      FieldName = 'BAMT3'
      Precision = 18
    end
    object qryListBAMTC3: TStringField
      FieldName = 'BAMTC3'
      Size = 3
    end
    object qryListPCRLIC_ISS_NO: TStringField
      FieldName = 'PCRLIC_ISS_NO'
      Size = 35
    end
    object qryListBK_CD2: TStringField
      FieldName = 'BK_CD2'
      Size = 6
    end
    object qryListBAL_CD2: TStringField
      FieldName = 'BAL_CD2'
      Size = 6
    end
    object qryListEMAIL_ID: TStringField
      FieldName = 'EMAIL_ID'
      Size = 35
    end
    object qryListEMAIL_DOMAIN: TStringField
      FieldName = 'EMAIL_DOMAIN'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 416
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'RG510TD2'
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[HS_NO]'
      '      ,[NAME]'
      '      ,[NAME1]'
      '      ,[SIZE]'
      '      ,[SIZE1]'
      '      ,[REMARK]'
      '      ,[QTY]'
      '      ,[QTYC]'
      '      ,[AMT1]'
      '      ,[AMT1C]'
      '      ,[AMT2]'
      '      ,[PRI1]'
      '      ,[PRI2]'
      '      ,[PRI_BASE]'
      '      ,[PRI_BASEC]'
      '      ,[ACHG_G]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[LOC_TYPE]'
      '      ,[NAMESS]'
      '      ,[SIZESS]'
      '      ,[APP_DATE]'
      '      ,[ITM_NUM]'
      '  FROM [PCRLICD1]'
      '  WHERE KEYY = :KEYY')
    Left = 8
    Top = 448
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '9999.99-9999;0l'
      Size = 10
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryDetailNAME1GetText
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailREMARK: TMemoField
      FieldName = 'REMARK'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryDetailAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailAMT2: TBCDField
      FieldName = 'AMT2'
      Precision = 18
    end
    object qryDetailPRI1: TBCDField
      FieldName = 'PRI1'
      Precision = 18
    end
    object qryDetailPRI2: TBCDField
      FieldName = 'PRI2'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailPRI_BASE: TBCDField
      FieldName = 'PRI_BASE'
      Precision = 18
    end
    object qryDetailPRI_BASEC: TStringField
      FieldName = 'PRI_BASEC'
      Size = 3
    end
    object qryDetailACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryDetailAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryDetailAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryDetailAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryDetailLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryDetailNAMESS: TStringField
      FieldName = 'NAMESS'
    end
    object qryDetailSIZESS: TStringField
      FieldName = 'SIZESS'
    end
    object qryDetailAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryDetailITM_NUM: TStringField
      FieldName = 'ITM_NUM'
      Size = 35
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 40
    Top = 448
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = '201312-DRUM12(R2)-1'
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[DATEE]'
      '      ,[BILL_NO]'
      '      ,[BILL_DATE]'
      '      ,[ITEM_NAME1]'
      '      ,[ITEM_NAME2]'
      '      ,[ITEM_NAME3]'
      '      ,[ITEM_NAME4]'
      '      ,[ITEM_NAME5]'
      '      ,[ITEM_DESC]'
      '      ,[BILL_AMOUNT]'
      '      ,[BILL_AMOUNT_UNIT]'
      '      ,[TAX_AMOUNT]'
      '      ,[TAX_AMOUNT_UNIT]'
      '      ,[QUANTITY]'
      '      ,[QUANTITY_UNIT]'
      '  FROM [PCRLIC_TAX]'
      '  WHERE KEYY = :KEYY')
    Left = 8
    Top = 480
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryTaxDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryTaxITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      OnGetText = qryTaxITEM_DESCGetText
      BlobType = ftMemo
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryTaxQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 40
    Top = 480
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = A4
    Left = 264
    Top = 232
  end
end
