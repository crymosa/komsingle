unit CustomerAdminMSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CustomerAdmin, sSkinProvider, Grids, DBGrids, acDBGrid, Mask,
  DBCtrls, sDBEdit, StdCtrls, sComboBox, sEdit, Buttons, sSpeedButton,
  ExtCtrls, sPanel, sSplitter, DB, ADODB, TypeDefine, sButton, DBTables;

type
  TCustomerAdminMSSQL_frm = class(TCustomerAdmin_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    qryCopy: TADOQuery;
    sDBEdit23: TsDBEdit;
    sButton1: TsButton;
    Query1: TQuery;
    btnExit: TsButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edt_FindKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure Btn_NewClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentWork : TProgramControlType;
  protected
    procedure ReadData; override;
    procedure doRefresh;  
  public
    { Public declarations }
  end;

var
  CustomerAdminMSSQL_frm: TCustomerAdminMSSQL_frm;

implementation

uses MSSQL, dlg_CustomerReg, dlg_FindNation, MessageDefine, ADV700, ICON;

{$R *.dfm}

procedure TCustomerAdminMSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  CustomerAdminMSSQL_frm := nil;
end;

procedure TCustomerAdminMSSQL_frm.ReadData;
var
  TempStr : String;
begin
  inherited;
  with qryList do
  begin
    Close;
    SQL.Text := qryCopy.SQL.Text;

    IF edt_Find.Text = MSG_INPUTAFTERENTER Then Tempstr := ''
    else TempStr := edt_Find.Text;

    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+TempStr+'%'));
      1: SQL.Add('WHERE [ENAME] LIKE '+QuotedStr('%'+TempStr+'%'));
    end;
    
    Open;

    if FCurrentWork = ctInsert Then Last;
  end;
end;

procedure TCustomerAdminMSSQL_frm.edt_FindKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadData;
end;

procedure TCustomerAdminMSSQL_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  ShowScrollbar(sDBGrid1.handle,SB_VERT,True);
  inherited;
end;

procedure TCustomerAdminMSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  FCurrentWork := ctView;
  ReadData;
end;

procedure TCustomerAdminMSSQL_frm.Btn_NewClick(Sender: TObject);
var
  Result : TModalResult;
begin
  inherited;
  dlg_CustomerReg_frm := Tdlg_CustomerReg_frm.Create(Self);
  try
    Case (Sender as TsSpeedButton).Tag of
      0: FCurrentWork := ctInsert;
      1: FCurrentWork := ctModify;
      2: FCurrentWork := ctDelete;
    end;
    IF FCurrentWork = ctDelete Then
    begin
      Result := dlg_CustomerReg_frm.DeleteData(qryList.FieldByName('CODE').AsString);
    end
    else
    begin
      Result := dlg_CustomerReg_frm.Run(FCurrentWork,qryList.Fields);
    end;
  finally
    FreeAndNil(dlg_CustomerReg_frm);
  end;

  IF Result = mrOK then
  begin
    Case FCurrentWork of
      ctModify : doRefresh;
      ctInsert,ctDelete : ReadData;
    end;
  end;

end;

procedure TCustomerAdminMSSQL_frm.doRefresh;
var
  Bmk : TBookmark;
begin
  qryList.UpdateCursorPos;
  Bmk := qryList.GetBookmark;
  qryList.DisableControls;

  try
    qryList.Close;
    qryList.Open;

    if qryList.BookmarkValid(Bmk) then
    begin
      qryList.GotoBookmark(Bmk);
    end;

  finally
    qryList.EnableControls;
    qryList.FreeBookmark(Bmk);
  end;
end;

procedure TCustomerAdminMSSQL_frm.sButton1Click(Sender: TObject);
var
  i : integer;
  sMSG : String;
begin
  inherited;
  IF MessageBox(Self.Handle, '가져오기를 진행하면 현재 저장된 거래처는 삭제됩니다. 진행하시겠습니까?', '가져오기 실행', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;


  with TADOQuery.Create(nil) do
  begin
    try
      try
        Connection := DMMssql.KISConnect;
        DMMssql.BeginTrans;
        SQL.Text := 'DELETE FROM CUSTOM';
        ExecSQL;

        Query1.DatabaseName := 'KOM_SINGLE';
        Query1.SQL.Text := 'SELECT CODE, TRAD_NO, SAUP_NO, ENAME, REP_NAME, REP_TEL, REP_FAX, DDAN_DEPT, DDAN_NAME, DDAN_TEL, DDAN_FAX, ZIPCD, ADDR1, ADDR2, ADDR3, CITY, NAT, ESU1, ESU2, ESU3, Jenja, ADDR4, ADDR5, EMAIL_ID, EMAIL_DOMAIN FROM CUSTOM';
        Query1.Open;

        while not Query1.Eof do
        begin
          SQL.Text := 'INSERT INTO CUSTOM(CODE, TRAD_NO, SAUP_NO, ENAME, REP_NAME, REP_TEL, REP_FAX, DDAN_DEPT, DDAN_NAME, DDAN_TEL, DDAN_FAX, ZIPCD, ADDR1, ADDR2, ADDR3, CITY, NAT, ESU1, ESU2, ESU3, Jenja, ADDR4, ADDR5, EMAIL_ID, EMAIL_DOMAIN)'#13#10+
                      'VALUES(';
          for i := 0 to Query1.FieldCount-1 do
          begin
            IF i = Query1.FieldCount-1 Then
              SQL.Text := SQL.Text + QuotedStr(Query1.Fields[i].AsString)
            else
              SQL.Text := SQL.Text + QuotedStr(Query1.Fields[i].AsString)+', ';
          end;

          SQL.Text := SQL.Text + ')';
          ExecSQL;
          Query1.Next;
        end;

        DMMssql.CommitTrans;
        qryList.Close;
        qryList.Open;
        ShowMessage('전환이 완료되었습니다');

      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
          DMMssql.RollbackTrans;
        end;
      end;
    finally
      Query1.Close;
      Close;
      Free;
    end;
  end;
end;

procedure TCustomerAdminMSSQL_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
