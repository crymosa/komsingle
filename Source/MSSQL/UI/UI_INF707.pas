unit UI_INF707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, UI_APP707_BP, DB, ADODB, Grids,
  DBGrids, acDBGrid, StdCtrls, sComboBox, ExtCtrls, sSplitter, sMemo,
  sLabel, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, sCheckBox, Buttons, sBitBtn, Mask, sMaskEdit, sEdit,
  sButton, sSpeedButton, sPanel, sBevel;

type
  TUI_INF707_frm = class(TUI_APP707_BP_frm)
    sButton4: TsButton;
    sButton2: TsButton;
    sPanel21: TsPanel;
    edt_RcvRef: TsEdit;
    edt_AmdDate: TsMaskEdit;
    Page4: TsTabSheet;
    sPanel10: TsPanel;
    page4_RightPanel: TsPanel;
    sPanel32: TsPanel;
    edt_lstDate: TsMaskEdit;
    sPanel33: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    sPanel22: TsPanel;
    memo_Narrat1: TsMemo;
    sPanel37: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel38: TsPanel;
    edt_dochackPort: TsEdit;
    sLabel24: TsLabel;
    sPanel39: TsPanel;
    edt_loadOn: TsEdit;
    sLabel27: TsLabel;
    sPanel40: TsPanel;
    edt_forTran: TsEdit;
    sPanel23: TsPanel;
    edt_Aacv1: TsEdit;
    edt_Aacv2: TsEdit;
    edt_Aacv3: TsEdit;
    edt_Aacv4: TsEdit;
    sPanel24: TsPanel;
    edt_SrInfo1: TsEdit;
    edt_SrInfo2: TsEdit;
    edt_SrInfo3: TsEdit;
    edt_SrInfo4: TsEdit;
    edt_SrInfo5: TsEdit;
    edt_SrInfo6: TsEdit;
    sPanel26: TsPanel;
    edt_OpAddr3: TsEdit;
    edt_OpAddr2: TsEdit;
    edt_OpAddr1: TsEdit;
    edt_OpBank3: TsEdit;
    edt_OpBank2: TsEdit;
    edt_OpBank1: TsEdit;
    sPanel27: TsPanel;
    edt_IssBank1: TsEdit;
    edt_IssBank2: TsEdit;
    edt_IssBank3: TsEdit;
    edt_IssBank4: TsEdit;
    edt_IssBank5: TsEdit;
    edt_IssBank6: TsEdit;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListIBANK_REF: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListPrno: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListMSEQ_1: TIntegerField;
    qryListAMD_NO_1: TIntegerField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListOP_BANK1: TStringField;
    qryListOP_BANK2: TStringField;
    qryListOP_BANK3: TStringField;
    qryListOP_ADDR1: TStringField;
    qryListOP_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCDMAX_Name: TStringField;
    sSpeedButton1: TsSpeedButton;
    sSplitter7: TsSplitter;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sLabel46: TsLabel;
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    INF707_SQL : string;

    procedure Readlist(fromDate, toDate : String;KeyValue : String='');overload;
    procedure readDocument; override;
    procedure DeleteDocument; override;
    procedure Readlist(OrderSyntax : string = ''); overload;
    
  public
    { Public declarations }
  end;

var
  UI_INF707_frm: TUI_INF707_frm;

implementation

uses Commonlib, TypeDefine, DateUtils, StrUtils, MessageDefine, MSSQL,
  KISCalendar, INF707_PRINT;

{$R *.dfm}

procedure TUI_INF707_frm.FormShow(Sender: TObject);
begin
  inherited;
  Self.Caption := '취소불능화환 신용장 변경 응답서';
  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;

  //ReadOnlyControlValue(maintno_Panel);
  EnabledControlValue(maintno_Panel);
  EnabledControlValue(page1_RightPanel);
  EnabledControlValue(page2_RightPanel);
  EnabledControlValue(page3_RightPanel);
  EnabledControlValue(page4_RightPanel);

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

end;                    

procedure TUI_INF707_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_INF707_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_INF707_frm := nil;
end;

procedure TUI_INF707_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //maintno_Panel.Visible := not (sPageControl1.ActivePageIndex = 4);
  sPanel52.Visible := not (sPageControl1.ActivePageIndex = 4); 
end;

procedure TUI_INF707_frm.FormCreate(Sender: TObject);
begin
  inherited;
  INF707_SQL := Self.qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
  ProgramControlType := ctView;
end;

procedure TUI_INF707_frm.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;
  if not qryList.Active then Exit;
  
  if not DMMssql.KISConnect.InTransaction then
  begin
    KeyValue := qryListMAINT_NO.AsString;
    qryList.Close;
    qryList.Open;
    qryList.Locate('MAINT_NO',KeyValue,[]);
  end;
end;

procedure TUI_INF707_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    sql.Text := INF707_SQL;
    SQL.Add(' WHERE I707_1.DATEE between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    sql.Add(' ORDER BY I707_1.DATEE ');
    Open;

    if Trim(KeyValue) <> '' then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;

  end;
end;

procedure TUI_INF707_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
end;

procedure TUI_INF707_frm.readDocument;
begin
  inherited;
  if not qryList.Active then Exit;

  if qryList.RecordCount = 0 then Exit;

//------------------------------------------------------------------------------
//기본정보
//------------------------------------------------------------------------------
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //유저번호
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //관리번호 옆 차수 = 조건변경횟수
  edt_chasu.Text := qryListMSEQ.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;

//------------------------------------------------------------------------------
//개설은행
//------------------------------------------------------------------------------
  //신청일자
  edt_APPDATE.Text := qryListAPP_DATE.AsString;
  //개설방법
  edt_In_Mathod.Text := qryListIN_MATHOD.AsString;
  edt_In_Mathod1.Text := qryListmathod_Name.AsString;

  //개설의뢰은행
  edt_ApBank.Text := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString;
  edt_ApBank2.Text := qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString;
  edt_ApBank4.Text := qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;

//------------------------------------------------------------------------------
//통지은행
//------------------------------------------------------------------------------
  //(희망)통지은행
  edt_AdBank1.Text := qryListAD_BANK1.AsString;
  edt_AdBank2.Text := qryListAD_BANK2.AsString;
  edt_AdBank3.Text := qryListAD_BANK3.AsString;
  edt_AdBank4.Text := qryListAD_BANK4.AsString;
  //신용공여 사용되지않음 
  //edt_AdPay.Text := qryListAD_PAY.AsString;

//------------------------------------------------------------------------------
//수입용도
//------------------------------------------------------------------------------
  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd1_1.Text := qryListImp_Name_1.AsString;
  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd2_1.Text := qryListImp_Name_2.AsString;
  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd3_1.Text := qryListImp_Name_3.AsString;
  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd4_1.Text := qryListImp_Name_4.AsString;
  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd5_1.Text := qryListImp_Name_5.AsString;

//------------------------------------------------------------------------------
// I/L번호
//------------------------------------------------------------------------------
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILno5.Text := qryListIL_NO5.AsString;

  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;

  edt_ILAMT1.Value := qryListIL_AMT1.AsCurrency;
  edt_ILAMT2.Value := qryListIL_AMT2.AsCurrency;
  edt_ILAMT3.Value := qryListIL_AMT3.AsCurrency;
  edt_ILAMT4.Value := qryListIL_AMT4.AsCurrency;
  edt_ILAMT5.Value := qryListIL_AMT5.AsCurrency;

//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;

//------------------------------------------------------------------------------
// 명의인정보
//------------------------------------------------------------------------------
  //명의인
  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  //식별부호
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  //주소
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

  //Sender's Reference(L/C No)
  edt_cdNo.Text := qryListCD_NO.AsString;
  //Receive's Reference(L/C No)
  edt_RcvRef.Text := qryListRCV_REF.AsString;
  //Date of Issue
  edt_IssDate.Text := qryListISS_DATE.AsString;
  //Date / Number Of Amendment
  edt_AmdDate.Text := qryListAMD_DATE.AsString;
  edt_AmdNo.Value := qryListAMD_NO.AsCurrency;

  //Applicatn
  edt_Applic1.Text := qryListAPPLIC1.AsString;
  edt_Applic2.Text := qryListAPPLIC2.AsString;
  edt_Applic3.Text := qryListAPPLIC3.AsString;
  edt_Applic4.Text := qryListAPPLIC4.AsString;
  edt_Applic5.Text := qryListAPPLIC5.AsString;

  //Beneficiary
  edt_Benefc1.Text := qryListBENEFC1.AsString;
  edt_Benefc2.Text := qryListBENEFC2.AsString;
  edt_Benefc3.Text := qryListBENEFC3.AsString;
  edt_Benefc4.Text := qryListBENEFC4.AsString;
  edt_Benefc5.Text := qryListBENEFC5.AsString;

  //New Date of Expiry
  edt_exDate.Text := qryListEX_DATE.AsString;

  //Increase of Documentary Credit Amount
  edt_IncdCur.Text := qryListINCD_CUR.AsString;
  edt_IncdAmt.Value := qryListINCD_AMT.AsCurrency;

  //Decrease of Documentary Credit Amount
  edt_DecdCur.Text := qryListDECD_CUR.AsString;
  edt_DecdAmt.Value := qryListDECD_AMT.AsCurrency;

  //New Documentary CreditAmount after Amendment
  edt_NwcdCur.Text := qryListNWCD_CUR.AsString;
  edt_NwcdAmt.Value := qryListNWCD_AMT.AsCurrency;

  //New Documentary CreditAmount before Amendment
  edt_BfcdCur.Text := qryListBFCD_CUR.AsString;
  edt_BfcdAmt.Value := qryListBFCD_AMT.AsCurrency;

  //Maximum Credit Amount
  edt_CdMax.Text := qryListCD_MAX.AsString;
  edt_CdMax1.Text := qryListCDMAX_Name.AsString;

  //Percentage Credit Amount Tolerance
  edt_CdPerp.Value := qryListCD_PERP.AsCurrency;
  edt_CdPerm.Value := qryListCD_PERM.AsCurrency;

  //Additional Amounts Covered
  edt_Aacv1.Text := qryListAA_CV1.AsString;
  edt_Aacv2.Text := qryListAA_CV2.AsString;
  edt_Aacv3.Text := qryListAA_CV2.AsString;
  edt_Aacv4.Text := qryListAA_CV4.AsString;

  //Sender to Receive Information
  edt_SrInfo1.Text := qryListSR_INFO1.AsString;
  edt_SrInfo2.Text := qryListSR_INFO2.AsString;
  edt_SrInfo3.Text := qryListSR_INFO3.AsString;
  edt_SrInfo4.Text := qryListSR_INFO4.AsString;
  edt_SrInfo5.Text := qryListSR_INFO5.AsString;
  edt_SrInfo6.Text := qryListSR_INFO6.AsString;

  //개설은행
  edt_OpBank1.Text := qryListOP_BANK1.AsString;
  edt_OpBank2.Text := qryListOP_BANK2.AsString;
  edt_OpBank3.Text := qryListOP_BANK3.AsString;
  //개설은행주소
  edt_OpAddr1.Text := qryListOP_ADDR1.AsString;
  edt_OpAddr2.Text := qryListOP_ADDR2.AsString;

  //Issuing Bank's Reference
  edt_IssBank1.Text := qryListISS_BANK1.AsString;
  edt_IssBank2.Text := qryListISS_BANK2.AsString;
  edt_IssBank3.Text := qryListISS_BANK3.AsString;
  edt_IssBank4.Text := qryListISS_BANK4.AsString;
  edt_IssBank5.Text := qryListISS_BANK5.AsString;
  //edt_IssBank6.Text := qryListISS_BANK6.AsString;

  //Latest Date of Shipment
  edt_lstDate.Text := qryListLST_DATE.AsString;

  //shipment Period
  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;
  edt_shipPD6.Text := qryListSHIP_PD6.AsString;

  //선적항
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;

  //도착항
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;

  //수탁(발송)지
  edt_loadOn.Text := qryListLOAD_ON.AsString;

  //최종목적지
  edt_forTran.Text := qryListFOR_TRAN.AsString;

  //Narrative
  memo_Narrat1.Lines.Text := qryListNARRAT_1.AsString;

end;

procedure TUI_INF707_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then readDocument;
end;

procedure TUI_INF707_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then readDocument;
end;

procedure TUI_INF707_frm.DeleteDocument;
var
  maint_no , chasu_no : String;
  nCursor : Integer;
begin
  inherited;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
    chasu_no := edt_chasu.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;
        SQL.Text :=  'DELETE FROM INF707_1 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and MSEQ = ' + QuotedStr(chasu_no) ;
        ExecSQL;

        SQL.Text :=  'DELETE FROM INF707_2 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and MSEQ = ' + QuotedStr(chasu_no) ;
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end
      else
        DMMssql.KISConnect.RollbackTrans;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_INF707_frm.btnDelClick(Sender: TObject);
begin
  inherited;

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;

   if qryList.RecordCount = 0 then
  begin
    ClearControlValue(page1_RightPanel);
    ClearControlValue(page2_RightPanel);
    ClearControlValue(page3_RightPanel);
    ClearControlValue(page4_RightPanel);
    ClearControlValue(maintno_Panel);
  end;

end;

procedure TUI_INF707_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : Integer;
begin
  inherited;

  nIndex := StrToIntDef(qryListCHK2.AsString,-1);

  case nIndex of
    -1 : Text := '';
     0 : Text := '임시';
     1 : Text := '저장';
     2 : Text := '결재';
     3 : Text := '반려';
     4 : Text := '접수';
     5 : Text := '준비';
     6 : Text := '취소';
     7 : Text := '전송';
     8 : Text := '오류';
     9 : Text := '승인';
  end;
end;

procedure TUI_INF707_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;

  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);
  with Sender as TsDBGrid do
  begin

    case CHK2Value of
    9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

    6 :
      begin
        if AnsiMatchText(Column.FieldName, ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end

end;

procedure TUI_INF707_frm.Readlist(OrderSyntax: string);
begin
  inherited;
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    if Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 4 then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := INF707_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add('WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+ ' AND ' + QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;

      1 : SQL.Add(' WHERE I707_1.MAINT_NO LIKE '+ QuotedStr('%'+edt_SearchText.Text+'%'));

      2 : SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      sql.Add(' ORDER BY APP_DATE ');
    end;

    Open;
  end;
end;

procedure TUI_INF707_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_INF707_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top + (Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_INF707_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    901 : sMaskEdit1DblClick(sMaskEdit1);
    902 : sMaskEdit1DblClick(Mask_SearchDate1);
    903 : sMaskEdit1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_INF707_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_INF707_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  INF707_PRINT_frm := TINF707_PRINT_frm.Create(Self);
  try

    INF707_PRINT_frm.PrintDocument(qryList.Fields,(Sender as TsButton).Tag = 1);

  finally
    FreeAndNil( INF707_PRINT_frm );
  end;

end;

procedure TUI_INF707_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

end.
