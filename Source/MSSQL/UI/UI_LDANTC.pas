unit UI_LDANTC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ExtCtrls, sBevel, Grids, DBGrids, acDBGrid, StdCtrls,
  sComboBox, sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, sCheckBox, sEdit, Mask, sMaskEdit, Buttons, sBitBtn,
  sButton, sLabel, sSpeedButton, sPanel, sSplitter, sSkinProvider, DB,
  ADODB;

type
  TUI_LDANTC_frm = class(TChildForm_frm)
    sSplitter1: TsSplitter;
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    btnExit: TsButton;
    btnDel: TsButton;
    sButton4: TsButton;
    sButton2: TsButton;
    sSplitter3: TsSplitter;
    sPanel4: TsPanel;
    btn_Cal: TsBitBtn;
    mask_DATEE: TsMaskEdit;
    edt_MaintNo: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_msg1: TsEdit;
    edt_UserNo: TsEdit;
    sCheckBox1: TsCheckBox;
    edt_msg2: TsEdit;
    sBitBtn3: TsBitBtn;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter4: TsSplitter;
    sPanel5: TsPanel;
    sPanel2: TsPanel;
    sSpeedButton1: TsSpeedButton;
    sPanel18: TsPanel;
    mask_SETDATE: TsMaskEdit;
    sPanel24: TsPanel;
    edt_BKNAME1: TsEdit;
    edt_BKNAME2: TsEdit;
    edt_BKNAME3: TsEdit;
    edt_LCNO: TsEdit;
    edt_BGMGUBUN: TsEdit;
    edt_RCNO2: TsEdit;
    edt_RCNO3: TsEdit;
    mask_NGDATE: TsMaskEdit;
    mask_RESDATE: TsMaskEdit;
    curr_AMT1: TsCurrencyEdit;
    edt_AMT1C: TsEdit;
    sPanel13: TsPanel;
    edt_AMT2C: TsEdit;
    curr_AMT2: TsCurrencyEdit;
    curr_RATE: TsCurrencyEdit;
    edt_APPNAME2: TsEdit;
    edt_APPNAME1: TsEdit;
    edt_APPNAME3: TsEdit;
    edt_RCNO: TsEdit;
    sPanel14: TsPanel;
    sPanel12: TsPanel;
    memo_REMARK1: TsMemo;
    sTabSheet9: TsTabSheet;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    edt_BGMGUBUNName: TsEdit;
    edt_RCNO6: TsEdit;
    edt_RCNO5: TsEdit;
    edt_RCNO4: TsEdit;
    edt_RCNO8: TsEdit;
    edt_RCNO7: TsEdit;
    sPanel6: TsPanel;
    edt_FINNO: TsEdit;
    edt_FINNO2: TsEdit;
    edt_FINNO4: TsEdit;
    edt_FINNO3: TsEdit;
    edt_FINNO5: TsEdit;
    edt_FINNO6: TsEdit;
    edt_FINNO7: TsEdit;
    sPanel7: TsPanel;
    edt_BANK1: TsEdit;
    edt_BANK2: TsEdit;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    sDBGrid8: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBK_CODE: TStringField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListLC_NO: TStringField;
    qryListRC_NO: TStringField;
    qryListAMT1: TBCDField;
    qryListAMT1C: TStringField;
    qryListAMT2: TBCDField;
    qryListAMT2C: TStringField;
    qryListRATE: TBCDField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListNG_DATE: TStringField;
    qryListREMAKR1: TMemoField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListBGM_GUBUN: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn60Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnDelClick(Sender: TObject);
    procedure sButton4Click(Sender: TObject);

  private
    { Private declarations }
    LDANTC_SQL : String;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;

    procedure ReadDocument;
    procedure DeleteDocument;
  public
    { Public declarations }
  end;

var
  UI_LDANTC_frm: TUI_LDANTC_frm;

implementation

uses MSSQL, Commonlib, DateUtils, MessageDefine, StrUtils, KISCalendar, LDANTC_PRINT,
  QuickRpt ;
{$R *.dfm}

procedure TUI_LDANTC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;
  LDANTC_SQL := qryList.SQL.Text;

end;

procedure TUI_LDANTC_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LDANTC_frm := nil;
end;

procedure TUI_LDANTC_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LDANTC_frm.FormShow(Sender: TObject);
begin
  inherited;
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);

  Mask_fromDate.Text := FormatDateTime('YYYYMMDD', StartOfTheYear(Now));
  mask_toDate.Text := FormatDateTime('YYYYMMDD', Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD', StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD' , Now);

  ReadList(Mask_fromDate.Text , mask_toDate.Text , '');

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
  end;

  
end;

procedure TUI_LDANTC_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 1 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := LDANTC_SQL;
    case com_SearchKeyword.ItemIndex of
      0 : //등록일자
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : //관리번호
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :  //내국신용장번호
      begin
        SQL.Add(' WHERE LC_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end;
    Open;
  end;

end;

function TUI_LDANTC_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
    Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := LDANTC_SQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_LDANTC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;



procedure TUI_LDANTC_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_LDANTC_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (sender as TsBitBtn).Tag of
    0 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    1 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_LDANTC_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_LDANTC_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  Readlist(Mask_fromDate.Text , mask_toDate.Text , '');

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
  end;
end;

procedure TUI_LDANTC_frm.ReadDocument;
begin
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //구분
  edt_BGMGUBUN.Text := qryListBGM_GUBUN.AsString;
  //구분name
  //구분코드에 다른 코드가 들어올경우 추가해줘야함 - 일반표준코드에서 보이지않음
  case AnsiIndexText( Trim(qryListBGM_GUBUN.AsString) , ['2CS']) of
    0 : edt_BGMGUBUNName.Text := '내국신용장판매대금추심도착통보서';
  end;

  //발신기관
  edt_BKNAME1.Text := qryListBK_NAME1.AsString;
  edt_BKNAME2.Text := qryListBK_NAME2.AsString;
  //전자서명
  edt_BKNAME3.Text := qryListBK_NAME3.AsString;
  //내국신용장번호
  edt_LCNO.Text := qryListLC_NO.AsString;
  //통지일자
  mask_RESDATE.Text := qryListRES_DATE.AsString;
  //NEGO일자
  mask_NGDATE.Text := qryListNG_DATE.AsString;
  //최종결제일
  mask_SETDATE.Text := qryListSET_DATE.AsString;
  //수신인
  edt_APPNAME1.Text := qryListAPP_NAME1.AsString;
  edt_APPNAME2.Text := qryListAPP_NAME2.AsString;
  edt_APPNAME3.Text := qryListAPP_NAME3.AsString;
  //외화금액
  edt_AMT1C.Text := qryListAMT1C.AsString;
  curr_AMT1.Value := qryListAMT1.AsCurrency;
  //원화환산금액
  edt_AMT2C.Text := qryListAMT2C.AsString;
  curr_AMT2.Value := qryListAMT2.AsCurrency;
  //적용환율
  curr_RATE.Value := qryListRATE.AsCurrency;
  //물품수령증명서번호
  edt_RCNO.Text := qryListRC_NO.AsString;
  edt_RCNO2.Text := qryListRC_NO2.AsString;
  edt_RCNO3.Text := qryListRC_NO3.AsString;
  edt_RCNO4.Text := qryListRC_NO4.AsString;
  edt_RCNO5.Text := qryListRC_NO5.AsString;
  edt_RCNO6.Text := qryListRC_NO6.AsString;
  edt_RCNO7.Text := qryListRC_NO7.AsString;
  edt_RCNO8.Text := qryListRC_NO8.AsString;
  //세금계산서번호
  edt_FINNO.Text := qryListFIN_NO.AsString;
  edt_FINNO2.Text := qryListFIN_NO2.AsString;
  edt_FINNO3.Text := qryListFIN_NO3.AsString;
  edt_FINNO4.Text := qryListFIN_NO4.AsString;
  edt_FINNO5.Text := qryListFIN_NO5.AsString;
  edt_FINNO6.Text := qryListFIN_NO6.AsString;
  edt_FINNO7.Text := qryListFIN_NO7.AsString;
  //통지은행
  edt_BANK1.Text := qryListBANK1.AsString;
  edt_BANK2.Text := qryListBANK2.AsString;
  //기타사항
  memo_REMARK1.Lines.Text := Trim(qryListREMAKR1.AsString);
end;

procedure TUI_LDANTC_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  ReadDocument;
end;

procedure TUI_LDANTC_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_LDANTC_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  DeleteDocument;
end;

procedure TUI_LDANTC_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM LDANTC WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally

    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

    Close;
    Free;
   end;
  end;
end;

procedure TUI_LDANTC_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  LDANTC_PRINT_frm := TLDANTC_PRINT_frm.Create(Self);
  try
    LDANTC_PRINT_frm.MaintNo := edt_MaintNo.Text;

    LDANTC_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
       0 :
       begin
         LDANTC_PRINT_frm.PrinterSetup;
         if LDANTC_PRINT_frm.Tag = 0 then LDANTC_PRINT_frm.Print;
       end;
       1 : LDANTC_PRINT_frm.Preview;
    end
  finally
    FreeAndNil(LDANTC_PRINT_frm);
  end;
end;

end.
