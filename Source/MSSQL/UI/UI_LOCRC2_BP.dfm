inherited UI_LOCRC2_BP_frm: TUI_LOCRC2_BP_frm
  Left = 404
  Top = 87
  Caption = 'UI_LOCRC2_BP_frm'
  ClientHeight = 829
  ClientWidth = 1173
  Constraints.MaxHeight = 868
  Constraints.MaxWidth = 1189
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 74
    Width = 1173
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [1]
    Left = 0
    Top = 0
    Width = 1173
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1173
      41)
    object sSpeedButton2: TsSpeedButton
      Left = 210
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 512
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 595
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 293
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1093
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 3
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 71
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 139
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 222
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 524
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 305
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 373
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 441
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 607
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 703
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 0
    Top = 77
    Width = 1173
    Height = 752
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 30
    TabIndex = 1
    TabOrder = 1
    TabWidth = 110
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet3: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1165
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter11: TsSplitter
        Left = 0
        Top = 509
        Width = 1165
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 34
        Width = 1165
        Height = 475
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sPanel33: TsPanel
        Left = 0
        Top = 511
        Width = 1165
        Height = 24
        SkinData.CustomColor = True
        SkinData.SkinSection = 'DRAGBAR'
        Align = alBottom
        BevelOuter = bvNone
        Caption = #49345#54408#45236#50669
        Ctl3D = False
        
        ParentCtl3D = False
        TabOrder = 1
      end
      object sDBGrid5: TsDBGrid
        Left = 0
        Top = 535
        Width = 1165
        Height = 177
        Align = alBottom
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 2
        Width = 1165
        Height = 32
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 3
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 23
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 0
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 321
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 4
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          Text = '20161125'
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 907
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
    end
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#45236#50857
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1165
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel2: TsPanel
        Left = 0
        Top = 2
        Width = 1165
        Height = 710
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        object sSplitter4: TsSplitter
          Left = 215
          Top = 1
          Width = 3
          Height = 708
          Cursor = crHSplit
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sPanel5: TsPanel
          Left = 1
          Top = 1
          Width = 214
          Height = 708
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'sPanel5'
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 0
        end
        object sPanel6: TsPanel
          Left = 218
          Top = 1
          Width = 946
          Height = 708
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          BevelOuter = bvNone
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 1
          object sPanel9: TsPanel
            Left = 0
            Top = 0
            Width = 946
            Height = 481
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alTop
            BevelOuter = bvNone
            
            TabOrder = 0
            DesignSize = (
              946
              481)
            object sSpeedButton1: TsSpeedButton
              Left = 365
              Top = -2
              Width = 10
              Height = 479
              Anchors = [akLeft, akTop, akBottom]
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'SPEEDBUTTON'
            end
            object mask_GET_DAT: TsMaskEdit
              Left = 134
              Top = 29
              Width = 76
              Height = 23
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              Text = '20161122'
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_BSN_HSCODE: TsEdit
              Left = 134
              Top = 125
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_RFF_NO: TsEdit
              Left = 134
              Top = 101
              Width = 223
              Height = 23
              HelpContext = 1
              Color = clBtnFace
              Ctl3D = True
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #48156#44553#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object mask_ISS_DAT: TsMaskEdit
              Left = 134
              Top = 53
              Width = 76
              Height = 23
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              Text = '20161122'
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #48156#44553#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object mask_EXP_DAT: TsMaskEdit
              Left = 134
              Top = 77
              Width = 76
              Height = 23
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              Text = '20161122'
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47928#49436#50976#54952#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_BENEFC: TsEdit
              Left = 134
              Top = 205
              Width = 65
              Height = 23
              HelpContext = 1
              Color = clYellow
              Ctl3D = True
              MaxLength = 5
              ParentCtl3D = False
              TabOrder = 5
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49688#54812#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn5: TsBitBtn
              Tag = 1
              Left = 200
              Top = 205
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 6
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_BENEFC1: TsEdit
              Left = 134
              Top = 229
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 7
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49345#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_APPLIC: TsEdit
              Tag = 1
              Left = 134
              Top = 285
              Width = 65
              Height = 23
              HelpContext = 1
              Color = clYellow
              Ctl3D = True
              MaxLength = 5
              ParentCtl3D = False
              TabOrder = 8
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#50629#52404
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn2: TsBitBtn
              Tag = 1
              Left = 200
              Top = 285
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 9
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_APPLIC1: TsEdit
              Left = 134
              Top = 309
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 10
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49345#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_APPLIC2: TsEdit
              Left = 134
              Top = 359
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 11
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47749#51032#51064
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_APPLIC3: TsEdit
              Left = 134
              Top = 383
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 12
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51204#51088#49436#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_APPLIC4: TsEdit
              Left = 134
              Top = 407
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 13
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51452#49548
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_AP_BANK: TsEdit
              Tag = 1
              Left = 526
              Top = 29
              Width = 65
              Height = 23
              HelpContext = 1
              Color = clYellow
              Ctl3D = True
              MaxLength = 5
              ParentCtl3D = False
              TabOrder = 14
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#51008#54665
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn3: TsBitBtn
              Tag = 1
              Left = 592
              Top = 29
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 15
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_AP_BANK1: TsEdit
              Left = 526
              Top = 53
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 16
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object edt_AP_BANK2: TsEdit
              Left = 526
              Top = 77
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 17
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object edt_AP_BANK4: TsEdit
              Left = 526
              Top = 125
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 18
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object edt_AP_NAME: TsEdit
              Left = 526
              Top = 149
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 19
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51008#54665#51204#51088#49436#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_AP_BANK3: TsEdit
              Left = 526
              Top = 101
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 20
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51008#54665'('#48512')'#51648#51216#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sPanel12: TsPanel
              Left = 379
              Top = 176
              Width = 375
              Height = 24
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #45236#44397#49888#50857#51109
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 21
            end
            object edt_LOC_NO: TsEdit
              Left = 526
              Top = 205
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 22
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_LOC1AMTC: TsEdit
              Tag = 102
              Left = 526
              Top = 229
              Width = 33
              Height = 23
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 3
              ParentCtl3D = False
              TabOrder = 23
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn8: TsBitBtn
              Tag = 102
              Left = 560
              Top = 229
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 24
              TabStop = False
              ImageIndex = 25
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object curr_LOC1AMT: TsCurrencyEdit
              Left = 585
              Top = 229
              Width = 164
              Height = 23
              AutoSize = False
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 25
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_EX_RATE: TsEdit
              Left = 526
              Top = 253
              Width = 99
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 26
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47588#47588#44592#51456#50984
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_LOC2AMTC: TsEdit
              Tag = 102
              Left = 526
              Top = 277
              Width = 33
              Height = 23
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clBtnFace
              Ctl3D = True
              MaxLength = 3
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 27
              Text = 'KRW'
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#44552#50529'('#50896#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object curr_LOC2AMT: TsCurrencyEdit
              Left = 560
              Top = 277
              Width = 189
              Height = 23
              AutoSize = False
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 28
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object mask_LOADDATE: TsMaskEdit
              Left = 526
              Top = 301
              Width = 76
              Height = 23
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 29
              Text = '20161122'
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object mask_EXPDATE: TsMaskEdit
              Left = 526
              Top = 325
              Width = 76
              Height = 23
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 30
              Text = '20161122'
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #50976#54952#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_AMAINT_NO: TsEdit
              Left = 526
              Top = 349
              Width = 223
              Height = 23
              HelpContext = 1
              Color = clBtnFace
              Ctl3D = True
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 31
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49888#50857#51109#44288#47532#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_RCT_AMT1C: TsEdit
              Tag = 102
              Left = 526
              Top = 405
              Width = 33
              Height = 23
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 3
              ParentCtl3D = False
              TabOrder = 32
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#44552#50529'('#50808#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn4: TsBitBtn
              Tag = 102
              Left = 560
              Top = 405
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 33
              TabStop = False
              ImageIndex = 25
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object curr_RCT_AMT1: TsCurrencyEdit
              Left = 585
              Top = 405
              Width = 164
              Height = 23
              AutoSize = False
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 34
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_RATE: TsEdit
              Left = 526
              Top = 429
              Width = 75
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 35
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #54872#50984
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_RCT_AMT2C: TsEdit
              Tag = 102
              Left = 526
              Top = 453
              Width = 33
              Height = 23
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clBtnFace
              Ctl3D = True
              MaxLength = 3
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 36
              Text = 'KRW'
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#44552#50529'('#50896#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object curr_RCT_AMT2: TsCurrencyEdit
              Left = 560
              Top = 453
              Width = 189
              Height = 23
              AutoSize = False
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 37
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_APPLIC5: TsEdit
              Left = 134
              Top = 431
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 38
              SkinData.SkinSection = 'EDIT'
            end
            object edt_APPLIC6: TsEdit
              Left = 134
              Top = 455
              Width = 223
              Height = 23
              HelpContext = 1
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 39
              SkinData.SkinSection = 'EDIT'
            end
            object edt_BENEFC2: TsMaskEdit
              Left = 134
              Top = 253
              Width = 91
              Height = 25
              HelpContext = 1
              Ctl3D = True
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 40
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.SkinSection = 'EDIT'
            end
            object edt_APPLIC7: TsMaskEdit
              Left = 134
              Top = 333
              Width = 91
              Height = 25
              HelpContext = 1
              Ctl3D = True
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 41
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.SkinSection = 'EDIT'
            end
          end
          object sPanel15: TsPanel
            Left = 0
            Top = 481
            Width = 946
            Height = 227
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alClient
            BevelOuter = bvNone
            
            TabOrder = 1
            object memo_LOC_REM1: TsMemo
              Left = 153
              Top = 61
              Width = 451
              Height = 55
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = []
              ParentFont = False
              ScrollBars = ssVertical
              TabOrder = 0
              BoundLabel.Active = True
              BoundLabel.UseSkinColor = False
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #52280#44256#49324#54637
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 5197647
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.SkinSection = 'EDIT'
            end
            object memo_REMARK1: TsMemo
              Left = 153
              Top = 5
              Width = 451
              Height = 55
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = []
              ParentFont = False
              ScrollBars = ssVertical
              TabOrder = 1
              BoundLabel.Active = True
              BoundLabel.UseSkinColor = False
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44592#53440#51312#44148
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 5197647
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.SkinSection = 'EDIT'
            end
          end
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #49345#54408#45236#50669
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1165
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter13: TsSplitter
        Left = 213
        Top = 2
        Width = 3
        Height = 710
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel20: TsPanel
        Left = 216
        Top = 2
        Width = 949
        Height = 710
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        BorderWidth = 4
        
        TabOrder = 0
        object sSplitter6: TsSplitter
          Left = 5
          Top = 30
          Width = 939
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sSplitter7: TsSplitter
          Left = 5
          Top = 272
          Width = 939
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sDBGrid3: TsDBGrid
          Left = 5
          Top = 32
          Width = 939
          Height = 240
          Align = alTop
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel21: TsPanel
          Left = 5
          Top = 274
          Width = 939
          Height = 245
          SkinData.SkinSection = 'PANEL'
          Align = alTop
          
          TabOrder = 1
          DesignSize = (
            939
            245)
          object sSpeedButton6: TsSpeedButton
            Left = 536
            Top = 3
            Width = 10
            Height = 237
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object edt_NAME_COD: TsEdit
            Left = 38
            Top = 29
            Width = 223
            Height = 23
            HelpContext = 1
            Color = clYellow
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_HS_NO: TsMaskEdit
            Left = 432
            Top = 29
            Width = 100
            Height = 23
            AutoSize = False
            Color = clWhite
            Ctl3D = False
            EditMask = '0000.00-0000;0;'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 12
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '2907131000'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS'#48512#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object memo_NAME1: TsMemo
            Left = 38
            Top = 53
            Width = 494
            Height = 80
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
          end
          object memo_SIZE1: TsMemo
            Left = 38
            Top = 159
            Width = 494
            Height = 80
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel22: TsPanel
            Left = 4
            Top = 3
            Width = 528
            Height = 24
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49345#54408' '#54408#47749
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 4
          end
          object sPanel23: TsPanel
            Left = 4
            Top = 134
            Width = 528
            Height = 24
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #44508#44201
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 5
          end
          object edt_QTY_G: TsEdit
            Tag = 102
            Left = 626
            Top = 29
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn9: TsBitBtn
            Tag = 102
            Left = 660
            Top = 29
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 7
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_QTY: TsCurrencyEdit
            Left = 685
            Top = 29
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_PRICE_G: TsEdit
            Tag = 102
            Left = 626
            Top = 53
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn10: TsBitBtn
            Tag = 102
            Left = 660
            Top = 53
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 10
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_PRICE: TsCurrencyEdit
            Left = 685
            Top = 53
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_QTYG_G: TsEdit
            Tag = 102
            Left = 626
            Top = 77
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn11: TsBitBtn
            Tag = 102
            Left = 660
            Top = 77
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 13
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_QTYG: TsCurrencyEdit
            Left = 685
            Top = 77
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 14
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_AMT_G: TsEdit
            Tag = 102
            Left = 626
            Top = 101
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 15
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn12: TsBitBtn
            Tag = 102
            Left = 660
            Top = 101
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 16
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_AMT: TsCurrencyEdit
            Left = 685
            Top = 101
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 17
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_STQTY_G: TsEdit
            Tag = 102
            Left = 626
            Top = 163
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 18
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn13: TsBitBtn
            Tag = 102
            Left = 660
            Top = 163
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 19
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_STQTY: TsCurrencyEdit
            Left = 685
            Top = 163
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 20
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_STAMT_G: TsEdit
            Tag = 102
            Left = 626
            Top = 187
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 21
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn14: TsBitBtn
            Tag = 102
            Left = 660
            Top = 187
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_STAMT: TsCurrencyEdit
            Left = 685
            Top = 187
            Width = 164
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 23
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object btn_GoodsCalc: TsButton
            Left = 626
            Top = 128
            Width = 87
            Height = 31
            Caption = #44228#49328
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 24
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object sPanel24: TsPanel
            Left = 550
            Top = 3
            Width = 404
            Height = 24
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49688#47049'/'#45800#44032'/'#44552#50529
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 25
          end
          object sBitBtn15: TsBitBtn
            Left = 262
            Top = 29
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 26
            TabStop = False
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
        end
        object Pan_Detail: TsPanel
          Left = 5
          Top = 5
          Width = 939
          Height = 25
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 2
          object LIne1: TsSpeedButton
            Left = 1
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Line4: TsSpeedButton
            Left = 73
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_GoodsDel: TsSpeedButton
            Tag = 2
            Left = 150
            Top = 1
            Width = 67
            Height = 23
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object Line3: TsSpeedButton
            Left = 145
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_GoodsEdit: TsSpeedButton
            Tag = 1
            Left = 78
            Top = 1
            Width = 67
            Height = 23
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object LIne2: TsSpeedButton
            Left = 217
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_GoodsOK: TsSpeedButton
            Left = 222
            Top = 1
            Width = 67
            Height = 23
            Caption = #54869#51064
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object Btn_GoodsCancel: TsSpeedButton
            Left = 289
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 18
          end
          object sSpeedButton14: TsSpeedButton
            Left = 356
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_GoodsNew: TsSpeedButton
            Left = 6
            Top = 1
            Width = 67
            Height = 23
            Caption = #51077#47141
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object sSpeedButton18: TsSpeedButton
            Left = 819
            Top = 1
            Width = 119
            Height = 23
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 30
          end
        end
        object sPanel30: TsPanel
          Left = 5
          Top = 597
          Width = 939
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 3
        end
        object sPanel14: TsPanel
          Left = 5
          Top = 543
          Width = 939
          Height = 54
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 4
          object sLabel4: TsLabel
            Left = 521
            Top = 35
            Width = 144
            Height = 15
            Caption = #49345#54408#45236#50669' '#51077#47141' '#54980' '#44228#49328#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
          object btn_TOTAL_CALC: TsButton
            Left = 453
            Top = 4
            Width = 67
            Height = 47
            Caption = #44228#49328
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object edt_TQTY_G: TsEdit
            Tag = 102
            Left = 237
            Top = 4
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn6: TsBitBtn
            Tag = 102
            Left = 271
            Top = 4
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 2
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_TQTY: TsCurrencyEdit
            Left = 296
            Top = 4
            Width = 155
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TAMT_G: TsEdit
            Tag = 102
            Left = 237
            Top = 28
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn7: TsBitBtn
            Tag = 102
            Left = 271
            Top = 28
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 5
            TabStop = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_TAMT: TsCurrencyEdit
            Left = 296
            Top = 28
            Width = 155
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
        end
        object sPanel32: TsPanel
          Left = 5
          Top = 519
          Width = 939
          Height = 24
          SkinData.SkinSection = 'DRAGBAR'
          Align = alTop
          BevelOuter = bvNone
          Caption = #52509' '#54633#44228
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 5
        end
      end
      object sPanel7: TsPanel
        Left = 0
        Top = 2
        Width = 213
        Height = 710
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        Caption = 'sPanel7'
        
        TabOrder = 1
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      object sSplitter9: TsSplitter
        Left = 0
        Top = 0
        Width = 1165
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter14: TsSplitter
        Left = 213
        Top = 2
        Width = 3
        Height = 710
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object panel1: TsPanel
        Left = 216
        Top = 2
        Width = 949
        Height = 710
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        BevelWidth = 4
        Caption = 'panel1'
        
        TabOrder = 0
        object sPanel19: TsPanel
          Left = 4
          Top = 4
          Width = 941
          Height = 25
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 0
          object sSpeedButton7: TsSpeedButton
            Left = 1
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object sSpeedButton8: TsSpeedButton
            Left = 73
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_TaxDel: TsSpeedButton
            Tag = 2
            Left = 150
            Top = 1
            Width = 67
            Height = 23
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton10: TsSpeedButton
            Left = 145
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_TaxEdit: TsSpeedButton
            Tag = 1
            Left = 78
            Top = 1
            Width = 67
            Height = 23
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object sSpeedButton12: TsSpeedButton
            Left = 217
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_TaxOK: TsSpeedButton
            Left = 222
            Top = 1
            Width = 67
            Height = 23
            Caption = #54869#51064
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object btn_TaxCancel: TsSpeedButton
            Left = 289
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 18
          end
          object sSpeedButton16: TsSpeedButton
            Left = 356
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_TaxNew: TsSpeedButton
            Left = 6
            Top = 1
            Width = 67
            Height = 23
            Caption = #51077#47141
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object taxExcelBtn: TsSpeedButton
            Left = 821
            Top = 1
            Width = 119
            Height = 23
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 30
          end
        end
        object sDBGrid4: TsDBGrid
          Left = 4
          Top = 29
          Width = 941
          Height = 240
          Align = alTop
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel28: TsPanel
          Left = 4
          Top = 269
          Width = 941
          Height = 161
          SkinData.SkinSection = 'PANEL'
          Align = alTop
          
          TabOrder = 2
          object edt_BILL_NO: TsEdit
            Left = 246
            Top = 61
            Width = 223
            Height = 23
            HelpContext = 1
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object mask_BILL_DATE: TsMaskEdit
            Left = 558
            Top = 61
            Width = 76
            Height = 23
            AutoSize = False
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51089#49457#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BILL_AMOUNT_UNIT: TsEdit
            Tag = 102
            Left = 246
            Top = 101
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clBtnFace
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 2
            Text = 'KRW'
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object curr_BILL_AMOUNT: TsCurrencyEdit
            Left = 280
            Top = 101
            Width = 189
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TAX_AMOUNT_UNIT: TsEdit
            Tag = 102
            Left = 558
            Top = 101
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clBtnFace
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 4
            Text = 'KRW'
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object curr_TAX_AMOUNT: TsCurrencyEdit
            Left = 592
            Top = 101
            Width = 189
            Height = 23
            AutoSize = False
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sPanel18: TsPanel
            Left = 152
            Top = 19
            Width = 665
            Height = 24
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49464#44552#44228#49328#49436
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 6
          end
        end
      end
      object sPanel26: TsPanel
        Left = 0
        Top = 2
        Width = 213
        Height = 710
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        Caption = 'sPanel7'
        
        TabOrder = 1
      end
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 41
    Width = 1173
    Height = 33
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 2
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 241
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object mask_DATEE: TsMaskEdit
      Left = 366
      Top = 5
      Width = 83
      Height = 23
      AutoSize = False
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 528
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 448
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 3
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 649
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn19: TsBitBtn
      Tag = 1
      Left = 682
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 5
      TabStop = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 737
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn20: TsBitBtn
      Tag = 2
      Left = 770
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object left_panel: TsPanel [4]
    Left = 4
    Top = 115
    Width = 215
    Height = 708
    SkinData.SkinSection = 'PANEL'
    Caption = 'left_panel'
    
    TabOrder = 3
    object sPanel53: TsPanel
      Left = 1
      Top = 1
      Width = 213
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 0
      object Mask_fromDate: TsMaskEdit
        Left = 31
        Top = 5
        Width = 74
        Height = 23
        TabStop = False
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 181
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 1
        TabStop = False
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 106
        Top = 5
        Width = 74
        Height = 23
        TabStop = False
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        Text = '20161122'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 34
      Width = 213
      Height = 673
      Align = alClient
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.SkinSection = 'EDIT'
    end
  end
end
