unit UI_ADV700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sComboBox, Grids, DBGrids,
  acDBGrid, ComCtrls, sPageControl, sCheckBox, sEdit, Mask, sMaskEdit,
  Buttons, sBitBtn, ExtCtrls, sSplitter, sButton, sSpeedButton, sPanel,
  sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, sLabel, DBCtrls,
  sDBMemo, sDBEdit, sBevel, DB, ADODB, QuickRpt;

type
  TUI_ADV700_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    sButton4: TsButton;
    sButton2: TsButton;
    sSplitter1: TsSplitter;
    sPanel4: TsPanel;
    btn_Cal: TsBitBtn;
    mask_DATEE: TsMaskEdit;
    edt_MaintNo: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_msg1: TsEdit;
    edt_UserNo: TsEdit;
    sCheckBox1: TsCheckBox;
    edt_msg2: TsEdit;
    sBitBtn3: TsBitBtn;
    sSplitter3: TsSplitter;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter4: TsSplitter;
    sTabSheet2: TsTabSheet;
    sSplitter7: TsSplitter;
    sTabSheet3: TsTabSheet;
    sSplitter5: TsSplitter;
    sTabSheet4: TsTabSheet;
    sSplitter8: TsSplitter;
    sTabSheet5: TsTabSheet;
    sSplitter6: TsSplitter;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    sPanel13: TsPanel;
    sTabSheet9: TsTabSheet;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    mask_APPDATE: TsMaskEdit;
    edt_APPNO: TsEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    edt_APBANK: TsEdit;
    edt_APBANK1: TsEdit;
    edt_APBANK2: TsEdit;
    edt_APBANK3: TsEdit;
    edt_APBANK4: TsEdit;
    sPanel20: TsPanel;
    edt_ADBANK: TsEdit;
    edt_ADBANK1: TsEdit;
    edt_ADBANK2: TsEdit;
    edt_ADBANK3: TsEdit;
    edt_ADBANK4: TsEdit;
    edt_ADBANK5: TsEdit;
    sPanel21: TsPanel;
    memo_ADDINFO1: TsMemo;
    sPanel22: TsPanel;
    edt_DOCCD1: TsEdit;
    edt_DOCCD2: TsEdit;
    edt_DOCCD3: TsEdit;
    edt_CDNO: TsEdit;
    edt_APPLICABLERULES1: TsEdit;
    edt_APPLICABLERULES2: TsEdit;
    edt_REEPRE: TsEdit;
    mask_ISSDATE: TsMaskEdit;
    mask_EXDATE: TsMaskEdit;
    edt_EXPLACE: TsEdit;
    sPanel23: TsPanel;
    edt_APPBANK: TsEdit;
    edt_APPBANK1: TsEdit;
    edt_APPBANK2: TsEdit;
    edt_APPBANK3: TsEdit;
    edt_APPBANK4: TsEdit;
    edt_APPACCNT: TsEdit;
    sPanel24: TsPanel;
    edt_APPLIC1: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC4: TsEdit;
    sSpeedButton1: TsSpeedButton;
    sPanel26: TsPanel;
    edt_BENEFC3: TsEdit;
    sSpeedButton6: TsSpeedButton;
    edt_BENEFC6: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BENEFC5: TsEdit;
    edt_BENEFC4: TsEdit;
    sPanel27: TsPanel;
    sPanel28: TsPanel;
    edt_BENEFC1: TsEdit;
    edt_CDCUR: TsEdit;
    curr_CDAMT: TsCurrencyEdit;
    sLabel1: TsLabel;
    edt_AVPAY: TsEdit;
    edt_AVAIL: TsEdit;
    edt_AVAIL1: TsEdit;
    edt_AVAIL4: TsEdit;
    edt_AVAIL3: TsEdit;
    edt_AVAIL2: TsEdit;
    edt_AVACCNT: TsEdit;
    sPanel30: TsPanel;
    edt_AACV1: TsEdit;
    edt_AACV2: TsEdit;
    edt_AACV3: TsEdit;
    edt_AACV4: TsEdit;
    sPanel29: TsPanel;
    edt_DRAFT1: TsEdit;
    edt_DRAFT2: TsEdit;
    edt_DRAFT3: TsEdit;
    edt_DRAFT4: TsEdit;
    sPanel31: TsPanel;
    edt_MIXPAY1: TsEdit;
    edt_MIXPAY3: TsEdit;
    edt_MIXPAY2: TsEdit;
    edt_MIXPAY4: TsEdit;
    sPanel32: TsPanel;
    edt_DEFPAY1: TsEdit;
    edt_DEFPAY2: TsEdit;
    edt_DEFPAY3: TsEdit;
    edt_DEFPAY4: TsEdit;
    sPanel33: TsPanel;
    sSpeedButton7: TsSpeedButton;
    edt_DRAWEE: TsEdit;
    edt_DRAWEE1: TsEdit;
    edt_DRAWEE2: TsEdit;
    edt_DRAWEE3: TsEdit;
    edt_DRAWEE4: TsEdit;
    edt_DRACCNT: TsEdit;
    sPanel34: TsPanel;
    edt_PSHIP: TsEdit;
    edt_TSHIP: TsEdit;
    mask_LSTDATE: TsMaskEdit;
    edt_LOADON: TsEdit;
    sLabel2: TsLabel;
    edt_FORTRAN: TsEdit;
    sLabel3: TsLabel;
    edt_SUNJUCKPORT: TsEdit;
    sLabel4: TsLabel;
    edt_DOCHACKPORT: TsEdit;
    sLabel5: TsLabel;
    sPanel35: TsPanel;
    edt_SHIPPD4: TsEdit;
    edt_SHIPPD5: TsEdit;
    edt_SHIPPD6: TsEdit;
    edt_SHIPPD1: TsEdit;
    edt_SHIPPD2: TsEdit;
    edt_SHIPPD3: TsEdit;
    sPanel36: TsPanel;
    edt_CHARGE1: TsEdit;
    edt_CHARGE2: TsEdit;
    edt_CHARGE3: TsEdit;
    edt_CHARGE4: TsEdit;
    edt_CHARGE5: TsEdit;
    edt_CHARGE6: TsEdit;
    sPanel37: TsPanel;
    edt_PDPRSNT1: TsEdit;
    edt_PDPRSNT2: TsEdit;
    edt_PDPRSNT3: TsEdit;
    edt_PDPRSNT4: TsEdit;
    sPanel38: TsPanel;
    edt_CONFIRMM: TsEdit;
    sPanel39: TsPanel;
    edt_REIBANK: TsEdit;
    edt_REIBANK1: TsEdit;
    edt_REIBANK2: TsEdit;
    edt_REIBANK3: TsEdit;
    edt_REIBANK4: TsEdit;
    edt_REIACCMT: TsEdit;
    memo_DESGOOD1: TsMemo;
    sPanel40: TsPanel;
    sPanel41: TsPanel;
    memo_DOCREQU1: TsMemo;
    sPanel42: TsPanel;
    memo_ADDCOND1: TsMemo;
    memo_INSTRCT1: TsMemo;
    sPanel43: TsPanel;
    sPanel15: TsPanel;
    edt_AVTBANK: TsEdit;
    edt_AVTBANK1: TsEdit;
    edt_AVTBANK2: TsEdit;
    edt_AVTBANK3: TsEdit;
    edt_AVTBANK4: TsEdit;
    edt_AVTACCNT: TsEdit;
    sSpeedButton8: TsSpeedButton;
    sPanel16: TsPanel;
    edt_SNDINFO1: TsEdit;
    edt_SNDINFO2: TsEdit;
    edt_SNDINFO3: TsEdit;
    edt_SNDINFO4: TsEdit;
    edt_SNDINFO5: TsEdit;
    edt_SNDINFO6: TsEdit;
    sPanel17: TsPanel;
    edt_EXNAME1: TsEdit;
    edt_EXNAME2: TsEdit;
    edt_EXNAME3: TsEdit;
    edt_EXADDR1: TsEdit;
    edt_EXADDR2: TsEdit;
    sPanel44: TsPanel;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    sDBGrid8: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListChk1: TBooleanField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListDOC_CD1: TStringField;
    qryListDOC_CD2: TStringField;
    qryListDOC_CD3: TStringField;
    qryListCD_NO: TStringField;
    qryListREE_PRE: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListAPP_BANK: TStringField;
    qryListAPP_BANK1: TStringField;
    qryListAPP_BANK2: TStringField;
    qryListAPP_BANK3: TStringField;
    qryListAPP_BANK4: TStringField;
    qryListAPP_BANK5: TStringField;
    qryListAPP_ACCNT: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListPRNO: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAVAIL5: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListAV_PAY: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListDRAFT4: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListDRAWEE5: TStringField;
    qryListDR_ACCNT: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListDESGOOD: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListDOCREQU: TStringField;
    qryListDOCREQU_1: TMemoField;
    qryListADDCOND: TStringField;
    qryListADDCOND_1: TMemoField;
    qryListCHARGE1: TStringField;
    qryListCHARGE2: TStringField;
    qryListCHARGE3: TStringField;
    qryListCHARGE4: TStringField;
    qryListCHARGE5: TStringField;
    qryListCHARGE6: TStringField;
    qryListPD_PRSNT1: TStringField;
    qryListPD_PRSNT2: TStringField;
    qryListPD_PRSNT3: TStringField;
    qryListPD_PRSNT4: TStringField;
    qryListCONFIRMM: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListREI_BANK5: TStringField;
    qryListREI_ACCNT: TStringField;
    qryListINSTRCT: TStringField;
    qryListINSTRCT_1: TMemoField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAVT_BANK5: TStringField;
    qryListAVT_ACCNT: TStringField;
    qryListSND_INFO1: TStringField;
    qryListSND_INFO2: TStringField;
    qryListSND_INFO3: TStringField;
    qryListSND_INFO4: TStringField;
    qryListSND_INFO5: TStringField;
    qryListSND_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListCHK1_1: TStringField;
    qryListCHK2_1: TStringField;
    qryListCHK3_1: TStringField;
    qryListPRNO_1: TIntegerField;
    qryListMAINT_NO_3: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLOAD_ON: TStringField;
    qryListTSHIP: TStringField;
    qryListPSHIP: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryDetailMAINT_NO: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailLCNUM: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    curr_CDPERP: TsCurrencyEdit;
    curr_CDPERM: TsCurrencyEdit;
    edt_CDMAX: TsEdit;
    sSpeedButton3: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sPanel14: TsPanel;
    sSplitter2: TsSplitter;
    detailDBGrid: TsDBGrid;
    memo_SZIE1: TsDBMemo;
    memo_NAME1: TsDBMemo;
    edt_QTY_G: TsDBEdit;
    edt_PRICEG: TsDBEdit;
    edt_QTY: TsDBEdit;
    edt_PRICE: TsDBEdit;
    edt_QTYG_G: TsDBEdit;
    edt_AMTG: TsDBEdit;
    edt_QTYG: TsDBEdit;
    edt_AMT: TsDBEdit;
    edt_NAMECOD: TsDBEdit;
    sSplitter9: TsSplitter;
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn60Click(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    ADV700_SQL : String;
    ADV700_SQL_D : String;

    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;

    procedure ReadDocument;
    procedure DeleteDocument;    
  public
    { Public declarations }
  end;

var
  UI_ADV700_frm: TUI_ADV700_frm;

implementation

uses MSSQL, Commonlib, DateUtils, MessageDefine, StrUtils, KISCalendar, ADV700_PRINT;

{$R *.dfm}

procedure TUI_ADV700_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ADV700_frm := nil;
end;

procedure TUI_ADV700_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ADV700_frm.FormCreate(Sender: TObject);
begin
  inherited;
  ADV700_SQL := qryList.SQL.Text;
  ADV700_SQL_D := qryDetail.SQL.Text;
end;

procedure TUI_ADV700_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;

  //일자
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD' , Now);

  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel7);
  EnabledControlValue(sPanel9);
  EnabledControlValue(sPanel11);
  EnabledControlValue(sPanel13);
//  EnabledControlValue(sPanel14);

  ReadList(Mask_fromDate.Text, mask_toDate.Text , '');

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
    ClearControlValue(sPanel9);
    ClearControlValue(sPanel11);
    ClearControlValue(sPanel13);

  end;
end;

function TUI_ADV700_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := ADV700_SQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_ADV700_frm.Readlist(OrderSyntax: string);
begin
 if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 5 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := ADV700_SQL;
    case com_SearchKeyword.ItemIndex of
      0 : //등록일자
      begin
        SQL.Add(' WHERE ADV700.DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : //관리번호
      begin
        SQL.Add(' WHERE ADV700.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :  //개설의뢰인
      begin
        SQL.Add(' WHERE ADV700.APPLIC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end;
    Open;
  end;
end;

procedure TUI_ADV700_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  ReadList(Mask_fromDate.Text, mask_toDate.Text,'');

  //삭제 후 레코드가 없으면 클리어
  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
    ClearControlValue(sPanel9);
    ClearControlValue(sPanel11);
    ClearControlValue(sPanel13);
  end;

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;
end;

procedure TUI_ADV700_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //sPanel4.Visible := not (sPageControl1.ActivePageIndex = 5);
  sPanel44.Visible := not (sPageControl1.ActivePageIndex = 5);
end;

procedure TUI_ADV700_frm.ReadDocument;
begin

  IF not qrylist.Active Then Exit;
  IF qrylist.RecordCount = 0 Then Exit;

  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //수신일자
  mask_DATEE.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;

  //통지일자
  mask_APPDATE.Text := qryListAPP_DATE.AsString;
  //통지번호
  edt_APPNO.Text := qryListAPP_NO.AsString;
  //개설은행
  edt_APBANK.Text := qryListAP_BANK.AsString;
  edt_APBANK1.Text := qryListAP_BANK1.AsString;
  edt_APBANK2.Text := qryListAP_BANK2.AsString;
  edt_APBANK3.Text := qryListAP_BANK3.AsString;
  edt_APBANK4.Text := qryListAP_BANK4.AsString;
  //통지은행
  edt_ADBANK.Text := qryListAD_BANK.AsString;
  edt_ADBANK1.Text := qryListAD_BANK1.AsString;
  edt_ADBANK2.Text := qryListAD_BANK2.AsString;
  edt_ADBANK3.Text := qryListAD_BANK3.AsString;
  edt_ADBANK4.Text := qryListAD_BANK4.AsString;
  edt_ADBANK5.Text := qryListAD_BANK5.AsString;
  //기타정보(부가정보)
  memo_ADDINFO1.Lines.Text := qryListADDINFO_1.AsString;
  //신용장
  edt_DOCCD1.Text := qryListDOC_CD1.AsString;
  edt_DOCCD2.Text := qryListDOC_CD2.AsString;
  edt_DOCCD3.Text := qryListDOC_CD3.AsString;
  edt_CDNO.Text := qryListCD_NO.AsString;
  //Applicable Rules
  edt_APPLICABLERULES1.Text := qryListAPPLICABLE_RULES_1.AsString;
  edt_APPLICABLERULES2.Text := qryListAPPLICABLE_RULES_2.AsString;
  //선통지 참조사항
  edt_REEPRE.Text := qryListREE_PRE.AsString;
  //개설일자
  mask_ISSDATE.Text := qryListISS_DATE.AsString;
  //유효기일
  mask_EXDATE.Text := qryListEX_DATE.AsString;
  //유효장소
  edt_EXPLACE.Text := qryListEX_PLACE.AsString;
  //개설의뢰인은행
  edt_APPBANK.Text := qryListAPP_BANK.AsString;
  edt_APPBANK1.Text := qryListAPP_BANK1.AsString;
  edt_APPBANK2.Text := qryListAPP_BANK2.AsString;
  edt_APPBANK3.Text := qryListAPP_BANK3.AsString;
  edt_APPBANK4.Text := qryListAPP_BANK4.AsString;
  edt_APPACCNT.Text := qryListAPP_ACCNT.AsString;
  //개설의뢰인
  edt_APPLIC1.Text := qryListAPPLIC1.AsString;
  edt_APPLIC2.Text := qryListAPPLIC2.AsString;
  edt_APPLIC3.Text := qryListAPPLIC3.AsString;
  edt_APPLIC4.Text := qryListAPPLIC4.AsString;
  //수익자
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  edt_BENEFC4.Text := qryListBENEFC4.AsString;
  edt_BENEFC5.Text := qryListBENEFC5.AsString;
  edt_BENEFC6.Text := qryListBENEFC6.AsString;
  //금액정보
  edt_CDCUR.Text := qryListCD_CUR.AsString;
  curr_CDAMT.Value := qryListCD_AMT.AsCurrency;
  curr_CDPERP.Value := qryListCD_PERP.AsCurrency;
  curr_CDPERM.Value := qryListCD_PERM.AsCurrency;
  edt_CDMAX.Text := qryListCD_MAX.AsString;
  //신용장 지급방식및 은행
  edt_AVPAY.Text := qryListAV_PAY.AsString;
  edt_AVAIL.Text := qryListAVAIL.AsString;
  edt_AVAIL1.Text := qryListAVAIL1.AsString;
  edt_AVAIL2.Text := qryListAVAIL2.AsString;
  edt_AVAIL3.Text := qryListAVAIL3.AsString;
  edt_AVAIL4.Text := qryListAVAIL4.AsString;
  edt_AVACCNT.Text := qryListAV_ACCNT.AsString;
  //부가금액부담
  edt_AACV1.Text := qryListAA_CV1.AsString;
  edt_AACV2.Text := qryListAA_CV2.AsString;
  edt_AACV3.Text := qryListAA_CV3.AsString;
  edt_AACV4.Text := qryListAA_CV4.AsString;
  //화환어음 조건
  edt_DRAFT1.Text := qryListDRAFT1.AsString;
  edt_DRAFT2.Text := qryListDRAFT2.AsString;
  edt_DRAFT3.Text := qryListDRAFT3.AsString;
  edt_DRAFT4.Text := qryListDRAFT4.AsString;
  //혼합지급조건명세
  edt_MIXPAY1.Text := qryListMIX_PAY1.AsString;
  edt_MIXPAY2.Text := qryListMIX_PAY2.AsString;
  edt_MIXPAY3.Text := qryListMIX_PAY3.AsString;
  edt_MIXPAY4.Text := qryListMIX_PAY4.AsString;
  //연지금조건명세
  edt_DEFPAY1.Text := qryListDEF_PAY1.AsString;
  edt_DEFPAY2.Text := qryListDEF_PAY2.AsString;
  edt_DEFPAY3.Text := qryListDEF_PAY3.AsString;
  edt_DEFPAY4.Text := qryListDEF_PAY4.AsString;
  //어음지급인
  edt_DRAWEE.Text := qryListDRAWEE.AsString;
  edt_DRAWEE1.Text := qryListDRAWEE1.AsString;
  edt_DRAWEE2.Text := qryListDRAWEE2.AsString;
  edt_DRAWEE3.Text := qryListDRAWEE3.AsString;
  edt_DRAWEE4.Text := qryListDRAWEE4.AsString;
  edt_DRACCNT.Text := qryListDR_ACCNT.AsString;
  //선적정보
  edt_PSHIP.Text := qryListPSHIP.AsString;
  edt_TSHIP.Text := qryListTSHIP.AsString;
  edt_LOADON.Text := qryListLOAD_ON.AsString;
  edt_FORTRAN.Text := qryListFOR_TRAN.AsString;
  edt_SUNJUCKPORT.Text := qryListSUNJUCK_PORT.AsString;
  edt_DOCHACKPORT.Text := qryListDOCHACK_PORT.AsString;
  mask_LSTDATE.Text := qryListLST_DATE.AsString;
  //선적기간
  edt_SHIPPD1.Text := qryListSHIP_PD1.AsString;
  edt_SHIPPD2.Text := qryListSHIP_PD2.AsString;
  edt_SHIPPD3.Text := qryListSHIP_PD3.AsString;
  edt_SHIPPD4.Text := qryListSHIP_PD4.AsString;
  edt_SHIPPD5.Text := qryListSHIP_PD5.AsString;
  edt_SHIPPD6.Text := qryListSHIP_PD6.AsString;
  //수수료부담자
  edt_CHARGE1.Text := qryListCHARGE1.AsString;
  edt_CHARGE2.Text := qryListCHARGE2.AsString;
  edt_CHARGE3.Text := qryListCHARGE3.AsString;
  edt_CHARGE4.Text := qryListCHARGE4.AsString;
  edt_CHARGE5.Text := qryListCHARGE5.AsString;
  edt_CHARGE6.Text := qryListCHARGE6.AsString;
  //서류제시기간
  edt_PDPRSNT1.Text := qryListPD_PRSNT1.AsString;
  edt_PDPRSNT2.Text := qryListPD_PRSNT2.AsString;
  edt_PDPRSNT3.Text := qryListPD_PRSNT3.AsString;
  edt_PDPRSNT4.Text := qryListPD_PRSNT4.AsString;
  //확인지시문언
  edt_CONFIRMM.Text := qryListCONFIRMM.AsString;
  //상환은행
  edt_REIBANK.Text := qryListREI_BANK.AsString;
  edt_REIBANK1.Text := qryListREI_BANK1.AsString;
  edt_REIBANK2.Text := qryListREI_BANK2.AsString;
  edt_REIBANK3.Text := qryListREI_BANK3.AsString;
  edt_REIBANK4.Text := qryListREI_BANK4.AsString;
  edt_REIACCMT.Text := qryListREI_ACCNT.AsString;
  //상품(용역)명세
  memo_DESGOOD1.Lines.Text := qryListDESGOOD_1.AsString;
  //구비서류
  memo_DOCREQU1.Lines.Text := qryListDOCREQU_1.AsString;
  //부가조건
  memo_ADDCOND1.Lines.Text := qryListADDCOND_1.AsString;
  //매입/지급/인수은행에 대한 지시사항
  memo_INSTRCT1.Lines.Text := qryListINSTRCT_1.AsString;
  //최종통지은행
  edt_AVTBANK.Text := qryListAVT_BANK.AsString;
  edt_AVTBANK1.Text := qryListAVT_BANK1.AsString;
  edt_AVTBANK2.Text := qryListAVT_BANK2.AsString;
  edt_AVTBANK3.Text := qryListAVT_BANK3.AsString;
  edt_AVTBANK4.Text := qryListAVT_BANK4.AsString;
  edt_AVTACCNT.Text := qryListAVT_ACCNT.AsString;
  //통지은행
  edt_EXNAME1.Text := qryListEX_NAME1.AsString;
  edt_EXNAME2.Text := qryListEX_NAME2.AsString;
  edt_EXNAME3.Text := qryListEX_NAME3.AsString;
  edt_EXADDR1.Text := qryListEX_ADDR1.AsString;
  edt_EXADDR2.Text := qryListEX_ADDR2.AsString;
  //수신앞은행정보
  edt_SNDINFO1.Text := qryListSND_INFO1.AsString;
  edt_SNDINFO2.Text := qryListSND_INFO2.AsString;
  edt_SNDINFO3.Text := qryListSND_INFO3.AsString;
  edt_SNDINFO4.Text := qryListSND_INFO4.AsString;
  edt_SNDINFO5.Text := qryListSND_INFO5.AsString;
  edt_SNDINFO6.Text := qryListSND_INFO6.AsString;

  with qryDetail do
  begin
    Close;
    SQL.Text :=  ADV700_SQL_D;
    SQL.Add(' WHERE MAINT_NO = ' + QuotedStr(edt_MaintNo.Text));
    Open;
  end;
  qryDetail.Append;
  detailDBGrid.DataSource.DataSet.FieldByName('SIZE1').AsString := qryListDESGOOD_1.AsString;

end;

procedure TUI_ADV700_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

procedure TUI_ADV700_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.RecordCount = 0 then ReadDocument;
end;

procedure TUI_ADV700_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_ADV700_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
    ClearControlValue(sPanel9);
    ClearControlValue(sPanel11);
    ClearControlValue(sPanel13);
  end;

end;

procedure TUI_ADV700_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
  //삭제 후 레코드가 없으면 클리어
  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
    ClearControlValue(sPanel9);
    ClearControlValue(sPanel11);
    ClearControlValue(sPanel13);
  end;
end;

procedure TUI_ADV700_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM ADV700  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7001  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7002  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7003  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV700_D WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally

    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

    Close;
    Free;
   end;
  end;

end;

procedure TUI_ADV700_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_ADV700_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    902 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    903 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end

end;

procedure TUI_ADV700_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  ADV700_PRINT_frm := TADV700_PRINT_frm.Create(Self);
  try
    ADV700_PRINT_frm.MaintNo := edt_MaintNo.Text;

    ADV700_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        ADV700_PRINT_frm.PrinterSetup;
        if ADV700_PRINT_frm.Tag = 0 then ADV700_PRINT_frm.Print;
      end;
      1 : ADV700_PRINT_frm.Preview;
    end;

  finally
    FreeAndNil(ADV700_PRINT_frm);
  end;
end;

procedure TUI_ADV700_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
