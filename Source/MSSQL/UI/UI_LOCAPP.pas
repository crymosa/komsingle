unit UI_LOCAPP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ExtCtrls, sSplitter, sPanel, sSkinProvider, StdCtrls,
  Mask, sMaskEdit, sComboBox, Grids, DBGrids, acDBGrid, ComCtrls,
  sPageControl, sButton, Buttons, sBitBtn, sEdit, DB, ADODB, sScrollBox,
  sLabel, sSpeedButton, sCustomComboEdit, sCurrEdit, sCurrencyEdit, sMemo,
  sCheckBox, sGroupBox, StrUtils, DateUtils, Menus, Clipbrd, TypeDefine;

type
  TUI_LOCAPP_frm = class(TChildForm_frm)
    qryBank: TADOQuery;
    dsBank: TDataSource;
    qryList: TADOQuery;
    dsList: TDataSource;
    Pan_bank: TsPanel;
    sPanel6: TsPanel;
    sBitBtn3: TsBitBtn;
    sDBGrid2: TsDBGrid;
    sPanel22: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    qryListMAINT_NO: TStringField;
    qryListMAINT_NO2: TStringField;
    qryListDATEE: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListBUSINESSNAME: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListAPP_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListGOODDES: TStringField;
    qryListREMARK: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListDOC_DTL: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListDESTNAME: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListEXGOOD: TStringField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListLCNO: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListGOODDES1: TStringField;
    qryListREMARK1: TStringField;
    qryListDOC_ETC1: TStringField;
    qryListEXGOOD1: TStringField;
    spCopyLOCAPP: TADOStoredProc;
    qryReady: TADOQuery;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    sPageControl1: TsPageControl;
    sTabSheet2: TsTabSheet;
    sSplitter3: TsSplitter;
    sScrollBox1: TsScrollBox;
    sSpeedButton1: TsSpeedButton;
    sLabel1: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    edt_CreateUserCode: TsEdit;
    sBitBtn4: TsBitBtn;
    edt_CreateUserSangho: TsEdit;
    edt_CreateUserDaepyo: TsEdit;
    edt_CreateUserAddr1: TsEdit;
    edt_CreateUserAddr2: TsEdit;
    edt_CreateUserAddr3: TsEdit;
    edt_CreateUserSaupNo: TsMaskEdit;
    sPanel5: TsPanel;
    sPanel7: TsPanel;
    edt_RecvCode: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_RecvSangho: TsEdit;
    edt_RecvDaepyo: TsEdit;
    edt_RecvAddr1: TsEdit;
    edt_RecvAddr2: TsEdit;
    edt_RecvAddr3: TsEdit;
    edt_RecvSaup: TsMaskEdit;
    edt_RecvEmail1: TsEdit;
    sPanel8: TsPanel;
    edt_RecvEmail2: TsEdit;
    edt_RecvID: TsEdit;
    edt_RecvID_Detail: TsEdit;
    sPanel9: TsPanel;
    edt_CreateBankCode: TsEdit;
    sBitBtn6: TsBitBtn;
    edt_CreateBankName: TsEdit;
    edt_CreateBankBrunch: TsEdit;
    sEdit20: TsEdit;
    sBitBtn7: TsBitBtn;
    sEdit21: TsEdit;
    sEdit22: TsEdit;
    sBitBtn8: TsBitBtn;
    sCurrencyEdit1: TsCurrencyEdit;
    sEdit23: TsEdit;
    sEdit24: TsEdit;
    sEdit25: TsEdit;
    sBitBtn9: TsBitBtn;
    sEdit26: TsEdit;
    sEdit27: TsEdit;
    edt_Sell1: TsEdit;
    edt_Sell2: TsEdit;
    edt_Sell3: TsEdit;
    edt_Sell4: TsEdit;
    edt_Sell5: TsEdit;
    edt_Sell6: TsEdit;
    edt_Sell7: TsEdit;
    edt_Sell8: TsEdit;
    edt_Sell9: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    Curr_AttachDoc1: TsCurrencyEdit;
    Curr_AttachDoc2: TsCurrencyEdit;
    Curr_AttachDoc3: TsCurrencyEdit;
    Curr_AttachDoc4: TsCurrencyEdit;
    Curr_AttachDoc5: TsCurrencyEdit;
    sPanel12: TsPanel;
    Curr_CreateSeq: TsCurrencyEdit;
    Mask_CreateRequestDate: TsMaskEdit;
    sBitBtn10: TsBitBtn;
    sComboBox1: TsComboBox;
    sPanel13: TsPanel;
    Mask_IndoDate: TsMaskEdit;
    sBitBtn11: TsBitBtn;
    Mask_ExpiryDate: TsMaskEdit;
    sBitBtn12: TsBitBtn;
    sEdit37: TsEdit;
    sBitBtn13: TsBitBtn;
    sEdit38: TsEdit;
    sPanel18: TsPanel;
    sTabSheet4: TsTabSheet;
    sSplitter4: TsSplitter;
    sPanel21: TsPanel;
    sLabel16: TsLabel;
    sLabel2: TsLabel;
    sLabel5: TsLabel;
    sLabel56: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sPanel14: TsPanel;
    sMemo1: TsMemo;
    edt_Hs: TsMaskEdit;
    sMemo2: TsMemo;
    sMemo3: TsMemo;
    sPanel15: TsPanel;
    edt_SetupDocumentType: TsEdit;
    sBitBtn14: TsBitBtn;
    sEdit40: TsEdit;
    edt_LetterOfCreditNo: TsEdit;
    sEdit42: TsEdit;
    sBitBtn15: TsBitBtn;
    sCurrencyEdit8: TsCurrencyEdit;
    sMaskEdit7: TsMaskEdit;
    sBitBtn16: TsBitBtn;
    sMaskEdit8: TsMaskEdit;
    sBitBtn17: TsBitBtn;
    sEdit43: TsEdit;
    sBitBtn18: TsBitBtn;
    sEdit44: TsEdit;
    sEdit45: TsEdit;
    sBitBtn19: TsBitBtn;
    sEdit46: TsEdit;
    sEdit47: TsEdit;
    sEdit48: TsEdit;
    edt_ExportAreaCode: TsEdit;
    sBitBtn20: TsBitBtn;
    sEdit50: TsEdit;
    sEdit51: TsEdit;
    sMemo4: TsMemo;
    sPanel17: TsPanel;
    edt_Sign1: TsEdit;
    edt_Sign2: TsEdit;
    edt_Sign3: TsEdit;
    edt_ExportAreaName: TsEdit;
    sBitBtn24: TsBitBtn;
    sPanel23: TsPanel;
    sTabSheet1: TsTabSheet;
    sSplitter2: TsSplitter;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sSplitter1: TsSplitter;
    sSplitter5: TsSplitter;
    sPanel4: TsPanel;
    edt_DocNo1: TsEdit;
    edt_DocNo2: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_DocNo3: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    btn_Cal: TsBitBtn;
    sCheckBox1: TsCheckBox;
    edt_msg1: TsEdit;
    sBitBtn25: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn26: TsBitBtn;
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    btnExit: TsButton;
    sButton2: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sButton1: TsButton;
    sButton3: TsButton;
    sSplitter6: TsSplitter;
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sDBGrid2DblClick(Sender: TObject);
    procedure sDBGrid2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_DocNo2DblClick(Sender: TObject);
    procedure sCurrencyEdit9ButtonClick(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt_CreateUserCodeDblClick(Sender: TObject);
    procedure sBitBtn6Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sEdit20Change(Sender: TObject);
    procedure sBitBtn7Click(Sender: TObject);
    procedure sEdit20DblClick(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure Curr_AttachDoc2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_CreateBankCodeDblClick(Sender: TObject);
    procedure sEdit20KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Mask_CreateRequestDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sBitBtn22Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnCancelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edt_SearchTextKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure qryListCHK1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sButton1Click(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormActivate(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton3Click(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure sBitBtn24Click(Sender: TObject);
    procedure edt_HsDblClick(Sender: TObject);
    procedure sMemo1Change(Sender: TObject);
    procedure memoLimit(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    ProgramControlType : TProgramControlType;
    LOCAPP_SQL : String;
    sKeyField : String;
    procedure NewDocument;
    procedure EditDocument;
    procedure DelDocument;
    procedure ReadDocument;
    procedure SaveDocument(Sender : TObject);
    procedure CancelDocument;
    procedure ReadyDocument(DocNo : String);
    procedure ButtonEnable(Val : Boolean = true);
    function AutoDocNo(onlyReturn : Boolean=False):String;
    procedure OriginalExportEnable(sCode : string);
    procedure Readlist(fromDate, toDate : String;KeyValue : String='');overload;
    procedure Readlist(OrderSyntax : string = '');overload;

    function ValidData:Boolean;

    //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
    procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;
  public
    { Public declarations }
  end;

var
  UI_LOCAPP_frm: TUI_LOCAPP_frm;
Const
  UniqueDocumentCode = 'LOCAPP';
implementation

uses ICON, KISCalendar, Commonlib, MSSQL, CodeContents,
  VarDefine, AutoNo, Dialog_SearchCustom, Dialog_BANK, MessageDefine,
  Dialog_CodeList, SQLCreator, dlg_CreateDocumentChoice, Dialog_CopyLocapp,
  Dlg_RecvSelect, CreateDocuments, DocumentSend, LOCAPP_PRINT, Dialog_HsCodeList;

{$R *.dfm}

procedure TUI_LOCAPP_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_LOCAPP_frm.btn_CalClick(Sender: TObject);
begin
  case (Sender as TsBitBtn).Tag of
    900: sMaskEdit1DblClick(Mask_SearchDate1);
    907: sMaskEdit1DblClick(Mask_SearchDate2);
    901: sMaskEdit1DblClick(sMaskEdit1);
    902: sMaskEdit1DblClick(Mask_CreateRequestDate);
    903: sMaskEdit1DblClick(Mask_IndoDate);
    904: sMaskEdit1DblClick(Mask_ExpiryDate);
    905: sMaskEdit1DblClick(sMaskEdit7);
    906: sMaskEdit1DblClick(sMaskEdit8);
  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  If not (qryList.State = dsInsert) Then Exit;
    
  with Pan_bank do
  begin
    Pan_bank.Tag := (Sender as TsBitBtn).Tag;
    if (Sender as TsBitBtn).Tag = 0 Then
    begin
      Left   := 125;
      Top    := 109;
      Width  := 270;
      Height := 387;

      sDBGrid2.Width := 260;
      sDBGrid2.Height := 343;
      IF not sDBGrid2.DataSource.DataSet.Active Then sDBGrid2.DataSource.DataSet.Open;
      sDBGrid2.DataSource.DataSet.Locate('CODE',edt_DocNo2.Text,[]);
      Pan_bank.Visible := True;
      sDBGrid2.SetFocus;
    end
    else
    if (Sender as TsBitBtn).Tag = 1 Then
    begin
//      Left   := 114;
//      Top    := 208;
//      Width  := 270;
//      Height := 387;
//
//      sDBGrid2.Width := 260;
//      sDBGrid2.Height := 343;
//      IF not sDBGrid2.DataSource.DataSet.Active Then sDBGrid2.DataSource.DataSet.Open;
//      sDBGrid2.DataSource.DataSet.Locate('CODE',sEdit57.Text,[]);
//      Pan_bank.Visible := True;
//      sDBGrid2.SetFocus;
    end
  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  Pan_bank.Visible := False;
  Case Pan_bank.Tag of
    0: If sPageControl1.ActivePageIndex = 1 Then edt_DocNo2.SetFocus;
//    1: If sPageControl1.ActivePageIndex = 2 Then sEdit57.SetFocus;
  end;
  IF sDBGrid2.DataSource.DataSet.Active Then sDBGrid2.DataSource.DataSet.Close;
end;

procedure TUI_LOCAPP_frm.sDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  if Pan_bank.Tag = 0 then
  begin
    edt_DocNo2.Text := sDBGrid2.DataSource.DataSet.FieldByName('CODE').AsString;
    Pan_bank.Visible := False;
    edt_DocNo2.SetFocus;
    IF sDBGrid2.DataSource.DataSet.Active Then sDBGrid2.DataSource.DataSet.Close;
  end
  else
  if Pan_bank.Tag = 1 then
  begin
//    sEdit57.Text := sDBGrid2.DataSource.DataSet.FieldByName('CODE').AsString;
//    Pan_bank.Visible := False;
//    sEdit57.SetFocus;
//    IF sDBGrid2.DataSource.DataSet.Active Then sDBGrid2.DataSource.DataSet.Close;
  end;
end;

procedure TUI_LOCAPP_frm.sDBGrid2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN then sDBGrid2DblClick(nil);
end;

procedure TUI_LOCAPP_frm.edt_DocNo2DblClick(Sender: TObject);
begin
  inherited;
  sBitBtn2Click(sBitBtn2)
end;

procedure TUI_LOCAPP_frm.sCurrencyEdit9ButtonClick(Sender: TObject);
begin
  inherited;
//  (Sender as TsCurrencyEdit).Button.Enabled := False;
  (Sender as TsCurrencyEdit).DoClick;
end;

procedure TUI_LOCAPP_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  IF Pan_bank.Visible Then sBitBtn3Click(sBitBtn3);

//  기존에있던 Visible 이벤트
//  with sPanel4 do
//  begin
//    Visible := not (sPageControl1.ActivePageIndex = 2);
//  end;
//
//  with sPanel22 do
//  begin
//    Visible := not (sPageControl1.ActivePageIndex = 2);
//  end;

  if (Sender as TsPageControl).ActivePageIndex = 2 then
  begin
    //관리번호가있는 패널  숨김
    //sPanel4.Visible := not (sPageControl1.ActivePageIndex = 2);
    //DBGrid3이 위치해 있는 패널 숨김
    sPanel22.Visible := not (sPageControl1.ActivePageIndex = 2);
  end
  else
  begin
    //관리번호가있는 패널
    //sPanel4.Visible := True;
    //DBGrid3이 위치해 있는 패널
     sPanel22.Visible := True;
  end;
  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel3)
  else
    EnabledControlValue(sPanel3,False);


end;

procedure TUI_LOCAPP_frm.NewDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
//  ReadOnlyControlValue(sPanel4,false);
//  ReadOnlyControlValue(sScrollBox1,false);
//  ReadOnlyControlValue(sPanel21,false);
    EnabledControlValue(sPanel4,False);
    EnabledControlValue(sScrollBox1,false);
    EnabledControlValue(sPanel21,false);
//검색패널 읽기전용
//  ReadOnlyControlValue(sPanel24);
    EnabledControlValue(sPanel24);
    EnabledControlValue(sPanel3);

//------------------------------------------------------------------------------
// 버튼설정
//------------------------------------------------------------------------------
  ButtonEnable(false);
//------------------------------------------------------------------------------
// 페이지 초기화
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled :=False;
  sDBGrid3.Enabled := False;
  sPageControl1Change(sPageControl1);
//------------------------------------------------------------------------------
// 초기화
//------------------------------------------------------------------------------
  ClearControlValue(sScrollbox1);
  ClearControlValue(sPanel21);
//------------------------------------------------------------------------------
// 관리번호 생성
//------------------------------------------------------------------------------
  AutoDocNo;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
  qryList.Append;  
//------------------------------------------------------------------------------
// 등록일자 설정
//------------------------------------------------------------------------------
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
//------------------------------------------------------------------------------
// 문서기능,유형
//------------------------------------------------------------------------------
   edt_msg1.Text := '9';
   edt_msg2.Text := 'AB';
//------------------------------------------------------------------------------
// 개설의뢰인 셋팅
//------------------------------------------------------------------------------
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    edt_CreateUserCode.Text   :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    edt_CreateUserSangho.Text  :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_CreateUserDaepyo.Text  :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_CreateUserAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    edt_CreateUserAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    edt_CreateUserAddr3.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    edt_CreateUserSaupNo.Text  := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
  end;
//------------------------------------------------------------------------------
// 사용자
//------------------------------------------------------------------------------
  edt_UserNo.Text := LoginData.sID;
//------------------------------------------------------------------------------
// 주요구비서류
//------------------------------------------------------------------------------
  Curr_AttachDoc1.Value := 1;
  Curr_AttachDoc2.Value := 1;
  Curr_AttachDoc3.Value := 0;
  Curr_AttachDoc4.Value := 1;
  Curr_AttachDoc5.Value := 1;
//------------------------------------------------------------------------------
// 개설관련
//------------------------------------------------------------------------------
  //개설회차
  Curr_CreateSeq.Value := 1;
  //개설신청일자
  Mask_CreateRequestDate.Text := FormatDateTime('YYYYMMDD',Now);
  //물품인도기일
  Mask_IndoDate.Text := FormatDateTime('YYYYMMDD',Now);
  //유효기일
  Mask_ExpiryDate.Text := FormatDateTime('YYYYMMDD',Now);
  //분할허용여부
  sEdit37.Text := '9';
//------------------------------------------------------------------------------
// 전자서명
//------------------------------------------------------------------------------
  edt_Sign1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_Sign2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_Sign3.Text := DMCodeContents.ConfigSIGN.AsString;

end;

procedure TUI_LOCAPP_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  dlg_CreateDocumentChoice_frm := Tdlg_CreateDocumentChoice_frm.Create(Self);
  try
    Case dlg_CreateDocumentChoice_frm.ShowModal of
      mrOk : NewDocument;
//      mrYes : CopyDocument;
    end;

  finally
    FreeAndNil( dlg_CreateDocumentChoice_frm );
  end;
end;

procedure TUI_LOCAPP_frm.FormShow(Sender: TObject);
begin
  inherited;
  DMCodeContents.OPENTABLE(DMCodeContents.Config);
  IF DMCodeContents.ConfigAutoGubun.AsInteger = 0 Then
    edt_DocNo3.Color := clWhite
  else
    edt_DocNo3.Color := clBtnFace;

  edt_UserNo.Text := LoginData.sID;
  sPageControl1Change(sPageControl1);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist;

  //메모 행열수 보여주는 레이블
  sLabel56.Caption := '';
  sLabel6.Caption := '';
  sLabel7.Caption := '';
  sLabel9.Caption := '';
end;

procedure TUI_LOCAPP_frm.edt_CreateUserCodeDblClick(Sender: TObject);
begin
  inherited;
  If (Sender as TsEdit).ReadOnly Then Exit;
  
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Application);
  try
    IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOk then
    begin
      IF (Sender as TsEdit).Tag = 0 Then
      begin
        edt_CreateUserCode.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        edt_CreateUserSangho.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_CreateUserDaepyo.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        edt_CreateUserAddr1.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
        edt_CreateUserAddr2.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR2').AsString;
        edt_CreateUserAddr3.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR3').AsString;
        edt_CreateUserSaupNo.Text := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
      end
      else
      IF (Sender as TsEdit).Tag = 1 Then
      begin
        edt_RecvCode.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        edt_RecvSangho.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_RecvDaepyo.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        edt_RecvAddr1.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
        edt_RecvAddr2.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR2').AsString;
        edt_RecvAddr3.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR3').AsString;
        edt_RecvSaup.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
        edt_RecvEmail1.Text := Dialog_SearchCustom_frm.qryList.FieldByName('EMAIL_ID').AsString;
        edt_RecvEmail2.Text := Dialog_SearchCustom_frm.qryList.FieldByName('EMAIL_DOMAIN').AsString;
      end
      else
      IF (Sender as TsEdit).Tag = 2 Then
      begin
        sEdit45.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        sEdit46.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        sEdit47.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        sEdit48.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
      end
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;

function TUI_LOCAPP_frm.AutoDocNo(onlyReturn : Boolean):String;
var
  AutoNumbering : TAutoNumbering;
begin
  Result := '';
//------------------------------------------------------------------------------
// 문서 자동사용확인
//------------------------------------------------------------------------------
  AutoNumbering := DMCodeContents.AutoNumberingForLOCAPP;

  if not onlyReturn Then
  begin
    //------------------------------------------------------------------------------
    // 신규버튼을 눌렀을경우 자동으로 EDIT에 관리번호를 넣는다
    //------------------------------------------------------------------------------
    edt_DocNo1.ReadOnly := (AutoNumbering.LOCAPP_NO <> '');

    IF edt_DocNo1.ReadOnly Then
      edt_DocNo1.Color := clBtnFace
    else
      edt_DocNo1.Color := clWhite;

    IF AutoNumbering.LOCAPP_NO <> '' Then
      edt_DocNo1.Text := 'LOCAPP'+AutoNumbering.LOCAPP_NO
    else
      edt_DocNo1.Text := 'LOCAPP';

    edt_DocNo2.Clear;
    IF AutoNumbering.LOCAPP_BANK <> '' Then
      edt_DocNo2.Text := AutoNumbering.LOCAPP_BANK;

    edt_DocNo3.Text := DMAutoNo.GetDocumentNoAutoInc('LOCAPP');
    edt_DocNo3.ReadOnly := not(Trim(edt_DocNo3.Text) = '');
    IF edt_DocNo3.ReadOnly Then edt_DocNo3.Color := clBtnFace;

    IF DMCodeContents.ConfigAutoGubun.AsInteger <> 0 Then
    begin
      IF qryList.State = dsInsert Then
      begin
        qryListMAINT_NO.Text := edt_DocNo1.Text+'-'+edt_DocNo2.Text+edt_DocNo3.Text;
      end;
    end;
  end
  else
  begin
    //------------------------------------------------------------------------------
    // onlyReturn는 EDIT에 자동으로 넣지않고 리턴값으로 관리번호를 받는다(COPY용)
    //------------------------------------------------------------------------------
    IF AutoNumbering.LOCAPP_NO = '' Then
      raise Exception.Create('식별자가 지정되어있지 않습니다. 식별자를 입력하세요');

    Result := 'LOCAPP'+AutoNumbering.LOCAPP_NO+'-'+DMAutoNo.GetDocumentNoAutoInc('LOCAPP');
  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn6Click(Sender: TObject);
begin
  inherited;
  Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    If Dialog_BANK_frm.openDialog(edt_CreateBankCode.Text) = mrOk Then
    begin
      edt_CreateBankCode.Text := Dialog_BANK_frm.qryBank.FieldByName('CODE').AsString;
      edt_CreateBankName.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME1').AsString;
      edt_CreateBankBrunch.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH1').AsString;
    end;
  finally
    FreeAndNil(Dialog_BANK_frm);
  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn4Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsBitBtn).Tag of
    0: edt_CreateUserCodeDblClick(edt_CreateUserCode);
    1: edt_CreateUserCodeDblClick(edt_RecvCode);
    2: edt_CreateUserCodeDblClick(sEdit45);
  end;
end;

procedure TUI_LOCAPP_frm.sEdit20Change(Sender: TObject);
var
  CodeRecord : TCodeRecord;
  sGroup, sCode : string;
begin
  inherited;
//    IF (Sender as TsEdit).ReadOnly Then Exit;
//  If (Sender as TsEdit).MaxLength <= Length((Sender as TsEdit).Text) Then
//  begin
    sGroup := (Sender as TsEdit).Hint;
    sCode  := (Sender as TsEdit).Text;
    IF sGroup = '' Then
    begin
      MessageBox(Self.Handle,MSG_GETCODE_ERROR_EMPTY_HINT,'ERROR',MB_OK+MB_ICONERROR);
      Exit;
    end;

    CodeRecord := DMCodeContents.getCodeName(sGroup,sCode);

    Case (Sender as TsEdit).Tag of
      101 : sEdit21.Text := CodeRecord.sName;
      102 :;//개설급액 단위임
      103 : begin
              sEdit26.Text := CodeRecord.sName;
              OriginalExportEnable(sCode);
            end;
      104 : sEdit38.Text := CodeRecord.sName;
      105 : sEdit40.Text := CodeRecord.sName;
      106 :;
      107 : sEdit44.Text := CodeRecord.sName;
      108 : edt_ExportAreaName.Text := CodeRecord.sName;
    end;

//  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn7Click(Sender: TObject);
begin
  inherited;
  Case (sender as TsBitBtn).Tag of
    101: sEdit20DblClick(sEdit20);
    102: sEdit20DblClick(sEdit22);
    103: sEdit20DblClick(sEdit25);
    104: sEdit20DblClick(sEdit37);
    105: sEdit20DblClick(edt_SetupDocumentType);
    106: sEdit20DblClick(sEdit42);
    107: sEdit20DblClick(sEdit43);
    108: sEdit20DblClick(edt_ExportAreaCode);
    109: sEdit20DblClick(edt_msg1);
    110: sEdit20DblClick(edt_msg2);
  end;
end;

procedure TUI_LOCAPP_frm.sEdit20DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;
  
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
    begin
      (Sender as TsEdit).Text := Dialog_CodeList_frm.qryList.FieldByName('CODE').AsString;

      case (Sender as TsEdit).Tag of
        101: sEdit21.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
        102:;
        103: sEdit26.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
        104: sEdit38.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
        105: sEdit40.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
        106:;
        107: sEdit44.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
        108: edt_ExportAreaName.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_LOCAPP_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  //IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCAPP_frm.Curr_AttachDoc2Change(Sender: TObject);
begin
  inherited;
  case (Sender as TsCurrencyEdit).Tag of
    0: Curr_AttachDoc4.Value := 1 - Curr_AttachDoc3.Value;
    1: Curr_AttachDoc3.Value := 1 - Curr_AttachDoc4.Value;
  end;
end;

procedure TUI_LOCAPP_frm.OriginalExportEnable(sCode: string);
var
  bValue : Boolean;
begin
  bValue := AnsiMatchText(UpperCase(sCode),['2AA','2AB','2AC','2AJ','2AK','2AO']);
  //개설근거서류종류
  edt_SetupDocumentType.Enabled := bValue;
  edt_SetupDocumentType.Clear;
  sBitBtn14.Enabled := bValue;
  sEdit40.Clear;
  //신용장(계약서)번호
  edt_LetterOfCreditNo.Enabled := bValue;
  edt_LetterOfCreditNo.Clear;
  //결제금액
  sEdit42.Enabled := bValue;
  sEdit42.Clear;
  sCurrencyEdit8.Enabled := bValue;
  sCurrencyEdit8.Value := 0;
  sBitBtn15.Enabled := bValue;
  //선적기일
  sMaskEdit7.Enabled := bValue;
  sMaskEdit7.Clear;
  sBitBtn16.Enabled := bValue;
  //유효기일
  sMaskEdit8.Enabled := bValue;
  sMaskEdit8.Clear;
  sBitBtn17.Enabled := bValue;
  //대금결제조건
  sEdit43.Enabled   := bValue;
  sEdit43.Clear;
  sBitBtn18.Enabled := bValue;
  sEdit44.Clear;
  //수출공급상대방
  sEdit45.Enabled := bValue;
  sEdit45.Clear;
  sEdit46.Enabled := bValue;
  sEdit46.Clear;
  sEdit47.Enabled := bValue;
  sEdit47.Clear;
  sEdit48.Enabled := bValue;
  sEdit48.Clear;
  sBitBtn19.Enabled := bValue;
  //수출지역
  edt_ExportAreaCode.Enabled := bValue;
  edt_ExportAreaCode.Clear;
  edt_ExportAreaName.Clear;
  sBitBtn20.Enabled := bValue;
  //발행은행(확인기관)
  sEdit50.Enabled := bValue;
  sEdit50.Clear;
  sEdit51.Enabled := bValue;
  sEdit51.Clear;
end;

procedure TUI_LOCAPP_frm.FormCreate(Sender: TObject);
begin
  inherited;
  LOCAPP_SQL := self.qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
  ProgramControlType := ctView;
end;

procedure TUI_LOCAPP_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  If ProgramControlType = ctView Then
  begin
    Case Key of
      VK_F1 : sPageControl1.ActivePageIndex := 0;
      Vk_F2 : sPageControl1.ActivePageIndex := 1;
      VK_F3 : sPageControl1.ActivePageIndex := 2;
    end;
    sPageControl1Change(sPageControl1);
  end;
end;

procedure TUI_LOCAPP_frm.edt_CreateBankCodeDblClick(Sender: TObject);
begin
  inherited;
  Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    If Dialog_BANK_frm.openDialog(edt_CreateBankCode.Text) = mrOk Then
    begin
      edt_CreateBankCode.Text := Dialog_BANK_frm.qryBank.FieldByName('CODE').AsString;
      edt_CreateBankName.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME1').AsString;
      edt_CreateBankBrunch.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH1').AsString;
    end;
  finally
    FreeAndNil(Dialog_BANK_frm);
  end;
end;

procedure TUI_LOCAPP_frm.sEdit20KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_F11 Then sEdit20DblClick(Sender);
end;

procedure TUI_LOCAPP_frm.Mask_CreateRequestDateKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_F11 Then sMaskEdit1DblClick(Sender);
end;

procedure TUI_LOCAPP_frm.Readlist(fromDate, toDate, KeyValue : String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := LOCAPP_SQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;
    
    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;
end;

procedure TUI_LOCAPP_frm.Readlist(OrderSyntax : string);
begin
//------------------------------------------------------------------------------
//0 개설일자
//1 관리번호
//2 수혜자
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// 관리번호 및 수혜자는 입력안할시 엄청난 양의 데이터를
// 검색하기 때문에 꼭 키워드를 입력해야함
//------------------------------------------------------------------------------
  IF com_SearchKeyword.ItemIndex in [1,2] Then
  begin
    IF Trim(edt_SearchText.Text) = '' Then
    begin
      IF sPageControl1.ActivePageIndex = 2 Then edt_SearchText.SetFocus;
      
      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  
  with qryList do
  begin
    Close;
    SQL.Text := LOCAPP_SQL;
    Case com_SearchKeyword.ItemIndex of
      0: begin
           SQL.Add('WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
           Mask_fromDate.Text := Mask_SearchDate1.Text;
           Mask_toDate.Text := Mask_SearchDate2.Text;
         end;
      1: SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    IF OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;
  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
end;

procedure TUI_LOCAPP_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
//  ReadOnlyControlValue(sPanel4);
//  ReadOnlyControlValue(sScrollBox1);
//  ReadOnlyControlValue(sPanel21);
  If DataSet.RecordCount = 0 Then ReadDocument;

end;

procedure TUI_LOCAPP_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist;
end;

procedure TUI_LOCAPP_frm.sDBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  if sKeyField <> Column.FieldName Then
  begin
    sKeyField := Column.FieldName;
    Readlist('ORDER BY '+sKeyField);
    qryList.Tag := 0;
  end
  else
  begin
    IF qryList.Tag = 0 Then
    begin
      Readlist('ORDER BY '+sKeyField+' DESC');
      qryList.Tag := 1;
    end
    else
    begin
      Readlist('ORDER BY '+sKeyField);
      qryList.Tag := 0;
    end;
  end;
end;

procedure TUI_LOCAPP_frm.ReadDocument;
begin
//------------------------------------------------------------------------------
// 읽기전용
//------------------------------------------------------------------------------
//  ReadOnlyControlValue(sPanel4);
//  ReadOnlyControlValue(sScrollBox1);
//  ReadOnlyControlValue(sPanel21);
  EnabledControlValue(sPanel4);
  EnabledControlValue(sScrollBox1);
  EnabledControlValue(sPanel21);

//  ReadOnlyControlValue(sPanel24,False);
  EnabledControlValue(sPanel24,False);
//------------------------------------------------------------------------------
// 데이터 가져오기
//------------------------------------------------------------------------------
  //관리번호
  edt_DocNo1.Text := LeftStr( qryListMAINT_NO.AsString , 14);
  edt_DocNo2.Text := MidStr( qryListMAINT_NO.AsString, 16,2 );
  edt_DocNo3.Text := MidStr( qryListMAINT_NO.AsString, 18,20);
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  //개설의뢰인 코드
  edt_CreateUserCode.Text := qryListAPPLIC.AsString;
  //상호
  edt_CreateUserSangho.Text := qryListAPPLIC1.AsString;
  //대표자
  edt_CreateUserDaepyo.Text := qryListAPPLIC2.AsString;
  //주소
  edt_CreateUserAddr1.Text := qryListAPPADDR1.AsString;
  edt_CreateUserAddr2.Text := qryListAPPADDR2.AsString;
  edt_CreateUserAddr3.Text := qryListAPPADDR3.AsString;
  //사업자등록번호
  edt_CreateUserSaupNo.Text := qryListAPPLIC3.AsString;
//------------------------------------------------------------------------------
// 수혜자
//------------------------------------------------------------------------------
  //수혜자코드
  edt_RecvCode.Text := qryListBENEFC.AsString;
  //수혜자상호
  edt_RecvSangho.Text := qryListBENEFC1.AsString;
  //대표자
  edt_RecvDaepyo.Text := qryListBENEFC2.AsString;
  //수혜자주소
  edt_RecvAddr1.Text := qryListBNFADDR1.AsString;
  edt_RecvAddr2.Text := qryListBNFADDR2.AsString;
  edt_RecvAddr3.Text := qryListBNFADDR3.AsString;
  //사업자등록번호
  edt_RecvSaup.Text := qryListBENEFC3.AsString;
  //이메일주소
  edt_RecvEmail1.Text := qryListBNFEMAILID.AsString;
  edt_RecvEmail2.Text := qryListBNFDOMAIN.AsString;
  //수발신인 식별자
  edt_RecvID.Text := qryListCHKNAME1.AsString;
  //상세 수발신인 식별자
  edt_RecvID_Detail.Text := qryListCHKNAME2.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  edt_CreateBankCode.Text := qryListAP_BANK.AsString;
  edt_CreateBankName.Text := qryListAP_BANK1.AsString;
  edt_CreateBankBrunch.Text := qryListAP_BANK2.AsString;
  //신용장종류
  sEdit20.Text := qryListLOC_TYPE.AsString;
  //개설금액(단위)
  sEdit22.Text := qryListLOC_AMTC.AsString;
  sCurrencyEdit1.Value := qryListLOC_AMT.AsCurrency;
  //허용오차
  sEdit23.Text := qryListCD_PERP.AsString;
  sEdit24.Text := qryListCD_PERM.AsString;
  //개설근거별 용도
  sEdit25.Text := qryListBUSINESS.AsString;
  //L/C 번호
  sEdit27.Text := qryListLCNO.AsString;
//------------------------------------------------------------------------------
// 매도확약서번호
//------------------------------------------------------------------------------
  edt_Sell1.Text := qryListOFFERNO1.AsString;
  edt_Sell2.Text := qryListOFFERNO2.AsString;
  edt_Sell3.Text := qryListOFFERNO3.AsString;
  edt_Sell4.Text := qryListOFFERNO4.AsString;
  edt_Sell5.Text := qryListOFFERNO5.AsString;
  edt_Sell6.Text := qryListOFFERNO6.AsString;
  edt_Sell7.Text := qryListOFFERNO7.AsString;
  edt_Sell8.Text := qryListOFFERNO8.AsString;
  edt_Sell9.Text := qryListOFFERNO9.AsString;
//------------------------------------------------------------------------------
// 첨부서류
//------------------------------------------------------------------------------
  Curr_AttachDoc1.Value := qryListDOCCOPY1.AsInteger;
  Curr_AttachDoc2.Value := qryListDOCCOPY2.AsInteger;
  Curr_AttachDoc3.Value := qryListDOCCOPY3.AsInteger;
  Curr_AttachDoc4.Value := qryListDOCCOPY4.AsInteger;
  Curr_AttachDoc5.Value := qryListDOCCOPY5.AsInteger;
//------------------------------------------------------------------------------
// 개설관련
//------------------------------------------------------------------------------
  //개설회차
  Curr_CreateSeq.Value := qryListOPEN_NO.AsInteger;
  //개설신청일자
  Mask_CreateRequestDate.Text := qrylistAPP_DATE.AsString;
  //물품인도기일
  Mask_IndoDate.Text := qryListDELIVERY.AsString;
  //유효기일
  Mask_ExpiryDate.Text := qryListEXPIRY.AsString;
  //분할허용여부
  sEdit37.Text := qrylistTRANSPRT.AsString;
//------------------------------------------------------------------------------
// 서류제시기간
//------------------------------------------------------------------------------
  case qryListDOC_PRD.AsInteger of
    5: sComboBox1.ItemIndex := 0;
    7: sComboBox1.ItemIndex := 1;
  end;
//------------------------------------------------------------------------------
// 대표공급물품
//------------------------------------------------------------------------------
  //HS부호
  edt_Hs.Text := qryListBSN_HSCODE.AsString;
  //대표공급물품명
  sMemo1.Text := qrylistGOODDES1.AsString;
  //기타구비서류
  sMemo2.Text := qrylistDOC_ETC1.AsString;
  //기타정보
  sMemo3.Text := qryListREMARK1.AsString;
//------------------------------------------------------------------------------
// 원수출신용장
//------------------------------------------------------------------------------
  //개설근거서류
  edt_SetupDocumentType.Text := qryListDOC_DTL.AsString;
  //신용장(계약서)번호
  edt_LetterOfCreditNo.Text := qryListDOC_NO.AsString;
  //결제금액
  sEdit42.Text := qryListDOC_AMTC.AsString;
  sCurrencyEdit8.Value := qryListDOC_AMT.AsCurrency;
  //선적기일
  sMaskEdit7.Text := qryListLOADDATE.AsString;
  //유효기일
  sMaskEdit8.Text := qryListEXPDATE.AsString;
  //대금결제조건
  sEdit43.Text := qrylistPAYMENT.AsString;
  //수출공급상대방
  sEdit45.Text:= qrylistIM_NAME.AsString;
  sEdit46.Text:= qrylistIM_NAME1.AsString;
  sEdit47.Text:= qrylistIM_NAME2.AsString;
  sEdit48.Text:= qrylistIM_NAME3.AsString;
  //수출지역
  edt_ExportAreaCode.Text := qrylistDEST.AsString;
  //발행기관
  sEdit50.Text := qryListISBANK1.AsString;
  sEdit51.Text := qryListISBANK2.AsString;
  //대표수출품목
  sMemo4.Text := qryListEXGOOD1.AsString;
//------------------------------------------------------------------------------
// 발신기관 전자서명
//------------------------------------------------------------------------------
  edt_Sign1.Text := qrylistEXNAME1.AsString;
  edt_Sign2.Text := qrylistEXNAME2.AsString;
  edt_Sign3.Text := qrylistEXNAME3.AsString;
end;

procedure TUI_LOCAPP_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  IF qryList.State = dsBrowse Then ReadDocument;
  //메모 행열 출력
  sLabel56.Caption := '';
  sLabel6.Caption := '';
  sLabel7.Caption := '';
  sLabel9.Caption := '';
end;

procedure TUI_LOCAPP_frm.SaveDocument(Sender : TObject);
var
  SQLCreate : TSQLCreate;
  DocNo : String;
  CreateDate : TDateTime;
begin
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  try
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    IF (Sender as TsButton).Tag = 1 Then
    begin
      IF ValidData Then Exit;
    end;

//------------------------------------------------------------------------------
// SQL생성기
//------------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;
    try
      with SQLCreate do
      begin
        DocNo := edt_DocNo1.Text+'-'+edt_DocNo2.Text+edt_DocNo3.Text;

        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
          end;
        end;

        //------------------------------------------------------------------------------
        // 문서헤더
        //------------------------------------------------------------------------------
        SQLHeader('LOCAPP');        

        //문서저장구분(0:임시, 1:저장)
        ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));

        //등록일자
        ADDValue('DATEE',sMaskEdit1.Text);

        //유저ID
        ADDValue('USER_ID',edt_UserNo.Text);

        //메세지1
        ADDValue('MESSAGE1',edt_msg1.Text);
        //메세지2
        ADDValue('MESSAGE2',edt_msg2.Text);

        //------------------------------------------------------------------------------
        // 개설의뢰인
        //------------------------------------------------------------------------------
        //개설의뢰인코드
        ADDValue('APPLIC',edt_CreateUserCode.Text);
        //상호
        ADDValue('APPLIC1',edt_CreateUserSangho.Text);
        //대표자명
        ADDValue('APPLIC2',edt_CreateUserDaepyo.Text);
        //주소
        ADDValue('APPADDR1',edt_CreateUserAddr1.Text);
        ADDValue('APPADDR2',edt_CreateUserAddr2.Text);
        ADDValue('APPADDR3',edt_CreateUserAddr3.Text);
        //사업자등록번호
        ADDValue('APPLIC3',edt_CreateUserSaupNo.Text);

        //------------------------------------------------------------------------------
        // 수혜자
        //------------------------------------------------------------------------------
        //수혜자코드
        ADDValue('BENEFC',edt_RecvCode.Text);
        //상호
        ADDValue('BENEFC1',edt_RecvSangho.Text);
        //대표자
        ADDValue('BENEFC2',edt_RecvDaepyo.Text);
        //수혜자주소
        ADDValue('BNFADDR1',edt_RecvAddr1.Text);
        ADDValue('BNFADDR2',edt_RecvAddr2.Text);
        ADDValue('BNFADDR3',edt_RecvAddr3.Text);
        //사업자등록번호
        ADDValue('BENEFC3',edt_RecvSaup.Text);
        //이메일
        ADDValue('BNFEMAILID',edt_RecvEmail1.Text);
        ADDValue('BNFDOMAIN',edt_RecvEmail2.Text);
        //수발신인식별자
        ADDValue('CHKNAME1',edt_RecvID.Text);
        //수발신인 상세 식별자
        ADDValue('CHKNAME2',edt_RecvID_Detail.Text);

        //------------------------------------------------------------------------------
        // 개설은행/신용장
        //------------------------------------------------------------------------------
        //개설은행
        ADDValue('AP_BANK',edt_CreateBankCode.Text);
        ADDValue('AP_BANK1',edt_CreateBankName.Text);
        ADDValue('AP_BANK2',edt_CreateBankBrunch.Text);
        //신용장종류
        ADDValue('LOC_TYPE',sEdit20.Text);
        //개설금액(단위/금액)
        ADDValue('LOC_AMTC',sEdit22.Text);
        ADDValue('LOC_AMT',sCurrencyEdit1.Text);
        //허용오차
        ADDValue('CD_PERP',sEdit23.Text);
        ADDValue('CD_PERM',sEdit24.Text);
        //개설근거별용도
        ADDValue('BUSINESS',sEdit25.Text);
        //L/C번호
        ADDValue('LCNO',sEdit27.Text);

        //------------------------------------------------------------------------------
        // 물품매도확약서
        //------------------------------------------------------------------------------
        ADDValue('OFFERNO1',edt_Sell1.Text);
        ADDValue('OFFERNO2',edt_Sell2.Text);
        ADDValue('OFFERNO3',edt_Sell3.Text);
        ADDValue('OFFERNO4',edt_Sell4.Text);
        ADDValue('OFFERNO5',edt_Sell5.Text);
        ADDValue('OFFERNO6',edt_Sell6.Text);
        ADDValue('OFFERNO7',edt_Sell7.Text);
        ADDValue('OFFERNO8',edt_Sell8.Text);
        ADDValue('OFFERNO9',edt_Sell9.Text);

        //------------------------------------------------------------------------------
        // 주요구비서류
        //------------------------------------------------------------------------------
        ADDValue('DOCCOPY1',Curr_AttachDoc1.Text);
        ADDValue('DOCCOPY2',Curr_AttachDoc2.Text);
        ADDValue('DOCCOPY3',Curr_AttachDoc3.Text);
        ADDValue('DOCCOPY4',Curr_AttachDoc4.Text);
        ADDValue('DOCCOPY5',Curr_AttachDoc5.Text);

        //------------------------------------------------------------------------------
        // 개설관련
        //------------------------------------------------------------------------------
        //개설회차
        ADDValue('OPEN_NO',Curr_CreateSeq.Text);
        //개설신청일자
        ADDValue('APP_DATE',Mask_CreateRequestDate.Text);
        //인도기일
        ADDValue('DELIVERY',Mask_IndoDate.Text);
        //유효기일
        ADDValue('EXPIRY',Mask_ExpiryDate.Text);
        //분할허용여부
        ADDValue('TRANSPRT',sEdit37.Text);

        //------------------------------------------------------------------------------
        // 서류제시기간
        //------------------------------------------------------------------------------
        ADDValue('DOC_PRD',AnsiReplaceText(sComboBox1.Text,'일',''));
        
        //------------------------------------------------------------------------------
        // 대표공급물품
        //------------------------------------------------------------------------------
        //HS부호
        ADDValue('BSN_HSCODE',edt_Hs.Text);
        //공급물품명
        ADDValue('GOODDES1',sMemo1.Text);
        //기타구비서류
        ADDValue('DOC_ETC1',sMemo2.Text);
        //기타정보
        ADDValue('REMARK1',sMemo3.Text);

        //------------------------------------------------------------------------------
        // 원수출 신용장
        //------------------------------------------------------------------------------
        //개설근거서류
        ADDValue('DOC_DTL',edt_SetupDocumentType.Text);
        //신용장(계약서)번호
        ADDValue('DOC_NO',edt_LetterOfCreditNo.Text);
        //결제금액
        ADDValue('DOC_AMTC',sEdit42.Text);
        ADDValue('DOC_AMT',sCurrencyEdit8.Text);
        //선적기일
        ADDValue('LOADDATE',sMaskEdit7.Text);
        //유효기일
        ADDValue('EXPDATE',sMaskEdit8.Text);
        //대금결제조건
        ADDValue('PAYMENT',sEdit43.Text);
        //수출공급상대방
        ADDValue('IM_NAME',sEdit45.Text);
        ADDValue('IM_NAME1',sEdit46.Text);
        ADDValue('IM_NAME2',sEdit47.Text);
        ADDValue('IM_NAME3',sEdit48.Text);
        //수출지역
        ADDValue('DEST',edt_ExportAreaCode.Text);
        //발행은행(확인기관)
        ADDValue('ISBANK1',sEdit50.Text);
        ADDValue('ISBANK2',sEdit50.Text);
        //대표수출품목
        ADDValue('EXGOOD1',sMemo4.Text);

        //------------------------------------------------------------------------------
        // 발신기관전자서명
        //------------------------------------------------------------------------------
        ADDValue('EXNAME1',edt_Sign1.Text);
        ADDValue('EXNAME2',edt_Sign2.Text);
        ADDValue('EXNAME3',edt_Sign3.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          If sCheckBox1.Checked Then
            Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    finally
      SQLCreate.Free;
    end;

    DMMssql.KISConnect.CommitTrans;
  //------------------------------------------------------------------------------
  // 프로그램제어
  //------------------------------------------------------------------------------
    ProgramControlType := ctView;
  //------------------------------------------------------------------------------
  // 접근제어
  //------------------------------------------------------------------------------
    sDBGrid1.Enabled := True;
    sDBGrid3.Enabled := True;
    sPageControl1.Pages[2].Enabled := True;

  //------------------------------------------------------------------------------
  // 읽기전용 해제
  //------------------------------------------------------------------------------
//    ReadOnlyControlValue(sPanel4);
//    ReadOnlyControlValue(sScrollBox1);
//    ReadOnlyControlValue(sPanel21);
    EnabledControlValue(sPanel4);
    EnabledControlValue(sScrollBox1);
    EnabledControlValue(sPanel21);

  //검색패널 읽기전용
//    ReadOnlyControlValue(sPanel24,False);
    EnabledControlValue(sPanel24,False);
    EnabledControlValue(sPanel3 , False);
  //------------------------------------------------------------------------------
  // 버튼설정
  //------------------------------------------------------------------------------
    ButtonEnable(True);

  //------------------------------------------------------------------------------
  // 새로고침
  //------------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);

    Case (Sender as TsButton).Tag of
      0:MessageBox(Self.Handle,MSG_SYSTEM_SAVETEMP_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);    
      1:MessageBox(Self.Handle,MSG_SYSTEM_SAVE_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);
    end;
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
//      DMMssql.KISConnect.RollbackTrans;
    end;
  end;
end;

procedure TUI_LOCAPP_frm.EditDocument;
begin
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled :=False;
  sDBGrid3.Enabled := False;
  sPageControl1.Pages[2].Enabled := false;

//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
  //ReadOnlyControlValue(sPanel4);
  EnabledControlValue(sPanel4);
  //수정시에는 등록일자 수정을 할 수 도 있음
  sMaskEdit1.ReadOnly := False;
  btn_Cal.Enabled := True;
  //수정시에는 문서기능 , 유형을 수정할수있음
  edt_msg1.ReadOnly := False;
  sBitBtn25.Enabled := True;

  edt_msg2.ReadOnly := False;
  sBitBtn26.Enabled := True;

//  ReadOnlyControlValue(sScrollBox1,False);
//  ReadOnlyControlValue(sPanel21,False);
//
//  ReadOnlyControlValue(sPanel24);
  EnabledControlValue(sScrollBox1,False);
  EnabledControlValue(sPanel21,False);

  EnabledControlValue(sPanel24);
  EnabledControlValue(sPanel3);

//------------------------------------------------------------------------------
// 버튼설정
//------------------------------------------------------------------------------
  ButtonEnable(false);
//------------------------------------------------------------------------------
// 페이지 초기화
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
  sPageControl1Change(sPageControl1);

end;

procedure TUI_LOCAPP_frm.CancelDocument;
begin
  qryList.Cancel;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
    
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled := True;
  sDBGrid3.Enabled := True;
  sPageControl1.Pages[2].Enabled := True;

//------------------------------------------------------------------------------
// 읽기전용
//------------------------------------------------------------------------------
//  ReadOnlyControlValue(sPanel4);
//  ReadOnlyControlValue(sScrollBox1);
//  ReadOnlyControlValue(sPanel21);

  EnabledControlValue(sPanel4);
  EnabledControlValue(sScrollBox1);
  EnabledControlValue(sPanel21);
//검색패널 읽기전용해제
//  ReadOnlyControlValue(sPanel24,False);
  EnabledControlValue(sPanel24,False);
  EnabledControlValue(sPanel3,False);
//------------------------------------------------------------------------------
// 버튼설정
//------------------------------------------------------------------------------
  ButtonEnable(True);

  if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.RollbackTrans;

  qryList.Close;
  qryList.Open;

end;

procedure TUI_LOCAPP_frm.ButtonEnable(Val: Boolean);
begin
  sButton2.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;
end;

procedure TUI_LOCAPP_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  CancelDocument;
end;

procedure TUI_LOCAPP_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  IF AnsiMatchText(qryListCHK2.AsString,['','0','1','5','6']) Then
    EditDocument
  else
  begin
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가',MB_OK+MB_ICONINFORMATION);
  end;
end;

function TUI_LOCAPP_frm.ValidData: Boolean;
var
  sComponentName : String;
begin
  Result := False;
//------------------------------------------------------------------------------
// 관리번호 관련
//------------------------------------------------------------------------------
  sComponentName := CheckEmptyValue(sScrollbox1);

  IF sComponentName <> '' then         
  begin
    MessageBox(Self.Handle,PChar(sComponentName+'를 입력하세요'),'저장오류',MB_OK+MB_ICONERROR);
  end;
  Result := sComponentName <> '';

//  IF isEmptyText(edt_DocNo1.Text)Then
//  begin
//    edt_DocNo1.SetFocus;
//    MessageBox(Self.Handle,MSG_LOCAPP_ERR_EMPTY_DOCNO1,'저장오류',MB_OK+MB_ICONERROR);
//    Result := True;
//  end;
//  IF isEmptyText(edt_DocNo2.Text)Then
//  begin
//    edt_DocNo2.SetFocus;
//    MessageBox(Self.Handle,MSG_LOCAPP_ERR_EMPTY_DOCNO2,'저장오류',MB_OK+MB_ICONERROR);
//    Result := True;
//  end;
//  IF isEmptyText(edt_DocNo3.Text)Then
//  begin
//    edt_DocNo2.SetFocus;
//    MessageBox(Self.Handle,MSG_LOCAPP_ERR_EMPTY_DOCNO3,'저장오류',MB_OK+MB_ICONERROR);
//    Result := True;
//  end;
end;

procedure TUI_LOCAPP_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

procedure TUI_LOCAPP_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
inherited;
//
end;

procedure TUI_LOCAPP_frm.edt_SearchTextKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = Vk_RETURN THEN Readlist;
end;

procedure TUI_LOCAPP_frm.DelDocument;
var
  DocNo : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := edt_DocNo1.Text+'-'+edt_DocNo2.Text+edt_DocNo3.Text;
    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCAPP WHERE MAINT_NO = '+QuotedStr(DocNo);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;

          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans; 
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCAPP_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;
  
  DelDocument;
end;

procedure TUI_LOCAPP_frm.btnCopyClick(Sender: TObject);
var
  BaseDocNo : String;
  CopyDocNo : String;
begin
  inherited;
  Dialog_CopyLocapp_frm := TDialog_CopyLocapp_frm.Create(self);
  try
    BaseDocNo := Dialog_CopyLocapp_frm.openDialog;

    IF BaseDocNo = '' Then Exit;

    CopyDocNo := AutoDocNo(True);

    //------------------------------------------------------------------------------
    // COPYLOCAPP
    //------------------------------------------------------------------------------
    with spCopyLOCAPP do
    begin
      Close;
      Parameters.ParamByName('@BaseDocNo').Value := BaseDocNo;
      Parameters.ParamByName('@CopyDocNO').Value := CopyDocNo;
      Parameters.ParamByName('@UserID'   ).Value := LoginData.sID;
      IF not DMMssql.KISConnect.InTransaction Then
        DMMssql.KISConnect.BeginTrans;
      try
        ExecProc;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+E.Message),'복사오류',MB_OK+MB_ICONERROR);
        end;
      end;
    end;

    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),CopyDocNo);

    //MessageBox(Self.Handle,MSG_SYSTEM_COPY_OK,'복사완료',MB_OK+MB_ICONINFORMATION);
    EditDocument;
  finally
    FreeAndNil(Dialog_CopyLocapp_frm);
  end;
end;

procedure TUI_LOCAPP_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := LOCAPP(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCAPP';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCAPP';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
                 RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_LOCAPP_frm.qryListCHK1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  IF qryListCHK1.AsString = '' Then
    Text := ''
  else
    Text := '▶';
end;

procedure TUI_LOCAPP_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_LOCAPP_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_LOCAPP_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);

end;

procedure TUI_LOCAPP_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then
    exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin
    Case CHK2Value of
      9 :
      begin
        IF Column.FieldName = 'CHK2' Then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;
      6 :
      begin
        IF AnsiMatchText( Column.FieldName , ['CHK2','CHK3'] ) Then
          Canvas.Font.Color := clred
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;

//    if Column.FieldName = 'CHK3' then
//    begin
//      case AnsiIndexText(RightStr(qryList.FieldByName('CHK3').AsString, 1), ['1', '2', '3', '4']) of
//        0:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clWhite;
//          end;
//        1:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clYellow;
//          end;
//        2, 3:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clRed;
//          end;
//      end;
//    end;
//
//    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
//    begin
//      Canvas.Brush.Color := $00CC8A00;
//      Canvas.Font.Color := clWhite;
//    end;
//
//    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clblack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TUI_LOCAPP_frm.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;
  KeyValue := qryListMAINT_NO.AsString;
  qryList.Close;
  qryList.Open;
  qryList.Locate('MAINT_NO',KeyValue,[]);
end;

procedure TUI_LOCAPP_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCAPP_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCAPP_frm := nil;
end;

procedure TUI_LOCAPP_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCAPP_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  LOCAPP_PRINT_frm := TLOCAPP_PRINT_frm.Create(Self);
  try
    LOCAPP_PRINT_frm.PrintDocument(qryList.Fields,True);
  finally
    FreeAndNil( LOCAPP_PRINT_frm );
  end;
end;

procedure TUI_LOCAPP_frm.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  N1.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
  N3.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
  N4.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
end;

procedure TUI_LOCAPP_frm.N8Click(Sender: TObject);
begin
  inherited;
  LOCAPP_PRINT_frm := TLOCAPP_PRINT_frm.Create(Self);
  try
    CASe (Sender as TMenuItem).Tag of
      0: LOCAPP_PRINT_frm.PrintDocument(qryList.Fields,True);
      1: LOCAPP_PRINT_frm.PrintDocument(qryList.Fields);
    end;
  finally
    FreeAndNil( LOCAPP_PRINT_frm );
  end;
end;

//대표공급물픔 HS부호
procedure TUI_LOCAPP_frm.edt_HsDblClick(Sender: TObject);
begin
  inherited;
 Dialog_HsCodeList_frm := TDialog_HsCodeList_frm.Create(Self);
  try
    if edt_Hs.ReadOnly = False then
    begin
      if Dialog_HsCodeList_frm.openDialog = mrOK then
      begin
       //선택된 코드 출력
          (Sender as TsMaskEdit).Text := Dialog_HsCodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
      end;
    end;
  finally

    FreeAndNil(Dialog_HsCodeList_frm);

  end;
end;

procedure TUI_LOCAPP_frm.sBitBtn24Click(Sender: TObject);
begin
  edt_HsDblClick(edt_Hs);
end;

procedure TUI_LOCAPP_frm.sMemo1Change(Sender: TObject);
var
  memorow,memoCol : integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);

  if (Sender as TsMemo).Name = 'sMemo1' then
  begin
    if sMemo1.Text = '' then
      slabel56.Caption := ''
    else
      sLabel56.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end
  else if (Sender as TsMemo).Name = 'sMemo2' then
  begin
    if sMemo2.Text = '' then
      slabel6.Caption := ''
    else
      sLabel6.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end
  else if (Sender as TsMemo).Name = 'sMemo3' then
  begin
    if sMemo3.Text = '' then
      slabel7.Caption := ''
    else
      sLabel7.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end
  else if (Sender as TsMemo).Name = 'sMemo4' then
  begin
    if sMemo4.Text = '' then
      slabel9.Caption := ''
    else
      sLabel9.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end;

end;

procedure TUI_LOCAPP_frm.memoLimit(Sender: TObject; var Key: Char);
var
  limitcount : Integer;
begin
  limitcount := 6;
  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;
end;

procedure TUI_LOCAPP_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

   if (ActiveControl.Name = 'sComboBox1') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 1;
     //edt_ORGN1N.SetFocus;
     Self.KeyPreview := False;
   end
   else
   begin
     Self.KeyPreview := True;
     inherited;
   end;
end;

end.

