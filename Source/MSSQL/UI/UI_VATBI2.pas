unit UI_VATBI2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, UI_VATBI_BP, DB, ADODB, StdCtrls,
  sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, Grids, DBGrids,
  acDBGrid, sLabel, ExtCtrls, sBevel, sMemo, ComCtrls, sPageControl,
  Buttons, sBitBtn, sEdit, Mask, sMaskEdit, sButton, sSpeedButton, sPanel,
  sSplitter, TypeDefine, QuickRpt;

type
  TUI_VATBI2_frm = class(TUI_VATBI_BP_frm)
    sButton4: TsButton;
    sButton2: TsButton;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRE_NO: TStringField;
    qryListSE_NO: TStringField;
    qryListFS_NO: TStringField;
    qryListACE_NO: TStringField;
    qryListRFF_NO: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_SAUP: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_ADDR1: TStringField;
    qryListSE_ADDR2: TStringField;
    qryListSE_ADDR3: TStringField;
    qryListSE_UPTA: TStringField;
    qryListSE_UPTA1: TMemoField;
    qryListSE_ITEM: TStringField;
    qryListSE_ITEM1: TMemoField;
    qryListBY_CODE: TStringField;
    qryListBY_SAUP: TStringField;
    qryListBY_NAME1: TStringField;
    qryListBY_NAME2: TStringField;
    qryListBY_ADDR1: TStringField;
    qryListBY_ADDR2: TStringField;
    qryListBY_ADDR3: TStringField;
    qryListBY_UPTA: TStringField;
    qryListBY_UPTA1: TMemoField;
    qryListBY_ITEM: TStringField;
    qryListBY_ITEM1: TMemoField;
    qryListAG_CODE: TStringField;
    qryListAG_SAUP: TStringField;
    qryListAG_NAME1: TStringField;
    qryListAG_NAME2: TStringField;
    qryListAG_NAME3: TStringField;
    qryListAG_ADDR1: TStringField;
    qryListAG_ADDR2: TStringField;
    qryListAG_ADDR3: TStringField;
    qryListAG_UPTA: TStringField;
    qryListAG_UPTA1: TMemoField;
    qryListAG_ITEM: TStringField;
    qryListAG_ITEM1: TMemoField;
    qryListDRAW_DAT: TStringField;
    qryListDETAILNO: TBCDField;
    qryListSUP_AMT: TBCDField;
    qryListTAX_AMT: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAMT11: TBCDField;
    qryListAMT11C: TStringField;
    qryListAMT12: TBCDField;
    qryListAMT21: TBCDField;
    qryListAMT21C: TStringField;
    qryListAMT22: TBCDField;
    qryListAMT31: TBCDField;
    qryListAMT31C: TStringField;
    qryListAMT32: TBCDField;
    qryListAMT41: TBCDField;
    qryListAMT41C: TStringField;
    qryListAMT42: TBCDField;
    qryListINDICATOR: TStringField;
    qryListTAMT: TBCDField;
    qryListSUPTAMT: TBCDField;
    qryListTAXTAMT: TBCDField;
    qryListUSTAMT: TBCDField;
    qryListUSTAMTC: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListVAT_CODE: TStringField;
    qryListVAT_TYPE: TStringField;
    qryListNEW_INDICATOR: TStringField;
    qryListSE_ADDR4: TStringField;
    qryListSE_ADDR5: TStringField;
    qryListSE_SAUP1: TStringField;
    qryListSE_SAUP2: TStringField;
    qryListSE_SAUP3: TStringField;
    qryListSE_FTX1: TStringField;
    qryListSE_FTX2: TStringField;
    qryListSE_FTX3: TStringField;
    qryListSE_FTX4: TStringField;
    qryListSE_FTX5: TStringField;
    qryListBY_SAUP_CODE: TStringField;
    qryListBY_ADDR4: TStringField;
    qryListBY_ADDR5: TStringField;
    qryListBY_SAUP1: TStringField;
    qryListBY_SAUP2: TStringField;
    qryListBY_SAUP3: TStringField;
    qryListBY_FTX1: TStringField;
    qryListBY_FTX2: TStringField;
    qryListBY_FTX3: TStringField;
    qryListBY_FTX4: TStringField;
    qryListBY_FTX5: TStringField;
    qryListBY_FTX1_1: TStringField;
    qryListBY_FTX2_1: TStringField;
    qryListBY_FTX3_1: TStringField;
    qryListBY_FTX4_1: TStringField;
    qryListBY_FTX5_1: TStringField;
    qryListAG_ADDR4: TStringField;
    qryListAG_ADDR5: TStringField;
    qryListAG_SAUP1: TStringField;
    qryListAG_SAUP2: TStringField;
    qryListAG_SAUP3: TStringField;
    qryListAG_FTX1: TStringField;
    qryListAG_FTX2: TStringField;
    qryListAG_FTX3: TStringField;
    qryListAG_FTX4: TStringField;
    qryListAG_FTX5: TStringField;
    qryListSE_NAME3: TStringField;
    qryListBY_NAME3: TStringField;
    qryListINDICATOR_NAME: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsDE_DATE: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsDE_REM1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsSUPAMT: TBCDField;
    qryGoodsTAXAMT: TBCDField;
    qryGoodsUSAMT: TBCDField;
    qryGoodsUSAMT_G: TStringField;
    qryGoodsSUPSTAMT: TBCDField;
    qryGoodsTAXSTAMT: TBCDField;
    qryGoodsUSSTAMT: TBCDField;
    qryGoodsUSSTAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsRATE: TBCDField;
    QRCompositeReport1: TQRCompositeReport;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    byAddPanel: TsPanel;
    sPanel6: TsPanel;
    edt_BYFTX1_1: TsEdit;
    edt_BYFTX2_1: TsEdit;
    edt_BYFTX3_1: TsEdit;
    edt_BYFTX4_1: TsEdit;
    edt_BYFTX5_1: TsEdit;
    sBitBtn24: TsBitBtn;
    sBitBtn25: TsBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure sBitBtn60Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
    VATBI2_SQL : String;
    procedure ReadDocument; override;
    procedure GoodsReadDocument; override;
    procedure DeleteDocument; override;

    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;

  end;

var
  UI_VATBI2_frm: TUI_VATBI2_frm;

implementation

{$R *.dfm}

uses MessageDefine, DateUtils, Commonlib, StrUtils, MSSQL, AutoNo, Dlg_Customer,
     VarDefine, SQLCreator, KISCalendar, Dlg_ErrorMessage, VATBI2_PRINT;

procedure TUI_VATBI2_frm.FormActivate(Sender: TObject);
begin
  inherited;
  //
end;

procedure TUI_VATBI2_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  UI_VATBI2_frm := nil;
end;

procedure TUI_VATBI2_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

procedure TUI_VATBI2_frm.FormShow(Sender: TObject);
begin
  inherited;
  //
  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  //문서정보
  EnabledControlValue(taxPanel);
  EnabledControlValue(providerPanel);
  EnabledControlValue(buyerPanel);
  //품목내역
  EnabledControlValue(goodsPanel);
  //금맥 및 수탁자
  EnabledControlValue(moneyPanel);
  EnabledControlValue(trusteePanel);



  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');
end;

procedure TUI_VATBI2_frm.FormCreate(Sender: TObject);
begin
  inherited;
  VATBI2_SQL := qryList.SQL.Text;
  ProgramControlType := ctView;
end;

procedure TUI_VATBI2_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;
  //----------------------------------------------------------------------------
  //기본정보
  //----------------------------------------------------------------------------
    //관리번호
    edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
    //신청일자
    mask_DATEE.Text := qryListDATEE.AsString;
    //사용자
    edt_USER_ID.Text := qryListUSER_ID.AsString;
    //문서기능
    edt_msg1.Text := qryListMESSAGE1.AsString;
    //유형
    edt_msg2.Text := qryListMESSAGE2.AsString;
  //----------------------------------------------------------------------------
  //문서정보
  //----------------------------------------------------------------------------
    //세금계산서코드
    edt_VATCODE.Text := qryListVAT_CODE.AsString;
    //세금계산서종류
    edt_VATTYPE.Text := qryListVAT_TYPE.AsString;
    //수정사유코드
    edt_NEWINDICATOR.Text := qryListNEW_INDICATOR.AsString;
    //책번호 - 권
    edt_RENO.Text := qryListRE_NO.AsString;
    //책번호 - 호
    edt_SENO.Text := qryListSE_NO.AsString;
    //일련번호
    edt_FSNO.Text := qryListFS_NO.AsString;
    //참조번호
    edt_ACENO.Text := qryListACE_NO.AsString;
    //세금계산서번호
    edt_RFFNO.Text := qryListRFF_NO.AsString;
    //--------------------------------------------------------------------------
    //공급자
    //--------------------------------------------------------------------------
      //코드
      edt_SECODE.Text := qryListSE_CODE.AsString;
      //사업자번호
      mask_SESAUP.Text := qryListSE_SAUP.AsString;
      //대표자명
      edt_SENAME3.Text := qryListSE_NAME3.AsString;
      //상호
      edt_SENAME1.Text := qryListSE_NAME1.AsString;
      edt_SENAME2.Text := qryListSE_NAME2.AsString;
      //주소
      edt_SEADDR1.Text := qryListSE_ADDR1.AsString;
      edt_SEADDR2.Text := qryListSE_ADDR2.AsString;
      edt_SEADDR3.Text := qryListSE_ADDR3.AsString;
      edt_SEADDR4.Text := qryListSE_ADDR4.AsString;
      edt_SEADDR5.Text := qryListSE_ADDR5.AsString;
      //종사업장번호
      edt_SESAUP1.Text := qryListSE_SAUP1.AsString;
      //담당부서명
      edt_SEFTX1.Text := qryListSE_FTX1.AsString;
      //담당자명
      edt_SEFTX2.Text := qryListSE_FTX2.AsString;
      //전화번호
      edt_SEFTX3.Text := qryListSE_FTX3.AsString;
      //이메일
      edt_SEFTX4.Text := qryListSE_FTX4.AsString;
      //도메인
      edt_SEFTX5.Text := qryListSE_FTX5.AsString;
      //업태
      memo_SEUPTA1.Text := qryListSE_UPTA1.AsString;
      //종목
      memo_SEITEM1.Text := qryListSE_ITEM1.AsString;
    //--------------------------------------------------------------------------
    //공급받는자
    //--------------------------------------------------------------------------
      //코드
      edt_BYCODE.Text := qryListBY_CODE.AsString;
      //사업자등록구분
      edt_BYSAUPCODE.Text := qryListBY_SAUP_CODE.AsString;
      //등록번호
      mask_BYSAUP.Text := qryListBY_SAUP.AsString;
      //대표자명
      edt_BYNAME3.Text := qryListBY_NAME3.AsString;
      //상호
      edt_BYNAME1.Text := qryListBY_NAME1.AsString;
      edt_BYNAME2.Text := qryListBY_NAME2.AsString;
      //주소
      edt_BYADDR1.Text := qryListBY_ADDR1.AsString;
      edt_BYADDR2.Text := qryListBY_ADDR2.AsString;
      edt_BYADDR3.Text := qryListBY_ADDR3.AsString;
      edt_BYADDR4.Text := qryListBY_ADDR4.AsString;
      edt_BYADDR5.Text := qryListBY_ADDR5.AsString;
      //종사업자번호
      edt_BYSAUP1.Text := qryListBY_SAUP1.AsString;
      //담당부서명
      edt_BYFTX1.Text := qryListBY_FTX1.AsString;
      //담당자명
      edt_BYFTX2.Text := qryListBY_FTX2.AsString;
      //전화번호
      edt_BYFTX3.Text := qryListBY_FTX3.AsString;
      //이메일
      edt_BYFTX4.Text := qryListBY_FTX4.AsString;
      edt_BYFTX5.Text := qryListBY_FTX5.AsString;
      //담당부서명(2)
      edt_BYFTX1_1.Text := qryListBY_FTX1_1.AsString;
      //담당자명(2)
      edt_BYFTX2_1.Text := qryListBY_FTX2_1.AsString;
      //전화번호(2)
      edt_BYFTX3_1.Text := qryListBY_FTX3_1.AsString;
      //이메일(2)
      edt_BYFTX4_1.Text := qryListBY_FTX4_1.AsString;
      edt_BYFTX5_1.Text := qryListBY_FTX5_1.AsString;
      //업태
      memo_BYUPTA1.Text := qryListBY_UPTA1.AsString;
      //종목
      memo_BYITEM1.Text := qryListBY_ITEM1.AsString;
  //----------------------------------------------------------------------------
  //금액 및 수탁자
  //----------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //금액
    //--------------------------------------------------------------------------
      //작성일자
      mask_DRAWDAT.Text := qryListDRAW_DAT.AsString;
      //공급가액
      curr_SUPAMT.Value := qryListSUP_AMT.AsCurrency;
      //세액
      curr_TAXAMT.Value := qryListTAX_AMT.AsCurrency;
      //공란수
      curr_DETAILNO.Value := qryListDETAILNO.AsCurrency;
      //비고
      memo_REMARK1.Text := qryListREMARK1.AsString;
      //현금외화
      curr_AMT11.Value := qryListAMT11.AsCurrency;
      //현금외화 (통화)
      edt_AMT11C.Text := qryListAMT11C.AsString;
      //현금
      curr_AMT12.Value := qryListAMT12.AsCurrency;
      //수표외화
      curr_AMT21.Value := qryListAMT21.AsCurrency;
      //수표외화 (통화)
      edt_AMT21C.Text := qryListAMT21C.AsString;
      //수표
      curr_AMT22.Value := qryListAMT22.AsCurrency;
      //어음외화
      curr_AMT31.Value := qryListAMT31.AsCurrency;
      //어음외화 (통화)
      edt_AMT31C.Text := qryListAMT31C.AsString;
      //어음
      curr_AMT32.Value := qryListAMT32.AsCurrency;
      //외상미수금외화
      curr_AMT41.Value := qryListAMT41.AsCurrency;
      //외상미수금 외화 (통화)
      edt_AMT41C.Text := qryListAMT41C.AsString;
      //외상미수금
      curr_AMT42.Value := qryListAMT42.AsCurrency;
      //영수/청구
      edt_INDICATOR.Text := qryListINDICATOR.AsString;
      //영수 / 청구 name
      edt_INDICATORNAME.Text := qryListINDICATOR_NAME.AsString;
      //총금액
      curr_TAMT.Value := qryListTAMT.AsCurrency;
      //총공급가액
      curr_SUPTAMT.Value := qryListSUP_AMT.AsCurrency;
      //총외화공금가액단위
      edt_USTAMTC.Text := qryListUSTAMTC.AsString;
      //총외화공급가액
      curr_USTAMT.Value := qryListUSTAMT.AsCurrency;
      //총수량단위
      edt_TQTYC.Text := qryListTQTYC.AsString;
      //총수량
      curr_TQTY.Value := qryListTQTY.AsCurrency;
      //총세액
      curr_TAXTAMT.Value := qryListTAXTAMT.AsCurrency;
    //--------------------------------------------------------------------------
    //수탁자
    //--------------------------------------------------------------------------
      //수탁자
      edt_AGCODE.Text := qryListAG_CODE.AsString;
      //사업자등록번호
      mask_AGSAUP.Text := qryListAG_SAUP.AsString;
      //대표자명
      edt_AGNAME3.Text := qryListAG_NAME3.AsString;
      //상호
      edt_AGNAME1.Text := qryListAG_NAME1.AsString;
      edt_AGNAME2.Text := qryListAG_NAME2.AsString;
      //주소
      edt_AGADDR1.Text := qryListAG_ADDR1.AsString;
      edt_AGADDR2.Text := qryListAG_ADDR2.AsString;
      edt_AGADDR3.Text := qryListAG_ADDR3.AsString;
      edt_AGADDR4.Text := qryListAG_ADDR4.AsString;
      edt_AGADDR5.Text := qryListAG_ADDR5.AsString;
      //종사업장번호
      edt_AGSAUP1.Text := qryListAG_SAUP1.AsString;
      //담당부서명
      edt_AGFTX1.Text := qryListAG_FTX1.AsString;
      //담당자명
      edt_AGFTX2.Text := qryListAG_FTX2.AsString;
      //전화번호
      edt_AGFTX3.Text := qryListAG_FTX3.AsString;
      //이메일
      edt_AGFTX4.Text := qryListAG_FTX4.AsString;
      edt_AGFTX5.Text := qryListAG_FTX5.AsString;
      //업태
      memo_AGUPTA1.Text := qryListAG_UPTA1.AsString;
      //종목
      memo_AGITEM1.Text := qryListAG_ITEM1.AsString;
end;

procedure TUI_VATBI2_frm.GoodsReadDocument;
begin
  inherited;
//------------------------------------------------------------------------------
//품명 / 규격 / REMARK
//------------------------------------------------------------------------------
  //품명코드
  edt_NAMECOD_D.Text := qryGoodsNAME_COD.AsString;
  //품명
  memo_NAME1_D.Text := qryGoodsNAME1.AsString;
  //규격
  memo_SIZE1_D.Text := qryGoodsSIZE1.AsString;
  //REMARK1
  memo_DEREM1_D.Text := qryGoodsDE_REM1.AsString;
  //공급일자
  mask_DEDATE_D.Text := qryGoodsDE_DATE.AsString;
  //환율
  curr_RATE_D.Value := qryGoodsRATE.AsCurrency;
//------------------------------------------------------------------------------
//수량 / 단가 / 금액
//-----------------------------------------------------------------------------
  //수량단위
  edt_QTY_G_D.Text := qryGoodsQTY_G.AsString;
  //수량
  curr_QTY_D.Value := qryGoodsQTY.AsCurrency;
  //수량소계단위
  edt_STQTY_G_D.Text := qryGoodsSTQTY_G.AsString;
  //수량소계
  curr_STQTY_D.Value := qryGoodsSTQTY.AsCurrency;
  //단가
  curr_PRICE_D.Value := qryGoodsPRICE.AsCurrency;
  //단가기준수량단위
  edt_QTYG_G_D.Text := qryGoodsQTYG_G.AsString;
  //단가기준수량
  curr_QTYG_D.Value := qryGoodsQTYG.AsCurrency;
  //원화공급가액
  curr_SUPAMT_D.Value := qryGoodsSUPAMT.AsCurrency;
  //원화공급가액소계
  curr_SUPSTAMT_D.Value := qryGoodsSUPSTAMT.AsCurrency;
  //세액
  curr_TAXAMT_D.Value := qryGoodsTAXAMT.AsCurrency;
  //세액소계
  curr_TAXSTAMT_D.Value := qryGoodsTAXSTAMT.AsCurrency;
  //외화공급가액단위
  edt_USAMT_G_D.Text := qryGoodsUSAMT_G.AsString;
  //외화공급가액
  curr_USAMT_D.Value := qryGoodsUSAMT.AsCurrency;
  //외화공급가액 소계단위
  edt_USSTAMT_G_D.Text := qryGoodsUSSTAMT_G.AsString;
  //외화공급가액 소계
  curr_USSTAMT_D.Value := qryGoodsUSSTAMT.AsCurrency;
end;

procedure TUI_VATBI2_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := VATBI2_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE BY_NAME1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ASC ');
    end;

    Open;
  end;
end;

function TUI_VATBI2_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
 Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := VATBI2_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_VATBI2_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
                                   
  with qryGoods do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
    Open;
  end;

  GoodsReadDocument;
  
end;

procedure TUI_VATBI2_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryGoods.State = dsBrowse then GoodsReadDocument;
end;

procedure TUI_VATBI2_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;
  
end;

procedure TUI_VATBI2_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if qryList.RecordCount = 0 then ReadDocument;
end;

procedure TUI_VATBI2_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM VATBI2_H WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM VATBI2_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;


        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;

end;

procedure TUI_VATBI2_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_VATBI2_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_VATBI2_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_VATBI2_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  VATBI2_PRINT_frm := TVATBI2_PRINT_frm.Create(Self);
  try
    VATBI2_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    VATBI2_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
       0 :
       begin
          VATBI2_PRINT_frm.PrinterSetup;
          //------------------------------------------------------------------------------
          // 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
          //------------------------------------------------------------------------------
          if VATBI2_PRINT_frm.Tag = 0 then VATBI2_PRINT_frm.Print;
       end;
       1 : VATBI2_PRINT_frm.Preview;
    end;
  finally
    FreeAndNil(VATBI2_PRINT_frm);
  end;
end;

end.
