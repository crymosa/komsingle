unit UI_DOANTC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Grids, DBGrids, acDBGrid,
  ExtCtrls, sSplitter, ComCtrls, sPageControl, Buttons, sBitBtn, sEdit,
  Mask, sMaskEdit, sButton, sSpeedButton, sPanel, sSkinProvider,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sMemo, sBevel, DB, ADODB, TypeDefine,
  QuickRpt, sLabel;

type
  TUI_DOANTC_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sButton4: TsButton;
    sButton2: TsButton;
    sPanel4: TsPanel;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    edt_MAINT_NO: TsEdit;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    page1_RIGHT: TsPanel;
    sPanel2: TsPanel;
    sTabSheet5: TsTabSheet;
    sDBGrid2: TsDBGrid;
    sPanel21: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    sBitBtn23: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn28: TsBitBtn;
    sPanel23: TsPanel;
    sPanel56: TsPanel;
    sDBGrid8: TsDBGrid;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    sPanel3: TsPanel;
    sPanel6: TsPanel;
    edt_BANKCODE: TsEdit;
    mask_RESDATE: TsMaskEdit;
    edt_BANK1: TsEdit;
    edt_BANK2: TsEdit;
    mask_SETDATE: TsMaskEdit;
    sPanel7: TsPanel;
    sPanel5: TsPanel;
    sBevel1: TsBevel;
    sPanel9: TsPanel;
    edt_LCG: TsEdit;
    edt_LCNO: TsEdit;
    edt_AMTC: TsEdit;
    curr_AMT: TsCurrencyEdit;
    edt_BLG: TsEdit;
    edt_BLNO: TsEdit;
    edt_CHRGC: TsEdit;
    curr_CHRG: TsCurrencyEdit;
    sPanel8: TsPanel;
    sSpeedButton1: TsSpeedButton;
    sPanel11: TsPanel;
    edt_APPCODE: TsEdit;
    edt_APPNAME1: TsEdit;
    edt_APPNAME2: TsEdit;
    edt_APPNAME3: TsEdit;
    edt_BKNAME1: TsEdit;
    edt_BKNAME2: TsEdit;
    edt_BKNAME3: TsEdit;
    sPanel10: TsPanel;
    sPanel12: TsPanel;
    memo_REMARK1: TsMemo;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBANK_CODE: TStringField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListAMT: TBCDField;
    qryListAMTC: TStringField;
    qryListCHRG: TBCDField;
    qryListCHRGC: TStringField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListREMARK1: TMemoField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    sSplitter1: TsSplitter;
    QRCompositeReport1: TQRCompositeReport;
    sSplitter2: TsSplitter;
    sSplitter4: TsSplitter;
    sSplitter3: TsSplitter;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sBitBtn60Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure sBitBtn28Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    DOANTC_SQL : String;
    procedure ReadDocument;
    procedure DeleteDocument;

    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;
  end;

var
  UI_DOANTC_frm: TUI_DOANTC_frm;

implementation

{$R *.dfm}
uses MessageDefine, DateUtils, Commonlib, StrUtils, AutoNo, Dlg_Customer,
     VarDefine, SQLCreator, KISCalendar, Dlg_ErrorMessage, MSSQL, DOANTC_PRINT;

procedure TUI_DOANTC_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_DOANTC_frm := nil;
end;

procedure TUI_DOANTC_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOANTC_frm.FormShow(Sender: TObject);
begin
  inherited;
  Page_control.ActivePageIndex := 0;

  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel3);
  EnabledControlValue(sPanel5);
  EnabledControlValue(sPanel7);
  EnabledControlValue(sPanel10);
  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');
end;

procedure TUI_DOANTC_frm.Page_controlChange(Sender: TObject);
begin
  inherited;
  if Page_control.ActivePageIndex = 1 then
  begin
    //관리번호가있는 패널  숨김
    //sPanel4.Visible := not (Page_control.ActivePageIndex = 1);
    //DBGrid8이 위치해 있는 패널 숨김
    sPanel56.Visible := not (Page_control.ActivePageIndex = 1);
  end
  else
  begin
    //관리번호가있는 패널
    //sPanel4.Visible := True;
    //DBGrid8이 위치해 있는 패널
    sPanel56.Visible := True;
  end;
end;

procedure TUI_DOANTC_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_DOANTC_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;
  //----------------------------------------------------------------------------
  //기본정보
  //----------------------------------------------------------------------------
    //관리번호
    edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
    //신청일자
    mask_DATEE.Text := qryListDATEE.AsString;
    //사용자
    edt_USER_ID.Text := qryListUSER_ID.AsString;
    //문서기능
    edt_msg1.Text := qryListMESSAGE1.AsString;
    //유형
    edt_msg2.Text := qryListMESSAGE2.AsString;
  //----------------------------------------------------------------------------
  //통지은행
  //----------------------------------------------------------------------------
    //통지은행
    edt_BANKCODE.Text := qryListBANK_CODE.AsString;
    edt_BANK1.Text := qryListBANK1.AsString;
    edt_BANK2.Text := qryListBANK2.AsString;
    //통지일자
    mask_RESDATE.Text := qryListRES_DATE.AsString;
    //최종결제일
    mask_SETDATE.Text := qryListSET_DATE.AsString;
  //----------------------------------------------------------------------------
  //신용장,선하증권
  //----------------------------------------------------------------------------
    //신용장 구분 및 번호
    edt_LCG.Text := qryListLC_G.AsString;
    edt_LCNO.Text := qryListLC_NO.AsString;
    //선하증건 구분 및 번호
    edt_BLG.Text := qryListBL_G.AsString;
    edt_BLNO.Text := qryListBL_NO.AsString;
    //어음금액
    edt_AMTC.Text := qryListAMTC.AsString;
    curr_AMT.Value := qryListAMT.AsCurrency;
    //기타수수료
    edt_CHRGC.Text := qryListCHRGC.AsString;
    curr_CHRG.Value := qryListCHRG.AsCurrency;
  //----------------------------------------------------------------------------
  //수신업체
  //---------------------------------------------------------------------------
    edt_APPCODE.Text := qryListAPP_CODE.AsString;
    edt_APPNAME1.Text := qryListAPP_NAME1.AsString;
    edt_APPNAME2.Text := qryListAPP_NAME2.AsString;
    edt_APPNAME3.Text := qryListAPP_NAME3.AsString;
  //----------------------------------------------------------------------------
  //발신기관
  //---------------------------------------------------------------------------
    edt_BKNAME1.Text := qryListBK_NAME1.AsString;
    edt_BKNAME2.Text := qryListBK_NAME2.AsString;
    edt_BKNAME3.Text := qryListBK_NAME3.AsString;
  //---------------------------------------------------------------------------
  //기타조건
  //-------------------------------------------------------------------------
    memo_REMARK1.Lines.Text := qryListREMARK1.AsString;

end;

procedure TUI_DOANTC_frm.Readlist(OrderSyntax: string);
begin
if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 1 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := DOANTC_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE LC_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ASC ');
    end;

    Open;
  end;
end;

function TUI_DOANTC_frm.Readlist(FromDate, ToDate,KeyValue: String): Boolean;
begin
 Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := DOANTC_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_DOANTC_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

procedure TUI_DOANTC_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;
end;

procedure TUI_DOANTC_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if qryList.RecordCount = 0 then ReadDocument;
end;

procedure TUI_DOANTC_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM DOANTC WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end
      else
      begin
        if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;
end;

procedure TUI_DOANTC_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel3);
    ClearControlValue(sPanel5);
    ClearControlValue(sPanel7);
    ClearControlValue(sPanel10);
  end;

end;

procedure TUI_DOANTC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_DOANTC_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_DOANTC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  DOANTC_SQL := qryList.SQL.Text;
  ProgramControlType := ctView;
end;

procedure TUI_DOANTC_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
POS : TPoint;
begin
  inherited;

//if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_DOANTC_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TUI_DOANTC_frm.sBitBtn28Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate2);
end;

procedure TUI_DOANTC_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  DOANTC_PRINT_frm := TDOANTC_PRINT_frm.Create(Self);
  try
    DOANTC_PRINT_frm.MaintNo := edt_MAINT_NO.Text;

    DOANTC_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        DOANTC_PRINT_frm.PrinterSetup;
        if DOANTC_PRINT_frm.Tag = 0 then DOANTC_PRINT_frm.Print;
      end;
      1 : DOANTC_PRINT_frm.Preview;
    end;

  finally
    FreeAndNil(DOANTC_PRINT_frm);
  end;

end;

procedure TUI_DOANTC_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
//
end;

end.
