inherited UI_APPRMI_NEW_frm: TUI_APPRMI_NEW_frm
  Left = 552
  Top = 332
  BorderWidth = 4
  Caption = '[APPRMI]'#49688#51077#49569#44552#49888#52397#49436
  ClientHeight = 619
  ClientWidth = 1115
  Constraints.MaxHeight = 0
  Constraints.MaxWidth = 0
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1115
    Height = 72
    Align = alTop
    DoubleBuffered = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object sImage2: TsImage
      Left = 11
      Top = 4
      Width = 18
      Height = 32
      Cursor = crHandPoint
      ParentShowHint = False
      Picture.Data = {07544269746D617000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      ImageIndex = 22
      Images = DMICON.System18
      Reflected = True
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 767
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton5: TsSpeedButton
      Left = 489
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 16
      Top = 13
      Width = 125
      Height = 23
      Caption = #49688#51077#49569#44552' '#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 44
      Top = 37
      Width = 73
      Height = 21
      Caption = '(APPRMI)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 415
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton7: TsSpeedButton
      Left = 693
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 208
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sSpeedButton1: TsSpeedButton
      Left = 906
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sButton13: TsButton
      Left = 140
      Top = 38
      Width = 65
      Height = 31
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = 'ERP'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      TabStop = False
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
    object btnExit: TsButton
      Left = 1038
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 220
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 25
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 285
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Tag = 2
      Left = 350
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 702
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 498
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 11
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 563
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 0
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Tag = 2
      Left = 628
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 776
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #51456#48708
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 841
      Top = 3
      Width = 64
      Height = 35
      Cursor = crHandPoint
      Caption = #51204#49569
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 424
      Top = 2
      Width = 64
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 27
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 216
      Top = 41
      Width = 889
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      DoubleBuffered = False
      TabOrder = 12
      object edtMAINT_NO: TsEdit
        Left = 72
        Top = 3
        Width = 178
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 312
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        ReadOnly = True
        TabOrder = 1
        Color = clBtnFace
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        SkinData.CustomColor = True
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object edt_userno: TsEdit
        Left = 440
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
      object com_func: TsComboBox
        Left = 653
        Top = 4
        Width = 92
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 1
        TabOrder = 3
        TabStop = False
        Text = '9: Original'
        Items.Strings = (
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 801
        Top = 4
        Width = 88
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 4
        TabStop = False
        Text = 'AB: Message acknowledgement'
        Items.Strings = (
          'AB: Message acknowledgement')
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 341
    Height = 547
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    DoubleBuffered = False
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 489
      TabStop = False
      Align = alClient
      Ctl3D = False
      DataSource = dsAPPRMI_H
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      SkinData.CustomColor = True
      DefaultRowHeight = 19
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 148
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #51652#54665
          Width = 43
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'REG_DT'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197' '#51068#51088
          Width = 125
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      DoubleBuffered = False
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 245
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 64
        Top = 28
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 64
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        TabOrder = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;1'
        Text = '    -  -  '
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 157
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;1'
        Text = '    -  -  '
      end
      object sBitBtn1: TsBitBtn
        Left = 262
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 1
      Top = 57
      Width = 339
      Height = 489
      Align = alClient
      DoubleBuffered = False
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 88
        Top = 215
        Width = 163
        Height = 21
        Caption = '('#50808#54868'/'#49688#51077')'#49569#44552#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 107
        Top = 239
        Width = 124
        Height = 15
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 341
    Top = 72
    Width = 774
    Height = 547
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 26
    TabOrder = 2
    TabPadding = 10
    object sTabSheet1: TsTabSheet
      Caption = #49888#52397#49436
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 511
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sLabel12: TsLabel
          Left = 474
          Top = 88
          Width = 211
          Height = 15
          Caption = #50808#44397#54872#49688#49688#47308#45225#48512'(2AJ)'#49440#53469#49884' '#52628#44032#51077#47141
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          UseSkinColor = False
        end
        object sImage3: TsImage
          Left = 13
          Top = 113
          Width = 16
          Height = 16
          Cursor = crHelp
          Hint = 
            '('#50808#54868')'#51648#44553#44552#50529' '#51077#47141' : '#51204#52404' '#44552#50529' '#51473' '#50808#54868#44228#51340' '#44208#51228' '#44552#50529' '#51077#47141'<br>'#13#10'<font color=red><b>'#51452') '#50808 +
            #54868#44228#51340#50752' '#50896#54868#44228#51340#50640#49436' '#48516#54624' '#52636#44552#54616#50668' '#49569#44552#54616#45716' '#44221#50864' '#51077#47141' '#54596#49688'</b></font><br>'#13#10'- '#50808#54868#44228#51340#50640#49436#47564' '#52636#44552#54616#50668 +
            ' '#49569#44552#54616#45716' '#44221#50864' '#51077#47141' '#48520#54596#50836'<br>'#13#10'- '#50896#54868#44228#51340#50640#49436#47564' '#52636#44552#54616#50668' '#49569#44552#54616#45716' '#44221#50864' '#51077#47141' '#48520#54596#50836#13
          AutoSize = True
          ParentShowHint = False
          Picture.Data = {07544269746D617000000000}
          ShowHint = True
          Transparent = True
          ImageIndex = 9
          Images = DMICON.System16
          SkinData.SkinSection = 'CHECKBOX'
          UseFullSize = True
        end
        object msk_APP_DATE: TsMaskEdit
          Left = 123
          Top = 14
          Width = 79
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 0
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object msk_TRN_HOPE_DT: TsMaskEdit
          Left = 123
          Top = 38
          Width = 79
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 1
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #51060#52404#55148#47581#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object msk_IMPT_SCHEDULE_DT: TsMaskEdit
          Left = 277
          Top = 14
          Width = 79
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 2
          Color = clWhite
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#50696#51221#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object edt_ETC_NO: TsEdit
          Left = 123
          Top = 62
          Width = 233
          Height = 23
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
        end
        object edt_TRD_CD: TsEdit
          Tag = 100
          Left = 474
          Top = 14
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 4
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44144#47000#54805#53468#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object sEdit3: TsEdit
          Left = 508
          Top = 14
          Width = 200
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
        end
        object edt_IMPT_REMIT_CD: TsEdit
          Tag = 101
          Left = 474
          Top = 38
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 6
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#49569#44552#49888#52397#50857#46020
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object sEdit5: TsEdit
          Left = 508
          Top = 38
          Width = 200
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
        end
        object edt_IMPT_TRAN_FEE_CD: TsEdit
          Tag = 102
          Left = 474
          Top = 62
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          MaxLength = 3
          TabOrder = 8
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#49688#47308#50976#54805
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object sEdit6: TsEdit
          Tag = -1
          Left = 508
          Top = 62
          Width = 200
          Height = 23
          TabStop = False
          Color = clBtnFace
          Enabled = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
        end
        object edt_REMIT_DESC1: TsEdit
          Left = 123
          Top = 170
          Width = 585
          Height = 23
          Color = clWhite
          MaxLength = 70
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49569#44552#45236#50669
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_REMIT_DESC2: TsEdit
          Left = 123
          Top = 192
          Width = 585
          Height = 23
          MaxLength = 70
          TabOrder = 11
        end
        object edt_REMIT_DESC3: TsEdit
          Left = 123
          Top = 214
          Width = 585
          Height = 23
          MaxLength = 70
          TabOrder = 12
        end
        object edt_REMIT_DESC4: TsEdit
          Left = 123
          Top = 236
          Width = 585
          Height = 23
          MaxLength = 70
          TabOrder = 13
        end
        object edt_REMIT_CD: TsEdit
          Tag = 103
          Left = 123
          Top = 146
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 2
          TabOrder = 14
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49569#44552#48169#48277
        end
        object sEdit9: TsEdit
          Left = 157
          Top = 146
          Width = 200
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
        end
        object edt_ADDED_FEE_TAR: TsEdit
          Tag = 104
          Left = 123
          Top = 267
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 2
          TabOrder = 16
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #48512#44032#49688#49688#47308#48512#45812#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object sEdit11: TsEdit
          Left = 157
          Top = 267
          Width = 201
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
        end
        object edt_TOTAL_AMT_UNIT: TsEdit
          Tag = 98
          Left = 123
          Top = 86
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 18
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52509' '#49569#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object curr_TOTAL_AMT: TsCurrencyEdit
          Left = 157
          Top = 86
          Width = 199
          Height = 23
          DoubleBuffered = False
          TabOrder = 19
          Color = 12582911
          SkinData.CustomColor = True
          DecimalPlaces = 3
          DisplayFormat = '#,0.00#'
        end
        object edt_FORE_AMT_UNIT: TsEdit
          Tag = 98
          Left = 123
          Top = 110
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 20
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #50808#54868#44228#51340' '#49569#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object curr_FORE_AMT: TsCurrencyEdit
          Left = 157
          Top = 110
          Width = 199
          Height = 23
          DoubleBuffered = False
          TabOrder = 21
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 3
          DisplayFormat = '#,0.00#'
        end
        object Edt_ApplicationName1: TsEdit
          Left = 176
          Top = 298
          Width = 182
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 22
          SkinData.CustomColor = True
        end
        object Edt_ApplicationSAUPNO: TsEdit
          Left = 123
          Top = 322
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationCode: TsEdit
          Tag = 101
          Left = 123
          Top = 298
          Width = 52
          Height = 23
          Color = 12775866
          TabOrder = 24
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49569#44552#51032#47280#51064
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAddr1: TsEdit
          Left = 123
          Top = 346
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 25
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAddr2: TsEdit
          Left = 123
          Top = 370
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 26
        end
        object edtBEN_CODE: TsEdit
          Tag = 102
          Left = 473
          Top = 298
          Width = 52
          Height = 23
          Color = 12775866
          TabOrder = 27
          OnDblClick = edtBEN_CODEDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51061#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtBEN_NAME1: TsEdit
          Left = 526
          Top = 298
          Width = 182
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 28
          SkinData.CustomColor = True
        end
        object edtBEN_NAME2: TsEdit
          Left = 473
          Top = 322
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 29
        end
        object edtBEN_NAME3: TsEdit
          Left = 473
          Top = 346
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 30
        end
        object edtBEN_STR1: TsEdit
          Left = 473
          Top = 370
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 31
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtBEN_STR2: TsEdit
          Left = 473
          Top = 394
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 32
        end
        object edtBEN_TELE: TsEdit
          Left = 473
          Top = 442
          Width = 136
          Height = 23
          MaxLength = 35
          TabOrder = 33
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#54868#48264#54840
        end
        object edtBEN_STR3: TsEdit
          Left = 473
          Top = 418
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 34
        end
        object edtBEN_NATION: TsEdit
          Tag = 105
          Left = 673
          Top = 442
          Width = 35
          Height = 23
          Color = 12775866
          MaxLength = 2
          TabOrder = 35
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44397#44032#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edt_ONLY_PWD: TsEdit
          Left = 475
          Top = 267
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          PasswordChar = '*'
          TabOrder = 36
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51648#44553#51648#49884' '#48708#48128#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationElectronicSign: TsEdit
          Left = 123
          Top = 466
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edt_AUTH_NM1: TsEdit
          Left = 123
          Top = 418
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 38
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edt_AUTH_NM2: TsEdit
          Left = 123
          Top = 442
          Width = 235
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 39
          SkinData.CustomColor = True
          BoundLabel.Caption = #45824#54364#51088
        end
        object edtOY_TEL: TsEdit
          Left = 123
          Top = 394
          Width = 235
          Height = 23
          MaxLength = 35
          TabOrder = 40
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#54868#48264#54840
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #51008#54665#51221#48372
      object sPanel27: TsPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 511
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sImage4: TsImage
          Left = 26
          Top = 9
          Width = 16
          Height = 16
          Cursor = crHelp
          Hint = 
            #51008#54665#51221#48372' '#51077#47141'<br>'#13#10'<font color=blue><b>'#50808#54868' '#50896#44552#44228#51340' '#52636#44552#49884'</b></font><br>'#13#10'<b>' +
            '['#54596#49688'] </b>'#49569#44552#51032#47280#51064#51008#54665'('#50808#54868#50896#44552#44228#51340')<br>'#13#10'<b>['#54596#49688'] </b>'#49688#51061#51088#51008#54665'<br>'#13#10'<b>['#49440#53469'] </b' +
            '>'#51473#44036#44221#50976#51008#54665'<br>'#13#10'<font color=blue><b>'#50896#54868' '#50896#44552#44228#51340' '#52636#44552#49884'</b></font><br>'#13#10'<b>' +
            '['#54596#49688'] </b>'#49569#44552#51032#47280#51064#51008#54665'('#50896#54868#50896#44552#44228#51340')<br>'#13#10'<b>['#54596#49688'] </b>'#49688#51061#51088#51008#54665'<br>'#13#10'<b>['#49440#53469'] </b' +
            '>'#51473#44036#44221#50976#51008#54665'<br>'#13#10'<font color=blue><b>'#50808#54868'/'#50896#54868' '#50896#44552#44228#51340' '#52636#44552#49884'</b></font><br>'#13#10 +
            '<b>['#54596#49688'] </b>'#49569#44552#51032#47280#51064#51008#54665'('#50808#54868#50896#44552#44228#51340')<br>'#13#10'<b>['#54596#49688'] </b>'#49569#44552#51032#47280#51064#51008#54665'('#50808#54868#50896#44552#44228#51340')<br>' +
            #13#10'<b>['#54596#49688'] </b>'#49688#51061#51088#51008#54665'<br>'#13#10'<b>['#49440#53469'] </b>'#51473#44036#44221#50976#51008#54665'<br>'
          AutoSize = True
          ParentShowHint = False
          Picture.Data = {07544269746D617000000000}
          ShowHint = True
          Transparent = True
          ImageIndex = 9
          Images = DMICON.System16
          SkinData.SkinSection = 'CHECKBOX'
          UseFullSize = True
        end
        object edtOD_BANK: TsEdit
          Tag = 101
          Left = 87
          Top = 58
          Width = 129
          Height = 23
          Color = 12775866
          MaxLength = 11
          TabOrder = 0
          OnDblClick = edtOD_BANKDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edtOD_BANK1: TsEdit
          Left = 87
          Top = 82
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 1
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
        end
        object edtOD_BANK2: TsEdit
          Left = 87
          Top = 106
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 2
        end
        object edtOD_NAME1: TsEdit
          Left = 87
          Top = 130
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = '('#48512')'#51216#47749
        end
        object edtOD_NAME2: TsEdit
          Left = 87
          Top = 154
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 4
        end
        object edtOD_Nation: TsEdit
          Tag = 106
          Left = 336
          Top = 58
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 2
          TabOrder = 5
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44397#44032#53076#46300
        end
        object edtOD_CURR: TsEdit
          Tag = 98
          Left = 336
          Top = 178
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 6
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#53685#54868
        end
        object edtOD_ACCNT2: TsEdit
          Left = 87
          Top = 178
          Width = 186
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51452
        end
        object edtOD_ACCNT1: TsEdit
          Left = 87
          Top = 202
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#48264#54840
        end
        object sPanel15: TsPanel
          Left = 121
          Top = 34
          Width = 248
          Height = 23
          SkinData.SkinSection = 'SELECTION'
          Caption = #50808#54868#50896#44552#44228#51340
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
        end
        object edtEC_BANK: TsEdit
          Tag = 104
          Left = 463
          Top = 58
          Width = 129
          Height = 23
          Color = 12775866
          MaxLength = 11
          TabOrder = 10
          OnDblClick = edtOD_BANKDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edtEC_BANK1: TsEdit
          Left = 463
          Top = 82
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
        end
        object edtEC_BANK2: TsEdit
          Left = 463
          Top = 106
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 12
        end
        object edtEC_NAME1: TsEdit
          Left = 463
          Top = 130
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 13
          BoundLabel.Active = True
          BoundLabel.Caption = '('#48512')'#51216#47749
        end
        object edtEC_NAME2: TsEdit
          Left = 463
          Top = 154
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 14
        end
        object edtEC_Nation: TsEdit
          Tag = 108
          Left = 712
          Top = 58
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 2
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44397#44032#53076#46300
        end
        object edtEC_ACCNT2: TsEdit
          Left = 463
          Top = 178
          Width = 186
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 16
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51452
        end
        object edtEC_ACCNT1: TsEdit
          Left = 463
          Top = 202
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#48264#54840
        end
        object sPanel14: TsPanel
          Left = 403
          Top = 34
          Width = 93
          Height = 23
          SkinData.SkinSection = 'SELECTION'
          Caption = '['#49569#44552#51032#47280#51008#54665']'
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object sPanel16: TsPanel
          Left = 497
          Top = 34
          Width = 248
          Height = 23
          SkinData.SkinSection = 'SELECTION'
          Caption = #50896#54868#50896#44552#44228#51340
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 19
        end
        object sPanel17: TsPanel
          Left = 658
          Top = 178
          Width = 87
          Height = 23
          Caption = #44228#51340#53685#54868' KRW'
          DoubleBuffered = False
          TabOrder = 20
        end
        object sPanel13: TsPanel
          Left = 27
          Top = 34
          Width = 93
          Height = 23
          SkinData.SkinSection = 'SELECTION'
          Caption = '['#49569#44552#51032#47280#51008#54665']'
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 21
        end
        object edtI1_BANK: TsEdit
          Tag = 103
          Left = 463
          Top = 250
          Width = 129
          Height = 23
          Color = 12775866
          MaxLength = 11
          TabOrder = 22
          OnDblClick = edtOD_BANKDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51473#44036#44221#50976#51008#54665
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edtI1_BANK1: TsEdit
          Left = 463
          Top = 274
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
        end
        object edtI1_BANK2: TsEdit
          Left = 463
          Top = 298
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 24
        end
        object edtI1_NAME1: TsEdit
          Left = 463
          Top = 322
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 25
          BoundLabel.Active = True
          BoundLabel.Caption = '('#48512')'#51216#47749
        end
        object edtI1_NAME2: TsEdit
          Left = 463
          Top = 346
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 26
        end
        object edtI1_Nation: TsEdit
          Tag = 111
          Left = 712
          Top = 250
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 2
          TabOrder = 27
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44397#44032#53076#46300
        end
        object edtI1_CURR: TsEdit
          Tag = 98
          Left = 712
          Top = 370
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 28
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#53685#54868
        end
        object edtI1_ACCNT2: TsEdit
          Left = 463
          Top = 370
          Width = 186
          Height = 23
          MaxLength = 35
          TabOrder = 29
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51452
        end
        object edtI1_ACCNT1: TsEdit
          Left = 463
          Top = 394
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 30
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#48264#54840
        end
        object edtBF_BANK: TsEdit
          Tag = 102
          Left = 87
          Top = 250
          Width = 129
          Height = 23
          Color = 12775866
          MaxLength = 11
          TabOrder = 31
          OnDblClick = edtBF_BANKDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51061#51088' '#51008#54665
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtBF_BANK1: TsEdit
          Left = 87
          Top = 274
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 32
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
        end
        object edtBF_BANK2: TsEdit
          Left = 87
          Top = 298
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 33
        end
        object edtBF_NAME1: TsEdit
          Left = 87
          Top = 322
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 34
          BoundLabel.Active = True
          BoundLabel.Caption = '('#48512')'#51216#47749
        end
        object edtBF_NAME2: TsEdit
          Left = 87
          Top = 346
          Width = 282
          Height = 23
          MaxLength = 35
          TabOrder = 35
        end
        object edtBF_Nation: TsEdit
          Tag = 109
          Left = 336
          Top = 250
          Width = 33
          Height = 23
          Color = 12775866
          MaxLength = 2
          TabOrder = 36
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44397#44032#53076#46300
        end
        object edtBF_CURR: TsEdit
          Tag = 98
          Left = 336
          Top = 370
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#53685#54868
        end
        object edtBF_ACCNT2: TsEdit
          Left = 87
          Top = 370
          Width = 186
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 38
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51452
        end
        object edtBF_ACCNT1: TsEdit
          Left = 87
          Top = 394
          Width = 282
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 39
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#48264#54840
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #44396#48708#49436#47448
      object panDoc: TsPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 511
        Align = alClient
        BorderWidth = 4
        DoubleBuffered = False
        TabOrder = 0
        object sImage1: TsImage
          Left = 134
          Top = 397
          Width = 16
          Height = 16
          Cursor = crHelp
          Hint = 
            #54408#47785#49345#49464#51221#48372' '#46608#45716' '#49569#44552#45824#49345#49345#49464#51221#48372' '#48143' '#44592#53440#51221#48372' '#51077#47141'<br>'#13#10'<b>1. '#47924#50669#45824#44552' '#51473' '#49324#51204#49569#44552#51064' '#44221#50864' '#44228#50557#49436#49345#51032' '#49569#44552 +
            #45824#49345' '#54408#47785#48324' '#49345#49464#51221#48372#47484' '#50500#47000' '#54805#49885#51004#47196' '#51077#47141'</b><br>'#13#10'- 1'#48264#51704#51460' : HS-CODE<br>'#13#10'- 2'#48264#51704#51460' : ' +
            #45800#44032'<br>'#13#10'- 3'#48264#51704#51460' : '#49688#47049'<br>'#13#10'<b>2. '#47924#50669#45824#44552' '#51473' '#49324#54980#49569#44552' '#46608#45716' '#47924#50669' '#50808' '#45824#44552#51064' '#44221#50864' '#49569#44552#50640' '#45824#54620 +
            ' '#49345#49464#51221#48372' '#46608#45716' '#44592#53440#52280#51312#51221#48372#47484' '#51088#50976' '#54805#49885#51004#47196' '#51077#47141'</b>'
          AutoSize = True
          ParentShowHint = False
          Picture.Data = {07544269746D617000000000}
          ShowHint = True
          Transparent = True
          ImageIndex = 9
          Images = DMICON.System16
          SkinData.SkinSection = 'CHECKBOX'
          UseFullSize = True
        end
        object edtDoc_RFF_NO: TsEdit
          Left = 155
          Top = 200
          Width = 217
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 0
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#49888#44256'/'#44228#50557#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object currDoc_AMT: TsCurrencyEdit
          Left = 189
          Top = 296
          Width = 183
          Height = 23
          DoubleBuffered = False
          TabOrder = 6
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 3
          DisplayFormat = '#,0.00#'
        end
        object currDoc_CIF_AMT: TsCurrencyEdit
          Left = 189
          Top = 344
          Width = 183
          Height = 23
          DoubleBuffered = False
          TabOrder = 10
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 3
          DisplayFormat = '#,0.00#'
        end
        object mskDoc_HS_CODE: TsMaskEdit
          Left = 155
          Top = 248
          Width = 89
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 12
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = 'HS'#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          CheckOnExit = True
          EditMask = '9999.99-9999;0;_'
        end
        object edtDoc_GOODS_NM: TsEdit
          Left = 155
          Top = 368
          Width = 217
          Height = 23
          Color = clWhite
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #45824#54364#47932#54408#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtDoc_IMPT_CD: TsEdit
          Tag = 201
          Left = 155
          Top = 272
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 1
          TabOrder = 4
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Caption = #49688#51077#50857#46020
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sEdit8: TsEdit
          Tag = -1
          Left = 189
          Top = 272
          Width = 183
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
        end
        object mskDoc_CONT_DT: TsMaskEdit
          Left = 155
          Top = 224
          Width = 89
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 1
          Color = clWhite
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#49888#44256#49688#47532'/'#44228#50557#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object edtDoc_TOD_CD: TsEdit
          Tag = 202
          Left = 459
          Top = 200
          Width = 33
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 12
          OnDblClick = edt_ADDED_FEE_TARDblClick
          OnKeyUp = edt_TRD_CDKeyUp
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44032#44201#51312#44148
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object sEdit12: TsEdit
          Tag = -1
          Left = 493
          Top = 200
          Width = 183
          Height = 23
          TabStop = False
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
        end
        object edtDoc_TOD_REMARK: TsEdit
          Left = 459
          Top = 224
          Width = 217
          Height = 23
          TabOrder = 13
          TextHint = #44592#53440' '#49440#53469#49884' '#44032#44201#51312#44148#54217#47928#51077#47141
          BoundLabel.Caption = #45824#54364#47932#54408#47749
        end
        object edtDoc_EXPORT_NAT_CD: TsEdit
          Left = 339
          Top = 224
          Width = 33
          Height = 23
          MaxLength = 2
          TabOrder = 2
          Text = 'ZZ'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#52636#44397'('#49688#52636#44397')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clGray
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object sMemo1: TsMemo
          Left = 155
          Top = 392
          Width = 521
          Height = 89
          ScrollBars = ssVertical
          TabOrder = 20
          WordWrap = False
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Caption = #54408#47785' '#49345#49464#51221#48372#13#10#49569#44552#45824#49345' '#49345#49464#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edtDoc_EXPORT_OW_NM: TsEdit
          Left = 459
          Top = 248
          Width = 217
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 14
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#52636#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtDoc_IMPORT_OW_NM: TsEdit
          Left = 459
          Top = 320
          Width = 217
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtDoc_EXPORT_OW_ADDR: TsEdit
          Left = 459
          Top = 272
          Width = 217
          Height = 23
          MaxLength = 35
          TabOrder = 15
          BoundLabel.Caption = #49688#52636#51088
        end
        object edtDoc_IMPORT_OW_ADDR: TsEdit
          Left = 459
          Top = 344
          Width = 217
          Height = 23
          MaxLength = 35
          TabOrder = 18
          BoundLabel.Caption = #49688#51077#51088
        end
        object edtDoc_EXPORT_OW_NAT_CD: TsEdit
          Left = 459
          Top = 296
          Width = 217
          Height = 23
          MaxLength = 35
          TabOrder = 16
          BoundLabel.Caption = #49688#52636#51088
        end
        object edtDoc_IMPORT_OW_NAT_CD: TsEdit
          Left = 459
          Top = 368
          Width = 217
          Height = 23
          MaxLength = 35
          TabOrder = 19
          BoundLabel.Caption = #49688#51077#51088
        end
        object sDBGrid2: TsDBGrid
          Left = 5
          Top = 33
          Width = 756
          Height = 160
          Align = alTop
          Color = clWhite
          DataSource = dsRFF
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = DrawColumnCell
          DefaultRowHeight = 19
          Columns = <
            item
              Expanded = False
              FieldName = 'ORDER_SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RFF_NO'
              Title.Alignment = taCenter
              Title.Caption = #49688#51077#49888#44256'/'#44228#50557#49436#48264#54840
              Width = 167
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CONT_DT'
              Title.Alignment = taCenter
              Title.Caption = #49688#47532'/'#44228#50557#51068#51088
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Alignment = taCenter
              Title.Caption = #44208#51228'/'#44228#50557#44552#50529
              Width = 120
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REMIT_AMT'
              Title.Alignment = taCenter
              Title.Caption = #49888#52397'('#49569#44552')'#44552#50529
              Width = 144
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'REMIT_AMT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 45
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'IMPT_CD'
              Title.Alignment = taCenter
              Title.Caption = #49688#51077#50857#46020
              Width = 65
              Visible = True
            end>
        end
        object TsPanel
          Left = 5
          Top = 5
          Width = 756
          Height = 28
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          DoubleBuffered = False
          TabOrder = 24
          object sSpeedButton2: TsSpeedButton
            Left = 223
            Top = 2
            Width = 8
            Height = 23
            ButtonStyle = tbsDivider
          end
          object btnDocNew: TsButton
            Left = 0
            Top = -1
            Width = 88
            Height = 28
            Cursor = crHandPoint
            Caption = #49436#47448#52628#44032
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            TabStop = False
            OnClick = btnDocNewClick
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 25
            ContentMargin = 8
          end
          object btnDocEdit: TsButton
            Tag = 1
            Left = 89
            Top = -1
            Width = 66
            Height = 28
            Cursor = crHandPoint
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 1
            TabStop = False
            OnClick = btnDocNewClick
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
            ContentMargin = 8
          end
          object btnDocDel: TsButton
            Left = 156
            Top = -1
            Width = 66
            Height = 28
            Cursor = crHandPoint
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            TabStop = False
            OnClick = btnDocDelClick
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 26
            ContentMargin = 8
          end
          object btnDocExcel: TsButton
            Left = 690
            Top = -1
            Width = 66
            Height = 28
            Cursor = crHandPoint
            Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
            Caption = #50641#49472
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = False
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 31
            ContentMargin = 8
          end
          object btnDocSave: TsButton
            Left = 232
            Top = -1
            Width = 66
            Height = 28
            Cursor = crHandPoint
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            TabStop = False
            OnClick = btnDocSaveClick
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 0
            ContentMargin = 8
          end
          object btnDocCancel: TsButton
            Left = 299
            Top = -1
            Width = 66
            Height = 28
            Cursor = crHandPoint
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 5
            TabStop = False
            OnClick = btnDocSaveClick
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
            ContentMargin = 8
          end
        end
        object edtDoc_AMT_UNIT: TsEdit
          Tag = 98
          Left = 155
          Top = 296
          Width = 33
          Height = 23
          Color = 12775866
          MaxLength = 3
          TabOrder = 5
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44208#51228'/'#44228#50557#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edtDoc_REMIT_AMT_UNIT: TsEdit
          Tag = 98
          Left = 155
          Top = 320
          Width = 33
          Height = 23
          Color = 12775866
          MaxLength = 3
          TabOrder = 7
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#52397'('#49569#44552')'#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object currDoc_REMIT_AMT: TsCurrencyEdit
          Left = 189
          Top = 320
          Width = 183
          Height = 23
          DoubleBuffered = False
          TabOrder = 8
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 3
          DisplayFormat = '#,0.00#'
        end
        object edtDoc_CIF_AMT_UNIT: TsEdit
          Tag = 98
          Left = 155
          Top = 344
          Width = 33
          Height = 23
          Color = 12775866
          MaxLength = 3
          TabOrder = 9
          OnDblClick = edt_ADDED_FEE_TARDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Caption = #44284#49464#44032#44201'(CIF)'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      TabVisible = False
      object sDBGrid3: TsDBGrid
        Left = 0
        Top = 32
        Width = 766
        Height = 479
        TabStop = False
        Align = alClient
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.CustomColor = True
        DefaultRowHeight = 19
        Columns = <
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 36
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 172
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'CD_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 97
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 32
        Align = alTop
        DoubleBuffered = False
        TabOrder = 1
        object sSpeedButton12: TsSpeedButton
          Left = 234
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit3: TsMaskEdit
          Tag = -1
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;1'
          Text = '    -  -  '
        end
        object sMaskEdit4: TsMaskEdit
          Tag = -1
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;1'
          Text = '    -  -  '
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 474
          Top = 4
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
        end
        object sEdit1: TsEdit
          Tag = -1
          Left = 302
          Top = 4
          Width = 171
          Height = 23
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 144
  end
  object sAlphaHints1: TsAlphaHints
    MaxWidth = 500
    Templates = <
      item
        ImageDefault.ImageHeight = 0
        ImageDefault.ImageWidth = 0
        ImageDefault.ClientMargins.Top = 15
        ImageDefault.ClientMargins.Left = 19
        ImageDefault.ClientMargins.Bottom = 23
        ImageDefault.ClientMargins.Right = 19
        ImageDefault.BordersWidths.Top = 16
        ImageDefault.BordersWidths.Left = 32
        ImageDefault.BordersWidths.Bottom = 23
        ImageDefault.BordersWidths.Right = 19
        ImageDefault.ShadowSizes.Top = 5
        ImageDefault.ShadowSizes.Left = 11
        ImageDefault.ShadowSizes.Bottom = 15
        ImageDefault.ShadowSizes.Right = 10
        ImageDefault.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000004E24944415478DAEC9CCF6F1B4514C7BF33BB761C27509408215221
          D1564DC3814AFC3AD0046815A549CA812B9C11EAA93DC00DA957FE05842A4520
          71E801218A6814940372915BA948107100B9A2AE4B8A8552B9C5244E5DC7BBAF
          87EEC2EC666776D675FC63BD4F7A5A2759EFEE7CF27DEFCD8EDF9A21A2BD7D85
          3C3F5F3AC6580BC72074C92E1D8B76B9668B27618F7991AC1760E9186BD37B3C
          BF9B5FAD1E34864717ACFB5B2BDFCFED2BFAF6A56E2A2AAA82A282119D0B6EB8
          3EB7BC71F8D4E5C6997C856E9DBADC3833B7BC7158FCBBEF7D3CE0B86D712242
          9077028E1F8AE9FAECC5DB932E9C9B35AAB890662FDE9E049012F65501DB3368
          01BE278A11C1A41C4F9FF8AA786431573FEBC2212272212DE6EA678F5F284C01
          483B9E52006B87738547026568E619E683269E881FBF503898797AFFC2B91733
          1F4D6430FA7C166300F0540AC316C17A69CC7CF54A2D7B6762EEDD7BB7BEFEB4
          BA876AD0F93D02C048411921705417C401F0373EFFF95076FFA193E78E0E7F28
          C271CD85F4F278EA95ABF79FD878E6CD77FE59FFF67CD507BA1D61C543E040B1
          95162016026717A4F9D5EA0923939D6186F902802100F8F8088E06C111ADB48D
          BBE53AB63E29E0D72E54EB0764357FB71B0F7E5C991DFDC1A9A4E4ABAAA4AAB4
          080821EECB35E9C55CFDF45289D60A9BB4D1B0A8497D620D8B9A854DDA582AD1
          DA62AE7E3A20FFF9F39356A5F2C039B95259582AD1DA8E4D16F5A9EDD8642D95
          686D7EB53A2F140B11D2AEC4CD35AA1703C08CE1D199E9714C982CF03D7D6126
          039F1EC78491C9CEE8967EAE91B41800CE53E9A9038A1CD32F76208B31277FCA
          42CAC3C28C30FF194A71E9B4A06FCC19C390A4BAED4AD066844962DCCC1D932D
          0022D5DDBC6CFEC0630E889C71529092B8A682584C01858E9147C841830088E9
          0092818A9B319DC9215795B898276916729FA60CB141511014E5DE1387085112
          0648419E7D788435A0B80252AE6AF008328C6B88694D9674D788E2AC22E894F9
          38C36849593CE1113DC406514DAC1540892580124009A00450022801145B4034
          401C2851509B144409122F03AE21378A390C655707D7A4191B483B362C103574
          C7C435944300A8B955FDEDFA16EEF43BA09BDBB8DBACFDFB07827B8476B1E012
          92E493206D978BD7F215949B04BB5FE1340976BE82F276B9784D1C9B224AC810
          6EF7C5D5354F0F2200F6E7379FFDB5F9D6FB16B2FB9E1D31618CA731D24F6175
          A386CA777FE3C697BFAC7F917B6F6A198F3E9327672BBEF6C00AFA8847EC2EE3
          F8BFC1C804604E9FBF3A3DF2DCE46BA927C7A6FA2715536367F35EB1B67EFDA7
          FC07AFE701341DB79CADEDBC166111005201F2B7E1893DD1618DE122FC4E9767
          5115B630784B0022C2B102F6FF0F90A9287DA2BB2D22EEC982F6B77B0C104900
          05820828F900E4FD4132404170640DDAE810280AB96E1524251C3F20F20D8A7C
          89CB3F703F1C7F2340371514A4223B249C1004CBD494AB3860EA41385120D98A
          30834E8891E4849622F47AA9D1811479342CFF487390B20D0DDE3E3E289272AF
          29C89FB44943399E89639882FC660B10598082D06505918692C25C99A499E264
          614FD9C8E074B28AC96EB25530542A522669A638B92E9C5E58D791851DE9AC58
          E8FCC799E6B693AAD1551385000B5DFF920D44E731A9A8C74017424D179AF418
          618388AA0ED663E1153567B53CA0B8767A50DB07EEFFE605D71EF7CB06DA6DB2
          67F1A33E37FF70005DA9929C8674CBA50000000049454E44AE426082}
        Img_LeftBottom.ImageHeight = 0
        Img_LeftBottom.ImageWidth = 0
        Img_LeftBottom.ClientMargins.Top = 12
        Img_LeftBottom.ClientMargins.Left = 20
        Img_LeftBottom.ClientMargins.Bottom = 25
        Img_LeftBottom.ClientMargins.Right = 20
        Img_LeftBottom.BordersWidths.Top = 16
        Img_LeftBottom.BordersWidths.Left = 35
        Img_LeftBottom.BordersWidths.Bottom = 27
        Img_LeftBottom.BordersWidths.Right = 22
        Img_LeftBottom.ShadowSizes.Top = 6
        Img_LeftBottom.ShadowSizes.Left = 11
        Img_LeftBottom.ShadowSizes.Bottom = 14
        Img_LeftBottom.ShadowSizes.Right = 10
        Img_LeftBottom.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000005274944415478DAEC9CED4F1C451CC7BF33BB0788174B20D648A1A9
          D1549B68D317D668A931C618DAF42FF085894963FA2F984A7C6334F185BE346A
          15E2B36F4C7CD16AB5B5945A88164C2FBED0D29687889C9506140E08E56EF6E7
          0BEF8EB965E761EFB6C82EF74B267BDCCCCECDEF33DF79DA9D8121A41D19A2C0
          EF4F1D600C9BC88E0C1129CA192A1F1671BAB819D5EA380B790F8B81E314263D
          B3FC9E59C6FD9FB0C8F27BB28CB35608B3BC6E0625D9C0505D03F3702D94225F
          D9A1EF179FE60D8D4F32C7DD17A3BEE61689C26FDEEAAD1F4E3F93EE37343366
          6A720C002F06A708D105903A3CB072AC779232A3399AA118D9AAA0C2688E667A
          2729737860E5188094E49723F9CB74EA673E40253829000DDD67E6BB7B272993
          F748504C2DEF91E89DA44CF799F96E000D1228C707A80C891B409583D3D47CB0
          AB0DED2E0BBC2716E632F0AE36B43B4DCD07557EFAEFE1860EB7AC28E6B8FB76
          A77177DC273EF735A39539EE1E4D93AA60E1DA2A282933C3148703A0D1E717B3
          1DC554FD1147F2ACE49327015A377AB90A28D84280481ADAD729895B28882518
          90B11BB11DC5B60A2066032868D298C4D53CB3991C728B75174B3020665A5B72
          CD42931916A4490064F4931B56E35B51411569B8E5638E2403D23EE3E2216498
          D426663559B279608684AB0836C37C926154A52C5EE711BE896D4535B16A00D5
          AD0EA80EA80EA80EA80EA80E28B180680B71A0BA82225210D5915432E01672A3
          84C3D0ED173236B1B53D3444AB790F22EE44F21E0488566D2B9E5B2887005061
          69E1FAC432E6E20E6862197385A585EB3EF5285B0D877E3351396E393B7E6970
          16D902C18B2B9C02C11B9C4576393B7E4902449AAE841C69B92F3F5D93DF1771
          00FCF7AFDE9DCE3D7554A079DBBD77BA70EE72D1E4C4642B4CDE83185BC2ECC9
          1B18FBE4F2D48703CF3DF435FE7B274FC5ABFCB90256D0DB0B797719C7DA0623
          1740EAC03B17BBD2BBF63CEAA6B7DDCFB8930280E30F626F7B13D2BB9AD1AA2A
          E4E432E6B22B587C7D14BF6C7C574CABF9DCDFE34B535787075F7C621040A118
          44F1EA153FCBB00800E900F10040FEE03CF0424F67C7A1E71F7FEDD9DD475590
          4A708E9FFEB56FEAE40723E39FBFF5A742DAB58C98727E9E2F0829F8E18880F4
          6540AA26C60CA1ACBEB9CC859C9B6EC98D389D338FED6C7B5810444B0A77AC83
          F3DD95DEA9537DC3E39FBD39ED93749043B506A10114144F01433E5054063400
          B4704A3697B9B0D0B0AD6D6198EDB829432AC179F9ECB5F7A7BFF9E8A7B18FDF
          98D63823228021347F8B8078F201820E906CAA77F4FE346559CF5E3E9F6B6CDD
          FECF30DF71737F67EB238220B22B58EC39377622FBEDA73F5EEB7B755A51AB41
          8E45A51ACFA01C2F6024AB80E51840D8BC552D673AFB73FF4263EB3DF323EECE
          BFF677B4ECEDE99F782F7BF68BA1AB275EF9C35783C2004854113CC355550114
          4641A697864C05670DD2B9F9A6ED1DF3C3BC63E6C6F92F2F5E79FBA5294B38C2
          A088304AB251242982D26153DFE31FFE8382BCD98A05CDC82D0A16D548E61FD5
          2860DEA32B0B01C19B387587423C85B31E141BB11505D6162AA2953855192ACC
          F565C834A0BC8078198E67014805A91638364B2553A528D762AEE64755873A4A
          F2E4523A594108A120DC8609A309122C7E5FD9E9EAFA2568FA26DD6EAD9A0A58
          A39A223F0EE56F6AA6937BF2FE62D35EA28D8613069A328F30C72B759347DD46
          C86A6AF1B63D8447444732C3363B553A323D670A5BE00D04190A9069D2C80C79
          8692751416D5B170B70ADA2C649B37C6AB9CD90CC623AA99AA27629B194E5805
          053A55CD7F5CD8EC5064FB770037415D7829D241AF0000000049454E44AE4260
          82}
        Img_RightBottom.ImageHeight = 0
        Img_RightBottom.ImageWidth = 0
        Img_RightBottom.ClientMargins.Top = 12
        Img_RightBottom.ClientMargins.Left = 19
        Img_RightBottom.ClientMargins.Bottom = 25
        Img_RightBottom.ClientMargins.Right = 19
        Img_RightBottom.BordersWidths.Top = 17
        Img_RightBottom.BordersWidths.Left = 21
        Img_RightBottom.BordersWidths.Bottom = 26
        Img_RightBottom.BordersWidths.Right = 34
        Img_RightBottom.ShadowSizes.Top = 6
        Img_RightBottom.ShadowSizes.Left = 11
        Img_RightBottom.ShadowSizes.Bottom = 13
        Img_RightBottom.ShadowSizes.Right = 10
        Img_RightBottom.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C5460000052E4944415478DAEC9CCF6F1B4514C7BFB3BBB6D334A86D024244B8
          A53D041528421529521384108714198E3D70A2B417B87224AD4008F55F40A125
          50150107244E1511299428F2064AA4861F124A9A3629895C3552526C5C2BC4DE
          795CBCD16633BF36719BACED278DC6DEB5379ECF7EDF9B1FFB260C06967149FB
          994B4719C336B28C4B548BEBB01A7F2E566602916DE03C8B01488A724E058A19
          1E6786E7B61216191E27D939112816018EAC8E026D2B2091612D8464A29460CD
          8EFD507CD94AA65E64B67310402A2EF186BCCA045FF96F74E895B62B55285250
          41488EC63D58F0F5AB23CB278FEF4BBDD3D381CEFDAD684F58B0E30268AAE8BC
          905D745E6723CB1F7FF752CBA040654C13BB566130001600BB0A300120D9379C
          EF1B9CA58932278F626A654EDEE02C4DF40DE7FB0024AB6D73AA6DB5820C322E
          21E3122C0DA8D562B7B4F6F674A0D361C2EFC4C21C06ABA7039D764B6BAFAC9D
          E1EF588AC01B846531DB39B8BF15ED711FFB74B5E111663BCF8514B32EBCF803
          5FC754410052718A3986835F9130D6C421C7301E59A83FF3DBC40380D6056947
          02050D0428D883AD539265A02056C78094015A06883530206602080217ABC7D9
          3C53F46452404C3292AE57404C33C794BA98AA1BAC2740DA765A9A197D232A68
          D52E1D65CC325CE6A86740CA550D2BA21CEBD5C5B48325D59A505DAE471BAC87
          297BB146366D2FD6B40803C586544D5341355250D39A809A809A809A809A80E2
          0B881A880335155423055113C95A0696E60354E7D0482710CBF842442B650E2F
          EE44CA1C1E885620C90F520192D1240054B957989E296129EE80664A58AADC2B
          4C439C23B48E85057D561601A052EEE6D5EC227215028F2B9C0A816717912BE5
          6E5E95B4758D655C22C7000E00D0E889C3D9C4B77F5F00D26FC62D81AACCE1CD
          94B0945D44EE8B6B7317464F1C7683375F0549F4F4C27F16EF2715F909460E00
          A7E7DC58CFCE745777E2A13D07C058F24137F6BD27F16C670BDA9E50A4E2CC96
          B0945B46F1EC247E0700E25EB952CCDF28CEFE35EEBEDD9B0550065001E0556B
          5E7DCDAB850050C62552010A825A03A9FADA0E400C965A3C4B13E64A1E78E3DD
          C7D2AF9D7AFEECB1A7DE9241F2E1F40F4F7D3A3F74F1E7E9CF3F9A0B80089720
          202E02640B0085DF6B1FF087E355E08FF00D1612D577FF1CFBD7DEB1B3306EA7
          178EEC7BF8698FE0ED4E60C73A3897AF9F9F1FBAF84B008E175048F83D17B81A
          00A0EBD407B0254064D0545048D0C0CD80A2D08FE700E8EE1F6E21D1B62B3F6E
          EFBD73646FC7333EA4209CDCF75F8E5DFFECC3B910102EA8C380A002248B4D08
          015329A6560A92812600B4F4DB6821B9AB233F6EA717BAD3ED873C82975B46F1
          F48F37CEDDBEFCB53B75FEFD7981522A02E57041905E7D2F02C4204F106786EE
          2492B2693186B778EDA742AAFDD17F7EB51E5FE84EEF397466E4D6406EF82B77
          F293D33AB70A1E27997BF9801C83A9865F7B12978A12A74C03B3515C9C1CE8BF
          652553DE19FB38BF7DE59BECE440FF9C42B9B29BA084C4345D3E93F470A2DC21
          769F7A305D8711CE2FD4B9B7A780B30AC9CFB6D729286CFE45FD1FC825CAD9EC
          3335DDCD8244ADA67190547082E6842ECE14AE667217A36E9FDA082428DC9914
          90480368CD144304080220A4386702A72603444D4C924D93B8A6A7D54E334CEF
          F8566D87628A1E5615834803CA78A78FAA21DB65439D0E124C562264E31C6C70
          435D5485D51ACA66D4ABDA341769235DD4063DA84C0F16312E058F93C1584EE9
          4E1B6EF8166F0B8FE2EE3000650CA7A680EE8749A047B911917638EBD6A4B79D
          491A4311CAA6E06C7B05D5C28D37FB1F18FE1F00AEB493A90B16C2F700000000
          49454E44AE426082}
        Img_RightTop.ImageHeight = 0
        Img_RightTop.ImageWidth = 0
        Img_RightTop.ClientMargins.Top = 16
        Img_RightTop.ClientMargins.Left = 19
        Img_RightTop.ClientMargins.Bottom = 22
        Img_RightTop.ClientMargins.Right = 18
        Img_RightTop.BordersWidths.Top = 16
        Img_RightTop.BordersWidths.Left = 19
        Img_RightTop.BordersWidths.Bottom = 25
        Img_RightTop.BordersWidths.Right = 32
        Img_RightTop.ShadowSizes.Top = 6
        Img_RightTop.ShadowSizes.Left = 11
        Img_RightTop.ShadowSizes.Bottom = 15
        Img_RightTop.ShadowSizes.Right = 10
        Img_RightTop.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000004D04944415478DAEC9B4D6C1B4514C7FFB35EDB8949054DA44A18AA
          6247A254AD7A88C481BA7C091A87FAC005246E44481C1037E889C209CAA970E5
          8028AAD4031207C4A1D0C8AA0A4A6DA470A02A14DA0A2529A92CD1CA81548E71
          B0771F07BCD6663C333B1B7FDBFBA4D1ACEDD98FF9F9FFDECCEEBE61D0B04C9E
          D00F76FE08637EF7C9E4A9A58B67FD0E682750DA09CB401F9B040E7397747663
          FAF865EBCD7476639AFFAD1DB0DBA6A076FED31ED7D8F87CEC9B3BC9F0C40373
          270F864F9CBA563D5D2DFD7D217B7CCFB2AB2D49B6B5D5C4DA09B20330F8EF9C
          6DF6DCD7B713D1DD7BD2270F864FC4C73051A8A074EA5AF5F4D65F77162EBEF8
          B008125F6B41622D5E70A7413251FDCC173792E30FEE9B7DF750F4EDF818261E
          896172B58CF54205A50F7ED9FAA872B7B070E9A5E40A07450A4A058BF995B5C7
          6FED86D504E8A9733F27620F4DCFBE7778FC2D078ED3D881F4FED57F3EAEFCB9
          B6F0DD2BFB570470C80F24E6038EACF603AD2538A9CF9612BB92879E17C11141
          DABC753DBB383FB3CC81213F6EA7A31477CDE62E969E3522D12759C83C0020DA
          ED91ED9DFD382C83C343FAF006AED6BFDA22ABF69B55295F5E3876FF250F58DB
          20E9A8A5B1FDC2F795D75EDE177D233585782286C9B0811006C0AA36AC9532D6
          734514BEBCB5F5C9B74F8F7DAE82A48A474E310084009800C20022E9EC46FACC
          2A5DA9DA64D1805AD526EBCC2A5D99BD509C0310A9F7CDACF7D57033C8E40999
          3C09278A4C544263B1A3A929C44DD6DF934B95990C466A0AF1D0F8C451593F65
          3369A6703906C06021F34042E1F783628918268D70E4314E314D61C699F89ABA
          0A02101D9498A3B27A1FA22E71D81C24D2B917E3E39181E133C3CBBD7805F10D
          47019033823157BD4D493A0A62430A88E9284877141B560579BA99A141D8E8E2
          DD7C2F15A4F5C04C36936643AA20E6718F29753148761E76053119452F48C308
          081E93649C3FC298A1F998639815A47CAA61F8A43C12CAD18D411821F7822A06
          0DBB525A8266043CBCE702A314777CF73550500B0A0A2C0014000A0005800240
          830B88468803050A6A93822840E2FD5E8C4F5BA311722DDAA98B1188FEADDAB0
          86100EE9BA18290E40B5CD7BBFAF94B13EE8546E9670B756DAF8158A4C33F767
          4342B02965AD5C585ECA1551A811EC41855323D8B9220AE5C2F2123CD2F180FF
          F3844C0D3800408BF333B9F0577F9C05F6BE3A6809548E72724514CEFDB47676
          717E2607758267C3446F2F9C378E4E52919360640230539FFE90BA6FEFA38F87
          77ED4E82B1C8A000AADE5BBFBEB976F3C7DCEB4FE400D4EAC5AAD7767DDBAE17
          0240993C910A901BD43648F5ED900BA2BBF4EA5D9A5B0536572C57E1E15882F6
          0D403217E38B9343E39C4CD4DEEE33402401D40442108F1A662A4E26022482E3
          9504C0BA044676DD2A484A383C20E23AE53E09041DE7E1C8DE75F742412215D9
          2A771200F654100480DC07E927387E20D90A37838E8B91E48496C2F5B45249BA
          08893CDCCD56B4130252A6A1419CE8280ACAFDA6203E68938672B625937B2988
          37DB05910914841E0382249EF829CA20CD14AEA64ABC5629A79BA398EC465405
          A3098C7B29822A4833C5C975E1F4D3DD3AC1C7FA31F8F8C77BB51CAA936A9282
          F15A0EA5D3E16E2EA86B158ED743B11D2DA8F3ABB07E508E9F8146F858A3D50E
          0D65A6473B16F53A07F26CD3C165E11DEBBC8EFD3700D38A07EBEBD501E50000
          000049454E44AE426082}
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Name = 'White Baloon Border'
      end>
    TemplateName = 'White Baloon Border'
    TextAlignment = taLeftJustify
    HTMLMode = True
    SkinSection = 'HINT'
    Left = 168
    Top = 8
  end
  object qryRFF: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryRFFAfterOpen
    AfterScroll = qryRFFAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'TEMP_KEY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9'MAINT_NO, SEQ, ROW_NUMBER() OVER(ORDER BY SEQ) as ORDER_SEQ, RF' +
        'F_NO, AMT, AMT_UNIT, REMIT_AMT, REMIT_AMT_UNIT, CIF_AMT, CIF_AMT' +
        '_UNIT, HS_CD, '
      
        #9'GOODS_NM, IMPT_CD, IMP.NAME as IMPT_NM, CONT_DT, TOD_CD, TOD.NA' +
        'ME as TOD_NM, TOD_REMARK, EXPORT_NAT_CD, EXPORT_OW_NM, EXPORT_OW' +
        '_ADDR, EXPORT_OW_NAT_CD, '
      
        #9'DETAIL_DESC1, DETAIL_DESC2, DETAIL_DESC3, DETAIL_DESC4, DETAIL_' +
        'DESC5,'
      #9'IMPORT_OW_NM, IMPORT_OW_ADDR, IMPORT_OW_NAT_CD'
      'FROM'
      #9'APPRMI_RFF'
      'LEFT JOIN '
      
        #9'(SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ' +
        #39'IMP_CD'#39') IMP ON APPRMI_RFF.IMPT_CD = IMP.CODE'
      'LEFT JOIN'
      
        #9'(SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ' +
        #39#44032#44201#51312#44148#39') TOD ON APPRMI_RFF.TOD_CD = TOD.CODE'
      'WHERE MAINT_NO IN (:MAINT_NO, :TEMP_KEY)'
      'AND TMP_DEL = 0'
      'ORDER BY SEQ')
    Left = 352
    Top = 568
    object qryRFFMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryRFFRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryRFFAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,0.00'
      Precision = 19
    end
    object qryRFFAMT_UNIT: TStringField
      FieldName = 'AMT_UNIT'
      Size = 3
    end
    object qryRFFREMIT_AMT: TBCDField
      FieldName = 'REMIT_AMT'
      DisplayFormat = '#,0.00'
      Precision = 19
    end
    object qryRFFREMIT_AMT_UNIT: TStringField
      FieldName = 'REMIT_AMT_UNIT'
      Size = 3
    end
    object qryRFFCIF_AMT: TBCDField
      FieldName = 'CIF_AMT'
      Precision = 19
    end
    object qryRFFCIF_AMT_UNIT: TStringField
      FieldName = 'CIF_AMT_UNIT'
      Size = 3
    end
    object qryRFFHS_CD: TStringField
      FieldName = 'HS_CD'
      Size = 10
    end
    object qryRFFGOODS_NM: TStringField
      FieldName = 'GOODS_NM'
      Size = 35
    end
    object qryRFFIMPT_CD: TStringField
      FieldName = 'IMPT_CD'
      Size = 3
    end
    object qryRFFCONT_DT: TStringField
      FieldName = 'CONT_DT'
      Size = 10
    end
    object qryRFFTOD_CD: TStringField
      FieldName = 'TOD_CD'
      Size = 3
    end
    object qryRFFTOD_REMARK: TStringField
      FieldName = 'TOD_REMARK'
      Size = 70
    end
    object qryRFFEXPORT_NAT_CD: TStringField
      FieldName = 'EXPORT_NAT_CD'
      Size = 2
    end
    object qryRFFEXPORT_OW_NM: TStringField
      FieldName = 'EXPORT_OW_NM'
      Size = 35
    end
    object qryRFFEXPORT_OW_ADDR: TStringField
      FieldName = 'EXPORT_OW_ADDR'
      Size = 35
    end
    object qryRFFEXPORT_OW_NAT_CD: TStringField
      FieldName = 'EXPORT_OW_NAT_CD'
      Size = 35
    end
    object qryRFFDETAIL_DESC1: TStringField
      FieldName = 'DETAIL_DESC1'
      Size = 70
    end
    object qryRFFDETAIL_DESC2: TStringField
      FieldName = 'DETAIL_DESC2'
      Size = 70
    end
    object qryRFFDETAIL_DESC3: TStringField
      FieldName = 'DETAIL_DESC3'
      Size = 70
    end
    object qryRFFDETAIL_DESC4: TStringField
      FieldName = 'DETAIL_DESC4'
      Size = 70
    end
    object qryRFFDETAIL_DESC5: TStringField
      FieldName = 'DETAIL_DESC5'
      Size = 70
    end
    object qryRFFIMPORT_OW_NM: TStringField
      FieldName = 'IMPORT_OW_NM'
      Size = 35
    end
    object qryRFFIMPORT_OW_ADDR: TStringField
      FieldName = 'IMPORT_OW_ADDR'
      Size = 35
    end
    object qryRFFIMPORT_OW_NAT_CD: TStringField
      FieldName = 'IMPORT_OW_NAT_CD'
      Size = 35
    end
    object qryRFFSEQ: TAutoIncField
      FieldName = 'SEQ'
      ReadOnly = True
    end
    object qryRFFORDER_SEQ: TLargeintField
      FieldName = 'ORDER_SEQ'
      ReadOnly = True
    end
    object qryRFFIMPT_NM: TStringField
      FieldName = 'IMPT_NM'
      ReadOnly = True
      Size = 100
    end
    object qryRFFTOD_NM: TStringField
      FieldName = 'TOD_NM'
      ReadOnly = True
      Size = 100
    end
  end
  object dsRFF: TDataSource
    DataSet = qryRFF
    Left = 384
    Top = 568
  end
  object qryAPPRMI_H: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryAPPRMI_HAfterOpen
    AfterScroll = qryAPPRMI_HAfterScroll
    Parameters = <
      item
        Name = 'FROM_DT'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'TO_DT'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[APP_DT]'
      '      ,[REG_DT]'
      '      ,[USER_ID]'
      '      ,[TRN_HOPE_DT]'
      '      ,[IMPT_SCHEDULE_DT]'
      '      ,[ETC_NO]'
      '      ,[TRD_CD]'
      #9'  ,CD_BFCD.NAME as TRD_NM'
      '      ,[IMPT_REMIT_CD]'
      #9'  ,CD_BUS_CD.NAME as IMPT_REMIT_NM'
      '      ,[IMPT_TRAN_FEE_CD]'
      #9'  ,CD_BUS_CD1.NAME as IMPT_TRAN_FEE_NM'
      '      ,[REMIT_DESC]'
      '      ,[REMIT_CD]'
      #9'  ,CD_SND.NAME as REMIT_NM'
      '      ,[ADDED_FEE_TAR]'
      #9'  ,CD_CHARGETO.NAME as ADDED_FEE_TAR_NM'
      '      ,[TOTAL_AMT]'
      '      ,[TOTAL_AMT_UNIT]'
      '      ,[FORE_AMT]'
      '      ,[FORE_AMT_UNIT]'
      '      ,[ONLY_PWD]'
      '      ,[AUTH_NM1]'
      '      ,[AUTH_NM2]'
      '      ,[AUTH_NM3]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '  FROM [APPRMI_H]'
      
        'LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE ' +
        'Prefix = '#39'PAYORDBFCD'#39') CD_BFCD ON APPRMI_H.TRD_CD = CD_BFCD.CODE'
      
        'LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE ' +
        'Prefix = '#39'4487'#39') CD_BUS_CD ON APPRMI_H.IMPT_REMIT_CD = CD_BUS_CD' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE ' +
        'Prefix = '#39'4383'#39') CD_BUS_CD1 ON APPRMI_H.IMPT_TRAN_FEE_CD = CD_BU' +
        'S_CD1.CODE'
      
        'LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE ' +
        'Prefix = '#39#49569#44552#48169#48277#39') CD_SND ON APPRMI_H.REMIT_CD = CD_SND.CODE'
      
        'LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE ' +
        'Prefix = '#39#49688#49688#47308#48512#45812#39') CD_CHARGETO ON APPRMI_H.ADDED_FEE_TAR = CD_CHA' +
        'RGETO.CODE'
      ' WHERE REG_DT BETWEEN :FROM_DT AND :TO_DT')
    Left = 24
    Top = 176
    object qryAPPRMI_HMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryAPPRMI_HAPP_DT: TStringField
      FieldName = 'APP_DT'
      Size = 10
    end
    object qryAPPRMI_HREG_DT: TDateTimeField
      FieldName = 'REG_DT'
      DisplayFormat = 'yyyy-mm-dd hh:mm'
    end
    object qryAPPRMI_HUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryAPPRMI_HTRN_HOPE_DT: TStringField
      FieldName = 'TRN_HOPE_DT'
      Size = 10
    end
    object qryAPPRMI_HIMPT_SCHEDULE_DT: TStringField
      FieldName = 'IMPT_SCHEDULE_DT'
      Size = 10
    end
    object qryAPPRMI_HETC_NO: TStringField
      FieldName = 'ETC_NO'
      Size = 70
    end
    object qryAPPRMI_HTRD_CD: TStringField
      FieldName = 'TRD_CD'
      Size = 3
    end
    object qryAPPRMI_HIMPT_REMIT_CD: TStringField
      FieldName = 'IMPT_REMIT_CD'
      Size = 3
    end
    object qryAPPRMI_HIMPT_TRAN_FEE_CD: TStringField
      FieldName = 'IMPT_TRAN_FEE_CD'
      Size = 3
    end
    object qryAPPRMI_HREMIT_DESC: TStringField
      FieldName = 'REMIT_DESC'
      Size = 350
    end
    object qryAPPRMI_HREMIT_CD: TStringField
      FieldName = 'REMIT_CD'
      Size = 1
    end
    object qryAPPRMI_HADDED_FEE_TAR: TStringField
      FieldName = 'ADDED_FEE_TAR'
      Size = 3
    end
    object qryAPPRMI_HTOTAL_AMT: TBCDField
      FieldName = 'TOTAL_AMT'
      Precision = 19
    end
    object qryAPPRMI_HTOTAL_AMT_UNIT: TStringField
      FieldName = 'TOTAL_AMT_UNIT'
      Size = 3
    end
    object qryAPPRMI_HFORE_AMT: TBCDField
      FieldName = 'FORE_AMT'
      Precision = 19
    end
    object qryAPPRMI_HFORE_AMT_UNIT: TStringField
      FieldName = 'FORE_AMT_UNIT'
      Size = 3
    end
    object qryAPPRMI_HONLY_PWD: TStringField
      FieldName = 'ONLY_PWD'
      Size = 17
    end
    object qryAPPRMI_HAUTH_NM1: TStringField
      FieldName = 'AUTH_NM1'
      Size = 35
    end
    object qryAPPRMI_HAUTH_NM2: TStringField
      FieldName = 'AUTH_NM2'
      Size = 35
    end
    object qryAPPRMI_HAUTH_NM3: TStringField
      FieldName = 'AUTH_NM3'
      Size = 35
    end
    object qryAPPRMI_HMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryAPPRMI_HMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryAPPRMI_HCHK2: TIntegerField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
    end
    object qryAPPRMI_HTRD_NM: TStringField
      FieldName = 'TRD_NM'
      ReadOnly = True
      Size = 100
    end
    object qryAPPRMI_HIMPT_REMIT_NM: TStringField
      FieldName = 'IMPT_REMIT_NM'
      ReadOnly = True
      Size = 100
    end
    object qryAPPRMI_HIMPT_TRAN_FEE_NM: TStringField
      FieldName = 'IMPT_TRAN_FEE_NM'
      ReadOnly = True
      Size = 100
    end
    object qryAPPRMI_HREMIT_NM: TStringField
      FieldName = 'REMIT_NM'
      ReadOnly = True
      Size = 100
    end
    object qryAPPRMI_HADDED_FEE_TAR_NM: TStringField
      FieldName = 'ADDED_FEE_TAR_NM'
      ReadOnly = True
      Size = 100
    end
    object qryAPPRMI_HCHK3: TIntegerField
      FieldName = 'CHK3'
    end
  end
  object dsAPPRMI_H: TDataSource
    DataSet = qryAPPRMI_H
    Left = 56
    Top = 176
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPRMI_H'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 24
    Top = 144
  end
end
