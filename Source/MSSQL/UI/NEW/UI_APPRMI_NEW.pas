unit UI_APPRMI_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sCheckBox, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  Buttons, sBitBtn, Grids, DBGrids, acDBGrid, sComboBox, Mask, sMaskEdit,
  sEdit, sButton, sLabel, sSpeedButton, ExtCtrls, acImage, sPanel, DateUtils,
  acAlphaHints, BankPassword, TypeDefine, CodeDialogParent, DB, ADODB, SQLCreator,
  StrUtils;

type
  TUI_APPRMI_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sImage2: TsImage;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sButton13: TsButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    btnCopy: TsButton;
    sPanel6: TsPanel;
    edtMAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    edt_userno: TsEdit;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    msk_APP_DATE: TsMaskEdit;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    msk_TRN_HOPE_DT: TsMaskEdit;
    msk_IMPT_SCHEDULE_DT: TsMaskEdit;
    edt_ETC_NO: TsEdit;
    edt_TRD_CD: TsEdit;
    sEdit3: TsEdit;
    edt_IMPT_REMIT_CD: TsEdit;
    sEdit5: TsEdit;
    edt_IMPT_TRAN_FEE_CD: TsEdit;
    sEdit6: TsEdit;
    sLabel12: TsLabel;
    edt_REMIT_DESC1: TsEdit;
    edt_REMIT_DESC2: TsEdit;
    edt_REMIT_DESC3: TsEdit;
    edt_REMIT_DESC4: TsEdit;
    edt_REMIT_CD: TsEdit;
    sEdit9: TsEdit;
    edt_ADDED_FEE_TAR: TsEdit;
    sEdit11: TsEdit;
    edt_TOTAL_AMT_UNIT: TsEdit;
    curr_TOTAL_AMT: TsCurrencyEdit;
    edt_FORE_AMT_UNIT: TsEdit;
    curr_FORE_AMT: TsCurrencyEdit;
    sImage3: TsImage;
    sAlphaHints1: TsAlphaHints;
    edtOD_BANK: TsEdit;
    edtOD_BANK1: TsEdit;
    edtOD_BANK2: TsEdit;
    edtOD_NAME1: TsEdit;
    edtOD_NAME2: TsEdit;
    edtOD_Nation: TsEdit;
    edtOD_CURR: TsEdit;
    edtOD_ACCNT2: TsEdit;
    edtOD_ACCNT1: TsEdit;
    sPanel15: TsPanel;
    edtEC_BANK: TsEdit;
    edtEC_BANK1: TsEdit;
    edtEC_BANK2: TsEdit;
    edtEC_NAME1: TsEdit;
    edtEC_NAME2: TsEdit;
    edtEC_Nation: TsEdit;
    edtEC_ACCNT2: TsEdit;
    edtEC_ACCNT1: TsEdit;
    sPanel14: TsPanel;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    sPanel13: TsPanel;
    edtI1_BANK: TsEdit;
    edtI1_BANK1: TsEdit;
    edtI1_BANK2: TsEdit;
    edtI1_NAME1: TsEdit;
    edtI1_NAME2: TsEdit;
    edtI1_Nation: TsEdit;
    edtI1_CURR: TsEdit;
    edtI1_ACCNT2: TsEdit;
    edtI1_ACCNT1: TsEdit;
    Edt_ApplicationName1: TsEdit;
    Edt_ApplicationSAUPNO: TsEdit;
    Edt_ApplicationCode: TsEdit;
    Edt_ApplicationAddr1: TsEdit;
    Edt_ApplicationAddr2: TsEdit;
    edtBF_BANK: TsEdit;
    edtBF_BANK1: TsEdit;
    edtBF_BANK2: TsEdit;
    edtBF_NAME1: TsEdit;
    edtBF_NAME2: TsEdit;
    edtBF_Nation: TsEdit;
    edtBF_CURR: TsEdit;
    edtBF_ACCNT2: TsEdit;
    edtBF_ACCNT1: TsEdit;
    edtBEN_CODE: TsEdit;
    edtBEN_NAME1: TsEdit;
    edtBEN_NAME2: TsEdit;
    edtBEN_NAME3: TsEdit;
    edtBEN_STR1: TsEdit;
    edtBEN_STR2: TsEdit;
    edtBEN_TELE: TsEdit;
    edtBEN_STR3: TsEdit;
    edtBEN_NATION: TsEdit;
    sTabSheet2: TsTabSheet;
    panDoc: TsPanel;
    edt_ONLY_PWD: TsEdit;
    Edt_ApplicationElectronicSign: TsEdit;
    sSpeedButton1: TsSpeedButton;
    edtDoc_RFF_NO: TsEdit;
    currDoc_AMT: TsCurrencyEdit;
    currDoc_CIF_AMT: TsCurrencyEdit;
    mskDoc_HS_CODE: TsMaskEdit;
    edtDoc_GOODS_NM: TsEdit;
    edtDoc_IMPT_CD: TsEdit;
    sEdit8: TsEdit;
    mskDoc_CONT_DT: TsMaskEdit;
    edtDoc_TOD_CD: TsEdit;
    sEdit12: TsEdit;
    edtDoc_TOD_REMARK: TsEdit;
    edtDoc_EXPORT_NAT_CD: TsEdit;
    sMemo1: TsMemo;
    edtDoc_EXPORT_OW_NM: TsEdit;
    edtDoc_IMPORT_OW_NM: TsEdit;
    edtDoc_EXPORT_OW_ADDR: TsEdit;
    edtDoc_IMPORT_OW_ADDR: TsEdit;
    edtDoc_EXPORT_OW_NAT_CD: TsEdit;
    edtDoc_IMPORT_OW_NAT_CD: TsEdit;
    sDBGrid2: TsDBGrid;
    btnDocNew: TsButton;
    btnDocEdit: TsButton;
    btnDocDel: TsButton;
    btnDocExcel: TsButton;
    sSpeedButton2: TsSpeedButton;
    qryRFF: TADOQuery;
    dsRFF: TDataSource;
    edtDoc_AMT_UNIT: TsEdit;
    edtDoc_REMIT_AMT_UNIT: TsEdit;
    currDoc_REMIT_AMT: TsCurrencyEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    btnDocSave: TsButton;
    btnDocCancel: TsButton;
    edtDoc_CIF_AMT_UNIT: TsEdit;
    qryAPPRMI_H: TADOQuery;
    dsAPPRMI_H: TDataSource;
    qryAPPRMI_HMAINT_NO: TStringField;
    qryAPPRMI_HAPP_DT: TStringField;
    qryAPPRMI_HREG_DT: TDateTimeField;
    qryAPPRMI_HUSER_ID: TStringField;
    qryAPPRMI_HTRN_HOPE_DT: TStringField;
    qryAPPRMI_HIMPT_SCHEDULE_DT: TStringField;
    qryAPPRMI_HETC_NO: TStringField;
    qryAPPRMI_HTRD_CD: TStringField;
    qryAPPRMI_HIMPT_REMIT_CD: TStringField;
    qryAPPRMI_HIMPT_TRAN_FEE_CD: TStringField;
    qryAPPRMI_HREMIT_DESC: TStringField;
    qryAPPRMI_HREMIT_CD: TStringField;
    qryAPPRMI_HADDED_FEE_TAR: TStringField;
    qryAPPRMI_HTOTAL_AMT: TBCDField;
    qryAPPRMI_HTOTAL_AMT_UNIT: TStringField;
    qryAPPRMI_HFORE_AMT: TBCDField;
    qryAPPRMI_HFORE_AMT_UNIT: TStringField;
    qryAPPRMI_HONLY_PWD: TStringField;
    qryAPPRMI_HAUTH_NM1: TStringField;
    qryAPPRMI_HAUTH_NM2: TStringField;
    qryAPPRMI_HAUTH_NM3: TStringField;
    qryAPPRMI_HMESSAGE1: TStringField;
    qryAPPRMI_HMESSAGE2: TStringField;
    qryAPPRMI_HCHK2: TIntegerField;
    qryAPPRMI_HTRD_NM: TStringField;
    qryAPPRMI_HIMPT_REMIT_NM: TStringField;
    qryAPPRMI_HIMPT_TRAN_FEE_NM: TStringField;
    qryAPPRMI_HREMIT_NM: TStringField;
    qryAPPRMI_HADDED_FEE_TAR_NM: TStringField;
    qryRFFMAINT_NO: TStringField;
    qryRFFRFF_NO: TStringField;
    qryRFFAMT: TBCDField;
    qryRFFAMT_UNIT: TStringField;
    qryRFFREMIT_AMT: TBCDField;
    qryRFFREMIT_AMT_UNIT: TStringField;
    qryRFFCIF_AMT: TBCDField;
    qryRFFCIF_AMT_UNIT: TStringField;
    qryRFFHS_CD: TStringField;
    qryRFFGOODS_NM: TStringField;
    qryRFFIMPT_CD: TStringField;
    qryRFFCONT_DT: TStringField;
    qryRFFTOD_CD: TStringField;
    qryRFFTOD_REMARK: TStringField;
    qryRFFEXPORT_NAT_CD: TStringField;
    qryRFFEXPORT_OW_NM: TStringField;
    qryRFFEXPORT_OW_ADDR: TStringField;
    qryRFFEXPORT_OW_NAT_CD: TStringField;
    qryRFFDETAIL_DESC1: TStringField;
    qryRFFDETAIL_DESC2: TStringField;
    qryRFFDETAIL_DESC3: TStringField;
    qryRFFDETAIL_DESC4: TStringField;
    qryRFFDETAIL_DESC5: TStringField;
    qryRFFIMPORT_OW_NM: TStringField;
    qryRFFIMPORT_OW_ADDR: TStringField;
    qryRFFIMPORT_OW_NAT_CD: TStringField;
    qryRFFSEQ: TAutoIncField;
    qryRFFORDER_SEQ: TLargeintField;
    edt_AUTH_NM1: TsEdit;
    edt_AUTH_NM2: TsEdit;
    qryRFFIMPT_NM: TStringField;
    qryRFFTOD_NM: TStringField;
    sImage1: TsImage;
    qryReady: TADOQuery;
    qryAPPRMI_HCHK3: TIntegerField;
    sImage4: TsImage;
    edtOY_TEL: TsEdit;
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNewClick(Sender: TObject);
    procedure edt_ADDED_FEE_TARDblClick(Sender: TObject);
    procedure edt_TRD_CDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtBEN_CODEDblClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edtBF_BANKDblClick(Sender: TObject);
    procedure edtOD_BANKDblClick(Sender: TObject);
    procedure btnDocNewClick(Sender: TObject);
    procedure btnDocSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryAPPRMI_HAfterScroll(DataSet: TDataSet);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure qryAPPRMI_HAfterOpen(DataSet: TDataSet);
    procedure qryRFFAfterScroll(DataSet: TDataSet);
    procedure qryRFFAfterOpen(DataSet: TDataSet);
    procedure btnDocDelClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    CDDialog : TCodeDialogParent_frm;
    tempKey : String;
    FSQL : String;
    subDataMode : TProgramControlType;
    requireDataFontStyle : TFont;
    normalDataFontStyle : TFont;
    noDataFontStyle : TFont;

    procedure createTempKey;
    procedure clearTempKey;

    procedure BtnControl;
    procedure ReadDocList(DocNo : String);
    procedure newDocument(CopyDocNo : String='');
    procedure editDocument;

    procedure DocBtnControl;
    procedure insertRFF;
    procedure readRFFData;

    procedure ReadAPPRMI_H;
    procedure ReadAPPRMI_H_DATA;
    procedure ReadAPPRMI_NAD(MAINT_NO : String);
    procedure ReadAPPRMI_FII(MAINT_NO : String);
    procedure SaveData(Sender: TObject);
    procedure SaveDataNAD;
    procedure SaveDataFII;

    procedure SaveTempData;
    procedure DeleteTempData;
    procedure RollbakTempDelData(MAINT_NO : String);
    procedure CommitTempDelData(MAINT_NO : String);
    function ValidData:Boolean;
    function ValidDocData:Boolean;
    procedure showHintControl(Control: TControl; msg: String;
      showDelay: integer=0);
    function isPRE_CODE:Boolean;
    function isAFTER_CODE:Boolean;
    function isETC_CODE:Boolean;
    procedure changeLabels;
    function copyDocument:String;
  protected
    procedure ReadyDocument(DocNo: String); override;
  public
    { Public declarations }
  end;

var
  UI_APPRMI_NEW_frm: TUI_APPRMI_NEW_frm;
const
  DOCUMENT_CODE : String = 'APPRMI';
  PRE_CODE : array [0..4] of String = ('11', '12', '31', '33', '35');
  AFTER_CODE : array [0..3] of String = ('13', '32', '34', '36');
  ETC_CODE : string = 'P01';

  requireLabel : TFontStyles = [fsBold, fsUnderline];
  normalLabel : TFontStyles = [];
implementation

uses
  MSSQL, VarDefine, Commonlib, CodeContents, CD_ChargeTo, CD_PAYORDBFCD, CD_4487, CD_4383, CD_SNDCD, CD_AMT_UNIT, CD_NATION, CD_CUST, CD_BANK_SWIFT, CD_BANK, AutoNo, MessageDefine, CD_IMP_CD, CD_TERM, CreateDocuments, DocumentSend, Dlg_RecvSelect;

{$R *.dfm}

procedure TUI_APPRMI_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  subDataMode := ctView;
  sMaskEdit1.Text := FormatDateTime('YYYY-MM-DD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYY-MM-DD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  ReadOnlyControl(panDoc);

  sPageControl1.ActivePageIndex := 0;

  ReadAPPRMI_H;  

end;

procedure TUI_APPRMI_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APPRMI_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  normalDataFontStyle.Free;
  requireDataFontStyle.Free;
  noDataFontStyle.Free;

  Action := caFree;
  UI_APPRMI_NEW_frm := nil;
end;

procedure TUI_APPRMI_NEW_frm.btnNewClick(Sender: TObject);
begin
  createTempKey;
  newDocument();
  ReadDocList(edtMAINT_NO.Text);
  BtnControl;
end;

procedure TUI_APPRMI_NEW_frm.edt_ADDED_FEE_TARDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Case (Sender as TsEdit).Tag of
    //통화단위
    98 : CDDialog := TCD_AMT_UNIT_frm.Create(Self);
    // 거래형태구분
    100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    // 지급지시서용도
    101 : CDDialog := TCD_4487_frm.Create(Self);
    // 납부수수료유형
    102 : CDDialog := TCD_4383_frm.Create(Self);
    // 송금방법
    103 : CDDialog := TCD_SNDCD_frm.Create(Self);
    // 수수료부담자
    104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
    // 국가
    105 : CDDialog := TCD_NATION_frm.Create(Self);
    // 수입용도
    201 : CDDialog := TCD_IMP_CD_frm.Create(Self);
    // 가격조건
    202 : CDDialog := TCD_TERM_frm.Create(Self);
  end;


  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        100 :
        begin
          sEdit3.Text  := CDDialog.Values[1];
          changeLabels;
        end;
        101 :
        begin
          sEdit5.Text  := CDDialog.Values[1];
          edt_IMPT_TRAN_FEE_CD.clear;
          sEdit6.clear;
          edt_IMPT_TRAN_FEE_CD.Enabled := (Sender as TsEdit).Text = '2AJ';
          sEdit6.Enabled := edt_IMPT_TRAN_FEE_CD.Enabled;
        end;
        102 : sEdit6.Text := CDDialog.Values[1];
        103 : sEdit9.Text := CDDialog.Values[1];
        104 : sEdit11.Text := CDDialog.Values[1];

        201 : sEdit8.Text := CDDialog.Values[1];
        202 : sEdit12.Text := CDDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.edt_TRD_CDKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TMP_STR : string;
begin
  inherited;
  Case (Sender as TsEdit).Tag of
    // 거래형태구분
    100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    // 지급지시서용도
    101 : CDDialog := TCD_4487_frm.Create(Self);
    // 납부수수료유형
    102 : CDDialog := TCD_4383_frm.Create(Self);
    // 송금방법
    103 : CDDialog := TCD_SNDCD_frm.Create(Self);
    // 수수료부담자
    104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
    // 수입용도
    201 : CDDialog := TCD_IMP_CD_frm.Create(Self);
    // 가격조건
    202 : CDDialog := TCD_TERM_frm.Create(Self);
  end;

  try
    TMP_STR := CDDialog.GetCodeText((Sender as TsEdit).Text);
    Case (Sender as TsEdit).Tag of
      100 : sEdit3.Text := TMP_STR;
      101 :
      begin
        sEdit5.Text  := TMP_STR;
        edt_IMPT_TRAN_FEE_CD.clear;
        sEdit6.clear;
        edt_IMPT_TRAN_FEE_CD.Enabled := (Sender as TsEdit).Text = '2AJ';
        sEdit6.Enabled := edt_IMPT_TRAN_FEE_CD.Enabled;
      end;
      102 : sEdit6.Text := TMP_STR;
      103 : sEdit9.Text := TMP_STR;
      104 : sEdit11.Text := TMP_STR;
      201 : sEdit8.Text := TMP_STR;
      202 : sEdit12.Text := TMP_STR;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.edtBEN_CODEDblClick(Sender: TObject);
begin
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        //지급의뢰인
//        101:
//        begin
//          edtAPP_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
//          edtAPP_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
//          edtAPP_NAME3.Text := CD_CUST_frm.FieldValues('SAUP_NO');
//          edtAPP_STR1.Text := CD_CUST_frm.FieldValues('ADDR1');
//          edtAPP_TELE.Text := CD_CUST_frm.FieldValues('REP_TEL');
//        end;
        //수익자
        102:
        begin
          edtBEN_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtBEN_STR1.Text  := CD_CUST_frm.FieldValues('ADDR1');
          edtBEN_STR2.Text  := CD_CUST_frm.FieldValues('ADDR2');
          edtBEN_STR3.Text  := CD_CUST_frm.FieldValues('ADDR3');
          edtBEN_TELE.Text  := CD_CUST_frm.FieldValues('REP_TEL');
          edtBEN_NATION.Text := CD_CUST_frm.FieldValues('NAT');
        end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_APPRMI_NEW_frm.BtnControl;
begin
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnEdit.Enabled;
  btnCopy.Enabled := btnEdit.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := btnTemp.Enabled;
  btnCancel.Enabled := btnTemp.Enabled;

  btnPrint.Enabled := btnEdit.Enabled;
  btnSend.Enabled  := btnEdit.Enabled;
  btnReady.Enabled  := btnEdit.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sPanel29.Visible := not btnNew.Enabled;
  // 서브 버튼
  if ProgramControlType = ctView then
  begin
    btnDocNew.Enabled := false;
    btnDocEdit.Enabled := btnDocNew.Enabled and (qryRFF.RecordCount > 0);
    btnDocDel.Enabled := btnDocEdit.Enabled;
    btnDocCancel.Enabled := btnDocNew.Enabled;
    btnDocSave.Enabled := btnDocCancel.Enabled;
  end
  else
  begin
    btnDocNew.Enabled := true;
    btnDocEdit.Enabled := btnDocNew.Enabled and (qryRFF.RecordCount > 0);
    btnDocDel.Enabled := btnDocEdit.Enabled;
    btnDocCancel.Enabled := not btnDocNew.Enabled;
    btnDocSave.Enabled := btnDocCancel.Enabled;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnCancelClick(Sender: TObject);
var
  MAINT_NO : String;
begin
  inherited;
  MAINT_NO := edtMAINT_NO.Text;

  if ((Sender as TsButton).Name = 'btnSave') OR ((Sender as TsButton).Name = 'btnTemp') then
  begin
    if btnDocSave.Enabled Then
    begin
      MessageBox(Self.Handle,MSG_APPRMI_DOCUMENT_WRITING,'저장실패',MB_OK+MB_ICONINFORMATION);
      Exit;
    end;

    if ValidData then exit;

    DMMssql.BeginTrans;
    try
      SaveData(Sender);
      SaveDataNAD;
      SaveDataFII;
      CommitTempDelData(MAINT_NO);
      SaveTempData;
      DMMssql.CommitTrans;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
        DMMssql.RollbackTrans;
      end;
    end;

    clearTempKey;
    ReadAPPRMI_H;
    qryAPPRMI_H.Locate('MAINT_NO', MAINT_NO, []);

  end
  else
  if (Sender as TsButton).Name = 'btnCancel' then
  begin
    DeleteTempData;
    RollbakTempDelData(MAINT_NO);
    clearTempKey;    
    ReadAPPRMI_H;
    qryAPPRMI_H.Locate('MAINT_NO', MAINT_NO, []);
  end;

  ProgramControlType := ctView;
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  BtnControl;
end;

procedure TUI_APPRMI_NEW_frm.edtBF_BANKDblClick(Sender: TObject);
var
  CD_BANK_SWIFT : TCD_BANK_SWIFT_frm;
begin
  inherited;

  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_BANK_SWIFT := TCD_BANK_SWIFT_frm.Create(Self);
  try
    if CD_BANK_SWIFT.OpenDialog(0,(Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_BANK_SWIFT.Values[0];
       case (Sender as TsEdit).Tag of
          //수익자 은행
          102:
          begin
            edtBF_Nation.Text := CD_BANK_SWIFT.Field('BANK_NAT_CD').AsString;
            edtBF_BANK1.Text := CD_BANK_SWIFT.Field('BANK_NM1').AsString;
            edtBF_BANK2.Text := CD_BANK_SWIFT.Field('BANK_NM2').AsString;
            edtBF_NAME1.Text := CD_BANK_SWIFT.Field('BANK_BNCH_NM1').AsString;
            edtBF_NAME2.Text := CD_BANK_SWIFT.Field('BANK_BNCH_NM2').AsString;
            edtBF_ACCNT2.Text := CD_BANK_SWIFT.Field('ACCT_NM').AsString;
            edtBF_CURR.Text := CD_BANK_SWIFT.Field('BANK_UNIT').AsString;
          end;
       end;
    end;
  finally
    FreeAndNil( CD_BANK_SWIFT );
  end;

end;

procedure TUI_APPRMI_NEW_frm.edtOD_BANKDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CDDialog := TCD_BANK_frm.Create(Self);
  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        // 지급의뢰인은행 외화원금계좌
        101:
        begin
          edtOD_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtOD_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 중간경유 은행
        103:
        begin
          edtI1_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtI1_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 지급의뢰인 원화원금계좌
        104:
        begin
          edtEC_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtEC_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnDocNewClick(Sender: TObject);
begin
  inherited;
  // 사전송금/ 사후송금 코드가 필요함
  if isEmpty(edt_TRD_CD) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_TRD_CD,'<font color=blue><b>거래형태구분</b></font>을 입력하세요<br>해당 코드에 따라서 필수입력값이 다르기 때문에 먼저 입력하셔야합니다.');
    exit;
  end;

  changeLabels;

  Case (Sender as TsButton).Tag of
    0: subDataMode := ctInsert;
    1: subDataMode := ctModify;
  end;
  
  DocBtnControl;
  ReadOnlyControl(panDoc, false);

  if subDataMode = ctInsert then
  begin
    ClearControl(panDoc);
    // 공통항목에서 가져올 초기값
    mskDoc_CONT_DT.Text := msk_APP_DATE.Text;
    // 통화단위
    edtDoc_AMT_UNIT.Text := edt_TOTAL_AMT_UNIT.Text;
    currDoc_AMT.Value := curr_TOTAL_AMT.Value;
    edtDoc_REMIT_AMT_UNIT.Text := edt_TOTAL_AMT_UNIT.Text;
    currDoc_REMIT_AMT.Value := curr_TOTAL_AMT.Value;
    // 수출국
    edtDoc_EXPORT_NAT_CD.Text := edtBEN_NATION.Text;
    // 수출자 (공통의 수익자)
    edtDoc_EXPORT_OW_NM.Text := edtBEN_NAME1.Text;
    // 수입자 (공통의 송금의뢰인)
    edtDoc_IMPORT_OW_NM.Text := Edt_ApplicationName1.Text;
  end;
end;

procedure TUI_APPRMI_NEW_frm.newDocument(CopyDocNo: String);
begin
  //문서모드
  ProgramControlType := ctInsert;
  ReadOnlyControl(sPanel5);

  // 입력잠금 해제
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel27, False);

  //패널 초기화
  ClearControl(sPanel7);
  ClearControl(sPanel27);

  //날짜 초기화
  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD', Now);
  msk_TRN_HOPE_DT.Text := FormatDateTime('YYYYMMDD', Now);

  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc(DOCUMENT_CODE);
  edt_userno.Text := LoginData.sID;

  edt_TOTAL_AMT_UNIT.Text := 'KRW';

  try
    //거래형태 구분 초기화
    edt_TRD_CD.Text := 'P11';
    CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    sEdit3.Text := CDDialog.GetCodeText(edt_TRD_CD.Text);

    //수입송금신청용도 초기화
    edt_IMPT_REMIT_CD.Text := '2AM';
    CDDialog := TCD_4487_frm.Create(Self);
    sEdit5.Text := CDDialog.GetCodeText(edt_IMPT_REMIT_CD.Text);

    //송금방법
    edt_REMIT_CD.Text := '4';
    CDDialog := TCD_SNDCD_frm.Create(Self);
    sEdit9.Text := CDDialog.GetCodeText(edt_REMIT_CD.Text);

    //부가수수료 부담자
    edt_ADDED_FEE_TAR.Text := '14';
    CDDialog := TCD_ChargeTo_frm.Create(Self);
    sEdit11.Text := CDDialog.GetCodeText(edt_ADDED_FEE_TAR.Text);

  finally
    FreeAndNil(CDDialog);
  end;

  DMCodeContents.CODEREFRESH;
  // 대표자
  edt_AUTH_NM1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_AUTH_NM2.Text := DMCodeContents.ConfigNAME2.AsString;

  // 신청인
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    Edt_ApplicationCode.Text := DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    Edt_ApplicationName1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    Edt_ApplicationSAUPNO.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
    edtOY_TEL.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_TEL').AsString;
    Edt_ApplicationAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    Edt_ApplicationAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    Edt_ApplicationElectronicSign.Text := DMCodeContents.GEOLAECHEO.FieldByName('Jenja').AsString;
  end;

  sPageControl1.ActivePageIndex := 0;
  msk_APP_DATE.SetFocus;

end;

procedure TUI_APPRMI_NEW_frm.createTempKey;
begin
  tempKey := AnsiReplaceText( getStringFromQuery('SELECT NEWID()',0) , '{', '' );
  tempKey := AnsiReplaceText( tempKey , '}', '');
  tempKey := AnsiReplaceText( tempKey , '-', '');  
end;

procedure TUI_APPRMI_NEW_frm.DocBtnControl;
begin
  btnDocNew.Enabled := subDataMode = ctView;
  btnDocEdit.Enabled := btnDocNew.Enabled;
  btnDocDel.Enabled := btnDocNew.Enabled;
  btnDocSave.Enabled := subDataMode in [ctInsert, ctModify];
  btnDocCancel.Enabled := btnDocSave.Enabled;

  sDBGrid2.Enabled := btnDocNew.Enabled;

end;

procedure TUI_APPRMI_NEW_frm.btnDocSaveClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsButton).Name = 'btnDocSave' then
  begin
    if ValidDocData then exit;

    IF isPRE_CODE AND (sMemo1.Text = '') then
    begin
      sMemo1.Clear;
      sMemo1.Lines.Add(mskDoc_HS_CODE.Text);
    end;
    insertRFF;
  end;

  subDataMode := ctView;
  DocBtnControl;
  ReadOnlyControl(panDoc);

  ReadDocList(edtMAINT_NO.Text);
end;

procedure TUI_APPRMI_NEW_frm.ReadDocList(DocNo : String);
begin
  with qryRFF do
  begin
    Close;
    Parameters[0].Value := DocNo;
    Parameters[1].Value := tempKey;
    Open;
  end;
end;

procedure TUI_APPRMI_NEW_frm.insertRFF;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;
  try
    with SQLCreator do
    begin
      case subDataMode of
        ctInsert:
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO', tempKey);
        end;

        ctModify:
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO', qryRFFMAINT_NO.AsString);
          ADDWhere('SEQ', qryRFFSEQ.AsInteger);          
        end;
      end;
      
      SQLHeader('APPRMI_RFF');
      ADDValue('RFF_NO', edtDoc_RFF_NO.Text);
      ADDValue('HS_CD', mskDoc_HS_CODE.Text);
      ADDValue('CONT_DT', mskDoc_CONT_DT.Text);
      ADDValue('EXPORT_NAT_CD', edtDoc_EXPORT_NAT_CD.Text);
      ADDValue('IMPT_CD', edtDoc_IMPT_CD.Text);
      ADDValue('AMT', currDoc_AMT.Value);
      ADDValue('AMT_UNIT', edtDoc_AMT_UNIT.Text);
      ADDValue('REMIT_AMT', currDoc_REMIT_AMT.Value);
      ADDValue('REMIT_AMT_UNIT', edtDoc_REMIT_AMT_UNIT.Text);
      ADDValue('CIF_AMT', currDoc_CIF_AMT.Value);
      ADDValue('CIF_AMT_UNIT', edtDoc_CIF_AMT_UNIT.Text);
      ADDValue('GOODS_NM', edtDoc_GOODS_NM.Text);
      ADDValue('TOD_CD', edtDoc_TOD_CD.Text);
      ADDValue('TOD_REMARK', edtDoc_TOD_REMARK.Text);
      ADDValue('EXPORT_OW_NM', edtDoc_EXPORT_OW_NM.Text);
      ADDValue('EXPORT_OW_ADDR', edtDoc_EXPORT_OW_ADDR.Text);
      ADDValue('EXPORT_OW_NAT_CD', edtDoc_EXPORT_OW_NAT_CD.Text);
      ADDValue('IMPORT_OW_NM', edtDoc_IMPORT_OW_NM.Text);
      ADDValue('IMPORT_OW_ADDR', edtDoc_IMPORT_OW_ADDR.Text);
      ADDValue('IMPORT_OW_NAT_CD', edtDoc_IMPORT_OW_NAT_CD.Text);
      ADDValue('DETAIL_DESC1', sMemo1.Lines.Strings[0]);
      ADDValue('DETAIL_DESC2', sMemo1.Lines.Strings[1]);
      ADDValue('DETAIL_DESC3', sMemo1.Lines.Strings[2]);
      ADDValue('DETAIL_DESC4', sMemo1.Lines.Strings[3]);
      ADDValue('DETAIL_DESC5', sMemo1.Lines.Strings[4]);
      ExecSQL(CreateSQL);     
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.DeleteTempData;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlDelete;
      SQLHeader('APPRMI_RFF');
      ADDWhere('MAINT_NO', tempKey);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;

end;

procedure TUI_APPRMI_NEW_frm.SaveTempData;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlUpdate;
      SQLHeader('APPRMI_RFF');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDWhere('MAINT_NO', tempKey);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;

end;

procedure TUI_APPRMI_NEW_frm.SaveData(Sender : TObject);
var
  SQLCreator : TSQLCreate;
  TEMP_STR_LIST : TStringList;
  BankPassword : TEncryptBankPasword;
begin
  SQLCreator := TSQLCreate.Create;
  TEMP_STR_LIST := TStringList.Create;
  BankPassword := TEncryptBankPasword.Create;
  
  try
    with SQLCreator do
    begin
      case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO', edtMAINT_NO.Text);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO', edtMAINT_NO.Text);
        end;
      end;

      SQLHeader('APPRMI_H');
      ADDValue('MESSAGE1', CM_TEXT(com_func,[':']));
      ADDValue('MESSAGE2', CM_TEXT(com_type,[':']));
      ADDValue('CHK2', (Sender as TsButton).Tag);
      ADDValue('APP_DT', msk_APP_DATE.Text);
      ADDValue('REG_DT', LeftStr(msk_Datee.Text,4)+'-'+MidStr(msk_Datee.Text,5,2)+'-'+MidStr(msk_Datee.Text,7,2));
      ADDValue('USER_ID', edt_userno.Text);
      ADDValue('TRN_HOPE_DT', msk_TRN_HOPE_DT.Text);
      ADDValue('IMPT_SCHEDULE_DT', msk_IMPT_SCHEDULE_DT.Text);
      ADDValue('ETC_NO', edt_ETC_NO.Text);
      ADDValue('TRD_CD', edt_TRD_CD.Text);
      ADDValue('IMPT_REMIT_CD', edt_IMPT_REMIT_CD.Text);
      ADDValue('IMPT_TRAN_FEE_CD', edt_IMPT_TRAN_FEE_CD.Text);

      //송금내역
      IF Trim(edt_REMIT_DESC1.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC1.Text);
      IF Trim(edt_REMIT_DESC2.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC2.Text);
      IF Trim(edt_REMIT_DESC3.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC3.Text);
      IF Trim(edt_REMIT_DESC4.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC4.Text);
      ADDValue('REMIT_DESC', TEMP_STR_LIST.Text);

      ADDValue('REMIT_CD', edt_REMIT_CD.Text);
      ADDValue('ADDED_FEE_TAR', edt_ADDED_FEE_TAR.Text);
      ADDValue('TOTAL_AMT', curr_TOTAL_AMT.Value);
      ADDValue('TOTAL_AMT_UNIT', edt_TOTAL_AMT_UNIT.Text);
      ADDValue('FORE_AMT', curr_FORE_AMT.Value);
      ADDValue('FORE_AMT_UNIT', edt_FORE_AMT_UNIT.Text);
      BankPassword.Key := RightStr(msk_APP_DATE.Text,4);
      ADDValue('ONLY_PWD', BankPassword.EncryptBankPassword(edt_ONLY_PWD.Text));
      ADDValue('AUTH_NM1', edt_AUTH_NM1.Text);
      ADDValue('AUTH_NM2', edt_AUTH_NM2.Text);
      ADDValue('AUTH_NM3', Edt_ApplicationElectronicSign.Text);

      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
    TEMP_STR_LIST.Free;
    BankPassword.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_H;
begin
  with qryAPPRMI_H do
  begin
    Close;
    SQL.Text := FSQL;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;
    IF Trim(edt_SearchNo.Text) <> '' Then
    begin
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+edt_SearchNo.text+'%'));
    end;
    Open;
  end;
end;

procedure TUI_APPRMI_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryAPPRMI_H.SQL.Text;

  requireDataFontStyle := TFont.Create;
  normalDataFontStyle := TFont.Create;
  noDataFontStyle := TFont.Create;

  requireDataFontStyle.Style := requireLabel;
  requireDataFontStyle.Color := clWindowText;
  requireDataFontStyle.Name := '맑은 고딕';
  requireDataFontStyle.Size := 9;

  normalDataFontStyle.Style := normalLabel;
  normalDataFontStyle.Color := clWindowText;
  normalDataFontStyle.Name := '맑은 고딕';
  normalDataFontStyle.Size := 9;

  noDataFontStyle.Style := normalLabel;
  noDataFontStyle.Color := clSilver;
  noDataFontStyle.Name := '맑은 고딕';
  noDataFontStyle.Size := 9;

end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_H_DATA;
var
  TEMP_STR_LIST : TStringList;
  BankPassword : TEncryptBankPasword;
begin
  TEMP_STR_LIST := TStringList.Create;
  BankPassword := TEncryptBankPasword.Create;
  try
    with qryAPPRMI_H do
    begin
      edtMAINT_NO.Text := qryAPPRMI_HMAINT_NO.AsString;
      if qryAPPRMI_HREG_DT.AsString <> '' then
        msk_Datee.Text := FormatDateTime('YYYYMMDD', qryAPPRMI_HREG_DT.AsDateTime)
      else
        msk_Datee.Clear;
      edt_userno.Text := qryAPPRMI_HUSER_ID.AsString;
      CM_INDEX(com_func, [':'], 0, qryAPPRMI_HMESSAGE1.AsString, 1);
      CM_INDEX(com_type, [':'], 0, qryAPPRMI_HMESSAGE2.AsString);
      msk_APP_DATE.Text := qryAPPRMI_HAPP_DT.AsString;
      msk_IMPT_SCHEDULE_DT.Text := qryAPPRMI_HIMPT_SCHEDULE_DT.AsString;
      msk_TRN_HOPE_DT.Text := qryAPPRMI_HTRN_HOPE_DT.AsString;
      edt_ETC_NO.Text := qryAPPRMI_HETC_NO.AsString;
      edt_TOTAL_AMT_UNIT.Text := qryAPPRMI_HTOTAL_AMT_UNIT.AsString;
      curr_TOTAL_AMT.Value := qryAPPRMI_HTOTAL_AMT.AsCurrency;
      edt_FORE_AMT_UNIT.Text := qryAPPRMI_HFORE_AMT_UNIT.AsString;
      curr_FORE_AMT.Value := qryAPPRMI_HFORE_AMT.AsCurrency;
      edt_TRD_CD.Text := qryAPPRMI_HTRD_CD.AsString;
      sEdit3.Text := qryAPPRMI_HTRD_NM.AsString;
      edt_IMPT_REMIT_CD.Text := qryAPPRMI_HIMPT_REMIT_CD.AsString;
      sEdit5.Text := qryAPPRMI_HIMPT_REMIT_NM.AsString;
      edt_IMPT_TRAN_FEE_CD.Text := qryAPPRMI_HIMPT_TRAN_FEE_CD.AsString;
      sEdit6.Text := qryAPPRMI_HIMPT_TRAN_FEE_NM.AsString;
      edt_IMPT_TRAN_FEE_CD.Enabled := edt_IMPT_REMIT_CD.Text = '2AJ';
      sEdit6.Enabled := edt_IMPT_TRAN_FEE_CD.Enabled;
      edt_REMIT_CD.Text := qryAPPRMI_HREMIT_CD.AsString;
      sEdit9.Text := qryAPPRMI_HREMIT_NM.AsString;
      TEMP_STR_LIST.Text := qryAPPRMI_HREMIT_DESC.AsString;
      if TEMP_STR_LIST.Count > 0 Then
        edt_REMIT_DESC1.Text := TEMP_STR_LIST.Strings[0];
      if TEMP_STR_LIST.Count > 1 Then
        edt_REMIT_DESC2.Text := TEMP_STR_LIST.Strings[1];
      if TEMP_STR_LIST.Count > 2 Then
        edt_REMIT_DESC3.Text := TEMP_STR_LIST.Strings[2];
      if TEMP_STR_LIST.Count > 3 Then
        edt_REMIT_DESC4.Text := TEMP_STR_LIST.Strings[3];
      edt_ADDED_FEE_TAR.Text := qryAPPRMI_HADDED_FEE_TAR.AsString;
      sEdit11.Text := qryAPPRMI_HADDED_FEE_TAR_NM.AsString;
      edt_AUTH_NM1.Text := qryAPPRMI_HAUTH_NM1.AsString;
      edt_AUTH_NM2.Text := qryAPPRMI_HAUTH_NM2.AsString;
      Edt_ApplicationElectronicSign.Text := qryAPPRMI_HAUTH_NM3.AsString;

      if trim(qryAPPRMI_HAPP_DT.AsString) <> '' then
      begin
        BankPassword.Key := RightStr(qryAPPRMI_HAPP_DT.AsString,4);
        edt_ONLY_PWD.Text := BankPassword.DescryptBankPassword(qryAPPRMI_HONLY_PWD.AsString);
      end;

    end;
  finally
    TEMP_STR_LIST.Free;
    BankPassword.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.qryAPPRMI_HAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  ReadAPPRMI_H_DATA;
  ReadAPPRMI_NAD(qryAPPRMI_HMAINT_NO.AsString);
  ReadAPPRMI_FII(qryAPPRMI_HMAINT_NO.AsString);
  ReadDocList(qryAPPRMI_HMAINT_NO.AsString);
  changeLabels;
end;

procedure TUI_APPRMI_NEW_frm.SaveDataNAD;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  //전체 삭제 후 다시 INSERT
  try
    with SQLCreator do
    begin
      //전체 삭제 후
      DMLType := dmlDelete;
      SQLHeader('APPRMI_NAD');
      ADDWhere('MAINT_NO', edtMAINT_NO.Text);
      ExecSQL(CreateSQL);

      //다시 INSERT
      // 송금 의뢰인
      DMLType := dmlInsert;
      SQLHeader('APPRMI_NAD');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDValue('CODE', Edt_ApplicationCode.Text);
      ADDValue('TAG', 'OY');
      ADDValue('BUS_NO', Edt_ApplicationSAUPNO.Text);
      ADDValue('BUS_NM1', Edt_ApplicationName1.Text);
      ADDValue('BUS_ADDR1', Edt_ApplicationAddr1.Text);
      ADDValue('BUS_ADDR2', Edt_ApplicationAddr2.Text);
      ADDValue('TEL_NO', edtOY_TEL.Text);
      ExecSQL(CreateSQL);

      // 수익자
      DMLType := dmlInsert;
      SQLHeader('APPRMI_NAD');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDValue('CODE', edtBEN_CODE.Text);      
      ADDValue('TAG', 'BE');
      ADDValue('BUS_NO', '');      
      ADDValue('BUS_NM1', edtBEN_NAME1.Text);
      ADDValue('BUS_NM2', edtBEN_NAME2.Text);
      ADDValue('BUS_NM3', edtBEN_NAME3.Text);
      ADDValue('BUS_ADDR1', edtBEN_STR1.Text);
      ADDValue('BUS_ADDR2', edtBEN_STR2.Text);
      ADDValue('BUS_ADDR3', edtBEN_STR3.Text);
      ADDValue('NAT_CD', edtBEN_NATION.Text);
      ADDValue('TEL_NO', edtBEN_TELE.Text);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadAPPRMI_H;
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_NAD(MAINT_NO : String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAINT_NO, TAG, CODE, BUS_NO, BUS_NM1, BUS_NM2, BUS_NM3, BUS_ADDR1, BUS_ADDR2, BUS_ADDR3, NAT_CD, TEL_NO FROM APPRMI_NAD WHERE MAINT_NO = '+QuotedStr(MAINT_NO);
      Open;

      while not eof do
      begin
        // 송금의뢰인
        if FieldByName('TAG').AsString = 'OY' then
        begin
          Edt_ApplicationCode.Text := FieldByName('CODE').AsString;
          Edt_ApplicationName1.Text := FieldByName('BUS_NM1').AsString;
          Edt_ApplicationSAUPNO.Text := FieldByName('BUS_NO').AsString;
          Edt_ApplicationAddr1.Text := FieldByName('BUS_ADDR1').AsString;
          Edt_ApplicationAddr2.Text := FieldByName('BUS_ADDR2').AsString;
          edtOY_TEL.Text := FieldByName('NAT_CD').AsString;
        end;

        // 송금의뢰인
        if FieldByName('TAG').AsString = 'BE' then
        begin
          edtBEN_CODE.Text := FieldByName('CODE').AsString;
          edtBEN_NAME1.Text := FieldByName('BUS_NM1').AsString;
          edtBEN_NAME2.Text := FieldByName('BUS_NM2').AsString;
          edtBEN_NAME3.Text := FieldByName('BUS_NM3').AsString;
          edtBEN_STR1.Text := FieldByName('BUS_ADDR1').AsString;
          edtBEN_STR2.Text := FieldByName('BUS_ADDR2').AsString;
          edtBEN_STR3.Text := FieldByName('BUS_ADDR3').AsString;
          edtBEN_TELE.Text := FieldByName('TEL_NO').AsString;
          edtBEN_NATION.Text := FieldByName('NAT_CD').AsString;
        end;

        Next;
      end;

      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPRMI_NEW_frm.SaveDataFII;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  //전체 삭제 후 다시 INSERT
  try
    with SQLCreator do
    begin
      //전체 삭제 후
      DMLType := dmlDelete;
      SQLHeader('APPRMI_FII');
      ADDWhere('MAINT_NO', edtMAINT_NO.Text);
      ExecSQL(CreateSQL);

      //다시 INSERT
      // 송금 의뢰은행(외화 원금계좌)
      if Trim(edtOD_BANK.Text) <> '' then
      begin
        DMLType := dmlInsert;
        SQLHeader('APPRMI_FII');
        ADDValue('MAINT_NO', edtMAINT_NO.Text);
        ADDValue('TAG', 'OR');
        ADDValue('ORD', 1);
        ADDValue('ACCNT_NO', edtOD_ACCNT1.Text);
        ADDValue('ACCNT_NM', edtOD_ACCNT2.Text);
        ADDValue('ACCNT_UNIT', edtOD_CURR.Text);
        ADDValue('BNK_CD', edtOD_BANK.Text);
        ADDValue('BNK_NM1', edtOD_BANK1.Text);
        ADDValue('BNK_NM2', edtOD_BANK2.Text);
        ADDValue('BNK_NM3', edtOD_NAME1.Text);
        ADDValue('BNK_NM4', edtOD_NAME2.Text);
        ADDValue('BNK_NAT_CD', edtOD_Nation.Text);
        ExecSQL(CreateSQL);
      end;

      // 수익자은행
      if Trim(edtBF_BANK.Text) <> '' then
      begin
        DMLType := dmlInsert;
        SQLHeader('APPRMI_FII');
        ADDValue('MAINT_NO', edtMAINT_NO.Text);
        ADDValue('TAG', 'BF');
        ADDValue('ORD', 2);
        ADDValue('ACCNT_NO', edtBF_ACCNT1.Text);
        ADDValue('ACCNT_NM', edtBF_ACCNT2.Text);
        ADDValue('ACCNT_UNIT', edtBF_CURR.Text);
        ADDValue('BNK_CD', edtBF_BANK.Text);
        ADDValue('BNK_NM1', edtBF_BANK1.Text);
        ADDValue('BNK_NM2', edtBF_BANK2.Text);
        ADDValue('BNK_NM3', edtBF_NAME1.Text);
        ADDValue('BNK_NM4', edtBF_NAME2.Text);
        ADDValue('BNK_NAT_CD', edtBF_Nation.Text);
        ExecSQL(CreateSQL);
      end;

      // 중간경유 은행
      if Trim(edtI1_BANK.Text) <> '' then
      begin
        DMLType := dmlInsert;
        SQLHeader('APPRMI_FII');
        ADDValue('MAINT_NO', edtMAINT_NO.Text);
        ADDValue('TAG', 'I1');
        ADDValue('ORD', 3);
        ADDValue('ACCNT_NO', edtI1_ACCNT1.Text);
        ADDValue('ACCNT_NM', edtI1_ACCNT2.Text);
        ADDValue('ACCNT_UNIT', edtI1_CURR.Text);
        ADDValue('BNK_CD', edtI1_BANK.Text);
        ADDValue('BNK_NM1', edtI1_BANK1.Text);
        ADDValue('BNK_NM2', edtI1_BANK2.Text);
        ADDValue('BNK_NM3', edtI1_NAME1.Text);
        ADDValue('BNK_NM4', edtI1_NAME2.Text);
        ADDValue('BNK_NAT_CD', edtI1_Nation.Text);
        ExecSQL(CreateSQL);
      end;

      // 송금의뢰은행(원화원금계좌)
      if Trim(edtEC_BANK.Text) <> '' then
      begin
        DMLType := dmlInsert;
        SQLHeader('APPRMI_FII');
        ADDValue('MAINT_NO', edtMAINT_NO.Text);
        ADDValue('TAG', 'EC');
        ADDValue('ORD', 4);
        ADDValue('ACCNT_NO', edtEC_ACCNT1.Text);
        ADDValue('ACCNT_NM', edtEC_ACCNT2.Text);
        ADDValue('ACCNT_UNIT', 'KRW');
        ADDValue('BNK_CD', edtEC_BANK.Text);
        ADDValue('BNK_NM1', edtEC_BANK1.Text);
        ADDValue('BNK_NM2', edtEC_BANK2.Text);
        ADDValue('BNK_NM3', edtEC_NAME1.Text);
        ADDValue('BNK_NM4', edtEC_NAME2.Text);
        ADDValue('BNK_NAT_CD', edtEC_Nation.Text);
        ExecSQL(CreateSQL);
      end;

    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  createTempKey;
  editDocument;
  ReadDocList(edtMAINT_NO.Text);
end;

procedure TUI_APPRMI_NEW_frm.editDocument;
begin
  //문서모드
  ProgramControlType := ctModify;
  BtnControl;
  ReadOnlyControl(sPanel5);

  // 입력잠금 해제
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel27, False);
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_FII(MAINT_NO: String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAINT_NO, TAG, ACCNT_NO, ACCNT_NM, ACCNT_UNIT, BNK_CD, BNK_NM1, BNK_NM2, BNK_NM3, BNK_NM4, BNK_NAT_CD FROM APPRMI_FII WHERE MAINT_NO = '+QuotedStr(MAINT_NO);
      Open;

      while not eof do
      begin
        // 송금의뢰인은행(외화)
        if FieldByName('TAG').AsString = 'OR' then
        begin
          edtOD_ACCNT1.Text := FieldByName('ACCNT_NO').AsString;
          edtOD_ACCNT2.Text := FieldByName('ACCNT_NM').AsString;
          edtOD_CURR.Text := FieldByName('ACCNT_UNIT').AsString;
          edtOD_BANK.Text := FieldByName('BNK_CD').AsString;
          edtOD_BANK1.Text := FieldByName('BNK_NM1').AsString;
          edtOD_BANK2.Text := FieldByName('BNK_NM2').AsString;
          edtOD_NAME1.Text := FieldByName('BNK_NM3').AsString;
          edtOD_NAME2.Text := FieldByName('BNK_NM4').AsString;
          edtOD_Nation.Text := FieldByName('BNK_NAT_CD').AsString;
        end;

        // 수익자은행
        if FieldByName('TAG').AsString = 'BF' then
        begin
          edtBF_ACCNT1.Text := FieldByName('ACCNT_NO').AsString;
          edtBF_ACCNT2.Text := FieldByName('ACCNT_NM').AsString;
          edtBF_CURR.Text := FieldByName('ACCNT_UNIT').AsString;
          edtBF_BANK.Text := FieldByName('BNK_CD').AsString;
          edtBF_BANK1.Text := FieldByName('BNK_NM1').AsString;
          edtBF_BANK2.Text := FieldByName('BNK_NM2').AsString;
          edtBF_NAME1.Text := FieldByName('BNK_NM3').AsString;
          edtBF_NAME2.Text := FieldByName('BNK_NM4').AsString;
          edtBF_Nation.Text := FieldByName('BNK_NAT_CD').AsString;
        end;

        // 중간경유은행
        if FieldByName('TAG').AsString = 'I1' then
        begin
          edtI1_ACCNT1.Text := FieldByName('ACCNT_NO').AsString;
          edtI1_ACCNT2.Text := FieldByName('ACCNT_NM').AsString;
          edtI1_CURR.Text := FieldByName('ACCNT_UNIT').AsString;
          edtI1_BANK.Text := FieldByName('BNK_CD').AsString;
          edtI1_BANK1.Text := FieldByName('BNK_NM1').AsString;
          edtI1_BANK2.Text := FieldByName('BNK_NM2').AsString;
          edtI1_NAME1.Text := FieldByName('BNK_NM3').AsString;
          edtI1_NAME2.Text := FieldByName('BNK_NM4').AsString;
          edtI1_Nation.Text := FieldByName('BNK_NAT_CD').AsString;
        end;

        // 송금의뢰인은행(원화)
        if FieldByName('TAG').AsString = 'EC' then
        begin
          edtEC_ACCNT1.Text := FieldByName('ACCNT_NO').AsString;
          edtEC_ACCNT2.Text := FieldByName('ACCNT_NM').AsString;
          edtEC_BANK.Text := FieldByName('BNK_CD').AsString;
          edtEC_BANK1.Text := FieldByName('BNK_NM1').AsString;
          edtEC_BANK2.Text := FieldByName('BNK_NM2').AsString;
          edtEC_NAME1.Text := FieldByName('BNK_NM3').AsString;
          edtEC_NAME2.Text := FieldByName('BNK_NM4').AsString;
          edtEC_Nation.Text := FieldByName('BNK_NAT_CD').AsString;
        end;

        Next;
      end;

      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnDelClick(Sender: TObject);
var
  SQLCreator : TSQLCreate;
  MAINT_NO : String;
  CurrIdx : Integer;
begin
  inherited;

  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  SQLCreator := TSQLCreate.Create;
  MAINT_NO := qryAPPRMI_HMAINT_NO.AsString;

  with SQLCreator do
  begin
    CurrIdx := qryAPPRMI_H.RecNo;
    DMMssql.BeginTrans;
    try
      DMLType := dmlDelete;
      SQLHeader('APPRMI_H');
      ADDWhere('MAINT_NO', MAINT_NO);
      ExecSQL(CreateSQL);

      SQLHeader('APPRMI_RFF');
      ADDWhere('MAINT_NO', MAINT_NO);
      ExecSQL(CreateSQL);

      SQLHeader('APPRMI_FII');
      ADDWhere('MAINT_NO', MAINT_NO);
      ExecSQL(CreateSQL);

      SQLHeader('APPRMI_NAD');
      ADDWhere('MAINT_NO', MAINT_NO);
      ExecSQL(CreateSQL);
      DMMssql.CommitTrans;

      qryAPPRMI_H.Close;
      qryAPPRMI_H.Open;
      if CurrIdx > 1 Then
        qryAPPRMI_H.MoveBy(CurrIdx-1);

    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
        DMMssql.RollbackTrans;
      end;
    end;
  end;
end;

procedure TUI_APPRMI_NEW_frm.qryAPPRMI_HAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qryAPPRMI_HAfterScroll(DataSet);
  end;
end;

procedure TUI_APPRMI_NEW_frm.readRFFData;
var
  TEMP_STR_LIST : TStringList;
begin
  edtDoc_RFF_NO.Text := qryRFFRFF_NO.AsString;
  mskDoc_CONT_DT.Text := qryRFFCONT_DT.AsString;
  edtDoc_EXPORT_NAT_CD.Text := qryRFFEXPORT_NAT_CD.AsString;
  mskDoc_HS_CODE.Text := qryRFFHS_CD.AsString;
  edtDoc_IMPT_CD.Text := qryRFFIMPT_CD.AsString;
  sEdit8.Text := qryRFFIMPT_NM.AsString;
  edtDoc_AMT_UNIT.Text := qryRFFAMT_UNIT.AsString;
  currDoc_AMT.Value := qryRFFAMT.Value;
  edtDoc_REMIT_AMT_UNIT.Text := qryRFFREMIT_AMT_UNIT.AsString;
  currDoc_REMIT_AMT.Value := qryRFFREMIT_AMT.Value;
  edtDoc_CIF_AMT_UNIT.Text := qryRFFCIF_AMT_UNIT.Text;
  currDoc_CIF_AMT.Value := qryRFFCIF_AMT.Value;
  edtDoc_GOODS_NM.Text := qryRFFGOODS_NM.AsString;
  edtDoc_TOD_CD.Text := qryRFFTOD_CD.AsString;
  sEdit12.Text := qryRFFTOD_NM.AsString;
  edtDoc_TOD_REMARK.Text := qryRFFTOD_REMARK.AsString;
  edtDoc_EXPORT_OW_NM.Text := qryRFFEXPORT_OW_NM.AsString;
  edtDoc_EXPORT_OW_ADDR.Text := qryRFFEXPORT_OW_ADDR.AsString;
  edtDoc_EXPORT_OW_NAT_CD.Text := qryRFFEXPORT_OW_NAT_CD.AsString;
  edtDoc_IMPORT_OW_NM.Text := qryRFFIMPORT_OW_NM.AsString;
  edtDoc_IMPORT_OW_ADDR.Text := qryRFFIMPORT_OW_ADDR.AsString;
  edtDoc_IMPORT_OW_NAT_CD.Text := qryRFFIMPORT_OW_NAT_CD.AsString;
  if Trim(qryRFFDETAIL_DESC1.AsString) <> ''Then sMemo1.Lines.Add(qryRFFDETAIL_DESC1.AsString);
  if Trim(qryRFFDETAIL_DESC2.AsString) <> ''Then sMemo1.Lines.Add(qryRFFDETAIL_DESC2.AsString);
  if Trim(qryRFFDETAIL_DESC3.AsString) <> ''Then sMemo1.Lines.Add(qryRFFDETAIL_DESC3.AsString);
  if Trim(qryRFFDETAIL_DESC4.AsString) <> ''Then sMemo1.Lines.Add(qryRFFDETAIL_DESC4.AsString);
  if Trim(qryRFFDETAIL_DESC5.AsString) <> ''Then sMemo1.Lines.Add(qryRFFDETAIL_DESC5.AsString);
end;

procedure TUI_APPRMI_NEW_frm.qryRFFAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ClearControl(panDoc);
  readRFFData;
end;

procedure TUI_APPRMI_NEW_frm.qryRFFAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
  begin
    qryRFFAfterScroll(DataSet);
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnDocDelClick(Sender: TObject);
var
  SQLCreator : TSQLCreate;
  currIdx : integer;
begin
  inherited;
  SQLCreator := TSQLCreate.Create;

  // 임시 문서번호이면 그냥 삭제, 원 문서번호이면 임시 필드 토글
  try
    with SQLCreator do
    begin
      currIdx := qryRFF.RecNo;
      if (Length(qryRFFMAINT_NO.AsString) = 32) then
      begin
        DMLType := dmlDelete;
        SQLHeader('APPRMI_RFF');
        ADDWhere('MAINT_NO', qryRFFMAINT_NO.AsString);
        ADDWhere('SEQ', qryRFFSEQ.AsInteger);
      end
      else
      begin
        DMLType := dmlUpdate;
        SQLHeader('APPRMI_RFF');
        ADDValue('TMP_DEL', 1);
        ADDWhere('MAINT_NO', qryRFFMAINT_NO.AsString);
        ADDWhere('SEQ', qryRFFSEQ.AsInteger);
      end;
      ExecSQL(CreateSQL);

      qryRFF.Close;
      qryRFF.Open;
      if CurrIdx > 1 Then
        qryRFF.MoveBy(CurrIdx-1);
    end;
  finally
    SQLCreator.Free;
  end;


end;

procedure TUI_APPRMI_NEW_frm.RollbakTempDelData(MAINT_NO : String);
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlUpdate;
      SQLHeader('APPRMI_RFF');
      ADDValue('TMP_DEL', 0);
      ADDWhere('MAINT_NO', MAINT_NO);
      ADDWhere('TMP_DEL', 1);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.CommitTempDelData(MAINT_NO: String);
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlDelete;
      SQLHeader('APPRMI_RFF');
      ADDWhere('MAINT_NO', MAINT_NO);
      ADDWhere('TMP_DEL', 1);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  inherited;
  IF AnsiMatchText( qryAPPRMI_HCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryAPPRMI_HMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);

//  APPRMI(qryAPPRMI_HMAINT_NO.AsString, 'NET12');
end;

procedure TUI_APPRMI_NEW_frm.clearTempKey;
begin
  tempKey := '';
end;

procedure TUI_APPRMI_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then
    DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
    DocumentSend_frm.BringToFront;
end;

procedure TUI_APPRMI_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;  
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryAPPRMI_HMAINT_NO.AsString;
      RecvFlat := APPRMI(RecvDoc, RecvCode);
      ReadyDateTime := Now;

      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := DOCUMENT_CODE;
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryAPPRMI_HCHK3.AsString, ' ', '', [rfReplaceAll]) = '0' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := DOCUMENT_CODE+'_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qryAPPRMI_H.Close;
        qryAPPRMI_H.Open;

        qryAPPRMI_H.Locate('MAINT_NO',RecvDoc,[]);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

function TUI_APPRMI_NEW_frm.ValidData: Boolean;
begin
  result := false;

  // 신청일자 누락
  if isEmpty(msk_APP_DATE) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(msk_APP_DATE,'<font color=blue><b>신청일자</b></font>를 입력하세요');
    Result := True;
    exit;
  end
  else if (msk_APP_DATE.Text <> FormatDateTime('YYYYMMDD', Now)) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(msk_APP_DATE,'<font color=red><b>신청일자</b></font>는 당일만 입력 가능합니다');
    Result := True;
    exit;
  end;

  // 이체희망일자
  if isEmpty(msk_TRN_HOPE_DT) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(msk_TRN_HOPE_DT,'<font color=blue><b>이체희망일자</b></font>를 입력하세요');
    Result := True;
    exit;
  end;

  if isEmpty(edt_TRD_CD) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_TRD_CD,'<font color=blue><b>거래형태구분</b></font>을 입력하세요');
    Result := True;
    exit;
  end;

  // 수입예정일자
  // 사전송금(11, 12, 31, 33, 35)로 끝나는 코드일때 필수값
  if isEmpty(msk_IMPT_SCHEDULE_DT) and isPRE_CODE then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(msk_IMPT_SCHEDULE_DT,'<font color=blue><b>수입예정일</b></font>을 입력하세요<br>사전송금일시 필수값 입니다.');
    Result := True;
    exit;
  end;

  // 수입송금신청용도
  if isEmpty(edt_IMPT_REMIT_CD) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_IMPT_REMIT_CD,'<font color=blue><b>수입송금신청용도</b></font>을 입력하세요');
    Result := True;
    exit;
  end
  else if (UpperCase(edt_IMPT_REMIT_CD.Text) = '2AJ') AND isEmpty(edt_IMPT_TRAN_FEE_CD) then
  begin
    // 수입송금신청용도가 2AJ일때 수수료 유형 필수입력
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_IMPT_TRAN_FEE_CD,'<font color=blue><b>수수료유형</b></font>을 입력하세요<br>수입송금신청용도가 <b>"2AJ"</b>일 경우 필수 입력입니다');
    Result := True;
    exit;
  end
  else if (UpperCase(edt_IMPT_REMIT_CD.Text) = '2AJ') AND ((edt_IMPT_TRAN_FEE_CD.Text <> '2AM') AND (edt_IMPT_TRAN_FEE_CD.Text <> '2AN')) then
  begin
    // 수입송금신청용도가 2AJ일때 수수료 유형 2AN 2AM 만 사용가능
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_IMPT_TRAN_FEE_CD,'<font color=blue><b>수수료유형</b></font>은<br>2AM : 입금지연이자<br>2AN : 인수지연이자<br>만 가능합니다');
    Result := True;
    exit;
  end;

  // 총 송금액 (코드, 값)
  if isEmpty(curr_TOTAL_AMT) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(curr_TOTAL_AMT,'<font color=blue><b>총 송금액</b></font>을 입력하세요');
    Result := True;
    exit;
  end;
  if isEmpty(edt_TOTAL_AMT_UNIT) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_TOTAL_AMT_UNIT,'<font color=blue><b>총 송금액 통화코드</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  
  // 외화계좌 송금액 (송금의뢰은행 - 원화/외화 둘다 입력시 필수입력)
  if (not isEmpty(edtOD_BANK)) and (not isEmpty(edtEC_BANK)) then
  begin
    if isEmpty(edt_FORE_AMT_UNIT) then
    begin
      sPageControl1.ActivePageIndex := 0;
      showHintControl(edt_FORE_AMT_UNIT,'<font color=blue><b>외화계좌 송금액 통화코드</b></font>를 입력하세요<br>송금의뢰은행 - 원화/외화 입력시 필수입니다');
      Result := True;
      exit;
    end;

    if isEmpty(curr_FORE_AMT) then
    begin
      sPageControl1.ActivePageIndex := 0;
      showHintControl(curr_FORE_AMT,'<font color=blue><b>외화계좌 송금액</b></font>을 입력하세요');
      Result := True;
      exit;
    end;

  end;

  // 부가수수료부담자
  if isEmpty(edt_ADDED_FEE_TAR) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_ADDED_FEE_TAR,'<font color=blue><b>부가수수료 부담자</b></font>를 입력하세요');
    Result := True;
    exit;
  end;

  // 지급지시 비밀번호
  if isEmpty(edt_ONLY_PWD) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_ONLY_PWD,'<font color=blue><b>지급지시 비밀번호</b></font>를 입력하세요');
    Result := True;
    exit;
  end;

  // 송금의뢰인 데이터 확인
  // 상호
  if isEmpty(Edt_ApplicationName1) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(Edt_ApplicationName1,'<font color=blue><b>송금의뢰인 상호</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  // 사업자 등록번호
  if isEmpty(Edt_ApplicationSAUPNO) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(Edt_ApplicationSAUPNO,'<font color=blue><b>사업자등록번호</b></font>를 입력하세요');
    Result := True;
    exit;  
  end;
  // 주소
  if isEmpty(Edt_ApplicationAddr1) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(Edt_ApplicationAddr1,'<font color=blue><b>주소</b></font>를 입력하세요');
    Result := True;
    exit;  
  end;

  // 수익자 데이터 확인
  if isEmpty(edtBEN_NAME1) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edtBEN_NAME1,'<font color=blue><b>수익자 상호</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  // 국가코드
  if isEmpty(edtBEN_NATION) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edtBEN_NATION,'<font color=blue><b>국가코드</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  // 주소
  if isEmpty(edtBEN_STR1) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edtBEN_STR1,'<font color=blue><b>주소</b></font>를 입력하세요');
    Result := True;
    exit;  
  end;

  // 명의인 데이터 확인
  if isEmpty(edt_AUTH_NM1) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_AUTH_NM1,'<font color=blue><b>명의인 - 상호</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  if isEmpty(edt_AUTH_NM2) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(edt_AUTH_NM2,'<font color=blue><b>명의인 - 대표자</b></font>를 입력하세요');
    Result := True;
    exit;
  end;
  if isEmpty(Edt_ApplicationElectronicSign) then
  begin
    sPageControl1.ActivePageIndex := 0;
    showHintControl(Edt_ApplicationElectronicSign,'<font color=blue><b>전자서명</b></font>을 입력하세요');
    Result := True;
    exit;
  end;

end;

procedure TUI_APPRMI_NEW_frm.showHintControl(Control: TControl;
  msg: String; showDelay: integer);
var
  R : TRect;
  P : TPoint;
begin
  IF msg = '' Then Exit;

  R := Control.BoundsRect;
  R.TopLeft := Control.Parent.ClientToScreen(R.TopLeft);
  P.X := R.Left-7;
  P.Y := R.Top+18;
  sAlphaHints1.HTMLMode := PosMulti(['<b>','<i>','<u>','</br>','<br>','<font'], LowerCase(msg));
  sAlphaHints1.ShowHint(P, msg ,showDelay);
end;

function TUI_APPRMI_NEW_frm.isPRE_CODE: Boolean;
begin
  Result := AnsiIndexStr(RightStr(edt_TRD_CD.Text,2), PRE_CODE) >= 0;
end;

function TUI_APPRMI_NEW_frm.isAFTER_CODE: Boolean;
begin
  Result := AnsiIndexStr(RightStr(edt_TRD_CD.Text,2), AFTER_CODE) >= 0;
end;

function TUI_APPRMI_NEW_frm.isETC_CODE: Boolean;
begin
  Result := edt_TRD_CD.Text = ETC_CODE;
end;

function TUI_APPRMI_NEW_frm.ValidDocData: Boolean;
var
  i : integer;
begin
  result := false;

  for i := 0 to panDoc.ControlCount-1 do
  begin
    if panDoc.Controls[i].Tag > -1 Then
    begin
      if panDoc.Controls[i] is TsEdit Then
      begin
        if ((panDoc.Controls[i] as TsEdit).BoundLabel.Font.Style = requireLabel) AND (Trim((panDoc.Controls[i] as TsEdit).Text) = '' ) then
        begin
          sPageControl1.ActivePageIndex := 2;
          showHintControl(panDoc.Controls[i],'<font color=blue><b>'+(panDoc.Controls[i] as TsEdit).BoundLabel.Caption+'</b></font>를 입력하세요');
          Result := True;
          exit;
        end;
      end
      else
      if panDoc.Controls[i] is TsMaskEdit Then
      begin
        if ((panDoc.Controls[i] as TsMaskEdit).BoundLabel.Font.Style = requireLabel) AND (Trim((panDoc.Controls[i] as TsMaskEdit).Text) = '' ) then
        begin
          sPageControl1.ActivePageIndex := 2;
          showHintControl(panDoc.Controls[i],'<font color=blue><b>'+(panDoc.Controls[i] as TsMaskEdit).BoundLabel.Caption+'</b></font>를 입력하세요');
          Result := True;
          exit;
        end;
      end;
    end;
  end;
end;

procedure TUI_APPRMI_NEW_frm.changeLabels;
begin
  if isPRE_CODE then
  begin
    edtDoc_RFF_NO.BoundLabel.Caption := '계약서번호';
    edtDoc_AMT_UNIT.BoundLabel.Caption := '계약금액';
    edtDoc_CIF_AMT_UNIT.BoundLabel.Font := noDataFontStyle;
    mskDoc_HS_CODE.BoundLabel.Font := requireDataFontStyle;
    edtDoc_GOODS_NM.BoundLabel.Font := requireDataFontStyle;
    edtDoc_IMPT_CD.BoundLabel.Font := noDataFontStyle;
    mskDoc_CONT_DT.BoundLabel.Caption := '계약일';
    mskDoc_CONT_DT.BoundLabel.Font := requireDataFontStyle;
    edtDoc_TOD_CD.BoundLabel.Font := requireDataFontStyle;
    edtDoc_EXPORT_NAT_CD.BoundLabel.Font := requireDataFontStyle;
    edtDoc_EXPORT_OW_NM.BoundLabel.Font := requireDataFontStyle;
    edtDoc_IMPORT_OW_NM.BoundLabel.Font := requireDataFontStyle;
  end
  else if isAFTER_CODE then
  begin
    edtDoc_RFF_NO.BoundLabel.Caption := '수입신고필증번호';
    edtDoc_AMT_UNIT.BoundLabel.Caption := '결제금액(수입신고필증상)';
    edtDoc_CIF_AMT_UNIT.BoundLabel.Font := requireDataFontStyle;
    mskDoc_HS_CODE.BoundLabel.Font := requireDataFontStyle;
    edtDoc_GOODS_NM.BoundLabel.Font := requireDataFontStyle;
    edtDoc_IMPT_CD.BoundLabel.Font := requireDataFontStyle;
    mskDoc_CONT_DT.BoundLabel.Caption := '수입신고수리일';
    mskDoc_CONT_DT.BoundLabel.Font := requireDataFontStyle;
    edtDoc_TOD_CD.BoundLabel.Font := requireDataFontStyle;
    edtDoc_EXPORT_NAT_CD.BoundLabel.Font := requireDataFontStyle;
    edtDoc_EXPORT_OW_NM.BoundLabel.Font := requireDataFontStyle;
    edtDoc_IMPORT_OW_NM.BoundLabel.Font := requireDataFontStyle;
  end
  else if isETC_CODE then
  begin
    edtDoc_AMT_UNIT.BoundLabel.Caption := '계약/결제금액';
    edtDoc_CIF_AMT_UNIT.BoundLabel.Font := noDataFontStyle;
    mskDoc_HS_CODE.BoundLabel.Font := noDataFontStyle;
    edtDoc_GOODS_NM.BoundLabel.Font := noDataFontStyle;
    edtDoc_IMPT_CD.BoundLabel.Font := noDataFontStyle;
    mskDoc_CONT_DT.BoundLabel.Font := normalDataFontStyle;
    edtDoc_TOD_CD.BoundLabel.Font := noDataFontStyle;
    edtDoc_EXPORT_NAT_CD.BoundLabel.Font := noDataFontStyle;
    edtDoc_EXPORT_OW_NM.BoundLabel.Font := noDataFontStyle;
    edtDoc_IMPORT_OW_NM.BoundLabel.Font := noDataFontStyle;    
  end;
end;

function TUI_APPRMI_NEW_frm.copyDocument:String;
var
  NEW_DOC_NO : String;
  SQLCreator : TSQLCreate;
begin
  if MessageBox(Self.Handle, MSG_COPY_QUESTION, '문서 복사확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
  Result := '';
  NEW_DOC_NO := DMAutoNo.GetDocumentNoAutoInc(DOCUMENT_CODE);
  Result := NEW_DOC_NO;

  SQLCreator := TSQLCreate.Create;
  try
    try
    DMMssql.BeginTrans;
    with SQLCreator do
    begin
      DMLType := dmlInsertSelect;
      // 공통사항
      SQLHeader('APPRMI_H', 'APPRMI_H');
      ADDWhere('MAINT_NO', qryAPPRMI_HMAINT_NO.AsString);

      ADDValue('MAINT_NO', NEW_DOC_NO, vtString);
      ADDValue('APP_DT', '', vtFieldName);
      ADDValue('REG_DT','CONVERT(varchar(10),getdate(),121)',vtVariant);
      ADDValue('USER_ID', LoginData.sID, vtInteger);
      ADDValue('TRN_HOPE_DT', '', vtFieldName);
      ADDValue('IMPT_SCHEDULE_DT', '', vtFieldName);
      ADDValue('ETC_NO', '', vtFieldName);
      ADDValue('TRD_CD', '', vtFieldName);
      ADDValue('IMPT_REMIT_CD', '', vtFieldName);
      ADDValue('IMPT_TRAN_FEE_CD', '', vtFieldName);
      ADDValue('REMIT_DESC', '', vtFieldName);
      ADDValue('REMIT_CD', '', vtFieldName);
      ADDValue('ADDED_FEE_TAR', '', vtFieldName);
      ADDValue('TOTAL_AMT', '', vtFieldName);
      ADDValue('TOTAL_AMT_UNIT', '', vtFieldName);
      ADDValue('FORE_AMT', '', vtFieldName);
      ADDValue('FORE_AMT_UNIT', '', vtFieldName);
      ADDValue('ONLY_PWD', '', vtFieldName);
      ADDValue('AUTH_NM1', '', vtFieldName);
      ADDValue('AUTH_NM2', '', vtFieldName);
      ADDValue('AUTH_NM3', '', vtFieldName);
      ADDValue('MESSAGE1', '', vtFieldName);
      ADDValue('MESSAGE2', '', vtFieldName);
      ADDValue('CHK2', '0');
      ADDValue('CHK3', '');

      ExecSQL(CreateSQL);

      // NAD
      SQLHeader('APPRMI_NAD', 'APPRMI_NAD');
      ADDWhere('MAINT_NO', qryAPPRMI_HMAINT_NO.AsString);

      ADDValue('MAINT_NO', NEW_DOC_NO, vtString);
      ADDValue('TAG', '', vtFieldName);
      ADDValue('CODE', '', vtFieldName);
      ADDValue('BUS_NO', '', vtFieldName);
      ADDValue('BUS_NM1', '', vtFieldName);
      ADDValue('BUS_NM2', '', vtFieldName);
      ADDValue('BUS_NM3', '', vtFieldName);
      ADDValue('BUS_ADDR1', '', vtFieldName);
      ADDValue('BUS_ADDR2', '', vtFieldName);
      ADDValue('BUS_ADDR3', '', vtFieldName);
      ADDValue('NAT_CD', '', vtFieldName);
      ADDValue('TEL_NO', '', vtFieldName);
      ExecSQL(CreateSQL);

      // FII
      SQLHeader('APPRMI_FII', 'APPRMI_FII');
      ADDWhere('MAINT_NO', qryAPPRMI_HMAINT_NO.AsString);

      ADDValue('MAINT_NO', NEW_DOC_NO, vtString);
      ADDValue('TAG', '', vtFieldName);
      ADDValue('ORD', '', vtFieldName);
      ADDValue('ACCNT_NO', '', vtFieldName);
      ADDValue('ACCNT_NM', '', vtFieldName);
      ADDValue('ACCNT_UNIT', '', vtFieldName);
      ADDValue('BNK_CD', '', vtFieldName);
      ADDValue('BNK_NM1', '', vtFieldName);
      ADDValue('BNK_NM2', '', vtFieldName);
      ADDValue('BNK_NM3', '', vtFieldName);
      ADDValue('BNK_NM4', '', vtFieldName);
      ADDValue('BNK_NAT_CD', '', vtFieldName);
      ExecSQL(CreateSQL);

      // RFF
      SQLHeader('APPRMI_RFF', 'APPRMI_RFF');
      ADDWhere('MAINT_NO', qryAPPRMI_HMAINT_NO.AsString);

      ADDValue('MAINT_NO', NEW_DOC_NO, vtString);
      ADDValue('RFF_NO', '', vtFieldName);
      ADDValue('AMT', '', vtFieldName);
      ADDValue('AMT_UNIT', '', vtFieldName);
      ADDValue('REMIT_AMT', '', vtFieldName);
      ADDValue('REMIT_AMT_UNIT', '', vtFieldName);
      ADDValue('CIF_AMT', '', vtFieldName);
      ADDValue('CIF_AMT_UNIT', '', vtFieldName);
      ADDValue('HS_CD', '', vtFieldName);
      ADDValue('GOODS_NM', '', vtFieldName);
      ADDValue('IMPT_CD', '', vtFieldName);
      ADDValue('CONT_DT', '', vtFieldName);
      ADDValue('TOD_CD', '', vtFieldName);
      ADDValue('TOD_REMARK', '', vtFieldName);
      ADDValue('EXPORT_NAT_CD', '', vtFieldName);
      ADDValue('EXPORT_OW_NM', '', vtFieldName);
      ADDValue('EXPORT_OW_ADDR', '', vtFieldName);
      ADDValue('EXPORT_OW_NAT_CD', '', vtFieldName);
      ADDValue('DETAIL_DESC1', '', vtFieldName);
      ADDValue('DETAIL_DESC2', '', vtFieldName);
      ADDValue('DETAIL_DESC3', '', vtFieldName);
      ADDValue('DETAIL_DESC4', '', vtFieldName);
      ADDValue('DETAIL_DESC5', '', vtFieldName);
      ADDValue('IMPORT_OW_NM', '', vtFieldName);
      ADDValue('IMPORT_OW_ADDR', '', vtFieldName);
      ADDValue('IMPORT_OW_NAT_CD', '', vtFieldName);
      ADDValue('REG_DT', '', vtFieldName);
      ADDValue('TMP_DEL', '', vtFieldName);
      ExecSQL(CreateSQL);

      DMMssql.CommitTrans;      
    end;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
        DMMssql.RollbackTrans;
      end;
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnCopyClick(Sender: TObject);
var
  DOC_NO : String;
begin
  inherited;
  DOC_NO := copyDocument;
  if DOC_NO <> '' Then
  begin
    qryAPPRMI_H.Close;
    qryAPPRMI_H.Open;
    qryAPPRMI_H.Locate('MAINT_NO', DOC_NO, []);
  end;
end;

procedure TUI_APPRMI_NEW_frm.FormActivate(Sender: TObject);
var
  MAINT_NO : String;
begin
  inherited;

  if qryAPPRMI_H.Active AND (qryAPPRMI_H.RecordCount > 0) then
  begin
    MAINT_NO := qryAPPRMI_HMAINT_NO.AsString;
    qryAPPRMI_H.Close;
    qryAPPRMI_H.Open;
    qryAPPRMI_H.Locate('MAINT_NO', MAINT_NO, []);
  end;


end;

end.

