unit UI_ADV707_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Mask, sMaskEdit, sEdit, sButton,
  sLabel, Buttons, sSpeedButton, ExtCtrls, sPanel, sSkinProvider, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  sBitBtn, Grids, DBGrids, acDBGrid, ADODB, DB, StrUtils, DateUtils,
  QuickRpt;

type
  TUI_ADV707_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPageControl1: TsPageControl;
    sTab_PAGE1: TsTabSheet;
    sPanel7: TsPanel;
    sTab_PAGE2: TsTabSheet;
    sPanel27: TsPanel;
    sTab_PAGEList: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel2: TsPanel;
    sPanel10: TsPanel;
    edt_APPNO: TsEdit;
    sPanel1: TsPanel;
    edt_CDNO: TsEdit;
    sPanel8: TsPanel;
    sPanel3: TsPanel;
    edt_RCVREF: TsEdit;
    sPanel9: TsPanel;
    sPanel11: TsPanel;
    edt_IBANKREF: TsEdit;
    sPanel12: TsPanel;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    msk_APPDATE: TsMaskEdit;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    msk_ISSDATE: TsMaskEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    msk_AMDDATE: TsMaskEdit;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    edt_PURP_MSG: TsEdit;
    edt_PURP_MSG_NM: TsEdit;
    curr_AMDNO1: TsCurrencyEdit;
    sPanel25: TsPanel;
    sPanel26: TsPanel;
    edt_CAN_REQ: TsEdit;
    sTab_PAGE5: TsTabSheet;
    sLabel3: TsLabel;
    sPanel30: TsPanel;
    sPanel31: TsPanel;
    sPanel32: TsPanel;
    sPanel33: TsPanel;
    sPanel34: TsPanel;
    sPanel35: TsPanel;
    curr_PERP: TsCurrencyEdit;
    curr_PERM: TsCurrencyEdit;
    edt_IncdCur: TsEdit;
    curr_IncdAmt: TsCurrencyEdit;
    edt_DecdCur: TsEdit;
    curr_DecdAmt: TsCurrencyEdit;
    sPanel45: TsPanel;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel54: TsPanel;
    sPanel55: TsPanel;
    edt_dochackPort: TsEdit;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sPanel58: TsPanel;
    edt_loadOn: TsEdit;
    edt_forTran: TsEdit;
    sPanel38: TsPanel;
    sPanel39: TsPanel;
    memo_ADDINFO1: TsMemo;
    sPanel40: TsPanel;
    sPanel71: TsPanel;
    sPanel72: TsPanel;
    edt_DRAFT1: TsEdit;
    edt_DRAFT2: TsEdit;
    edt_DRAFT3: TsEdit;
    sPanel73: TsPanel;
    edt_MIX_PAY1: TsEdit;
    edt_MIX_PAY2: TsEdit;
    edt_MIX_PAY3: TsEdit;
    edt_MIX_PAY4: TsEdit;
    sPanel74: TsPanel;
    edt_DEF_PAY1: TsEdit;
    edt_DEF_PAY2: TsEdit;
    edt_DEF_PAY3: TsEdit;
    edt_DEF_PAY4: TsEdit;
    sPanel75: TsPanel;
    sPanel76: TsPanel;
    sPanel41: TsPanel;
    sPanel42: TsPanel;
    edt_AA_CV1: TsEdit;
    edt_AA_CV2: TsEdit;
    edt_AA_CV3: TsEdit;
    edt_AA_CV4: TsEdit;
    sPanel43: TsPanel;
    sPanel44: TsPanel;
    sPanel46: TsPanel;
    sPanel47: TsPanel;
    edt_APPLICABLE_RULES_1: TsEdit;
    edt_DOCTYPE: TsEdit;
    edt_APPLICABLE_RULES_2: TsEdit;
    sPanel48: TsPanel;
    sPanel49: TsPanel;
    edt_PSHIP: TsEdit;
    sPanel50: TsPanel;
    sPanel51: TsPanel;
    edt_TSHIP: TsEdit;
    sPanel59: TsPanel;
    sPanel60: TsPanel;
    memo_shipPD1: TsMemo;
    sPanel61: TsPanel;
    sPanel62: TsPanel;
    memo_GOODS_DESC_1: TsMemo;
    sTab_PAGE6: TsTabSheet;
    sPanel63: TsPanel;
    sPanel64: TsPanel;
    sPanel65: TsPanel;
    memo_DOC_DESC_1: TsMemo;
    sPanel66: TsPanel;
    sPanel67: TsPanel;
    memo_ADD_DESC_1: TsMemo;
    sTab_PAGE3: TsTabSheet;
    sPanel70: TsPanel;
    sPanel77: TsPanel;
    sPanel78: TsPanel;
    edt_CON_INST: TsEdit;
    sPanel79: TsPanel;
    sPanel80: TsPanel;
    memo_INST_DESC_1: TsMemo;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    sPanel86: TsPanel;
    sPanel87: TsPanel;
    sPanel28: TsPanel;
    sPanel29: TsPanel;
    memo_AMD_CHARGE_NUM1: TsMemo;
    edt_AMD_CHARGE: TsEdit;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    memo_CHARGE_NUM1: TsMemo;
    edt_CHARGE: TsEdit;
    edt_APBANK: TsEdit;
    edt_APBANK1: TsEdit;
    edt_APBANK2: TsEdit;
    edt_APBANK3: TsEdit;
    edt_APBANK4: TsEdit;
    edt_ADBANK1: TsEdit;
    edt_ADBANK2: TsEdit;
    edt_ADBANK3: TsEdit;
    edt_ADBANK4: TsEdit;
    sPanel89: TsPanel;
    sPanel90: TsPanel;
    sPanel91: TsPanel;
    sPanel126: TsPanel;
    edt_ADBANK: TsEdit;
    edt_ADBANK5: TsEdit;
    sPanel130: TsPanel;
    sTab_PAGE4: TsTabSheet;
    sPanel92: TsPanel;
    sPanel93: TsPanel;
    sPanel94: TsPanel;
    sPanel95: TsPanel;
    edt_ISS_BANK: TsEdit;
    edt_ISS_BANK1: TsEdit;
    edt_ISS_BANK2: TsEdit;
    edt_ISS_BANK3: TsEdit;
    edt_ISS_BANK4: TsEdit;
    edt_ISS_BANK5: TsEdit;
    sPanel96: TsPanel;
    sPanel97: TsPanel;
    sPanel98: TsPanel;
    edt_NBANK_ISS1: TsEdit;
    edt_NBANK_ISS2: TsEdit;
    sPanel99: TsPanel;
    sPanel100: TsPanel;
    sPanel101: TsPanel;
    edt_AVAIL: TsEdit;
    edt_AVAIL1: TsEdit;
    edt_AVAIL2: TsEdit;
    edt_AVAIL3: TsEdit;
    edt_AVAIL4: TsEdit;
    edt_AVAIL_ACCNT: TsEdit;
    sPanel102: TsPanel;
    sPanel103: TsPanel;
    sPanel104: TsPanel;
    edt_DRAWEE: TsEdit;
    edt_DRAWEE1: TsEdit;
    edt_DRAWEE2: TsEdit;
    edt_DRAWEE3: TsEdit;
    edt_DRAWEE4: TsEdit;
    sPanel105: TsPanel;
    sPanel106: TsPanel;
    sPanel107: TsPanel;
    edt_CO_BANK: TsEdit;
    edt_CO_BANK1: TsEdit;
    edt_CO_BANK2: TsEdit;
    edt_CO_BANK3: TsEdit;
    edt_CO_BANK4: TsEdit;
    sPanel108: TsPanel;
    sPanel109: TsPanel;
    sPanel110: TsPanel;
    edt_REI_BANK: TsEdit;
    edt_REI_BANK1: TsEdit;
    edt_REI_BANK2: TsEdit;
    edt_REI_BANK3: TsEdit;
    edt_REI_BANK4: TsEdit;
    sPanel111: TsPanel;
    sPanel112: TsPanel;
    sPanel113: TsPanel;
    edt_AVT_BANK: TsEdit;
    edt_AVT_BANK1: TsEdit;
    edt_AVT_BANK2: TsEdit;
    edt_AVT_BANK3: TsEdit;
    edt_AVT_BANK4: TsEdit;
    sPanel114: TsPanel;
    sPanel115: TsPanel;
    msk_exDate: TsMaskEdit;
    edt_EXPLACE: TsEdit;
    edt_NBANK_ISS3: TsEdit;
    edt_NBANK_ISS4: TsEdit;
    sPanel81: TsPanel;
    sPanel82: TsPanel;
    sPanel83: TsPanel;
    memo_SRINFO: TsMemo;
    sPanel128: TsPanel;
    sPanel129: TsPanel;
    sPanel131: TsPanel;
    sPanel132: TsPanel;
    sPanel68: TsPanel;
    sPanel69: TsPanel;
    sPanel88: TsPanel;
    memo_SPECIAL_DESC_1: TsMemo;
    sPanel117: TsPanel;
    sPanel118: TsPanel;
    edt_EX_NAME1: TsEdit;
    edt_EX_NAME2: TsEdit;
    edt_EX_NAME3: TsEdit;
    edt_EX_ADDR1: TsEdit;
    edt_EX_ADDR2: TsEdit;
    sPanel119: TsPanel;
    sPanel120: TsPanel;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BENEFC3: TsEdit;
    edt_BENEFC4: TsEdit;
    edt_BENEFC5: TsEdit;
    edt_BENEFC6: TsEdit;
    sPanel121: TsPanel;
    sPanel124: TsPanel;
    sPanel125: TsPanel;
    edt_PERIOD: TsEdit;
    sPanel127: TsPanel;
    edt_PERIOD_TXT: TsEdit;
    sPanel122: TsPanel;
    sPanel123: TsPanel;
    edt_AMD_APPLIC1: TsEdit;
    edt_AMD_APPLIC2: TsEdit;
    edt_AMD_APPLIC3: TsEdit;
    edt_AMD_APPLIC4: TsEdit;
    sPanel20: TsPanel;
    sPanel21: TsPanel;
    msk_LSTDATE: TsMaskEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO_TXT: TStringField;
    qryListMAINT_NO: TStringField;
    qryListAMD_NO: TIntegerField;
    qryListAMD_NO1: TIntegerField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListISS_BANK: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListIBANK_REF: TStringField;
    qryListPRNO: TIntegerField;
    qryListGOODS_DESC: TStringField;
    qryListGOODS_DESC_1: TMemoField;
    qryListDOC_DESC: TStringField;
    qryListDOC_DESC_1: TMemoField;
    qryListADD_DESC: TStringField;
    qryListADD_DESC_1: TMemoField;
    qryListSPECIAL_DESC: TStringField;
    qryListSPECIAL_DESC_1: TMemoField;
    qryListINST_DESC: TStringField;
    qryListINST_DESC_1: TMemoField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListPURP_MSG: TStringField;
    qryListPURP_MSG_NM: TStringField;
    qryListCAN_REQ: TStringField;
    qryListCON_INST: TStringField;
    qryListDOC_TYPE: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_NUM1: TStringField;
    qryListCHARGE_NUM2: TStringField;
    qryListCHARGE_NUM3: TStringField;
    qryListCHARGE_NUM4: TStringField;
    qryListCHARGE_NUM5: TStringField;
    qryListCHARGE_NUM6: TStringField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_NUM1: TStringField;
    qryListAMD_CHARGE_NUM2: TStringField;
    qryListAMD_CHARGE_NUM3: TStringField;
    qryListAMD_CHARGE_NUM4: TStringField;
    qryListAMD_CHARGE_NUM5: TStringField;
    qryListAMD_CHARGE_NUM6: TStringField;
    qryListTSHIP: TStringField;
    qryListPSHIP: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListCO_BANK: TStringField;
    qryListCO_BANK1: TStringField;
    qryListCO_BANK2: TStringField;
    qryListCO_BANK3: TStringField;
    qryListCO_BANK4: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAMD_APPLIC1: TStringField;
    qryListAMD_APPLIC2: TStringField;
    qryListAMD_APPLIC3: TStringField;
    qryListAMD_APPLIC4: TStringField;
    qryListPERIOD: TIntegerField;
    qryListPERIOD_TXT: TStringField;
    qryListNBANK_ISS: TStringField;
    qryListNBANK_ISS1: TStringField;
    qryListNBANK_ISS2: TStringField;
    qryListNBANK_ISS3: TStringField;
    qryListNBANK_ISS4: TStringField;
    QRCompositeReport1: TQRCompositeReport;
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sPageControl1Change(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSQL : String;
    procedure RunSQL(sSQL: TStream);
    function ReadListOnly(sortField: String=''): integer;
    procedure setData;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_ADV707_NEW_frm: TUI_ADV707_NEW_frm;

implementation

uses
  VarDefine, MSSQL, MessageDefine, QR_ADV707_PRN, QR_ADV707_GOODS_PRN, Preview, QR_ADV707_DOCS_PRN, QR_ADV707_ADD_PRN, QR_ADV707_SPECIAL_PRN, QR_ADV707_DATA2_PRN;

{$R *.dfm}

procedure TUI_ADV707_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ADV707_NEW_frm.RunSQL(sSQL: TStream);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.LoadFromStream(sSQL);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_ADV707_NEW_frm.FormCreate(Sender: TObject);
var
  DLL_HAEDER : THandle;
  ResStream : TResourceStream;
begin
  inherited;
  // 코드생성 쿼리
  DllPatchExecute('INP4401_ADD');

  FSQL := qryList.SQL.Text;

  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  ReadOnlyControl(sPanel70);
  ReadOnlyControl(sPanel92);
  ReadOnlyControl(sPanel40);
  ReadOnlyControl(sPanel63);  
end;

function TUI_ADV707_NEW_frm.ReadListOnly(sortField : String = ''):integer;
begin
  ClearGridTitleCaption(sDBGrid1);
  ClearGridTitleCaption(sDBGrid3);

  with qryList do
  Begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(sMaskEdit1.Text)+' and '+QuotedStr(sMaskEdit2.Text));

    IF Length(Trim(edt_SearchNo.Text)) > 0 Then
      SQL.Add('AND A.MAINT_NO LIKE '+QuotedStr('%'+Trim(edt_SearchNo.Text)+'%'));

    IF sortField = '' Then
      SQL.Add('ORDER BY DATEE DESC, MAINT_NO DESC')
    else
    begin
      if (qryList.Filter = '') OR (qryList.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryList.Filter := sortField;
        Tag := 0;
      end
      else
      if qryList.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;

      if sortField <> 'MAINT_NO' then
        SQL.Add(', MAINT_NO DESC');
    end;

    Open;
  end;

  result := qryList.Tag;
end;

procedure TUI_ADV707_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  qrylist.Filter := '';
  ReadListOnly;
end;

procedure TUI_ADV707_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_ADV707_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', IncMonth(StartOfTheMonth( Now ),-1));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', Now);
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  ReadListOnly;
end;

procedure TUI_ADV707_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  btnDel.Enabled := DataSet.RecordCount > 0;
  btnPrint.Enabled := btnDel.Enabled;
  if DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_ADV707_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 6;
end;

procedure TUI_ADV707_NEW_frm.setData;
begin
  edt_MAINT_NO.Text := qryListMAINT_NO_TXT.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;
  msk_Datee.Text := qryListDATEE.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

  edt_APPNO.Text := qryListAPP_NO.AsString;
  msk_APPDATE.Text := qryListAPP_DATE.AsString;
  edt_CAN_REQ.Text := qryListCAN_REQ.AsString;
  curr_AMDNO1.Value := qryListAMD_NO1.AsInteger;
  msk_ISSDATE.Text := qryListISS_DATE.AsString;
  msk_AMDDATE.Text := qryListAMD_DATE.AsString;

  edt_CDNO.Text := qryListCD_NO.AsString;
  edt_RCVREF.Text := qryListRCV_REF.AsString;
  edt_IBANKREF.Text := qryListIBANK_REF.AsString;

  edt_PURP_MSG.Text := qryListPURP_MSG.AsString;
  edt_PURP_MSG_NM.Text := qryListPURP_MSG_NM.AsString;

  edt_APBANK.Text := qryListAP_BANK.AsString;
  edt_APBANK1.Text := qryListAP_BANK1.AsString;
  edt_APBANK2.Text := qryListAP_BANK2.AsString;
  edt_APBANK3.Text := qryListAP_BANK3.AsString;
  edt_APBANK4.Text := qryListAP_BANK4.AsString;

  edt_ADBANK.Text := qryListAD_BANK.AsString;
  edt_ADBANK1.Text := qryListAD_BANK1.AsString;
  edt_ADBANK2.Text := qryListAD_BANK2.AsString;
  edt_ADBANK3.Text := qryListAD_BANK3.AsString;
  edt_ADBANK4.Text := qryListAD_BANK4.AsString;
  edt_ADBANK5.Text := qryListAD_BANK5.AsString;

  memo_ADDINFO1.Lines.Text := qryListADDINFO_1.AsString;
  ClearEmptyLine(memo_ADDINFO1);

  IF Trim(qryListSR_INFO1.AsString) <> '' Then
  begin
    memo_SRINFO.Lines.Text := qryListSR_INFO1.AsString+#13#10+
                              qryListSR_INFO2.AsString+#13#10+
                              qryListSR_INFO3.AsString+#13#10+
                              qryListSR_INFO4.AsString+#13#10+
                              qryListSR_INFO5.AsString+#13#10+
                              qryListSR_INFO6.AsString;
    ClearEmptyLine(memo_SRINFO);
  end
  else
    memo_SRINFO.Clear;

  edt_IncdCur.Text := qryListINCD_CUR.AsString;
  curr_IncdAmt.Value := qryListINCD_AMT.AsFloat;
  edt_DecdCur.Text := qryListDECD_CUR.AsString;
  curr_DecdAmt.Value := qryListDECD_AMT.AsFloat;

  curr_PERP.Value := qryListCD_PERP.AsInteger;
  curr_PERM.Value := qryListCD_PERM.AsInteger;

  edt_loadOn.Text := qryListLOAD_ON.AsString;
  edt_forTran.Text := qryListFOR_TRAN.AsString;
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;

  edt_PERIOD.Text := qryListPERIOD.AsString;
  edt_PERIOD_TXT.Text := qryListPERIOD_TXT.AsString;
  msk_LSTDATE.Text := qryListLST_DATE.AsString;
  edt_DOCTYPE.Text := qryListDOC_TYPE.AsString;
  edt_APPLICABLE_RULES_1.Text := qryListAPPLICABLE_RULES_1.AsString;
  edt_APPLICABLE_RULES_2.Text := qryListAPPLICABLE_RULES_2.AsString;

  edt_PSHIP.Text := qryListPSHIP.AsString;
  edt_TSHIP.Text := qryListTSHIP.AsString;

  if Trim(qryListSHIP_PD1.AsString) <> '' Then
  begin
    memo_shipPD1.Lines.Text := qryListSHIP_PD1.AsString+#13#10+
                               qryListSHIP_PD2.AsString+#13#10+
                               qryListSHIP_PD3.AsString+#13#10+
                               qryListSHIP_PD4.AsString+#13#10+
                               qryListSHIP_PD5.AsString+#13#10+
                               qryListSHIP_PD6.AsString;
    ClearEmptyLine(memo_shipPD1);
  end
  else
    memo_shipPD1.Clear;

  msk_exDate.Text := qryListEX_DATE.AsString;
  edt_EXPLACE.Text := qryListEX_PLACE.AsString;
  edt_CON_INST.Text := qryListCON_INST.AsString;
  edt_CO_BANK.Text := qryListCO_BANK.AsString;
  edt_CO_BANK1.Text := qryListCO_BANK1.AsString;
  edt_CO_BANK2.Text := qryListCO_BANK2.AsString;
  edt_CO_BANK3.Text := qryListCO_BANK3.AsString;
  edt_CO_BANK4.Text := qryListCO_BANK4.AsString;

  memo_INST_DESC_1.Lines.Text := qryListINST_DESC_1.AsString;
  ClearEmptyLine(memo_INST_DESC_1);
  memo_SPECIAL_DESC_1.Lines.Text := qryListSPECIAL_DESC_1.AsString;
  ClearEmptyLine(memo_SPECIAL_DESC_1);

  edt_AMD_CHARGE.Text := qryListAMD_CHARGE.AsString;
  if Trim(qryListAMD_CHARGE_NUM1.AsString) <> '' then
  begin
    memo_AMD_CHARGE_NUM1.Lines.Text := qryListAMD_CHARGE_NUM1.AsString+#13#10+
                                       qryListAMD_CHARGE_NUM2.AsString+#13#10+
                                       qryListAMD_CHARGE_NUM3.AsString+#13#10+
                                       qryListAMD_CHARGE_NUM4.AsString+#13#10+
                                       qryListAMD_CHARGE_NUM5.AsString+#13#10+
                                       qryListAMD_CHARGE_NUM6.AsString;
    ClearEmptyLine(memo_AMD_CHARGE_NUM1);
  end
  else
    memo_AMD_CHARGE_NUM1.Clear;

  edt_CHARGE.Text := qryListCHARGE.AsString;
  if Trim(qryListCHARGE_NUM1.AsString) <> '' then
  begin
    memo_CHARGE_NUM1.Lines.Text := qryListCHARGE_NUM1.AsString+#13#10+
                                   qryListCHARGE_NUM2.AsString+#13#10+
                                   qryListCHARGE_NUM3.AsString+#13#10+
                                   qryListCHARGE_NUM4.AsString+#13#10+
                                   qryListCHARGE_NUM5.AsString+#13#10+
                                   qryListCHARGE_NUM6.AsString;
    ClearEmptyLine(memo_CHARGE_NUM1);
  end
  else
    memo_CHARGE_NUM1.Clear;

  edt_ISS_BANK.Text := qryListISS_BANK.AsString;
  edt_ISS_BANK1.Text := qryListISS_BANK1.AsString;
  edt_ISS_BANK2.Text := qryListISS_BANK2.AsString;
  edt_ISS_BANK3.Text := qryListISS_BANK3.AsString;
  edt_ISS_BANK4.Text := qryListISS_BANK4.AsString;
  edt_ISS_BANK5.Text := qryListISS_BANK5.AsString;

  edt_NBANK_ISS1.Text := qryListNBANK_ISS1.AsString;
  edt_NBANK_ISS2.Text := qryListNBANK_ISS2.AsString;
  edt_NBANK_ISS3.Text := qryListNBANK_ISS3.AsString;
  edt_NBANK_ISS4.Text := qryListNBANK_ISS4.AsString;

  edt_AVAIL.Text := qryListAVAIL.AsString;
  edt_AVAIL1.Text := qryListAVAIL1.AsString;
  edt_AVAIL2.Text := qryListAVAIL2.AsString;
  edt_AVAIL3.Text := qryListAVAIL3.AsString;
  edt_AVAIL4.Text := qryListAVAIL4.AsString;
  edt_AVAIL_ACCNT.Text := qryListAV_ACCNT.AsString;

  edt_REI_BANK.Text := qryListREI_BANK.AsString;
  edt_REI_BANK1.Text := qryListREI_BANK1.AsString;
  edt_REI_BANK2.Text := qryListREI_BANK2.AsString;
  edt_REI_BANK3.Text := qryListREI_BANK3.AsString;
  edt_REI_BANK4.Text := qryListREI_BANK4.AsString;

  edt_DRAWEE.Text := qryListDRAWEE.AsString;
  edt_DRAWEE1.Text := qryListDRAWEE1.AsString;
  edt_DRAWEE2.Text := qryListDRAWEE2.AsString;
  edt_DRAWEE3.Text := qryListDRAWEE3.AsString;
  edt_DRAWEE4.Text := qryListDRAWEE4.AsString;

  edt_AVT_BANK.Text := qryListAVT_BANK.AsString;
  edt_AVT_BANK1.Text := qryListAVT_BANK1.AsString;
  edt_AVT_BANK2.Text := qryListAVT_BANK2.AsString;
  edt_AVT_BANK3.Text := qryListAVT_BANK3.AsString;
  edt_AVT_BANK4.Text := qryListAVT_BANK4.AsString;

  edt_AA_CV1.Text := qryListAA_CV1.AsString;
  edt_AA_CV2.Text := qryListAA_CV2.AsString;
  edt_AA_CV3.Text := qryListAA_CV3.AsString;
  edt_AA_CV4.Text := qryListAA_CV4.AsString;

  edt_MIX_PAY1.Text := qryListMIX_PAY1.AsString;
  edt_MIX_PAY2.Text := qryListMIX_PAY2.AsString;
  edt_MIX_PAY3.Text := qryListMIX_PAY3.AsString;
  edt_MIX_PAY4.Text := qryListMIX_PAY4.AsString;

  edt_DRAFT1.Text := qryListDRAFT1.AsString;
  edt_DRAFT2.Text := qryListDRAFT2.AsString;
  edt_DRAFT3.Text := qryListDRAFT3.AsString;

  edt_DEF_PAY1.Text := qryListDEF_PAY1.AsString;
  edt_DEF_PAY2.Text := qryListDEF_PAY2.AsString;
  edt_DEF_PAY3.Text := qryListDEF_PAY3.AsString;
  edt_DEF_PAY4.Text := qryListDEF_PAY4.AsString;

  memo_GOODS_DESC_1.Lines.Text := qryListGOODS_DESC_1.AsString;
  ClearEmptyLine(memo_GOODS_DESC_1);

  memo_DOC_DESC_1.Lines.Text := qryListDOC_DESC_1.AsString;
  ClearEmptyLine(memo_DOC_DESC_1);

  memo_ADD_DESC_1.Lines.Text := qryListADD_DESC_1.AsString;
  ClearEmptyLine(memo_ADD_DESC_1);

  edt_AMD_APPLIC1.Text := qryListAMD_APPLIC1.AsString;
  edt_AMD_APPLIC2.Text := qryListAMD_APPLIC2.AsString;
  edt_AMD_APPLIC3.Text := qryListAMD_APPLIC3.AsString;
  edt_AMD_APPLIC4.Text := qryListAMD_APPLIC4.AsString;

  edt_EX_NAME1.Text := qryListEX_NAME1.AsString;
  edt_EX_NAME2.Text := qryListEX_NAME2.AsString;
  edt_EX_NAME3.Text := qryListEX_NAME3.AsString;
  edt_EX_ADDR1.Text := qryListEX_ADDR1.AsString;
  edt_EX_ADDR2.Text := qryListEX_ADDR2.AsString;

  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  edt_BENEFC4.Text := qryListBENEFC4.AsString;
  edt_BENEFC5.Text := qryListBENEFC5.AsString;
  edt_BENEFC6.Text := qryListBENEFC6.AsString;
end;

procedure TUI_ADV707_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setData;
end;

procedure TUI_ADV707_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_ADV707_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN then sBitBtn1Click(sBitBtn1);
end;

procedure TUI_ADV707_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no , amd_NO : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
    amd_NO := qryListAMD_NO.AsString;
    try
     try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM ADV707  WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and AMD_NO = ' + QuotedStr(amd_no)  ;
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7072  WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and AMD_NO = ' + QuotedStr(amd_no) ;
        ExecSQL;


        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if nCursor > 1 Then
          qryList.MoveBy(nCursor-1);
      end;

      except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
      end;

    finally
      if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      Close;
      Free;
    end;
  end;
end;

procedure TUI_ADV707_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  QR_ADV707_PRN_frm := TQR_ADV707_PRN_frm.Create(Self);
  QR_ADV707_GOODS_PRN_frm := TQR_ADV707_GOODS_PRN_frm.Create(Self);
  QR_ADV707_DOCS_PRN_frm  := TQR_ADV707_DOCS_PRN_frm.Create(Self);
  QR_ADV707_ADD_PRN_frm   := TQR_ADV707_ADD_PRN_frm.Create(Self);
  QR_ADV707_SPECIAL_PRN_frm := TQR_ADV707_SPECIAL_PRN_frm.Create(Self);
  QR_ADV707_DATA2_PRN_frm   := TQR_ADV707_DATA2_PRN_frm.Create(Self);

  Preview_frm := TPreview_frm.Create(Self);
  try
    QR_ADV707_PRN_frm.Maint_No := qryListMAINT_NO.AsString;
    QR_ADV707_PRN_frm.Amd_No   := qryListAMD_NO.AsInteger;
    QR_ADV707_GOODS_PRN_frm.FMAINT_NO := qryListMAINT_NO.AsString;
    QR_ADV707_GOODS_PRN_frm.FAMD_NO   := qryListAMD_NO.AsInteger;
    QR_ADV707_DOCS_PRN_frm.FMAINT_NO  := qryListMAINT_NO.AsString;
    QR_ADV707_DOCS_PRN_frm.FAMD_NO    := qryListAMD_NO.AsInteger;
    QR_ADV707_ADD_PRN_frm.FMAINT_NO   := qryListMAINT_NO.AsString;
    QR_ADV707_ADD_PRN_frm.FAMD_NO     := qryListAMD_NO.AsInteger;
    QR_ADV707_SPECIAL_PRN_frm.FMAINT_NO := qryListMAINT_NO.AsString;
    QR_ADV707_SPECIAL_PRN_frm.FAMD_NO := qryListAMD_NO.AsInteger;
    QR_ADV707_DATA2_PRN_frm.FMAINT_NO := qryListMAINT_NO.AsString;
    QR_ADV707_DATA2_PRN_frm.FAMD_NO := qryListAMD_NO.AsInteger;

    Preview_frm.CompositeRepost := QRCompositeReport1;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);  
    FreeAndNil(QR_ADV707_PRN_frm);
    FreeAndNil(QR_ADV707_GOODS_PRN_frm);
    FreeAndNil(QR_ADV707_DOCS_PRN_frm);
    FreeAndNil(QR_ADV707_ADD_PRN_frm);
    FreeAndNil(QR_ADV707_SPECIAL_PRN_frm);
    FreeAndNil(QR_ADV707_DATA2_PRN_frm);
  end;


//  QR_ADV707_PRN_frm := TQR_ADV707_PRN_frm.Create(Application);
//  QR_ADV707_GOODS_PRN_frm := TQR_ADV707_GOODS_PRN_frm.Create(Application);
//  try
////    QR_ADV707_PRN_frm.Maint_No := qryListMAINT_NO.AsString;
////    QR_ADV707_PRN_frm.Amd_No   := qryListAMD_NO.AsInteger;
////    QR_ADV707_PRN_frm.Preview;
//
//    QR_ADV707_GOODS_PRN_frm.FMAINT_NO := qryListMAINT_NO.AsString;
//    QR_ADV707_GOODS_PRN_frm.FAMD_NO := qryListAMD_NO.AsInteger;
//    QR_ADV707_GOODS_PRN_frm.Preview;
//  finally
//    FreeAndNil( QR_ADV707_PRN_frm );
//    FreeAndNil( QR_ADV707_GOODS_PRN_frm );
//  end;
end;

procedure TUI_ADV707_NEW_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(QR_ADV707_PRN_frm);
  QRCompositeReport1.Reports.Add(QR_ADV707_GOODS_PRN_frm);
  QRCompositeReport1.Reports.Add(QR_ADV707_DOCS_PRN_frm);
  QRCompositeReport1.Reports.Add(QR_ADV707_ADD_PRN_frm);
  QRCompositeReport1.Reports.Add(QR_ADV707_SPECIAL_PRN_frm);
  QRCompositeReport1.Reports.Add(QR_ADV707_DATA2_PRN_frm);
end;

procedure TUI_ADV707_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ADV707_NEW_frm := nil;
end;

end.
