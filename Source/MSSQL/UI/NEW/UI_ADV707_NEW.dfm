inherited UI_ADV707_NEW_frm: TUI_ADV707_NEW_frm
  Left = 405
  Top = 223
  BorderWidth = 4
  Caption = '[ADV707] '#49688#52636#49888#50857#51109' '#51312#44148#48320#44221#53685#51648#49436
  ClientHeight = 673
  ClientWidth = 1069
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1069
    Height = 74
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1069
      74)
    object sSpeedButton4: TsSpeedButton
      Left = 381
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 16
      Top = 14
      Width = 210
      Height = 23
      Alignment = taCenter
      Caption = #49688#52636#49888#50857#51109' '#51312#44148#48320#44221#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 84
      Top = 37
      Width = 73
      Height = 21
      Caption = '(ADV707)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 240
      Top = 3
      Width = 8
      Height = 66
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 994
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 249
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 315
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 248
      Top = 40
      Width = 818
      Height = 32
      SkinData.SkinSection = 'TRANSPARENT'
      Anchors = [akTop, akRight]
      
      TabOrder = 3
      DesignSize = (
        818
        32)
      object sSpeedButton1: TsSpeedButton
        Left = 604
        Top = 5
        Width = 11
        Height = 22
        Anchors = [akTop, akRight]
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 55
        Top = 4
        Width = 209
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 407
        Top = 4
        Width = 89
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 676
        Top = 4
        Width = 33
        Height = 23
        Anchors = [akTop, akRight]
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 772
        Top = 4
        Width = 43
        Height = 23
        Anchors = [akTop, akRight]
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 311
        Top = 4
        Width = 33
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 74
    Width = 341
    Height = 599
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 541
      TabStop = False
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      OnTitleClick = sDBGrid1TitleClick
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO_TXT'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 186
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'AMD_NO1'
          Title.Alignment = taCenter
          Title.Caption = #48320#44221
          Width = 35
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 84
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = sMaskEdit1Change
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 0
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 341
    Top = 74
    Width = 728
    Height = 599
    ActivePage = sTab_PAGE4
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 26
    TabOrder = 2
    OnChange = sPageControl1Change
    TabPadding = 15
    object sTab_PAGE1: TsTabSheet
      Caption = 'PAGE1'
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel2: TsPanel
          Left = 9
          Top = 9
          Width = 69
          Height = 47
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'NON-SWIFT'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel10: TsPanel
          Left = 79
          Top = 9
          Width = 106
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #53685#51648#48264#54840
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object edt_APPNO: TsEdit
          Left = 186
          Top = 9
          Width = 244
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel1: TsPanel
          Left = 9
          Top = 167
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '20'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object edt_CDNO: TsEdit
          Left = 186
          Top = 167
          Width = 143
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel8: TsPanel
          Left = 42
          Top = 167
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Sender'#39's Reference'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object sPanel3: TsPanel
          Left = 9
          Top = 191
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '21'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object edt_RCVREF: TsEdit
          Left = 186
          Top = 191
          Width = 143
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel9: TsPanel
          Left = 42
          Top = 191
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Receiver'#39's Reference'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object sPanel11: TsPanel
          Left = 9
          Top = 215
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '23'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object edt_IBANKREF: TsEdit
          Left = 186
          Top = 215
          Width = 143
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel12: TsPanel
          Left = 42
          Top = 215
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Issuing Bank'#39's Reference'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object sPanel13: TsPanel
          Left = 9
          Top = 81
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '26E'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object sPanel14: TsPanel
          Left = 42
          Top = 81
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51312#44148#48320#44221#54943#49688
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
        object sPanel15: TsPanel
          Left = 79
          Top = 33
          Width = 106
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51312#44148#48320#44221' '#53685#51648#51068#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object msk_APPDATE: TsMaskEdit
          Left = 186
          Top = 33
          Width = 79
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 15
          Color = clWhite
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sPanel16: TsPanel
          Left = 9
          Top = 107
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '31C'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object sPanel17: TsPanel
          Left = 42
          Top = 107
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44060#49444#51068#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object msk_ISSDATE: TsMaskEdit
          Left = 186
          Top = 107
          Width = 79
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 18
          Color = clWhite
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sPanel18: TsPanel
          Left = 9
          Top = 131
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '30'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 19
        end
        object sPanel19: TsPanel
          Left = 42
          Top = 131
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51312#44148#48320#44221#51068#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 20
        end
        object msk_AMDDATE: TsMaskEdit
          Left = 186
          Top = 131
          Width = 79
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 21
          Color = clWhite
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sPanel22: TsPanel
          Left = 9
          Top = 253
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '22A'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 22
        end
        object sPanel23: TsPanel
          Left = 42
          Top = 253
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #47928#49436#47785#51201
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object edt_PURP_MSG: TsEdit
          Left = 186
          Top = 253
          Width = 36
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_PURP_MSG_NM: TsEdit
          Left = 223
          Top = 253
          Width = 207
          Height = 23
          Color = clBtnFace
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object curr_AMDNO1: TsCurrencyEdit
          Left = 186
          Top = 81
          Width = 42
          Height = 23
          Hint = #44284#48512#51313' '#54728#50857#50984'+'
          AutoSize = False
          
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 26
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          DecimalPlaces = 4
          DisplayFormat = '0'
        end
        object sPanel25: TsPanel
          Left = 9
          Top = 57
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '23S'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object sPanel26: TsPanel
          Left = 42
          Top = 57
          Width = 143
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49888#50857#51109#52712#49548#50836#52397
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
        end
        object edt_CAN_REQ: TsEdit
          Left = 186
          Top = 57
          Width = 42
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel38: TsPanel
          Left = 9
          Top = 289
          Width = 69
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'NON-SWIFT'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
        end
        object sPanel39: TsPanel
          Left = 79
          Top = 289
          Width = 138
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44592#53440#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object memo_ADDINFO1: TsMemo
          Left = 9
          Top = 313
          Width = 702
          Height = 112
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 32
        end
        object edt_APBANK: TsEdit
          Left = 611
          Top = 9
          Width = 101
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 11
          TabOrder = 33
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APBANK1: TsEdit
          Left = 437
          Top = 33
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 34
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APBANK2: TsEdit
          Left = 437
          Top = 57
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 35
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APBANK3: TsEdit
          Left = 437
          Top = 81
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 36
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APBANK4: TsEdit
          Left = 437
          Top = 105
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ADBANK1: TsEdit
          Left = 437
          Top = 158
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 38
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ADBANK2: TsEdit
          Left = 437
          Top = 182
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 39
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ADBANK3: TsEdit
          Left = 437
          Top = 206
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 40
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ADBANK4: TsEdit
          Left = 437
          Top = 230
          Width = 275
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 41
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel89: TsPanel
          Left = 437
          Top = 9
          Width = 53
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'MT700'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 42
        end
        object sPanel90: TsPanel
          Left = 491
          Top = 9
          Width = 119
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51204#47928#48156#49888#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 43
        end
        object sPanel91: TsPanel
          Left = 491
          Top = 134
          Width = 119
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51204#47928#49688#49888#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 44
        end
        object sPanel126: TsPanel
          Left = 437
          Top = 134
          Width = 53
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'MT700'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 45
        end
        object edt_ADBANK: TsEdit
          Left = 611
          Top = 134
          Width = 101
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 11
          TabOrder = 46
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ADBANK5: TsEdit
          Left = 491
          Top = 254
          Width = 221
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 47
          Visible = False
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel130: TsPanel
          Left = 437
          Top = 254
          Width = 53
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'TEL'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 48
          Visible = False
        end
        object sPanel81: TsPanel
          Tag = -1
          Left = 9
          Top = 433
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '72Z'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 49
        end
        object sPanel82: TsPanel
          Left = 44
          Top = 433
          Width = 216
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Sender to Receiver Information'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 50
        end
        object sPanel83: TsPanel
          Tag = -1
          Left = 261
          Top = 433
          Width = 119
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#49888#51008#54665#50526' '#51221#48372
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 51
        end
        object memo_SRINFO: TsMemo
          Left = 9
          Top = 457
          Width = 702
          Height = 96
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 52
        end
      end
    end
    object sTab_PAGE2: TsTabSheet
      Caption = 'PAGE2'
      object sPanel27: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sLabel3: TsLabel
          Left = 516
          Top = 61
          Width = 10
          Height = 15
          Caption = '%'
        end
        object sPanel30: TsPanel
          Tag = -1
          Left = 9
          Top = 9
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '32B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel31: TsPanel
          Left = 44
          Top = 9
          Width = 327
          Height = 23
          SkinData.CustomColor = True
          Caption = #49888#50857#51109' '#51613#50529#48516'(Increase of Documentary Credit Amount)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object sPanel32: TsPanel
          Tag = -1
          Left = 9
          Top = 33
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '33B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object sPanel33: TsPanel
          Left = 44
          Top = 33
          Width = 327
          Height = 23
          SkinData.CustomColor = True
          Caption = #49888#50857#51109' '#44048#50529#48516'(Decrease of Documentary Credit Amount)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object sPanel34: TsPanel
          Tag = -1
          Left = 9
          Top = 57
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '39A'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object sPanel35: TsPanel
          Left = 44
          Top = 57
          Width = 327
          Height = 23
          SkinData.CustomColor = True
          Caption = #44284#48512#51313#54728#50857#50984'(Percentage Credit Amount Tolerance)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object curr_PERP: TsCurrencyEdit
          Left = 407
          Top = 57
          Width = 42
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 6
          BoundLabel.Active = True
          BoundLabel.Caption = '('#65291')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          DecimalPlaces = 0
          DisplayFormat = '0'
        end
        object curr_PERM: TsCurrencyEdit
          Left = 474
          Top = 57
          Width = 39
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 7
          BoundLabel.Active = True
          BoundLabel.Caption = '/ ('#65293')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          DecimalPlaces = 0
          DisplayFormat = '0'
        end
        object edt_IncdCur: TsEdit
          Tag = 1010
          Left = 372
          Top = 9
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          MaxLength = 3
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_IncdAmt: TsCurrencyEdit
          Left = 407
          Top = 9
          Width = 135
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 9
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt_DecdCur: TsEdit
          Tag = 1011
          Left = 372
          Top = 33
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          MaxLength = 3
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_DecdAmt: TsCurrencyEdit
          Left = 407
          Top = 33
          Width = 135
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 11
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object sPanel45: TsPanel
          Left = 44
          Top = 137
          Width = 405
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 
            'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
            'livery'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object sPanel52: TsPanel
          Tag = -1
          Left = 9
          Top = 185
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44E'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
        object sPanel53: TsPanel
          Left = 44
          Top = 185
          Width = 405
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Loading/Airport of Departure'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object edt_SunjukPort: TsEdit
          Left = 9
          Top = 209
          Width = 700
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel54: TsPanel
          Tag = -1
          Left = 9
          Top = 233
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44F'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object sPanel55: TsPanel
          Left = 44
          Top = 233
          Width = 405
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Discharge/Airport of Destination'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object edt_dochackPort: TsEdit
          Left = 9
          Top = 257
          Width = 700
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel56: TsPanel
          Tag = -1
          Left = 9
          Top = 89
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44A'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 19
        end
        object sPanel57: TsPanel
          Left = 44
          Top = 89
          Width = 405
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 20
        end
        object sPanel58: TsPanel
          Tag = -1
          Left = 9
          Top = 137
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 21
        end
        object edt_loadOn: TsEdit
          Left = 9
          Top = 113
          Width = 700
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_forTran: TsEdit
          Left = 9
          Top = 161
          Width = 700
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel43: TsPanel
          Tag = -1
          Left = 9
          Top = 345
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '40A'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object sPanel44: TsPanel
          Left = 44
          Top = 345
          Width = 229
          Height = 23
          SkinData.CustomColor = True
          Caption = #49888#50857#51109#51333#47448'(Form of Documentary Credit)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object sPanel46: TsPanel
          Tag = -1
          Left = 9
          Top = 369
          Width = 34
          Height = 47
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '40E'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
        end
        object sPanel47: TsPanel
          Left = 44
          Top = 369
          Width = 229
          Height = 47
          SkinData.CustomColor = True
          Caption = #49888#50857#51109' '#51201#50857#44508#52825'(Applicable Rules)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object edt_APPLICABLE_RULES_1: TsEdit
          Left = 274
          Top = 369
          Width = 250
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DOCTYPE: TsEdit
          Left = 274
          Top = 345
          Width = 250
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLICABLE_RULES_2: TsEdit
          Left = 274
          Top = 393
          Width = 250
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel48: TsPanel
          Tag = -1
          Left = 9
          Top = 425
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '43P'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object sPanel49: TsPanel
          Left = 44
          Top = 425
          Width = 173
          Height = 23
          SkinData.CustomColor = True
          Caption = #48516#54624#49440#51201'(Parial Shipment)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 32
        end
        object edt_PSHIP: TsEdit
          Left = 218
          Top = 425
          Width = 167
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel50: TsPanel
          Tag = -1
          Left = 9
          Top = 449
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '43T'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 34
        end
        object sPanel51: TsPanel
          Left = 44
          Top = 449
          Width = 173
          Height = 23
          SkinData.CustomColor = True
          Caption = #54872#51201#54728#50857'(Transhipment)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 35
        end
        object edt_TSHIP: TsEdit
          Left = 218
          Top = 449
          Width = 167
          Height = 23
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel59: TsPanel
          Tag = -1
          Left = 9
          Top = 473
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44D'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 37
        end
        object sPanel60: TsPanel
          Left = 44
          Top = 473
          Width = 173
          Height = 23
          SkinData.CustomColor = True
          Caption = #49440#51201#44592#44036'(Shipment Period)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 38
        end
        object memo_shipPD1: TsMemo
          Left = 218
          Top = 473
          Width = 491
          Height = 80
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 39
        end
        object sPanel128: TsPanel
          Tag = -1
          Left = 450
          Top = 89
          Width = 120
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#53441#48156#49569#51648
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 40
        end
        object sPanel129: TsPanel
          Tag = -1
          Left = 450
          Top = 137
          Width = 120
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #52572#51333#47785#51201#51648
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 41
        end
        object sPanel131: TsPanel
          Tag = -1
          Left = 450
          Top = 185
          Width = 120
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49440#51201#54637
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 42
        end
        object sPanel132: TsPanel
          Tag = -1
          Left = 450
          Top = 233
          Width = 120
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #46020#52265#54637
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 43
        end
        object sPanel124: TsPanel
          Left = 9
          Top = 289
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '48'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 44
        end
        object sPanel125: TsPanel
          Left = 44
          Top = 289
          Width = 127
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49436#47448#51228#49884#44592#44036
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 45
        end
        object edt_PERIOD: TsEdit
          Left = 172
          Top = 289
          Width = 31
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 46
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel127: TsPanel
          Left = 204
          Top = 289
          Width = 159
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49440#51201#51068#51088' '#44592#51456' '#50808' '#49345#49464#44592#51456
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 47
        end
        object edt_PERIOD_TXT: TsEdit
          Left = 364
          Top = 289
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 48
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel20: TsPanel
          Left = 9
          Top = 315
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44C'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 49
        end
        object sPanel21: TsPanel
          Left = 44
          Top = 315
          Width = 127
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #52572#51333#49440#51201#51068#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 50
        end
        object msk_LSTDATE: TsMaskEdit
          Left = 172
          Top = 315
          Width = 79
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 51
          Color = clWhite
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
      end
    end
    object sTab_PAGE3: TsTabSheet
      Caption = 'PAGE3'
      object sPanel70: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel77: TsPanel
          Tag = -1
          Left = 9
          Top = 41
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '49'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel78: TsPanel
          Left = 44
          Top = 41
          Width = 157
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Confirmation Instructions'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object edt_CON_INST: TsEdit
          Left = 292
          Top = 41
          Width = 79
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel79: TsPanel
          Tag = -1
          Left = 9
          Top = 193
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '78'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object sPanel80: TsPanel
          Left = 44
          Top = 193
          Width = 309
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Instructions to the Paying/Accepting/Negotiating Bank'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object memo_INST_DESC_1: TsMemo
          Left = 9
          Top = 217
          Width = 701
          Height = 80
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 5
        end
        object sPanel84: TsPanel
          Tag = -1
          Left = 354
          Top = 193
          Width = 231
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51648#44553'/'#51064#49688'/'#47588#51077#51008#54665#50640' '#45824#54620' '#51648#49884#49324#54637
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object sPanel85: TsPanel
          Tag = -1
          Left = 202
          Top = 41
          Width = 89
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #54869#51064#51648#49884#47928#50616
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object sPanel28: TsPanel
          Left = 9
          Top = 432
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '71N'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object sPanel29: TsPanel
          Left = 42
          Top = 432
          Width = 247
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49888#50857#51109#48320#44221' '#49688#49688#47308' '#48512#45812#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object memo_AMD_CHARGE_NUM1: TsMemo
          Left = 9
          Top = 456
          Width = 349
          Height = 96
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 10
        end
        object edt_AMD_CHARGE: TsEdit
          Left = 290
          Top = 432
          Width = 68
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentFont = False
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel36: TsPanel
          Left = 359
          Top = 432
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '71D'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object sPanel37: TsPanel
          Left = 392
          Top = 432
          Width = 247
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#49688#47308' '#48512#45812#51088' '#48320#44221'(Charges)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
        object memo_CHARGE_NUM1: TsMemo
          Left = 359
          Top = 456
          Width = 349
          Height = 96
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 14
        end
        object edt_CHARGE: TsEdit
          Left = 640
          Top = 432
          Width = 68
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel105: TsPanel
          Left = 421
          Top = 41
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '58a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object sPanel106: TsPanel
          Left = 454
          Top = 41
          Width = 178
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Request Confirmation Party'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object sPanel107: TsPanel
          Left = 633
          Top = 41
          Width = 75
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #54869#51064#51008#54665
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object edt_CO_BANK: TsEdit
          Left = 421
          Top = 65
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CO_BANK1: TsEdit
          Left = 421
          Top = 89
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CO_BANK2: TsEdit
          Left = 421
          Top = 113
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CO_BANK3: TsEdit
          Left = 421
          Top = 137
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CO_BANK4: TsEdit
          Left = 421
          Top = 161
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel114: TsPanel
          Left = 9
          Top = 9
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '31D'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object sPanel115: TsPanel
          Left = 44
          Top = 9
          Width = 149
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Date and Place of Expiry'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object msk_exDate: TsMaskEdit
          Left = 194
          Top = 9
          Width = 87
          Height = 23
          Hint = #50976#54952#44592#51068
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          ParentShowHint = False
          ShowHint = True
          TabOrder = 26
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object edt_EXPLACE: TsEdit
          Left = 282
          Top = 9
          Width = 427
          Height = 23
          Hint = #50976#54952#51109#49548
          Color = clWhite
          Enabled = False
          TabOrder = 27
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel68: TsPanel
          Tag = -1
          Left = 9
          Top = 309
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '49M'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
        end
        object sPanel69: TsPanel
          Left = 44
          Top = 309
          Width = 253
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Special Payment Conditions for Beneficiary'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
        end
        object sPanel88: TsPanel
          Tag = -1
          Left = 298
          Top = 309
          Width = 191
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#51061#51088#50640' '#45824#54620' '#53945#48324#51648#44553#51312#44148
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
        end
        object memo_SPECIAL_DESC_1: TsMemo
          Left = 9
          Top = 333
          Width = 701
          Height = 93
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 31
        end
      end
    end
    object sTab_PAGE4: TsTabSheet
      Caption = 'PAGE4'
      object sPanel92: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel93: TsPanel
          Left = 9
          Top = 9
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '52a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel94: TsPanel
          Left = 42
          Top = 9
          Width = 117
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Issuing Bank'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object sPanel95: TsPanel
          Left = 160
          Top = 9
          Width = 136
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44060#49444#51008#54665
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object edt_ISS_BANK: TsEdit
          Left = 9
          Top = 33
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ISS_BANK1: TsEdit
          Left = 9
          Top = 57
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ISS_BANK2: TsEdit
          Left = 9
          Top = 81
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 5
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ISS_BANK3: TsEdit
          Left = 9
          Top = 105
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 6
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ISS_BANK4: TsEdit
          Left = 9
          Top = 129
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_ISS_BANK5: TsEdit
          Left = 9
          Top = 153
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel96: TsPanel
          Left = 313
          Top = 9
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '50B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object sPanel97: TsPanel
          Left = 346
          Top = 9
          Width = 117
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Non-Bank Issuer'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object sPanel98: TsPanel
          Left = 464
          Top = 9
          Width = 136
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #48708#51008#54665#44060#49444#51088
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object edt_NBANK_ISS1: TsEdit
          Left = 313
          Top = 33
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_NBANK_ISS2: TsEdit
          Left = 313
          Top = 57
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel99: TsPanel
          Left = 9
          Top = 185
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '41a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object sPanel100: TsPanel
          Left = 42
          Top = 185
          Width = 117
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Available with.. by ..'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object sPanel101: TsPanel
          Left = 160
          Top = 185
          Width = 136
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49888#50857#51109#51648#44553#48169#49885' '#48143' '#51008#54665
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edt_AVAIL: TsEdit
          Left = 9
          Top = 209
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVAIL1: TsEdit
          Left = 9
          Top = 233
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVAIL2: TsEdit
          Left = 9
          Top = 257
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVAIL3: TsEdit
          Left = 9
          Top = 281
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVAIL4: TsEdit
          Left = 9
          Top = 305
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVAIL_ACCNT: TsEdit
          Left = 9
          Top = 329
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel102: TsPanel
          Left = 9
          Top = 361
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '42a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object sPanel103: TsPanel
          Left = 42
          Top = 361
          Width = 117
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Drawee'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object sPanel104: TsPanel
          Left = 160
          Top = 361
          Width = 136
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #50612#51020#51648#44553#51064
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object edt_DRAWEE: TsEdit
          Left = 9
          Top = 385
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 26
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAWEE1: TsEdit
          Left = 9
          Top = 409
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 27
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAWEE2: TsEdit
          Left = 9
          Top = 433
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 28
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAWEE3: TsEdit
          Left = 9
          Top = 457
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 29
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAWEE4: TsEdit
          Left = 9
          Top = 481
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 30
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel108: TsPanel
          Left = 313
          Top = 185
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '53a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object sPanel109: TsPanel
          Left = 346
          Top = 185
          Width = 117
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Reimbursing Bank'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 32
        end
        object sPanel110: TsPanel
          Left = 464
          Top = 185
          Width = 136
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49345#54872#51008#54665
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 33
        end
        object edt_REI_BANK: TsEdit
          Left = 313
          Top = 209
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 34
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_REI_BANK1: TsEdit
          Left = 313
          Top = 233
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 35
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_REI_BANK2: TsEdit
          Left = 313
          Top = 257
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 36
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_REI_BANK3: TsEdit
          Left = 313
          Top = 281
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_REI_BANK4: TsEdit
          Left = 313
          Top = 305
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 38
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel111: TsPanel
          Left = 313
          Top = 361
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '57a'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 39
        end
        object sPanel112: TsPanel
          Left = 346
          Top = 361
          Width = 149
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '"Advise Through" Bank'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 40
        end
        object sPanel113: TsPanel
          Left = 496
          Top = 361
          Width = 104
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #52572#51333#53685#51648#51008#54665
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 41
        end
        object edt_AVT_BANK: TsEdit
          Left = 313
          Top = 385
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 42
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVT_BANK1: TsEdit
          Left = 313
          Top = 409
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 43
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVT_BANK2: TsEdit
          Left = 313
          Top = 433
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 44
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVT_BANK3: TsEdit
          Left = 313
          Top = 457
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 45
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AVT_BANK4: TsEdit
          Left = 313
          Top = 481
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 46
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_NBANK_ISS3: TsEdit
          Left = 313
          Top = 81
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 47
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_NBANK_ISS4: TsEdit
          Left = 313
          Top = 105
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 48
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTab_PAGE5: TsTabSheet
      Caption = 'PAGE5'
      object sPanel40: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel71: TsPanel
          Tag = -1
          Left = 9
          Top = 125
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '42C'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel72: TsPanel
          Left = 44
          Top = 125
          Width = 313
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #54868#54872#50612#51020#51312#44148'(Drafts at...)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object edt_DRAFT1: TsEdit
          Left = 9
          Top = 149
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 2
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAFT2: TsEdit
          Left = 9
          Top = 171
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DRAFT3: TsEdit
          Left = 9
          Top = 193
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel73: TsPanel
          Left = 396
          Top = 9
          Width = 313
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #54844#54633#51648#44553#51312#44148'(Mixed Payment Detail)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object edt_MIX_PAY1: TsEdit
          Left = 361
          Top = 33
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 6
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_MIX_PAY2: TsEdit
          Left = 361
          Top = 55
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_MIX_PAY3: TsEdit
          Left = 361
          Top = 77
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_MIX_PAY4: TsEdit
          Left = 361
          Top = 99
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel74: TsPanel
          Left = 396
          Top = 125
          Width = 313
          Height = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #47588#51077'/'#50672#51648#44552#51312#44148#47749#49464#13#10'(Negotiation/Deferred Payment Details)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object edt_DEF_PAY1: TsEdit
          Left = 361
          Top = 162
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DEF_PAY2: TsEdit
          Left = 361
          Top = 184
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DEF_PAY3: TsEdit
          Left = 361
          Top = 206
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_DEF_PAY4: TsEdit
          Left = 361
          Top = 228
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 14
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel75: TsPanel
          Tag = -1
          Left = 361
          Top = 125
          Width = 34
          Height = 36
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '42P'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object sPanel76: TsPanel
          Tag = -1
          Left = 361
          Top = 9
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '42M'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object sPanel41: TsPanel
          Tag = -1
          Left = 9
          Top = 9
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '39C'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object sPanel42: TsPanel
          Left = 44
          Top = 9
          Width = 313
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #48512#44032#44552#50529#48512#45812'(Additional Amount Covered)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object edt_AA_CV1: TsEdit
          Left = 9
          Top = 33
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AA_CV2: TsEdit
          Left = 9
          Top = 55
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AA_CV3: TsEdit
          Left = 9
          Top = 77
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AA_CV4: TsEdit
          Left = 9
          Top = 99
          Width = 348
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 65
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Caption = 'PLACE'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel61: TsPanel
          Tag = -1
          Left = 9
          Top = 261
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '44D'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object sPanel62: TsPanel
          Left = 44
          Top = 261
          Width = 314
          Height = 23
          SkinData.CustomColor = True
          Caption = #49345#54408'['#50857#50669']'#47749#49464'(Description of Goods and/or Services)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object memo_GOODS_DESC_1: TsMemo
          Left = 9
          Top = 285
          Width = 701
          Height = 273
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 25
        end
      end
    end
    object sTab_PAGE6: TsTabSheet
      Caption = 'PAGE6'
      object sPanel63: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 563
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel64: TsPanel
          Tag = -1
          Left = 9
          Top = 9
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '46B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object sPanel65: TsPanel
          Left = 44
          Top = 9
          Width = 162
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Documents Required'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object memo_DOC_DESC_1: TsMemo
          Left = 9
          Top = 33
          Width = 701
          Height = 115
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 2
        end
        object sPanel66: TsPanel
          Tag = -1
          Left = 9
          Top = 153
          Width = 34
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '47B'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object sPanel67: TsPanel
          Left = 44
          Top = 153
          Width = 162
          Height = 23
          SkinData.CustomColor = True
          Caption = 'Additional Conditions'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object memo_ADD_DESC_1: TsMemo
          Left = 9
          Top = 177
          Width = 701
          Height = 88
          Ctl3D = True
          ParentCtl3D = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 5
        end
        object sPanel86: TsPanel
          Tag = -1
          Left = 207
          Top = 9
          Width = 89
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44396#48708#49436#47448
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object sPanel87: TsPanel
          Tag = -1
          Left = 207
          Top = 153
          Width = 89
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #48512#44032#51312#44148
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object sPanel117: TsPanel
          Left = 9
          Top = 401
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Non'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object sPanel118: TsPanel
          Left = 42
          Top = 401
          Width = 149
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #53685#51648#51008#54665' '#51204#51088#49436#47749
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object edt_EX_NAME1: TsEdit
          Left = 9
          Top = 425
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_EX_NAME2: TsEdit
          Left = 9
          Top = 449
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_EX_NAME3: TsEdit
          Left = 9
          Top = 473
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_EX_ADDR1: TsEdit
          Left = 9
          Top = 497
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_EX_ADDR2: TsEdit
          Left = 9
          Top = 521
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 14
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel119: TsPanel
          Left = 313
          Top = 273
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '59'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object sPanel120: TsPanel
          Left = 346
          Top = 273
          Width = 149
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#51061#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edt_BENEFC1: TsEdit
          Left = 313
          Top = 297
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC2: TsEdit
          Left = 313
          Top = 321
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC3: TsEdit
          Left = 313
          Top = 345
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC4: TsEdit
          Left = 313
          Top = 369
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC5: TsEdit
          Left = 313
          Top = 393
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC6: TsEdit
          Left = 346
          Top = 417
          Width = 254
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel121: TsPanel
          Left = 313
          Top = 417
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'TEL'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object sPanel122: TsPanel
          Left = 9
          Top = 273
          Width = 32
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '50'
          Color = clGray
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object sPanel123: TsPanel
          Left = 42
          Top = 273
          Width = 149
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44060#49444#51032#47280#51064' '#51221#48372' '#48320#44221
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object edt_AMD_APPLIC1: TsEdit
          Left = 9
          Top = 297
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 26
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AMD_APPLIC2: TsEdit
          Left = 9
          Top = 321
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 27
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AMD_APPLIC3: TsEdit
          Left = 9
          Top = 345
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 28
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AMD_APPLIC4: TsEdit
          Left = 9
          Top = 369
          Width = 287
          Height = 23
          Color = clWhite
          Enabled = False
          TabOrder = 29
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTab_PAGEList: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sDBGrid3: TsDBGrid
        Left = 0
        Top = 32
        Width = 720
        Height = 531
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnTitleClick = sDBGrid1TitleClick
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MAINT_NO_TXT'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 168
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_NO1'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221
            Width = 35
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_DATE'
            Title.Alignment = taCenter
            Title.Caption = #51312#44148#48320#44221#51068#51088
            Width = 78
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51068#51088
            Width = 78
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_NO'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#48264#54840
            Width = 138
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #51204#47928#48156#49888#51008#54665
            Width = 258
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK1'
            Title.Caption = #51204#47928#49688#49888#51008#54665
            Width = 233
            Visible = True
          end>
      end
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 720
        Height = 32
        Align = alTop
        
        TabOrder = 1
        object sSpeedButton12: TsSpeedButton
          Left = 230
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit3: TsMaskEdit
          Tag = -1
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 0
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sMaskEdit4: TsMaskEdit
          Tag = -1
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 1
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 469
          Top = 5
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
        end
        object sEdit1: TsEdit
          Tag = -1
          Left = 297
          Top = 5
          Width = 171
          Height = 23
          TabOrder = 3
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 40
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT A.MAINT_NO+'#39'-'#39'+CONVERT(varchar,B.AMD_NO) as MAINT_NO_TXT,'
      
        'A.MAINT_NO, B.AMD_NO, AMD_NO1, CHK1, CHK2, CHK3, MESSAGE1, MESSA' +
        'GE2, USER_ID, DATEE, AMD_DATE, APP_DATE, APP_NO,'
      'AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP_BANK5,'
      
        'AD_BANK, AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK4, AD_BANK5, ADDIN' +
        'FO, ADDINFO_1,'
      
        'CD_NO, RCV_REF, ISS_BANK, ISS_BANK1, ISS_BANK2, ISS_BANK3, ISS_B' +
        'ANK4, ISS_BANK5, ISS_ACCNT, ISS_DATE,'
      
        'EX_DATE, EX_PLACE, BENEFC1, BENEFC2, BENEFC3, BENEFC4, BENEFC5, ' +
        'INCD_CUR, INCD_AMT, DECD_CUR, DECD_AMT, NWCD_CUR, NWCD_AMT, CD_P' +
        'ERP, CD_PERM,'
      
        'CD_MAX, AA_CV1, AA_CV2, AA_CV3, AA_CV4, BENEFC6, IBANK_REF, PRNO' +
        ', GOODS_DESC, GOODS_DESC_1, DOC_DESC, DOC_DESC_1, ADD_DESC, ADD_' +
        'DESC_1, SPECIAL_DESC, SPECIAL_DESC_1,'
      'INST_DESC, INST_DESC_1,'
      
        'LOAD_ON, FOR_TRAN, LST_DATE, SHIP_PD, SHIP_PD1, SHIP_PD2, SHIP_P' +
        'D3, SHIP_PD4, SHIP_PD5, SHIP_PD6,'
      
        'NARRAT, NARRAT_1, SR_INFO1, SR_INFO2, SR_INFO3, SR_INFO4, SR_INF' +
        'O5, SR_INFO6, EX_NAME1, EX_NAME2, EX_NAME3, EX_ADDR1, EX_ADDR2,'
      
        'BFCD_AMT, BFCD_CUR, SUNJUCK_PORT, DOCHACK_PORT, PURP_MSG, PURP_M' +
        'SG_NM, CAN_REQ, CON_INST, DOC_TYPE, CHARGE, CHARGE_NUM1, CHARGE_' +
        'NUM2, CHARGE_NUM3, CHARGE_NUM4, CHARGE_NUM5, CHARGE_NUM6,'
      
        'AMD_CHARGE, AMD_CHARGE_NUM1, AMD_CHARGE_NUM2, AMD_CHARGE_NUM3, A' +
        'MD_CHARGE_NUM4, AMD_CHARGE_NUM5, AMD_CHARGE_NUM6,'
      
        'TSHIP, PSHIP, DRAFT1, DRAFT2, DRAFT3, MIX_PAY1, MIX_PAY2, MIX_PA' +
        'Y3, MIX_PAY4, DEF_PAY1, DEF_PAY2, DEF_PAY3, DEF_PAY4, APPLICABLE' +
        '_RULES_1, APPLICABLE_RULES_2,'
      
        'AVAIL, AVAIL1, AVAIL2, AVAIL3, AVAIL4, AV_ACCNT, DRAWEE, DRAWEE1' +
        ', DRAWEE2, DRAWEE3, DRAWEE4, CO_BANK, CO_BANK1, CO_BANK2, CO_BAN' +
        'K3, CO_BANK4,'
      
        'REI_BANK, REI_BANK1, REI_BANK2, REI_BANK3, REI_BANK4, AVT_BANK, ' +
        'AVT_BANK1, AVT_BANK2, AVT_BANK3, AVT_BANK4, AMD_APPLIC1, AMD_APP' +
        'LIC2, AMD_APPLIC3,'
      
        'AMD_APPLIC4, PERIOD, PERIOD_TXT, NBANK_ISS, NBANK_ISS1, NBANK_IS' +
        'S2, NBANK_ISS3, NBANK_ISS4'
      'FROM ADV707 A '
      
        'INNER JOIN ADV7072 B ON A.MAINT_NO = B.MAINT_NO AND A.AMD_NO = B' +
        '.AMD_NO'
      
        'LEFT JOIN(SELECT CODE, [NAME] as PURP_MSG_NM FROM CODE2NDD WHERE' +
        ' Prefix = '#39'INP_4401'#39') INP_4401 ON B.PURP_MSG = INP_4401.CODE')
    Left = 16
    Top = 232
    object qryListMAINT_NO_TXT: TStringField
      FieldName = 'MAINT_NO_TXT'
      ReadOnly = True
      Size = 66
    end
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListAMD_NO1: TIntegerField
      FieldName = 'AMD_NO1'
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK5: TStringField
      FieldName = 'AD_BANK5'
      Size = 35
    end
    object qryListADDINFO: TStringField
      FieldName = 'ADDINFO'
      Size = 1
    end
    object qryListADDINFO_1: TMemoField
      FieldName = 'ADDINFO_1'
      BlobType = ftMemo
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListRCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryListISS_BANK: TStringField
      FieldName = 'ISS_BANK'
      Size = 11
    end
    object qryListISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryListISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryListISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryListISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryListISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryListISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 70
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListBENEFC6: TStringField
      FieldName = 'BENEFC6'
      Size = 35
    end
    object qryListIBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListGOODS_DESC: TStringField
      FieldName = 'GOODS_DESC'
      Size = 1
    end
    object qryListGOODS_DESC_1: TMemoField
      FieldName = 'GOODS_DESC_1'
      BlobType = ftMemo
    end
    object qryListDOC_DESC: TStringField
      FieldName = 'DOC_DESC'
      Size = 1
    end
    object qryListDOC_DESC_1: TMemoField
      FieldName = 'DOC_DESC_1'
      BlobType = ftMemo
    end
    object qryListADD_DESC: TStringField
      FieldName = 'ADD_DESC'
      Size = 1
    end
    object qryListADD_DESC_1: TMemoField
      FieldName = 'ADD_DESC_1'
      BlobType = ftMemo
    end
    object qryListSPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Size = 1
    end
    object qryListSPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      BlobType = ftMemo
    end
    object qryListINST_DESC: TStringField
      FieldName = 'INST_DESC'
      Size = 1
    end
    object qryListINST_DESC_1: TMemoField
      FieldName = 'INST_DESC_1'
      BlobType = ftMemo
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TStringField
      FieldName = 'SHIP_PD'
      Size = 1
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListSR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryListSR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryListSR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryListSR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryListSR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryListSR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListPURP_MSG: TStringField
      FieldName = 'PURP_MSG'
      Size = 35
    end
    object qryListPURP_MSG_NM: TStringField
      FieldName = 'PURP_MSG_NM'
      Size = 100
    end
    object qryListCAN_REQ: TStringField
      FieldName = 'CAN_REQ'
      Size = 35
    end
    object qryListCON_INST: TStringField
      FieldName = 'CON_INST'
      Size = 35
    end
    object qryListDOC_TYPE: TStringField
      FieldName = 'DOC_TYPE'
      Size = 35
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListCHARGE_NUM1: TStringField
      FieldName = 'CHARGE_NUM1'
      Size = 35
    end
    object qryListCHARGE_NUM2: TStringField
      FieldName = 'CHARGE_NUM2'
      Size = 35
    end
    object qryListCHARGE_NUM3: TStringField
      FieldName = 'CHARGE_NUM3'
      Size = 35
    end
    object qryListCHARGE_NUM4: TStringField
      FieldName = 'CHARGE_NUM4'
      Size = 35
    end
    object qryListCHARGE_NUM5: TStringField
      FieldName = 'CHARGE_NUM5'
      Size = 35
    end
    object qryListCHARGE_NUM6: TStringField
      FieldName = 'CHARGE_NUM6'
      Size = 35
    end
    object qryListAMD_CHARGE: TStringField
      FieldName = 'AMD_CHARGE'
      Size = 3
    end
    object qryListAMD_CHARGE_NUM1: TStringField
      FieldName = 'AMD_CHARGE_NUM1'
      Size = 35
    end
    object qryListAMD_CHARGE_NUM2: TStringField
      FieldName = 'AMD_CHARGE_NUM2'
      Size = 35
    end
    object qryListAMD_CHARGE_NUM3: TStringField
      FieldName = 'AMD_CHARGE_NUM3'
      Size = 35
    end
    object qryListAMD_CHARGE_NUM4: TStringField
      FieldName = 'AMD_CHARGE_NUM4'
      Size = 35
    end
    object qryListAMD_CHARGE_NUM5: TStringField
      FieldName = 'AMD_CHARGE_NUM5'
      Size = 35
    end
    object qryListAMD_CHARGE_NUM6: TStringField
      FieldName = 'AMD_CHARGE_NUM6'
      Size = 35
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryListAVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryListAVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryListAVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryListAVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryListAVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryListAV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 17
    end
    object qryListDRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryListDRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryListDRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryListDRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryListDRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryListCO_BANK: TStringField
      FieldName = 'CO_BANK'
      Size = 11
    end
    object qryListCO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Size = 35
    end
    object qryListCO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Size = 35
    end
    object qryListCO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Size = 35
    end
    object qryListCO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Size = 35
    end
    object qryListREI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryListREI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryListREI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryListREI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryListREI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryListAVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryListAVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryListAVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryListAVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryListAVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryListAMD_APPLIC1: TStringField
      FieldName = 'AMD_APPLIC1'
      Size = 35
    end
    object qryListAMD_APPLIC2: TStringField
      FieldName = 'AMD_APPLIC2'
      Size = 35
    end
    object qryListAMD_APPLIC3: TStringField
      FieldName = 'AMD_APPLIC3'
      Size = 35
    end
    object qryListAMD_APPLIC4: TStringField
      FieldName = 'AMD_APPLIC4'
      Size = 35
    end
    object qryListPERIOD: TIntegerField
      FieldName = 'PERIOD'
    end
    object qryListPERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryListNBANK_ISS: TStringField
      FieldName = 'NBANK_ISS'
      Size = 11
    end
    object qryListNBANK_ISS1: TStringField
      FieldName = 'NBANK_ISS1'
      Size = 35
    end
    object qryListNBANK_ISS2: TStringField
      FieldName = 'NBANK_ISS2'
      Size = 35
    end
    object qryListNBANK_ISS3: TStringField
      FieldName = 'NBANK_ISS3'
      Size = 35
    end
    object qryListNBANK_ISS4: TStringField
      FieldName = 'NBANK_ISS4'
      Size = 35
    end
  end
  object dsList: TDataSource
    AutoEdit = False
    DataSet = qryList
    Left = 48
    Top = 232
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = A4
    Left = 16
    Top = 264
  end
end
