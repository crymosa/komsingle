inherited UI_SPCNTC_NEW_frm: TUI_SPCNTC_NEW_frm
  Left = 625
  Top = 184
  BorderWidth = 4
  Caption = '[SPCNTC] '#54032#47588#45824#44552#52628#49900'('#47588#51077')'#52376#47532' '#51025#45813#49436
  ClientHeight = 673
  ClientWidth = 1113
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1113
    Height = 41
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1113
      41)
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 184
      Height = 17
      Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#52376#47532' '#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'SPCNTC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 200
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1039
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 213
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 279
      Top = 2
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 2
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 377
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 375
      Height = 574
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 208
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 84
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MESSAGE1'
          Title.Alignment = taCenter
          Title.Caption = #53685#48372
          Width = 49
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 375
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 230
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 57
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 0
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 57
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 150
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 2
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 241
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 377
    Top = 41
    Width = 736
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 734
      Height = 575
      ActivePage = sTabSheet1
      Align = alClient
      TabHeight = 26
      TabOrder = 1
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 726
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          DesignSize = (
            726
            539)
          object Shape2: TShape
            Left = 9
            Top = 207
            Width = 709
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Enabled = False
            Pen.Color = 14540252
          end
          object Shape1: TShape
            Left = 9
            Top = 145
            Width = 709
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Enabled = False
            Pen.Color = 14540252
          end
          object Shape3: TShape
            Left = 9
            Top = 367
            Width = 709
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object sImage1: TsImage
            Left = 576
            Top = 24
            Width = 105
            Height = 105
            Enabled = False
            Picture.Data = {07544269746D617000000000}
            Images = DMICON.System24
            SkinData.SkinSection = 'CHECKBOX'
          end
          object sDBEdit1: TsDBEdit
            Left = 146
            Top = 32
            Width = 41
            Height = 23
            Color = clWhite
            DataField = 'BGM_GUBUN'
            DataSource = dsList
            Enabled = False
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49888#47928#49436#51333#47448
          end
          object sDBEdit2: TsDBEdit
            Left = 188
            Top = 32
            Width = 341
            Height = 23
            Color = clBtnFace
            DataField = 'BGM_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 1
            SkinData.CustomColor = True
          end
          object sDBEdit9: TsDBEdit
            Left = 146
            Top = 88
            Width = 225
            Height = 23
            Color = clWhite
            DataField = 'LC_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 2
            BoundLabel.Active = True
            BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
          end
          object sDBEdit10: TsDBEdit
            Left = 472
            Top = 240
            Width = 33
            Height = 23
            Color = 12775866
            DataField = 'CP_AMTC'
            DataSource = dsList
            Enabled = False
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50808#54868#44552#50529
          end
          object sDBEdit11: TsDBEdit
            Left = 506
            Top = 240
            Width = 167
            Height = 23
            Color = clWhite
            DataField = 'CP_AMTU'
            DataSource = dsList
            Enabled = False
            TabOrder = 4
            SkinData.CustomColor = True
          end
          object sDBEdit13: TsDBEdit
            Left = 146
            Top = 8
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'NT_DATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 5
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51068#51088
          end
          object sDBEdit14: TsDBEdit
            Left = 472
            Top = 312
            Width = 33
            Height = 23
            Color = 12775866
            DataField = 'CP_TOTCHARGEC'
            DataSource = dsList
            Enabled = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49688#47308#54633#44228
          end
          object sDBEdit15: TsDBEdit
            Left = 506
            Top = 312
            Width = 167
            Height = 23
            Color = clWhite
            DataField = 'CP_TOTCHARGE'
            DataSource = dsList
            Enabled = False
            TabOrder = 7
            SkinData.CustomColor = True
          end
          object sDBEdit17: TsDBEdit
            Left = 344
            Top = 8
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'CP_DATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 8
            BoundLabel.Active = True
            BoundLabel.Caption = #47588#51077#51068#51088
          end
          object sDBEdit3: TsDBEdit
            Left = 146
            Top = 64
            Width = 225
            Height = 23
            Color = clWhite
            DataField = 'DM_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51032#47280#49436' '#51204#51088#47928#49436#48264#54840
          end
          object sDBEdit6: TsDBEdit
            Left = 146
            Top = 113
            Width = 225
            Height = 23
            Color = clWhite
            DataField = 'CP_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#52397#48264#54840
          end
          object sDBEdit4: TsDBEdit
            Left = 146
            Top = 154
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'APP_SNAME'
            DataSource = dsList
            Enabled = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49888#51064
          end
          object sDBEdit7: TsDBEdit
            Left = 146
            Top = 178
            Width = 135
            Height = 23
            Color = clWhite
            DataField = 'APP_NAME'
            DataSource = dsList
            Enabled = False
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
          end
          object sDBEdit5: TsDBEdit
            Left = 483
            Top = 154
            Width = 135
            Height = 23
            Color = clWhite
            DataField = 'APP_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 13
            BoundLabel.Active = True
            BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          end
          object sDBEdit8: TsDBEdit
            Left = 146
            Top = 216
            Width = 103
            Height = 23
            Color = clWhite
            DataField = 'CP_BANK'
            DataSource = dsList
            Enabled = False
            TabOrder = 14
            BoundLabel.Active = True
            BoundLabel.Caption = #52628#49900'('#47588#51077')'#51008#54665#53076#46300
          end
          object sDBEdit16: TsDBEdit
            Left = 146
            Top = 240
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'CP_BANKNAME'
            DataSource = dsList
            Enabled = False
            TabOrder = 15
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#47749
          end
          object sDBEdit18: TsDBEdit
            Left = 472
            Top = 264
            Width = 201
            Height = 23
            Color = clWhite
            DataField = 'CP_AMT'
            DataSource = dsList
            Enabled = False
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868#44552#50529
          end
          object sDBEdit19: TsDBEdit
            Left = 472
            Top = 288
            Width = 33
            Height = 23
            Color = 12775866
            DataField = 'CP_TOTAMTC'
            DataSource = dsList
            Enabled = False
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #52572#51333#51648#44553#50529
          end
          object sDBEdit20: TsDBEdit
            Left = 506
            Top = 288
            Width = 167
            Height = 23
            Color = clWhite
            DataField = 'CP_TOTAMT'
            DataSource = dsList
            Enabled = False
            TabOrder = 18
            SkinData.CustomColor = True
          end
          object sDBEdit21: TsDBEdit
            Left = 146
            Top = 264
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'CP_BANKBU'
            DataSource = dsList
            Enabled = False
            TabOrder = 19
          end
          object sDBEdit22: TsDBEdit
            Left = 146
            Top = 288
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'CP_BANKELEC'
            DataSource = dsList
            Enabled = False
            TabOrder = 20
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
          end
          object sDBEdit23: TsDBEdit
            Left = 146
            Top = 312
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'CP_ACCOUNTNO'
            DataSource = dsList
            Enabled = False
            TabOrder = 21
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
          end
          object sDBEdit12: TsDBEdit
            Left = 146
            Top = 336
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'CP_NAME'
            DataSource = dsList
            Enabled = False
            TabOrder = 22
            BoundLabel.Active = True
            BoundLabel.Caption = #50696#44552#51452
          end
          object sPageControl2: TsPageControl
            Left = 47
            Top = 381
            Width = 631
            Height = 149
            ActivePage = sTabSheet2
            Images = DMICON.System18
            TabHeight = 28
            TabOrder = 23
            TabPadding = 10
            object sTabSheet2: TsTabSheet
              Caption = #47932#54408#49688#47161#51613#47749#49436'('#51064#49688#51613')'#48264#54840
              ImageIndex = -1
              object lst_RCNO: TsListBox
                Left = 0
                Top = 0
                Width = 623
                Height = 111
                Align = alClient
                Enabled = False
                ItemHeight = 18
                TabOrder = 0
              end
            end
            object sTabSheet3: TsTabSheet
              Caption = #49464#44552#44228#49328#49436#48264#54840
              ImageIndex = -1
              object lst_FINNO: TsListBox
                Left = 0
                Top = 0
                Width = 623
                Height = 111
                Align = alClient
                Enabled = False
                ItemHeight = 18
                TabOrder = 0
              end
            end
            object sTabSheet5: TsTabSheet
              Caption = #44592#53440#49324#54637
              ImageIndex = -1
              object sDBMemo1: TsDBMemo
                Left = 0
                Top = 0
                Width = 623
                Height = 111
                Align = alClient
                Color = clWhite
                DataField = 'REMAKR1'
                Enabled = False
                TabOrder = 0
              end
            end
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        TabVisible = False
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 726
          Height = 507
          Align = alClient
          Color = clGray
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = 12582911
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_SQ1'
              Title.Alignment = taCenter
              Title.Caption = #50976#54952#44592#44036
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_DATE'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_NO'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#48264#54840
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'UD_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49688#50836#51088
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TQTY'
              Title.Alignment = taCenter
              Title.Caption = #52509#49688#47049
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TQTYCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TAMT'
              Title.Alignment = taCenter
              Title.Caption = #52509#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TAMTCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51088
              Width = 50
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 726
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 1
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
          end
        end
      end
      object sTabSheet6: TsTabSheet
        Caption = #49464#44552#44228#49328#49436
        object sDBGrid2: TsDBGrid
          Left = 0
          Top = 0
          Width = 726
          Height = 163
          Align = alClient
          Color = clWhite
          Ctl3D = True
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHARGE_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#49328#49436#48264#54840
              Width = 176
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHARGE_TYPE'
              Title.Alignment = taCenter
              Title.Caption = #50976#54805
              Width = 42
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'CHARGE_RATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#50836#50984
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAS_AMT'
              Title.Alignment = taCenter
              Title.Caption = #45824#49345#44552#50529
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BAS_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHA_AMT'
              Title.Alignment = taCenter
              Title.Caption = #49328#52636#44552#50529
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHA_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CUX_RATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#54872#50984
              Width = 65
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SES_DAY'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#51068#49688
              Width = 52
              Visible = True
            end>
        end
        object sPanel1: TsPanel
          Left = 0
          Top = 165
          Width = 726
          Height = 374
          Align = alBottom
          
          TabOrder = 1
          DesignSize = (
            726
            374)
          object sSpeedButton2: TsSpeedButton
            Left = 314
            Top = 9
            Width = 17
            Height = 356
            Anchors = [akLeft, akTop, akBottom]
            Enabled = False
            ButtonStyle = tbsDivider
          end
          object sDBEdit24: TsDBEdit
            Left = 92
            Top = 16
            Width = 37
            Height = 23
            AutoSelect = False
            Color = clBtnFace
            DataField = 'SEQ'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49692#48264
          end
          object sDBEdit25: TsDBEdit
            Left = 92
            Top = 40
            Width = 205
            Height = 23
            Color = clWhite
            DataField = 'CHARGE_NO'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#49328#49436#48264#54840
          end
          object sDBEdit26: TsDBEdit
            Left = 92
            Top = 64
            Width = 37
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'CHARGE_TYPE'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49688#47308#50976#54805
          end
          object sDBEdit27: TsDBEdit
            Left = 130
            Top = 64
            Width = 167
            Height = 23
            AutoSelect = False
            Color = clBtnFace
            DataField = 'CHARGE_NM'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 3
            SkinData.CustomColor = True
          end
          object sDBEdit28: TsDBEdit
            Left = 92
            Top = 96
            Width = 85
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'CHARGE_RATE'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51201#50857#50836#50984
          end
          object sDBEdit29: TsDBEdit
            Left = 260
            Top = 96
            Width = 37
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'SES_DAY'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51201#50857#51068#49688
          end
          object sDBEdit30: TsDBEdit
            Left = 92
            Top = 120
            Width = 205
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'RES_DT'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51201#50857#44592#44036
          end
          object sDBEdit31: TsDBEdit
            Left = 404
            Top = 16
            Width = 37
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'BAS_AMTC'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#49345#44552#50529
          end
          object sDBEdit32: TsDBEdit
            Left = 442
            Top = 16
            Width = 167
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'BAS_AMT'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 8
            SkinData.CustomColor = True
          end
          object sDBEdit33: TsDBEdit
            Left = 404
            Top = 40
            Width = 37
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'CHA_AMTC'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#49345#44552#50529
          end
          object sDBEdit34: TsDBEdit
            Left = 442
            Top = 40
            Width = 167
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'CHA_AMT'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
          end
          object sDBEdit35: TsDBEdit
            Left = 524
            Top = 64
            Width = 85
            Height = 23
            AutoSelect = False
            Color = clWhite
            DataField = 'CUX_RATE'
            DataSource = dsDetail
            Enabled = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51201#50857#54872#50984
          end
        end
        object sPanel2: TsPanel
          Left = 0
          Top = 163
          Width = 726
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          
          TabOrder = 2
        end
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 734
      Height = 55
      Align = alTop
      
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 334
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 265
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_type: TsComboBox
        Left = 400
        Top = 28
        Width = 211
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 2
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 272
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
      object com_func: TsComboBox
        Left = 400
        Top = 4
        Width = 211
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 4
        TabStop = False
        Items.Strings = (
          '1: '#52628#49900'('#47588#51077') '#52712#49548#53685#48372
          '7: '#52628#49900'('#47588#51077') '#48512#46020#53685#48372
          '9: '#52628#49900'('#47588#51077') '#49849#51064#53685#48372)
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 136
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, CHARGE_NO, CHARGE_TYPE, NAME as CHARGE_NM, CHA' +
        'RGE_RATE, BAS_AMT, BAS_AMTC, CHA_AMT, CHA_AMTC, CUX_RATE, SES_DA' +
        'Y,SES_SDATE,SES_EDATE,'
      '       CASE'
      
        '       WHEN (RTRIM(LTRIM(SES_SDATE)) != '#39#39') OR (RTRIM(LTRIM(SES_' +
        'EDATE)) != '#39#39') THEN SES_SDATE+'#39' ~ '#39'+SES_EDATE'
      '       ELSE '#39#39
      '       END RES_DT'
      
        'FROM SPCNTC_D LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE P' +
        'refix = '#39'5189FINBIL'#39') CHARGE_CD ON CHARGE_TYPE = CHARGE_CD.CODE'
      'WHERE KEYY = :KEYY')
    Left = 48
    Top = 192
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailCHARGE_NO: TStringField
      FieldName = 'CHARGE_NO'
      Size = 35
    end
    object qryDetailCHARGE_TYPE: TStringField
      FieldName = 'CHARGE_TYPE'
      Size = 3
    end
    object qryDetailCHARGE_RATE: TStringField
      FieldName = 'CHARGE_RATE'
      Size = 8
    end
    object qryDetailBAS_AMT: TBCDField
      FieldName = 'BAS_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryDetailBAS_AMTC: TStringField
      FieldName = 'BAS_AMTC'
      Size = 3
    end
    object qryDetailCHA_AMT: TBCDField
      FieldName = 'CHA_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryDetailCHA_AMTC: TStringField
      FieldName = 'CHA_AMTC'
      Size = 3
    end
    object qryDetailCUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      DisplayFormat = '#,0.##'
      Precision = 18
    end
    object qryDetailSES_DAY: TStringField
      FieldName = 'SES_DAY'
      Size = 3
    end
    object qryDetailRES_DT: TStringField
      FieldName = 'RES_DT'
      ReadOnly = True
      Size = 19
    end
    object qryDetailCHARGE_NM: TStringField
      FieldName = 'CHARGE_NM'
      Size = 100
    end
    object qryDetailSES_SDATE: TStringField
      FieldName = 'SES_SDATE'
      Size = 8
    end
    object qryDetailSES_EDATE: TStringField
      FieldName = 'SES_EDATE'
      Size = 8
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 80
    Top = 192
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181130'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, BGM_GUBUN, NAME as BGM_NM, MESS' +
        'AGE1, MESSAGE2, NT_DATE, CP_DATE, DM_NO, LC_NO, CP_ACCOUNTNO, CP' +
        '_NAME1 + CP_NAME2 as CP_NAME, CP_BANK, CP_BANKNAME, CP_BANKBU, A' +
        'PP_SNAME, [APP_NAME],'
      
        '       CP_BANKELEC, CP_AMTU, CP_AMTC, CP_AMT, CP_TOTAMT, CP_TOTA' +
        'MTC, CP_TOTCHARGE, CP_TOTCHARGEC,'
      
        '       CP_FTX, CP_FTX1, CHK1, CHK2, CHK3, BSN_HSCODE, APP_NO, CP' +
        '_NO, RC_NO, RC_NO2, RC_NO3, RC_NO4, RC_NO5, RC_NO6, RC_NO7, RC_N' +
        'O8, FIN_NO, FIN_NO2, FIN_NO3, FIN_NO4, FIN_NO5, FIN_NO6, FIN_NO7'
      'FROM SPCNTC_H'
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'SPCNT' +
        'C_CD'#39') CD ON SPCNTC_H.BGM_GUBUN = CD.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY MAINT_NO DESC, DATEE DESC')
    Left = 48
    Top = 160
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      Size = 3
    end
    object qryListBGM_NM: TStringField
      FieldName = 'BGM_NM'
      Size = 100
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      OnGetText = qryListMESSAGE1GetText
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListNT_DATE: TStringField
      FieldName = 'NT_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListCP_DATE: TStringField
      FieldName = 'CP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListDM_NO: TStringField
      FieldName = 'DM_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListCP_ACCOUNTNO: TStringField
      FieldName = 'CP_ACCOUNTNO'
      Size = 17
    end
    object qryListCP_NAME: TStringField
      FieldName = 'CP_NAME'
      ReadOnly = True
      Size = 70
    end
    object qryListCP_BANK: TStringField
      FieldName = 'CP_BANK'
      Size = 11
    end
    object qryListCP_BANKNAME: TStringField
      FieldName = 'CP_BANKNAME'
      Size = 70
    end
    object qryListCP_BANKBU: TStringField
      FieldName = 'CP_BANKBU'
      Size = 70
    end
    object qryListAPP_SNAME: TStringField
      FieldName = 'APP_SNAME'
      Size = 35
    end
    object qryListAPP_NAME: TStringField
      FieldName = 'APP_NAME'
      Size = 35
    end
    object qryListCP_BANKELEC: TStringField
      FieldName = 'CP_BANKELEC'
      Size = 35
    end
    object qryListCP_AMTU: TBCDField
      FieldName = 'CP_AMTU'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListCP_AMTC: TStringField
      FieldName = 'CP_AMTC'
      Size = 3
    end
    object qryListCP_AMT: TBCDField
      FieldName = 'CP_AMT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListCP_TOTAMT: TBCDField
      FieldName = 'CP_TOTAMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListCP_TOTAMTC: TStringField
      FieldName = 'CP_TOTAMTC'
      Size = 3
    end
    object qryListCP_TOTCHARGE: TBCDField
      FieldName = 'CP_TOTCHARGE'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListCP_TOTCHARGEC: TStringField
      FieldName = 'CP_TOTCHARGEC'
      Size = 3
    end
    object qryListCP_FTX: TStringField
      FieldName = 'CP_FTX'
      Size = 3
    end
    object qryListCP_FTX1: TMemoField
      FieldName = 'CP_FTX1'
      BlobType = ftMemo
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 10
    end
    object qryListCP_NO: TStringField
      FieldName = 'CP_NO'
      Size = 35
    end
    object qryListRC_NO: TStringField
      FieldName = 'RC_NO'
      Size = 35
    end
    object qryListRC_NO2: TStringField
      FieldName = 'RC_NO2'
      Size = 35
    end
    object qryListRC_NO3: TStringField
      FieldName = 'RC_NO3'
      Size = 35
    end
    object qryListRC_NO4: TStringField
      FieldName = 'RC_NO4'
      Size = 35
    end
    object qryListRC_NO5: TStringField
      FieldName = 'RC_NO5'
      Size = 35
    end
    object qryListRC_NO6: TStringField
      FieldName = 'RC_NO6'
      Size = 35
    end
    object qryListRC_NO7: TStringField
      FieldName = 'RC_NO7'
      Size = 35
    end
    object qryListRC_NO8: TStringField
      FieldName = 'RC_NO8'
      Size = 35
    end
    object qryListFIN_NO: TStringField
      FieldName = 'FIN_NO'
      Size = 35
    end
    object qryListFIN_NO2: TStringField
      FieldName = 'FIN_NO2'
      Size = 35
    end
    object qryListFIN_NO3: TStringField
      FieldName = 'FIN_NO3'
      Size = 35
    end
    object qryListFIN_NO4: TStringField
      FieldName = 'FIN_NO4'
      Size = 35
    end
    object qryListFIN_NO5: TStringField
      FieldName = 'FIN_NO5'
      Size = 35
    end
    object qryListFIN_NO6: TStringField
      FieldName = 'FIN_NO6'
      Size = 35
    end
    object qryListFIN_NO7: TStringField
      FieldName = 'FIN_NO7'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 160
  end
end
