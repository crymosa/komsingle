unit UI_LOCAPP_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sComboBox, DBCtrls, sDBMemo,
  sDBEdit, sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sCheckBox, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, DB,
  ADODB, StrUtils, Clipbrd, DateUtils, CodeDialogParent, QuickRpt, QRCtrls;

type
  TDOC_HEADER = record
    Header : string;
    Bank : String;
  end;
  
  TUI_LOCAPP_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sCheckBox1: TsCheckBox;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    edt_wcd: TsEdit;
    edt_wsangho: TsEdit;
    edt_wceo: TsEdit;
    edt_waddr1: TsEdit;
    edt_waddr2: TsEdit;
    edt_waddr3: TsEdit;
    edt_rcd: TsEdit;
    edt_rsaupno: TsEdit;
    edt_rsangho: TsEdit;
    edt_rceo: TsEdit;
    edt_remail: TsEdit;
    edt_raddr1: TsEdit;
    edt_raddr2: TsEdit;
    edt_raddr3: TsEdit;
    edt_iden1: TsEdit;
    edt_iden2: TsEdit;
    edt_doc1: TsEdit;
    edt_doc2: TsEdit;
    edt_doc3: TsEdit;
    edt_doc4: TsEdit;
    edt_doc5: TsEdit;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    edt_wsaupno: TsEdit;
    edt_cbank: TsEdit;
    edt_cbanknm: TsEdit;
    sSpeedButton10: TsSpeedButton;
    edt_cbrunch: TsEdit;
    edt_cType: TsEdit;
    sEdit8: TsEdit;
    edt_LOCAMT_UNIT: TsEdit;
    edt_perr: TsEdit;
    cur_LOCAMT: TsCurrencyEdit;
    edt_merr: TsEdit;
    edt_cyong: TsEdit;
    edt_lcno: TsEdit;
    edt_doc6: TsEdit;
    edt_doc7: TsEdit;
    edt_doc8: TsEdit;
    edt_doc9: TsEdit;
    cur_AttachDoc1: TsCurrencyEdit;
    cur_AttachDoc4: TsCurrencyEdit;
    cur_AttachDoc5: TsCurrencyEdit;
    cur_AttachDoc2: TsCurrencyEdit;
    cur_AttachDoc3: TsCurrencyEdit;
    sPanel11: TsPanel;
    cur_chasu: TsCurrencyEdit;
    sLabel3: TsLabel;
    msk_CrtDT: TsMaskEdit;
    msk_IndoDt: TsMaskEdit;
    msk_ExpiryDT: TsMaskEdit;
    edt_isPart: TsEdit;
    sEdit20: TsEdit;
    comDocPeriod: TsComboBox;
    sLabel4: TsLabel;
    sPanel2: TsPanel;
    msk_hscd: TsMaskEdit;
    memo_goods: TsMemo;
    memo_etcdoc: TsMemo;
    memo_etcinfo: TsMemo;
    edt_OriginType: TsEdit;
    sEdit22: TsEdit;
    edt_OriginLCNO: TsEdit;
    edt_OriginAmtUnit: TsEdit;
    edt_OriginAmt: TsCurrencyEdit;
    msk_OriginShipDT: TsMaskEdit;
    msk_OriginVaildDT: TsMaskEdit;
    edt_AmtType: TsEdit;
    sEdit26: TsEdit;
    edt_OriginCD: TsEdit;
    edt_OriginSangho: TsEdit;
    edt_OriginCeo: TsEdit;
    edt_OriginAddr1: TsEdit;
    edt_OriginExportArea: TsEdit;
    sEdit32: TsEdit;
    edt_Originbank1: TsEdit;
    edt_Originbank2: TsEdit;
    memo_ExportGoods: TsMemo;
    sPanel1: TsPanel;
    edt_sign1: TsEdit;
    edt_sign2: TsEdit;
    edt_sign3: TsEdit;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMAINT_NO2: TStringField;
    qryListDATEE: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListBUSINESSNAME: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListAPP_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListGOODDES: TStringField;
    qryListREMARK: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListDOC_DTL: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListDESTNAME: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListEXGOOD: TStringField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListLCNO: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListGOODDES1: TStringField;
    qryListREMARK1: TStringField;
    qryListDOC_ETC1: TStringField;
    qryListEXGOOD1: TStringField;
    dsList: TDataSource;
    edt_cyongnm: TsEdit;
    sPanel8: TsPanel;
    sSpeedButton2: TsSpeedButton;
    qryReady: TADOQuery;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO_Header: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    edt_MAINT_NO_Serial: TsEdit;
    edt_MAINT_NO_Bank: TsEdit;
    comBank: TsComboBox;
    sLabel5: TsLabel;
    QRShape1: TQRShape;
    sSpeedButton3: TsSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnNewClick(Sender: TObject);
    procedure edt_wcdDblClick(Sender: TObject);
    procedure edt_cbankDblClick(Sender: TObject);
    procedure edt_cyongChange(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnTempClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edt_MAINT_NO_BankEnter(Sender: TObject);
    procedure comBankSelect(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edt_cbankExit(Sender: TObject);
    procedure edt_cTypeChange(Sender: TObject);
    procedure sDBGrid3DblClick(Sender: TObject);
  private
    uDialog : TCodeDialogParent_frm;
    function CHECK_VALIDITY:String;
    procedure ReadData;
    procedure CopyData;
    procedure ButtonEnable;
    procedure OriginSection;
    function getMaintHeader:TDOC_HEADER;
    procedure NewDoc;
    function CopyFromDOMOFB:Boolean;
    function DataProcess(nType: integer): string;
    procedure ReadyDocument(DocNo: String);
    procedure setBank;
    { Private declarations }
  protected
    procedure ReadList(SortField: TColumn=nil); virtual;  
  public
    { Public declarations }
  end;

var
  UI_LOCAPP_NEW_frm: TUI_LOCAPP_NEW_frm;

implementation

uses
  MSSQL, VarDefine, TypeDefine, CodeContents, AutoNo, CD_CUST, CD_UNIT, CD_BANK, CD_LOCTYPE, CD_AMT_UNIT, CD_LOCBASIC, MessageDefine, dlg_CreateDocumentChoice,
  dlg_CopyLOCAPPfromDOMOFB, dlg_SelectCopyDocument, dlg_CopyLOCAPPfromLOCAPP,
  Dlg_ErrorMessage, SQLCreator, LOCAPP_PRINT, Preview, Dlg_RecvSelect,
  DocumentSend, CreateDocuments, CD_NK1001, CD_NK4277, CD_NATION, CD_PSHIP;

{$R *.dfm}

{ TUI_LOCAPP_NEW_frm }

procedure TUI_LOCAPP_NEW_frm.ReadList(SortField : TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAPP.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    
    Open;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  sPageControl1.ActivePageIndex := 0;

  setBank;
  comBank.Perform(CB_SETDROPPEDWIDTH, 155, 0);

end;

procedure TUI_LOCAPP_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_LOCAPP_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN Then sBitBtn1Click(nil);
end;

procedure TUI_LOCAPP_NEW_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.ReadData;
var
  TempStr : String;
  nIDX : integer;
begin
  with qryList do
  begin
    if RecordCount > 0 then
    begin
      TempStr := qryListMAINT_NO.AsString;
      //------------------------------------------------------------------------------
      // 2018-11-28
      // 문서명6자리 + 발신자의 수발신인식별자(12자리) + "-" + 은행코드(2자리) + 년월일(6자리) + 일련번호(3자리)
      //------------------------------------------------------------------------------
      nIDX := Pos('-', TempStr);
      edt_MAINT_NO_Header.Text := LeftStr(TempStr, nIDX-1);
      edt_MAINT_NO_Bank.Text := MidStr(TempStr, nIDX+1, 2);
      edt_MAINT_NO_Serial.Text := MidStr(TempStr, nIDX+3, 9);
//      edt_MAINT_NO_Header.Text := LeftStr(TempStr,14);
//      edt_MAINT_NO_Bank.Text := MidStr(TempStr,16,2);
//      edt_MAINT_NO_Serial.Text := MidStr(TempStr,18,9);

      msk_Datee.Text := qryListDATEE.AsString;
      edt_userno.Text := qryListUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
      CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);      

      //개설의뢰인
      edt_wcd.Text     := qryListAPPLIC.AsString;
      edt_wsangho.Text := qryListAPPLIC1.AsString;
      edt_wceo.Text    := qryListAPPLIC2.AsString;
      edt_waddr1.Text  := qryListAPPADDR1.AsString;
      edt_waddr2.Text  := qryListAPPADDR2.AsString;
      edt_waddr3.Text  := qryListAPPADDR3.AsString;
      edt_wsaupno.Text := qryListAPPLIC3.AsString;

      //수혜자
      edt_rcd.Text     := qryListBENEFC.AsString;
      edt_rsangho.Text := qryListBENEFC1.AsString;
      edt_rceo.Text    := qryListBENEFC2.AsString;
      edt_raddr1.Text  := qryListBNFADDR1.AsString;
      edt_raddr2.Text  := qryListBNFADDR2.AsString;
      edt_raddr3.Text  := qryListBNFADDR3.AsString;
      edt_rsaupno.Text := qryListBENEFC3.AsString;
      edt_remail.Text  := qryListBNFEMAILID.AsString+ '@' +qryListBNFDOMAIN.AsString;

      //수발신인식별자
      edt_iden1.Text := qryListCHKNAME1.AsString;
      edt_iden2.Text := qryListCHKNAME2.AsString;

      //개설은행
      edt_cbank.Text   := qryListAP_BANK.AsString;
      edt_cbanknm.Text := qryListAP_BANK1.AsString;
      edt_cbrunch.Text := qryListAP_BANK2.AsString;

      //신용장종류
      edt_cType.Text := qryListLOC_TYPE.AsString;
      sEdit8.Text := qryListLOC_TYPENAME.AsString;

      //개설금액
      edt_LOCAMT_UNIT.Text := qryListLOC_AMTC.AsString;
      cur_LOCAMT.Value := qryListLOC_AMT.AsCurrency;

      //허용오차
      //+
      edt_perr.Text := qryListCD_PERP.AsString;
      //-
      edt_merr.Text := qryListCD_PERM.AsString;

      //개설근거별 용도
      edt_cyong.Text := qryListBUSINESS.AsString;
      edt_cyongnm.Text := qryListBUSINESSNAME.AsString;

      //LC번호
      edt_lcno.Text := qryListLCNO.AsString;

      //매도확약서
      edt_doc1.Text := qryListOFFERNO1.AsString;
      edt_doc2.Text := qryListOFFERNO2.AsString;
      edt_doc3.Text := qryListOFFERNO3.AsString;
      edt_doc4.Text := qryListOFFERNO4.AsString;
      edt_doc5.Text := qryListOFFERNO5.AsString;
      edt_doc6.Text := qryListOFFERNO6.AsString;
      edt_doc7.Text := qryListOFFERNO7.AsString;
      edt_doc8.Text := qryListOFFERNO8.AsString;
      edt_doc9.Text := qryListOFFERNO9.AsString;

      //주유구비서류
      cur_AttachDoc1.Value := qryListDOCCOPY1.AsInteger;
      cur_AttachDoc2.Value := qryListDOCCOPY2.AsInteger;
      cur_AttachDoc3.Value := qryListDOCCOPY3.AsInteger;
      cur_AttachDoc4.Value := qryListDOCCOPY4.AsInteger;
      cur_AttachDoc5.Value := qryListDOCCOPY5.AsInteger;

      //서류제시기간
      case qryListDOC_PRD.AsInteger of
        5: comDocPeriod.ItemIndex := 0;
        7: comDocPeriod.ItemIndex := 1;
      end;

      //개설관련
      cur_chasu.Value := qryListOPEN_NO.AsInteger;
      //개설신청일자
      msk_CrtDT.Text := qryListAPP_DATE.AsString;
      //물품인도기일
      msk_IndoDt.Text := qryListDELIVERY.AsString;
      //유효기일
      msk_ExpiryDT.Text := qryListEXPIRY.AsString;
      //분할허용여부
      edt_isPart.Text := qryListTRANSPRT.AsString;
      sEdit20.Text := qryListTRANSPRTNAME.AsString; 
      //------------------------------------------------------------------------------
      // 대표공급물품
      //------------------------------------------------------------------------------
      msk_hscd.Text     := qryListBSN_HSCODE.AsString;
      memo_goods.Text   := qryListGOODDES1.AsString;
      memo_etcdoc.Text  := qryListDOC_ETC1.AsString;
      memo_etcinfo.Text := qryListREMARK1.AsString;

      //원수출신용장
      edt_OriginType.Text := qryListDOC_DTL.AsString;
      sEdit22.Text := qryListDOC_DTLNAME.AsString;
      //신용장번호
      edt_OriginLCNO.Text := qryListDOC_NO.AsString;
      //결제금액
      edt_OriginAmt.Value    := qryListDOC_AMT.AsCurrency;
      edt_OriginAmtUnit.Text := qryListDOC_AMTC.AsString;
      //선적기일
      msk_OriginShipDT.Text := qryListLOADDATE.AsString;
      //유효기일
      msk_OriginVaildDT.Text := qryListEXPDATE.AsString;
      //대금결제조건
      edt_AmtType.Text := qryListPAYMENT.AsString;
      sEdit26.Text := qryListPAYMENTNAME.AsString;
      //수출공급상대방
      edt_OriginCD.Text     := qryListIM_NAME.AsString;
      edt_OriginSangho.Text := qryListIM_NAME1.AsString;
      edt_OriginCeo.Text    := qryListIM_NAME2.AsString;
      edt_OriginAddr1.Text  := qryListIM_NAME3.AsString;
      //수출지역
      edt_OriginExportArea.Text := qryListDEST.AsString;
      //발행기관
      edt_Originbank1.Text := qryListISBANK1.AsString;
      edt_Originbank2.Text := qryListISBANK2.AsString;
      //대표수입물품
      memo_ExportGoods.Text := qryListEXGOOD1.AsString;
      //발신기관 전자서명
      edt_Sign1.Text := qrylistEXNAME1.AsString;
      edt_Sign2.Text := qrylistEXNAME2.AsString;
      edt_Sign3.Text := qrylistEXNAME3.AsString;
    end
    else
    begin
      ClearControl(sPanel7);
      ClearControl(sPanel27);
    end;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_LOCAPP_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qryListAfterScroll(DataSet);
end;

procedure TUI_LOCAPP_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  IF ((Sender as TsButton).Tag = 1) AND AnsiMatchText( qryListCHK2.AsString , ['4', '7', '9'] ) Then
  begin
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_EDIT, '수정불가', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;

  IF (Sender as TsButton).Tag = 0 Then NewDoc;

  EnableControl(sPanel6);
  EnableControl(sPanel7);
  EnableControl(sPanel27);

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  if ProgramControlType = ctmodify then
  begin
    EnableControl(sPanel6, false);
  end;
  
  //------------------------------------------------------------------------------
  // 버튼활성화
  //------------------------------------------------------------------------------
  ButtonEnable;
  OriginSection;  
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOCAPP_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
  btnCopy.Enabled := btnNew.Enabled;
  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_LOCAPP_NEW_frm.OriginSection;
var
  Enable : Boolean;
begin
  Enable := AnsiMatchText(UpperCase(edt_cyong.Text),['2AA','2AB','2AC','2AJ','2AK','2AO']);
  edt_OriginType.Enabled := Enable;
  sEdit22.Enabled        := Enable;
  edt_OriginLCNO.Enabled := Enable;
  edt_OriginAmtUnit.Enabled := Enable;
  edt_OriginAmt.Enabled     := Enable;
  msk_OriginShipDT.Enabled  := Enable;
  msk_OriginVaildDT.Enabled := Enable;
  edt_AmtType.Enabled := Enable;
  sEdit26.Enabled := Enable;
  edt_OriginCD.Enabled := Enable;
  edt_OriginSangho.Enabled := Enable;
  edt_OriginCeo.Enabled := Enable;
  edt_OriginAddr1.Enabled := Enable;
  edt_OriginExportArea.Enabled := Enable;
  sEdit32.Enabled := Enable;
  edt_Originbank1.Enabled := Enable;
  edt_Originbank2.Enabled := Enable;
  memo_ExportGoods.Enabled := Enable;
end;

function TUI_LOCAPP_NEW_frm.getMaintHeader: TDOC_HEADER;
begin
  Result.Header := '';
  Result.Bank := '';
  
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LA_BGM1''';
      Open;

      Result.Header := 'LOCAPP'+FieldByName('NAME').AsString;

      Close;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LA_BGM2''';
      Open;

      Result.Bank := FieldByName('NAME').AsString;

      IF (Trim(Result.Header) = '') Then
        raise Exception.Create('[기본설정]-[전자문서 고정번호부여]에 LOAPP문서 고정번호를 입력하세요');

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_wcdDblClick(Sender: TObject);
begin
  inherited;
  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        101:
          begin
            edt_wsaupno.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_wsangho.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_wceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_waddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_waddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_waddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
          end;

        102:
          begin
            edt_rsaupno.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_rsangho.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_rceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_raddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_raddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_raddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
            IF CD_CUST_frm.FieldValues('EMAIL_ID')+'@'+CD_CUST_frm.FieldValues('EMAIL_DOMAIN') <> '@' Then
              edt_remail.Text := CD_CUST_frm.FieldValues('EMAIL_ID')+'@'+CD_CUST_frm.FieldValues('EMAIL_DOMAIN')
            else
              edt_remail.Clear;
            edt_iden1.Text := CD_CUST_frm.FieldValues('ESU1');
            edt_iden2.Text := CD_CUST_frm.FieldValues('ESU2');
          end;

        103:
        begin
          edt_OriginSangho.Text := CD_CUST_frm.FieldValues('ENAME');
          edt_OriginCeo.Text    := CD_CUST_frm.FieldValues('REP_NAME');
          edt_OriginAddr1.Text  := CD_CUST_frm.FieldValues('ADDR1');
        end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_cbankDblClick(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  Case (Sender as TsEdit).Tag of
    //개설은행
    1000: uDialog := TCD_BANK_frm.Create(Self);
    //신용장종류
    1001: uDialog := TCD_LOCTYPE_frm.Create(Self);
    //개설금액통화, 원수출신용장 결제금액
    1002, 1005: uDialog := TCD_AMT_UNIT_frm.Create(Self);
    //개설근거별용도
    1003: uDialog := TCD_LOCBASIC_frm.Create(Self);
    //개설근거서류종류
    1004: uDialog := TCD_NK1001_frm.Create(Self);
    //대금결제조건
    1006: uDialog := TCD_NK4277_frm.Create(Self);
    //수출지역
    1007: uDialog := TCD_NATION_frm.Create(Self);
    //분할허용여부
    1008: uDialog := TCD_PSHIP_frm.Create(Self);
  end;

  try
    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        //개설은행
        1000 :
        begin
          edt_cbanknm.Text := uDialog.Values[1];
          edt_cbrunch.Text := uDialog.Values[2];
        end;
        //신용장종류
        1001:
        begin
          sEdit8.Text := uDialog.Values[1];
        end;
        1003:
        begin
          edt_cyongnm.Text := uDialog.Values[1];
        end;
        //개설근거서류종류
        1004:
        begin
          sEdit22.Text := uDialog.Values[1];
        end;
        //대금결제조건
        1006:
        begin
          sEdit26.Text := uDialog.Values[1];
        end;
        //수출지역
        1007:
        begin
          sEdit32.Text := uDialog.Values[1];
        end;
        //분할허용여부
        1008:
        begin
          sEdit20.Text := uDialog.Values[1];
        end;
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_cyongChange(Sender: TObject);
begin
  inherited;
  OriginSection;
end;

procedure TUI_LOCAPP_NEW_frm.btnCancelClick(Sender: TObject);
var
  DOCNO : String;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;
  DOCNO := edt_MAINT_NO_HEADER.Text+'-'+edt_MAINT_NO_Bank.Text+edt_MAINT_NO_Serial.Text;

  Readlist;
  qryList.Locate('MAINT_NO',DOCNO,[]);
end;

procedure TUI_LOCAPP_NEW_frm.btnDelClick(Sender: TObject);
var
  DocNo : String;
  nCursor : integer;
begin
  IF qryList.RecordCount = 0 Then Exit;
  
  IF AnsiMatchText( qryListCHK2.AsString , ['4', '7', '9'] ) Then
  begin
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_DEL, '삭제불가', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := edt_MAINT_NO_Header.Text + '-' + edt_MAINT_NO_Bank.Text + edt_MAINT_NO_Serial.Text;
    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+'내국신용장'#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCAPP WHERE MAINT_NO = '+QuotedStr(DocNo);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;

          IF qryList.RecordCount > 1 Then
          begin
            qryList.MoveBy(nCursor-1);
          end;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans; 
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;


function TUI_LOCAPP_NEW_frm.CopyFromDOMOFB:Boolean;
begin
//  dlg_CopyLOCAPPfromDOMOFB_frm := Tdlg_CopyLOCAPPfromDOMOFB_frm.Create(Self);
//  try
//    Result := dlg_CopyLOCAPPfromDOMOFB_frm.OpenDialog(0);
//  finally
//    FreeAndNil(dlg_CopyLOCAPPfromDOMOFB_frm);
//  end;
end;

procedure TUI_LOCAPP_NEW_frm.NewDoc;
var
  DOC_HEADER : TDOC_HEADER;
  isCopy : Boolean;
  TMP_NAME, TMP_SIZE : String;
begin
  dlg_CreateDocumentChoice_frm := Tdlg_CreateDocumentChoice_frm.Create(Self);
  dlg_CopyLOCAPPfromDOMOFB_frm := Tdlg_CopyLOCAPPfromDOMOFB_frm.Create(Self);
  try
    try
      case dlg_CreateDocumentChoice_frm.ShowModal of
        mrYes :
        begin
          isCopy := dlg_CopyLOCAPPfromDOMOFB_frm.OpenDialog(0);
          IF not isCopy Then Abort;
        end;
        mrCancel:
        begin
          abort;
        end
      end;

    finally
      FreeAndNil(dlg_CreateDocumentChoice_frm);
    end;

    ClearControl(sPanel6);
    ClearControl(sPanel7);
    ClearControl(sPanel27);
    sPanel29.Visible := True;

  //------------------------------------------------------------------------------
  // 헤더
  //------------------------------------------------------------------------------
    msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
    edt_userno.Text := LoginData.sID;
    com_func.ItemIndex := 5;
    com_type.ItemIndex := 0;
  //------------------------------------------------------------------------------
  // 개설의뢰인 셋팅
  //------------------------------------------------------------------------------
    IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
    begin
      IF not isCopy Then
      begin
        edt_wcd.Text     :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
        edt_wsangho.Text :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
        edt_wceo.Text    :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
        edt_waddr1.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
        edt_waddr2.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
        edt_waddr3.Text  := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
        edt_wsaupno.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
      end;
    end;
  //------------------------------------------------------------------------------
  // 주요구비서류
  //------------------------------------------------------------------------------
    Cur_AttachDoc1.Value := 1;
    Cur_AttachDoc2.Value := 1;
    Cur_AttachDoc3.Value := 0;
    Cur_AttachDoc4.Value := 1;
    Cur_AttachDoc5.Value := 1;
  //------------------------------------------------------------------------------
  // 개설관련
  //------------------------------------------------------------------------------
    //개설회차
    cur_chasu.Value := 1;
    //개설신청일자
    msk_CrtDT.Text := FormatDateTime('YYYYMMDD',Now);
    //물품인도기일
    msk_IndoDt.Text := FormatDateTime('YYYYMMDD',Now);
    //유효기일
    msk_ExpiryDT.Text := FormatDateTime('YYYYMMDD',Now);
    //분할허용여부
    edt_isPart.Text := '9';
    sEdit20.Text := 'ALLOWED';
  //------------------------------------------------------------------------------
  // 전자서명
  //------------------------------------------------------------------------------
    edt_Sign1.Text := DMCodeContents.ConfigNAME1.AsString;
    edt_Sign2.Text := DMCodeContents.ConfigNAME2.AsString;
    edt_Sign3.Text := DMCodeContents.ConfigSIGN.AsString;

    DOC_HEADER := getMaintHeader;
    edt_MAINT_NO_Header.Text := DOC_HEADER.Header;
    edt_MAINT_NO_Bank.Text   := DOC_HEADER.Bank;
    edt_MAINT_NO_Serial.Text := DMAutoNo.GetDocumentNoAutoInc('LOCAPP');

  //------------------------------------------------------------------------------
  // 복사본
  //------------------------------------------------------------------------------
    IF isCopy Then
    begin
      edt_wcd.Text     := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_CODE');
      edt_wsangho.Text := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_NAME1');
      edt_wceo.Text    := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_NAME2');
      edt_waddr1.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_ADDR1');
      edt_waddr2.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_ADDR2');
      edt_waddr3.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_ADDR3');
      edt_wsaupno.Text := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('UD_NO');

      edt_rcd.Text     := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_CODE');
      edt_rsaupno.Text := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_NAME3');
      edt_rsangho.Text := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_NAME1');
      edt_rceo.Text    := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_NAME2');
      edt_raddr1.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_ADDR1');
      edt_raddr2.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_ADDR2');
      edt_raddr3.Text  := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('SR_ADDR3');

      edt_doc1.Text    := dlg_CopyLOCAPPfromDOMOFB_frm.FieldValues('OFR_NO');

      //------------------------------------------------------------------------------
      // 2018-12-05
      // 대표공급물품 입력
      //------------------------------------------------------------------------------
      memo_goods.Clear;  
      with TADOQuery.Create(nil) do
      begin
        try
          Close;
          Connection := DMMssql.KISConnect;
          //대표공급 세번부호
          SQL.Text := 'SELECT TOP 1 HS_NO FROM [DOMOFB_D] WHERE KEYY = '+QuotedStr(dlg_CopyLOCAPPfromDOMOFB_frm.FieldString('MAINT_NO'));
          Open;
          msk_hscd.Text := FieldByName('HS_NO').AsString;

          // 대표공급 물품명
          // 2018-12-05
          // 편의 사항으로 현재는 비활성화 (원래 파라독스 버전에는 안되던 기능임)
//          Close;
//          SQL.Text := 'SELECT NAME1, SIZE1 FROM [DOMOFB_D] WHERE KEYY = '+QuotedStr(dlg_CopyLOCAPPfromDOMOFB_frm.FieldString('MAINT_NO'))+' ORDER BY SEQ';
//          open;
//          while not Eof do
//          begin
//            TMP_NAME := AnsiReplaceText(FieldByName('NAME1').AsString, #13#10, '');
//            TMP_SIZE := AnsiReplaceText(FieldByName('SIZE1').AsString, #13#10, '');
//            IF  TMP_NAME <> '' Then
//              memo_goods.Lines.Add(TMP_NAME+' '+TMP_SIZE);
//            Next;
//          end;


        finally
          Close;
          Free;
        end;
      end;

    end;
  finally
    FreeAndNil(dlg_CopyLOCAPPfromDOMOFB_frm);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  dlg_CopyLOCAPPfromLOCAPP_frm := Tdlg_CopyLOCAPPfromLOCAPP_frm.Create(Self);
  try
    IF dlg_CopyLOCAPPfromLOCAPP_frm.OpenDialog(0) Then
    begin
      ClearControl(sPanel6);
      ClearControl(sPanel7);
      ClearControl(sPanel27);
      sPanel29.Visible := True;

    //------------------------------------------------------------------------------
    // 헤더
    //------------------------------------------------------------------------------
      msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
      edt_userno.Text := LoginData.sID;
      com_func.ItemIndex := 5;
      com_type.ItemIndex := 0;

      CopyData;

      EnableControl(sPanel6);
      EnableControl(sPanel7);
      EnableControl(sPanel27);

      ProgramControlType := ctInsert;

      //------------------------------------------------------------------------------
      // 버튼활성화
      //------------------------------------------------------------------------------
      ButtonEnable;
      OriginSection;
      sPageControl1.ActivePageIndex := 0;
    end;
  finally
    FreeAndNil(dlg_CopyLOCAPPfromLOCAPP_frm);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.CopyData;
var
  TempStr : String;
  DOC_HEADER : TDOC_HEADER;
begin
  with qryList do
  begin
//    if RecordCount > 0 then
//    begin
      DOC_HEADER := getMaintHeader;
      edt_MAINT_NO_Header.Text := DOC_HEADER.Header;
      edt_MAINT_NO_Bank.Text   := DOC_HEADER.Bank;
      edt_MAINT_NO_Serial.Text := DMAutoNo.GetDocumentNoAutoInc('LOCAPP');
//      TempStr := qryListMAINT_NO.AsString;
//      edt_MAINT_NO_Header.Text := LeftStr(TempStr,14);
//      edt_MAINT_NO_Bank.Text := MidStr(TempStr,16,2);
//      edt_MAINT_NO_Serial.Text := MidStr(TempStr,18,9);

      msk_Datee.Text  := FormatDateTime('YYYYMMDD',Now);
      edt_userno.Text := qryListUSER_ID.AsString;

      com_func.ItemIndex := 5;
      com_type.ItemIndex := 0;

      //개설의뢰인
      edt_wcd.Text     := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPLIC');
      edt_wsangho.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPLIC1');
      edt_wceo.Text    := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPLIC2');
      edt_waddr1.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPADDR1');
      edt_waddr2.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPADDR2');
      edt_waddr3.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPADDR3');
      edt_wsaupno.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APPLIC3');

      //수혜자
      edt_rcd.Text     := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BENEFC');
      edt_rsangho.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BENEFC1');
      edt_rceo.Text    := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BENEFC2');
      edt_raddr1.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BNFADDR1');
      edt_raddr2.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BNFADDR2');
      edt_raddr3.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BNFADDR3');
      edt_rsaupno.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BENEFC3');
      edt_remail.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BNFEMAILID')+ '@' +dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BNFDOMAIN');

      //수발신인식별자
      edt_iden1.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('CHKNAME1');
      edt_iden2.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('CHKNAME2');

      //개설은행
      edt_cbank.Text   := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('AP_BANK');
      edt_cbanknm.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('AP_BANK1');
      edt_cbrunch.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('AP_BANK2');

      //신용장종류
      edt_cType.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('LOC_TYPE');
      sEdit8.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('LOC_TYPENAME');

      //개설금액
      edt_LOCAMT_UNIT.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('LOC_AMTC');
      cur_LOCAMT.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldCurr('LOC_AMT');

      //허용오차
      //+
      edt_perr.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('CD_PERP');
      //-
      edt_merr.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('CD_PERM');

      //개설근거별 용도
      edt_cyong.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BUSINESS');
      edt_cyongnm.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BUSINESSNAME');

      //LC번호
      edt_lcno.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('LCNO');

      //매도확약서
      edt_doc1.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO1');
      edt_doc2.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO2');
      edt_doc3.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO3');
      edt_doc4.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO4');
      edt_doc5.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO5');
      edt_doc6.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO6');
      edt_doc7.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO7');
      edt_doc8.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO8');
      edt_doc9.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('OFFERNO9');

      //주유구비서류
      cur_AttachDoc1.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOCCOPY1');
      cur_AttachDoc2.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOCCOPY2');
      cur_AttachDoc3.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOCCOPY3');
      cur_AttachDoc4.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOCCOPY4');
      cur_AttachDoc5.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOCCOPY5');

      edt_isPart.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('TRANSPRT');
      sEdit20.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('TRANSPRTNAME');

      //서류제시기간
      case dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('DOC_PRD') of
        5: comDocPeriod.ItemIndex := 0;
        7: comDocPeriod.ItemIndex := 1;
      end;

      //개설관련
      cur_chasu.Value := dlg_CopyLOCAPPfromLOCAPP_frm.FieldInt('OPEN_NO');
      //개설신청일자
      msk_CrtDT.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('APP_DATE');
      //물품인도기일
      msk_IndoDt.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DELIVERY');
      //유효기일
      msk_ExpiryDT.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXPIRY');

      //------------------------------------------------------------------------------
      // 대표공급물품
      //------------------------------------------------------------------------------
      msk_hscd.Text     := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('BSN_HSCODE');
      memo_goods.Text   := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('GOODDES1');
      memo_etcdoc.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DOC_ETC1');
      memo_etcinfo.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('REMARK1');

      //개설근거서류종류
      edt_OriginType.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DOC_DTL');
      sEdit22.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DOC_DTLNAME');
      //신용장번호
      edt_OriginLCNO.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DOC_NO');
      //결제금액
      edt_OriginAmt.Value    := dlg_CopyLOCAPPfromLOCAPP_frm.FieldCurr('DOC_AMT');
      edt_OriginAmtUnit.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DOC_AMTC');
      //선적기일
      msk_OriginShipDT.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('LOADDATE');
      //유효기일
      msk_OriginVaildDT.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXPDATE');

      //대금결제조건
      edt_AmtType.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('PAYMENT');
      sEdit26.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('PAYMENTNAME');
      //수출공급상대방
      edt_OriginCD.Text     := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('IM_NAME');
      edt_OriginSangho.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('IM_NAME1');
      edt_OriginCeo.Text    := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('IM_NAME2');
      edt_OriginAddr1.Text  := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('IM_NAME3');
      //수출지역
      edt_OriginExportArea.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('DEST');
      //발행기관
      edt_Originbank1.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('ISBANK1');
      edt_Originbank2.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('ISBANK2');
      //대표수입물품
      memo_ExportGoods.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXGOOD1');
      //발신기관 전자서명
      edt_Sign1.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXNAME1');
      edt_Sign2.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXNAME2');
      edt_Sign3.Text := dlg_CopyLOCAPPfromLOCAPP_frm.FieldString('EXNAME3');
//    end;
  end;
end;
procedure TUI_LOCAPP_NEW_frm.btnTempClick(Sender: TObject);
var
  DOCNO : String;
begin
  inherited;
  if (Sender as TsButton).Tag = 1 then
  begin
//------------------------------------------------------------------------------
// 동보전송용 수혜자 수발신인 식별자 안넣었을 경우 에러표시
//------------------------------------------------------------------------------
    IF (Trim(edt_iden1.Text) = '') OR (Trim(edt_iden2.Text) = '') then
    begin
      IF MessageBox(Self.Handle, MSG_LOCAPP_NOT_IDEN, '동보전송 식별자 누락', MB_OKCANCEL + MB_ICONQUESTION) = ID_CANCEL Then
      begin
        sPageControl1.ActivePageIndex := 0;
        edt_iden1.SetFocus;
        Exit;
      end;
    end;

//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '내국신용장 개설신청서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK+MB_ICONERROR);
      IF DMMssql.inTrans Then DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
  qryList.Locate('MAINT_NO',DOCNO,[]);
end;

function TUI_LOCAPP_NEW_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;
begin
  ErrMsg := TStringList.Create;

  Try
    IF Trim(edt_MAINT_NO_Header.Text) = '' Then
      ErrMsg.Add('[공통] 관리번호[헤더]를 입력하세요');
    IF Trim(edt_MAINT_NO_Bank.Text) = '' Then
      ErrMsg.Add('[공통] 관리번호[은행]을 입력하세요');
    IF Trim(edt_MAINT_NO_Serial.Text) = '' Then
      ErrMsg.Add('[공통] 관리번호[일련번호]를 입력하세요');

    IF Trim(edt_wsaupno.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 사업자등록번호를 입력하세요');
    IF Trim(edt_wsangho.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 상호를 입력하세요');

    IF Trim(edt_rsaupno.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 사업자등록번호를 입력하세요');
    IF Trim(edt_rsangho.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 상호를 입력하세요');

    IF Trim(edt_cbank.Text) = '' THEN
      ErrMsg.Add('[공통] 개설은행을 입력하세요');
    IF Trim(edt_cType.Text) = '' THEN
      ErrMsg.Add('[공통] 신용장종류를 선택하세요');
    IF Trim(edt_LOCAMT_UNIT.Text) = '' THEN
      ErrMsg.Add('[공통] 개설금액 통화를 입력하세요');
    IF Trim(edt_cyong.Text) = '' THEN
      ErrMsg.Add('[공통] 개설근거별용도를 입력하세요');

    IF edt_OriginType.Enabled and (Trim(edt_OriginType.Text) = '') Then
      ErrMsg.Add('[대표공급물품] 원수출신용장의 개설근거서류종류를 입력하세요');
    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

function TUI_LOCAPP_NEW_frm.DataProcess(nType : integer):string;
var
  MAINT_NO : string;
  SQLCreate : TSQLCreate;
  TempStr : String;
  nIdx : integer;
begin
  IF nType = 2 Then Exit;
  //------------------------------------------------------------------------------
  // SQL생성기
  //------------------------------------------------------------------------------
  SQLCreate := TSQLCreate.Create;
  try
    MAINT_NO := Trim(edt_MAINT_NO_Header.Text) + '-' + Trim(edt_MAINT_NO_Bank.Text) + Trim(edt_MAINT_NO_Serial.Text);
    Result := MAINT_NO;
      with SQLCreate do
      begin
      //----------------------------------------------------------------------
      // 문서헤더 및 DOMOFC_H1.DB
      //----------------------------------------------------------------------
        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',MAINT_NO);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',MAINT_NO);
          end;
        end;

        SQLHeader('LOCAPP');

        //문서저장구분(0:임시, 1:저장)
        ADDValue('CHK2',nType);
        //등록일자
        ADDValue('DATEE',msk_Datee.Text);
        //유저ID
        ADDValue('USER_ID',edt_userno.Text);
        //메세지1
        ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
        //메세지2
        ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));
        //개설근거별용도
        ADDValue('BUSINESS', edt_cyong.Text);
        //매도확약서번호
        ADDValue('OFFERNO1', Trim(edt_doc1.Text));
        ADDValue('OFFERNO2', Trim(edt_doc2.Text));
        ADDValue('OFFERNO3', Trim(edt_doc3.Text));
        ADDValue('OFFERNO4', Trim(edt_doc4.Text));
        ADDValue('OFFERNO5', Trim(edt_doc5.Text));
        ADDValue('OFFERNO6', Trim(edt_doc6.Text));
        ADDValue('OFFERNO7', Trim(edt_doc7.Text));
        ADDValue('OFFERNO8', Trim(edt_doc8.Text));
        ADDValue('OFFERNO9', Trim(edt_doc9.Text));
        //개설회차
        ADDValue('OPEN_NO', cur_chasu.Text);
        //개설신청일자
        ADDValue('APP_DATE', msk_CrtDT.Text);
        //서류제시기간
        case comDocPeriod.ItemIndex of
          0: ADDValue('DOC_PRD', 5);
          1: ADDValue('DOC_PRD', 7);
        end;
        //물품인도기일
        ADDValue('DELIVERY', msk_IndoDt.Text);
        //유효기일
        ADDValue('EXPIRY', msk_ExpiryDT.Text);
        //분할허용여부
        ADDValue('TRANSPRT', Trim(edt_isPart.Text));

        //대표공급물품명
        IF Trim(memo_goods.Text) = '' Then
          ADDValue('GOODDES', 'Y')
        else
          ADDValue('GOODDES', 'N');
        ADDValue('GOODDES1', memo_goods.Text);

        //기타정보
        IF Trim(memo_etcinfo.Text) = '' Then
          ADDValue('REMARK', 'Y')
        else
          ADDValue('REMARK', 'N');
        ADDValue('REMARK1', memo_etcinfo.Text);

        //개설은행
        ADDValue('AP_BANK', edt_cbank.Text);
        ADDValue('AP_BANK1', edt_cbanknm.Text);
        ADDValue('AP_BANK2', edt_cbrunch.Text);

        //개설의뢰인
        ADDValue('APPLIC', edt_wcd.Text);
        ADDValue('APPLIC1', edt_wsangho.Text);
        ADDValue('APPLIC2', edt_wceo.Text);
        ADDValue('APPLIC3', edt_wsaupno.Text);
        //수혜자
        ADDValue('BENEFC', edt_rcd.Text);
        ADDValue('BENEFC1', edt_rsangho.Text);
        ADDValue('BENEFC2', edt_rceo.Text);
        ADDValue('BENEFC3', edt_rsaupno.Text);

        //발신기관 전자서명
        ADDValue('EXNAME1', edt_sign1.Text);
        ADDValue('EXNAME2', edt_sign2.Text);
        ADDValue('EXNAME3', edt_sign3.Text);

        //구비서류
        ADDValue('DOCCOPY1', cur_AttachDoc1.Text);
        ADDValue('DOCCOPY2', cur_AttachDoc2.Text);
        ADDValue('DOCCOPY3', cur_AttachDoc3.Text);
        ADDValue('DOCCOPY4', cur_AttachDoc4.Text);
        ADDValue('DOCCOPY5', cur_AttachDoc5.Text);
        
        //기타구비서류
        IF Trim(memo_etcdoc.Text) = '' Then
          ADDValue('DOC_ETC','N')
        else
          ADDValue('DOC_ETC','Y');
        ADDValue('DOC_ETC1', memo_etcdoc.Text);

        //신용장종류
        ADDValue('LOC_TYPE', edt_cType.Text);
        //개설금액
        ADDValue('LOC_AMT', cur_LOCAMT.Text, vtvariant );
        ADDValue('LOC_AMTC', edt_LOCAMT_UNIT.Text);

        //개설근거서류 종류
        ADDValue('DOC_DTL', edt_OriginType.Text);
        //신용장[계약서]번호
        ADDValue('DOC_NO', edt_OriginLCNO.Text);
        //원수출 결제금액
        ADDValue('DOC_AMT', edt_OriginAmt.Text, vtvariant);
        ADDValue('DOC_AMTC', edt_OriginAmtUnit.Text);
        //선적기일
        ADDValue('LOADDATE', msk_OriginShipDT.Text);
        //유효기일
        ADDValue('EXPDATE', msk_OriginVaildDT.Text);
        //수출공급상대방
        ADDValue('IM_NAME', edt_OriginCD.Text);
        //수출공급상호
        ADDValue('IM_NAME1', edt_OriginSangho.Text);
        //수출공급대표자
        ADDValue('IM_NAME2', edt_OriginCeo.Text);
        //수출공급주소
        ADDValue('IM_NAME3', edt_OriginAddr1.Text);        
        //수출지역
        ADDValue('DEST', edt_OriginExportArea.Text);
        //발행기관
        ADDValue('ISBANK1', edt_Originbank1.Text);
        ADDValue('ISBANK2', edt_Originbank1.Text);
        //대금결제조건
        ADDValue('PAYMENT', edt_AmtType.Text);
        //대표수출품목
        IF Trim(memo_ExportGoods.Text) = '' Then
          ADDValue('EXGOOD', 'N')
        else
          ADDValue('EXGOOD', 'Y');
        ADDValue('EXGOOD1', memo_ExportGoods.Text);

        //수발신인식별자
        ADDValue('CHKNAME1', edt_iden1.Text);
        //상세수발신인식별자
        ADDValue('CHKNAME2', edt_iden2.Text);

        //LC번호
        ADDValue('LCNO', edt_lcno.Text);
        //대표공급물품 HS부호
        ADDValue('BSN_HSCODE', msk_hscd.Text);

        //개설자 주소
        ADDValue('APPADDR1', edt_waddr1.Text);
        ADDValue('APPADDR2', edt_waddr2.Text);
        ADDValue('APPADDR3', edt_waddr3.Text);
        //수혜자 주소
        ADDValue('BNFADDR1', edt_raddr1.Text);
        ADDValue('BNFADDR2', edt_raddr2.Text);
        ADDValue('BNFADDR3', edt_raddr3.Text);
        //이메일 주소
        TempStr := edt_remail.Text;
        IF Trim(TempStr) <> '' Then
        begin
          nIdx := Pos('@', TempStr);
          ADDValue('BNFEMAILID', LeftStr(TempStr,nIdx-1));
          ADDValue('BNFDOMAIN', MidStr(TempStr, nIdx+1, Length(TempStr)-nIdx));
        end
        else
        begin
          ADDValue('BNFEMAILID', '');
          ADDValue('BNFDOMAIN', '');
        end;

        //허용오차
        ADDValue('CD_PERP', edt_perr.Text);
        ADDValue('CD_PERM', edt_merr.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          if sCheckBox1.Checked then
            Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
  finally
    SQLCreate.Free;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  LOCAPP_PRINT_frm := TLOCAPP_PRINT_frm.Create(Self);
  try
    LOCAPP_PRINT_frm.ReadOnlyDocument(qryList.Fields);
    Preview_frm.Report := LOCAPP_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCAPP_PRINT_frm);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);    
end;

procedure TUI_LOCAPP_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := LOCAPP(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCAPP';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCAPP';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qryList.Close;
        qrylist.Open;

        qrylist.Locate('MAINT_NO',RecvDoc,[]);

//        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
//                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
//                 RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;


procedure TUI_LOCAPP_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.btnExitClick(Sender: TObject);
begin
//  inherited;
  Close;
end;

procedure TUI_LOCAPP_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := Cafree;
  UI_LOCAPP_NEW_frm := nil;
end;

procedure TUI_LOCAPP_NEW_frm.setBank;
begin
  with comBank do
  begin
    Clear;

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        Close;
        SQL.Text := 'SELECT [CODE] ,[NAME] FROM [CODE2NDD] WHERE Prefix = ''APPSPC은행'' AND Remark = 1';
        Open;

        while not eof do
        begin
          comBank.Items.Add(FieldByName('CODE').AsString+': '+FieldByName('NAME').AsString);
          Next;
        end;
      finally
        Close;
        Free;
      end;
    end;
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_MAINT_NO_BankEnter(Sender: TObject);
begin
  inherited;
  comBank.Left := 178;
  comBank.Top := 4;
  comBank.Visible := True;

  IF combank.ItemIndex = -1 Then combank.ItemIndex := 0;

  comBank.SetFocus;
end;

procedure TUI_LOCAPP_NEW_frm.comBankSelect(Sender: TObject);
begin
  inherited;
  edt_MAINT_NO_Bank.Text := CM_TEXT(comBank,[':']);
  comBank.Visible := False;
end;

procedure TUI_LOCAPP_NEW_frm.sBitBtn5Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_LOCAPP_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 2;
end;

procedure TUI_LOCAPP_NEW_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  If ProgramControlType <> ctView Then Exit;
  
  BMK := qryList.GetBookmark;
  ReadList;
  try
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
  finally
    qryList.FreeBookmark(BMK);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_cbankExit(Sender: TObject);
var
  Return : TBANKINFO;
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;
    
  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    Return := CD_BANK_frm.GetCodeText(edt_cbank.Text);
    IF Return.sName <> 'NONE' Then
    begin
      edt_cbanknm.Text := Return.sName;
      edt_cbrunch.Text := Return.sBrunch;      
    end;
  finally
  end;
end;

procedure TUI_LOCAPP_NEW_frm.edt_cTypeChange(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;
  try
    Case (Sender as TsEdit).Tag of
      //신용장종류
      1001:
      begin
        uDialog := TCD_LOCTYPE_frm.Create(Self);
        sEdit8.Text := uDialog.GetCodeText(edt_cType.Text);
      end;

      //개설근거별용도
      1003:
      begin
        uDialog := TCD_LOCBASIC_frm.Create(Self);
        edt_cyongnm.Text := uDialog.GetCodeText(edt_cyong.Text);
      end;

      //개설근거서류종류
      1004:
      begin
        uDialog := TCD_NK1001_frm.Create(Self);
        sEdit22.Text := uDialog.GetCodeText(edt_OriginType.Text);
      end;

      //대금결제조건
      1006:
      begin
        uDialog := TCD_NK4277_frm.Create(Self);
        sEdit26.Text := uDialog.GetCodeText(edt_AmtType.Text);
      end;

      //수출지역
      1007:
      begin
        uDialog := TCD_NATION_frm.Create(Self);
        sEdit32.Text := uDialog.GetCodeText(edt_OriginExportArea.Text);
      end;

      //분할허용여부
      1008:
      begin
        uDialog := TCD_PSHIP_frm.Create(Self);
        sEdit20.Text := uDialog.GetCodeText(edt_isPart.Text);
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_LOCAPP_NEW_frm.sDBGrid3DblClick(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
end;

end.
