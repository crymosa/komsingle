unit UI_APPSPC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sCustomComboEdit, sCurrEdit, sCurrencyEdit, StdCtrls,
  sComboBox, Mask, sMaskEdit, Grids, DBGrids, acDBGrid, DBCtrls, sDBMemo,
  sDBEdit, sMemo, Buttons, sBitBtn, sEdit, ComCtrls, sPageControl,
  sCheckBox, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, sSkinProvider,
  sBevel, DB, ADODB, DateUtils, QuickRpt;

type
  TUI_APPSPC_NEW_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_DATEE: TsMaskEdit;
    com_MESSAGE1: TsComboBox;
    com_MESSAGE2: TsComboBox;
    edt_USER_ID: TsEdit;
    sPanel30: TsPanel;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    btnSearch: TsBitBtn;
    sPanel29: TsPanel;
    sPanel32: TsPanel;
    com_BGM_GUBUN: TsComboBox;
    edt_BGM3: TsEdit;
    edt_BGM2: TsEdit;
    edt_BGM4: TsEdit;
    edt_BGM1: TsEdit;
    edt_CP_NO: TsEdit;
    btn_Panel: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sCheckBox1: TsCheckBox;
    sPanel44: TsPanel;
    sPanel45: TsPanel;
    sPanel47: TsPanel;
    edt_APP_CODE: TsEdit;
    edt_APP_SNAME: TsEdit;
    edt_APP_NAME: TsEdit;
    edt_APP_ADDR1: TsEdit;
    edt_APP_ADDR3: TsEdit;
    edt_APP_ADDR2: TsEdit;
    edt_APP_ELEC: TsEdit;
    sPanel48: TsPanel;
    sPanel50: TsPanel;
    edt_CP_BANK: TsEdit;
    edt_CP_BANKNAME: TsEdit;
    edt_CP_BANKBU: TsEdit;
    edt_CP_ACCOUNTNO: TsEdit;
    edt_CP_CURR: TsEdit;
    edt_CP_NAME1: TsEdit;
    edt_CP_NAME2: TsEdit;
    edt_CP_AMT: TsCurrencyEdit;
    edt_CP_AMTC: TsEdit;
    edt_CP_AMTU: TsCurrencyEdit;
    sPanel1: TsPanel;
    sPanel8: TsPanel;
    sPanel39: TsPanel;
    msk_DF_SAUPNO: TsMaskEdit;
    edt_DF_NAME1: TsEdit;
    edt_DF_NAME2: TsEdit;
    edt_DF_NAME3: TsEdit;
    edt_DF_EMAIL1: TsEdit;
    edt_DF_EMAIL2: TsEdit;
    sPanel35: TsPanel;
    sPanel40: TsPanel;
    edt_CP_ADD_ACCOUNTNO1: TsEdit;
    edt_CP_ADD_NAME1: TsEdit;
    edt_CP_ADD_CURR1: TsEdit;
    edt_CP_ADD_ACCOUNTNO2: TsEdit;
    edt_CP_ADD_NAME2: TsEdit;
    edt_CP_ADD_CURR2: TsEdit;
    sTabSheet9: TsTabSheet;
    sPanel38: TsPanel;
    sPanel55: TsPanel;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sDBGrid8: TsDBGrid;
    sPanel59: TsPanel;
    sPanel60: TsPanel;
    sPanel61: TsPanel;
    sDBGrid9: TsDBGrid;
    sPanel63: TsPanel;
    sPanel64: TsPanel;
    sPanel52: TsPanel;
    btn2AH_EDT: TsBitBtn;
    btn2AH_DEL: TsBitBtn;
    btn2AH_ADD: TsBitBtn;
    sPanel54: TsPanel;
    sPanel58: TsPanel;
    sPanel62: TsPanel;
    sDBGrid7: TsDBGrid;
    edt_DOC_NO: TsEdit;
    com_LA_TYPE: TsComboBox;
    msk_ISS_DATE: TsMaskEdit;
    edt_LR_NO: TsEdit;
    edt_LA_BANKBUCODE: TsEdit;
    edt_LA_BANKNAMEP: TsEdit;
    edt_LA_BANKBUP: TsEdit;
    sPanel11: TsPanel;
    btn2AJ_EDT: TsBitBtn;
    btn2AJ_DEL: TsBitBtn;
    btn2AJ_ADD: TsBitBtn;
    sPanel41: TsPanel;
    btn1BW_EDT: TsBitBtn;
    btn1BW_DEL: TsBitBtn;
    btn1BW_ADD: TsBitBtn;
    sPanel9: TsPanel;
    sPanel12: TsPanel;
    sPanel13: TsPanel;
    sPanel10: TsPanel;
    msk_APP_SAUPNO: TsMaskEdit;
    sPanel14: TsPanel;
    edt_CP_CUX: TsCurrencyEdit;
    com_CP_CODE: TsComboBox;
    dsList: TDataSource;
    qryList: TADOQuery;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    qryListD1_2AP: TADOQuery;
    qryListD1_2AJ: TADOQuery;
    qryListD1_1BW: TADOQuery;
    qryListD1_2AH: TADOQuery;
    dsListD1_2AH: TDataSource;
    dsListD1_2AJ: TDataSource;
    dsListD1_1BW: TDataSource;
    DataSource1: TDataSource;
    spCopyAPPSPC: TADOStoredProc;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM1: TStringField;
    qryListBGM2: TStringField;
    qryListBGM3: TStringField;
    qryListBGM4: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_CURR: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListCP_ADD_ACCOUNTNO1: TStringField;
    qryListCP_ADD_NAME1: TStringField;
    qryListCP_ADD_CURR1: TStringField;
    qryListCP_ADD_ACCOUNTNO2: TStringField;
    qryListCP_ADD_NAME2: TStringField;
    qryListCP_ADD_CURR2: TStringField;
    qryListAPP_DATE: TStringField;
    qryListCP_CODE: TStringField;
    qryListCP_NO: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_SAUPNO: TStringField;
    qryListAPP_NAME: TStringField;
    qryListAPP_ELEC: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_CUX: TBCDField;
    qryListDF_SAUPNO: TStringField;
    qryListDF_EMAIL1: TStringField;
    qryListDF_EMAIL2: TStringField;
    qryListDF_NAME1: TStringField;
    qryListDF_NAME2: TStringField;
    qryListDF_NAME3: TStringField;
    qryListDF_CNT: TStringField;
    qryListVB_CNT: TStringField;
    qryListSS_CNT: TStringField;
    sBevel1: TsBevel;
    qryReady: TADOQuery;
    qryD1Edit: TADOQuery;
    qryD1Cancel: TADOQuery;
    btnCpy2AP: TsBitBtn;
    QRCompositeReport1: TQRCompositeReport;
    edt_BGM1Label: TsEditLabel;
    msk_APP_DATE: TsMaskEdit;
    procedure DOCADD(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnNewClick(Sender: TObject);
    procedure DOCEDT(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnTempClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure CODEDblClick(Sender: TObject);

    procedure DOCDEL(Sender: TObject);
    procedure qryListD1_2AHAfterScroll(DataSet: TDataSet);
    procedure qryListD1_2AJAfterScroll(DataSet: TDataSet);
    procedure qryListD1_1BWAfterScroll(DataSet: TDataSet);
    procedure edt_DOC_NODblClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure sMaskEdit3Change(Sender: TObject);
    procedure sMaskEdit4Change(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure sMaskEdit2Change(Sender: TObject);
    procedure edt_SearchNoChange(Sender: TObject);
    procedure sEdit1Change(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure btnReadyClick(Sender: TObject);
    procedure com_BGM_GUBUNChange(Sender: TObject);
    procedure btnCpy2APClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure edt_CP_BANKExit(Sender: TObject);
    procedure CODEExit(Sender: TObject);
    procedure btnSendClick(Sender: TObject);

  private
    ReadWrite:Boolean;//Read = True, Write = False
    Fmaint_No:String;
    FMode,FMode_D1,FSEQ:Integer;
    function ErrorCheck:String;
    procedure BtnControl(RW:Boolean);
    procedure SelectRecordView;
    procedure SelectRecordViewD1(DOC_GUBUN:Integer);
  protected
    procedure ReadyDocument(DocNo: String); override;

    { Private declarations }
  public
    property SEQ : Integer read FSEQ Write FSEQ;
    property mode : Integer read FMode Write Fmode;
    property mode_D1 : Integer read FMode_D1 Write Fmode_D1;
    property Maint_No : String read FMaint_No write FMaint_No;
    { Public declarations }
  end;
var
  UI_APPSPC_NEW_frm: TUI_APPSPC_NEW_frm;

implementation

uses
  ICON, MSSQL, UI_APPSPC_NEW_DOC1, UI_APPSPC_NEW_DOC2, UI_APPSPC_NEW_DOC3, AutoNo,VarDefine,TypeDefine,
  CD_CUST, SQLCreator, CD_AMT_UNIT, CD_BANK, CD_APPSPCBANK, CodeContents,
  CodeDialogParent, dlg_CopyAPPSPCfromLOCAD1, MessageDefine, StrUtils,
  Dlg_ErrorMessage, DocumentSend, Dlg_RecvSelect, CreateDocuments,
  APPSPC_PRINT_NEW, Preview;
{$R *.dfm}

procedure TUI_APPSPC_NEW_frm.DOCADD(Sender: TObject);
begin
  inherited;
  mode_D1 := 1;
  if (qryListD1_2AP.RecordCount + qryListD1_2AJ.RecordCount + qryListD1_1BW.RecordCount + qryListD1_2AH.RecordCount) > 8  then
  begin
    ShowMessage('물품수령증명서 첨부통수 + 세금계산서(or 상업송장) 첨부통수는 최대 8건까지만 등록가능합니다. ');
    exit;
  end;
  
  CASE TsBitBtn(Sender).Tag Of
  1://물품수령증명서 신규
  begin

    if not Assigned(UI_APPSPC_NEW_DOC1_frm) Then
       UI_APPSPC_NEW_DOC1_frm := TUI_APPSPC_NEW_DOC1_frm.Create(Application);

    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'INSERT INTO APPSPC_D1(KEYY,DOC_GUBUN,SEQ) VALUES('+QuotedStr(Maint_No)+','+QuotedStr('2AH')+','+IntToStr(qryListD1_2AH.RecordCount+1)+')';
        SEQ := qryListD1_2AH.RecordCount+1;

        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    with UI_APPSPC_NEW_DOC1_frm.qryListD2 do
    begin
      Close;
      Parameters[0].Value := Maint_No;
      Parameters[0].Value := '2AH';
      Parameters[0].Value := qryListD1_2AH.RecordCount+1;
      Open;
    end;

    UI_APPSPC_NEW_DOC1_frm.ShowModal;

    FreeAndNil(UI_APPSPC_NEW_DOC1_frm);

  end;
  2://세금계산서 신규
  begin
    if qryListD1_1BW.RecordCount > 0 then
    begin
      ShowMessage('세금계산서와 상업송장은 한가지만 입력(전송)할 수 있습니다.');
      exit;
    end;
    if not Assigned(UI_APPSPC_NEW_DOC2_frm) Then
      UI_APPSPC_NEW_DOC2_frm := TUI_APPSPC_NEW_DOC2_frm.Create(Application);

    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'INSERT INTO APPSPC_D1(KEYY,DOC_GUBUN,SEQ) VALUES('+QuotedStr(Maint_No)+','+QuotedStr('2AJ')+','+IntToStr(qryListD1_2AJ.RecordCount+1)+')';
        SEQ := qryListD1_2AJ.RecordCount+1;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    with UI_APPSPC_NEW_DOC2_frm.qryListD2 do
    begin
      Close;
      Parameters[0].Value := Maint_No;
      Parameters[0].Value := '2AJ';
      Parameters[0].Value := SEQ;
      Open;
    end;

    UI_APPSPC_NEW_DOC2_frm.ShowModal;
    FreeAndNil(UI_APPSPC_NEW_DOC2_frm);
  end;

  3:
  begin
    if qryListD1_2AJ.RecordCount > 0 then
    begin
      ShowMessage('세금계산서와 상업송장은 한가지만 입력(전송)할 수 있습니다.');
      exit;
    end;
    
    if not Assigned(UI_APPSPC_NEW_DOC3_frm) Then
       UI_APPSPC_NEW_DOC3_frm := TUI_APPSPC_NEW_DOC3_frm.Create(Application);

    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'INSERT INTO APPSPC_D1(KEYY,DOC_GUBUN,SEQ) VALUES('+QuotedStr(Maint_No)+','+QuotedStr('1BW')+','+IntToStr(qryListD1_1BW.RecordCount+1)+')';
        SEQ := qryListD1_1BW.RecordCount+1;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    with UI_APPSPC_NEW_DOC3_frm.qryListD2 do
    begin
      Close;
      Parameters[0].Value := Maint_No;
      Parameters[0].Value := '1BW';
      Parameters[0].Value := SEQ;
      Open;
    end;

    UI_APPSPC_NEW_DOC3_frm.ShowModal;
    FreeAndNil(UI_APPSPC_NEW_DOC3_frm);
  end;
  END;
end;

procedure TUI_APPSPC_NEW_frm.SelectRecordView;
begin
//공통사항 데이터(APPSPC_H) 가져오기
//========================================================================
     //관리번호
     edt_MAINT_NO.Text := qryList.FieldByName('MAINT_NO').AsString;

     //전자문서구분
     if qryList.FieldByName('BGM_GUBUN').AsString = '2BK' then
        com_BGM_GUBUN.ItemIndex := 0
     else
        com_BGM_GUBUN.ItemIndex := 1;
     //유저번호
     edt_USER_ID.Text := qryList.FieldByName('USER_ID').AsString;
     //문서번호
     edt_BGM1.Text := qryList.FieldByName('BGM1').AsString;
     edt_BGM2.Text := qryList.FieldByName('BGM2').AsString;
     edt_BGM3.Text := qryList.FieldByName('BGM3').AsString;
     edt_BGM4.Text := qryList.FieldByName('BGM4').AsString;
     //작성일자
     msk_DATEE.Text := qryList.FieldByName('DATEE').AsString;
     //신청일자
     msk_APP_DATE.Text := qryList.FieldByName('APP_DATE').AsString;
     //문서기능
     if qryList.FieldByName('MESSAGE1').AsString = '7' then
       com_MESSAGE1.ItemIndex := 0
     else if qryList.FieldByName('MESSAGE1').AsString = '9' then
       com_MESSAGE1.ItemIndex := 1;

     //문서유형
     if qryList.FieldByName('MESSAGE2').AsString = 'AB' then
        com_MESSAGE2.ItemIndex := 0
     else if qryList.FieldByName('MESSAGE2').AsString = 'AP' then
        com_MESSAGE2.ItemIndex := 1
     else if qryList.FieldByName('MESSAGE2').AsString =  'NA' then
        com_MESSAGE2.ItemIndex := 2
     else if qryList.FieldByName('MESSAGE2').AsString = 'RE' then
        com_MESSAGE2.ItemIndex := 3;

     //추심(매입)코드
     if qryList.FieldByName('CP_CODE').AsString = '2BA' then
       com_CP_CODE.ItemIndex := 1
     else if qryList.FieldByName('CP_CODE').AsString = '2BB' then
       com_CP_CODE.ItemIndex := 0;
     //추심(매입)신청번호
     edt_CP_NO.Text := qryList.FieldByName('CP_NO').AsString;

     //신청인 상호코드
     edt_APP_CODE.Text := qryList.FieldByName('APP_CODE').AsString;
     //신청인 상호명
     edt_APP_SNAME.Text := qryList.FieldByName('APP_SNAME').AsString;
     //신청인 사업자등록번호
     msk_APP_SAUPNO.Text := qryList.FieldByName('APP_SAUPNO').AsString;
     //신청인 대표자
     edt_APP_NAME.Text := qryList.FieldByName('APP_NAME').AsString;
     //신청인 주소
     edt_APP_ADDR1.Text := qryList.FieldByName('APP_ADDR1').AsString;
     edt_APP_ADDR2.Text := qryList.FieldByName('APP_ADDR2').AsString;
     edt_APP_ADDR3.Text := qryList.FieldByName('APP_ADDR3').AsString;
     //신청인 전자서명
     edt_APP_ELEC.Text := qryList.FieldByName('APP_ELEC').AsString;
     //추심은행 은행코드
     edt_CP_BANK.Text := qryList.FieldByName('CP_BANK').AsString;
     //추심은행 은행명
     edt_CP_BANKNAME.Text := qryList.FieldByName('CP_BANKNAME').AsString;
     //추심은행 지점명
     edt_CP_BANKBU.Text := qryList.FieldByName('CP_BANKBU').AsString;
     //추심은행 계좌번호
     edt_CP_ACCOUNTNO.Text := qryList.FieldByName('CP_ACCOUNTNO').AsString;
     //추심은행 계좌통화
     edt_CP_CURR.Text := qryList.FieldByName('CP_CURR').AsString;
     //추심은행 계좌주
     edt_CP_NAME1.Text := qryList.FieldByName('CP_NAME1').AsString;
     edt_CP_NAME2.Text := qryList.FieldByName('CP_NAME2').AsString;
     //추심금액(원화)
     edt_CP_AMT.Text := FloatToStr(qryList.FieldByName('CP_AMT').AsFloat);
     //추심금액 적용환율
     edt_CP_CUX.Text := FloatToStr(qryList.FieldByName('CP_CUX').AsFloat);
     //추심금액(외화) 통화단위
     edt_CP_AMTC.Text := qryList.FieldByName('CP_AMTC').AsString;
     //추심금액(외화) 금액
     edt_CP_AMTU.Text := FloatToStr(qryList.FieldByName('CP_AMTU').AsFloat);

     //구매자 사업자등록번호
     msk_DF_SAUPNO.Text := qryList.FieldByName('DF_SAUPNO').AsString;
     //구매자 상호
     edt_DF_NAME1.Text := qryList.FieldByName('DF_NAME1').AsString;
     edt_DF_NAME2.Text := qryList.FieldByName('DF_NAME2').AsString;
     edt_DF_NAME3.Text := qryList.FieldByName('DF_NAME3').AsString;
     //구매자 이메일
     edt_DF_EMAIL1.Text := qryList.FieldByName('DF_EMAIL1').AsString;
     edt_DF_EMAIL2.Text := qryList.FieldByName('DF_EMAIL2').AsString;

     //추가계좌번호1,2
     edt_CP_ADD_ACCOUNTNO1.Text := qryList.FieldByName('CP_ADD_ACCOUNTNO1').AsString;
     edt_CP_ADD_ACCOUNTNO2.Text := qryList.FieldByName('CP_ADD_ACCOUNTNO2').AsString;
     //추가계좌주 1,2,
     edt_CP_ADD_NAME1.Text := qryList.FieldByName('CP_ADD_NAME1').AsString;
     edt_CP_ADD_NAME2.Text := qryList.FieldByName('CP_ADD_NAME2').AsString;
     //추가계좌통화 1,2
     edt_CP_ADD_CURR1.Text := qryList.FieldByName('CP_ADD_CURR1').AsString;
     edt_CP_ADD_CURR2.Text := qryList.FieldByName('CP_ADD_CURR2').AsString;


      //문서별 데이터(APPSPC_D1) 가져오기
//==============================================================================
     qryListD1_2AP.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
     qryListD1_2AP.Close;
     qryListD1_2AP.Open;
     qryListD1_2AH.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
     qryListD1_2AH.Close;
     qryListD1_2AH.Open;
     sPanel60.Caption := '물품수령증명서 : '+IntToStr(qryListD1_2AH.RecordCount)+'건';
     qryListD1_2AJ.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
     qryListD1_2AJ.Close;
     qryListD1_2AJ.Open;
     sPanel58.Caption := '세금계산서 : '+IntToStr(qryListD1_2AJ.RecordCount)+'건';
     qryListD1_1BW.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
     qryListD1_1BW.Close;
     qryListD1_1BW.Open;
     sPanel56.Caption := '상업송장 : '+IntToStr(qryListD1_1BW.RecordCount)+'건';


      //내국신용장 문서번호
      edt_DOC_NO.Text := qryListD1_2AP.FieldByName('DOC_NO').AsString;
      //내국신용장 종류
      if qryListD1_2AP.FieldByName('LA_TYPE').AsString = '2AA' then
          com_LA_TYPE.ItemIndex := 0
      else if qryListD1_2AP.FieldByName('LA_TYPE').AsString = '2AB' then
          com_LA_TYPE.ItemIndex := 1
      else if qryListD1_2AP.FieldByName('LA_TYPE').AsString = '2AC' then
          com_LA_TYPE.ItemIndex := 2;
      //내국신용장 개설일자
      msk_ISS_DATE.Text := qrylistD1_2AP.FieldByName('ISS_DATE').AsString;
      //내국신용장 번호
      edt_LR_NO.Text := qryListD1_2AP.FieldByName('LR_NO').AsString;
      //내국신용장 개설은행코드
      edt_LA_BANKBUCODE.Text := qryListD1_2AP.FieldByName('LA_BANKBUCODE').AsString;
      //내국신용장 개설은행명
      edt_LA_BANKNAMEP.Text := qryListD1_2AP.FieldByName('LA_BANKNAMEP').AsString;
      //내국신용장 개설은행지점명
      edt_LA_BANKBUP.Text := qryListD1_2AP.FieldByName('LA_BANKBUP').AsString;



end;

procedure TUI_APPSPC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;


procedure TUI_APPSPC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SelectRecordView;
  Maint_No := edt_MAINT_NO.Text;

end;

procedure TUI_APPSPC_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  mode := 1;
  sPageControl1.ActivePageIndex := 0;
  ReadWrite := True;
  BtnControl(ReadWrite);
  EnableControl(sPanel47);
  EnableControl(sPanel50);
  EnableControl(sPanel39);
  EnableControl(sPanel40);
  EnableControl(sPanel32);
  EnableControl(sPanel5, False);//데이터 조회 부분
  com_MESSAGE1.Enabled := True;
  com_MESSAGE2.Enabled := True;
  com_CP_CODE.Enabled := False;
  EnableControl(sPanel63);
  ClearControl(sPanel47);
  ClearControl(sPanel50);
  ClearControl(sPanel39);
  ClearControl(sPanel40);
  ClearControl(sPanel32);
  ClearControl(sPanel63);

//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPSPC');

  msk_DATEE.Text := FormatDateTime('YYYYMMDD', Now);
  edt_USER_ID.Text := LoginData.sID;
  com_MESSAGE1.ItemIndex := 1;
  com_MESSAGE2.ItemIndex := 0;

  if not DMMssql.inTrans then
    DMMssql.BeginTrans;

  With TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'Insert Into APPSPC_H(MAINT_NO) VALUES('+QuotedStr(edt_MAINT_NO.Text)+')';
      ExecSQL;
    finally
      Close;
      free;
    end;
  end;
  Maint_No := edt_MAINT_NO.Text;

//------------------------------------------------------------------------------
// 공통사항
//------------------------------------------------------------------------------
  com_BGM_GUBUN.ItemIndex := 0;
  com_CP_CODE.ItemIndex := 0;
  if DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) then
    edt_BGM1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ESU1').AsString;//수발신인식별자 최대12자

  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD', Now);
  edt_BGM4.Text := edt_MAINT_NO.Text;
  edt_BGM1.Enabled := False;
  edt_BGM4.Enabled := False;

//------------------------------------------------------------------------------
// 신청인 : 00000코드로 등록된 거래처가 있으면 자동등록(자기자신)
//------------------------------------------------------------------------------

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.SearchData('00000') > 0 then
    begin
      edt_APP_CODE.Text := CD_CUST_frm.FieldValues('CODE');
      msk_APP_SAUPNO.Text := CD_CUST_frm.FieldValues('SAUP_NO');
      edt_APP_SNAME.Text := CD_CUST_frm.FieldValues('ENAME');
      edt_APP_NAME.Text := CD_CUST_frm.FieldValues('REP_NAME');
      edt_APP_ADDR1.Text := CD_CUST_frm.FieldValues('ADDR1');
      edt_APP_ADDR2.Text := CD_CUST_frm.FieldValues('ADDR2');
      edt_APP_ADDR3.Text := CD_CUST_frm.FieldValues('ADDR3');
      edt_APP_ELEC.Text := CD_CUST_frm.FieldValues('Jenja');
    end;
  finally
      FreeAndNil(CD_CUST_frm);
  end;

//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
    com_LA_TYPE.ItemIndex := 0;
//    msk_ISS_DATE.Text := FormatDateTime('YYYYMMDD',Now);\
    qryListD1_2AP.Close;
    qryListD1_2AP.Parameters[0].Value := Maint_No;
    qryListD1_2AP.Open;


//------------------------------------------------------------------------------
// 물품수령증명서
//------------------------------------------------------------------------------
    sPanel60.Caption := '물품수령증명서 : 0건';
    qryListD1_2AH.Close;
    qryListD1_2AH.Parameters[0].Value := Maint_No;
    qryListD1_2AH.Open;
//------------------------------------------------------------------------------
// 세금계산서
//------------------------------------------------------------------------------
    sPanel58.Caption := '세금계산서 : 0건';
    qryListD1_2AJ.Close;
    qryListD1_2AJ.Parameters[0].Value := Maint_No;
    qryListD1_2AJ.Open;

//------------------------------------------------------------------------------
// 상업송장
//------------------------------------------------------------------------------
    sPanel56.Caption := '상업송장 : 0건';
    qryListD1_1BW.Close;
    qryListD1_1BW.Parameters[0].Value := Maint_No;
    qryListD1_1BW.Open;
end;

procedure TUI_APPSPC_NEW_frm.DOCEDT(Sender: TObject);
begin
  inherited;
  //물품수령증명서(2AH) 수정
  mode_D1 := 2;
  CASE TsBitBtn(Sender).Tag OF
    1:
    begin
      if qryListD1_2AH.RecordCount = 0 then
      begin
        ShowMessage('등록된 물품수령증명서가 없습니다.');
        mode_D1 := 1;
        exit;
      end;
      qryD1Edit.Close;
      qryD1Edit.Parameters.ParamByName('KEYY').Value := Maint_No;
      qryD1Edit.Parameters.ParamByName('SEQ').Value := SEQ;
      qryD1Edit.Parameters.ParamByName('DOC_GUBUN').Value := '2AH';
      qryD1Edit.ExecSQL;
      
      if not Assigned(UI_APPSPC_NEW_DOC1_frm) Then
        UI_APPSPC_NEW_DOC1_frm := TUI_APPSPC_NEW_DOC1_frm.Create(Application);

        with UI_APPSPC_NEW_DOC1_frm do
        begin
          qryListD2.Close;
          qryListD2.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
          qryListD2.Parameters[1].Value := '2AH';
          qryListD2.Parameters[2].Value := qryListD1_2AH.FieldByName('SEQ').AsInteger;
          SEQ := qryListD1_2AH.FieldByName('SEQ').AsInteger;
          qryListD2.Open;
          SelectRecordViewD1(1);
          if ShowModal = mrCancel then
            FreeAndNil(UI_APPSPC_NEW_DOC1_frm)
          else
            FreeAndNil(UI_APPSPC_NEW_DOC1_frm);
        end;

    end;
    //세금계산서(2AJ) 수정
    2:
    begin    
      if qryListD1_2AJ.RecordCount = 0 then
      begin
        ShowMessage('등록된 세금계산서가 없습니다.');
        mode_D1 := 1;
        exit;
      end;
      
      qryD1Edit.Close;
      qryD1Edit.Parameters.ParamByName('KEYY').Value := Maint_No;
      qryD1Edit.Parameters.ParamByName('SEQ').Value := SEQ;
      qryD1Edit.Parameters.ParamByName('DOC_GUBUN').Value := '2AJ';
      qryD1Edit.ExecSQL;
      if not Assigned(UI_APPSPC_NEW_DOC2_frm) Then
         UI_APPSPC_NEW_DOC2_frm := TUI_APPSPC_NEW_DOC2_frm.Create(Application);
         with UI_APPSPC_NEW_DOC2_frm do
         begin
           qryListD2.Close;
           qryListD2.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
           qryListD2.Parameters[1].Value := '2AJ';
           qryListD2.Parameters[2].Value := qryListD1_2AJ.FieldByName('SEQ').AsInteger;
           SEQ := qryListD1_2AJ.FieldByName('SEQ').AsInteger;
           qryListD2.Open;
           SelectRecordViewD1(2);
           if ShowModal = mrCancel then
              FreeAndNil(UI_APPSPC_NEW_DOC2_frm)
           ELSE
              FreeAndNil(UI_APPSPC_NEW_DOC2_frm);
         end;

    end;
    //상업송장(1BW) 수정
    3:
    begin    
      if qryListD1_1BW.RecordCount = 0 then
      begin
        ShowMessage('등록된 상업송장이 없습니다.');
        mode_D1 := 1;
        exit;
      end;

      qryD1Edit.Close;
      qryD1Edit.Parameters.ParamByName('KEYY').Value := Maint_No;
      qryD1Edit.Parameters.ParamByName('SEQ').Value := SEQ;
      qryD1Edit.Parameters.ParamByName('DOC_GUBUN').Value := '1BW';
      qryD1Edit.ExecSQL;

      if not Assigned(UI_APPSPC_NEW_DOC3_frm) Then
         UI_APPSPC_NEW_DOC3_frm := TUI_APPSPC_NEW_DOC3_frm.Create(Application);
         with UI_APPSPC_NEW_DOC3_frm do
         begin
           qryListD2.Close;
           qryListD2.Parameters[0].Value := qryList.FIeldByName('MAINT_NO').AsString;
           qryListD2.Parameters[1].Value := '1BW';
           qryListD2.Parameters[2].Value := qryListD1_1BW.FieldByName('SEQ').AsInteger;
           SEQ := qryListD1_1BW.FieldByName('SEQ').AsInteger;
           qryListD2.Open;
           SelectRecordViewD1(3);

           if ShowModal = mrCancel then
              FreeAndNil(UI_APPSPC_NEW_DOC3_frm)
           ELSE
              FreeAndNil(UI_APPSPC_NEW_DOC3_frm);
         end;
    end;
  end;
end;

procedure TUI_APPSPC_NEW_frm.SelectRecordViewD1(DOC_GUBUN:Integer);
begin
  case DOC_GUBUN of
    //물품수령증명서 내용
    1:
    begin
      with UI_APPSPC_NEW_DOC1_frm do
      begin
        //문서번호
        edt_DOC_NO.Text := qryListD1_2AH.FieldByName('DOC_NO').AsString;
        //발급번호
        edt_LR_NO2.Text := qryListD1_2AH.FieldByName('LR_NO2').AsString;
        //HS부호
        msk_BSN_HSCODE.Text := qryListD1_2AH.FieldByName('BSN_HSCODE').AsString;
        //발급일자
        msk_ISS_DATE.Text := qryListD1_2AH.FieldByName('ISS_DATE').AsString;
        //인수일자
        msk_ACC_DATE.Text := qryListD1_2AH.FieldByName('ACC_DATE').AsString;
        //유효기일
        msk_VAL_DATE.Text := qryListD1_2AH.FieldByName('VAL_DATE').AsString;
        //공급자 상호코드
        edt_SE_CODE.Text := qryListD1_2AH.FieldByName('SE_CODE').AsString;
        //공급자 상호
        edt_SE_SNAME.Text := qryListD1_2AH.FieldByName('SE_SNAME').AsString;
        //물품수령인 상호 코드
        edt_BY_CODE.Text := qryListD1_2AH.FieldByName('BY_CODE').AsString;
        //물품수령인 상호
        edt_BY_SNAME.Text := qryListD1_2AH.FieldByName('BY_SNAME').AsString;
        //물품수령인 대표자명
        edt_BY_NAME.Text := qryListD1_2AH.FieldByName('BY_NAME').AsString;
        //물품수령인 전자서명
        edt_BY_ELEC.Text := qryListD1_2AH.FieldByName('BY_ELEC').AsString;
        //물품수령인 주소
        edt_BY_ADDR1.Text := qryListD1_2AH.FieldByName('BY_ADDR1').AsString;
        edt_BY_ADDR2.Text := qryListD1_2AH.FieldByName('BY_ADDR2').AsString;
        edt_BY_ADDR3.Text := qryListD1_2AH.FieldByName('BY_ADDR3').AsString;
        //내국신용장 번호
        edt_LR_NO.Text := qryListD1_2AH.FieldByName('LR_NO').AsString;
        //내국신용장 물품인도기일
        msk_GET_DATE.Text := qryListD1_2AH.FieldByName('GET_DATE').AsString;
        //내국신용장 유효기일
        msk_EXP_DATE.Text := qryListD1_2AH.FieldByName('EXP_DATE').AsString;
        //은행코드
        edt_LA_BANK.Text := qryListD1_2AH.FieldByName('LA_BANK').AsString;
        //은행명
        edt_LA_BANKNAME.Text := qryListD1_2AH.FieldByName('LA_BANKNAME').AsString;
        //은행지점
        edt_LA_BANKBU.Text := qryListD1_2AH.FieldByName('LA_BANKBU').AsString;
        //전자서명
        edt_LA_ELEC.Text := qryListD1_2AH.FieldByName('LA_ELEC').AsString;
        //매매기준율
        edt_EXCH.Text := qryListD1_2AH.FieldByName('EXCH').AsString;
        //개설금액
        edt_ISS_AMT.Text := FloatToStr(qryListD1_2AH.FieldByName('ISS_AMT').AsFloat);
        //개설금액 통화단위
        edt_ISS_AMTC.Text := qryListD1_2AH.FieldByName('ISS_AMTC').AsString;
        //개설금액(외화)
        edt_ISS_AMTU.Text := FloatToStr(qryListD1_2AH.FieldByName('ISS_AMTU').AsFloat);
        //인수금액
        edt_ISS_EXPAMT.Text := FloatToStr(qryListD1_2AH.FieldByName('ISS_EXPAMT').AsFloat);
        //인수금액 통화단위
        edt_ISS_EXPAMTC.Text := qryListD1_2AH.FieldByName('ISS_EXPAMTC').AsString;
        //인수금액(외화)
        edt_ISS_EXPAMTU.Text := FloatToStr(qryListD1_2AH.FieldByName('ISS_EXPAMTU').AsFloat);
        //총수량 단위
        edt_TOTCNTC.Text := qryListD1_2AH.FieldByName('TOTCNTC').AsString;
        //총수량
        edt_TOTCNT.Text := qryListD1_2AH.FieldByName('TOTCNT').AsString;
        //총금액 통화단위
        edt_ISS_TOTAMTC.Text := qryListD1_2AH.FieldByName('ISS_TOTAMTC').AsString;
        //총금액
        edt_ISS_TOTAMT.Text := FloatToStr(qryListD1_2AH.FieldByName('ISS_TOTAMT').AsFloat);
        //기타조건
        edt_REMARK2_1.Text := qryListD1_2AH.FieldByName('REMARK2_1').AsString;
        edt_REMARK2_2.Text := qryListD1_2AH.FieldByName('REMARK2_2').AsString;
        edt_REMARK2_3.Text := qryListD1_2AH.FieldByName('REMARK2_3').AsString;
        edt_REMARK2_4.Text := qryListD1_2AH.FieldByName('REMARK2_4').AsString;
        edt_REMARK2_5.Text := qryListD1_2AH.FieldByName('REMARK2_5').AsString;
        //참조사항
        edt_REMARK1_1.Text := qryListD1_2AH.FieldByName('REMARK1_1').AsString;
        edt_REMARK1_2.Text := qryListD1_2AH.FieldByName('REMARK1_2').AsString;
        edt_REMARK1_3.Text := qryListD1_2AH.FieldByName('REMARK1_3').AsString;
        edt_REMARK1_4.Text := qryListD1_2AH.FieldByName('REMARK1_4').AsString;
        edt_REMARK1_5.Text := qryListD1_2AH.FieldByName('REMARK1_5').AsString;
      end;
    end;
    //세금계산서 내용.
    2:
    begin
      with UI_APPSPC_NEW_DOC2_frm do
      begin
        //문서번호
        edt_DOC_NO.Text := qryListD1_2AJ.FieldByName('DOC_NO').AsString;
        //권
        edt_VB_RENO.Text := qryListD1_2AJ.FieldByName('VB_RENO').AsString;
        //호
        edt_VB_SENO.Text := qryListD1_2AJ.FieldByName('VB_SENO').AsString;
        //일련번호
        edt_VB_FSNO.Text := qryListD1_2AJ.FieldByName('VB_FSNO').AsString;
        //관리번호
        edt_VB_MAINTNO.Text := qryListD1_2AJ.FieldByName('VB_MAINTNO').AsString;
        //작성일
        msk_ISS_DATE.Text := qryListD1_2AJ.FieldByName('ISS_DATE').AsString;
        //참조번호
        edt_VB_DMNO.Text := qryListD1_2AJ.FieldByName('VB_DMNO').AsString;

        //공급자 상호코드
        edt_SE_CODE.Text := qryListD1_2AJ.FieldByName('SE_CODE').AsString;
        //공급자 상호
        edt_SE_SNAME.Text := qryListD1_2AJ.FieldByName('SE_SNAME').AsString;
        //공급자 대표자
        edt_SE_NAME.Text := qryListD1_2AJ.FieldByName('SE_NAME').AsString;
        //공급자 사업자등록번호
        msk_SE_SAUPNO.Text := qryListD1_2AJ.FieldByName('SE_SAUPNO').AsString;
        //공급자 전자서명
        edt_SE_ELEC.Text := qryListD1_2AJ.FieldByName('SE_ELEC').AsString;
        //공급자 주소
        edt_SE_ADDR1.Text := qryListD1_2AJ.FieldByName('SE_ADDR1').AsString;
        edt_SE_ADDR2.Text := qryListD1_2AJ.FieldByName('SE_ADDR2').AsString;
        edt_SE_ADDR3.Text := qryListD1_2AJ.FieldByName('SE_ADDR3').AsString;
        //공급자 업태
        edt_SG_UPTAI1_1.Text := qryListD1_2AJ.FieldByName('SG_UPTAI1_1').AsString;
        edt_SG_UPTAI1_2.Text := qryListD1_2AJ.FieldByName('SG_UPTAI1_2').AsString;
        edt_SG_UPTAI1_3.Text := qryListD1_2AJ.FieldByName('SG_UPTAI1_3').AsString;
        //공급자 종목
        edt_HN_ITEM1_1.Text := qryListD1_2AJ.FieldByName('HN_ITEM1_1').AsString;
        edt_HN_ITEM1_2.Text := qryListD1_2AJ.FieldByName('HN_ITEM1_2').AsString;
        edt_HN_ITEM1_3.Text := qryListD1_2AJ.FieldByName('HN_ITEM1_3').AsString;

        //공급자 상호코드
        edt_BY_CODE.Text := qryListD1_2AJ.FieldByName('BY_CODE').AsString;
        //공급자 상호
        edt_BY_SNAME.Text := qryListD1_2AJ.FieldByName('BY_SNAME').AsString;
        //공급자 대표자
        edt_BY_NAME.Text := qryListD1_2AJ.FieldByName('BY_NAME').AsString;
        //공급자 사업자등록번호
        msk_BY_SAUPNO.Text := qryListD1_2AJ.FieldByName('BY_SAUPNO').AsString;
        //공급자 전자서명
        edt_BY_ELEC.Text := qryListD1_2AJ.FieldByName('BY_ELEC').AsString;
        //공급자 주소
        edt_BY_ADDR1.Text := qryListD1_2AJ.FieldByName('BY_ADDR1').AsString;
        edt_BY_ADDR2.Text := qryListD1_2AJ.FieldByName('BY_ADDR2').AsString;
        edt_BY_ADDR3.Text := qryListD1_2AJ.FieldByName('BY_ADDR3').AsString;
        //공급자 업태
        edt_SG_UPTAI2_1.Text := qryListD1_2AJ.FieldByName('SG_UPTAI2_1').AsString;
        edt_SG_UPTAI2_2.Text := qryListD1_2AJ.FieldByName('SG_UPTAI2_2').AsString;
        edt_SG_UPTAI2_3.Text := qryListD1_2AJ.FieldByName('SG_UPTAI2_3').AsString;
        //공급자 종목
        edt_HN_ITEM2_1.Text := qryListD1_2AJ.FieldByName('HN_ITEM2_1').AsString;
        edt_HN_ITEM2_2.Text := qryListD1_2AJ.FieldByName('HN_ITEM2_2').AsString;
        edt_HN_ITEM2_3.Text := qryListD1_2AJ.FieldByName('HN_ITEM2_3').AsString;

        //수탁자 상호코드
        edt_AG_CODE.Text := qryListD1_2AJ.FieldByName('AG_CODE').AsString;
        //수탁자 상호
        edt_AG_SNAME.Text := qryListD1_2AJ.FieldByName('AG_SNAME').AsString;
        //수탁자 대표자
        edt_AG_NAME.Text := qryListD1_2AJ.FieldByName('AG_NAME').AsString;
        //수탁자 사업자등록번호
        msk_AG_SAUPNO.Text := qryListD1_2AJ.FieldByName('AG_SAUPNO').AsString;
        //수탁자 전자서명
        edt_AG_ELEC.Text := qryListD1_2AJ.FieldByName('AG_ELEC').AsString;
        //수탁자 주소
        edt_AG_ADDR1.Text := qryListD1_2AJ.FieldByName('AG_ADDR1').AsString;
        edt_AG_ADDR2.Text := qryListD1_2AJ.FieldByName('AG_ADDR2').AsString;
        edt_AG_ADDR3.Text := qryListD1_2AJ.FieldByName('AG_ADDR3').AsString;


        //현금 원화금액
        edt_PAI_AMT1.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMT1').AsFloat);
        //수표 원화금액
        edt_PAI_AMT2.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMT2').AsFloat);
        //어음 원화금액
        edt_PAI_AMT3.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMT3').AsFloat);
        //외상 원화금액
        edt_PAI_AMT4.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMT4').AsFloat);

        //현금 통화단위
        edt_PAI_AMTC1.Text := qryListD1_2AJ.FieldByName('PAI_AMTC1').AsString;
        //수표 통화단위                                                       
        edt_PAI_AMTC2.Text := qryListD1_2AJ.FieldByName('PAI_AMTC2').AsString;
        //어음 통화단위
        edt_PAI_AMTC3.Text := qryListD1_2AJ.FieldByName('PAI_AMTC3').AsString;
        //외상 통화단위
        edt_PAI_AMTC4.Text := qryListD1_2AJ.FieldByName('PAI_AMTC4').AsString;

        //현금 외화금액
        edt_PAI_AMTU1.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMTU1').AsFloat);
        //수표 외화금액
        edt_PAI_AMTU2.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMTU2').AsFloat);
        //어음 외화금액
        edt_PAI_AMTU3.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMTU3').AsFloat);
        //외상 외화금액
        edt_PAI_AMTU4.Text := FloatToStr(qryListD1_2AJ.FieldByName('PAI_AMTU4').AsFloat);

        //공급가액
        edt_ISS_EXPAMT.Text := FloatToStr(qryListD1_2AJ.FieldByName('ISS_EXPAMT').AsFloat);
        //영수/청구구분
        edt_VB_GUBUN.Text := qryListD1_2AJ.FieldByName('VB_GUBUN').AsString;
        //세액
        edt_ISS_EXPAMTU.Text := FloatToStr(qryListD1_2AJ.FieldByName('ISS_EXPAMTU').AsFloat);
        //공란수
        edt_VB_DETAILNO.Text := qryListD1_2AJ.FieldByName('VB_DETAILNO').AsString;
        //총수량단위
        edt_TOTCNTC.Text := qryListD1_2AJ.FieldByName('TOTCNTC').AsString;
        //총수량
        edt_TOTCNT.Text := FloatToStr(qryListD1_2AJ.FieldByName('TOTCNT').AsFloat);
        //비고
        edt_REMARK1_1.Text := qryListD1_2AJ.FieldByName('REMARK1_1').AsString;
        edt_REMARK1_2.Text := qryListD1_2AJ.FieldByName('REMARK1_2').AsString;
        edt_REMARK1_3.Text := qryListD1_2AJ.FieldByName('REMARK1_3').AsString;
        edt_REMARK1_4.Text := qryListD1_2AJ.FieldByName('REMARK1_4').AsString;
        edt_REMARK1_5.Text := qryListD1_2AJ.FieldByName('REMARK1_5').AsString;

      end;
    end;
    //상업송장 내용.
    3:
    begin
      with UI_APPSPC_NEW_DOC3_frm do
      begin
        //문서번호
        edt_DOC_NO.Text := qryListD1_1BW.FieldByName('DOC_NO').AsString;
        //발급일자
        msk_ISS_DATE.Text := qryListD1_1BW.FieldByName('ISS_DATE').AsString;
        //총수량단위
        edt_TOTCNTC.Text := qryListD1_1BW.FieldByName('TOTCNTC').AsString;
        //총수량
        edt_TOTCNT.Text := FloatToStr(qryListD1_1BW.FieldByName('TOTCNT').AsFloat);
        //총금액 통화단위
        edt_ISS_TOTAMTC.Text := qryListD1_1BW.FieldByName('ISS_TOTAMTC').AsString;
        //총금액 |'|
        edt_ISS_TOTAMT.Text := FloatToStr(qryListD1_1BW.FieldByName('ISS_TOTAMT').AsFloat);
        //항차
        edt_REMARK1_1.Text := qryListD1_1BW.FieldByName('REMARK1_1').AsString;
        //선명
        edt_REMARK1_2.Text := qryListD1_1BW.FieldByName('REMARK1_2').AsString;
        //선적지
        edt_REMARK1_3.Text := qryListD1_1BW.FieldByName('REMARK1_3').AsString;
        //도착지
        edt_REMARK1_4.Text := qryListD1_1BW.FieldByName('REMARK1_4').AsString;
        //인도조건
        edt_REMARK1_5.Text := qryListD1_1BW.FieldByName('REMARK1_5').AsString;
        //공급자 상호코드
        edt_SE_CODE.Text := qryListD1_1BW.FieldByName('SE_CODE').AsString;
        //공급자 상호
        edt_SE_SNAME.Text := qryListD1_1BW.FieldByName('SE_SNAME').AsString;
        //공급자 대표
        edt_SE_NAME.Text := qryListD1_1BW.FieldByName('SE_NAME').AsString;
        //공급자 주소
        edt_SE_ADDR1.Text := qryListD1_1BW.FieldByName('SE_ADDR1').AsString;
        edt_SE_ADDR2.Text := qryListD1_1BW.FieldByName('SE_ADDR2').AsString;
        edt_SE_ADDR3.Text := qryListD1_1BW.FieldByName('SE_ADDR3').AsString;
        //공급받는자 상호코드
        edt_BY_CODE.Text := qryListD1_1BW.FieldByName('BY_CODE').AsString;
        //공급받는자 상호
        edt_BY_SNAME.Text := qryListD1_1BW.FieldByName('BY_SNAME').AsString;
        //공급받는자 대표
        edt_BY_NAME.Text := qryListD1_1BW.FieldByName('BY_NAME').AsString;
        //공급받는자 주소
        edt_BY_ADDR1.Text := qryListD1_1BW.FieldByName('BY_ADDR1').AsString;
        edt_BY_ADDR2.Text := qryListD1_1BW.FieldByName('BY_ADDR2').AsString;
        edt_BY_ADDR3.Text := qryListD1_1BW.FieldByName('BY_ADDR3').AsString;


      end;


    end;
  end;
end;

procedure TUI_APPSPC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;            
  IF DMMssql.inTrans Then DMMssql.RollbackTrans;

  Action := caFree;
  UI_APPSPC_NEW_frm := nil;
end;

procedure TUI_APPSPC_NEW_frm.btnEditClick(Sender: TObject);
begin
  inherited;

  if qryList.RecordCount = 0 then
  begin
    ShowMessage('수정할 문서가 존재하지 않습니다.');
    exit;
  end;

  mode := 2;//수정모
  ReadWrite := True;
  BtnControl(ReadWrite);
  
  sPanel29.Visible := True;
  EnableControl(sPanel47);
  EnableControl(sPanel50);
  EnableControl(sPanel39);
  EnableControl(sPanel40);
  EnableControl(sPanel32); 
  EnableControl(sPanel5,False);//데이터 조회 부분

  com_CP_CODE.Enabled := False;
  edt_BGM1.Enabled := False;
  edt_BGM4.Enabled := False;

  com_MESSAGE1.Enabled := True;
  com_MESSAGE2.Enabled := True;

  EnableControl(sPanel63);
  EnableControl(sPanel41);
  EnableControl(sPanel52);
  EnableControl(sPanel11);

  Maint_No := edt_MAINT_NO.Text;

  if Not DMMssql.inTrans then
    DMMssql.BeginTrans;
end;

procedure TUI_APPSPC_NEW_frm.btnCancelClick(Sender: TObject);
var
  KEYY:String;
begin
  inherited;
  if DMMssql.inTrans then
    DMMssql.RollbackTrans;


  qryList.First;
  ReadWrite := False;
  BtnControl(ReadWrite);

  EnableControl(sPanel47, False);
  EnableControl(sPanel50, False);
  EnableControl(sPanel39, False);
  EnableControl(sPanel40, False);
  EnableControl(sPanel32, False);  
  EnableControl(sPanel5);//데이터 조회 부분
  com_MESSAGE1.Enabled := False;
  com_MESSAGE2.Enabled := False;

  EnableControl(sPanel63, False);
  EnableControl(sPanel41, False);
  EnableControl(sPanel52, False);
  EnableControl(sPanel11, False);
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPSPC_NEW_frm.btnCopyClick(Sender: TObject);
var
  sMaint_No_Old:String;
  sSQL:AnsiString;
begin
  inherited;

  if qryList.RecordCount = 0 then
  begin
    ShowMessage('복사할 문서가 존재하지 않습니다.');
    exit;
  end;

  mode := 3;
  sMaint_No_Old := edt_MAINT_NO.Text;
  ReadWrite := True;
  BtnControl(ReadWrite);
  sPanel29.Visible := True;
  EnableControl(sPanel47);
  EnableControl(sPanel50);
  EnableControl(sPanel39);
  EnableControl(sPanel40);
  EnableControl(sPanel32);
  EnableControl(sPanel5,False);//데이터 조회 부분
  com_CP_CODE.Enabled := False;
  com_MESSAGE1.Enabled := True;
  com_MESSAGE2.Enabled := True;

  EnableControl(sPanel63);
  EnableControl(sPanel41);
  EnableControl(sPanel52);
  EnableControl(sPanel11);
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPSPC');
  edt_BGM1.Enabled := False;
  edt_BGM4.Enabled := False;
  msk_DATEE.Text := FormatDateTime('YYYYMMDD', Now);

//  sSQL := 'INSERT INTO APPSPC_H(MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2, CHK3, BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, '+'CP_ACCOUNTNO, CP_NAME1, CP_NAME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACCOUNTNO1, CP_ADD_NAME1, '+'CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_NAME2, CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAME, APP_SAUPNO, APP_NAME, '+'APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADDR3, CP_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_EMAIL2, DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT)';
//  sSQL := sSQL + 'SELECT '+edt_MAINT_NO.Text+', USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2, CHK3, BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, '+'CP_ACCOUNTNO, CP_NAME1, CP_NAME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACCOUNTNO1, CP_ADD_NAME1, '+'CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_NAME2, CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAME, APP_SAUPNO, APP_NAME, '+'APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADDR3, CP_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_EMAIL2, DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT'
//               +'FROM APPSPC_H WHERE MAINT_NO = :OLD';
//  with TADOQuery.Create(Nil) do
//  begin
//    try
//      Connection := DMMssql.KISConnect;
//      SQL.Text := sSQL;
//      ExecSQL;
//    finally
//      Close;
//      Free;
//    end;
//  end;

    with spCopyAPPSPC do
    begin
      Close;
//     Parameters.ParamByName('@KEYY').Value := 'TESTZZZ ';
      //@OrgMaint_No
      Parameters[1].Value := sMaint_No_Old;
      //CpyMaint_No
      Parameters[2].Value := edt_MAINT_NO.Text;

      if not DMMssql.inTrans then
        DMMssql.BeginTrans;

      try
        ExecProc;
      except
        on e:Exception do
        begin
          MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
        end;
      end;
    end;

    Maint_No := edt_MAINT_NO.Text;
  sPageControl1.ActivePageIndex := 0;

end;

procedure TUI_APPSPC_NEW_frm.btnSaveClick(Sender: TObject);
var
  SQLCreate,SQLCreate_D1 : TSQLCreate;
  DOC_NO,LR_NO, ISS_DATE, LA_BANKBUCODE, LA_BANKNAMEP,LA_TYPE, LA_BANKBUP : String;
begin
  inherited;
  //유효성검사
  Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
  if Dlg_ErrorMessage_frm.Run_ErrorMessage( '판패대금추심(매입)의뢰서' ,ErrorCheck ) Then
  begin
    FreeAndNil(Dlg_ErrorMessage_frm);
    Exit;
  end;
    SQLCreate := TSQLCreate.Create;
  case mode of

    1,3://신규,복사 저장
    begin
      MAINT_NO := edt_MAINT_NO.Text;
      //========================================================================
      //공통사항
      //========================================================================

      with SQLCreate do
      begin
        DMLType := dmlUpdate;
        SQLCreate.SQLHeader('APPSPC_H');
        //관리번호
        ADDWhere('MAINT_NO',MAINT_NO);
        //문서상황(0. 임시 1. 저장)
        ADDValue('CHK2', '1');
//        CHK1,2,3처리할것.
        //문서기능
        case com_MESSAGE1.ItemIndex of
          0: ADDValue('MESSAGE1','7');
          1: ADDValue('MESSAGE1','9');
        end;

        //문서유형
        case com_MESSAGE2.ItemIndex of
          0: ADDValue('MESSAGE2','AB');
          1: ADDValue('MESSAGE2','AP');
          2: ADDValue('MESSAGE2','NA');
          3: ADDValue('MESSAGE2','RE');
        end;

        //등록일자
        ADDValue('DATEE',msk_DATEE.Text);
        //사용자
        ADDValue('USER_ID',LoginData.sID);
        //신청일자
        ADDValue('APP_DATE',msk_APP_DATE.Text);
        //전자문서구분
        case com_BGM_GUBUN.ItemIndex of
          0: ADDValue('BGM_GUBUN', '2BK');
          1: ADDValue('BGM_GUBUN', '2BJ');
        end;
        //문서번호
        ADDValue('BGM1',edt_BGM1.Text);
        ADDValue('BGM2',edt_BGM2.Text);
        ADDValue('BGM3',edt_BGM3.Text);
        ADDValue('BGM4',edt_BGM4.Text);
        //추심(매입)코드
        case com_CP_CODE.ItemIndex of
          0: ADDValue('CP_CODE','2BB');
          1: ADDValue('CP_CODE','2BA');
        end;
        //추심(매입) 신청번호
        ADDValue('CP_NO',edt_CP_NO.Text);

        //========================================================================
        //신청인
        //========================================================================

        //신청인 상호코드
        ADDValue('APP_CODE',edt_APP_CODE.Text);
        //신청인 상호
        ADDValue('APP_SNAME',edt_APP_SNAME.Text);
        //신청인 대표자
        ADDValue('APP_NAME',edt_APP_NAME.Text);
        //신청인 사업자등록번호
        ADDValue('APP_SAUPNO',msk_APP_SAUPNO.Text);
        //신청인 주소
        ADDValue('APP_ADDR1',edt_APP_ADDR1.Text);
        ADDValue('APP_ADDR2',edt_APP_ADDR2.Text);
        ADDValue('APP_ADDR3',edt_APP_ADDR3.Text);
        //신청인 전자서명
        ADDValue('APP_ELEC',edt_APP_ELEC.Text);

        //========================================================================
        //구매자
        //========================================================================

        //구매자 사업자등록번호
        ADDValue('DF_SAUPNO', msk_DF_SAUPNO.Text);
        //구매자 상호
        ADDValue('DF_NAME1', edt_DF_NAME1.Text);
        ADDValue('DF_NAME2', edt_DF_NAME2.Text);
        ADDValue('DF_NAME3', edt_DF_NAME3.Text);
        //구매자 이메일
        ADDValue('DF_EMAIL1', edt_DF_EMAIL1.Text);
        ADDValue('DF_EMAIL2', edt_DF_EMAIL2.Text);

        //========================================================================
        //추심은행
        //========================================================================

        //은행코드
        ADDValue('CP_BANK',edt_CP_BANK.Text);
        //은행명
        ADDValue('CP_BANKNAME', edt_CP_BANKNAME.Text);
        //지점명
        ADDValue('CP_BANKBU',edt_CP_BANKBU.Text);
        //계좌번호
        ADDValue('CP_ACCOUNTNO',edt_CP_ACCOUNTNO.Text);
        //계좌통화
        ADDValue('CP_CURR',edt_CP_CURR.Text);
        //계좌주
        ADDValue('CP_NAME1',edt_CP_NAME1.Text);
        ADDValue('CP_NAME2',edt_CP_NAME2.Text);
        //추심금액(원화)
        ADDValue('CP_AMT',StrToFloat(edt_CP_AMT.Text));
        //적용환율
        ADDValue('CP_CUX',StrToFloat(edt_CP_CUX.Text));
        //추심금액(외화)
        ADDValue('CP_AMTU',StrToFloat(edt_CP_AMTU.Text));
        //추심금액(외화) 통화단위
        ADDValue('CP_AMTC',edt_CP_AMTC.Text);

        //========================================================================
        //추가계좌
        //========================================================================
        //추가계좌번호1
        ADDValue('CP_ADD_ACCOUNTNO1',edt_CP_ADD_ACCOUNTNO1.Text);
        //추가계좌주1
        ADDValue('CP_ADD_NAME1',edt_CP_ADD_NAME1.Text);
        //계좌통화1
        ADDValue('CP_ADD_CURR1',edt_CP_ADD_CURR1.Text);
        //추가계좌번호2
        ADDValue('CP_ADD_ACCOUNTNO2',edt_CP_ADD_ACCOUNTNO2.Text);
        //추가계좌주2
        ADDValue('CP_ADD_NAME2',edt_CP_ADD_NAME2.Text);
        //계좌통화2
        ADDValue('CP_ADD_CURR2',edt_CP_ADD_CURR2.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;

      if mode = 1 then
      begin
        //내국신용장
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlInsert;
          SQLHeader('APPSPC_D1');

          ADDValue('KEYY',MAINT_NO);
          ADDValue('DOC_GUBUN','2AP');
          ADDValue('SEQ',1);

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;

          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LR_GUBUN','CRG');
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end
      else if mode = 3 then
      begin
        //내국신용장
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlUpdate;
          SQLHeader('APPSPC_D1');

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;

          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LR_GUBUN','CRG');
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end;

      //신규-저장 시 데이터 전부 저장하고 커밋.
      if DMMssql.inTrans then
        DMMssql.CommitTrans;

      qryList.Close;
      qryList.Open;

    end;
    2://수정 저장
    begin
      MAINT_NO := edt_MAINT_NO.Text;
      //========================================================================
      //공통사항
      //========================================================================
      with SQLCreate do
      begin
        DMLType := dmlUpdate;
        ADDWhere('MAINT_NO',Maint_No);
        SQLCreate.SQLHeader('APPSPC_H');

        //관리번호
        ADDValue('MAINT_NO',MAINT_NO);
        //0.임시 , 1.저장
        ADDValue('CHK2', '1');
        //문서기능
        case com_MESSAGE1.ItemIndex of
          0: ADDValue('MESSAGE1','7');
          1: ADDValue('MESSAGE1','9');
        end;

        //문서유형
        case com_MESSAGE2.ItemIndex of
          0: ADDValue('MESSAGE2','AB');
          1: ADDValue('MESSAGE2','AP');
          2: ADDValue('MESSAGE2','NA');
          3: ADDValue('MESSAGE2','RE');
        end;
        //등록일자
        ADDValue('DATEE',msk_DATEE.Text); 
        //신청일자
        ADDValue('APP_DATE',msk_APP_DATE.Text);
        //사용자
        ADDValue('USER_ID',LoginData.sID);
        //전자문서구분
        case com_BGM_GUBUN.ItemIndex of
          0: ADDValue('BGM_GUBUN', '2BK');
          1: ADDValue('BGM_GUBUN', '2BJ');
        end;
        //문서번호
        ADDValue('BGM1',edt_BGM1.Text);
        ADDValue('BGM2',edt_BGM2.Text);
        ADDValue('BGM3',edt_BGM3.Text);
        ADDValue('BGM4',edt_BGM4.Text);
        //추심(매입)코드
        case com_CP_CODE.ItemIndex of
          0: ADDValue('CP_CODE','2BB');
          1: ADDValue('CP_CODE','2BA');
        end;
        //추심(매입) 신청번호
        ADDValue('CP_NO',edt_CP_NO.Text);

        //========================================================================
        //신청인
        //========================================================================

        //신청인 상호코드
        ADDValue('APP_CODE',edt_APP_CODE.Text);
        //신청인 상호
        ADDValue('APP_SNAME',edt_APP_SNAME.Text);
        //신청인 대표자
        ADDValue('APP_NAME',edt_APP_NAME.Text);
        //신청인 사업자등록번호
        ADDValue('APP_SAUPNO',msk_APP_SAUPNO.Text);
        //신청인 주소
        ADDValue('APP_ADDR1',edt_APP_ADDR1.Text);
        ADDValue('APP_ADDR2',edt_APP_ADDR2.Text);
        ADDValue('APP_ADDR3',edt_APP_ADDR3.Text);
        //신청인 전자서명
        ADDValue('APP_ELEC',edt_APP_ELEC.Text);

        //========================================================================
        //구매자
        //========================================================================

        //구매자 사업자등록번호
        ADDValue('DF_SAUPNO', msk_DF_SAUPNO.Text);
        //구매자 상호
        ADDValue('DF_NAME1', edt_DF_NAME1.Text);
        ADDValue('DF_NAME2', edt_DF_NAME2.Text);
        ADDValue('DF_NAME3', edt_DF_NAME3.Text);
        //구매자 이메일
        ADDValue('DF_EMAIL1', edt_DF_EMAIL1.Text);
        ADDValue('DF_EMAIL2', edt_DF_EMAIL2.Text);

        //========================================================================
        //추심은행
        //========================================================================

        //은행코드
        ADDValue('CP_BANK',edt_CP_BANK.Text);
        //은행명
        ADDValue('CP_BANKNAME', edt_CP_BANKNAME.Text);
        //지점명
        ADDValue('CP_BANKBU',edt_CP_BANKBU.Text);
        //계좌번호
        ADDValue('CP_ACCOUNTNO',edt_CP_ACCOUNTNO.Text);
        //계좌통화
        ADDValue('CP_CURR',edt_CP_CURR.Text);
        //계좌주
        ADDValue('CP_NAME1',edt_CP_NAME1.Text);
        ADDValue('CP_NAME2',edt_CP_NAME2.Text);
        //추심금액(원화)
        ADDValue('CP_AMT',StrToFloat(edt_CP_AMT.Text));
        //적용환율
        ADDValue('CP_CUX',StrToFloat(edt_CP_CUX.Text));
        //추심금액(외화)
        ADDValue('CP_AMTU',StrToFloat(edt_CP_AMTU.Text));
        //추심금액(외화) 통화단위
        ADDValue('CP_AMTC',edt_CP_AMTC.Text);

        //========================================================================
        //추가계좌
        //========================================================================
        //추가계좌번호1
        ADDValue('CP_ADD_ACCOUNTNO1',edt_CP_ADD_ACCOUNTNO1.Text);
        //추가계좌주1
        ADDValue('CP_ADD_NAME1',edt_CP_ADD_NAME1.Text);
        //계좌통화1
        ADDValue('CP_ADD_CURR1',edt_CP_ADD_CURR1.Text);
        //추가계좌번호2
        ADDValue('CP_ADD_ACCOUNTNO2',edt_CP_ADD_ACCOUNTNO2.Text);
        //추가계좌주2
        ADDValue('CP_ADD_NAME2',edt_CP_ADD_NAME2.Text);
        //계좌통화2
        ADDValue('CP_ADD_CURR2',edt_CP_ADD_CURR2.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      END;

      //내국신용장 있을때
      if qryListD1_2AP.RecordCount > 0 then
      begin
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlUpdate;
          SQLHeader('APPSPC_D1');
          ADDWhere('KEYY',Maint_No);
          ADDWhere('DOC_GUBUN','2AP');

          ADDValue('KEYY',MAINT_NO);

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;
          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end else //내국신용장이 없었는데 수정으로 넣을때.
      begin
      SQLCreate_D1 := TSQLCreate.Create;

      with SQLCreate_D1 do
      begin
        DMLType := dmlInsert;
        SQLHeader('APPSPC_D1');

        ADDValue('KEYY',MAINT_NO);
        ADDValue('DOC_GUBUN','2AP');
        ADDValue('SEQ',1);

        //내국신용장 문서번호
        ADDValue('DOC_NO',edt_DOC_NO.Text);
        //내국신용장 종류
        case com_LA_TYPE.ItemIndex of
          0: ADDValue('LA_TYPE','2AA');
          1: ADDValue('LA_TYPE','2AB');
          2: ADDValue('LA_TYPE','2AC');
        end;
        ADDValue('ISS_DATE', msk_ISS_DATE.Text);
        ADDValue('LR_NO', edt_LR_NO.Text);
        ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
        ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
        ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate_D1.CreateSQL;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;
      end;
      end;

      //수정-저장 시 데이터 전부 저장하고 커밋.
      if DMMssql.inTrans then
        DMMssql.CommitTrans;

      qryList.Close;
      qryList.Open;

    end;

  end;

  ReadWrite := False;
  BtnControl(ReadWrite);

  EnableControl(sPanel47, False);
  EnableControl(sPanel50, False);
  EnableControl(sPanel39, False);
  EnableControl(sPanel40, False);
  EnableControl(sPanel32, False); 
  EnableControl(sPanel5);//데이터 조회 부분

  com_MESSAGE1.Enabled := False;
  com_MESSAGE2.Enabled := False;


  EnableControl(sPanel63, False);
  EnableControl(sPanel41, False);
  EnableControl(sPanel52, False);
  EnableControl(sPanel11, False);  
  sPageControl1.ActivePageIndex := 0;

end;

procedure TUI_APPSPC_NEW_frm.BtnControl(RW:Boolean);
begin

    btnNew.Enabled := not ReadWrite;
    btnEdit.Enabled := not ReadWrite;
    btnDel.Enabled := not ReadWrite;
    btnCopy.Enabled := not ReadWrite;
    sDBGrid3.Enabled := not ReadWrite;
    sBitBtn5.Enabled := not ReadWrite;
    btnTemp.Enabled := ReadWrite;
    btnSave.Enabled := ReadWrite;
    btnCancel.Enabled := ReadWrite;
    sPanel29.Visible := ReadWrite;
    EnableControl(sPanel52, ReadWrite);
    EnableControl(sPanel11, ReadWrite);
    EnableControl(sPanel41, ReadWrite);
//  신규,수정,복사  -> 임시,저장,취소 활성화
//                  -> 신규, 수정,삭제,복사 비활성화
//--------------------------------------------
//  임시,저장,취소  -> 신규,수정,삭제,복사 활성화
//            -> 임시,저장,취소 비활성화
end;

procedure TUI_APPSPC_NEW_frm.btnTempClick(Sender: TObject);
var
  SQLCreate,SQLCreate_D1 : TSQLCreate;
  DOC_NO,LR_NO, ISS_DATE, LA_BANKBUCODE, LA_BANKNAMEP,LA_TYPE, LA_BANKBUP : String;
begin
  inherited;
  SQLCreate := TSQLCreate.Create;
  case mode of

    1,3://신규,복사 저장
    begin
      MAINT_NO := edt_MAINT_NO.Text;
      //========================================================================
      //공통사항
      //========================================================================

      with SQLCreate do
      begin
        DMLType := dmlUpdate;
        SQLCreate.SQLHeader('APPSPC_H');

        //관리번호
        ADDWhere('MAINT_NO',MAINT_NO);
        //문서기능
        case com_MESSAGE1.ItemIndex of
          0: ADDValue('MESSAGE1','7');
          1: ADDValue('MESSAGE1','9');
        end;
        //문서유형
        case com_MESSAGE2.ItemIndex of
          0: ADDValue('MESSAGE2','AB');
          1: ADDValue('MESSAGE2','AP');
          2: ADDValue('MESSAGE2','NA');
          3: ADDValue('MESSAGE2','RE');
        end;
        //0.임시저장
        ADDValue('CHK2', '0'); 
        //신청일자
        ADDValue('APP_DATE',msk_APP_DATE.Text);
        //등록일자
        ADDValue('DATEE',msk_DATEE.Text);
        //사용자
        ADDValue('USER_ID',LoginData.sID);
        //전자문서구분
        case com_BGM_GUBUN.ItemIndex of
          0: ADDValue('BGM_GUBUN', '2BK');
          1: ADDValue('BGM_GUBUN', '2BJ');
        end;
        //문서번호
        ADDValue('BGM1',edt_BGM1.Text);
        ADDValue('BGM2',edt_BGM2.Text);
        ADDValue('BGM3',edt_BGM3.Text);
        ADDValue('BGM4',edt_BGM4.Text);
        //추심(매입)코드
        case com_CP_CODE.ItemIndex of
          0: ADDValue('CP_CODE','2BB');
          1: ADDValue('CP_CODE','2BA');
        end;
        //추심(매입) 신청번호
        ADDValue('CP_NO',edt_CP_NO.Text);

        //========================================================================
        //신청인
        //========================================================================

        //신청인 상호코드
        ADDValue('APP_CODE',edt_APP_CODE.Text);
        //신청인 상호
        ADDValue('APP_SNAME',edt_APP_SNAME.Text);
        //신청인 대표자
        ADDValue('APP_NAME',edt_APP_NAME.Text);
        //신청인 사업자등록번호
        ADDValue('APP_SAUPNO',msk_APP_SAUPNO.Text);
        //신청인 주소
        ADDValue('APP_ADDR1',edt_APP_ADDR1.Text);
        ADDValue('APP_ADDR2',edt_APP_ADDR2.Text);
        ADDValue('APP_ADDR3',edt_APP_ADDR3.Text);
        //신청인 전자서명
        ADDValue('APP_ELEC',edt_APP_ELEC.Text);

        //========================================================================
        //구매자
        //========================================================================

        //구매자 사업자등록번호
        ADDValue('DF_SAUPNO', msk_DF_SAUPNO.Text);
        //구매자 상호
        ADDValue('DF_NAME1', edt_DF_NAME1.Text);
        ADDValue('DF_NAME2', edt_DF_NAME2.Text);
        ADDValue('DF_NAME3', edt_DF_NAME3.Text);
        //구매자 이메일
        ADDValue('DF_EMAIL1', edt_DF_EMAIL1.Text);
        ADDValue('DF_EMAIL2', edt_DF_EMAIL2.Text);

        //========================================================================
        //추심은행
        //========================================================================

        //은행코드
        ADDValue('CP_BANK',edt_CP_BANK.Text);
        //은행명
        ADDValue('CP_BANKNAME', edt_CP_BANKNAME.Text);
        //지점명
        ADDValue('CP_BANKBU',edt_CP_BANKBU.Text);
        //계좌번호
        ADDValue('CP_ACCOUNTNO',edt_CP_ACCOUNTNO.Text);
        //계좌통화
        ADDValue('CP_CURR',edt_CP_CURR.Text);
        //계좌주
        ADDValue('CP_NAME1',edt_CP_NAME1.Text);
        ADDValue('CP_NAME2',edt_CP_NAME2.Text);
        //추심금액(원화)
        ADDValue('CP_AMT',StrToFloat(edt_CP_AMT.Text));
        //적용환율
        ADDValue('CP_CUX',StrToFloat(edt_CP_CUX.Text));
        //추심금액(외화)
        ADDValue('CP_AMTU',StrToFloat(edt_CP_AMTU.Text));
        //추심금액(외화) 통화단위
        ADDValue('CP_AMTC',edt_CP_AMTC.Text);

        //========================================================================
        //추가계좌
        //========================================================================
        //추가계좌번호1
        ADDValue('CP_ADD_ACCOUNTNO1',edt_CP_ADD_ACCOUNTNO1.Text);
        //추가계좌주1
        ADDValue('CP_ADD_NAME1',edt_CP_ADD_NAME1.Text);
        //계좌통화1
        ADDValue('CP_ADD_CURR1',edt_CP_ADD_CURR1.Text);
        //추가계좌번호2
        ADDValue('CP_ADD_ACCOUNTNO2',edt_CP_ADD_ACCOUNTNO2.Text);
        //추가계좌주2
        ADDValue('CP_ADD_NAME2',edt_CP_ADD_NAME2.Text);
        //계좌통화2
        ADDValue('CP_ADD_CURR2',edt_CP_ADD_CURR2.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;


      if mode = 1 then
      begin
        //내국신용장
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlInsert;
          SQLHeader('APPSPC_D1');

          ADDValue('KEYY',MAINT_NO);
          ADDValue('DOC_GUBUN','2AP');
          ADDValue('SEQ',1);

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;

          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LR_GUBUN','CRG');
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end
      else if mode = 3 then
      begin
        //내국신용장
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlUpdate;
          SQLHeader('APPSPC_D1');

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;

          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LR_GUBUN','CRG');
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end;

      //신규-저장 시 데이터 전부 저장하고 커밋.
      if DMMssql.inTrans then
        DMMssql.CommitTrans;

      qryList.Close;
      qryList.Open;

    end;
    2://수정 저장
    begin
      MAINT_NO := edt_MAINT_NO.Text;
      //========================================================================
      //공통사항
      //========================================================================
      with SQLCreate do
      begin
        DMLType := dmlUpdate;
        ADDWhere('MAINT_NO',Maint_No);
        SQLCreate.SQLHeader('APPSPC_H');

        //관리번호
        ADDValue('MAINT_NO',MAINT_NO);
        //0.임시저장
        ADDValue('CHK2', '0');

        //문서기능
        case com_MESSAGE1.ItemIndex of
          0: ADDValue('MESSAGE1','7');
          1: ADDValue('MESSAGE1','9');
        end;

        //문서유형
        case com_MESSAGE2.ItemIndex of
          0: ADDValue('MESSAGE2','AB');
          1: ADDValue('MESSAGE2','AP');
          2: ADDValue('MESSAGE2','NA');
          3: ADDValue('MESSAGE2','RE');
        end;                             
        //신청일자
        ADDValue('APP_DATE',msk_APP_DATE.Text);
        //등록일자
        ADDValue('DATEE',msk_DATEE.Text);
        //사용자
        ADDValue('USER_ID',LoginData.sID);
        //전자문서구분
        case com_BGM_GUBUN.ItemIndex of
          0: ADDValue('BGM_GUBUN', '2BK');
          1: ADDValue('BGM_GUBUN', '2BJ');
        end;
        //문서번호
        ADDValue('BGM1',edt_BGM1.Text);
        ADDValue('BGM2',edt_BGM2.Text);
        ADDValue('BGM3',edt_BGM3.Text);
        ADDValue('BGM4',edt_BGM4.Text);
        //추심(매입)코드
        case com_CP_CODE.ItemIndex of
          0: ADDValue('CP_CODE','2BB');
          1: ADDValue('CP_CODE','2BA');
        end;
        //추심(매입) 신청번호
        ADDValue('CP_NO',edt_CP_NO.Text);

        //========================================================================
        //신청인
        //========================================================================

        //신청인 상호코드
        ADDValue('APP_CODE',edt_APP_CODE.Text);
        //신청인 상호
        ADDValue('APP_SNAME',edt_APP_SNAME.Text);
        //신청인 대표자
        ADDValue('APP_NAME',edt_APP_NAME.Text);
        //신청인 사업자등록번호
        ADDValue('APP_SAUPNO',msk_APP_SAUPNO.Text);
        //신청인 주소
        ADDValue('APP_ADDR1',edt_APP_ADDR1.Text);
        ADDValue('APP_ADDR2',edt_APP_ADDR2.Text);
        ADDValue('APP_ADDR3',edt_APP_ADDR3.Text);
        //신청인 전자서명
        ADDValue('APP_ELEC',edt_APP_ELEC.Text);

        //========================================================================
        //구매자
        //========================================================================

        //구매자 사업자등록번호
        ADDValue('DF_SAUPNO', msk_DF_SAUPNO.Text);
        //구매자 상호
        ADDValue('DF_NAME1', edt_DF_NAME1.Text);
        ADDValue('DF_NAME2', edt_DF_NAME2.Text);
        ADDValue('DF_NAME3', edt_DF_NAME3.Text);
        //구매자 이메일
        ADDValue('DF_EMAIL1', edt_DF_EMAIL1.Text);
        ADDValue('DF_EMAIL2', edt_DF_EMAIL2.Text);

        //========================================================================
        //추심은행
        //========================================================================

        //은행코드
        ADDValue('CP_BANK',edt_CP_BANK.Text);
        //은행명
        ADDValue('CP_BANKNAME', edt_CP_BANKNAME.Text);
        //지점명
        ADDValue('CP_BANKBU',edt_CP_BANKBU.Text);
        //계좌번호
        ADDValue('CP_ACCOUNTNO',edt_CP_ACCOUNTNO.Text);
        //계좌통화
        ADDValue('CP_CURR',edt_CP_CURR.Text);
        //계좌주
        ADDValue('CP_NAME1',edt_CP_NAME1.Text);
        ADDValue('CP_NAME2',edt_CP_NAME2.Text);
        //추심금액(원화)
        ADDValue('CP_AMT',StrToFloat(edt_CP_AMT.Text));
        //적용환율
        ADDValue('CP_CUX',StrToFloat(edt_CP_CUX.Text));
        //추심금액(외화)
        ADDValue('CP_AMTU',StrToFloat(edt_CP_AMTU.Text));
        //추심금액(외화) 통화단위
        ADDValue('CP_AMTC',edt_CP_AMTC.Text);

        //========================================================================
        //추가계좌
        //========================================================================
        //추가계좌번호1
        ADDValue('CP_ADD_ACCOUNTNO1',edt_CP_ADD_ACCOUNTNO1.Text);
        //추가계좌주1
        ADDValue('CP_ADD_NAME1',edt_CP_ADD_NAME1.Text);
        //계좌통화1
        ADDValue('CP_ADD_CURR1',edt_CP_ADD_CURR1.Text);
        //추가계좌번호2
        ADDValue('CP_ADD_ACCOUNTNO2',edt_CP_ADD_ACCOUNTNO2.Text);
        //추가계좌주2
        ADDValue('CP_ADD_NAME2',edt_CP_ADD_NAME2.Text);
        //계좌통화2
        ADDValue('CP_ADD_CURR2',edt_CP_ADD_CURR2.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      END;

      //내국신용장 있을때
      if qryListD1_2AP.RecordCount > 0 then
      begin
        SQLCreate_D1 := TSQLCreate.Create;

        with SQLCreate_D1 do
        begin
          DMLType := dmlUpdate;
          SQLHeader('APPSPC_D1');
          ADDWhere('KEYY',Maint_No);
          ADDWhere('DOC_GUBUN','2AP');

          ADDValue('KEYY',MAINT_NO);

          //내국신용장 문서번호
          ADDValue('DOC_NO',edt_DOC_NO.Text);
          //내국신용장 종류
          case com_LA_TYPE.ItemIndex of
            0: ADDValue('LA_TYPE','2AA');
            1: ADDValue('LA_TYPE','2AB');
            2: ADDValue('LA_TYPE','2AC');
          end;
          ADDValue('ISS_DATE', msk_ISS_DATE.Text);
          ADDValue('LR_NO', edt_LR_NO.Text);
          ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
          ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
          ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

          with TADOQuery.Create(nil) do
          begin
            try
              Connection := DMMssql.KISConnect;
              SQL.Text := SQLCreate_D1.CreateSQL;
              ExecSQL;
            finally
              Close;
              Free;
            end;
          end;

        end;
      end else //내국신용장이 없었는데 수정으로 넣을때.
      begin
      SQLCreate_D1 := TSQLCreate.Create;

      with SQLCreate_D1 do
      begin
        DMLType := dmlInsert;
        SQLHeader('APPSPC_D1');

        ADDValue('KEYY',MAINT_NO);
        ADDValue('DOC_GUBUN','2AP');
        ADDValue('SEQ',1);

        //내국신용장 문서번호
        ADDValue('DOC_NO',edt_DOC_NO.Text);
        //내국신용장 종류
        case com_LA_TYPE.ItemIndex of
          0: ADDValue('LA_TYPE','2AA');
          1: ADDValue('LA_TYPE','2AB');
          2: ADDValue('LA_TYPE','2AC');
        end;
        ADDValue('ISS_DATE', msk_ISS_DATE.Text);
        ADDValue('LR_NO', edt_LR_NO.Text);
        ADDValue('LA_BANKBUCODE', edt_LA_BANKBUCODE.Text);
        ADDValue('LA_BANKNAMEP', edt_LA_BANKNAMEP.Text);
        ADDValue('LA_BANKBUP', edt_LA_BANKBUP.Text);

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate_D1.CreateSQL;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;
      end;
      end;

      //수정-저장 시 데이터 전부 저장하고 커밋.
      if DMMssql.inTrans then
        DMMssql.CommitTrans;

      qryList.Close;
      qryList.Open;

    end;

  end;

  ReadWrite := False;
  BtnControl(ReadWrite);

  EnableControl(sPanel47, False);
  EnableControl(sPanel50, False);
  EnableControl(sPanel39, False);
  EnableControl(sPanel40, False);
  EnableControl(sPanel32, False);  
  EnableControl(sPanel5);//데이터 조회 부분
  com_MESSAGE1.Enabled := False;
  com_MESSAGE2.Enabled := False;


  EnableControl(sPanel63, False);
  EnableControl(sPanel41, False);
  EnableControl(sPanel52, False);
  EnableControl(sPanel11, False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPSPC_NEW_frm.btnDelClick(Sender: TObject);
var
  SQLCreate:TSQLCreate;
  MAINT_NO:String;
begin
  inherited;
  if qryList.RecordCount = 0 then
  begin
    ShowMessage('삭제할 문서가 존재하지 않습니다.');
    exit;
  end;

  SQLCreate := TSQLCreate.Create;
  MAINT_NO := qryList.FieldByName('MAINT_NO').AsString;
  //전송된건이면 삭제불가.
  if MessageDlg('정말 삭제하시겠습니까?',mtWarning,mbOKCancel,0) = mrOk then
  begin
      with SQLCreate do
      begin
        DMLType := dmlDelete;
        SQLCreate.SQLHeader('APPSPC_H');
        ADDWhere('MAINT_NO',MAINT_NO);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      END;

      ShowMessage('삭제되었습니다!');
      qryList.Close;
      qryList.Open;

  end;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPSPC_NEW_frm.CODEDblClick(Sender: TObject);
var
  CodeDialog:TCodeDialogParent_frm;

begin
  inherited;
  CodeDialog := nil;
  case TsEdit(Sender).Tag of
    // BGM2, BGM3
    101,102: CodeDialog := TCD_APPSPCBANK_frm.Create(Self);
    // 거래처코드
    103: CodeDialog := TCD_CUST_frm.Create(Self);
    // 은행코드
    104,109: CodeDialog := TCD_BANK_frm.Create(Self);
    // 통화단위
    105,106,107,108: CodeDialog := TCD_AMT_UNIT_frm.Create(Self);
  end;

  try
    CodeDialog.OpenDialog(0, TsEdit(Sender).Text);
    TsEdit(Sender).Text := CodeDialog.FieldValues('CODE');
    case TsEdit(Sender).Tag of
      103:
      begin
        msk_APP_SAUPNO.Text := CodeDialog.FieldValues('SAUP_NO');
        edt_APP_SNAME.Text := CodeDialog.FieldValues('ENAME');
        edt_APP_NAME.Text := CodeDialog.FieldValues('REP_NAME');
        edt_APP_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
        edt_APP_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
        edt_APP_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        edt_APP_ELEC.Text := CodeDialog.FieldValues('Jenja');

      end;
      104:
      begin
        edt_CP_BANKNAME.Text := CodeDialog.FieldValues('BankName1');
        edt_CP_BANKBU.Text := CodeDialog.FieldValues('BankBranch1');
      end;
      109:
      begin
        edt_LA_BANKNAMEP.Text := CodeDialog.FieldValues('BankName1');
        edt_LA_BANKBUP.Text := CodeDialog.FieldValues('BankBranch1');
      end;

    end;
  finally
    FreeAndNil(CodeDialog);
  end;


end;

procedure TUI_APPSPC_NEW_frm.DOCDEL(Sender: TObject);
var
  SQLCreate_D1:TSQLCreate;
  SQL_D1:String;
  SEQ :Integer;
begin
  inherited;
  SQLCreate_D1 := TSQLCreate.Create;
  case TsBitBtn(Sender).Tag of
    1:
    begin

      if qryListD1_2AH.RecordCount = 0 then
      begin
        ShowMessage('등록된 물품수령증명서가 없습니다.');
        mode_D1 := 1;
        exit;
      end;

      with SQLCreate_D1 do
      begin
        DMLType := dmlDelete;
        ADDWhere('KEYY',Maint_No);
        SQLHeader('APPSPC_D1');
        SEQ := qryListD1_2AH.FieldByName('SEQ').AsInteger;
        ADDWhere('SEQ', SEQ);
        ADDWhere('DOC_GUBUN','2AH');
      end;

      SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('2AH');

      with TADOQuery.Create(nil) do
      begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D1.CreateSQL+#13#10+SQL_D1;
        ExecSQL;
        ShowMessage('삭제되었습니다.');
        qryListD1_2AH.Close;
        qryListD1_2AH.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
        qryListD1_2AH.Open;
        sPanel60.Caption := '물품수령증명서 : '+IntToStr(qryListD1_2AH.RecordCount)+'건';
      finally
        Close;
        Free;
      end;
      SEQ := SEQ-1;
      end;

    end;
    2://세금계산서 삭제
    begin

      if qryListD1_2AJ.RecordCount = 0 then
      begin
        ShowMessage('등록된 세금계산서가 없습니다.');
        mode_D1 := 1;
        exit;
      end;

      with SQLCreate_D1 do
      begin
        DMLType := dmlDelete;
        ADDWhere('KEYY',Maint_No);
        SQLHeader('APPSPC_D1');
        SEQ := qryListD1_2AJ.FieldByName('SEQ').AsInteger;
        ADDWhere('SEQ', SEQ);
        ADDWhere('DOC_GUBUN','2AJ');
      end;

      SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('2AJ');

      with TADOQuery.Create(nil) do
      begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D1.CreateSQL+#13#10+SQL_D1;
        ExecSQL;
        ShowMessage('삭제되었습니다.');
        qryListD1_2AJ.Close;
        qryListD1_2AJ.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
        qryListD1_2AJ.Open;
        sPanel58.Caption := '세금계산서 : '+IntToStr(qryListD1_2AJ.RecordCount)+'건';
      finally
        Close;
        Free;
      end;   
      SEQ := SEQ-1;
      end;
    end;

    3://상업송장 삭제
    begin

      if qryListD1_1BW.RecordCount = 0 then
      begin
        ShowMessage('등록된 상업송장이 없습니다.');
        mode_D1 := 1;
        exit;
      end;

      with SQLCreate_D1 do
      begin
        DMLType := dmlDelete;
        ADDWhere('KEYY',Maint_No);
        SQLHeader('APPSPC_D1');
        SEQ := qryListD1_1BW.FieldByName('SEQ').AsInteger;
        ADDWhere('SEQ', SEQ);
        ADDWhere('DOC_GUBUN','1BW');
      end;

      SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('1BW');

      with TADOQuery.Create(nil) do
      begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D1.CreateSQL+#13#10+SQL_D1;
        ExecSQL;
        ShowMessage('삭제되었습니다.');
        qryListD1_1BW.Close;
        qryListD1_1BW.Parameters[0].Value := qryList.FieldByName('MAINT_NO').AsString;
        qryListD1_1BW.Open;
        sPanel56.Caption := '상업송장 : '+IntToStr(qryListD1_1BW.RecordCount)+'건';
      finally        
        Close;
        Free;
      end;
      end;
    end;
  end;
end;

procedure TUI_APPSPC_NEW_frm.qryListD1_2AHAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SEQ := qryListD1_2AH.FieldByName('SEQ').AsInteger;
end;

procedure TUI_APPSPC_NEW_frm.qryListD1_2AJAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SEQ := qryListD1_2AJ.FieldByName('SEQ').AsInteger;
end;

procedure TUI_APPSPC_NEW_frm.qryListD1_1BWAfterScroll(DataSet: TDataSet);
begin
  inherited;      
  SEQ := qryListD1_1BW.FieldByName('SEQ').AsInteger;

end;

procedure TUI_APPSPC_NEW_frm.edt_DOC_NODblClick(Sender: TObject);
var
  isCopy:Boolean;
begin
  inherited;
  dlg_CopyAPPSPCfromLOCAD1_frm := Tdlg_CopyAPPSPCfromLOCAD1_frm.Create(Self);
  try
          isCopy := dlg_CopyAPPSPCfromLOCAD1_frm.OpenDialog(0);
          IF not isCopy Then
            Abort;

          //내국신용장 관리번호
          edt_DOC_NO.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('MAINT_NO');
          //내국신용장 타입
          if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AA' then
            com_LA_TYPE.ItemIndex := 0
          else if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AB' then
            com_LA_TYPE.ItemIndex := 1
          else if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AC' then
            com_LA_TYPE.ItemIndex := 2;
          //개설일자
          msk_ISS_DATE.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('ISS_DATE');
          //신용장번호
          edt_LR_NO.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LC_NO');
          //은행코드
          edt_LA_BANKBUCODE.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK');
          //은행명
          edt_LA_BANKNAMEP.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK1');
          //은행지점
          edt_LA_BANKBUP.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK2');

  finally
    FreeAndNil(dlg_CopyAPPSPCfromLOCAD1_frm);

  end;


end;

procedure TUI_APPSPC_NEW_frm.sButton1Click(Sender: TObject);
begin
  inherited;
//  ADOStoredProc1.Parameters[1].Value := 'T'
//    with spCopyAPPSPC do
//    begin
//      Close;
//     Parameters.ParamByName('@KEYY').Value := 'TESTZZZ ';
//      Parameters[1].Value := ;
//      Parameters[2].Value := ;
//
//      if not DMMssql.inTrans then
//        DMMssql.BeginTrans;
//
//      try
//        ExecProc;
//      except
//        on e:Exception do
//        begin
//          MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
//        end;
//      end;
//    end;

end;

procedure TUI_APPSPC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  btnSearchClick(Self);

end;

procedure TUI_APPSPC_NEW_frm.btnSearchClick(Sender: TObject);
begin
  inherited;
  qryList.Close;
  qryList.Parameters[0].Value := sMaskEdit1.Text;
  qryList.Parameters[1].Value := sMaskEdit2.Text;
  qryList.Parameters[2].Value := '%'+edt_SearchNo.Text+'%';
  qryList.Open;
end;

procedure TUI_APPSPC_NEW_frm.sBitBtn5Click(Sender: TObject);
begin
  inherited;
  if not ReadWrite then
    btnSearchClick(Self);

end;

procedure TUI_APPSPC_NEW_frm.sMaskEdit3Change(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := sMaskEdit3.Text;
end;

procedure TUI_APPSPC_NEW_frm.sMaskEdit4Change(Sender: TObject);
begin
  inherited;
  sMaskEdit2.Text := sMaskEdit4.Text;
end;

procedure TUI_APPSPC_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  sMaskEdit3.Text := sMaskEdit1.Text;
end;

procedure TUI_APPSPC_NEW_frm.sMaskEdit2Change(Sender: TObject);
begin
  inherited;
  sMaskEdit4.Text := sMaskEdit2.Text;
end;

procedure TUI_APPSPC_NEW_frm.edt_SearchNoChange(Sender: TObject);
begin
  inherited;
  sEdit1.Text := edt_SearchNo.Text;
end;

procedure TUI_APPSPC_NEW_frm.sEdit1Change(Sender: TObject);
begin
  inherited;
  edt_SearchNo.Text := sEdit1.Text;
end;

procedure TUI_APPSPC_NEW_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex: integer;
begin
  inherited;
  nIndex := StrToIntDef(qryListCHK2.AsString, -1);
  case nIndex of
    -1:
      Text := '';
    0:
      Text := '임시';
    1:
      Text := '저장';
    2:
      Text := '결재';
    3:
      Text := '반려';
    4:
      Text := '접수';
    5:
      Text := '준비';
    6:
      Text := '취소';
    7:
      Text := '전송';
    8:
      Text := '오류';
    9:
      Text := '승인';
  end;
end;

procedure TUI_APPSPC_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  //임시로 해놓은것.
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    begin
      with TADOQuery.Create(Nil) do
      begin
        try
          Connection := DMMssql.KISConnect;                                                                                                 
//          SQL.Text := 'UPDATE APPSPC_H SET CHK3 = '+FormatDateTime('YYYYMMDD', Now)+'1 WHERE MAINT_NO = '+Maint_No;
          SQL.Text := 'UPDATE APPSPC_H SET CHK3 = '+QuotedStr(FormatDateTime('YYYYMMDD', Now)+'1')+' WHERE MAINT_NO = '+QuotedStr(Maint_No);
//          ShowMessage(SQL.Text);
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
      ReadyDocument(qryListMAINT_NO.AsString);
    end
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'전송준비실패',MB_OK+MB_ICONINFORMATION);
  end
  else
    ShowMessage('전송준비할 문서가 없습니다.');
end;

function TUI_APPSPC_NEW_frm.ErrorCheck:String;
var
  ErrMsg:TStringList;

begin
  ErrMsg := TStringList.Create;
  Try
    //공통사항
    if Trim(edt_BGM1.Text) = '' then
      ErrMsg.Add('[공통사항]문서번호-수발신식별자를 입력하세요');
    if Trim(edt_BGM2.Text) = '' then
      ErrMsg.Add('[공통사항]문서번호-추심은행 코드를 입력하세요');
    if Trim(edt_BGM3.Text) = '' then
      ErrMsg.Add('[공통사항]문서번호-개설은행 코드를 입력하세요');
    if Trim(edt_CP_NO.Text) = '' then
      ErrMsg.Add('[공통사항]추심(매입) 신청번호를 입력하세요');


    //신청인
    if Trim(edt_APP_SNAME.Text) = '' then
      ErrMsg.Add('[신청인]상호를 입력하세요');
    if Trim(msk_APP_SAUPNO.Text) = '' then
      ErrMsg.Add('[신청인]사업자등록번호를 입력하세요');
    if Trim(edt_APP_NAME.Text) = '' then
      ErrMsg.Add('[신청인]대표자를 입력하세요');
    if Trim(edt_APP_ADDR1.Text) = '' then
      ErrMsg.Add('[신청인]주소를 입력하세요');

    //구매자
    if Trim(msk_DF_SAUPNO.Text) = '' then
      ErrMsg.Add('[구매자]사업자등록번호를 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[구매자]상호를 입력하세요');
//    if (Trim(edt_DF_EMAIL1.Text) = '') or (Trim(edt_DF_EMAIL2.Text) = '') then
//      ErrMsg.Add('[구매자]이메일을 입력하세요');

//추심은행
    if Trim(edt_CP_BANK.Text) = '' then
      ErrMsg.Add('[추심은행]은행코드를 입력하세요');
    if Trim(edt_CP_BANKNAME.Text) = '' then
      ErrMsg.Add('[추심은행]은행명을 입력하세요');
    if Trim(edt_CP_BANKBU.Text) = '' then
      ErrMsg.Add('[추심은행]지점명을 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[추심은행]계좌번호를 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[추심은행]계좌통화를 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[추심은행]계좌주를 입력하세요');

    case com_LA_TYPE.ItemIndex of
      0: //2AA : 원화표시 외화부기 내국신용장
      begin
        if edt_CP_AMT.Value = 0 then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 원화표시 외화부기 내국신용장일때는 추심금액(원화)를 입력하셔야합니다.');
        if edt_CP_AMTU.Value = 0 then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 원화표시 외화부기 내국신용장일때는 추심금액(외화)를 입력하셔야합니다.');
        if Trim(edt_CP_AMTC.Text) = '' then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 원화표시 외화부기 내국신용장일때는 추심금액(외화) 통화단위를 입력하셔야합니다.');
      end;
      1: //2AB : 외화표시 내국신용장
      begin
        if edt_CP_AMTU.Value = 0 then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 외화표시 내국신용장일때는 추심금액(외화)를 입력하셔야합니다.');
        if Trim(edt_CP_AMTC.Text) = '' then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 외화표시 내국신용장일때는 추심금액(외화) 통화단위를 입력하셔야합니다.');
      end;
      2: //2AC : 원화표시 내국신용장
      begin
        if edt_CP_AMT.Value = 0 then
          ErrMsg.Add('[추심금액] 내국신용장 종류가 원화표시 내국신용장일때는 추심금액(원화)를 입력하셔야합니다.');
      end;
    end;

    //내국신용장
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[내국신용장]문서번호를 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[내국신용장]내국신용장번호를 입력하세요');
    if Trim(edt_DF_NAME1.Text) = '' then
      ErrMsg.Add('[내국신용장]개설일자를 입력하세요');
    if Trim(edt_LA_BANKBUCODE.Text) = '' then
      ErrMsg.Add('[내국신용장]개설은행 코드를 입력하세요');
    if Trim(edt_LA_BANKNAMEP.Text) = '' then
      ErrMsg.Add('[내국신용장]개설은행명을 입력하세요');
    if Trim(edt_LA_BANKBUP.Text) = '' then
      ErrMsg.Add('[내국신용장]개설은행 지점명을 입력하세요');


    //물품수령증명서는 1부이상 필수입력
    if qryListD1_2AH.RecordCount < 1 then
      ErrMsg.Add('물품수령증명서는 최소 1부이상 입력해야합니다.');
    //세금계산서,상업송장 둘중 하나는 1부이상 필수 입력, 둘다입력은 불가.
    if (qryListD1_2AJ.RecordCount < 1) and (qryListD1_1BW.RecordCount < 1) then
      ErrMsg.Add('세금계산서 또는 상업송장은 최소 1부이상 입력해야합니다.');

//    유효성검사 마저 하기. D1쪽도. D2쪽도.

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;

END;

procedure TUI_APPSPC_NEW_frm.com_BGM_GUBUNChange(Sender: TObject);
begin
  inherited;
  if com_BGM_GUBUN.ItemIndex = 0 then
    com_CP_CODE.ItemIndex := 0
  else if com_BGM_GUBUN.ItemIndex = 1 then
    com_CP_CODE.ItemIndex := 1;


end;

procedure TUI_APPSPC_NEW_frm.ReadyDocument(DocNo:String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPSPC_NEW(RecvDoc, qryList.FieldByName('BGM1').AsString ,qryList.FieldByName('BGM2').AsString ,qryList.FieldByName('BGM3').AsString ,qryList.FieldByName('BGM4').AsString, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APPSPC';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPSPC_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;
//        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
      ShowMessage('전송준비완료');
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_APPSPC_NEW_frm.btnCpy2APClick(Sender: TObject);
var
  isCopy:Boolean;
begin
  inherited;
  dlg_CopyAPPSPCfromLOCAD1_frm := Tdlg_CopyAPPSPCfromLOCAD1_frm.Create(Self);
  try
          isCopy := dlg_CopyAPPSPCfromLOCAD1_frm.OpenDialog(0);
          IF not isCopy Then
            Abort;

          //내국신용장 관리번호
          edt_DOC_NO.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('MAINT_NO');
          //내국신용장 타입
          if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AA' then
            com_LA_TYPE.ItemIndex := 0
          else if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AB' then
            com_LA_TYPE.ItemIndex := 1
          else if dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LOC_TYPE') = '2AC' then
            com_LA_TYPE.ItemIndex := 2;
          //개설일자
          msk_ISS_DATE.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('ISS_DATE');
          //신용장번호
          edt_LR_NO.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('LC_NO');
          //은행코드
          edt_LA_BANKBUCODE.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK');
          //은행명
          edt_LA_BANKNAMEP.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK1');
          //은행지점
          edt_LA_BANKBUP.Text := dlg_CopyAPPSPCfromLOCAD1_frm.FieldValues('AP_BANK2');

  finally
    FreeAndNil(dlg_CopyAPPSPCfromLOCAD1_frm);

  end;


end;

procedure TUI_APPSPC_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then
  begin
    ShowMessage('출력할 문서가 없습니다.');
    exit;
  end;
  APPSPC_PRINT_NEW_frm := TAPPSPC_PRINT_NEW_frm.Create(Self);
//  try
//    APPSPC_PRINT_NEW_frm.MaintNo := edt_MAINT_NO.Text;
//    QRCompositeReport1.Preview;
//  finally
//    FreeAndNil(APPSPC_PRINT_NEW_frm);
//  end;

  Preview_frm := TPreview_frm.Create(Self);
  try
    Preview_frm.Report := APPSPC_PRINT_NEW_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(APPSPC_PRINT_NEW_frm);
  end;

end;

procedure TUI_APPSPC_NEW_frm.edt_CP_BANKExit(Sender: TObject);
begin
  inherited;
  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    case TsEdit(Sender).Tag of
      104:
      begin
        edt_CP_BANKNAME.Text := CD_BANK_frm.GetCodeText(edt_CP_BANK.Text).sName;
        edt_CP_BANKBU.Text :=  CD_BANK_frm.GetCodeText(edt_CP_BANK.Text).sBrunch;
      end;
      109:
      begin
        edt_LA_BANKNAMEP.Text := CD_BANK_frm.GetCodeText(edt_LA_BANKBUCODE.Text).sName;
        edt_LA_BANKBUP.Text :=  CD_BANK_frm.GetCodeText(edt_LA_BANKBUCODE.Text).sBrunch;
      end;
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;
end;

procedure TUI_APPSPC_NEW_frm.CODEExit(Sender: TObject);
var
  CodeDialog:TCodeDialogParent_frm;
begin
  inherited;
//  CodeDialog := nil;
//  case TsEdit(Sender).Tag of
//    // BGM2, BGM3
//    101,102: CodeDialog := TCD_APPSPCBANK_frm.Create(Self);
//    // 거래처코드
//    103: CodeDialog := TCD_CUST_frm.Create(Self);
//    // 은행코드
//    104,109: CodeDialog := TCD_BANK_frm.Create(Self);
//    // 통화단위
//    105,106,107,108: CodeDialog := TCD_AMT_UNIT_frm.Create(Self);
//  end;
//
//  try
//
//    case TsEdit(Sender).Tag of
//      // 거래처코드
////      103: CodeDialog := TCD_CUST_frm.Create(Self);
//      // 추심(매입)은행 코드
//      104:
//      begin
//        edt_CP_BANKNAME.Text := CodeDialog.GetCodeText(edt_CP_BANK.Text).S;
//        edt_CP_BANKBU.Text :=  CodeDialog.GetCodeText(edt_CP_BANK.Text).sBrunch;
//      end;
//
//      // 내국신용장 개설은행 코드
//      109: CodeDialog := TCD_BANK_frm.Create(Self);
//      begin
//        edt_LA_BANKNAMEP.Text := CodeDialog.GetCodeText(edt_LA_BANKBUCODE.Text).sName;
//        edt_LA_BANKBUP.Text :=  CodeDialog.GetCodeText(edt_LA_BANKBUCODE.Text).sBrunch;
//      end;
//    end;
//
//  finally
//    FreeAndNil(CodeDialog);
//
//  end;
end;

procedure TUI_APPSPC_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;        
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;

end;

end.
