unit UI_DEBADV_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ComCtrls, sPageControl, Buttons, sBitBtn, Grids,
  DBGrids, acDBGrid, QuickRpt, QRCtrls, StdCtrls, sComboBox, Mask,
  sMaskEdit, sEdit, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel,
  sSkinProvider, sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, DB,
  ADODB, DateUtils;

type
  TUI_DEBADV_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sLabel5: TsLabel;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel6: TsPanel;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    comBank: TsComboBox;
    QRShape1: TQRShape;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    Shape1: TShape;
    msk_ADV_DATE: TsMaskEdit;
    msk_WIT_DT: TsMaskEdit;
    msk_DEP_DT: TsMaskEdit;
    msk_EXT_DT: TsMaskEdit;
    sPanel2: TsPanel;
    edt_ETC_REFNO: TsEdit;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    memo_PAY_DTL: TsMemo;
    ETC_INFO1: TsMemo;
    ETC_INFO2: TsMemo;
    sPanel1: TsPanel;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sPanel11: TsPanel;
    sPanel3: TsPanel;
    edt60_AMT_UNIT: TsEdit;
    cur60_AMT: TsCurrencyEdit;
    edt60_BASE_UNIT: TsEdit;
    edt60_DEST_UNIT: TsEdit;
    cur60_RATE: TsCurrencyEdit;
    msk60_RATE_STDT: TsMaskEdit;
    msk60_RATE_EDDT: TsMaskEdit;
    edt60_D1_RFF_CD: TsEdit;
    edt60_WIT_ACNT: TsEdit;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    edt_CLT_BKCD: TsEdit;
    sTabSheet5: TsTabSheet;
    sPanel34: TsPanel;
    sDBGrid2: TsDBGrid;
    sTabSheet6: TsTabSheet;
    sPanel30: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    sDBGrid5: TsDBGrid;
    sEdit6: TsEdit;
    edt60_D1_RFF_NO: TsEdit;
    sPanel10: TsPanel;
    edt9_AMT_UNIT: TsEdit;
    cur9_AMT: TsCurrencyEdit;
    edt9_BASE_UNIT: TsEdit;
    edt9_DEST_UNIT: TsEdit;
    cur9_RATE: TsCurrencyEdit;
    msk9_RATE_STDT: TsMaskEdit;
    msk9_RATE_EDDT: TsMaskEdit;
    edt9_D1_RFF_CD: TsEdit;
    edt9_WIT_ACNT: TsEdit;
    sEdit13: TsEdit;
    edt9_D1_RFF_NO: TsEdit;
    sPanel12: TsPanel;
    edt98_AMT_UNIT: TsEdit;
    cur98_AMT: TsCurrencyEdit;
    sPanel13: TsPanel;
    edt36_AMT_UNIT: TsEdit;
    cur36_AMT: TsCurrencyEdit;
    edt36_BASE_UNIT: TsEdit;
    edt36_DEST_UNIT: TsEdit;
    cur36_RATE: TsCurrencyEdit;
    msk36_RATE_STDT: TsMaskEdit;
    msk36_RATE_EDDT: TsMaskEdit;
    edt_CLT_MAN: TsEdit;
    edt_CLT_BKNM: TsEdit;
    edt_CLT_BRNM: TsEdit;
    sPanel16: TsPanel;
    edt_BEN_ACNTNM: TsEdit;
    edt_BEN_ACNT: TsEdit;
    edt_BEN_UNIT: TsEdit;
    edt_BEN_BKCD: TsEdit;
    edt_BEN_MAN: TsEdit;
    edt_BEN_BKNM: TsEdit;
    edt_BEN_BRNM: TsEdit;
    sPanel17: TsPanel;
    edt_BEN_NM1: TsEdit;
    edt_BEN_NM2: TsEdit;
    edt_BEN_NM3: TsEdit;
    edt_BEN_NM4: TsEdit;
    edt_BEN_NM5: TsEdit;
    sPanel18: TsPanel;
    edt_CLT_NM1: TsEdit;
    edt_CLT_NM2: TsEdit;
    edt_CLT_NM3: TsEdit;
    edt_CLT_NM4: TsEdit;
    edt_CLT_NM5: TsEdit;
    edt_BEN_NM6: TsEdit;
    edt_CLT_NM6: TsEdit;
    sPanel23: TsPanel;
    edt_SIGN1: TsEdit;
    edt_SIGN2: TsEdit;
    edt_SIGN3: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListADV_DT: TStringField;
    qryListWIT_DT: TStringField;
    qryListDEP_DT: TStringField;
    qryListEXT_DT: TStringField;
    qryListETC_REFNO: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPAY_DTL: TStringField;
    qryListETC_INFO1: TStringField;
    qryListETC_INFO2: TStringField;
    qryListCLT_BKCD: TStringField;
    qryListCLT_MAN: TStringField;
    qryListCLT_BKNM: TStringField;
    qryListCLT_BRNM: TStringField;
    qryListBEN_BKCD: TStringField;
    qryListBEN_BKNM: TStringField;
    qryListBEN_BRNM: TStringField;
    qryListBEN_ACNT: TStringField;
    qryListBEN_ACNTNM: TStringField;
    qryListBEN_UNIT: TStringField;
    qryListBEN_MAN: TStringField;
    qryListBEN_NM1: TStringField;
    qryListBEN_NM2: TStringField;
    qryListBEN_NM3: TStringField;
    qryListBEN_NM4: TStringField;
    qryListBEN_NM5: TStringField;
    qryListBEN_NM6: TStringField;
    qryListCLT_NM1: TStringField;
    qryListCLT_NM2: TStringField;
    qryListCLT_NM3: TStringField;
    qryListCLT_NM4: TStringField;
    qryListCLT_NM5: TStringField;
    qryListCLT_NM6: TStringField;
    qryListSIGN1: TStringField;
    qryListSIGN2: TStringField;
    qryListSIGN3: TStringField;
    qryDTL: TADOQuery;
    dsDTL: TDataSource;
    qryDTLCD5025: TStringField;
    qryDTLAMT_UNIT: TStringField;
    qryDTLAMT: TBCDField;
    qryDTLBASE_UNIT: TStringField;
    qryDTLDEST_UNIT: TStringField;
    qryDTLRATE: TBCDField;
    qryDTLRATE_STDT: TDateField;
    qryDTLRATE_EDDT: TDateField;
    qryDTLD1_RFF_CD: TStringField;
    qryDTLD1_RFF_NO: TStringField;
    qryDTLWIT_ACNT: TStringField;
    qryListETC_REFCD: TStringField;
    edt_ETC_REFCD: TsEdit;
    sEdit45: TsEdit;
    qryFC1: TADOQuery;
    dsFC1: TDataSource;
    qryFC2: TADOQuery;
    dsFC2: TDataSource;
    qryFC1FC1CD: TStringField;
    qryFC1FC1NM: TStringField;
    qryFC1FC1AMT1: TBCDField;
    qryFC1FC1AMT1C: TStringField;
    qryFC1FC1AMT2: TBCDField;
    qryFC1FC1AMT2C: TStringField;
    qryFC2FC1SEQ: TIntegerField;
    qryFC2FC1CH: TStringField;
    qryFC2FC1CHNM: TStringField;
    qryFC2FC1SA1: TBCDField;
    qryFC2FC1SA1C: TStringField;
    qryFC2FC1SA2: TBCDField;
    qryFC2FC1SA2C: TStringField;
    qryFC2FC1RATE1: TStringField;
    qryFC2FC1RATE2: TStringField;
    qryFC2FC1RATE: TBCDField;
    qryFC2FC1RD1: TStringField;
    qryFC2FC1RD2: TStringField;
    qryDOCLIST: TADOQuery;
    dsDOCLIST: TDataSource;
    qryDOCLISTMAINT_NO: TStringField;
    qryDOCLISTSEQ: TIntegerField;
    qryDOCLISTRELIST_TYPE: TStringField;
    qryDOCLISTRELIST_NO: TStringField;
    qryDOCLISTRELIST_ADDNO: TStringField;
    qryDOCLISTRELIST_APPDT: TStringField;
    qryDOCLISTViewType: TStringField;
    qryDTLD1_RFF_CDNM: TStringField;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryFC1AfterScroll(DataSet: TDataSet);
    procedure qryFC1AfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
    procedure RunSQL(sSQL : TStream);
  protected
    procedure ReadData; override;
  public
    { Public declarations }
  end;

var
  UI_DEBADV_NEW_frm: TUI_DEBADV_NEW_frm;

implementation

uses
  MSSQL, VarDefine;

{$R *.dfm}

{ TUI_DEBADV_NEW_frm }

procedure TUI_DEBADV_NEW_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;
    Open;
  end;
end;

procedure TUI_DEBADV_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6,False);
  ReadOnlyControl(sPanel7,False);
  ReadOnlyControl(sPanel11,False);
  ReadOnlyControl(sPanel14,False);
  ReadOnlyControl(sPanel30,False);
  ReadOnlyControl(sPanel34,False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_DEBADV_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_DEBADV_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DEBADV_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_DEBADV_NEW_frm := nil;
end;

procedure TUI_DEBADV_NEW_frm.ReadData;
begin
  inherited;
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

  //문서공통
  msk_ADV_DATE.Text := qryListADV_DT.AsString;
  msk_WIT_DT.Text := qryListWIT_DT.AsString;
  msk_DEP_DT.Text := qryListDEP_DT.AsString;
  msk_EXT_DT.Text := qryListEXT_DT.AsString;

  edt_ETC_REFNO.Text := qryListETC_REFNO.AsString;
  edt_ETC_REFCD.Text := qryListETC_REFCD.AsString;

  memo_PAY_DTL.Lines.Text := qryListPAY_DTL.AsString;
  ETC_INFO1.Lines.Text := qryListETC_INFO1.AsString;
  ETC_INFO2.Lines.Text := qryListETC_INFO2.AsString;

  //PAGE2
  ClearControl(sPanel11);
  while not qryDTL.Eof do
  begin
    IF qryDTLCD5025.AsString = '60' Then
    begin
      edt60_AMT_UNIT.Text := qryDTLAMT_UNIT.AsString;
      cur60_AMT.Value := qryDTLAMT.AsCurrency;
      edt60_BASE_UNIT.Text := qryDTLBASE_UNIT.AsString;
      edt60_DEST_UNIT.Text := qryDTLDEST_UNIT.AsString;
      cur60_RATE.Value := qryDTLRATE.AsFloat;
      msk60_RATE_STDT.Text := qryDTLRATE_STDT.AsString;
      msk60_RATE_EDDT.Text := qryDTLRATE_EDDT.AsString;

      edt60_D1_RFF_CD.Text := qryDTLD1_RFF_CD.AsString;
      sEdit6.Text := qryDTLD1_RFF_CDNM.AsString;
      edt60_D1_RFF_NO.Text := qryDTLD1_RFF_NO.AsString;
      edt60_WIT_ACNT.Text := qryDTLWIT_ACNT.AsString;
    end
    else
    IF qryDTLCD5025.AsString = '9'  Then
    begin
      edt9_AMT_UNIT.Text := qryDTLAMT_UNIT.AsString;
      cur9_AMT.Value := qryDTLAMT.AsCurrency;
      edt9_BASE_UNIT.Text := qryDTLBASE_UNIT.AsString;
      edt9_DEST_UNIT.Text := qryDTLDEST_UNIT.AsString;
      cur9_RATE.Value := qryDTLRATE.AsFloat;
      msk9_RATE_STDT.Text := qryDTLRATE_STDT.AsString;
      msk9_RATE_EDDT.Text := qryDTLRATE_EDDT.AsString;

      edt9_D1_RFF_CD.Text := qryDTLD1_RFF_CD.AsString;
      sEdit13.Text := qryDTLD1_RFF_CDNM.AsString;      
      edt9_D1_RFF_NO.Text := qryDTLD1_RFF_NO.AsString;
      edt9_WIT_ACNT.Text := qryDTLWIT_ACNT.AsString;
    end
    else
    IF qryDTLCD5025.AsString = '98' Then
    begin
      edt98_AMT_UNIT.Text := qryDTLAMT_UNIT.AsString;
      cur98_AMT.Value := qryDTLAMT.AsCurrency;
    end
    else
    IF qryDTLCD5025.AsString = '36' Then
    begin
      edt36_AMT_UNIT.Text := qryDTLAMT_UNIT.AsString;
      cur36_AMT.Value := qryDTLAMT.AsCurrency;
      edt36_BASE_UNIT.Text := qryDTLBASE_UNIT.AsString;
      edt36_DEST_UNIT.Text := qryDTLDEST_UNIT.AsString;
      cur36_RATE.Value := qryDTLRATE.AsFloat;
      msk36_RATE_STDT.Text := qryDTLRATE_STDT.AsString;
      msk36_RATE_EDDT.Text := qryDTLRATE_EDDT.AsString;
    end;
    
    qryDTL.Next;
  end;

  //PAGE3
  edt_CLT_BKCD.Text := qryListCLT_BKCD.AsString;
  edt_CLT_MAN.Text  := qryListCLT_MAN.AsString;
  edt_CLT_BKNM.Text := qryListBEN_BKNM.AsString;
  edt_CLT_BRNM.Text := qryListBEN_BRNM.AsString;

  edt_BEN_BKCD.Text := qryListBEN_BKCD.AsString;
  edt_BEN_BKNM.Text := qryListBEN_BKNM.AsString;
  edt_BEN_BRNM.Text := qryListBEN_BRNM.AsString;
  edt_BEN_ACNT.Text := qryListBEN_ACNT.AsString;
  edt_BEN_ACNTNM.Text := qryListBEN_ACNTNM.AsString;
  edt_BEN_UNIT.Text := qryListBEN_UNIT.AsString;
  edt_BEN_MAN.Text := qryListBEN_MAN.AsString;

  edt_BEN_NM1.Text := qryListBEN_NM1.AsString;
  edt_BEN_NM2.Text := qryListBEN_NM2.AsString;
  edt_BEN_NM3.Text := qryListBEN_NM3.AsString;
  edt_BEN_NM4.Text := qryListBEN_NM4.AsString;
  edt_BEN_NM5.Text := qryListBEN_NM5.AsString;
  edt_BEN_NM6.Text := qryListBEN_NM6.AsString;

  edt_CLT_NM1.Text := qryListCLT_NM1.AsString;
  edt_CLT_NM2.Text := qryListCLT_NM2.AsString;
  edt_CLT_NM3.Text := qryListCLT_NM3.AsString;
  edt_CLT_NM4.Text := qryListCLT_NM4.AsString;
  edt_CLT_NM5.Text := qryListCLT_NM5.AsString;
  edt_CLT_NM6.Text := qryListCLT_NM6.AsString;

  edt_SIGN1.Text := qryListSIGN1.AsString;
  edt_SIGN2.Text := qryListSIGN2.AsString;
  edt_SIGN3.Text := qryListSIGN3.AsString;
end;

procedure TUI_DEBADV_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  with qryDTL do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;

  with qryFC1 do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;

  with qryDOCLIST do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;

  ReadData;  
end;

procedure TUI_DEBADV_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_DEBADV_NEW_frm.qryFC1AfterScroll(DataSet: TDataSet);
begin
  inherited;
  with qryFC2 do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Parameters[1].Value := qryFC1FC1CD.AsString;
    Open;
  end;
end;

procedure TUI_DEBADV_NEW_frm.qryFC1AfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryFC1AfterScroll(DataSet);
end;

procedure TUI_DEBADV_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  //테이블생성 FROM DLL
  DllPatchExecute('CTABLE_DEBADV');
end;

procedure TUI_DEBADV_NEW_frm.RunSQL(sSQL: TStream);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.LoadFromStream(sSQL);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_DEBADV_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 5;
end;

end.
