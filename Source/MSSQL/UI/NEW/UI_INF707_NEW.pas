unit UI_INF707_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sMemo, sComboBox, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton, sLabel,
  sSpeedButton, ExtCtrls, sPanel, sSkinProvider, DB, ADODB, StrUtils, DateUtils, Clipbrd,
  acImage;

type
  TUI_INF707_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPage1: TsPanel;
    sPanel2: TsPanel;
    sPanel10: TsPanel;
    msk_IssDate: TsMaskEdit;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    edt_APPDATE: TsMaskEdit;
    edt_In_Mathod1: TsEdit;
    edt_In_Mathod: TsEdit;
    sPanel8: TsPanel;
    edt_ApBank: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank2: TsEdit;
    edt_ApBank3: TsEdit;
    edt_ApBank4: TsEdit;
    edt_ApBank5: TsEdit;
    sPanel9: TsPanel;
    sPanel59: TsPanel;
    sPanel89: TsPanel;
    sPanel90: TsPanel;
    curr_AMD_NO: TsCurrencyEdit;
    sPanel91: TsPanel;
    sPanel92: TsPanel;
    edt_AdBank1: TsEdit;
    edt_AdBank2: TsEdit;
    edt_AdBank3: TsEdit;
    edt_AdBank4: TsEdit;
    sPanel95: TsPanel;
    edt_AdBank: TsEdit;
    sPanel94: TsPanel;
    sPanel96: TsPanel;
    com_isCancel: TsComboBox;
    sPanel97: TsPanel;
    sPanel98: TsPanel;
    edt_ImpCd1: TsEdit;
    edt_ImpCd1_1: TsEdit;
    sPanel99: TsPanel;
    edt_ILno1: TsEdit;
    sPanel100: TsPanel;
    edt_ILCur1: TsEdit;
    sPanel101: TsPanel;
    curr_ILAMT1: TsCurrencyEdit;
    edt_ImpCd2: TsEdit;
    edt_ImpCd1_2: TsEdit;
    edt_ILno2: TsEdit;
    edt_ILCur2: TsEdit;
    curr_ILAMT2: TsCurrencyEdit;
    edt_ImpCd3: TsEdit;
    edt_ImpCd1_3: TsEdit;
    edt_ILno3: TsEdit;
    edt_ILCur3: TsEdit;
    curr_ILAMT3: TsCurrencyEdit;
    edt_ImpCd4: TsEdit;
    edt_ImpCd1_4: TsEdit;
    edt_ILno4: TsEdit;
    edt_ILCur4: TsEdit;
    curr_ILAMT4: TsCurrencyEdit;
    edt_ImpCd5: TsEdit;
    edt_ImpCd1_5: TsEdit;
    edt_ILno5: TsEdit;
    edt_ILCur5: TsEdit;
    curr_ILAMT5: TsCurrencyEdit;
    sPanel102: TsPanel;
    sPanel103: TsPanel;
    edt_AdInfo1: TsEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    edt_AdInfo5: TsEdit;
    sPanel1: TsPanel;
    sPanel13: TsPanel;
    edt_cdNo: TsEdit;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    edt_RecvRef: TsEdit;
    sPanel27: TsPanel;
    sPanel34: TsPanel;
    edt_IssRef: TsEdit;
    sTabSheet3: TsTabSheet;
    sPage2: TsPanel;
    sLabel3: TsLabel;
    sPanel14: TsPanel;
    edt_Applic1: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic4: TsEdit;
    sPanel16: TsPanel;
    sPanel15: TsPanel;
    edt_Benefc1: TsEdit;
    edt_Benefc2: TsEdit;
    edt_Benefc3: TsEdit;
    edt_Benefc4: TsEdit;
    sPanel17: TsPanel;
    edt_Benefc5: TsEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sPanel25: TsPanel;
    edt_CdPerp: TsCurrencyEdit;
    edt_CdPerm: TsCurrencyEdit;
    sPanel26: TsPanel;
    sPanel28: TsPanel;
    com_amd_charge: TsComboBox;
    sPanel33: TsPanel;
    sPanel30: TsPanel;
    sPanel31: TsPanel;
    com_charge: TsComboBox;
    sPanel32: TsPanel;
    edt_IncdCur: TsEdit;
    edt_IncdAmt: TsCurrencyEdit;
    edt_DecdCur: TsEdit;
    edt_DecdAmt: TsCurrencyEdit;
    sPanel77: TsPanel;
    sPanel78: TsPanel;
    msk_exDate: TsMaskEdit;
    edt_exPlace: TsEdit;
    sPanel105: TsPanel;
    sPanel106: TsPanel;
    edt_Doccd: TsEdit;
    edt_Doccd1: TsEdit;
    edt_ApplicOrg1: TsEdit;
    edt_ApplicOrg2: TsEdit;
    edt_ApplicOrg3: TsEdit;
    edt_ApplicOrg4: TsEdit;
    sTabSheet2: TsTabSheet;
    sPage3: TsPanel;
    com_Pship: TsComboBox;
    sPanel43: TsPanel;
    sPanel44: TsPanel;
    sPanel45: TsPanel;
    msk_lstDate: TsMaskEdit;
    sPanel48: TsPanel;
    sPanel49: TsPanel;
    sPanel50: TsPanel;
    sPanel51: TsPanel;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel54: TsPanel;
    sPanel55: TsPanel;
    edt_dochackPort: TsEdit;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sPanel58: TsPanel;
    edt_loadOn: TsEdit;
    edt_forTran: TsEdit;
    com_TShip: TsComboBox;
    sPanel46: TsPanel;
    sPanel47: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    memo_SPECIAL_PAY: TsMemo;
    sPanel64: TsPanel;
    sPanel65: TsPanel;
    sPanel66: TsPanel;
    sTabSheet5: TsTabSheet;
    sPage4: TsPanel;
    sPanel71: TsPanel;
    sPanel72: TsPanel;
    edt_DRAFT1: TsEdit;
    edt_DRAFT2: TsEdit;
    edt_DRAFT3: TsEdit;
    sPanel73: TsPanel;
    edt_MIX1: TsEdit;
    edt_MIX2: TsEdit;
    edt_MIX3: TsEdit;
    edt_MIX4: TsEdit;
    sPanel74: TsPanel;
    edt_DEFPAY1: TsEdit;
    edt_DEFPAY2: TsEdit;
    edt_DEFPAY3: TsEdit;
    edt_DEFPAY4: TsEdit;
    sPanel75: TsPanel;
    sPanel76: TsPanel;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    edt_aaCcv1: TsEdit;
    edt_aaCcv2: TsEdit;
    edt_aaCcv3: TsEdit;
    edt_aaCcv4: TsEdit;
    memo_GoodsDesc: TsMemo;
    sPanel38: TsPanel;
    sPanel39: TsPanel;
    sPanel41: TsPanel;
    sTabSheet6: TsTabSheet;
    sPage5: TsPanel;
    memo_DOCREQ: TsMemo;
    sPanel67: TsPanel;
    sPanel68: TsPanel;
    sPanel42: TsPanel;
    sTabSheet7: TsTabSheet;
    sPage6: TsPanel;
    sPanel82: TsPanel;
    edt_EXName1: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXAddr2: TsEdit;
    sPanel83: TsPanel;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    sPanel86: TsPanel;
    sPanel87: TsPanel;
    sPanel104: TsPanel;
    edt_period: TsEdit;
    com_PeriodIndex: TsComboBox;
    edt_PeriodDetail: TsEdit;
    sPanel80: TsPanel;
    sPanel81: TsPanel;
    com_Confirm: TsComboBox;
    sPanel69: TsPanel;
    sPanel70: TsPanel;
    edt_CONFIRM_BIC: TsEdit;
    edt_CONFIRM1: TsEdit;
    edt_CONFIRM2: TsEdit;
    edt_CONFIRM4: TsEdit;
    edt_CONFIRM3: TsEdit;
    memo_ADD_CONDITION: TsMemo;
    sPanel60: TsPanel;
    sPanel61: TsPanel;
    sPanel63: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    memo_amd_charge_1: TsMemo;
    memo_charge_1: TsMemo;
    sPanel79: TsPanel;
    sPanel93: TsPanel;
    memo_TXT78: TsMemo;
    sPanel107: TsPanel;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListIBANK_REF: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListPrno: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListMSEQ_1: TIntegerField;
    qryListAMD_NO_1: TIntegerField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListOP_BANK1: TStringField;
    qryListOP_BANK2: TStringField;
    qryListOP_BANK3: TStringField;
    qryListOP_ADDR1: TStringField;
    qryListOP_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListIS_CANCEL: TStringField;
    qryListDOC_CD: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_1: TMemoField;
    qryListSPECIAL_PAY: TMemoField;
    qryListGOODS_DESC: TMemoField;
    qryListDOC_REQ: TMemoField;
    qryListADD_CONDITION: TMemoField;
    qryListMIX1: TStringField;
    qryListMIX2: TStringField;
    qryListMIX3: TStringField;
    qryListMIX4: TStringField;
    qryListDEFPAY1: TStringField;
    qryListDEFPAY2: TStringField;
    qryListDEFPAY3: TStringField;
    qryListDEFPAY4: TStringField;
    qryListPERIOD_DAYS: TIntegerField;
    qryListPERIOD_IDX: TIntegerField;
    qryListPERIOD_DETAIL: TStringField;
    qryListCONFIRM: TStringField;
    qryListCONFIRM_BIC: TStringField;
    qryListCONFIRM1: TStringField;
    qryListCONFIRM2: TStringField;
    qryListCONFIRM3: TStringField;
    qryListCONFIRM4: TStringField;
    qryListTXT_78: TMemoField;
    qryListmathod_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListAPPLIC_CHG1: TStringField;
    qryListAPPLIC_CHG2: TStringField;
    qryListAPPLIC_CHG3: TStringField;
    qryListAPPLIC_CHG4: TStringField;
    sPanel108: TsPanel;
    edt_OP_BANK1: TsEdit;
    edt_OP_BANK2: TsEdit;
    edt_OP_BANK3: TsEdit;
    edt_OP_ADDR1: TsEdit;
    edt_OP_ADDR2: TsEdit;
    sPanel109: TsPanel;
    sPanel110: TsPanel;
    sPanel111: TsPanel;
    sPanel112: TsPanel;
    qryListDOC_CDNM: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    sButton13: TsButton;
    sPanel29: TsPanel;
    sPanel35: TsPanel;
    msk_AMDDATE: TsMaskEdit;
    sPanel40: TsPanel;
    sPanel62: TsPanel;
    qryListAPP_RULE1: TStringField;
    qryListAPP_RULE2: TStringField;
    edt_APP_RULE1: TsEdit;
    edt_APP_RULE2: TsEdit;
    sPanel88: TsPanel;
    sPanel113: TsPanel;
    edt_SR_INFO1: TsEdit;
    edt_SR_INFO2: TsEdit;
    edt_SR_INFO3: TsEdit;
    edt_SR_INFO4: TsEdit;
    edt_SR_INFO5: TsEdit;
    edt_SR_INFO6: TsEdit;
    qryListREIM_BANK_BIC: TStringField;
    qryListREIM_BANK1: TStringField;
    qryListREIM_BANK2: TStringField;
    qryListREIM_BANK3: TStringField;
    qryListREIM_BANK4: TStringField;
    sPanel114: TsPanel;
    sPanel115: TsPanel;
    sPanel116: TsPanel;
    sPanel117: TsPanel;
    edt_REIBANK: TsEdit;
    sPanel118: TsPanel;
    edt_REIBANK1: TsEdit;
    edt_REIBANK2: TsEdit;
    edt_REIBANK3: TsEdit;
    edt_REIBANK4: TsEdit;
    sPanel125: TsPanel;
    sPanel134: TsPanel;
    sPanel135: TsPanel;
    sPanel136: TsPanel;
    edt_AVTBANK: TsEdit;
    sPanel137: TsPanel;
    edt_AVTBANK1: TsEdit;
    edt_AVTBANK2: TsEdit;
    edt_AVTBANK3: TsEdit;
    edt_AVTBANK4: TsEdit;
    qryListAVT_BANK_BIC: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    sComboBox1: TsComboBox;
    sComboBox2: TsComboBox;
    qryCopy: TADOQuery;
    sImage2: TsImage;
    sHeader: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    edt_MSEQ: TsEdit;
    sSpeedButton2: TsSpeedButton;
    procedure sMaskEdit1Change(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton13Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure sComboBox2Change(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sImage2Click(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
  private
    procedure ReadList(SortField: TColumn = nil);
    procedure INF707_UPLOAD_LIVART;
    procedure DeleteDocument;
  protected
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_INF707_NEW_frm: TUI_INF707_NEW_frm;

implementation

uses
  MSSQL, ICON, MessageDefine, VarDefine, TypeDefine, LivartConfig, SQLCreator, RECVDOC, Preview, QR_INF707_PRN,
  QuickRpt, Dlg_ImportINF707;

{$R *.dfm}

procedure TUI_INF707_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;

    SQL.Text := qryCopy.SQL.Text;
    SQL.Add('WHERE (DATEE BETWEEN :FDATE AND :TDATE) AND ');
    case sComboBox1.ItemIndex of
      //관리번호
      0: SQL.Add('(I707_1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))');
      //LC번호
      1: SQL.Add('(I707_1.CD_NO LIKE :MAINT_NO OR (1=:ALLDATA))');
    end;
    SQL.Add('ORDER BY DATEE DESC');

    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'A707_1.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_INF707_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
//  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_INF707_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_INF707_NEW_frm.ReadData;
begin
//  inherited;
//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  edt_MSEQ.Text     := qryListMSeq.AsString;
  msk_Datee.Text    := qryListDATEE.AsString;
  edt_userno.Text   := qryListUSER_ID.AsString;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

//------------------------------------------------------------------------------
// PAGE 1
//------------------------------------------------------------------------------
  edt_APPDATE.Text := qryListAPP_DATE.AsString;
  msk_IssDate.Text := qryListISS_DATE.AsString;
  msk_AMDDATE.Text := qryListAMD_DATE.AsString;
  curr_AMD_NO.Value := qryListAMD_NO.AsInteger;
  IF UpperCase( qryListIS_CANCEL.AsString ) = 'CR' Then
    com_isCancel.ItemIndex := 1
  else
    com_isCancel.ItemIndex := 0;
  edt_cdNo.Text := qryListCD_NO.AsString;
  edt_RecvRef.Text := qryListRCV_REF.AsString;
  edt_IssRef.Text := qryListIBANK_REF.AsString;

  edt_In_Mathod.Text := qryListIN_MATHOD.AsString;
  edt_In_Mathod1.Text := qryListmathod_Name.AsString;
  edt_ApBank.Text  := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString;
  edt_ApBank2.Text := qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString;
  edt_ApBank4.Text := qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;

  edt_AdBank.Text  := qryListAD_BANK.AsString;
  edt_AdBank1.Text := qryListAD_BANK1.AsString;
  edt_AdBank2.Text := qryListAD_BANK2.AsString;
  edt_AdBank3.Text := qryListAD_BANK3.AsString;
  edt_AdBank4.Text := qryListAD_BANK4.AsString;

  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd1_1.Text := qryListImp_Name_1.AsString;
  edt_ImpCd1_2.Text := qryListImp_Name_2.AsString;
  edt_ImpCd1_3.Text := qryListImp_Name_3.AsString;
  edt_ImpCd1_4.Text := qryListImp_Name_4.AsString;
  edt_ImpCd1_5.Text := qryListImp_Name_5.AsString;
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILno5.Text := qryListIL_NO5.AsString;
  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;
  curr_ILAMT1.Text := qryListIL_AMT1.AsString;
  curr_ILAMT2.Text := qryListIL_AMT2.AsString;
  curr_ILAMT3.Text := qryListIL_AMT3.AsString;
  curr_ILAMT4.Text := qryListIL_AMT4.AsString;
  curr_ILAMT5.Text := qryListIL_AMT5.AsString;
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;

  edt_APP_RULE1.Text := qryListAPP_RULE1.AsString;
  edt_APP_RULE2.Text := qryListAPP_RULE2.AsString;

  edt_SR_INFO1.Text := qryListSR_INFO1.AsString;
  edt_SR_INFO2.Text := qryListSR_INFO2.AsString;
  edt_SR_INFO3.Text := qryListSR_INFO3.AsString;
  edt_SR_INFO4.Text := qryListSR_INFO4.AsString;
  edt_SR_INFO5.Text := qryListSR_INFO5.AsString;
  edt_SR_INFO6.Text := qryListSR_INFO6.AsString;

  edt_REIBANK.Text := qryListREIM_BANK_BIC.AsString;
  edt_REIBANK1.Text := qryListREIM_BANK1.AsString;
  edt_REIBANK2.Text := qryListREIM_BANK2.AsString;
  edt_REIBANK3.Text := qryListREIM_BANK3.AsString;
  edt_REIBANK4.Text := qryListREIM_BANK4.AsString;

  edt_AVTBANK.Text := qryListAVT_BANK_BIC.AsString;
  edt_AVTBANK1.Text := qryListAVT_BANK1.AsString;
  edt_AVTBANK2.Text := qryListAVT_BANK2.AsString;
  edt_AVTBANK3.Text := qryListAVT_BANK3.AsString;
  edt_AVTBANK4.Text := qryListAVT_BANK4.AsString;
//------------------------------------------------------------------------------
// PAGE 2
//------------------------------------------------------------------------------
  edt_ApplicOrg1.Text := qryListAPPLIC1.AsString;
  edt_ApplicOrg2.Text := qryListAPPLIC2.AsString;
  edt_ApplicOrg3.Text := qryListAPPLIC3.AsString;
  edt_ApplicOrg4.Text := qryListAPPLIC4.AsString;

  IF qryListAPPLIC_CHG1.AsString <> '' Then
  begin
    sPanel14.Caption := '개설의뢰인 정보변경';
    edt_Applic1.Text := qryListAPPLIC_CHG1.AsString;
    edt_Applic2.Text := qryListAPPLIC_CHG2.AsString;
    edt_Applic3.Text := qryListAPPLIC_CHG3.AsString;
    edt_Applic4.Text := qryListAPPLIC_CHG4.AsString;
  end
  else
  begin
    sPanel14.Caption := '개설의뢰인';
    edt_Applic1.Text := qryListAPPLIC1.AsString;
    edt_Applic2.Text := qryListAPPLIC2.AsString;
    edt_Applic3.Text := qryListAPPLIC3.AsString;
    edt_Applic4.Text := qryListAPPLIC4.AsString;
  end;
//  edt_Applic5.Text := qryListAPPLIC5.AsString;

  IF qryListBENEFC1.AsString <> '' Then
  begin
    edt_Benefc1.Text := qryListBENEFC1.AsString;
    edt_Benefc2.Text := qryListBENEFC2.AsString;
    edt_Benefc3.Text := qryListBENEFC3.AsString;
    edt_Benefc4.Text := qryListBENEFC4.AsString;
    edt_Benefc5.Text := qryListBENEFC5.AsString;
  end;

  edt_Doccd.Text      := qryListDOC_CD.AsString;
  edt_Doccd1.Text     := qryListDOC_CDNM.AsString;
//  edt_C_METHOD.Text   := qryListCARRIAGE.AsString;
//  edt_C_METHODNM.Text := qryListCarriage_Name.AsString;
  msk_exDate.Text     := qryListEX_DATE.AsString;
  edt_exPlace.Text    := qryListEX_PLACE.AsString;
  edt_IncdCur.Text    := qryListINCD_CUR.AsString;
  edt_IncdAmt.Value   := qryListINCD_AMT.AsCurrency;
  edt_DecdCur.Text := qryListDECD_CUR.AsString;
  edt_DecdAmt.Value := qryListDECD_AMT.AsCurrency;
  edt_CdPerp.Value := qryListCD_PERP.AsCurrency;
  edt_CdPerm.Value := qryListCD_PERM.AsCurrency;

  memo_charge_1.Clear;
  com_charge.ItemIndex     := AnsiIndexText(UpperCase(qryListCHARGE.AsString),['','2AF','2AG','2AH']);
  IF com_charge.ItemIndex = 3 Then
    memo_charge_1.Text := qryListCHARGE_1.AsString;

  memo_amd_charge_1.Clear;
  com_amd_charge.ItemIndex := AnsiIndexText(UpperCase(qryListAMD_CHARGE.AsString),['','2AC','2AD','2AE']);
  IF com_amd_charge.ItemIndex = 3 Then
    memo_amd_charge_1.Text := qryListAMD_CHARGE_1.AsString;

//------------------------------------------------------------------------------
// PAGE 3
//------------------------------------------------------------------------------
  CM_INDEX(com_PShip,[':'],0,qryListPSHIP.AsString,0);
  CM_INDEX(com_TShip,[':'],0,qryListTSHIP.AsString,0);
  edt_loadOn.Text := qryListLOAD_ON.AsString;
  edt_forTran.Text := qryListFOR_TRAN.AsString;
  msk_lstDate.Text := qryListLST_DATE.AsString;
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;
  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;
  edt_shipPD6.Text := qryListSHIP_PD6.AsString;
  memo_SPECIAL_PAY.Text := qryListSPECIAL_PAY.AsString;
//------------------------------------------------------------------------------
// PAGE 4
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := qryListAA_CV1.AsString;
  edt_Aaccv2.Text := qryListAA_CV2.AsString;
  edt_Aaccv3.Text := qryListAA_CV3.AsString;
  edt_Aaccv4.Text := qryListAA_CV4.AsString;
  edt_MIX1.Text := qryListMIX1.AsString;
  edt_MIX2.Text := qryListMIX2.AsString;
  edt_MIX3.Text := qryListMIX3.AsString;
  edt_MIX4.Text := qryListMIX4.AsString;
  edt_DRAFT1.Text := qryListDRAFT1.AsString;
  edt_DRAFT2.Text := qryListDRAFT2.AsString;
  edt_DRAFT3.Text := qryListDRAFT3.AsString;
  edt_DEFPAY1.Text := qryListDEFPAY1.AsString;
  edt_DEFPAY2.Text := qryListDEFPAY2.AsString;
  edt_DEFPAY3.Text := qryListDEFPAY3.AsString;
  edt_DEFPAY4.Text := qryListDEFPAY4.AsString;
  memo_GoodsDesc.Text := qryListGOODS_DESC.AsString;
  memo_TXT78.Text := qryListTXT_78.AsString;
//------------------------------------------------------------------------------
// PAGE 5
//------------------------------------------------------------------------------
  memo_DOCREQ.Text := qryListDOC_REQ.AsString;

//------------------------------------------------------------------------------
// PAGE 6
//------------------------------------------------------------------------------
  memo_ADD_CONDITION.Text := qryListADD_CONDITION.AsString;
  edt_period.Text := qryListPERIOD_DAYS.AsString;
  com_PeriodIndex.ItemIndex := qryListPERIOD_IDX.AsInteger;
  IF com_PeriodIndex.ItemIndex = 1 Then
    edt_PeriodDetail.Text := qryListPERIOD_DETAIL.AsString;
  //DA: WITHOUT, DB: MAY ADD, DC: CONFIRM
  com_Confirm.ItemIndex := AnsiIndexText(UpperCase(qryListCONFIRM.AsString),['','DA','DB','DC']);
  edt_CONFIRM_BIC.Text := qryListCONFIRM_BIC.AsString;
  edt_CONFIRM1.Text := qryListCONFIRM1.AsString;
  edt_CONFIRM2.Text := qryListCONFIRM2.AsString;
  edt_CONFIRM3.Text := qryListCONFIRM3.AsString;
  edt_CONFIRM4.Text := qryListCONFIRM4.AsString;
  //명의인
  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  //식별부호
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  //주소
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

  //개설은행 전자서명
  edt_OP_BANK1.Text := qryListOP_BANK1.Text;
  edt_OP_BANK2.Text := qryListOP_BANK2.Text;
  edt_OP_BANK3.Text := qryListOP_BANK3.Text;
  edt_OP_ADDR1.Text  := qryListOP_ADDR1.Text;
  edt_OP_ADDR2.Text  := qryListOP_ADDR2.Text;

end;

procedure TUI_INF707_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_INF707_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
  sButton13.Enabled := DataSet.RecordCount > 0;
end;

procedure TUI_INF707_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  com_Pship.Perform(CB_SETDROPPEDWIDTH, 300, 0);
  com_Tship.Perform(CB_SETDROPPEDWIDTH, 300, 0);

  sBitBtn1Click(nil);


  sPageControl1.ActivePageIndex := 0;

  sButton13.Visible := LoginData.sCompanyNo = '1358132239';
end;

procedure TUI_INF707_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_INF707_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_INF707_NEW_frm := nil;
end;

const
  UPLOAD_SID : String = 'LDF_KIS';
  SYSDATE : string = 'SYSDATE';
procedure TUI_INF707_NEW_frm.INF707_UPLOAD_LIVART;
var
  SC : TSQLCreate;
  __MAINT_NO, __MSEQ : String;
begin
  IF qrylist.RecordCount = 0 Then
  begin
    raise Exception.Create('업로드할 데이터가 없습니다');
  end;

  SC := TSQLCreate.Create;

  with TADOQuery.Create(nil) do
  begin
    try
      IF not Assigned(LivartConfig_frm) Then LivartConfig_frm := TLivartConfig_frm.Create(Self);
      LivartConfig_frm.setConnectionString;
      Connection := LivartConfig_frm.LivartConn;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('TC_KIS_LCRCHR_RCV_IF');

      SC.ADDValue('CO_GBCD','11');
      __MAINT_NO := qrylistMAINT_NO.AsString;
      __MSEQ := qryListMSEQ.AsString;
      SC.ADDValue('IMPTNO',__MAINT_NO);
      SC.ADDValue('IMPTSQ',__MSEQ ,vtInteger);
      SC.ADDValue('AMNDSQ',qrylistAMD_NO.AsString ,vtInteger);

      SC.ADDValue('USER_ID',LoginData.sID);
      SC.ADDValue('MESSAGE1', qrylistMESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qrylistMESSAGE2.AsString);
      SC.ADDValue('DATEE', qrylistDATEE.AsString);
      SC.ADDValue('APP_DATE', qrylistAPP_DATE.AsString);
      SC.ADDValue('IN_MATHOD', qrylistIN_MATHOD.AsString);
      SC.ADDValue('AP_BANK1', qrylistAP_BANK1.AsString);
      SC.ADDValue('AP_BANK2', qrylistAP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qrylistAP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qrylistAP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qrylistAP_BANK5.AsString);
      SC.ADDValue('AD_BANK1', qrylistAD_BANK1.AsString);
      SC.ADDValue('AD_BANK2', qrylistAD_BANK2.AsString);
      SC.ADDValue('AD_BANK3', qrylistAD_BANK3.AsString);
      SC.ADDValue('AD_BANK4', qrylistAD_BANK4.AsString);
      SC.ADDValue('IL_NO1', qrylistIL_NO1.AsString);
      SC.ADDValue('IL_NO2', qrylistIL_NO2.AsString);
      SC.ADDValue('IL_NO3', qrylistIL_NO3.AsString);
      SC.ADDValue('IL_NO4', qrylistIL_NO4.AsString);
      SC.ADDValue('IL_NO5', qrylistIL_NO5.AsString);
      SC.ADDValue('IL_AMT1', qrylistIL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qrylistIL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qrylistIL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qrylistIL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qrylistIL_AMT5.AsString,vtInteger);
//      SC.ADDValue('IL_AMT6', qrylistIL_AMT6.AsString,vtInteger);
      SC.ADDValue('IL_CUR1', qrylistIL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qrylistIL_CUR2.AsString);
      SC.ADDValue('IL_CUR3', qrylistIL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qrylistIL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qrylistIL_CUR5.AsString);
//      SC.ADDValue('IL_CUR6', qrylistIL_CUR6.AsString);
      SC.ADDValue('AD_INFO1', qrylistAD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qrylistAD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qrylistAD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qrylistAD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qrylistAD_INFO5.AsString);
      SC.ADDValue('CD_NO', qrylistCD_NO.AsString);
      SC.ADDValue('RCV_REF', qrylistRCV_REF.AsString);
      SC.ADDValue('ISS_BANK1', qrylistISS_BANK1.AsString);
      SC.ADDValue('ISS_BANK2', qrylistISS_BANK2.AsString);
      SC.ADDValue('ISS_BANK3', qrylistISS_BANK3.AsString);
      SC.ADDValue('ISS_BANK4', qrylistISS_BANK4.AsString);
      SC.ADDValue('ISS_BANK5', qrylistISS_BANK5.AsString);
      SC.ADDValue('ISS_ACCNT', qrylistISS_ACCNT.AsString);
      SC.ADDValue('ISS_DATE', qrylistISS_DATE.AsString);
      SC.ADDValue('AMD_DATE', qrylistAMD_DATE.AsString);
      SC.ADDValue('EX_DATE', qrylistEX_DATE.AsString);
      SC.ADDValue('APPLIC1', qrylistAPPLIC1.AsString);
      SC.ADDValue('APPLIC2', qrylistAPPLIC2.AsString);
      SC.ADDValue('APPLIC3', qrylistAPPLIC3.AsString);
      SC.ADDValue('APPLIC4', qrylistAPPLIC4.AsString);
      SC.ADDValue('APPLIC5', qrylistAPPLIC5.AsString);
      SC.ADDValue('BENEFC1', qrylistBENEFC1.AsString);
      SC.ADDValue('BENEFC2', qrylistBENEFC2.AsString);
      SC.ADDValue('BENEFC3', qrylistBENEFC3.AsString);
      SC.ADDValue('BENEFC4', qrylistBENEFC4.AsString);
      SC.ADDValue('BENEFC5', qrylistBENEFC5.AsString);
      SC.ADDValue('INCD_CUR', qrylistINCD_CUR.AsString);
      SC.ADDValue('INCD_AMT', qrylistINCD_AMT.AsString);
      SC.ADDValue('DECO_CUR', qrylistDECD_CUR.AsString);
      SC.ADDValue('DECO_AMT', qrylistDECD_AMT.AsString,vtInteger);
      SC.ADDValue('NWCD_CUR', qrylistNWCD_CUR.AsString);
      SC.ADDValue('NWCD_AMT', qrylistNWCD_AMT.AsString,vtInteger);
      SC.ADDValue('BFCD_CUR', qrylistBFCD_CUR.AsString);
      SC.ADDValue('BFCD_AMT', qrylistBFCD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_PERP', qrylistCD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qrylistCD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qrylistCD_MAX.AsString);
      SC.ADDValue('AA_CV1', qrylistAA_CV1.AsString);
      SC.ADDValue('AA_CV2', qrylistAA_CV2.AsString);
      SC.ADDValue('AA_CV3', qrylistAA_CV3.AsString);
      SC.ADDValue('AA_CV4', qrylistAA_CV4.AsString);
      //------------------------------------------------------------------------
      // 2017-12-19
      // INF707 For Oracle 필드에 SUNJUCK_PORT, DOCHAK_PORT Varchar(35) 추가
      //------------------------------------------------------------------------
      SC.ADDValue('SUNJUCK_PORT', qrylistSUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHAK_PORT', qrylistDOCHACK_PORT.AsString);
      SC.ADDValue('LOAD_ON', qrylistLOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qrylistFOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qrylistLST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qrylistSHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qrylistSHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qrylistSHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qrylistSHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qrylistSHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qrylistSHIP_PD6.AsString);
      IF qrylistNARRAT.AsBoolean Then
        SC.ADDValue('NARRT', '1')
      else
        SC.ADDValue('NARRT', '0');
      SC.ADDValue('NARRT_1',qrylistNARRAT_1.AsString);
      SC.ADDValue('SR_INFO1', qrylistSR_INFO1.AsString);
      SC.ADDValue('SR_INFO2', qrylistSR_INFO2.AsString);
      SC.ADDValue('SR_INFO3', qrylistSR_INFO3.AsString);
      SC.ADDValue('SR_INFO4', qrylistSR_INFO4.AsString);
      SC.ADDValue('SR_INFO5', qrylistSR_INFO5.AsString);
      SC.ADDValue('SR_INFO6', qrylistSR_INFO6.AsString);
      SC.ADDValue('EX_NAME', qrylistEX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qrylistEX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qrylistEX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qrylistEX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qrylistEX_ADDR2.AsString);
      SC.ADDValue('OP_BANK1', qrylistOP_BANK1.AsString);
      SC.ADDValue('OP_BANK2', qrylistOP_BANK2.AsString);
      SC.ADDValue('OP_BANK3', qrylistOP_BANK3.AsString);
      //DP->OP 로 잘못표기함
      SC.ADDValue('DP_ADDR1', qrylistOP_ADDR1.AsString);
      SC.ADDValue('DP_ADDR2', qrylistOP_ADDR2.AsString);

    //------------------------------------------------------------------------------
    // 2018-09-19 스위프트 변경
    //------------------------------------------------------------------------------
      SC.ADDValue('IS_CANCEL', qryListIS_CANCEL.AsString);
      SC.ADDValue('applic_chg1', qryListAPPLIC_CHG1.AsString);
      SC.ADDValue('applic_chg2', qryListAPPLIC_CHG2.AsString);
      SC.ADDValue('applic_chg3', qryListAPPLIC_CHG3.AsString);
      SC.ADDValue('applic_chg4', qryListAPPLIC_CHG4.AsString);
      SC.ADDValue('exploc', qryListEX_PLACE.AsString);
      SC.ADDValue('confirm_banknm', qryListCONFIRM1.AsString + qryListCONFIRM2.AsString);
      SC.ADDValue('confirm_bankbr', qryListCONFIRM3.AsString + qryListCONFIRM4.AsString);
      SC.ADDValue('charge', qryListCHARGE.AsString);
      SC.ADDValue('charge_1', qryListCHARGE_1.AsString);
      SC.ADDValue('amd_charge', qryListAMD_CHARGE.AsString);
      SC.ADDValue('amd_charge_1', qryListAMD_CHARGE_1.AsString);
      SC.ADDValue('mix1', qryListMIX1.AsString);
      SC.ADDValue('mix2', qryListMIX2.AsString);
      SC.ADDValue('mix3', qryListMIX3.AsString);
      SC.ADDValue('mix4', qryListMIX4.AsString);
      SC.ADDValue('draft1', qryListDRAFT1.AsString);
      SC.ADDValue('draft2', qryListDRAFT2.AsString);
      SC.ADDValue('draft3', qryListDRAFT3.AsString);
      SC.ADDValue('defpay1', qryListDEFPAY1.AsString);
      SC.ADDValue('defpay2', qryListDEFPAY2.AsString);
      SC.ADDValue('defpay3', qryListDEFPAY3.AsString);
      SC.ADDValue('defpay4', qryListDEFPAY4.AsString);
      SC.ADDValue('special_pay', qryListSPECIAL_PAY.AsString);
      SC.ADDValue('goods_desc', qryListGOODS_DESC.AsString);
      SC.ADDValue('doc_req', qryListDOC_REQ.AsString);
      SC.ADDValue('add_condition', qryListADD_CONDITION.AsString);
      SC.ADDValue('period_days', qryListPERIOD_DAYS.asInteger);
      SC.ADDValue('period_idx', qryListPERIOD_IDX.asInteger);
      SC.ADDValue('period_txt', qryListPERIOD_DETAIL.AsString);
      SC.ADDValue('txt_78_text', qryListTXT_78.AsString);
      SC.ADDValue('pship', qryListPSHIP.AsString);
      SC.ADDValue('tship', qryListTSHIP.AsString);
      SC.ADDValue('confirmm', qryListCONFIRM.AsString);

      SC.ADDValue('FRST_REG_USER_ID',UPLOAD_SID);
      SC.ADDValue('FRST_REG_DTM',SYSDATE,vtVariant);
      SC.ADDValue('LAST_MOD_USER_ID',UPLOAD_SID);
      SC.ADDValue('LAST_MOD_DTM',SYSDATE,vtVariant);
      AddLog('INF707',SC.FieldList);
      SQL.Text := SC.CreateSQL;
      AddLog('INF707',SQL.Text);      
      try
        ExecSQL;
      except
        on e:exception do
        begin
          ShowMessage(e.Message);
        end;
      end;
    finally
      LivartConfig_frm.LivartConn.Close;
      Freeandnil(LivartConfig_frm);
      Close;
      Free;
      SC.Free;
    end;
  end;

end;

procedure TUI_INF707_NEW_frm.sButton13Click(Sender: TObject);
begin
//  inherited;
  try
    INF707_UPLOAD_LIVART;
    ShowMessage('업로드가 완료되었습니다');
  except
    on E:Exception do
    begin
      ShowMessage('에러가 발생하여 업로드에 실패했습니다'#13#10+E.Message);
    end;
  end;
end;

procedure TUI_INF707_NEW_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  DeleteDocument;
end;

procedure TUI_INF707_NEW_frm.DeleteDocument;
var
  maint_no , chasu_no, AMD_NO : String;
  nCursor : Integer;
begin
  inherited;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
    chasu_no := qryListMSEQ.AsString;
    AMD_NO := qryListAMD_NO.AsString;
    
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;
        SQL.Text :=  'DELETE FROM INF707_1 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and MSEQ = ' + QuotedStr(chasu_no) + ' AND AMD_NO = '+AMD_NO;
        ExecSQL;

        SQL.Text :=  'DELETE FROM INF707_2 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and MSEQ = ' + QuotedStr(chasu_no) + ' AND AMD_NO = '+AMD_NO;
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end
      else
        DMMssql.KISConnect.RollbackTrans;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_INF707_NEW_frm.sComboBox1Change(Sender: TObject);
begin
  inherited;
  sComboBox2.ItemIndex := sComboBox1.ItemIndex; 
end;

procedure TUI_INF707_NEW_frm.sComboBox2Change(Sender: TObject);
begin
  inherited;
  sComboBox1.ItemIndex := sComboBox2.ItemIndex; 
end;

procedure TUI_INF707_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  QR_INF707_PRN_frm := TQR_INF707_PRN_frm.Create(Self);
  Preview_frm := TPreview_frm.Create(Self);
  try
    with QR_INF707_PRN_frm do
    begin
      MAINT_NO := Self.qryListMAINT_NO.AsString;
      MSEQ := Self.qryListMSEQ.AsInteger;
      AMD_NO := Self.qryListAMD_NO.AsInteger;
    end;
    Preview_frm.Report := QR_INF707_PRN_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(QR_INF707_PRN_frm);
  end;
end;

procedure TUI_INF707_NEW_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 81) and (Shift = [ssctrl]) then
  begin
    sImage2.Visible := not sImage2.Visible;
  end;
end;

procedure TUI_INF707_NEW_frm.sImage2Click(Sender: TObject);
begin
  inherited;
  Dlg_ImportINF707_frm := TDlg_ImportINF707_frm.Create(Self);
  try
    Dlg_ImportINF707_frm.ShowModal;
    sBitBtn1Click(sBitBtn1);
  finally
    FreeAndNil(Dlg_ImportINF707_frm);
  end;  
end;

procedure TUI_INF707_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 6;
end;

end.
