inherited UI_ADV700_NEW_frm: TUI_ADV700_NEW_frm
  Left = 535
  Top = 146
  BorderWidth = 4
  Caption = '[ADV700] '#49688#52636#49888#50857#51109#53685#51648#49436
  ClientHeight = 673
  ClientWidth = 1029
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1029
    Height = 74
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1029
      74)
    object sSpeedButton4: TsSpeedButton
      Left = 353
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 32
      Top = 14
      Width = 142
      Height = 23
      Alignment = taCenter
      Caption = #49688#52636#49888#50857#51109' '#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 64
      Top = 37
      Width = 73
      Height = 21
      Caption = '(ADV700)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton7: TsSpeedButton
      Left = 1054
      Top = 27
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 200
      Top = 3
      Width = 8
      Height = 66
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 954
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 213
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 283
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 209
      Top = 40
      Width = 816
      Height = 32
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object sSpeedButton1: TsSpeedButton
        Left = 600
        Top = 5
        Width = 11
        Height = 22
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 209
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 440
        Top = 4
        Width = 89
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 672
        Top = 4
        Width = 33
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 768
        Top = 4
        Width = 43
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 320
        Top = 4
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 74
    Width = 341
    Height = 599
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBgrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 541
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 211
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = sMaskEdit1Change
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 0
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 232
      Top = 576
      Width = 108
      Height = 55
      
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 55
        Top = 257
        Width = 230
        Height = 21
        Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 55
        Top = 281
        Width = 126
        Height = 12
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 74
    Width = 688
    Height = 599
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      Enabled = False
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 686
      Height = 597
      ActivePage = sTabSheet8
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabOrder = 1
      OnChange = sPageControl1Change
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          DesignSize = (
            678
            561)
          object msk_APP_DATE: TsMaskEdit
            Left = 183
            Top = 9
            Width = 108
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 0
            Color = clWhite
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_APPNO: TsEdit
            Left = 183
            Top = 33
            Width = 276
            Height = 23
            Color = 10944511
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APBANK: TsEdit
            Left = 183
            Top = 64
            Width = 146
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APBANK1: TsEdit
            Left = 9
            Top = 88
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APBANK2: TsEdit
            Left = 9
            Top = 112
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APBANK3: TsEdit
            Left = 9
            Top = 136
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APBANK4: TsEdit
            Left = 9
            Top = 160
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ADBANK1: TsEdit
            Left = 345
            Top = 88
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ADBANK2: TsEdit
            Left = 345
            Top = 112
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ADBANK3: TsEdit
            Left = 345
            Top = 136
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ADBANK4: TsEdit
            Left = 345
            Top = 160
            Width = 320
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel2: TsPanel
            Left = 9
            Top = 9
            Width = 74
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 11
          end
          object sPanel8: TsPanel
            Left = 84
            Top = 9
            Width = 98
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #53685#51648#51068#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 12
          end
          object sPanel10: TsPanel
            Left = 84
            Top = 33
            Width = 98
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #53685#51648#48264#54840
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object sPanel9: TsPanel
            Left = 9
            Top = 64
            Width = 53
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'MT700'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object sPanel11: TsPanel
            Left = 63
            Top = 64
            Width = 119
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51204#47928#48156#49888#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 15
          end
          object sPanel12: TsPanel
            Left = 399
            Top = 64
            Width = 119
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51204#47928#49688#49888#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 16
          end
          object sPanel126: TsPanel
            Left = 345
            Top = 64
            Width = 53
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'MT700'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object edt_ADBANK: TsEdit
            Left = 519
            Top = 64
            Width = 146
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel127: TsPanel
            Left = 9
            Top = 216
            Width = 74
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object sPanel128: TsPanel
            Left = 84
            Top = 216
            Width = 246
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44592#53440#51221#48372'('#48512#44032#51221#48372')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object memo_ADDINFO1: TsMemo
            Tag = 122
            Left = 9
            Top = 240
            Width = 660
            Height = 146
            Hint = 'DesGood'
            Anchors = [akLeft, akTop, akRight]
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 21
            WordWrap = False
            SkinData.CustomFont = True
          end
          object sPanel129: TsPanel
            Left = 592
            Top = 216
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Anchors = [akTop, akRight]
            Caption = '5R * 65C'
            Color = 9211135
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object edt_ADBANK5: TsEdit
            Left = 399
            Top = 184
            Width = 266
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 23
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel130: TsPanel
            Left = 345
            Top = 184
            Width = 53
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'TEL'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
            Visible = False
          end
          object sEdit14: TsEdit
            Left = 9
            Top = 184
            Width = 320
            Height = 23
            Color = clBtnFace
            Enabled = False
            TabOrder = 25
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = 'SWIFT PAGE 1'
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel15: TsPanel
            Left = 9
            Top = 57
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '40A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel1: TsPanel
            Left = 44
            Top = 57
            Width = 189
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Form of Documentary Credit'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object edt_DOCCD1: TsEdit
            Left = 234
            Top = 57
            Width = 170
            Height = 23
            Hint = #49888#50857#51109#51333#47448
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            Text = 'IRREVOCABLE TRANSFERABLE'
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel13: TsPanel
            Left = 9
            Top = 81
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '20'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object sPanel14: TsPanel
            Left = 44
            Top = 81
            Width = 189
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Documentary Credit Number'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object edt_CDNO: TsEdit
            Left = 234
            Top = 81
            Width = 170
            Height = 23
            Hint = #49888#50857#51109#48264#54840
            Color = 10092543
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel16: TsPanel
            Left = 9
            Top = 105
            Width = 34
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '40E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object sPanel17: TsPanel
            Left = 44
            Top = 105
            Width = 117
            Height = 47
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Applicable Rules'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object edt_APPLICABLERULES1: TsEdit
            Left = 162
            Top = 105
            Width = 207
            Height = 23
            Hint = #49888#50857#51109' '#51201#50857#44508#52825
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPLICABLERULES2: TsEdit
            Left = 162
            Top = 129
            Width = 407
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel18: TsPanel
            Left = 9
            Top = 153
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '23'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
          end
          object sPanel19: TsPanel
            Left = 44
            Top = 153
            Width = 250
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Reference to Pre-Advice'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 11
          end
          object edt_REEPRE: TsEdit
            Left = 295
            Top = 153
            Width = 274
            Height = 23
            Hint = #49440#53685#51648' '#52280#51312#49324#54637
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel21: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
          end
          object sPanel22: TsPanel
            Left = 44
            Top = 9
            Width = 101
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Date of Issue'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object msk_ISSDATE: TsMaskEdit
            Left = 146
            Top = 9
            Width = 87
            Height = 23
            Hint = #44060#49444#51068#51088
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            ParentShowHint = False
            ShowHint = True
            TabOrder = 15
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sPanel23: TsPanel
            Left = 9
            Top = 33
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 16
          end
          object sPanel25: TsPanel
            Left = 44
            Top = 33
            Width = 149
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Date and Place of Expiry'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object msk_EXDATE: TsMaskEdit
            Left = 194
            Top = 33
            Width = 87
            Height = 23
            Hint = #50976#54952#44592#51068
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            ParentShowHint = False
            ShowHint = True
            TabOrder = 18
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_EXPLACE: TsEdit
            Left = 282
            Top = 33
            Width = 382
            Height = 23
            Hint = #50976#54952#51109#49548
            Color = clWhite
            Enabled = False
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel26: TsPanel
            Left = 9
            Top = 233
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '51a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object sPanel28: TsPanel
            Left = 44
            Top = 233
            Width = 165
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Applicant Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object edt_APPBANK: TsEdit
            Left = 210
            Top = 233
            Width = 120
            Height = 23
            Hint = #44060#49444#51032#47280#51064' '#51008#54665'(BIC'#53076#46300')'
            Color = clWhite
            Enabled = False
            MaxLength = 11
            ParentShowHint = False
            ShowHint = True
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPBANK1: TsEdit
            Left = 81
            Top = 257
            Width = 249
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel30: TsPanel
            Left = 9
            Top = 257
            Width = 71
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object edt_APPBANK2: TsEdit
            Left = 81
            Top = 279
            Width = 249
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPBANK3: TsEdit
            Left = 81
            Top = 301
            Width = 249
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPBANK4: TsEdit
            Left = 81
            Top = 323
            Width = 249
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPACCNT: TsEdit
            Left = 81
            Top = 347
            Width = 249
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel31: TsPanel
            Left = 9
            Top = 347
            Width = 71
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object sPanel32: TsPanel
            Left = 9
            Top = 380
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '50'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 30
          end
          object sPanel33: TsPanel
            Left = 44
            Top = 380
            Width = 286
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Applicant'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object edt_APPLIC1: TsEdit
            Left = 9
            Top = 404
            Width = 321
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPLIC2: TsEdit
            Left = 9
            Top = 428
            Width = 321
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPLIC3: TsEdit
            Left = 9
            Top = 452
            Width = 321
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPLIC4: TsEdit
            Left = 9
            Top = 476
            Width = 321
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel36: TsPanel
            Left = 9
            Top = 205
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '32B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 36
          end
          object sPanel37: TsPanel
            Left = 44
            Top = 205
            Width = 149
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Currency Code, Amount'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 37
          end
          object edt_CDCUR: TsEdit
            Left = 194
            Top = 205
            Width = 34
            Height = 23
            Hint = #44060#49444#44552#50529' '#53685#54868
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 38
            Text = 'KRW'
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel38: TsPanel
            Left = 9
            Top = 181
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 39
          end
          object sPanel40: TsPanel
            Left = 44
            Top = 181
            Width = 234
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Percentage Credit Amount Tolerance'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 40
          end
          object edt_cdPerP: TsCurrencyEdit
            Left = 308
            Top = 181
            Width = 42
            Height = 23
            Hint = #44284#48512#51313' '#54728#50857#50984'+'
            AutoSize = False
            
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 41
            BoundLabel.Active = True
            BoundLabel.Caption = '('#65291')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object edt_cdPerM: TsCurrencyEdit
            Left = 375
            Top = 181
            Width = 39
            Height = 23
            Hint = #44284#48512#51313' '#54728#50857#50984'-'
            AutoSize = False
            
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 42
            BoundLabel.Active = True
            BoundLabel.Caption = '/ ('#65293')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            DecimalPlaces = 4
            DisplayFormat = '0'
          end
          object sPanel39: TsPanel
            Left = 334
            Top = 380
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '59'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 43
          end
          object sPanel41: TsPanel
            Left = 369
            Top = 380
            Width = 297
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Beneficiary'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 44
          end
          object edt_BENEFC1: TsEdit
            Left = 334
            Top = 404
            Width = 332
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 45
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC2: TsEdit
            Left = 334
            Top = 428
            Width = 332
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 46
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC3: TsEdit
            Left = 334
            Top = 452
            Width = 332
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 47
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC4: TsEdit
            Left = 334
            Top = 476
            Width = 332
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 48
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC5: TsEdit
            Left = 406
            Top = 500
            Width = 260
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel42: TsPanel
            Left = 334
            Top = 500
            Width = 71
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 50
          end
          object sPanel142: TsPanel
            Left = 334
            Top = 524
            Width = 71
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51204#54868#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 51
          end
          object edt_BENEFC6: TsEdit
            Left = 406
            Top = 524
            Width = 260
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 52
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_CDAMT: TsCurrencyEdit
            Left = 229
            Top = 205
            Width = 121
            Height = 23
            Hint = #44284#48512#51313' '#54728#50857#50984'-'
            AutoSize = False
            
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 53
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = 'PAGE 2'
        object sPanel34: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel43: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '39C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel44: TsPanel
            Left = 44
            Top = 9
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional Amounts Covered'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object edt_AACV1: TsEdit
            Left = 9
            Top = 33
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AACV2: TsEdit
            Left = 9
            Top = 57
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AACV3: TsEdit
            Left = 9
            Top = 81
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AACV4: TsEdit
            Left = 9
            Top = 105
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel45: TsPanel
            Left = 226
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48512#44032#44552#50529#48512#45812
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object sPanel46: TsPanel
            Left = 345
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 7
          end
          object sPanel47: TsPanel
            Left = 380
            Top = 9
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Drafts at...'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object edt_DRAFT1: TsEdit
            Left = 345
            Top = 33
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAFT2: TsEdit
            Left = 345
            Top = 57
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAFT3: TsEdit
            Left = 345
            Top = 81
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAFT4: TsEdit
            Left = 345
            Top = 105
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel48: TsPanel
            Left = 562
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54868#54872#50612#51020#51312#44148
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object sPanel49: TsPanel
            Left = 9
            Top = 137
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '41a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
          end
          object sPanel50: TsPanel
            Left = 44
            Top = 137
            Width = 133
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Available with...by...'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 15
          end
          object sPanel51: TsPanel
            Left = 178
            Top = 137
            Width = 150
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49888#50857#51109#51648#44553#48169#49885' '#48143' '#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 16
          end
          object edt_AVPAY: TsEdit
            Left = 80
            Top = 161
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel52: TsPanel
            Left = 9
            Top = 161
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#44553#48169#49885
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object sPanel53: TsPanel
            Left = 9
            Top = 185
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object edt_AVAIL: TsEdit
            Left = 80
            Top = 185
            Width = 120
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel54: TsPanel
            Left = 9
            Top = 209
            Width = 70
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object edt_AVAIL1: TsEdit
            Left = 80
            Top = 209
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVAIL2: TsEdit
            Left = 80
            Top = 231
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVAIL3: TsEdit
            Left = 80
            Top = 253
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVAIL4: TsEdit
            Left = 80
            Top = 275
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel55: TsPanel
            Left = 9
            Top = 299
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object edt_AVACCNT: TsEdit
            Left = 80
            Top = 299
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel56: TsPanel
            Left = 345
            Top = 137
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 28
          end
          object sPanel57: TsPanel
            Left = 380
            Top = 137
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Drawee'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object sPanel58: TsPanel
            Left = 562
            Top = 137
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #50612#51020#51648#44553#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel61: TsPanel
            Left = 345
            Top = 161
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object edt_DRAWEE: TsEdit
            Left = 416
            Top = 161
            Width = 120
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel62: TsPanel
            Left = 345
            Top = 185
            Width = 70
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object edt_DRAWEE1: TsEdit
            Left = 416
            Top = 185
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAWEE2: TsEdit
            Left = 416
            Top = 207
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAWEE3: TsEdit
            Left = 416
            Top = 229
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DRAWEE4: TsEdit
            Left = 416
            Top = 251
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel63: TsPanel
            Left = 345
            Top = 275
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
          object edt_DRACCNT: TsEdit
            Left = 416
            Top = 275
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 39
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel60: TsPanel
            Left = 9
            Top = 329
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42M'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 40
          end
          object sPanel64: TsPanel
            Left = 44
            Top = 329
            Width = 173
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Mixed Payment Details'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 41
          end
          object edt_MIXPAY1: TsEdit
            Left = 9
            Top = 353
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 42
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_MIXPAY2: TsEdit
            Left = 9
            Top = 377
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 43
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_MIXPAY3: TsEdit
            Left = 9
            Top = 401
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 44
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_MIXPAY4: TsEdit
            Left = 9
            Top = 425
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 45
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel65: TsPanel
            Left = 218
            Top = 329
            Width = 110
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54844#54633#51648#44553#51312#44148#47749#49464
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 46
          end
          object sPanel66: TsPanel
            Left = 345
            Top = 329
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 47
          end
          object sPanel67: TsPanel
            Left = 380
            Top = 329
            Width = 284
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Negotiation/Deferred Payment Details'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 48
          end
          object edt_DEFPAY1: TsEdit
            Left = 345
            Top = 353
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DEFPAY2: TsEdit
            Left = 345
            Top = 377
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 50
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DEFPAY3: TsEdit
            Left = 345
            Top = 401
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 51
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DEFPAY4: TsEdit
            Left = 345
            Top = 425
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 52
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel68: TsPanel
            Left = 9
            Top = 457
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43P'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 53
          end
          object sPanel69: TsPanel
            Left = 44
            Top = 457
            Width = 117
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Partial Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 54
          end
          object edt_PSHIP: TsEdit
            Left = 162
            Top = 457
            Width = 263
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 55
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel70: TsPanel
            Left = 9
            Top = 481
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43T'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 56
          end
          object sPanel72: TsPanel
            Left = 44
            Top = 481
            Width = 117
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Transhipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 57
          end
          object edt_TSHIP: TsEdit
            Left = 162
            Top = 481
            Width = 263
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 58
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel73: TsPanel
            Left = 9
            Top = 505
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '43T'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 59
          end
          object sPanel74: TsPanel
            Left = 44
            Top = 505
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Latest Date of Shipment'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 60
          end
          object msk_LSTDATE: TsMaskEdit
            Left = 226
            Top = 505
            Width = 87
            Height = 23
            Hint = #44060#49444#51068#51088
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            ParentShowHint = False
            ShowHint = True
            TabOrder = 61
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
        end
      end
      object sTabSheet7: TsTabSheet
        Caption = 'PAGE3'
        object sPanel102: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel75: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel76: TsPanel
            Left = 44
            Top = 9
            Width = 389
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Place of Taking in Charge/Dispatch from.../Place of Receipt'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object edt_LOADON: TsEdit
            Left = 9
            Top = 33
            Width = 656
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel77: TsPanel
            Left = 434
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#53441'('#48156#49569')'#51648
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object sPanel78: TsPanel
            Left = 9
            Top = 57
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object sPanel79: TsPanel
            Left = 44
            Top = 57
            Width = 389
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 
              'Place of Final Destination/For Transportation to... /Place of De' +
              'livery'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object edt_FORTRAN: TsEdit
            Left = 9
            Top = 81
            Width = 656
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel80: TsPanel
            Left = 434
            Top = 57
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #52572#51333#47785#51201#51648
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object sPanel81: TsPanel
            Left = 9
            Top = 252
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
          end
          object sPanel82: TsPanel
            Left = 44
            Top = 252
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Loading/Airport of Departure'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object edt_SUNJUCKPORT: TsEdit
            Left = 9
            Top = 276
            Width = 656
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel83: TsPanel
            Left = 290
            Top = 252
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49440#51201#54637
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 11
          end
          object sPanel84: TsPanel
            Left = 9
            Top = 300
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44F'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object sPanel85: TsPanel
            Left = 44
            Top = 300
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Port of Discharge/Airport of Destination'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object edt_DOCHACKPORT: TsEdit
            Left = 9
            Top = 324
            Width = 656
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel86: TsPanel
            Left = 290
            Top = 300
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #46020#52265#54637
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 15
          end
          object sPanel87: TsPanel
            Left = 9
            Top = 105
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '44D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 16
          end
          object sPanel88: TsPanel
            Left = 44
            Top = 105
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Shipment Period'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object sPanel90: TsPanel
            Left = 290
            Top = 105
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49440#51201#44592#44036
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object edt_SHIPPD1: TsMemo
            Left = 9
            Top = 129
            Width = 656
            Height = 120
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 19
          end
          object sPanel91: TsPanel
            Left = 9
            Top = 353
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '45A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object sPanel92: TsPanel
            Left = 44
            Top = 353
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Description of Goods and/or Services'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object sPanel93: TsPanel
            Left = 290
            Top = 353
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49345#54408#50857#50669#47749#49464
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object memo_DESGOOD1: TsMemo
            Left = 9
            Top = 377
            Width = 656
            Height = 152
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 23
          end
        end
      end
      object sTabSheet5: TsTabSheet
        Caption = 'PAGE 4'
        object sPanel35: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel94: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '46A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel95: TsPanel
            Left = 44
            Top = 9
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Documents Required'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object sPanel96: TsPanel
            Left = 290
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44396#48708#49436#47448
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object memo_DOCREQU1: TsMemo
            Left = 9
            Top = 33
            Width = 656
            Height = 232
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 3
          end
          object sPanel97: TsPanel
            Left = 9
            Top = 273
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '47A'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object sPanel98: TsPanel
            Left = 44
            Top = 273
            Width = 245
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Additional Conditions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object sPanel99: TsPanel
            Left = 290
            Top = 273
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48512#44032#51312#44148
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object memo_ADDCOND1: TsMemo
            Left = 9
            Top = 297
            Width = 656
            Height = 232
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 7
          end
        end
      end
      object sTabSheet6: TsTabSheet
        Caption = 'PAGE 5'
        object sPanel71: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel100: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '71D'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel101: TsPanel
            Left = 44
            Top = 9
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Charges'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object edt_CHARGE1: TsEdit
            Left = 9
            Top = 33
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_CHARGE2: TsEdit
            Left = 9
            Top = 55
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_CHARGE3: TsEdit
            Left = 9
            Top = 77
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel103: TsPanel
            Left = 226
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#49688#47308#48512#45812#51088
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object sPanel104: TsPanel
            Left = 9
            Top = 107
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '48'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object sPanel105: TsPanel
            Left = 44
            Top = 107
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Period for Presentation in days'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object sPanel106: TsPanel
            Left = 226
            Top = 107
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49436#47448#51228#49884#44592#44036
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object edt_PERIOD_IN_DAYS: TsEdit
            Left = 9
            Top = 131
            Width = 34
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel107: TsPanel
            Left = 44
            Top = 131
            Width = 39
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Days'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 10
          end
          object edt_PERIOD_DETAIL: TsEdit
            Left = 84
            Top = 131
            Width = 244
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel108: TsPanel
            Left = 9
            Top = 179
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object sPanel109: TsPanel
            Left = 44
            Top = 179
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Confirmation Instructions'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object edt_CONFIRMM: TsEdit
            Left = 226
            Top = 179
            Width = 123
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 14
            Text = 'WITH OUT'
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel110: TsPanel
            Left = 350
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '53a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 15
          end
          object sPanel111: TsPanel
            Left = 385
            Top = 9
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Reimbursing Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 16
          end
          object sPanel112: TsPanel
            Left = 567
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49345#54872#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object sPanel113: TsPanel
            Left = 350
            Top = 33
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 18
          end
          object edt_REIBANK: TsEdit
            Left = 421
            Top = 33
            Width = 120
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel114: TsPanel
            Left = 350
            Top = 57
            Width = 70
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object edt_REIBANK1: TsEdit
            Left = 421
            Top = 57
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REIBANK2: TsEdit
            Left = 421
            Top = 79
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REIBANK3: TsEdit
            Left = 421
            Top = 101
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REIBANK4: TsEdit
            Left = 421
            Top = 123
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel115: TsPanel
            Left = 350
            Top = 147
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_REIACCMT: TsEdit
            Left = 421
            Top = 147
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel116: TsPanel
            Left = 350
            Top = 179
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '58a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 27
          end
          object sPanel117: TsPanel
            Left = 385
            Top = 179
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Requested Confirmation Party'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel118: TsPanel
            Left = 567
            Top = 179
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54869#51064#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object sPanel119: TsPanel
            Left = 350
            Top = 203
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object edt_REQ_BANK: TsEdit
            Left = 421
            Top = 203
            Width = 120
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel120: TsPanel
            Left = 350
            Top = 227
            Width = 70
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_REQ_BANK1: TsEdit
            Left = 421
            Top = 227
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REQ_BANK2: TsEdit
            Left = 421
            Top = 249
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REQ_BANK3: TsEdit
            Left = 421
            Top = 271
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_REQ_BANK4: TsEdit
            Left = 421
            Top = 293
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel121: TsPanel
            Left = 9
            Top = 329
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '78'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 37
          end
          object sPanel122: TsPanel
            Left = 44
            Top = 329
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Instructions to the Paying/Accepting/Negotiating Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 38
          end
          object sPanel123: TsPanel
            Left = 418
            Top = 329
            Width = 250
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#44553'/'#51064#49688'/'#47588#51077#51008#54665#50640' '#45824#54620' '#51648#49884#49324#54637
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 39
          end
          object memo_INSTRCT1: TsMemo
            Left = 9
            Top = 353
            Width = 659
            Height = 176
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 40
          end
        end
      end
      object sTabSheet8: TsTabSheet
        Caption = 'PAGE 6'
        object sPanel124: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 561
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel125: TsPanel
            Left = 9
            Top = 9
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '57a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sPanel134: TsPanel
            Left = 44
            Top = 9
            Width = 181
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '"Advise Through" Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object sPanel135: TsPanel
            Left = 226
            Top = 9
            Width = 102
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #52572#51333#53685#51648#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object sPanel136: TsPanel
            Left = 9
            Top = 33
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object edt_AVTBANK: TsEdit
            Left = 80
            Top = 33
            Width = 120
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel137: TsPanel
            Left = 9
            Top = 57
            Width = 70
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object edt_AVTBANK1: TsEdit
            Left = 80
            Top = 57
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVTBANK2: TsEdit
            Left = 80
            Top = 79
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVTBANK3: TsEdit
            Left = 80
            Top = 101
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AVTBANK4: TsEdit
            Left = 80
            Top = 123
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel138: TsPanel
            Left = 9
            Top = 147
            Width = 70
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 10
          end
          object edt_AVTACCNT: TsEdit
            Left = 80
            Top = 147
            Width = 248
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel139: TsPanel
            Left = 338
            Top = 9
            Width = 34
            Height = 27
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '72Z'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
          end
          object sPanel140: TsPanel
            Left = 373
            Top = 9
            Width = 196
            Height = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Sender to Receiver Information'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object edt_SNDINFO1: TsEdit
            Left = 338
            Top = 37
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SNDINFO2: TsEdit
            Left = 338
            Top = 59
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SNDINFO3: TsEdit
            Left = 338
            Top = 81
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel141: TsPanel
            Left = 570
            Top = 9
            Width = 102
            Height = 27
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#49888#51008#54665#50526#51221#48372
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object edt_SNDINFO4: TsEdit
            Left = 338
            Top = 103
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SNDINFO5: TsEdit
            Left = 338
            Top = 125
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SNDINFO6: TsEdit
            Left = 338
            Top = 147
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EXNAME1: TsEdit
            Left = 83
            Top = 208
            Width = 246
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EXNAME2: TsEdit
            Left = 83
            Top = 232
            Width = 246
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EXNAME3: TsEdit
            Left = 83
            Top = 256
            Width = 246
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EXADDR1: TsEdit
            Left = 83
            Top = 280
            Width = 246
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel59: TsPanel
            Left = 83
            Top = 184
            Width = 246
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #53685#51648#51008#54665' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_EXADDR2: TsEdit
            Left = 83
            Top = 304
            Width = 246
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel132: TsPanel
            Left = 8
            Top = 184
            Width = 74
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sPanel89: TsPanel
            Left = 8
            Top = 208
            Width = 74
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #47749#51032#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel131: TsPanel
            Left = 8
            Top = 256
            Width = 74
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51204#51088#49436#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object sPanel133: TsPanel
            Left = 8
            Top = 280
            Width = 74
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51452#49548
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel152: TsPanel
            Left = 9
            Top = 336
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '49G'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 31
          end
          object sPanel153: TsPanel
            Left = 44
            Top = 336
            Width = 373
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Special Payment Conditions for Beneficiary'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object sPanel154: TsPanel
            Left = 418
            Top = 336
            Width = 250
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#51061#51088' '#53945#48324#51648#44553#51312#44148
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object memo_Special: TsMemo
            Left = 9
            Top = 360
            Width = 659
            Height = 176
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 34
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 678
          Height = 529
          TabStop = False
          Align = alClient
          Color = clGray
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 80
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 220
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'ISS_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51068#51088
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'EX_DATE'
              Title.Alignment = taCenter
              Title.Caption = #50976#54952#44592#51068
              Width = 80
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'CD_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CD_CUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 60
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'APPLIC1'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51032#47280#51064
              Width = 220
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 0
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 1
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
            OnClick = sBitBtn1Click
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 0
    Top = 48
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180817'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT '#9'ADV700.MAINT_NO, ADV700.Chk1, ADV700.Chk2, ADV700.Chk3, ' +
        'ADV700.MESSAGE1, ADV700.MESSAGE2, ADV700.USER_ID, ADV700.DATEE, ' +
        'ADV700.APP_DATE, ADV700.APP_NO, ADV700.AP_BANK, ADV700.AP_BANK1,' +
        ' ADV700.AP_BANK2, ADV700.AP_BANK3, ADV700.AP_BANK4, ADV700.AP_BA' +
        'NK5, '
      
        #9'ADV700.AD_BANK, ADV700.AD_BANK1, ADV700.AD_BANK2, ADV700.AD_BAN' +
        'K3, ADV700.AD_BANK4, ADV700.AD_BANK5, ADV700.ADDINFO, ADV700.ADD' +
        'INFO_1, ADV700.DOC_CD1, ADV700.DOC_CD2, ADV700.DOC_CD3, ADV700.C' +
        'D_NO, ADV700.REE_PRE, ADV700.ISS_DATE, ADV700.EX_DATE, '
      
        #9'ADV700.EX_PLACE, ADV700.APP_BANK, ADV700.APP_BANK1, ADV700.APP_' +
        'BANK2, ADV700.APP_BANK3, ADV700.APP_BANK4, ADV700.APP_BANK5, ADV' +
        '700.APP_ACCNT, ADV700.APPLIC1, ADV700.APPLIC2, ADV700.APPLIC3, A' +
        'DV700.APPLIC4, ADV700.APPLIC5, '
      
        #9'ADV700.BENEFC1, ADV700.BENEFC2, ADV700.BENEFC3, ADV700.BENEFC4,' +
        ' ADV700.BENEFC5, ADV700.CD_AMT, ADV700.CD_CUR, ADV700.CD_PERP, A' +
        'DV700.CD_PERM, ADV700.CD_MAX, ADV700.AA_CV1, ADV700.AA_CV2, ADV7' +
        '00.AA_CV3, ADV700.AA_CV4, ADV700.BENEFC6, ADV700.PRNO,'
      #9
      
        #9'ADV7001.MAINT_NO, ADV7001.AVAIL, ADV7001.AVAIL1, ADV7001.AVAIL2' +
        ', ADV7001.AVAIL3, ADV7001.AVAIL4, ADV7001.AVAIL5, ADV7001.AV_ACC' +
        'NT, ADV7001.AV_PAY, ADV7001.DRAFT1, ADV7001.DRAFT2, ADV7001.DRAF' +
        'T3, ADV7001.DRAFT4, '
      
        #9'ADV7001.DRAWEE, ADV7001.DRAWEE1, ADV7001.DRAWEE2, ADV7001.DRAWE' +
        'E3, ADV7001.DRAWEE4, ADV7001.DRAWEE5, ADV7001.DR_ACCNT, ADV7001.' +
        'MIX_PAY1, ADV7001.MIX_PAY2, ADV7001.MIX_PAY3, ADV7001.MIX_PAY4, ' +
        'ADV7001.DEF_PAY1, ADV7001.DEF_PAY2, ADV7001.DEF_PAY3, ADV7001.DE' +
        'F_PAY4, '
      
        #9'ADV7001.LST_DATE, ADV7001.SHIP_PD, ADV7001.SHIP_PD1, ADV7001.SH' +
        'IP_PD2, ADV7001.SHIP_PD3, ADV7001.SHIP_PD4, ADV7001.SHIP_PD5, AD' +
        'V7001.SHIP_PD6,'
      ''
      
        #9'ADV7002.MAINT_NO, ADV7002.DESGOOD, ADV7002.DESGOOD_1, ADV7002.D' +
        'OCREQU, ADV7002.DOCREQU_1, ADV7002.ADDCOND, ADV7002.ADDCOND_1, '
      
        #9'ADV7002.CHARGE1, ADV7002.CHARGE2, ADV7002.CHARGE3, ADV7002.CHAR' +
        'GE4, ADV7002.CHARGE5, ADV7002.CHARGE6, ADV7002.PD_PRSNT1, ADV700' +
        '2.PD_PRSNT2, ADV7002.PD_PRSNT3, ADV7002.PD_PRSNT4, '
      
        #9'ADV7002.CONFIRMM, ADV7002.REI_BANK, ADV7002.REI_BANK1, ADV7002.' +
        'REI_BANK2, ADV7002.REI_BANK3, ADV7002.REI_BANK4, ADV7002.REI_BAN' +
        'K5, ADV7002.REI_ACCNT, ADV7002.INSTRCT, ADV7002.INSTRCT_1, ADV70' +
        '02.AVT_BANK, ADV7002.AVT_BANK1, ADV7002.AVT_BANK2, ADV7002.AVT_B' +
        'ANK3, ADV7002.AVT_BANK4, ADV7002.AVT_BANK5, '
      
        #9'ADV7002.AVT_ACCNT, ADV7002.SND_INFO1, ADV7002.SND_INFO2, ADV700' +
        '2.SND_INFO3, ADV7002.SND_INFO4, ADV7002.SND_INFO5, ADV7002.SND_I' +
        'NFO6, ADV7002.EX_NAME1, ADV7002.EX_NAME2, ADV7002.EX_NAME3, ADV7' +
        '002.EX_ADDR1, ADV7002.EX_ADDR2, ADV7002.CHK1, ADV7002.CHK2, ADV7' +
        '002.CHK3, ADV7002.PRNO,'
      
        '                ADV7002.PD_PRSNT5, ADV7002.CO_BANK, ADV7002.CO_B' +
        'ANK1, ADV7002.CO_BANK2, ADV7002.CO_BANK3, ADV7002.CO_BANK4, ADV7' +
        '002.SPECIAL_DESC, ADV7002.SPECIAL_DESC_1,'
      #9
      
        #9'ADV7003.MAINT_NO, ADV7003.FOR_TRAN, ADV7003.LOAD_ON, ADV7003.TS' +
        'HIP, ADV7003.PSHIP, ADV7003.APPLICABLE_RULES_1, ADV7003.APPLICAB' +
        'LE_RULES_2, ADV7003.SUNJUCK_PORT, ADV7003.DOCHACK_PORT,'
      
        '                PERIOD_IN_DAYS, PERIOD_DETAIL, REQ_BANK, REQ_BAN' +
        'K1, REQ_BANK2, REQ_BANK3, REQ_BANK4 '
      'FROM ADV700 AS ADV700'
      
        'INNER JOIN ADV7001 AS ADV7001 ON ADV700.MAINT_NO = ADV7001.MAINT' +
        '_NO'
      
        'INNER JOIN ADV7002 AS ADV7002 ON ADV700.MAINT_NO = ADV7002.MAINT' +
        '_NO'
      
        'INNER JOIN ADV7003 AS ADV7003 ON ADV700.MAINT_NO = ADV7003.MAINT' +
        '_NO'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(ADV700.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 56
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListChk1: TBooleanField
      FieldName = 'Chk1'
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK5: TStringField
      FieldName = 'AD_BANK5'
      Size = 35
    end
    object qryListADDINFO: TStringField
      FieldName = 'ADDINFO'
      Size = 1
    end
    object qryListADDINFO_1: TMemoField
      FieldName = 'ADDINFO_1'
      BlobType = ftMemo
    end
    object qryListDOC_CD1: TStringField
      FieldName = 'DOC_CD1'
      Size = 35
    end
    object qryListDOC_CD2: TStringField
      FieldName = 'DOC_CD2'
      Size = 35
    end
    object qryListDOC_CD3: TStringField
      FieldName = 'DOC_CD3'
      Size = 65
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListREE_PRE: TStringField
      FieldName = 'REE_PRE'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListAPP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryListAPP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryListAPP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryListAPP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryListAPP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryListAPP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryListAPP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 70
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListBENEFC6: TStringField
      FieldName = 'BENEFC6'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryListAVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryListAVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryListAVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryListAVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryListAVAIL5: TStringField
      FieldName = 'AVAIL5'
      Size = 35
    end
    object qryListAV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryListAV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListDRAFT4: TStringField
      FieldName = 'DRAFT4'
      Size = 35
    end
    object qryListDRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryListDRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryListDRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryListDRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryListDRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryListDRAWEE5: TStringField
      FieldName = 'DRAWEE5'
      Size = 35
    end
    object qryListDR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TStringField
      FieldName = 'SHIP_PD'
      Size = 1
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListDESGOOD: TStringField
      FieldName = 'DESGOOD'
      Size = 1
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListDOCREQU: TStringField
      FieldName = 'DOCREQU'
      Size = 1
    end
    object qryListDOCREQU_1: TMemoField
      FieldName = 'DOCREQU_1'
      BlobType = ftMemo
    end
    object qryListADDCOND: TStringField
      FieldName = 'ADDCOND'
      Size = 1
    end
    object qryListADDCOND_1: TMemoField
      FieldName = 'ADDCOND_1'
      BlobType = ftMemo
    end
    object qryListCHARGE1: TStringField
      FieldName = 'CHARGE1'
      Size = 35
    end
    object qryListCHARGE2: TStringField
      FieldName = 'CHARGE2'
      Size = 35
    end
    object qryListCHARGE3: TStringField
      FieldName = 'CHARGE3'
      Size = 35
    end
    object qryListCHARGE4: TStringField
      FieldName = 'CHARGE4'
      Size = 35
    end
    object qryListCHARGE5: TStringField
      FieldName = 'CHARGE5'
      Size = 35
    end
    object qryListCHARGE6: TStringField
      FieldName = 'CHARGE6'
      Size = 35
    end
    object qryListPD_PRSNT1: TStringField
      FieldName = 'PD_PRSNT1'
      Size = 35
    end
    object qryListPD_PRSNT2: TStringField
      FieldName = 'PD_PRSNT2'
      Size = 35
    end
    object qryListPD_PRSNT3: TStringField
      FieldName = 'PD_PRSNT3'
      Size = 35
    end
    object qryListPD_PRSNT4: TStringField
      FieldName = 'PD_PRSNT4'
      Size = 35
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 35
    end
    object qryListREI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryListREI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryListREI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryListREI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryListREI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryListREI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryListREI_ACCNT: TStringField
      FieldName = 'REI_ACCNT'
      Size = 35
    end
    object qryListINSTRCT: TStringField
      FieldName = 'INSTRCT'
      Size = 1
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListAVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryListAVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryListAVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryListAVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryListAVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryListAVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryListAVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryListSND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryListSND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryListSND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryListSND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryListSND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryListSND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListCHK1_1: TStringField
      FieldName = 'CHK1_1'
      Size = 1
    end
    object qryListCHK2_1: TStringField
      FieldName = 'CHK2_1'
      Size = 1
    end
    object qryListCHK3_1: TStringField
      FieldName = 'CHK3_1'
      Size = 10
    end
    object qryListPRNO_1: TIntegerField
      FieldName = 'PRNO_1'
    end
    object qryListMAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryListAPPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListPERIOD_IN_DAYS: TIntegerField
      FieldName = 'PERIOD_IN_DAYS'
    end
    object qryListPERIOD_DETAIL: TStringField
      FieldName = 'PERIOD_DETAIL'
      Size = 35
    end
    object qryListREQ_BANK: TStringField
      FieldName = 'REQ_BANK'
      Size = 11
    end
    object qryListREQ_BANK1: TStringField
      FieldName = 'REQ_BANK1'
      Size = 35
    end
    object qryListREQ_BANK2: TStringField
      FieldName = 'REQ_BANK2'
      Size = 35
    end
    object qryListREQ_BANK3: TStringField
      FieldName = 'REQ_BANK3'
      Size = 35
    end
    object qryListREQ_BANK4: TStringField
      FieldName = 'REQ_BANK4'
      Size = 35
    end
    object qryListPD_PRSNT5: TStringField
      FieldName = 'PD_PRSNT5'
      Size = 35
    end
    object qryListCO_BANK: TStringField
      FieldName = 'CO_BANK'
      Size = 11
    end
    object qryListCO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Size = 35
    end
    object qryListCO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Size = 35
    end
    object qryListCO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Size = 35
    end
    object qryListCO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Size = 35
    end
    object qryListSPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Size = 1
    end
    object qryListSPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      BlobType = ftMemo
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 88
    Top = 192
  end
end
