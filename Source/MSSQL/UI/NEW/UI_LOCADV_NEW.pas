unit UI_LOCADV_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCAPP_NEW, DB, ADODB, sSkinProvider, StdCtrls, sMemo,
  sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sCheckBox, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, StrUtils, DateUtils,
  QuickRpt, QRCtrls;

type
  TUI_LOCADV_NEW_frm = class(TUI_LOCAPP_NEW_frm)
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    qryLOCADV: TADOQuery;
    qryLOCADVMAINT_NO: TStringField;
    qryLOCADVCHK1: TStringField;
    qryLOCADVCHK2: TStringField;
    qryLOCADVCHK3: TStringField;
    qryLOCADVUSER_ID: TStringField;
    qryLOCADVDATEE: TStringField;
    qryLOCADVBGM_REF: TStringField;
    qryLOCADVMESSAGE1: TStringField;
    qryLOCADVMESSAGE2: TStringField;
    qryLOCADVBUSINESS: TStringField;
    qryLOCADVRFF_NO: TStringField;
    qryLOCADVLC_NO: TStringField;
    qryLOCADVOFFERNO1: TStringField;
    qryLOCADVOFFERNO2: TStringField;
    qryLOCADVOFFERNO3: TStringField;
    qryLOCADVOFFERNO4: TStringField;
    qryLOCADVOFFERNO5: TStringField;
    qryLOCADVOFFERNO6: TStringField;
    qryLOCADVOFFERNO7: TStringField;
    qryLOCADVOFFERNO8: TStringField;
    qryLOCADVOFFERNO9: TStringField;
    qryLOCADVOPEN_NO: TBCDField;
    qryLOCADVADV_DATE: TStringField;
    qryLOCADVISS_DATE: TStringField;
    qryLOCADVDOC_PRD: TBCDField;
    qryLOCADVDELIVERY: TStringField;
    qryLOCADVEXPIRY: TStringField;
    qryLOCADVTRANSPRT: TStringField;
    qryLOCADVGOODDES: TStringField;
    qryLOCADVGOODDES1: TMemoField;
    qryLOCADVREMARK: TStringField;
    qryLOCADVREMARK1: TMemoField;
    qryLOCADVAP_BANK: TStringField;
    qryLOCADVAP_BANK1: TStringField;
    qryLOCADVAP_BANK2: TStringField;
    qryLOCADVAPPLIC1: TStringField;
    qryLOCADVAPPLIC2: TStringField;
    qryLOCADVAPPLIC3: TStringField;
    qryLOCADVBENEFC1: TStringField;
    qryLOCADVBENEFC2: TStringField;
    qryLOCADVBENEFC3: TStringField;
    qryLOCADVEXNAME1: TStringField;
    qryLOCADVEXNAME2: TStringField;
    qryLOCADVEXNAME3: TStringField;
    qryLOCADVDOCCOPY1: TBCDField;
    qryLOCADVDOCCOPY2: TBCDField;
    qryLOCADVDOCCOPY3: TBCDField;
    qryLOCADVDOCCOPY4: TBCDField;
    qryLOCADVDOCCOPY5: TBCDField;
    qryLOCADVDOC_ETC: TStringField;
    qryLOCADVDOC_ETC1: TMemoField;
    qryLOCADVLOC_TYPE: TStringField;
    qryLOCADVLOC1AMT: TBCDField;
    qryLOCADVLOC1AMTC: TStringField;
    qryLOCADVLOC2AMT: TBCDField;
    qryLOCADVLOC2AMTC: TStringField;
    qryLOCADVEX_RATE: TBCDField;
    qryLOCADVDOC_DTL: TStringField;
    qryLOCADVDOC_NO: TStringField;
    qryLOCADVDOC_AMT: TBCDField;
    qryLOCADVDOC_AMTC: TStringField;
    qryLOCADVLOADDATE: TStringField;
    qryLOCADVEXPDATE: TStringField;
    qryLOCADVIM_NAME: TStringField;
    qryLOCADVIM_NAME1: TStringField;
    qryLOCADVIM_NAME2: TStringField;
    qryLOCADVIM_NAME3: TStringField;
    qryLOCADVDEST: TStringField;
    qryLOCADVISBANK1: TStringField;
    qryLOCADVISBANK2: TStringField;
    qryLOCADVPAYMENT: TStringField;
    qryLOCADVEXGOOD: TStringField;
    qryLOCADVEXGOOD1: TMemoField;
    qryLOCADVPRNO: TIntegerField;
    qryLOCADVBSN_HSCODE: TStringField;
    qryLOCADVAPPADDR1: TStringField;
    qryLOCADVAPPADDR2: TStringField;
    qryLOCADVAPPADDR3: TStringField;
    qryLOCADVBNFADDR1: TStringField;
    qryLOCADVBNFADDR2: TStringField;
    qryLOCADVBNFADDR3: TStringField;
    qryLOCADVBNFEMAILID: TStringField;
    qryLOCADVBNFDOMAIN: TStringField;
    qryLOCADVCD_PERP: TIntegerField;
    qryLOCADVCD_PERM: TIntegerField;
    qryLOCADVBUSINESSNAME: TStringField;
    qryLOCADVTRANSPRTNAME: TStringField;
    qryLOCADVLOC_TYPENAME: TStringField;
    qryLOCADVDOC_DTLNAME: TStringField;
    qryLOCADVPAYMENTNAME: TStringField;
    qryLOCADVDESTNAME: TStringField;
    sEdit2: TsEdit;
    cur_LOCAMT_W: TsCurrencyEdit;
    sEdit3: TsEdit;
    msk_issdate: TsMaskEdit;
    procedure qryLOCADVAfterScroll(DataSet: TDataSet);
    procedure qryLOCADVAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure DelDocument;
  protected
    procedure ReadData; virtual;  
    procedure ReadList(SortField: TColumn = nil); override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOCADV_NEW_frm: TUI_LOCADV_NEW_frm;

implementation

uses
  LOCADV_PRINT, Preview, MSSQL, MessageDefine, ChildForm;

{$R *.dfm}

{ TUI_LOCADV_NEW_frm }

procedure TUI_LOCADV_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryLOCADV do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCADV.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;

    Open;
  end;
end;

procedure TUI_LOCADV_NEW_frm.ReadData;
var
  TempStr : String;
  nIDX : integer;
begin
  with qryLOCADV do
  begin
    if RecordCount > 0 then
    begin
      TempStr := qryLOCADVMAINT_NO.AsString;
      nIDX := Pos('-', TempStr);
      edt_MAINT_NO_Header.Text := LeftStr(TempStr, nIDX-1);
      edt_MAINT_NO_Bank.Text := MidStr(TempStr, nIDX+1, 2);
      edt_MAINT_NO_Serial.Text := MidStr(TempStr, nIDX+3, 9);
//      edt_MAINT_NO_Header.Text := LeftStr(TempStr,14);
//      edt_MAINT_NO_Bank.Text := MidStr(TempStr,16,2);
//      edt_MAINT_NO_Serial.Text := MidStr(TempStr,18,9);

      msk_Datee.Text := qryLOCADVDATEE.AsString;
      edt_userno.Text := qryLOCADVUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryLOCADVMESSAGE1.AsString, 5);
      CM_INDEX(com_type, [':'], 0, qryLOCADVMESSAGE2.AsString);      

      //개설의뢰인
//      edt_wcd.Text     := qryLOCADVAPPLIC.AsString;
      edt_wsangho.Text := qryLOCADVAPPLIC1.AsString;
      edt_wceo.Text    := qryLOCADVAPPLIC2.AsString;
      edt_waddr1.Text  := qryLOCADVAPPADDR1.AsString;
      edt_waddr2.Text  := qryLOCADVAPPADDR2.AsString;
      edt_waddr3.Text  := qryLOCADVAPPADDR3.AsString;
      edt_wsaupno.Text := qryLOCADVAPPLIC3.AsString;

      //수혜자
//      edt_rcd.Text     := qryLOCADVBENEFC.AsString;
      edt_rsangho.Text := qryLOCADVBENEFC1.AsString;
      edt_rceo.Text    := qryLOCADVBENEFC2.AsString;
      edt_raddr1.Text  := qryLOCADVBNFADDR1.AsString;
      edt_raddr2.Text  := qryLOCADVBNFADDR2.AsString;
      edt_raddr3.Text  := qryLOCADVBNFADDR3.AsString;
      edt_rsaupno.Text := qryLOCADVBENEFC3.AsString;
      edt_remail.Text  := qryLOCADVBNFEMAILID.AsString+ '@' +qryLOCADVBNFDOMAIN.AsString;
      IF edt_remail.Text = '@' then
        edt_remail.Clear;

      //수발신인식별자
      edt_iden1.Text := AnsiReplaceText( MidStr(qryLOCADVBENEFC3.AsString,11,100) , '/' , ' / ');
//      edt_iden2.Text := qryLOCADVCHKNAME2.AsString;

      //개설은행
      edt_cbank.Text   := qryLOCADVAP_BANK.AsString;
      edt_cbanknm.Text := qryLOCADVAP_BANK1.AsString;
      edt_cbrunch.Text := qryLOCADVAP_BANK2.AsString;

      //신용장종류
      edt_cType.Text := qryLOCADVLOC_TYPE.AsString;
      sEdit8.Text := qryLOCADVLOC_TYPENAME.AsString;

      //개설금액
      edt_LOCAMT_UNIT.Text := qryLOCADVLOC1AMTC.AsString;
      cur_LOCAMT.Value     := qryLOCADVLOC1AMT.AsCurrency;
      sEdit2.Text :=          qryLOCADVLOC2AMTC.asString;
      cur_LOCAMT_W.Value   := qryLOCADVLOC2AMT.AsCurrency;


      //허용오차
      //+
      edt_perr.Text := qryLOCADVCD_PERP.AsString;
      //-
      edt_merr.Text := qryLOCADVCD_PERM.AsString;

      //개설근거별 용도
      edt_cyong.Text := qryLOCADVBUSINESS.AsString;
      edt_cyongnm.Text := qryLOCADVBUSINESSNAME.AsString;

      //LC번호
      edt_lcno.Text := qryLOCADVLC_NO.AsString;

      //매도확약서
      edt_doc1.Text := qryLOCADVOFFERNO1.AsString;
      edt_doc2.Text := qryLOCADVOFFERNO2.AsString;
      edt_doc3.Text := qryLOCADVOFFERNO3.AsString;
      edt_doc4.Text := qryLOCADVOFFERNO4.AsString;
      edt_doc5.Text := qryLOCADVOFFERNO5.AsString;
      edt_doc6.Text := qryLOCADVOFFERNO6.AsString;
      edt_doc7.Text := qryLOCADVOFFERNO7.AsString;
      edt_doc8.Text := qryLOCADVOFFERNO8.AsString;
      edt_doc9.Text := qryLOCADVOFFERNO9.AsString;

      //주유구비서류
      cur_AttachDoc1.Value := qryLOCADVDOCCOPY1.AsInteger;
      cur_AttachDoc2.Value := qryLOCADVDOCCOPY2.AsInteger;
      cur_AttachDoc3.Value := qryLOCADVDOCCOPY3.AsInteger;
      cur_AttachDoc4.Value := qryLOCADVDOCCOPY4.AsInteger;
      cur_AttachDoc5.Value := qryLOCADVDOCCOPY5.AsInteger;

      //서류제시기간
      case qryLOCADVDOC_PRD.AsInteger of
        5: comDocPeriod.ItemIndex := 0;
        7: comDocPeriod.ItemIndex := 1;
      end;

      //개설관련
      cur_chasu.Value := qryLOCADVOPEN_NO.AsInteger;
      //개설신청일자
      msk_CrtDT.Text := qryLOCADVADV_DATE.AsString;
//      msk_CrtDT.Text := qryLOCADVAPP_DATE.AsString;
      //물품인도기일
      msk_IndoDt.Text := qryLOCADVDELIVERY.AsString;
      //유효기일
      msk_ExpiryDT.Text := qryLOCADVEXPIRY.AsString;
      //통지일자
      msk_issdate.Text := qryLOCADVISS_DATE.AsString;
      //분할허용여부
      edt_isPart.Text := qryLOCADVTRANSPRT.AsString;
      sEdit20.Text := qryLOCADVTRANSPRTNAME.AsString; 
      //------------------------------------------------------------------------------
      // 대표공급물품
      //------------------------------------------------------------------------------
      msk_hscd.Text     := qryLOCADVBSN_HSCODE.AsString;
      memo_goods.Text   := qryLOCADVGOODDES1.AsString;
      memo_etcdoc.Text  := qryLOCADVDOC_ETC1.AsString;
      memo_etcinfo.Text := qryLOCADVREMARK1.AsString;

      //원수출신용장
      edt_OriginType.Text := qryLOCADVDOC_DTL.AsString;
      sEdit22.Text := qryLOCADVDOC_DTLNAME.AsString;
      //신용장번호
      edt_OriginLCNO.Text := qryLOCADVDOC_NO.AsString;
      //결제금액
      edt_OriginAmt.Value    := qryLOCADVDOC_AMT.AsCurrency;
      edt_OriginAmtUnit.Text := qryLOCADVDOC_AMTC.AsString;
      //선적기일
      msk_OriginShipDT.Text := qryLOCADVLOADDATE.AsString;
      //유효기일
      msk_OriginVaildDT.Text := qryLOCADVEXPDATE.AsString;
      //대금결제조건
      edt_AmtType.Text := qryLOCADVPAYMENT.AsString;
      sEdit26.Text := qryLOCADVPAYMENTNAME.AsString;
      //수출공급상대방
      edt_OriginCD.Text     := qryLOCADVIM_NAME.AsString;
      edt_OriginSangho.Text := qryLOCADVIM_NAME1.AsString;
      edt_OriginCeo.Text    := qryLOCADVIM_NAME2.AsString;
      edt_OriginAddr1.Text  := qryLOCADVIM_NAME3.AsString;
      //수출지역
      edt_OriginExportArea.Text := qryLOCADVDEST.AsString;
      //발행기관
      edt_Originbank1.Text := qryLOCADVISBANK1.AsString;
      edt_Originbank2.Text := qryLOCADVISBANK2.AsString;
      //대표수입물품
      memo_ExportGoods.Text := qryLOCADVEXGOOD1.AsString;
      //발신기관 전자서명
      edt_Sign1.Text := qryLOCADVEXNAME1.AsString;
      edt_Sign2.Text := qryLOCADVEXNAME2.AsString;
      edt_Sign3.Text := qryLOCADVEXNAME3.AsString;
    end
    else
    begin
      ClearControl(sPanel7);
      ClearControl(sPanel27);      
    end;
  end;
end;

procedure TUI_LOCADV_NEW_frm.qryLOCADVAfterScroll(DataSet: TDataSet);
begin
//  inherited;
  ReadData;
end;

procedure TUI_LOCADV_NEW_frm.qryLOCADVAfterOpen(DataSet: TDataSet);
begin
//  inherited;
  if DataSet.RecordCount = 0 then
    qryLOCADVAfterScroll(DataSet);
end;

procedure TUI_LOCADV_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6,False);
  ReadOnlyControl(sPanel7,False);
  ReadOnlyControl(sPanel27,false);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOCADV_NEW_frm.btnPrintClick(Sender: TObject);
begin
//  inherited;
  if qryLOCADV.RecordCount = 0 then Exit;

  Preview_frm := TPreview_frm.Create(Self);
  LOCADV_PRINT_frm := TLOCADV_PRINT_frm.Create(Self);
  try
    LOCADV_PRINT_frm.ReadOnlyDocument(qryLOCADV.Fields);
    Preview_frm.Report := LOCADV_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCADV_PRINT_frm);
  end;
end;

procedure TUI_LOCADV_NEW_frm.btnDelClick(Sender: TObject);
begin
//  inherited;
  if qryLOCADV.RecordCount = 0 then Exit;
  if not qryLOCADV.Active then Exit;

  DelDocument;
end;

procedure TUI_LOCADV_NEW_frm.DelDocument;
var
  DocNo : String;
  nCursor : Integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := qryLOCADVMAINT_NO.AsString;
    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+'내국신용장 개설응답서'#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          DMMssql.KISConnect.BeginTrans;        
          nCursor := qryLOCADV.RecNo;
          SQL.Text := 'DELETE FROM LOCADV WHERE MAINT_NO = '+QuotedStr(DocNo);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryLOCADV.Close;
          qryLOCADV.Open;

          IF (qryLOCADV.RecordCount > 0) AND (qryLOCADV.RecordCount >= nCursor) Then
          begin
            qryLOCADV.MoveBy(nCursor-1);
          end
          else
            qryLOCADV.last;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;


procedure TUI_LOCADV_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCADV_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_LOCADV_NEW_frm := nil;
end;

end.
