inherited UI_LOCAPP_NEW_frm: TUI_LOCAPP_NEW_frm
  Left = 481
  Top = 183
  BorderWidth = 4
  Caption = '[LOCAPP] '#45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 731
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 901
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 135
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 44
      Height = 13
      Caption = 'LOCAPP'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 369
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton7: TsSpeedButton
      Left = 654
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton8: TsSpeedButton
      Left = 160
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton2: TsSpeedButton
      Left = 444
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 171
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 237
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Tag = 2
      Left = 303
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 380
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 664
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 455
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = btnTempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 521
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btnTempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Tag = 2
      Left = 587
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 741
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      TabStop = False
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 834
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      TabStop = False
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
    object sCheckBox1: TsCheckBox
      Left = 912
      Top = 13
      Width = 64
      Height = 16
      Caption = #46356#48260#44536
      TabOrder = 11
      Visible = False
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 381
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 379
      Height = 574
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 202
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 102
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #51652#54665
          Width = 41
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 379
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 258
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 77
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = sMaskEdit1Change
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 77
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 170
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 277
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 1
      Top = 57
      Width = 379
      Height = 574
      Align = alClient
      
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 103
        Top = 269
        Width = 172
        Height = 21
        Caption = #45236#44397#49888#50857#51109' '#44060#49444' '#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 103
        Top = 293
        Width = 126
        Height = 12
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 381
    Top = 41
    Width = 733
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 731
      Height = 575
      ActivePage = sTabSheet4
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabIndex = 2
      TabOrder = 1
      OnChange = sPageControl1Change
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 723
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sSpeedButton10: TsSpeedButton
            Left = 360
            Top = 3
            Width = 11
            Height = 532
            ButtonStyle = tbsDivider
          end
          object sLabel3: TsLabel
            Left = 539
            Top = 272
            Width = 76
            Height = 15
            Caption = #52264' '#45236#44397#49888#50857#51109
          end
          object sLabel4: TsLabel
            Left = 635
            Top = 224
            Width = 24
            Height = 15
            AutoSize = False
            Caption = #51060#45236
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
          object edt_wcd: TsEdit
            Tag = 101
            Left = 91
            Top = 4
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 0
            OnDblClick = edt_wcdDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#51032#47280#51064
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_wsangho: TsEdit
            Left = 91
            Top = 28
            Width = 265
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_wceo: TsEdit
            Left = 91
            Top = 52
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_waddr1: TsEdit
            Left = 91
            Top = 76
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_waddr2: TsEdit
            Left = 91
            Top = 100
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_waddr3: TsEdit
            Left = 91
            Top = 124
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_rcd: TsEdit
            Tag = 102
            Left = 91
            Top = 150
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 7
            OnDblClick = edt_wcdDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#54812#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_rsaupno: TsEdit
            Left = 235
            Top = 150
            Width = 121
            Height = 23
            Color = 12582911
            MaxLength = 17
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_rsangho: TsEdit
            Left = 91
            Top = 174
            Width = 265
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_rceo: TsEdit
            Left = 91
            Top = 198
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_remail: TsEdit
            Left = 91
            Top = 294
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'Email'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_raddr1: TsEdit
            Left = 91
            Top = 222
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_raddr2: TsEdit
            Left = 91
            Top = 246
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_demAddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_raddr3: TsEdit
            Left = 91
            Top = 270
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_demAddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_iden1: TsEdit
            Left = 179
            Top = 318
            Width = 177
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#48156#49888#51064' '#49885#48324#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_iden2: TsEdit
            Left = 179
            Top = 342
            Width = 177
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#49464#49885#48324#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_doc1: TsEdit
            Left = 489
            Top = 4
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 26
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#47588#46020#54869#50557#49436#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_doc2: TsEdit
            Left = 489
            Top = 24
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc3: TsEdit
            Left = 489
            Top = 44
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 28
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc4: TsEdit
            Left = 489
            Top = 64
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 29
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc5: TsEdit
            Left = 489
            Top = 84
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 30
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_wsaupno: TsEdit
            Left = 235
            Top = 4
            Width = 121
            Height = 23
            Color = 12582911
            MaxLength = 17
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_cbank: TsEdit
            Tag = 1000
            Left = 91
            Top = 369
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 17
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cbankExit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_cbanknm: TsEdit
            Left = 141
            Top = 369
            Width = 215
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 46
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_cbrunch: TsEdit
            Left = 91
            Top = 393
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_cType: TsEdit
            Tag = 1001
            Left = 91
            Top = 417
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 19
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#50857#51109#51333#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sEdit8: TsEdit
            Left = 141
            Top = 417
            Width = 215
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 47
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_LOCAMT_UNIT: TsEdit
            Tag = 1002
            Left = 91
            Top = 441
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 20
            OnDblClick = edt_cbankDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_perr: TsEdit
            Left = 91
            Top = 465
            Width = 49
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54728#50857#50724#52264'[+]'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object cur_LOCAMT: TsCurrencyEdit
            Left = 141
            Top = 441
            Width = 215
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 21
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_merr: TsEdit
            Left = 163
            Top = 465
            Width = 49
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '[-]'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_cyong: TsEdit
            Tag = 1003
            Left = 91
            Top = 489
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 24
            OnChange = edt_cyongChange
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#44540#44144#48324#50857#46020
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_lcno: TsEdit
            Left = 91
            Top = 513
            Width = 265
            Height = 23
            Color = clBtnFace
            MaxLength = 35
            ReadOnly = True
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'L/C'#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_doc6: TsEdit
            Left = 489
            Top = 104
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 31
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc7: TsEdit
            Left = 489
            Top = 124
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 32
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc8: TsEdit
            Left = 489
            Top = 144
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 33
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object edt_doc9: TsEdit
            Left = 489
            Top = 164
            Width = 215
            Height = 21
            Hint = 'APP_CODE'
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            MaxLength = 70
            ParentCtl3D = False
            TabOrder = 34
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object cur_AttachDoc1: TsCurrencyEdit
            Left = 673
            Top = 416
            Width = 31
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            TabOrder = 41
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object cur_AttachDoc4: TsCurrencyEdit
            Left = 673
            Top = 488
            Width = 31
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            TabOrder = 44
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48376' '#45236#44397#49888#50857#51109' '#49324#48376
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object cur_AttachDoc5: TsCurrencyEdit
            Left = 673
            Top = 512
            Width = 31
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            TabOrder = 45
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#51088#48156#54665' '#47932#54408#47588#46020#54869#50557#49436' '#49324#48376
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object cur_AttachDoc2: TsCurrencyEdit
            Left = 673
            Top = 440
            Width = 31
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 42
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#51088#48156#54665' '#49464#44552#44228#49328#49436' '#49324#48376
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object cur_AttachDoc3: TsCurrencyEdit
            Left = 673
            Top = 464
            Width = 31
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 43
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#47749#49464#44032' '#44592#51116#46108' '#49569#51109
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object sPanel11: TsPanel
            Left = 375
            Top = 394
            Width = 330
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #51452#50836#44396#48708#49436#47448
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 48
          end
          object cur_chasu: TsCurrencyEdit
            Left = 505
            Top = 268
            Width = 25
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 36
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#54924#52264
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
            MinValue = 1
            Value = 1
          end
          object msk_CrtDT: TsMaskEdit
            Left = 505
            Top = 292
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 37
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#49888#52397#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object msk_IndoDt: TsMaskEdit
            Left = 505
            Top = 316
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 38
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object msk_ExpiryDT: TsMaskEdit
            Left = 505
            Top = 340
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 39
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #50976#54952#44592#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object edt_isPart: TsEdit
            Tag = 1008
            Left = 505
            Top = 364
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 40
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48516#54624#54728#50857#50668#48512
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sEdit20: TsEdit
            Left = 539
            Top = 364
            Width = 142
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object comDocPeriod: TsComboBox
            Left = 582
            Top = 220
            Width = 49
            Height = 23
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436' '#48156#44553#51068#47196#48512#53552
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.SkinSection = 'COMBOBOX'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 35
            Text = '5'#51068
            Items.Strings = (
              '5'#51068
              '7'#51068)
          end
          object sPanel2: TsPanel
            Left = 375
            Top = 194
            Width = 330
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49436#47448#51228#49884#44592#44036
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 50
          end
          object edt_cyongnm: TsEdit
            Left = 141
            Top = 489
            Width = 215
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 51
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel8: TsPanel
            Left = 375
            Top = 245
            Width = 330
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#44288#47144
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 52
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = #45824#54364#44277#44553#47932#54408
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 723
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object msk_hscd: TsMaskEdit
            Left = 129
            Top = 12
            Width = 96
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999.99-9999;0'
            MaxLength = 12
            TabOrder = 0
            Text = '1234567890'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object memo_goods: TsMemo
            Left = 129
            Top = 36
            Width = 580
            Height = 85
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#44277#44553#47932#54408#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object memo_etcdoc: TsMemo
            Left = 129
            Top = 122
            Width = 580
            Height = 71
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 2
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44592#53440#44396#48708#49436#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object memo_etcinfo: TsMemo
            Left = 129
            Top = 194
            Width = 580
            Height = 71
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 3
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44592#53440#51221#48372
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_OriginType: TsEdit
            Tag = 1004
            Left = 129
            Top = 298
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 4
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#44540#44144#49436#47448' '#51333#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit22: TsEdit
            Left = 179
            Top = 298
            Width = 134
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginLCNO: TsEdit
            Left = 129
            Top = 322
            Width = 184
            Height = 23
            Color = clWhite
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#50857#51109'['#44228#50557#49436'] '#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginAmtUnit: TsEdit
            Tag = 1005
            Left = 129
            Top = 346
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 6
            OnDblClick = edt_cbankDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44208#51228#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginAmt: TsCurrencyEdit
            Left = 179
            Top = 346
            Width = 134
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object msk_OriginShipDT: TsMaskEdit
            Left = 129
            Top = 370
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 8
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#51201#44592#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object msk_OriginVaildDT: TsMaskEdit
            Left = 129
            Top = 394
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 9
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #50976#54952#44592#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object edt_AmtType: TsEdit
            Tag = 1006
            Left = 129
            Top = 418
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 10
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#44552#44208#51228' '#51312#44148
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit26: TsEdit
            Left = 179
            Top = 418
            Width = 134
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginCD: TsEdit
            Tag = 103
            Left = 443
            Top = 298
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 11
            OnDblClick = edt_wcdDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#52636#44277#44553' '#49345#45824#48169
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_OriginSangho: TsEdit
            Left = 443
            Top = 322
            Width = 265
            Height = 23
            Color = 12582911
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginCeo: TsEdit
            Left = 443
            Top = 346
            Width = 265
            Height = 23
            Color = clWhite
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginAddr1: TsEdit
            Left = 443
            Top = 370
            Width = 265
            Height = 23
            Color = clWhite
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_OriginExportArea: TsEdit
            Tag = 1007
            Left = 443
            Top = 394
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 15
            OnDblClick = edt_cbankDblClick
            OnExit = edt_cTypeChange
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#52636#51648#50669
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit32: TsEdit
            Left = 493
            Top = 394
            Width = 215
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Originbank1: TsEdit
            Left = 443
            Top = 418
            Width = 134
            Height = 23
            Color = clWhite
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48156#54665#51008#54665'['#54869#51064#44592#44288']'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Originbank2: TsEdit
            Left = 578
            Top = 418
            Width = 130
            Height = 23
            Color = clWhite
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_ExportGoods: TsMemo
            Left = 129
            Top = 442
            Width = 580
            Height = 54
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 18
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#49688#52636#54408#47785
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel1: TsPanel
            Left = 129
            Top = 274
            Width = 580
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #50896#49688#52636' '#49888#50857#51109
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 25
          end
          object edt_sign1: TsEdit
            Left = 129
            Top = 514
            Width = 134
            Height = 23
            Color = clWhite
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_sign2: TsEdit
            Left = 265
            Top = 514
            Width = 134
            Height = 23
            Color = clWhite
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_sign3: TsEdit
            Left = 575
            Top = 514
            Width = 134
            Height = 23
            Color = clWhite
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 723
          Height = 507
          Align = alClient
          Color = clGray
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid1DrawColumnCell
          OnDblClick = sDBGrid3DblClick
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #52376#47532
              Width = 60
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'BENEFC1'
              Title.Alignment = taCenter
              Title.Caption = #49688#54812#51088
              Width = 172
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'AP_BANK1'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51008#54665
              Width = 164
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'LOC_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#44552#50529
              Width = 97
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'LOC_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 723
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            OnChange = sMaskEdit1Change
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
            OnClick = sBitBtn5Click
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 731
      Height = 55
      Align = alTop
      
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 336
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO_Header: TsEdit
        Left = 64
        Top = 4
        Width = 143
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object com_func: TsComboBox
        Left = 400
        Top = 4
        Width = 121
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 400
        Top = 28
        Width = 211
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 276
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object edt_MAINT_NO_Serial: TsEdit
        Left = 240
        Top = 4
        Width = 93
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 15
        ParentFont = False
        TabOrder = 5
        SkinData.CustomFont = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object edt_MAINT_NO_Bank: TsEdit
        Left = 208
        Top = 4
        Width = 31
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 2
        ParentFont = False
        TabOrder = 6
        Text = '18'
        OnEnter = edt_MAINT_NO_BankEnter
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object comBank: TsComboBox
        Left = 178
        Top = 28
        Width = 155
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 7
        Visible = False
        OnSelect = comBankSelect
        OnExit = comBankSelect
      end
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180701'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180718'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO,SUBSTRING(MAINT_NO,16,100) as MAINT_NO2, DATEE, ' +
        'USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(LOCAPP.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 8
    Top = 144
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMAINT_NO2: TStringField
      FieldName = 'MAINT_NO2'
      ReadOnly = True
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = CHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListGOODDES1: TStringField
      FieldName = 'GOODDES1'
      Size = 350
    end
    object qryListREMARK1: TStringField
      FieldName = 'REMARK1'
      Size = 350
    end
    object qryListDOC_ETC1: TStringField
      FieldName = 'DOC_ETC1'
      Size = 350
    end
    object qryListEXGOOD1: TStringField
      FieldName = 'EXGOOD1'
      Size = 350
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 144
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE LOCAPP'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 40
    Top = 176
  end
end
