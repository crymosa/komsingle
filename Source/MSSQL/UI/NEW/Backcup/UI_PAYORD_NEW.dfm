inherited UI_PAYORD_NEW_frm: TUI_PAYORD_NEW_frm
  Left = 649
  Top = 154
  BorderWidth = 4
  Caption = '[PAYORD] '#51648#44553#51648#49884#49436
  ClientHeight = 673
  ClientWidth = 1047
  Font.Name = #47569#51008' '#44256#46357
  KeyPreview = False
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPageControl1: TsPageControl [0]
    Left = 0
    Top = 78
    Width = 1047
    Height = 595
    ActivePage = sTabSheet2
    Align = alClient
    TabHeight = 30
    TabIndex = 1
    TabOrder = 0
    OnChange = sPageControl1Change
    OnChanging = sPageControl1Changing
    TabPadding = 15
    object sTabSheet1: TsTabSheet
      Caption = #51089#49457#45236#50669
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 43
        Width = 1039
        Height = 512
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid1DrawColumnCell
        OnTitleClick = sDBGrid1TitleClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #47928#49436
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 161
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 89
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #49324#50857#51088
            Width = 45
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #49569#49888#54788#54889
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BEN_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #49688#51061#51088
            Width = 288
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAY_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAY_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44552#50529
            Width = 114
            Visible = True
          end>
      end
      object sPanel18: TsPanel
        Left = 760
        Top = 395
        Width = 1039
        Height = 512
        SkinData.SkinSection = 'PANEL'
        
        TabOrder = 3
        Visible = False
        object sLabel2: TsLabel
          Left = 419
          Top = 241
          Width = 201
          Height = 30
          Caption = #45936#51060#53552' '#51077#47141#51473#51077#45768#45796'.'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 1039
        Height = 41
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object sSpeedButton9: TsSpeedButton
          Left = 243
          Top = 10
          Width = 23
          Height = 23
          ButtonStyle = tbsDivider
        end
        object mskDate1: TsMaskEdit
          Left = 64
          Top = 10
          Width = 81
          Height = 23
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 0
          Text = '20190806'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object mskDate2: TsMaskEdit
          Left = 161
          Top = 10
          Width = 81
          Height = 23
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 1
          Text = '20190806'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object btnFind: TsBitBtn
          Left = 527
          Top = 10
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = btnFindClick
        end
        object comFind: TsComboBox
          Left = 267
          Top = 10
          Width = 89
          Height = 23
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 3
          Text = #44288#47532#48264#54840
          Items.Strings = (
            #44288#47532#48264#54840)
        end
        object edtFind: TsEdit
          Left = 357
          Top = 10
          Width = 169
          Height = 23
          TabOrder = 4
          OnKeyUp = edtFindKeyUp
        end
      end
      object sPanel2: TsPanel
        Left = 0
        Top = 41
        Width = 1039
        Height = 2
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 1
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #51648#44553#51648#49884#49436' '#51089#49457
      object sPanel4: TsPanel
        Left = 0
        Top = 0
        Width = 225
        Height = 555
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        
        TabOrder = 0
        object sDBGrid2: TsDBGrid
          Left = 1
          Top = 1
          Width = 223
          Height = 553
          Align = alClient
          Color = clWhite
          Ctl3D = True
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 191
              Visible = True
            end>
        end
        object sPanel19: TsPanel
          Left = 57
          Top = 505
          Width = 223
          Height = 553
          SkinData.SkinSection = 'PANEL'
          
          TabOrder = 1
          Visible = False
          object sLabel3: TsLabel
            Left = 11
            Top = 261
            Width = 201
            Height = 30
            Caption = #45936#51060#53552' '#51077#47141#51473#51077#45768#45796'.'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -21
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
      end
      object sPanel5: TsPanel
        Left = 225
        Top = 0
        Width = 814
        Height = 555
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 1
        object sPanel6: TsPanel
          Left = 1
          Top = 1
          Width = 812
          Height = 553
          Align = alClient
          
          TabOrder = 0
          object sImage1: TsImage
            Left = 45
            Top = 236
            Width = 16
            Height = 16
            Cursor = crHelp
            Hint = 
              '<b>['#44592#53440#51221#48372']</b><br>'#13#10'1. '#51648#44553#51648#49884#44288#47144' '#44033#51333' '#50629#47924#52280#51312#49324#54637#51012' '#51077#47141'<br>'#13#10'2. '#49324#51204#49569#44552#48169#49885' '#49688#51077#45824#44552' '#44208 +
              #51228#49884' Offer'#51221#48372#47484' '#51077#47141'<br>'#13#10'- 1'#48264#51704' '#51460' : HS CODE<br>'#13#10'- 2'#48264#51704' '#51460' : '#45824#54364#47932#54408#47749'<br>'#13#10 +
              '- 3'#48264#51704' '#51460' : '#53685#54868#45800#50948'<br>'#13#10'- 4'#48264#51704' '#51460' : Offer '#44552#50529'<br>'#13#10'- 5'#48264#51704' '#51460' : Offer '#48264#54840
            AutoSize = True
            ParentShowHint = False
            Picture.Data = {07544269746D617000000000}
            ShowHint = True
            Transparent = True
            ImageIndex = 9
            Images = DMICON.System16
            SkinData.SkinSection = 'CHECKBOX'
            UseFullSize = True
          end
          object sImage3: TsImage
            Left = 45
            Top = 84
            Width = 16
            Height = 16
            Cursor = crHelp
            Hint = 
              '<font color=red><b>'#51648#44553#52509#50529'</b></font>'#44284' <font color=red><b>'#50808#54868#44228#51340' '#51648#44553#44552#50529 +
              '</b></font> '#48520#51068#52824#49884' '#52264#50529#47564#53372' '#50896#54868#44228#51340#50640#49436'<br>'#13#10#49569#44552#51068' '#54872#50984#47196' '#44228#49328#54616#50668' '#49569#44552#51012' '#54633#45768#45796'.<br>'#13#10'('#48520#51068 +
              #52824#49884' <b>'#50896#54868#50896#44552#44228#51340'</b> '#54596#49688#51077#47141')'
            AutoSize = True
            ParentShowHint = False
            Picture.Data = {07544269746D617000000000}
            ShowHint = True
            Transparent = True
            ImageIndex = 9
            Images = DMICON.System16
            SkinData.SkinSection = 'CHECKBOX'
            UseFullSize = True
          end
          object mskAPP_DATE: TsMaskEdit
            Left = 115
            Top = 10
            Width = 89
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#52397#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object mskEXE_DATE: TsMaskEdit
            Left = 115
            Top = 34
            Width = 89
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51060#52404#55148#47581#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
          object mskIMP_DATE: TsMaskEdit
            Left = 283
            Top = 10
            Width = 89
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 2
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#51077#50696#51221#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object mskHS_CODE: TsMaskEdit
            Left = 283
            Top = 34
            Width = 89
            Height = 23
            AutoSize = False
            EditMask = '9999.99-99;0;_'
            MaxLength = 10
            TabOrder = 3
            Text = '9507102000'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = 'HS'#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtADREFNO: TsEdit
            Left = 115
            Top = 58
            Width = 257
            Height = 23
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBF_CODE: TsEdit
            Tag = 100
            Left = 490
            Top = 10
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 5
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44144#47000#54805#53468#44396#48516
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sEdit3: TsEdit
            Tag = -1
            Left = 524
            Top = 10
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 6
            SkinData.CustomColor = True
          end
          object edtBUS_CD: TsEdit
            Tag = 101
            Left = 490
            Top = 34
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 7
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#44553#51648#49884#49436' '#50857#46020
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sEdit5: TsEdit
            Tag = -1
            Left = 524
            Top = 34
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 8
            SkinData.CustomColor = True
          end
          object edtBUS_CD1: TsEdit
            Tag = 102
            Left = 490
            Top = 58
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 9
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45225#48512#49688#49688#47308' '#50976#54805
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sEdit7: TsEdit
            Tag = -1
            Left = 524
            Top = 58
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 10
            SkinData.CustomColor = True
          end
          object edtSND_CD: TsEdit
            Tag = 103
            Left = 490
            Top = 82
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 2
            TabOrder = 11
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49569#44552#48169#48277
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sEdit9: TsEdit
            Tag = -1
            Left = 524
            Top = 82
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 12
            SkinData.CustomColor = True
          end
          object edtCHARGE_TO: TsEdit
            Tag = 104
            Left = 490
            Top = 106
            Width = 33
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 2
            TabOrder = 13
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#49688#47308#48512#45812#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object sEdit11: TsEdit
            Tag = -1
            Left = 524
            Top = 106
            Width = 271
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 14
            SkinData.CustomColor = True
          end
          object edtPAY_AMTC: TsEdit
            Tag = 98
            Left = 115
            Top = 82
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 3
            TabOrder = 15
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#44553#52509#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtPAY_AMT2C: TsEdit
            Tag = 99
            Left = 115
            Top = 106
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 3
            TabOrder = 16
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50808#54868#44228#51340' '#51648#44553#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object currPAY_AMT: TsCurrencyEdit
            Left = 149
            Top = 82
            Width = 223
            Height = 23
            Color = 12582911
            TabOrder = 17
            SkinData.CustomColor = True
            DecimalPlaces = 3
            DisplayFormat = '#,0.###'
          end
          object currPAY_AMT2: TsCurrencyEdit
            Left = 149
            Top = 106
            Width = 223
            Height = 23
            TabOrder = 18
            DecimalPlaces = 3
            DisplayFormat = '#,0.###'
          end
          object edtPAYDET1: TsEdit
            Left = 115
            Top = 138
            Width = 632
            Height = 23
            Color = 12582911
            MaxLength = 70
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49569#44552#45236#50669
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtPAYDET2: TsEdit
            Left = 115
            Top = 160
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 20
          end
          object edtPAYDET3: TsEdit
            Left = 115
            Top = 182
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 21
          end
          object edtPAYDET4: TsEdit
            Left = 115
            Top = 204
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 22
          end
          object edtREMARK1: TsEdit
            Left = 115
            Top = 234
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 23
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#53440#51221#48372
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtREMARK2: TsEdit
            Left = 115
            Top = 256
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 24
          end
          object edtREMARK3: TsEdit
            Left = 115
            Top = 278
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 25
          end
          object edtREMARK4: TsEdit
            Left = 115
            Top = 300
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 26
          end
          object edtREMARK5: TsEdit
            Left = 115
            Top = 322
            Width = 632
            Height = 23
            MaxLength = 70
            TabOrder = 27
          end
          object edtAPP_NAME1: TsEdit
            Left = 168
            Top = 350
            Width = 212
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 28
            SkinData.CustomColor = True
          end
          object edtAPP_NAME2: TsEdit
            Left = 115
            Top = 374
            Width = 265
            Height = 23
            MaxLength = 35
            TabOrder = 29
          end
          object edtAPP_CODE: TsEdit
            Tag = 101
            Left = 115
            Top = 350
            Width = 52
            Height = 23
            Color = 12775866
            TabOrder = 30
            OnDblClick = edtAPP_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51648#44553#51032#47280#51064
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtAPP_NAME3: TsEdit
            Left = 115
            Top = 398
            Width = 265
            Height = 23
            MaxLength = 35
            TabOrder = 31
          end
          object edtAPP_STR1: TsEdit
            Left = 115
            Top = 422
            Width = 265
            Height = 23
            MaxLength = 35
            TabOrder = 32
          end
          object edtAPP_TELE: TsEdit
            Left = 115
            Top = 446
            Width = 166
            Height = 23
            MaxLength = 35
            TabOrder = 33
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#54868#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBEN_CODE: TsEdit
            Tag = 102
            Left = 456
            Top = 350
            Width = 63
            Height = 23
            Color = 12775866
            TabOrder = 34
            OnDblClick = edtAPP_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#51061#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtSND_CODE: TsEdit
            Tag = 103
            Left = 115
            Top = 475
            Width = 52
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 35
            OnDblClick = edtAPP_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#52397#50629#52404
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtSND_NAME1: TsEdit
            Left = 168
            Top = 475
            Width = 212
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edtSND_NAME2: TsEdit
            Left = 115
            Top = 499
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'sEdit23'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edtSND_NAME3: TsEdit
            Left = 115
            Top = 523
            Width = 265
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 38
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edtBEN_NAME1: TsEdit
            Left = 520
            Top = 350
            Width = 227
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 39
            SkinData.CustomColor = True
          end
          object edtBEN_NAME2: TsEdit
            Left = 456
            Top = 374
            Width = 291
            Height = 23
            MaxLength = 35
            TabOrder = 40
          end
          object edtBEN_NAME3: TsEdit
            Left = 456
            Top = 398
            Width = 291
            Height = 23
            MaxLength = 35
            TabOrder = 41
          end
          object edtBEN_STR1: TsEdit
            Left = 456
            Top = 422
            Width = 291
            Height = 23
            MaxLength = 35
            TabOrder = 42
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBEN_STR2: TsEdit
            Left = 456
            Top = 446
            Width = 291
            Height = 23
            MaxLength = 35
            TabOrder = 43
          end
          object edtBEN_TELE: TsEdit
            Left = 456
            Top = 494
            Width = 166
            Height = 23
            MaxLength = 35
            TabOrder = 44
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#54868#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBEN_STR3: TsEdit
            Left = 456
            Top = 470
            Width = 291
            Height = 23
            MaxLength = 35
            TabOrder = 45
          end
          object edtBEN_NATION: TsEdit
            Tag = 105
            Left = 712
            Top = 494
            Width = 35
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 46
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #50896#44552#44228#51340' '#48143' '#49888#52397#50629#52404
      object sPanel9: TsPanel
        Left = 0
        Top = 0
        Width = 225
        Height = 555
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        
        TabOrder = 0
        object sDBGrid3: TsDBGrid
          Left = 1
          Top = 1
          Width = 223
          Height = 553
          Align = alClient
          Color = clWhite
          Ctl3D = True
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 191
              Visible = True
            end>
        end
        object sPanel20: TsPanel
          Left = 97
          Top = 505
          Width = 223
          Height = 553
          SkinData.SkinSection = 'PANEL'
          
          TabOrder = 1
          Visible = False
          object sLabel4: TsLabel
            Left = 11
            Top = 261
            Width = 201
            Height = 30
            Caption = #45936#51060#53552' '#51077#47141#51473#51077#45768#45796'.'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -21
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
      end
      object sPanel11: TsPanel
        Left = 225
        Top = 0
        Width = 814
        Height = 555
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 1
        object sPanel12: TsPanel
          Left = 1
          Top = 1
          Width = 812
          Height = 553
          Align = alClient
          
          TabOrder = 0
          object sPanel13: TsPanel
            Left = 27
            Top = 18
            Width = 93
            Height = 23
            SkinData.SkinSection = 'SELECTION'
            Caption = '['#51648#44553#51032#47280#51064']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object edtOD_BANK: TsEdit
            Tag = 101
            Left = 87
            Top = 42
            Width = 129
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 1
            OnDblClick = edtOD_BANKDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51008#54665#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtOD_BANK1: TsEdit
            Left = 87
            Top = 66
            Width = 306
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtOD_BANK2: TsEdit
            Left = 87
            Top = 90
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 3
          end
          object edtOD_NAME1: TsEdit
            Left = 87
            Top = 114
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.Caption = '('#48512')'#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtOD_NAME2: TsEdit
            Left = 87
            Top = 138
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 5
          end
          object edtOD_Nation: TsEdit
            Tag = 106
            Left = 360
            Top = 42
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 6
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtOD_CURR: TsEdit
            Tag = 98
            Left = 360
            Top = 162
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 7
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#53685#54868
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtOD_ACCNT2: TsEdit
            Left = 87
            Top = 162
            Width = 210
            Height = 23
            MaxLength = 35
            TabOrder = 8
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#51452
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtOD_ACCNT1: TsEdit
            Left = 87
            Top = 186
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 9
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtEC_BANK: TsEdit
            Tag = 104
            Left = 479
            Top = 42
            Width = 129
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 10
            OnDblClick = edtOD_BANKDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51008#54665#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtEC_BANK1: TsEdit
            Left = 479
            Top = 66
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 11
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtEC_BANK2: TsEdit
            Left = 479
            Top = 90
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 12
          end
          object edtEC_NAME1: TsEdit
            Left = 479
            Top = 114
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 13
            BoundLabel.Active = True
            BoundLabel.Caption = '('#48512')'#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtEC_NAME2: TsEdit
            Left = 479
            Top = 138
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 14
          end
          object edtEC_Nation: TsEdit
            Tag = 108
            Left = 752
            Top = 42
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 15
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtEC_ACCNT2: TsEdit
            Left = 479
            Top = 162
            Width = 210
            Height = 23
            MaxLength = 35
            TabOrder = 16
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#51452
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtEC_ACCNT1: TsEdit
            Left = 479
            Top = 186
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 17
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sPanel15: TsPanel
            Left = 121
            Top = 18
            Width = 272
            Height = 23
            SkinData.SkinSection = 'SELECTION'
            Caption = #50808#54868#50896#44552#44228#51340
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 18
          end
          object sPanel14: TsPanel
            Left = 419
            Top = 18
            Width = 93
            Height = 23
            SkinData.SkinSection = 'SELECTION'
            Caption = '['#51648#44553#51032#47280#51064']'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 19
          end
          object sPanel16: TsPanel
            Left = 513
            Top = 18
            Width = 272
            Height = 23
            SkinData.SkinSection = 'SELECTION'
            Caption = #50896#54868#50896#44552#44228#51340
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object sPanel17: TsPanel
            Left = 698
            Top = 162
            Width = 87
            Height = 23
            Caption = #44228#51340#53685#54868' KRW'
            
            TabOrder = 21
          end
          object edtBN_BANK: TsEdit
            Tag = 102
            Left = 87
            Top = 242
            Width = 129
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 22
            OnDblClick = edtOD_BANKDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#51061#51088' '#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edtBN_BANK1: TsEdit
            Left = 87
            Top = 266
            Width = 306
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBN_BANK2: TsEdit
            Left = 87
            Top = 290
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 24
          end
          object edtBN_NAME1: TsEdit
            Left = 87
            Top = 314
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 25
            BoundLabel.Active = True
            BoundLabel.Caption = '('#48512')'#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBN_NAME2: TsEdit
            Left = 87
            Top = 338
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 26
          end
          object edtBN_Nation: TsEdit
            Tag = 109
            Left = 360
            Top = 242
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 27
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBN_CURR: TsEdit
            Tag = 98
            Left = 360
            Top = 362
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 28
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#53685#54868
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBN_ACCNT2: TsEdit
            Left = 87
            Top = 362
            Width = 210
            Height = 23
            MaxLength = 35
            TabOrder = 29
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#51452
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtBN_ACCNT1: TsEdit
            Left = 87
            Top = 386
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 30
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_BANK: TsEdit
            Tag = 103
            Left = 479
            Top = 242
            Width = 129
            Height = 23
            Color = 12775866
            MaxLength = 11
            TabOrder = 31
            OnDblClick = edtOD_BANKDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51473#44036#44221#50976#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edtIT_BANK1: TsEdit
            Left = 479
            Top = 266
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 32
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_BANK2: TsEdit
            Left = 479
            Top = 290
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 33
          end
          object edtIT_NAME1: TsEdit
            Left = 479
            Top = 314
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 34
            BoundLabel.Active = True
            BoundLabel.Caption = '('#48512')'#51216#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_NAME2: TsEdit
            Left = 479
            Top = 338
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 35
          end
          object edtIT_Nation: TsEdit
            Tag = 111
            Left = 752
            Top = 242
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 36
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_CURR: TsEdit
            Tag = 98
            Left = 752
            Top = 362
            Width = 33
            Height = 23
            Color = 12775866
            MaxLength = 2
            TabOrder = 37
            OnDblClick = edtBF_CODEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#53685#54868
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_ACCNT2: TsEdit
            Left = 479
            Top = 362
            Width = 210
            Height = 23
            MaxLength = 35
            TabOrder = 38
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#51452
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtIT_ACCNT1: TsEdit
            Left = 479
            Top = 386
            Width = 306
            Height = 23
            MaxLength = 35
            TabOrder = 39
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtDOC_GU: TsEdit
            Tag = 113
            Left = 87
            Top = 425
            Width = 31
            Height = 23
            Color = 12775866
            MaxLength = 3
            TabOrder = 40
            OnChange = edtBF_CODEChange
            OnDblClick = edtBF_CODEDblClick
            OnKeyUp = edtBF_CODEKeyUp
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51077#44552#44288#47144#49436#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sEdit26: TsEdit
            Tag = -1
            Left = 119
            Top = 425
            Width = 229
            Height = 23
            TabStop = False
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 41
            SkinData.CustomColor = True
          end
          object mskDOC_DTE1: TsMaskEdit
            Left = 87
            Top = 449
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 42
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49436#47448#48156#54665#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edtDOC_NO1: TsEdit
            Left = 87
            Top = 473
            Width = 265
            Height = 23
            TabOrder = 43
            BoundLabel.Active = True
            BoundLabel.Caption = #49436#47448#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
  end
  object btn_Panel: TsPanel [1]
    Left = 0
    Top = 0
    Width = 1047
    Height = 76
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object sPanel8: TsPanel
      Left = 1
      Top = 1
      Width = 224
      Height = 74
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alLeft
      
      TabOrder = 0
      object sSpeedButton1: TsSpeedButton
        Left = 221
        Top = 1
        Width = 2
        Height = 72
        Align = alRight
        ButtonStyle = tbsDivider
      end
      object sLabel7: TsLabel
        Left = 67
        Top = 13
        Width = 85
        Height = 23
        Caption = #51648#44553#51648#49884#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel1: TsLabel
        Left = 69
        Top = 37
        Width = 81
        Height = 23
        Caption = '(PAYORD)'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sImage2: TsImage
        Left = 5
        Top = 4
        Width = 18
        Height = 32
        Cursor = crHandPoint
        Hint = '<b>'#44396#48260#51204' '#45936#51060#53552'</b>'#47484' '#44032#51256#50741#45768#45796
        ParentShowHint = False
        Picture.Data = {07544269746D617000000000}
        ShowHint = True
        Transparent = True
        OnClick = sImage2Click
        ImageIndex = 22
        Images = DMICON.System18
        Reflected = True
        SkinData.SkinSection = 'CHECKBOX'
      end
      object sCheckBox1: TsCheckBox
        Left = 5
        Top = 54
        Width = 64
        Height = 19
        Caption = #46356#48260#44536
        TabOrder = 0
        Visible = False
      end
    end
    object sPanel10: TsPanel
      Left = 225
      Top = 1
      Width = 821
      Height = 74
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alClient
      
      TabOrder = 1
      object TsPanel
        Left = 1
        Top = 1
        Width = 819
        Height = 42
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sSpeedButton6: TsSpeedButton
          Left = 197
          Top = 3
          Width = 8
          Height = 35
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sSpeedButton7: TsSpeedButton
          Left = 479
          Top = 3
          Width = 8
          Height = 35
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sSpeedButton2: TsSpeedButton
          Left = 272
          Top = 3
          Width = 8
          Height = 35
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sSpeedButton5: TsSpeedButton
          Left = 695
          Top = 3
          Width = 8
          Height = 35
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sSpeedButton3: TsSpeedButton
          Left = 554
          Top = 3
          Width = 8
          Height = 35
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object btnNew: TsButton
          Left = 1
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
          Caption = #49888#44508
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          TabStop = False
          OnClick = btnNewClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 26
          ContentMargin = 8
        end
        object btnEdit: TsButton
          Tag = 1
          Left = 67
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
          Caption = #49688#51221
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = False
          OnClick = btnEditClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 3
          ContentMargin = 8
        end
        object btnDel: TsButton
          Tag = 2
          Left = 133
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
          Caption = #49325#51228
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          TabStop = False
          OnClick = btnDelClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 27
          ContentMargin = 8
        end
        object btnCopy: TsButton
          Left = 206
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #48373#49324
          TabOrder = 3
          TabStop = False
          OnClick = btnCopyClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 28
          ContentMargin = 8
        end
        object btnPrint: TsButton
          Left = 488
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #52636#47141
          TabOrder = 4
          TabStop = False
          OnClick = btnPrintClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 21
          ContentMargin = 8
        end
        object btnTemp: TsButton
          Left = 281
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #51076#49884
          Enabled = False
          TabOrder = 5
          TabStop = False
          OnClick = btnSaveClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 12
          ContentMargin = 8
        end
        object btnSave: TsButton
          Tag = 1
          Left = 347
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #51200#51109
          Enabled = False
          TabOrder = 6
          TabStop = False
          OnClick = btnSaveClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 7
          ContentMargin = 8
        end
        object btnCancel: TsButton
          Tag = 2
          Left = 413
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #52712#49548
          Enabled = False
          TabOrder = 7
          TabStop = False
          OnClick = btnCancelClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 18
          ContentMargin = 8
        end
        object btnExit: TsButton
          Left = 744
          Top = 2
          Width = 72
          Height = 37
          Cursor = crHandPoint
          Caption = #45803#44592
          TabOrder = 8
          TabStop = False
          OnClick = btnExitClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 20
          ContentMargin = 12
        end
        object btnReady: TsButton
          Left = 563
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #51456#48708
          TabOrder = 9
          TabStop = False
          OnClick = btnReadyClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 29
          ContentMargin = 8
        end
        object btnSend: TsButton
          Left = 629
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #51204#49569
          TabOrder = 10
          TabStop = False
          OnClick = btnSendClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 22
          ContentMargin = 8
        end
      end
      object sPanel7: TsPanel
        Left = 1
        Top = 43
        Width = 819
        Height = 30
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alBottom
        
        TabOrder = 1
        object edtMAINT_NO: TsEdit
          Left = 67
          Top = 4
          Width = 230
          Height = 23
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          MaxLength = 35
          ParentFont = False
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44288#47532#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object mskDATEE: TsMaskEdit
          Left = 360
          Top = 4
          Width = 89
          Height = 23
          AutoSize = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 1
          Text = '20180621'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object com_func: TsComboBox
          Left = 589
          Top = 4
          Width = 92
          Height = 23
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#44592#45733
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 1
          TabOrder = 2
          TabStop = False
          Text = '9: Original'
          Items.Strings = (
            '7: Duplicate'
            '9: Original')
        end
        object com_type: TsComboBox
          Left = 737
          Top = 4
          Width = 72
          Height = 23
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#50976#54805
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 3
          TabStop = False
          Text = 'AB: Message acknowledgement'
          Items.Strings = (
            'AB: Message acknowledgement')
        end
        object edtUSER_ID: TsEdit
          Left = 501
          Top = 4
          Width = 29
          Height = 23
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50857#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
      end
    end
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 76
    Width = 1047
    Height = 2
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    
    TabOrder = 2
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 128
  end
  object sAlphaHints1: TsAlphaHints
    MaxWidth = 500
    Templates = <
      item
        ImageDefault.ImageHeight = 0
        ImageDefault.ImageWidth = 0
        ImageDefault.ClientMargins.Top = 15
        ImageDefault.ClientMargins.Left = 19
        ImageDefault.ClientMargins.Bottom = 23
        ImageDefault.ClientMargins.Right = 19
        ImageDefault.BordersWidths.Top = 16
        ImageDefault.BordersWidths.Left = 32
        ImageDefault.BordersWidths.Bottom = 23
        ImageDefault.BordersWidths.Right = 19
        ImageDefault.ShadowSizes.Top = 5
        ImageDefault.ShadowSizes.Left = 11
        ImageDefault.ShadowSizes.Bottom = 15
        ImageDefault.ShadowSizes.Right = 10
        ImageDefault.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000004E24944415478DAEC9CCF6F1B4514C7BF33BB761C27509408215221
          D1564DC3814AFC3AD0046815A549CA812B9C11EAA93DC00DA957FE05842A4520
          71E801218A6814940372915BA948107100B9A2AE4B8A8552B9C5244E5DC7BBAF
          87EEC2EC666776D675FC63BD4F7A5A2759EFEE7CF27DEFCD8EDF9A21A2BD7D85
          3C3F5F3AC6580BC72074C92E1D8B76B9668B27618F7991AC1760E9186BD37B3C
          BF9B5FAD1E34864717ACFB5B2BDFCFED2BFAF6A56E2A2AAA82A282119D0B6EB8
          3EB7BC71F8D4E5C6997C856E9DBADC3833B7BC7158FCBBEF7D3CE0B86D712242
          9077028E1F8AE9FAECC5DB932E9C9B35AAB890662FDE9E049012F65501DB3368
          01BE278A11C1A41C4F9FF8AA786431573FEBC2212272212DE6EA678F5F284C01
          483B9E52006B87738547026568E619E683269E881FBF503898797AFFC2B91733
          1F4D6430FA7C166300F0540AC316C17A69CC7CF54A2D7B6762EEDD7BB7BEFEB4
          BA876AD0F93D02C048411921705417C401F0373EFFF95076FFA193E78E0E7F28
          C271CD85F4F278EA95ABF79FD878E6CD77FE59FFF67CD507BA1D61C543E040B1
          95162016026717A4F9D5EA0923939D6186F902802100F8F8088E06C111ADB48D
          BBE53AB63E29E0D72E54EB0764357FB71B0F7E5C991DFDC1A9A4E4ABAAA4AAB4
          080821EECB35E9C55CFDF45289D60A9BB4D1B0A8497D620D8B9A854DDA582AD1
          DA62AE7E3A20FFF9F39356A5F2C039B95259582AD1DA8E4D16F5A9EDD8642D95
          686D7EB53A2F140B11D2AEC4CD35AA1703C08CE1D199E9714C982CF03D7D6126
          039F1EC78491C9CEE8967EAE91B41800CE53E9A9038A1CD32F76208B31277FCA
          42CAC3C28C30FF194A71E9B4A06FCC19C390A4BAED4AD066844962DCCC1D932D
          0022D5DDBC6CFEC0630E889C71529092B8A682584C01858E9147C841830088E9
          0092818A9B319DC9215795B898276916729FA60CB141511014E5DE1387085112
          0648419E7D788435A0B80252AE6AF008328C6B88694D9674D788E2AC22E894F9
          38C36849593CE1113DC406514DAC1540892580124009A00450022801145B4034
          401C2851509B144409122F03AE21378A390C655707D7A4191B483B362C103574
          C7C435944300A8B955FDEDFA16EEF43BA09BDBB8DBACFDFB07827B8476B1E012
          92E493206D978BD7F215949B04BB5FE1340976BE82F276B9784D1C9B224AC810
          6EF7C5D5354F0F2200F6E7379FFDB5F9D6FB16B2FB9E1D31618CA731D24F6175
          A386CA777FE3C697BFAC7F917B6F6A198F3E9327672BBEF6C00AFA8847EC2EE3
          F8BFC1C804604E9FBF3A3DF2DCE46BA927C7A6FA2715536367F35EB1B67EFDA7
          FC07AFE701341DB79CADEDBC166111005201F2B7E1893DD1618DE122FC4E9767
          5115B630784B0022C2B102F6FF0F90A9287DA2BB2D22EEC982F6B77B0C104900
          05820828F900E4FD4132404170640DDAE810280AB96E1524251C3F20F20D8A7C
          89CB3F703F1C7F2340371514A4223B249C1004CBD494AB3860EA41385120D98A
          30834E8891E4849622F47AA9D1811479342CFF487390B20D0DDE3E3E289272AF
          29C89FB44943399E89639882FC660B10598082D06505918692C25C99A499E264
          614FD9C8E074B28AC96EB25530542A522669A638B92E9C5E58D791851DE9AC58
          E8FCC799E6B693AAD1551385000B5DFF920D44E731A9A8C74017424D179AF418
          618388AA0ED663E1153567B53CA0B8767A50DB07EEFFE605D71EF7CB06DA6DB2
          67F1A33E37FF70005DA9929C8674CBA50000000049454E44AE426082}
        Img_LeftBottom.ImageHeight = 0
        Img_LeftBottom.ImageWidth = 0
        Img_LeftBottom.ClientMargins.Top = 12
        Img_LeftBottom.ClientMargins.Left = 20
        Img_LeftBottom.ClientMargins.Bottom = 25
        Img_LeftBottom.ClientMargins.Right = 20
        Img_LeftBottom.BordersWidths.Top = 16
        Img_LeftBottom.BordersWidths.Left = 35
        Img_LeftBottom.BordersWidths.Bottom = 27
        Img_LeftBottom.BordersWidths.Right = 22
        Img_LeftBottom.ShadowSizes.Top = 6
        Img_LeftBottom.ShadowSizes.Left = 11
        Img_LeftBottom.ShadowSizes.Bottom = 14
        Img_LeftBottom.ShadowSizes.Right = 10
        Img_LeftBottom.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000005274944415478DAEC9CED4F1C451CC7BF33BB0788174B20D648A1A9
          D1549B68D317D668A931C618DAF42FF085894963FA2F984A7C6334F185BE346A
          15E2B36F4C7CD16AB5B5945A88164C2FBED0D29687889C9506140E08E56EF6E7
          0BEF8EB965E761EFB6C82EF74B267BDCCCCECDEF33DF79DA9D8121A41D19A2C0
          EF4F1D600C9BC88E0C1129CA192A1F1671BAB819D5EA380B790F8B81E314263D
          B3FC9E59C6FD9FB0C8F27BB28CB35608B3BC6E0625D9C0505D03F3702D94225F
          D9A1EF179FE60D8D4F32C7DD17A3BEE61689C26FDEEAAD1F4E3F93EE37343366
          6A720C002F06A708D105903A3CB072AC779232A3399AA118D9AAA0C2688E667A
          2729737860E5188094E49723F9CB74EA673E40253829000DDD67E6BB7B272993
          F748504C2DEF91E89DA44CF799F96E000D1228C707A80C891B409583D3D47CB0
          AB0DED2E0BBC2716E632F0AE36B43B4DCD07557EFAEFE1860EB7AC28E6B8FB76
          A77177DC273EF735A39539EE1E4D93AA60E1DA2A282933C3148703A0D1E717B3
          1DC554FD1147F2ACE49327015A377AB90A28D84280481ADAD729895B28882518
          90B11BB11DC5B60A2066032868D298C4D53CB3991C728B75174B3020665A5B72
          CD42931916A4490064F4931B56E35B51411569B8E5638E2403D23EE3E2216498
          D426663559B279608684AB0836C37C926154A52C5EE711BE896D4535B16A00D5
          AD0EA80EA80EA80EA80EA80E28B180680B71A0BA82225210D5915432E01672A3
          84C3D0ED173236B1B53D3444AB790F22EE44F21E0488566D2B9E5B2887005061
          69E1FAC432E6E20E6862197385A585EB3EF5285B0D877E3351396E393B7E6970
          16D902C18B2B9C02C11B9C4576393B7E4902449AAE841C69B92F3F5D93DF1771
          00FCF7AFDE9DCE3D7554A079DBBD77BA70EE72D1E4C4642B4CDE83185BC2ECC9
          1B18FBE4F2D48703CF3DF435FE7B274FC5ABFCB90256D0DB0B797719C7DA0623
          1740EAC03B17BBD2BBF63CEAA6B7DDCFB8930280E30F626F7B13D2BB9AD1AA2A
          E4E432E6B22B587C7D14BF6C7C574CABF9DCDFE34B535787075F7C621040A118
          44F1EA153FCBB00800E900F10040FEE03CF0424F67C7A1E71F7FEDD9DD475590
          4A708E9FFEB56FEAE40723E39FBFF5A742DAB58C98727E9E2F0829F8E18880F4
          6540AA26C60CA1ACBEB9CC859C9B6EC98D389D338FED6C7B5810444B0A77AC83
          F3DD95DEA9537DC3E39FBD39ED93749043B506A10114144F01433E5054063400
          B4704A3697B9B0D0B0AD6D6198EDB829432AC179F9ECB5F7A7BFF9E8A7B18FDF
          98D63823228021347F8B8078F201820E906CAA77F4FE346559CF5E3E9F6B6CDD
          FECF30DF71737F67EB238220B22B58EC39377622FBEDA73F5EEB7B755A51AB41
          8E45A51ACFA01C2F6024AB80E51840D8BC552D673AFB73FF4263EB3DF323EECE
          BFF677B4ECEDE99F782F7BF68BA1AB275EF9C35783C2004854113CC355550114
          4641A697864C05670DD2B9F9A6ED1DF3C3BC63E6C6F92F2F5E79FBA5294B38C2
          A088304AB251242982D26153DFE31FFE8382BCD98A05CDC82D0A16D548E61FD5
          2860DEA32B0B01C19B387587423C85B31E141BB11505D6162AA2953855192ACC
          F565C834A0BC8078198E67014805A91638364B2553A528D762AEE64755873A4A
          F2E4523A594108A120DC8609A309122C7E5FD9E9EAFA2568FA26DD6EAD9A0A58
          A39A223F0EE56F6AA6937BF2FE62D35EA28D8613069A328F30C72B759347DD46
          C86A6AF1B63D8447444732C3363B553A323D670A5BE00D04190A9069D2C80C79
          8692751416D5B170B70ADA2C649B37C6AB9CD90CC623AA99AA27629B194E5805
          053A55CD7F5CD8EC5064FB770037415D7829D241AF0000000049454E44AE4260
          82}
        Img_RightBottom.ImageHeight = 0
        Img_RightBottom.ImageWidth = 0
        Img_RightBottom.ClientMargins.Top = 12
        Img_RightBottom.ClientMargins.Left = 19
        Img_RightBottom.ClientMargins.Bottom = 25
        Img_RightBottom.ClientMargins.Right = 19
        Img_RightBottom.BordersWidths.Top = 17
        Img_RightBottom.BordersWidths.Left = 21
        Img_RightBottom.BordersWidths.Bottom = 26
        Img_RightBottom.BordersWidths.Right = 34
        Img_RightBottom.ShadowSizes.Top = 6
        Img_RightBottom.ShadowSizes.Left = 11
        Img_RightBottom.ShadowSizes.Bottom = 13
        Img_RightBottom.ShadowSizes.Right = 10
        Img_RightBottom.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C5460000052E4944415478DAEC9CCF6F1B4514C7BFB3BBB6D334A86D024244B8
          A53D041528421529521384108714198E3D70A2B417B87224AD4008F55F40A125
          50150107244E1511299428F2064AA4861F124A9A3629895C3552526C5C2BC4DE
          795CBCD16633BF36719BACED278DC6DEB5379ECF7EDF9B1FFB260C06967149FB
          994B4719C336B28C4B548BEBB01A7F2E566602916DE03C8B01488A724E058A19
          1E6786E7B61216191E27D939112816018EAC8E026D2B2091612D8464A29460CD
          8EFD507CD94AA65E64B67310402A2EF186BCCA045FF96F74E895B62B55285250
          41488EC63D58F0F5AB23CB278FEF4BBDD3D381CEFDAD684F58B0E30268AAE8BC
          905D745E6723CB1F7FF752CBA040654C13BB566130001600BB0A300120D9379C
          EF1B9CA58932278F626A654EDEE02C4DF40DE7FB0024AB6D73AA6DB5820C322E
          21E3122C0DA8D562B7B4F6F674A0D361C2EFC4C21C06ABA7039D764B6BAFAC9D
          E1EF588AC01B846531DB39B8BF15ED711FFB74B5E111663BCF8514B32EBCF803
          5FC754410052718A3986835F9130D6C421C7301E59A83FF3DBC40380D6056947
          02050D0428D883AD539265A02056C78094015A06883530206602080217ABC7D9
          3C53F46452404C3292AE57404C33C794BA98AA1BAC2740DA765A9A197D232A68
          D52E1D65CC325CE6A86740CA550D2BA21CEBD5C5B48325D59A505DAE471BAC87
          297BB146366D2FD6B40803C586544D5341355250D39A809A809A809A809A80E2
          0B881A880335155423055113C95A0696E60354E7D0482710CBF842442B650E2F
          EE44CA1C1E885620C90F520192D1240054B957989E296129EE80664A58AADC2B
          4C439C23B48E85057D561601A052EEE6D5EC227215028F2B9C0A816717912BE5
          6E5E95B4758D655C22C7000E00D0E889C3D9C4B77F5F00D26FC62D81AACCE1CD
          94B0945D44EE8B6B7317464F1C7683375F0549F4F4C27F16EF2715F909460E00
          A7E7DC58CFCE745777E2A13D07C058F24137F6BD27F16C670BDA9E50A4E2CC96
          B0945B46F1EC247E0700E25EB952CCDF28CEFE35EEBEDD9B0550065001E0556B
          5E7DCDAB850050C62552010A825A03A9FADA0E400C965A3C4B13E64A1E78E3DD
          C7D2AF9D7AFEECB1A7DE9241F2E1F40F4F7D3A3F74F1E7E9CF3F9A0B80089720
          202E02640B0085DF6B1FF087E355E08FF00D1612D577FF1CFBD7DEB1B3306EA7
          178EEC7BF8698FE0ED4E60C73A3897AF9F9F1FBAF84B008E175048F83D17B81A
          00A0EBD407B0254064D0545048D0C0CD80A2D08FE700E8EE1F6E21D1B62B3F6E
          EFBD73646FC7333EA4209CDCF75F8E5DFFECC3B910102EA8C380A002248B4D08
          015329A6560A92812600B4F4DB6821B9AB233F6EA717BAD3ED873C82975B46F1
          F48F37CEDDBEFCB53B75FEFD7981522A02E57041905E7D2F02C4204F106786EE
          2492B2693186B778EDA742AAFDD17F7EB51E5FE84EEF397466E4D6406EF82B77
          F293D33AB70A1E27997BF9801C83A9865F7B12978A12A74C03B3515C9C1CE8BF
          652553DE19FB38BF7DE59BECE440FF9C42B9B29BA084C4345D3E93F470A2DC21
          769F7A305D8711CE2FD4B9B7A780B30AC9CFB6D729286CFE45FD1FC825CAD9EC
          3335DDCD8244ADA67190547082E6842ECE14AE667217A36E9FDA082428DC9914
          90480368CD144304080220A4386702A72603444D4C924D93B8A6A7D54E334CEF
          F8566D87628A1E5615834803CA78A78FAA21DB65439D0E124C562264E31C6C70
          435D5485D51ACA66D4ABDA341769235DD4063DA84C0F16312E058F93C1584EE9
          4E1B6EF8166F0B8FE2EE3000650CA7A680EE8749A047B911917638EBD6A4B79D
          491A4311CAA6E06C7B05D5C28D37FB1F18FE1F00AEB493A90B16C2F700000000
          49454E44AE426082}
        Img_RightTop.ImageHeight = 0
        Img_RightTop.ImageWidth = 0
        Img_RightTop.ClientMargins.Top = 16
        Img_RightTop.ClientMargins.Left = 19
        Img_RightTop.ClientMargins.Bottom = 22
        Img_RightTop.ClientMargins.Right = 18
        Img_RightTop.BordersWidths.Top = 16
        Img_RightTop.BordersWidths.Left = 19
        Img_RightTop.BordersWidths.Bottom = 25
        Img_RightTop.BordersWidths.Right = 32
        Img_RightTop.ShadowSizes.Top = 6
        Img_RightTop.ShadowSizes.Left = 11
        Img_RightTop.ShadowSizes.Bottom = 15
        Img_RightTop.ShadowSizes.Right = 10
        Img_RightTop.ImgData = {
          89504E470D0A1A0A0000000D49484452000000480000003B0806000000D85BCC
          28000000097048597300000B1300000B1301009A9C18000000206348524D0000
          7A25000080830000F9FF000080E9000075300000EA6000003A980000176F925F
          C546000004D04944415478DAEC9B4D6C1B4514C7FFB35EDB8949054DA44A18AA
          6247A254AD7A88C481BA7C091A87FAC005246E44481C1037E889C209CAA970E5
          8028AAD4031207C4A1D0C8AA0A4A6DA470A02A14DA0A2529A92CD1CA81548E71
          B0771F07BCD6663C333B1B7FDBFBA4D1ACEDD98FF9F9FFDECCEEBE61D0B04C9E
          D00F76FE08637EF7C9E4A9A58B67FD0E682750DA09CB401F9B040E7397747663
          FAF865EBCD7476639AFFAD1DB0DBA6A076FED31ED7D8F87CEC9B3BC9F0C40373
          270F864F9CBA563D5D2DFD7D217B7CCFB2AB2D49B6B5D5C4DA09B20330F8EF9C
          6DF6DCD7B713D1DD7BD2270F864FC4C73051A8A074EA5AF5F4D65F77162EBEF8
          B008125F6B41622D5E70A7413251FDCC173792E30FEE9B7DF750F4EDF818261E
          896172B58CF54205A50F7ED9FAA872B7B070E9A5E40A07450A4A058BF995B5C7
          6FED86D504E8A9733F27620F4DCFBE7778FC2D078ED3D881F4FED57F3EAEFCB9
          B6F0DD2BFB570470C80F24E6038EACF603AD2538A9CF9612BB92879E17C11141
          DABC753DBB383FB3CC81213F6EA7A31477CDE62E969E3522D12759C83C0020DA
          ED91ED9DFD382C83C343FAF006AED6BFDA22ABF69B55295F5E3876FF250F58DB
          20E9A8A5B1FDC2F795D75EDE177D233585782286C9B0811006C0AA36AC9532D6
          734514BEBCB5F5C9B74F8F7DAE82A48A474E310084009800C20022E9EC46FACC
          2A5DA9DA64D1805AD526EBCC2A5D99BD509C0310A9F7CDACF7D57033C8E40999
          3C09278A4C544263B1A3A929C44DD6DF934B95990C466A0AF1D0F8C451593F65
          3369A6703906C06021F34042E1F783628918268D70E4314E314D61C699F89ABA
          0A02101D9498A3B27A1FA22E71D81C24D2B917E3E39181E133C3CBBD7805F10D
          47019033823157BD4D493A0A62430A88E9284877141B560579BA99A141D8E8E2
          DD7C2F15A4F5C04C36936643AA20E6718F29753148761E76053119452F48C308
          081E93649C3FC298A1F998639815A47CAA61F8A43C12CAD18D411821F7822A06
          0DBB525A8266043CBCE702A314777CF73550500B0A0A2C0014000A0005800240
          830B88468803050A6A93822840E2FD5E8C4F5BA311722DDAA98B1188FEADDAB0
          86100EE9BA18290E40B5CD7BBFAF94B13EE8546E9670B756DAF8158A4C33F767
          4342B02965AD5C585ECA1551A811EC41855323D8B9220AE5C2F2123CD2F180FF
          F3844C0D3800408BF333B9F0577F9C05F6BE3A6809548E72724514CEFDB47676
          717E2607758267C3446F2F9C378E4E52919360640230539FFE90BA6FEFA38F87
          77ED4E82B1C8A000AADE5BBFBEB976F3C7DCEB4FE400D4EAC5AAD7767DDBAE17
          0240993C910A901BD43648F5ED900BA2BBF4EA5D9A5B0536572C57E1E15882F6
          0D403217E38B9343E39C4CD4DEEE33402401D40442108F1A662A4E26022482E3
          9504C0BA044676DD2A484A383C20E23AE53E09041DE7E1C8DE75F742412215D9
          2A771200F654100480DC07E927387E20D90A37838E8B91E48496C2F5B45249BA
          08893CDCCD56B4130252A6A1419CE8280ACAFDA6203E68938672B625937B2988
          37DB05910914841E0382249EF829CA20CD14AEA64ABC5629A79BA398EC465405
          A3098C7B29822A4833C5C975E1F4D3DD3AC1C7FA31F8F8C77BB51CAA936A9282
          F15A0EA5D3E16E2EA86B158ED743B11D2DA8F3ABB07E508E9F8146F858A3D50E
          0D65A6473B16F53A07F26CD3C165E11DEBBC8EFD3700D38A07EBEBD501E50000
          000049454E44AE426082}
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Name = 'White Baloon Border'
      end>
    TemplateName = 'White Baloon Border'
    TextAlignment = taLeftJustify
    HTMLMode = True
    SkinSection = 'HINT'
    Left = 24
    Top = 240
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20190101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20190808'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, [USER_ID], DATEE, MESSAGE1, MESSAGE2, APP_DATE,' +
        ' EXE_DATE, IMP_DATE, HS_CODE, ADREFNO, BUS_CD, BUS_CD1, PAYDET1,' +
        ' PAYDET2, PAYDET3, PAYDET4, PAYDET5, SND_CD, CHARGE_TO, PAY_AMT,' +
        ' PAY_AMTC, PAY_AMT2, PAY_AMT2C, OD_BANK, OD_BANK1, OD_BANK2, OD_' +
        'NAME1, OD_NAME2, OD_ACCNT1, OD_ACCNT2, OD_CURR, OD_NATION, BN_BA' +
        'NK, BN_BANK1, BN_BANK2, BN_NAME1, BN_NAME2, BN_ACCNT1, BN_ACCNT2' +
        ', BN_CURR, BN_NATION, IT_ACCNT, IT_PCOD4, IT_BANK, IT_BANK1, IT_' +
        'BANK2, IT_NAME1, IT_NAME2, IT_ACCNT1, IT_ACCNT2, IT_CURR, IT_NAT' +
        'ION, EC_BANK, EC_BANK1, EC_BANK2, EC_NAME1, EC_NAME2, EC_ACCNT1,' +
        ' EC_ACCNT2, EC_NATION,'
      
        'APP_CODE, APP_NAME1, APP_NAME2, APP_NAME3, APP_TELE, APP_STR1, B' +
        'EN_CODE, BEN_NAME1, BEN_NAME2, BEN_NAME3, BEN_TELE, BEN_STR1, BE' +
        'N_STR2, BEN_STR3, BEN_NATION, SND_CODE, SND_NAME1, SND_NAME2, SN' +
        'D_NAME3,'
      
        'REMARK, REMARK1, REMARK2, REMARK3, REMARK4, REMARK5, DOC_GU, DOC' +
        '_NO1, DOC_DTE1, CHK1, CHK2, CHK3, BF_CODE,'
      
        'CD_BFCD.NAME as BF_CODE_NM, CD_BUS_CD.NAME as BUS_CD_NM, CD_BUS_' +
        'CD1.NAME as BUS_CD1_NM, CD_SND.NAME as SND_CD_NM, CD_CHARGETO.NA' +
        'ME as CHARGE_TO_NM, CD_DOC_GU.NAME as DOC_GU_NM'
      
        'FROM PAYORD LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39'PAYORDBFCD'#39') CD_BFCD ON PAYORD.BF_CODE = C' +
        'D_BFCD.CODE'
      
        '            LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39'4487'#39') CD_BUS_CD ON PAYORD.BUS_CD = CD_BUS' +
        '_CD.CODE'
      
        '            LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39'4383'#39') CD_BUS_CD1 ON PAYORD.BUS_CD1 = CD_B' +
        'US_CD1.CODE'
      
        '            LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39#49569#44552#48169#48277#39') CD_SND ON PAYORD.SND_CD = CD_SND.CO' +
        'DE'
      
        '            LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39#49688#49688#47308#48512#45812#39') CD_CHARGETO ON PAYORD.CHARGE_TO = ' +
        'CD_CHARGETO.CODE'
      
        '            LEFT JOIN (SELECT CODE, LTRIM(NAME) as NAME FROM COD' +
        'E2NDD WHERE Prefix = '#39'PAYORD1001'#39') CD_DOC_GU ON PAYORD.DOC_GU = ' +
        'CD_DOC_GU.CODE'
      'WHERE DATEE BETWEEN :FDATE AND :TDATE'
      'AND (MAINT_NO LIKE :MAINT_NO OR 1 = :ALLDATA)')
    Left = 24
    Top = 208
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListEXE_DATE: TStringField
      FieldName = 'EXE_DATE'
      Size = 8
    end
    object qryListIMP_DATE: TStringField
      FieldName = 'IMP_DATE'
      Size = 8
    end
    object qryListHS_CODE: TStringField
      FieldName = 'HS_CODE'
      Size = 10
    end
    object qryListADREFNO: TStringField
      FieldName = 'ADREFNO'
      Size = 35
    end
    object qryListBUS_CD: TStringField
      FieldName = 'BUS_CD'
      Size = 3
    end
    object qryListBUS_CD1: TStringField
      FieldName = 'BUS_CD1'
      Size = 3
    end
    object qryListSND_CD: TStringField
      FieldName = 'SND_CD'
      Size = 3
    end
    object qryListCHARGE_TO: TStringField
      FieldName = 'CHARGE_TO'
      Size = 3
    end
    object qryListPAY_AMT: TBCDField
      FieldName = 'PAY_AMT'
      DisplayFormat = '#,0.###'
      Precision = 16
      Size = 3
    end
    object qryListPAY_AMTC: TStringField
      Alignment = taCenter
      FieldName = 'PAY_AMTC'
      Size = 3
    end
    object qryListOD_BANK: TStringField
      FieldName = 'OD_BANK'
      Size = 6
    end
    object qryListOD_BANK1: TStringField
      FieldName = 'OD_BANK1'
      Size = 35
    end
    object qryListOD_BANK2: TStringField
      FieldName = 'OD_BANK2'
      Size = 35
    end
    object qryListOD_NAME1: TStringField
      FieldName = 'OD_NAME1'
      Size = 35
    end
    object qryListOD_NAME2: TStringField
      FieldName = 'OD_NAME2'
      Size = 35
    end
    object qryListOD_ACCNT1: TStringField
      FieldName = 'OD_ACCNT1'
      Size = 35
    end
    object qryListOD_ACCNT2: TStringField
      FieldName = 'OD_ACCNT2'
      Size = 35
    end
    object qryListOD_CURR: TStringField
      FieldName = 'OD_CURR'
      Size = 3
    end
    object qryListOD_NATION: TStringField
      FieldName = 'OD_NATION'
      Size = 3
    end
    object qryListBN_BANK1: TStringField
      FieldName = 'BN_BANK1'
      Size = 35
    end
    object qryListBN_BANK2: TStringField
      FieldName = 'BN_BANK2'
      Size = 35
    end
    object qryListBN_NAME1: TStringField
      FieldName = 'BN_NAME1'
      Size = 35
    end
    object qryListBN_NAME2: TStringField
      FieldName = 'BN_NAME2'
      Size = 35
    end
    object qryListBN_ACCNT1: TStringField
      FieldName = 'BN_ACCNT1'
      Size = 35
    end
    object qryListBN_ACCNT2: TStringField
      FieldName = 'BN_ACCNT2'
      Size = 35
    end
    object qryListBN_CURR: TStringField
      FieldName = 'BN_CURR'
      Size = 3
    end
    object qryListBN_NATION: TStringField
      FieldName = 'BN_NATION'
      Size = 3
    end
    object qryListIT_ACCNT: TStringField
      FieldName = 'IT_ACCNT'
      Size = 17
    end
    object qryListIT_PCOD4: TStringField
      FieldName = 'IT_PCOD4'
      Size = 3
    end
    object qryListIT_BANK: TStringField
      FieldName = 'IT_BANK'
      Size = 6
    end
    object qryListIT_BANK1: TStringField
      FieldName = 'IT_BANK1'
      Size = 35
    end
    object qryListIT_BANK2: TStringField
      FieldName = 'IT_BANK2'
      Size = 35
    end
    object qryListIT_NAME1: TStringField
      FieldName = 'IT_NAME1'
      Size = 35
    end
    object qryListIT_NAME2: TStringField
      FieldName = 'IT_NAME2'
      Size = 35
    end
    object qryListIT_ACCNT1: TStringField
      FieldName = 'IT_ACCNT1'
      Size = 35
    end
    object qryListIT_ACCNT2: TStringField
      FieldName = 'IT_ACCNT2'
      Size = 35
    end
    object qryListIT_CURR: TStringField
      FieldName = 'IT_CURR'
      Size = 3
    end
    object qryListIT_NATION: TStringField
      FieldName = 'IT_NATION'
      Size = 3
    end
    object qryListEC_BANK: TStringField
      FieldName = 'EC_BANK'
      Size = 11
    end
    object qryListEC_BANK1: TStringField
      FieldName = 'EC_BANK1'
      Size = 35
    end
    object qryListEC_BANK2: TStringField
      FieldName = 'EC_BANK2'
      Size = 35
    end
    object qryListEC_NAME1: TStringField
      FieldName = 'EC_NAME1'
      Size = 35
    end
    object qryListEC_NAME2: TStringField
      FieldName = 'EC_NAME2'
      Size = 35
    end
    object qryListEC_ACCNT1: TStringField
      FieldName = 'EC_ACCNT1'
      Size = 17
    end
    object qryListEC_ACCNT2: TStringField
      FieldName = 'EC_ACCNT2'
      Size = 35
    end
    object qryListEC_NATION: TStringField
      FieldName = 'EC_NATION'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListAPP_TELE: TStringField
      FieldName = 'APP_TELE'
      Size = 25
    end
    object qryListAPP_STR1: TStringField
      FieldName = 'APP_STR1'
      Size = 35
    end
    object qryListBEN_CODE: TStringField
      FieldName = 'BEN_CODE'
      Size = 10
    end
    object qryListBEN_NAME1: TStringField
      FieldName = 'BEN_NAME1'
      Size = 35
    end
    object qryListBEN_NAME2: TStringField
      FieldName = 'BEN_NAME2'
      Size = 35
    end
    object qryListBEN_NAME3: TStringField
      FieldName = 'BEN_NAME3'
      Size = 35
    end
    object qryListBEN_TELE: TStringField
      FieldName = 'BEN_TELE'
      Size = 25
    end
    object qryListBEN_STR1: TStringField
      FieldName = 'BEN_STR1'
      Size = 35
    end
    object qryListBEN_STR2: TStringField
      FieldName = 'BEN_STR2'
      Size = 35
    end
    object qryListBEN_STR3: TStringField
      FieldName = 'BEN_STR3'
      Size = 35
    end
    object qryListBEN_NATION: TStringField
      FieldName = 'BEN_NATION'
      Size = 3
    end
    object qryListSND_CODE: TStringField
      FieldName = 'SND_CODE'
      Size = 10
    end
    object qryListSND_NAME1: TStringField
      FieldName = 'SND_NAME1'
      Size = 35
    end
    object qryListSND_NAME2: TStringField
      FieldName = 'SND_NAME2'
      Size = 35
    end
    object qryListSND_NAME3: TStringField
      FieldName = 'SND_NAME3'
      Size = 10
    end
    object qryListREMARK: TBooleanField
      FieldName = 'REMARK'
    end
    object qryListREMARK1: TStringField
      FieldName = 'REMARK1'
      Size = 70
    end
    object qryListREMARK2: TStringField
      FieldName = 'REMARK2'
      Size = 70
    end
    object qryListREMARK3: TStringField
      FieldName = 'REMARK3'
      Size = 70
    end
    object qryListREMARK4: TStringField
      FieldName = 'REMARK4'
      Size = 70
    end
    object qryListREMARK5: TStringField
      FieldName = 'REMARK5'
      Size = 70
    end
    object qryListDOC_GU: TStringField
      FieldName = 'DOC_GU'
      Size = 3
    end
    object qryListDOC_NO1: TStringField
      FieldName = 'DOC_NO1'
      Size = 35
    end
    object qryListDOC_DTE1: TStringField
      FieldName = 'DOC_DTE1'
      Size = 8
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = CHK3GetText
      Size = 10
    end
    object qryListPAY_AMT2: TBCDField
      FieldName = 'PAY_AMT2'
      Precision = 16
      Size = 3
    end
    object qryListPAY_AMT2C: TStringField
      FieldName = 'PAY_AMT2C'
      Size = 3
    end
    object qryListBF_CODE: TStringField
      FieldName = 'BF_CODE'
      Size = 3
    end
    object qryListPAYDET1: TStringField
      DisplayWidth = 70
      FieldName = 'PAYDET1'
      Size = 70
    end
    object qryListPAYDET2: TStringField
      DisplayWidth = 70
      FieldName = 'PAYDET2'
      Size = 70
    end
    object qryListPAYDET3: TStringField
      DisplayWidth = 70
      FieldName = 'PAYDET3'
      Size = 70
    end
    object qryListPAYDET4: TStringField
      DisplayWidth = 70
      FieldName = 'PAYDET4'
      Size = 70
    end
    object qryListPAYDET5: TStringField
      DisplayWidth = 70
      FieldName = 'PAYDET5'
      Size = 70
    end
    object qryListBN_BANK: TStringField
      FieldName = 'BN_BANK'
      Size = 11
    end
    object qryListBF_CODE_NM: TStringField
      FieldName = 'BF_CODE_NM'
      ReadOnly = True
      Size = 100
    end
    object qryListBUS_CD_NM: TStringField
      FieldName = 'BUS_CD_NM'
      ReadOnly = True
      Size = 100
    end
    object qryListBUS_CD1_NM: TStringField
      FieldName = 'BUS_CD1_NM'
      ReadOnly = True
      Size = 100
    end
    object qryListSND_CD_NM: TStringField
      FieldName = 'SND_CD_NM'
      ReadOnly = True
      Size = 100
    end
    object qryListCHARGE_TO_NM: TStringField
      FieldName = 'CHARGE_TO_NM'
      ReadOnly = True
      Size = 100
    end
    object qryListDOC_GU_NM: TStringField
      FieldName = 'DOC_GU_NM'
      ReadOnly = True
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 208
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE PAYORD'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 24
    Top = 176
  end
end
