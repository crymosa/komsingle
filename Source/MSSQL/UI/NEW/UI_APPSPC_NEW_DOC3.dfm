inherited UI_APPSPC_NEW_DOC3_frm: TUI_APPSPC_NEW_DOC3_frm
  Left = 939
  Top = 276
  Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#51032#47280#49436' - '#49345#50629#49569#51109
  ClientHeight = 563
  ClientWidth = 784
  FormStyle = fsNormal
  OldCreateOrder = True
  Visible = False
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel17: TsPanel [0]
    Left = 0
    Top = 0
    Width = 784
    Height = 563
    Align = alClient
    
    TabOrder = 0
    object sPanel15: TsPanel
      Left = 1
      Top = 376
      Width = 782
      Height = 186
      Align = alClient
      
      TabOrder = 1
      object sPanel19: TsPanel
        Left = 1
        Top = 1
        Width = 780
        Height = 23
        SkinData.ColorTone = clGradientInactiveCaption
        Align = alTop
        Caption = #49345#50629#49569#51109' '#54408#47785#45236#50669
        Color = clBtnText
        
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object sPanel18: TsPanel
        Left = 1
        Top = 24
        Width = 344
        Height = 161
        Align = alLeft
        
        TabOrder = 1
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 32
          Width = 342
          Height = 128
          Align = alClient
          Color = clWhite
          DataSource = dsListD2
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'LINE_NO'
              Title.Alignment = taCenter
              Title.Caption = #48264#54840
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VB_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529'('#50808#54868')'
              Width = 92
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'VB_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 34
              Visible = True
            end>
        end
        object sPanel2: TsPanel
          Left = 1
          Top = 1
          Width = 342
          Height = 31
          Align = alTop
          
          TabOrder = 1
          object btnEdtD2: TsBitBtn
            Left = 136
            Top = -1
            Width = 69
            Height = 32
            Caption = #49688#51221
            TabOrder = 0
            OnClick = btnEdtD2Click
            ImageIndex = 3
            Images = DMICON.System18
          end
          object btnDelD2: TsBitBtn
            Left = 272
            Top = -1
            Width = 69
            Height = 32
            Caption = #49325#51228
            TabOrder = 1
            OnClick = btnDelD2Click
            ImageIndex = 1
            Images = DMICON.System18
          end
          object btnSaveD2: TsBitBtn
            Tag = 102
            Left = 204
            Top = -1
            Width = 69
            Height = 32
            Caption = #51200#51109
            Enabled = False
            TabOrder = 2
            OnClick = btnSaveD2Click
            ImageIndex = 11
            Images = DMICON.System18
          end
          object btnCancelD2: TsBitBtn
            Left = 68
            Top = -1
            Width = 69
            Height = 32
            Caption = #52712#49548
            Enabled = False
            TabOrder = 3
            OnClick = btnCancelD2Click
            ImageIndex = 18
            Images = DMICON.System18
          end
          object btnAddD2: TsBitBtn
            Left = 0
            Top = -1
            Width = 69
            Height = 32
            Caption = #51077#47141
            TabOrder = 4
            OnClick = btnAddD2Click
            ImageIndex = 2
            Images = DMICON.System18
          end
        end
      end
      object sPanel16: TsPanel
        Left = 345
        Top = 24
        Width = 436
        Height = 161
        Align = alClient
        
        TabOrder = 2
        object btnAdd_SIZE: TsSpeedButton
          Left = 391
          Top = 51
          Width = 22
          Height = 23
          Caption = '+'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          OnClick = btnAdd_SIZEClick
        end
        object btnAdd_IMD: TsSpeedButton
          Left = 391
          Top = 27
          Width = 22
          Height = 23
          Caption = '+'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          OnClick = btnAdd_IMDClick
        end
        object edt_LINE_NO: TsEdit
          Left = 91
          Top = 3
          Width = 42
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51068#47144#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SIZE: TsEdit
          Left = 91
          Top = 51
          Width = 299
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnExit = edt_SIZEExit
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44508#44201
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_IMD_CODE: TsEdit
          Left = 91
          Top = 27
          Width = 299
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          OnExit = edt_IMD_CODEExit
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54408#47785
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_QTYC: TsEdit
          Tag = 105
          Left = 91
          Top = 75
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_PRICEC: TsEdit
          Tag = 107
          Left = 281
          Top = 99
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#51456#49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_VB_AMTC: TsEdit
          Tag = 108
          Left = 91
          Top = 123
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#44032#50529#50808#54868
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SUP_TOTAMTC: TsEdit
          Tag = 109
          Left = 281
          Top = 123
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          Visible = False
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49548#44228
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_PRICE: TsCurrencyEdit
          Left = 91
          Top = 99
          Width = 132
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45800#44032
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_QTY: TsCurrencyEdit
          Left = 134
          Top = 75
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_VB_AMT: TsCurrencyEdit
          Left = 134
          Top = 123
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_PRICE_G: TsCurrencyEdit
          Left = 324
          Top = 99
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_SUP_TOTAMT: TsCurrencyEdit
          Left = 324
          Top = 123
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          Visible = False
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_TOTQTYC: TsEdit
          Tag = 106
          Left = 281
          Top = 75
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          Visible = False
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49548#44228
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_TOTQTY: TsCurrencyEdit
          Left = 324
          Top = 75
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          Visible = False
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
      end
    end
    object sPanel14: TsPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 375
      Align = alTop
      
      TabOrder = 0
      object sPanel3: TsPanel
        Left = 1
        Top = 42
        Width = 780
        Height = 164
        Align = alTop
        
        TabOrder = 0
        object sPanel5: TsPanel
          Left = 1
          Top = 1
          Width = 398
          Height = 162
          Align = alLeft
          
          TabOrder = 0
          object sPanel9: TsPanel
            Left = 1
            Top = 1
            Width = 396
            Height = 23
            SkinData.ColorTone = clGradientInactiveCaption
            Align = alTop
            Caption = #49345#50629#49569#51109#51613#47749#49436
            Color = clBtnText
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object edt_DOC_NO: TsEdit
            Left = 89
            Top = 33
            Width = 276
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47928#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object msk_ISS_DATE: TsMaskEdit
            Left = 89
            Top = 57
            Width = 74
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48156#44553#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_TOTCNTC: TsEdit
            Tag = 101
            Left = 89
            Top = 81
            Width = 42
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            OnDblClick = CODEDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ISS_TOTAMTC: TsEdit
            Tag = 102
            Left = 89
            Top = 105
            Width = 42
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            OnDblClick = CODEDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_TOTCNT: TsCurrencyEdit
            Left = 132
            Top = 81
            Width = 233
            Height = 23
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
          end
          object edt_ISS_TOTAMT: TsCurrencyEdit
            Left = 132
            Top = 105
            Width = 233
            Height = 23
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
          end
        end
        object sPanel6: TsPanel
          Left = 399
          Top = 1
          Width = 380
          Height = 162
          Align = alClient
          
          TabOrder = 1
          object sPanel10: TsPanel
            Left = 1
            Top = 1
            Width = 378
            Height = 23
            SkinData.ColorTone = clGradientInactiveCaption
            Align = alTop
            Caption = #48708#44256
            Color = clBtnText
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object edt_REMARK1_1: TsEdit
            Left = 71
            Top = 33
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54637#52264
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_REMARK1_2: TsEdit
            Left = 71
            Top = 57
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_REMARK1_3: TsEdit
            Left = 71
            Top = 81
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#51201#51648
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_REMARK1_4: TsEdit
            Left = 71
            Top = 105
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46020#52265#51648
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_REMARK1_5: TsEdit
            Left = 71
            Top = 129
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51064#46020#51312#44148
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
      object sPanel4: TsPanel
        Left = 1
        Top = 206
        Width = 780
        Height = 168
        Align = alClient
        
        TabOrder = 1
        object sPanel7: TsPanel
          Left = 1
          Top = 1
          Width = 398
          Height = 166
          Align = alLeft
          
          TabOrder = 0
          object sPanel11: TsPanel
            Left = 1
            Top = 1
            Width = 396
            Height = 23
            SkinData.ColorTone = clGradientInactiveCaption
            Align = alTop
            Caption = #47932#54408#44277#44553#51088'('#49688#52636#51088')'
            Color = clBtnText
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object edt_SE_CODE: TsEdit
            Tag = 103
            Left = 89
            Top = 33
            Width = 42
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            OnDblClick = CODEDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_SE_SNAME: TsEdit
            Left = 132
            Top = 33
            Width = 232
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_SE_NAME: TsEdit
            Left = 89
            Top = 57
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_SE_ADDR1: TsEdit
            Left = 89
            Top = 81
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_SE_ADDR2: TsEdit
            Left = 89
            Top = 103
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_SE_ADDR3: TsEdit
            Left = 89
            Top = 125
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
        object sPanel8: TsPanel
          Left = 399
          Top = 1
          Width = 380
          Height = 166
          Align = alClient
          
          TabOrder = 1
          object sPanel12: TsPanel
            Left = 1
            Top = 1
            Width = 378
            Height = 23
            SkinData.ColorTone = clGradientInactiveCaption
            Align = alTop
            Caption = #44277#44553#48155#45716#51088'('#49688#51077#51088')'
            Color = clBtnText
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object edt_BY_CODE: TsEdit
            Tag = 104
            Left = 71
            Top = 33
            Width = 42
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 12775866
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            OnDblClick = CODEDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49345#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BY_SNAME: TsEdit
            Left = 114
            Top = 33
            Width = 232
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_BY_NAME: TsEdit
            Left = 71
            Top = 57
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BY_ADDR1: TsEdit
            Left = 71
            Top = 81
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51452#49548
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BY_ADDR2: TsEdit
            Left = 71
            Top = 103
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BY_ADDR3: TsEdit
            Left = 71
            Top = 125
            Width = 275
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
      end
      object sPanel1: TsPanel
        Left = 1
        Top = 1
        Width = 780
        Height = 41
        Align = alTop
        
        TabOrder = 2
        object btnSave: TsButton
          Tag = 101
          Left = 645
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #51200#51109
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TabStop = False
          OnClick = btnSaveClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 7
          ContentMargin = 8
        end
        object btnCancel: TsButton
          Tag = 2
          Left = 715
          Top = 2
          Width = 65
          Height = 37
          Cursor = crHandPoint
          Caption = #52712#49548
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ModalResult = 2
          ParentFont = False
          TabOrder = 1
          TabStop = False
          OnClick = btnCancelClick
          SkinData.SkinSection = 'TRANSPARENT'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 18
          ContentMargin = 8
        end
      end
    end
  end
  object sPanel13: TsPanel [1]
    Left = 406
    Top = 205
    Width = 330
    Height = 269
    
    TabOrder = 1
    Visible = False
    object sLabel2: TsLabel
      Left = 153
      Top = 5
      Width = 24
      Height = 15
      Caption = #44508#44201
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object edt_SIZE1: TsEdit
      Left = 15
      Top = 27
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = edt_SIZE1Exit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE2: TsEdit
      Left = 15
      Top = 49
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE3: TsEdit
      Left = 15
      Top = 71
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE4: TsEdit
      Left = 15
      Top = 93
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sButton2: TsButton
      Tag = 2
      Left = 265
      Top = 5
      Width = 65
      Height = 17
      Cursor = crHandPoint
      Caption = #45803#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object edt_SIZE5: TsEdit
      Left = 15
      Top = 115
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE6: TsEdit
      Left = 15
      Top = 137
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE7: TsEdit
      Left = 15
      Top = 159
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE8: TsEdit
      Left = 15
      Top = 181
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE9: TsEdit
      Left = 15
      Top = 203
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE10: TsEdit
      Left = 15
      Top = 225
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object sPanel20: TsPanel [2]
    Left = 406
    Top = 317
    Width = 330
    Height = 133
    
    TabOrder = 2
    Visible = False
    object sLabel1: TsLabel
      Left = 153
      Top = 5
      Width = 24
      Height = 15
      Caption = #54408#47785
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object edt_IMD_CODE1: TsEdit
      Left = 15
      Top = 27
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = edt_IMD_CODE1Exit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE2: TsEdit
      Left = 15
      Top = 49
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE3: TsEdit
      Left = 15
      Top = 71
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE4: TsEdit
      Left = 15
      Top = 93
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Tag = 2
      Left = 265
      Top = 5
      Width = 65
      Height = 17
      Cursor = crHandPoint
      Caption = #45803#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
  end
  object qryListD2: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListD2AfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM APPSPC_D2 WHERE KEYY = :KEYY and DOC_GUBUN = :DOC_' +
        'GUBUN and SEQ = :SEQ'
      '')
    Left = 128
    Top = 504
    object qryListD2KEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryListD2DOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryListD2SEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryListD2LINE_NO: TStringField
      FieldName = 'LINE_NO'
      Size = 8
    end
    object qryListD2HS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 35
    end
    object qryListD2DE_DATE: TStringField
      FieldName = 'DE_DATE'
      Size = 35
    end
    object qryListD2IMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryListD2IMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryListD2IMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryListD2IMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryListD2SIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryListD2SIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryListD2SIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryListD2SIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryListD2SIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryListD2SIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryListD2SIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryListD2SIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryListD2SIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryListD2SIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryListD2QTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryListD2QTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryListD2TOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryListD2TOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryListD2PRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '###,###,##0.00;0;'
      Precision = 18
    end
    object qryListD2PRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryListD2PRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryListD2CUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      Precision = 18
    end
    object qryListD2SUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListD2SUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryListD2VB_TAX: TBCDField
      FieldName = 'VB_TAX'
      Precision = 18
    end
    object qryListD2VB_TAXC: TStringField
      FieldName = 'VB_TAXC'
      Size = 3
    end
    object qryListD2VB_AMT: TBCDField
      FieldName = 'VB_AMT'
      DisplayFormat = '###,###,##0.00;0;'
      Precision = 18
    end
    object qryListD2VB_AMTC: TStringField
      FieldName = 'VB_AMTC'
      Size = 3
    end
    object qryListD2SUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
    object qryListD2SUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryListD2VB_TOTTAX: TBCDField
      FieldName = 'VB_TOTTAX'
      Precision = 18
    end
    object qryListD2VB_TOTTAXC: TStringField
      FieldName = 'VB_TOTTAXC'
      Size = 3
    end
    object qryListD2VB_TOTAMT: TBCDField
      FieldName = 'VB_TOTAMT'
      Precision = 18
    end
    object qryListD2VB_TOTAMTC: TStringField
      FieldName = 'VB_TOTAMTC'
      Size = 3
    end
    object qryListD2BIGO1: TStringField
      FieldName = 'BIGO1'
      Size = 70
    end
    object qryListD2BIGO2: TStringField
      FieldName = 'BIGO2'
      Size = 70
    end
    object qryListD2BIGO3: TStringField
      FieldName = 'BIGO3'
      Size = 70
    end
    object qryListD2BIGO4: TStringField
      FieldName = 'BIGO4'
      Size = 70
    end
    object qryListD2BIGO5: TStringField
      FieldName = 'BIGO5'
      Size = 70
    end
  end
  object dsListD2: TDataSource
    DataSet = qryListD2
    Left = 168
    Top = 504
  end
  object qryD1Cancel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'KEYY2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'DELETE FROM APPSPC_D2 WHERE KEYY = :KEYY and SEQ = :SEQ and DOC_' +
        'GUBUN = :DOC_GUBUN '
      'UPDATE APPSPC_D2 SET KEYY = :KEYY2 WHERE KEYY = '#39'0'#39)
    Left = 752
    Top = 48
  end
end
