inherited UI_APPPCR_NEW_frm: TUI_APPPCR_NEW_frm
  Left = 424
  BorderWidth = 4
  Caption = '[APPPCR]'#50808#54868#54925#46301#50857#50896#47308' '#44396#47588#54869#51064#49888#52397#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    Align = alTop
    DoubleBuffered = False
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 873
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton5: TsSpeedButton
      Left = 1037
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 31
      Top = 13
      Width = 244
      Height = 23
      Caption = #50808#54868#54925#46301#50857#50896#47308' '#44396#47588#54869#51064#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 516
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton7: TsSpeedButton
      Left = 798
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 299
      Top = 3
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sSpeedButton2: TsSpeedButton
      Left = 591
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel5: TsLabel
      Left = 119
      Top = 37
      Width = 73
      Height = 21
      Caption = '(APPPCR)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton3: TsSpeedButton
      Left = 1030
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object Btn_New: TsButton
      Left = 318
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = Btn_NewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 25
      ContentMargin = 8
    end
    object Btn_Modify: TsButton
      Tag = 1
      Left = 384
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = Btn_ModifyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 3
      ContentMargin = 8
    end
    object Btn_Del: TsButton
      Tag = 2
      Left = 450
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = Btn_DelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 525
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 27
      ContentMargin = 8
    end
    object Btn_Print: TsButton
      Left = 807
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      TabStop = False
      OnClick = Btn_PrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object Btn_Temp: TsButton
      Left = 600
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = Btn_TempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 11
      ContentMargin = 8
    end
    object Btn_Save: TsButton
      Tag = 1
      Left = 666
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = Btn_TempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 0
      ContentMargin = 8
    end
    object Btn_Cancel: TsButton
      Tag = 2
      Left = 732
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = Btn_CancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object Btn_Ready: TsButton
      Left = 882
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51456#48708
      TabOrder = 9
      TabStop = False
      OnClick = Btn_ReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 28
      ContentMargin = 8
    end
    object btn_Send: TsButton
      Left = 948
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      TabStop = False
      OnClick = btn_SendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 21
      ContentMargin = 8
    end
    object sCheckBox1: TsCheckBox
      Left = 232
      Top = 53
      Width = 62
      Height = 16
      Caption = #46356#48260#44536
      TabOrder = 11
      Visible = False
    end
    object sPanel6: TsPanel
      Left = 306
      Top = 40
      Width = 805
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      DoubleBuffered = False
      TabOrder = 12
      object edt_MAINT_NO_Header: TsEdit
        Left = 72
        Top = 4
        Width = 217
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 355
        Top = 4
        Width = 89
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        SkinData.CustomColor = True
        CheckOnExit = True
        EditMask = '9999-99-99;0'
      end
      object com_func: TsComboBox
        Left = 656
        Top = 4
        Width = 41
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 2
        TabStop = False
        Text = '1:'
        Items.Strings = (
          '1:'
          '2:'
          '4:'
          '6:'
          '7:'
          '9:')
      end
      object com_type: TsComboBox
        Left = 756
        Top = 4
        Width = 44
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 3
        TabStop = False
        Text = 'AB:'
        Items.Strings = (
          'AB:'
          'AP:'
          'NA:'
          'RE:')
      end
      object edt_userno: TsEdit
        Left = 496
        Top = 4
        Width = 29
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
      object comBank: TsComboBox
        Left = 954
        Top = 28
        Width = 155
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 5
        Visible = False
      end
    end
    object QRShape1: TQRShape
      Left = 306
      Top = 40
      Width = 805
      Height = 1
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        2.645833333333330000
        809.625000000000000000
        105.833333333333000000
        2129.895833333330000000)
      Brush.Color = clBtnFace
      Pen.Color = clBtnFace
      Shape = qrsRectangle
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 353
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    DoubleBuffered = False
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 351
      Height = 543
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnTitleClick = sDBGrid1TitleClick
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 188
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 89
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #51652#54665
          Width = 40
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 351
      Height = 56
      Align = alTop
      DoubleBuffered = False
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 258
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 77
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = edt_SearchNoChange
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Left = 77
        Top = 4
        Width = 74
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        TabOrder = 1
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = 1
        Left = 170
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        DoubleBuffered = False
        MaxLength = 10
        TabOrder = 2
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 273
        Top = 4
        Width = 66
        Height = 45
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 1
      Top = 57
      Width = 351
      Height = 543
      Align = alClient
      DoubleBuffered = False
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 61
        Top = 269
        Width = 230
        Height = 21
        Caption = #50808#54868#54925#46301#50857#50896#47308' '#44396#47588#54869#51064#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 112
        Top = 293
        Width = 126
        Height = 12
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 353
    Top = 72
    Width = 761
    Height = 601
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Images = DMICON.System18
    ParentFont = False
    TabHeight = 26
    TabOrder = 2
    OnChange = sPageControl1Change
    TabPadding = 7
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      ImageIndex = 2
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sSpeedButton10: TsSpeedButton
          Left = 389
          Top = 5
          Width = 11
          Height = 555
          ButtonStyle = tbsDivider
        end
        object sLabel6: TsLabel
          Left = 168
          Top = 360
          Width = 212
          Height = 15
          Alignment = taRightJustify
          AutoSize = False
          Caption = #44396#47588#54869#51064#49436#48264#54840#45716' '#48320#44221#49888#52397#51068#46412#47564' '#51077#47141
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 3092479
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          UseSkinColor = False
        end
        object sLabel3: TsLabel
          Left = 455
          Top = 56
          Width = 280
          Height = 15
          Alignment = taRightJustify
          AutoSize = False
          Caption = #49692#49688#50896#54868' '#46608#45716' '#50808#54868'(USD'#47484' '#51228#50808#54620')'#51068' '#44221#50864' '#49324#50857#50504#54632
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          UseSkinColor = False
        end
        object Edt_ApplicationCode: TsEdit
          Tag = 101
          Left = 115
          Top = 31
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          TabOrder = 0
          OnDblClick = Edt_ApplicationCodeDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#52397#51064
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationName1: TsEdit
          Left = 115
          Top = 55
          Width = 265
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 2
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAddr1: TsEdit
          Left = 115
          Top = 103
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAddr2: TsEdit
          Left = 115
          Top = 127
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAddr3: TsEdit
          Left = 115
          Top = 151
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 5
          SkinData.CustomColor = True
          BoundLabel.Caption = 'edt_pubaddr3'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationSAUPNO: TsEdit
          Left = 288
          Top = 31
          Width = 92
          Height = 23
          Color = 12582911
          MaxLength = 17
          TabOrder = 1
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object sPanel11: TsPanel
          Left = 408
          Top = 7
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          Caption = #52509' '#54624#51064'/'#54624#51613'/'#48320#46041' '#45236#50669
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object sPanel8: TsPanel
          Left = 8
          Top = 286
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          Caption = #47928#49436#44396#48516
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
        end
        object Edt_ApplicationName2: TsEdit
          Left = 115
          Top = 79
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAXNAME1: TsEdit
          Left = 115
          Top = 175
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationElectronicSign: TsEdit
          Left = 115
          Top = 199
          Width = 126
          Height = 23
          Color = 12582911
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_ApplicationAPPDATE: TsMaskEdit
          Left = 299
          Top = 199
          Width = 81
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 11
          Color = clWhite
          BoundLabel.Active = True
          BoundLabel.Caption = #48156#54665#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object edt_N4025: TsEdit
          Tag = 1000
          Left = 115
          Top = 255
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 12
          OnChange = edt_N4025Change
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44277#44553#47932#54408' '#47749#49464#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_N4025_NM: TsEdit
          Tag = -1
          Left = 188
          Top = 255
          Width = 192
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel2: TsPanel
          Left = 8
          Top = 230
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          Caption = #44277#44553#47932#54408#47749#49464#44396#48516
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 14
        end
        object sPanel3: TsPanel
          Left = 8
          Top = 7
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          Caption = #49888#52397#51064
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 15
        end
        object Edt_DocumentsSection: TsEdit
          Tag = 1001
          Left = 115
          Top = 311
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 16
          OnChange = edt_N4025Change
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_DocumentsSection_NM: TsEdit
          Tag = -1
          Left = 188
          Top = 311
          Width = 192
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_BuyConfirmDocuments: TsEdit
          Left = 115
          Top = 335
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.Caption = #44396#47588#54869#51064#49436#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyCode: TsEdit
          Tag = 102
          Left = 115
          Top = 407
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          TabOrder = 19
          OnDblClick = Edt_ApplicationCodeDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44277#44553#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_SupplySaupNo: TsEdit
          Left = 288
          Top = 407
          Width = 92
          Height = 23
          Color = 12582911
          MaxLength = 17
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyName: TsEdit
          Left = 115
          Top = 431
          Width = 265
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyCEO: TsEdit
          Left = 115
          Top = 455
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #45824#54364#51060#49324
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyEID: TsEdit
          Left = 115
          Top = 479
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#51088#47700#51068
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyAddr1: TsEdit
          Left = 115
          Top = 503
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 24
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object Edt_SupplyAddr2: TsEdit
          Left = 115
          Top = 527
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 25
          SkinData.CustomColor = True
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CHG_G: TsEdit
          Tag = 1002
          Left = 472
          Top = 31
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 1
          TabOrder = 26
          OnChange = edt_N4025Change
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #48320#46041#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CHG_G_NM: TsEdit
          Tag = -1
          Left = 545
          Top = 31
          Width = 190
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object curr_C2_AMT: TsCurrencyEdit
          Left = 472
          Top = 74
          Width = 134
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          DoubleBuffered = False
          TabOrder = 28
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object sPanel9: TsPanel
          Left = 607
          Top = 74
          Width = 40
          Height = 23
          SkinData.CustomColor = True
          Caption = 'USD'
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
        end
        object curr_C1_AMT: TsCurrencyEdit
          Left = 472
          Top = 98
          Width = 134
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          DoubleBuffered = False
          TabOrder = 30
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt_C1_C: TsEdit
          Tag = 2000
          Left = 607
          Top = 98
          Width = 40
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 31
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Caption = #48320#46041#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object sPanel10: TsPanel
          Left = 408
          Top = 247
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          Caption = #54869#51064#44592#44288
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 32
        end
        object edt_BK_CD: TsEdit
          Tag = 101
          Left = 472
          Top = 271
          Width = 89
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          TabOrder = 33
          OnDblClick = edt_BK_CDDblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #54869#51064#44592#44288
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
          BoundLabel.ParentFont = False
        end
        object edt_BK_NAME1: TsEdit
          Left = 472
          Top = 295
          Width = 263
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 34
          SkinData.CustomColor = True
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BK_NAME2: TsEdit
          Left = 472
          Top = 319
          Width = 263
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 35
          SkinData.CustomColor = True
          BoundLabel.Caption = 'edt_pubaddr3'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sButton1: TsButton
          Left = 165
          Top = 31
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 36
          OnClick = sButton1Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 41
        end
        object sButton3: TsButton
          Tag = 1
          Left = 165
          Top = 407
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 37
          OnClick = sButton1Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 41
        end
        object sButton4: TsButton
          Left = 154
          Top = 255
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 38
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sButton5: TsButton
          Tag = 1
          Left = 154
          Top = 311
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 39
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sButton6: TsButton
          Tag = 2
          Left = 511
          Top = 31
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 40
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sButton7: TsButton
          Left = 562
          Top = 271
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 41
          OnClick = edt_BK_CDDblClick
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sButton23: TsButton
          Tag = 3
          Left = 648
          Top = 98
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 42
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sPanel31: TsPanel
          Left = 8
          Top = 382
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          Caption = #44277#44553#51088
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 43
        end
        object sPanel16: TsPanel
          Left = 408
          Top = 135
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          Caption = #52509' '#54633#44228
          Color = 16042877
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 44
        end
        object edt_TQTYC: TsEdit
          Tag = 2001
          Left = 490
          Top = 160
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 45
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#49688#47049
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_TQTY: TsCurrencyEdit
          Left = 563
          Top = 160
          Width = 140
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          DoubleBuffered = False
          TabOrder = 46
          BoundLabel.Caption = #52509#49688#47049
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object curr_TAMT2: TsCurrencyEdit
          Left = 563
          Top = 184
          Width = 140
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          DoubleBuffered = False
          TabOrder = 47
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529'('#50896#54868')'
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 0
          DisplayFormat = '#,0'
        end
        object edt_TAMT1C: TsEdit
          Tag = 2000
          Left = 490
          Top = 208
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 48
          OnDblClick = edt_N4025DblClick
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_TAMT1: TsCurrencyEdit
          Left = 563
          Top = 208
          Width = 140
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          DoubleBuffered = False
          TabOrder = 49
          BoundLabel.Caption = #52509#44552#50529'('#50808#54868')'
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object sButton2: TsButton
          Left = 704
          Top = 160
          Width = 37
          Height = 71
          Cursor = crHandPoint
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 50
          OnClick = sButton2Click
          Reflected = True
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 38
        end
        object sButton13: TsButton
          Tag = 4
          Left = 529
          Top = 160
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 51
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sButton14: TsButton
          Tag = 5
          Left = 529
          Top = 208
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 52
          OnClick = sButton4Click
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
      end
    end
    object sTabSheetDetail: TsTabSheet
      Caption = #49345#54408#45236#50669
      ImageIndex = 37
      object sPanel27: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sDBGrid2: TsDBGrid
          Left = 1
          Top = 33
          Width = 751
          Height = 162
          TabStop = False
          Align = alClient
          Color = clWhite
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnTitleClick = sDBGrid2TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 91
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 70
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTYC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT1'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 108
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT1C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRI2'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44396#47588'('#44277#44553#51068')'
              Width = 80
              Visible = True
            end>
        end
        object sPanel1: TsPanel
          Left = 1
          Top = 1
          Width = 751
          Height = 32
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          DoubleBuffered = False
          TabOrder = 1
          object Btn_DetailAdd: TsSpeedButton
            Tag = 1
            Left = 1
            Top = 1
            Width = 90
            Height = 30
            Cursor = crHandPoint
            Caption = #49345#54408#52628#44032
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object Btn_DetailEdit: TsSpeedButton
            Tag = 2
            Left = 91
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object Btn_DetailDel: TsSpeedButton
            Left = 151
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailDelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton14: TsSpeedButton
            Left = 211
            Top = 1
            Width = 14
            Height = 30
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailOk: TsSpeedButton
            Left = 225
            Top = 1
            Width = 60
            Height = 30
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailOkClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 0
          end
          object Btn_DetailCancel: TsSpeedButton
            Tag = 1
            Left = 285
            Top = 1
            Width = 60
            Height = 30
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailCancelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object GoodsExcelBtn: TsSpeedButton
            Left = 556
            Top = 1
            Width = 110
            Height = 30
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = GoodsExcelBtnClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 31
          end
          object GoodsSampleBtn: TsSpeedButton
            Left = 666
            Top = 1
            Width = 84
            Height = 30
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = GoodsSampleBtnClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 14
          end
        end
        object sPanel12: TsPanel
          Left = 1
          Top = 197
          Width = 751
          Height = 367
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 2
          DesignSize = (
            751
            367)
          object sSpeedButton13: TsSpeedButton
            Left = 395
            Top = 5
            Width = 11
            Height = 357
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
          end
          object sPanel14: TsPanel
            Left = 8
            Top = 7
            Width = 383
            Height = 21
            SkinData.CustomColor = True
            Caption = #49345#54408#51221#48372
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object msk_HS_NO: TsMaskEdit
            Left = 299
            Top = 32
            Width = 92
            Height = 23
            AutoSize = False
            DoubleBuffered = False
            MaxLength = 12
            TabOrder = 0
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = 'HS'#48512#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999.99-9999;0'
            Text = '1234567890'
          end
          object msk_APP_DATE: TsMaskEdit
            Left = 299
            Top = 57
            Width = 92
            Height = 23
            AutoSize = False
            DoubleBuffered = False
            MaxLength = 10
            TabOrder = 2
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #44396#47588'('#44277#44553#51068')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object memo_NAME1: TsMemo
            Left = 52
            Top = 82
            Width = 339
            Height = 103
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 3
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object memo_Size: TsMemo
            Left = 52
            Top = 188
            Width = 339
            Height = 58
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 4
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object memo_Remark: TsMemo
            Left = 52
            Top = 249
            Width = 339
            Height = 60
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 5
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #48708#44256
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_NAMESS: TsEdit
            Tag = 101
            Left = 52
            Top = 57
            Width = 133
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 1
            OnDblClick = edt_NAMESSDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel15: TsPanel
            Left = 408
            Top = 7
            Width = 335
            Height = 21
            SkinData.CustomColor = True
            Caption = #49688#47049' / '#45800#44032' / '#44552#50529
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 21
          end
          object edt_QTYC: TsEdit
            Tag = 3001
            Left = 530
            Top = 32
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 7
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object curr_QTY: TsCurrencyEdit
            Left = 603
            Top = 32
            Width = 140
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 8
            OnExit = curr_QTYExit
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_PRI2: TsCurrencyEdit
            Tag = 1
            Left = 530
            Top = 56
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 9
            OnExit = curr_QTYExit
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_PRI_BASEC: TsEdit
            Tag = 3001
            Left = 530
            Top = 80
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 10
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_PRI_BASE: TsCurrencyEdit
            Left = 603
            Top = 80
            Width = 140
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_PRI1: TsCurrencyEdit
            Left = 530
            Top = 104
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032'(USD'#44552#50529#48512#44592')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_AMT1C: TsEdit
            Tag = 3000
            Left = 530
            Top = 128
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 13
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object curr_AMT1: TsCurrencyEdit
            Left = 603
            Top = 128
            Width = 140
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 14
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_AMT2: TsCurrencyEdit
            Left = 530
            Top = 152
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 15
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529'(USD'#44552#50529#48512#44592')'
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_ACHG_G: TsEdit
            Tag = 3002
            Left = 530
            Top = 208
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 1
            TabOrder = 16
            OnChange = edt_N4025Change
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48320#46041#44396#48516
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_AC1_AMT: TsCurrencyEdit
            Left = 569
            Top = 232
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 17
            BoundLabel.Active = True
            BoundLabel.Caption = #48320#46041#44552#50529'(USD '#44552#50529' '#48512#44592')'
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_AC2_AMT: TsCurrencyEdit
            Left = 603
            Top = 256
            Width = 140
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 19
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object sButton8: TsButton
            Left = 186
            Top = 57
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            OnClick = sButton8Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton9: TsButton
            Tag = 6
            Left = 569
            Top = 32
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 23
            TabStop = False
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton10: TsButton
            Tag = 7
            Left = 569
            Top = 80
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 24
            TabStop = False
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton11: TsButton
            Tag = 8
            Left = 569
            Top = 128
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 25
            TabStop = False
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton12: TsButton
            Left = 569
            Top = 208
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 26
            TabStop = False
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sPanel33: TsPanel
            Left = 408
            Top = 183
            Width = 335
            Height = 21
            SkinData.CustomColor = True
            Caption = #54408#47785#48324' '#54624#51064'/'#54624#51613'/'#48320#46041' '#45236#50669
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 27
          end
          object sEdit2: TsEdit
            Tag = -1
            Left = 603
            Top = 208
            Width = 140
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AC1_C: TsEdit
            Tag = 3000
            Left = 530
            Top = 256
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 18
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48320#46041#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_COMPAY_CD: TsEdit
            Tag = 101
            Left = 258
            Top = 313
            Width = 133
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51088#49324#44288#47532#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sButton24: TsButton
            Tag = 9
            Left = 569
            Top = 256
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 29
            TabStop = False
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sCheckBox2: TsCheckBox
            Tag = -1
            Left = 408
            Top = 130
            Width = 74
            Height = 19
            Caption = #51088#46041#44228#49328
            Checked = True
            State = cbChecked
            TabOrder = 30
          end
        end
        object sPanel13: TsPanel
          Left = 1
          Top = 195
          Width = 751
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 3
        end
        object sPanel34: TsPanel
          Left = 1
          Top = 33
          Width = 751
          Height = 162
          Align = alClient
          Caption = #49345#54408#45236#50669' '#51077#47141#51473#51077#45768#45796
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          Visible = False
        end
      end
    end
    object sTabSheetDocument: TsTabSheet
      Caption = #44540#44144#49436#47448
      ImageIndex = 39
      object sPanel17: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sDBGrid3: TsDBGrid
          Left = 1
          Top = 33
          Width = 751
          Height = 165
          Align = alClient
          Color = clWhite
          DataSource = dsDocument
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnTitleClick = sDBGrid3TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DOC_G'
              Title.Alignment = taCenter
              Title.Caption = #53076#46300
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DOC_NAME'
              Title.Alignment = taCenter
              Title.Caption = #49436#47448#47749
              Width = 186
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DOC_D'
              Title.Alignment = taCenter
              Title.Caption = #44540#44144#49436#47448#48264#54840
              Width = 135
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BNAME1'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749
              Width = 156
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAMT'
              Title.Alignment = taCenter
              Title.Caption = #52509#44552#50529
              Width = 84
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BAMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SHIP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #49440#51201#44592#51068
              Width = 86
              Visible = True
            end>
        end
        object sPanel18: TsPanel
          Left = 1
          Top = 1
          Width = 751
          Height = 32
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          DoubleBuffered = False
          TabOrder = 1
          object btn_docNew: TsSpeedButton
            Tag = 1
            Left = 1
            Top = 1
            Width = 90
            Height = 30
            Cursor = crHandPoint
            Caption = #49436#47448#52628#44032
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_docNewClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object btn_DocEdit: TsSpeedButton
            Tag = 2
            Left = 91
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_docNewClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object btn_DocDel: TsSpeedButton
            Left = 151
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_DocDelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton17: TsSpeedButton
            Left = 211
            Top = 1
            Width = 14
            Height = 30
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_DocOk: TsSpeedButton
            Left = 225
            Top = 1
            Width = 60
            Height = 30
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_DocOkClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 0
          end
          object btn_DocCancel: TsSpeedButton
            Tag = 1
            Left = 285
            Top = 1
            Width = 60
            Height = 30
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_DocCancelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object btn_DocExcel: TsSpeedButton
            Left = 556
            Top = 1
            Width = 110
            Height = 30
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_DocExcelClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 31
          end
          object btn_DocSample: TsSpeedButton
            Left = 666
            Top = 1
            Width = 84
            Height = 30
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_DocSampleClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 14
          end
        end
        object sPanel19: TsPanel
          Left = 1
          Top = 200
          Width = 751
          Height = 364
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 2
          DesignSize = (
            751
            364)
          object sSpeedButton22: TsSpeedButton
            Left = 380
            Top = 5
            Width = 11
            Height = 354
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
          end
          object sLabel4: TsLabel
            Left = 67
            Top = 250
            Width = 192
            Height = 13
            AutoSize = False
            Caption = #50756#51228#54408#51068' '#44221#50864#45716' '#44552#50529#51077#47141#51012' '#49373#47029' '#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            UseSkinColor = False
          end
          object sLabel8: TsLabel
            Left = 66
            Top = 306
            Width = 307
            Height = 13
            Caption = #44540#44144#49436#47448' '#49345#51032' '#49440#51201#44592#51068#51060' '#50668#47084' '#44060#51068' '#44221#50864' '#52572#51109' '#49440#51201#44592#51068' '#51077#47141
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            UseSkinColor = False
          end
          object sLabel9: TsLabel
            Left = 66
            Top = 322
            Width = 314
            Height = 13
            Caption = #44540#44144#49436#47448' '#51333#47448#44032' '#44396#47588#54869#51064#49436#51068' '#44221#50864', '#49440#51201#44592#51068#51008' '#47932#54408#44277#44553'('#50696#51221')'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            UseSkinColor = False
          end
          object sLabel10: TsLabel
            Left = 66
            Top = 338
            Width = 37
            Height = 13
            Caption = #51068' '#51077#47141
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            UseSkinColor = False
          end
          object sPanel20: TsPanel
            Left = 8
            Top = 7
            Width = 367
            Height = 21
            SkinData.CustomColor = True
            Caption = #49345#54408#51221#48372
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object edt_BNAME: TsEdit
            Tag = 101
            Left = 68
            Top = 40
            Width = 132
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 1
            OnDblClick = edt_BNAMEDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object msk_BHS_NO: TsMaskEdit
            Left = 283
            Top = 40
            Width = 92
            Height = 23
            AutoSize = False
            DoubleBuffered = False
            MaxLength = 12
            TabOrder = 2
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = 'HS'#48512#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999.99-9999;0'
            Text = '1234567890'
          end
          object memo_BNAME1: TsMemo
            Left = 68
            Top = 64
            Width = 307
            Height = 89
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 3
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object memo_BSIZE1: TsMemo
            Left = 68
            Top = 154
            Width = 307
            Height = 58
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 4
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
          end
          object edt_BAMTC: TsEdit
            Tag = 4000
            Left = 68
            Top = 224
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 5
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #52509#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object curr_BAMT: TsCurrencyEdit
            Left = 141
            Top = 224
            Width = 137
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 6
            Color = 12582911
            BoundLabel.Caption = #52509#44552#50529'('#50808#54868')'
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object msk_SHIP_DATE: TsMaskEdit
            Left = 67
            Top = 281
            Width = 92
            Height = 23
            AutoSize = False
            DoubleBuffered = False
            MaxLength = 10
            TabOrder = 7
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#51201#44592#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sPanel21: TsPanel
            Left = 392
            Top = 7
            Width = 351
            Height = 21
            SkinData.CustomColor = True
            Caption = #44540#44144#49436#47448
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
          end
          object edt_DOC_G: TsEdit
            Tag = 4010
            Left = 476
            Top = 40
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 9
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44540#44144#49436#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object edt_DOC_D: TsEdit
            Tag = 101
            Left = 476
            Top = 64
            Width = 267
            Height = 23
            CharCase = ecUpperCase
            Color = 12582911
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44540#44144#49436#47448#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
          end
          object edt_BAL_CD: TsEdit
            Tag = 1008
            Left = 476
            Top = 112
            Width = 85
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 11
            OnDblClick = edt_BAL_CDDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#48156#44553#44592#44288
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BAL_NAME1: TsEdit
            Tag = 101
            Left = 476
            Top = 136
            Width = 267
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_BAL_NAME2: TsEdit
            Tag = 101
            Left = 476
            Top = 160
            Width = 267
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_NAT: TsEdit
            Tag = 4011
            Left = 671
            Top = 88
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 14
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#52636#45824#49345' '#44397#44032#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DOC_G_NM: TsEdit
            Tag = -1
            Left = 549
            Top = 40
            Width = 194
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sButton15: TsButton
            Left = 201
            Top = 40
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 16
            OnClick = sButton15Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton16: TsButton
            Tag = 10
            Left = 107
            Top = 224
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 17
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton17: TsButton
            Tag = 11
            Left = 515
            Top = 40
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 18
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton18: TsButton
            Left = 562
            Top = 112
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 19
            OnClick = sButton18Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton19: TsButton
            Tag = 12
            Left = 710
            Top = 88
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 20
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
        end
        object sPanel23: TsPanel
          Left = 1
          Top = 198
          Width = 751
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 3
        end
        object sPanel35: TsPanel
          Left = 1
          Top = 33
          Width = 751
          Height = 165
          Align = alClient
          Caption = #44540#44144#49436#47448' '#51077#47141#51473#51077#45768#45796
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          Visible = False
        end
      end
    end
    object sTabSheetTax: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      ImageIndex = 40
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        DoubleBuffered = False
        TabOrder = 0
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 33
          Width = 751
          Height = 241
          Align = alClient
          Color = clWhite
          DataSource = dsTax
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnTitleClick = sDBGrid4TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Alignment = taCenter
              Title.Caption = #49464#44552#44228#49328#49436#48264#54840
              Width = 206
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529
              Width = 127
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529
              Width = 85
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTITY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QUANTITY_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end>
        end
        object sPanel25: TsPanel
          Left = 1
          Top = 1
          Width = 751
          Height = 32
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          DoubleBuffered = False
          TabOrder = 1
          object btn_TaxNew: TsSpeedButton
            Tag = 1
            Left = 1
            Top = 1
            Width = 104
            Height = 30
            Cursor = crHandPoint
            Caption = #44228#49328#49436#52628#44032
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxNewClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object btn_TaxEdit: TsSpeedButton
            Tag = 2
            Left = 105
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxNewClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object btn_TaxDel: TsSpeedButton
            Left = 165
            Top = 1
            Width = 60
            Height = 30
            Cursor = crHandPoint
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxDelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton26: TsSpeedButton
            Left = 225
            Top = 1
            Width = 14
            Height = 30
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_TaxOk: TsSpeedButton
            Left = 239
            Top = 1
            Width = 60
            Height = 30
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxOkClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 0
          end
          object btn_TaxCancel: TsSpeedButton
            Tag = 1
            Left = 299
            Top = 1
            Width = 60
            Height = 30
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxCancelClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object btn_TaxExcel: TsSpeedButton
            Left = 556
            Top = 1
            Width = 110
            Height = 30
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxExcelClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 31
          end
          object btn_TaxSample: TsSpeedButton
            Left = 666
            Top = 1
            Width = 84
            Height = 30
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = btn_TaxSampleClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 14
          end
        end
        object sPanel26: TsPanel
          Left = 1
          Top = 276
          Width = 751
          Height = 288
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 2
          DesignSize = (
            751
            288)
          object Shape1: TShape
            Left = 375
            Top = 7
            Width = 1
            Height = 269
            Anchors = [akLeft, akTop, akBottom]
            Brush.Color = 13421772
            Pen.Color = 13421772
          end
          object edt_BILL_NO: TsEdit
            Left = 99
            Top = 39
            Width = 262
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object msk_BILL_DATE: TsMaskEdit
            Left = 99
            Top = 66
            Width = 81
            Height = 23
            AutoSize = False
            DoubleBuffered = False
            MaxLength = 10
            TabOrder = 1
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #51089#49457#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_ITEM_NAME1: TsEdit
            Left = 99
            Top = 93
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47785
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ITEM_NAME2: TsEdit
            Left = 99
            Top = 117
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Caption = 'edt_pubaddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ITEM_NAME3: TsEdit
            Left = 99
            Top = 141
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Caption = 'edt_pubaddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ITEM_NAME4: TsEdit
            Left = 99
            Top = 165
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Caption = 'edt_pubaddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ITEM_NAME5: TsEdit
            Left = 99
            Top = 189
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Caption = 'edt_pubaddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object memo_DESC: TsMemo
            Left = 99
            Top = 216
            Width = 262
            Height = 65
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 7
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
          end
          object edt_BILL_AMOUNT_UNIT: TsEdit
            Tag = 5000
            Left = 491
            Top = 40
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 8
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_BILL_AMOUNT: TsCurrencyEdit
            Left = 564
            Top = 40
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object edt_TAX_AMOUNT_UNIT: TsEdit
            Tag = 5000
            Left = 491
            Top = 64
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 10
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_TAX_AMOUNT: TsCurrencyEdit
            Left = 564
            Top = 64
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object edt_QUANTITY_UNIT: TsEdit
            Tag = 5001
            Left = 491
            Top = 88
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 12
            OnDblClick = edt_N4025DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_QUANTITY: TsCurrencyEdit
            Left = 564
            Top = 88
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            DoubleBuffered = False
            TabOrder = 13
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object sPanel28: TsPanel
            Left = 11
            Top = 7
            Width = 350
            Height = 21
            SkinData.CustomColor = True
            Caption = #49464#44552#44228#49328#49436
            Color = 16042877
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
          end
          object sButton20: TsButton
            Tag = 13
            Left = 530
            Top = 40
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 15
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton21: TsButton
            Tag = 14
            Left = 530
            Top = 64
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 16
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
          object sButton22: TsButton
            Tag = 15
            Left = 530
            Top = 88
            Width = 33
            Height = 23
            Cursor = crHandPoint
            TabOrder = 17
            OnClick = sButton4Click
            Images = DMICON.System18
            ImageAlignment = iaCenter
            ImageIndex = 42
          end
        end
        object sPanel32: TsPanel
          Left = 1
          Top = 274
          Width = 751
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          DoubleBuffered = False
          TabOrder = 3
        end
        object sPanel36: TsPanel
          Left = 1
          Top = 33
          Width = 751
          Height = 241
          Align = alClient
          Caption = #49464#44552#44228#49328#49436' '#51077#47141#51473#51077#45768#45796
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          Visible = False
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      ImageIndex = 24
      object sPanel30: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 32
        Align = alTop
        DoubleBuffered = False
        TabOrder = 0
        object sSpeedButton31: TsSpeedButton
          Left = 230
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit8: TsMaskEdit
          Tag = 2
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 0
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sMaskEdit9: TsMaskEdit
          Tag = 3
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          DoubleBuffered = False
          MaxLength = 10
          TabOrder = 1
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 469
          Top = 5
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = sBitBtn1Click
        end
        object sEdit47: TsEdit
          Tag = -1
          Left = 297
          Top = 5
          Width = 171
          Height = 23
          TabOrder = 3
          OnChange = sEdit47Change
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
      end
      object sDBGrid5: TsDBGrid
        Left = 0
        Top = 32
        Width = 753
        Height = 533
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnTitleClick = sDBGrid5TitleClick
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 36
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 143
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUP_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#51088
            Width = 202
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT1'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 157
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TAMT1C'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 33
            Visible = True
          end>
      end
    end
  end
  object sPanel22: TsPanel [3]
    Left = 925
    Top = 75
    Width = 187
    Height = 24
    SkinData.SkinSection = 'TRANSPARENT'
    DoubleBuffered = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object edt_TEMPNO: TsEdit
      Left = 0
      Top = 0
      Width = 185
      Height = 23
      TabOrder = 0
      Text = 'edt_TEMPNO'
      Visible = False
    end
  end
  object qryAPPPCR_H: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryAPPPCR_HAfterOpen
    AfterScroll = qryAPPPCR_HAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, CHK1, CHK2, CHK3, MESSAGE1, MES' +
        'SAGE2, APP_CODE, APP_NAME1, APP_NAME2, APP_NAME3, APP_ADDR1, APP' +
        '_ADDR2, APP_ADDR3,'
      
        'AX_NAME1, AX_NAME2, AX_NAME3, SUP_CODE, SUP_NAME1, SUP_NAME2, SU' +
        'P_NAME3, SUP_ADDR1, SUP_ADDR2, SUP_ADDR3, ITM_G1, ITM_G2, ITM_G3' +
        ','
      
        'TQTY, TQTYC, TAMT1, TAMT1C, TAMT2, TAMT2C, CHG_G, C1_AMT, C1_C, ' +
        'C2_AMT, BK_NAME1, BK_NAME2, PRNO, PCrLic_No, ChgCd, APP_DATE, DO' +
        'C_G,'
      
        'DOC_D, BHS_NO, BNAME, BNAME1, BAMT, BAMTC, EXP_DATE, SHIP_DATE, ' +
        'BSIZE1, BAL_CD, BAL_NAME1, BAL_NAME2, F_INTERFACE, SRBUHO, ITM_G' +
        'BN,'
      
        'ITM_GNAME, ISSUE_GBN, DOC_GBN, DOC_NO, BAMT2, BAMTC2, BAMT3, BAM' +
        'TC3, PCRLIC_ISS_NO, FIN_CODE, FIN_NAME, FIN_NAME2, NEGO_APPDT, E' +
        'MAIL_ID, EMAIL_DOMAIN, BK_CD,'
      
        'N4025.NAME as ITM_GBN_NM, APPPCR_G.NAME as ChgCd_NM, CHG_G_T.NAM' +
        'E as CHG_G_NM'
      
        'FROM APPPCR_H LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE P' +
        'refix = '#39#45236#44397#49888'4025'#39') N4025 ON APPPCR_H.ITM_GBN = N4025.CODE '
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'APPPC' +
        'R'#44396#48516#39') APPPCR_G ON APPPCR_H.ChgCd = APPPCR_G.CODE'
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39#48320#46041#44396#48516#39 +
        ') CHG_G_T ON APPPCR_H.CHG_G = CHG_G_T.CODE')
    Left = 8
    Top = 184
    object qryAPPPCR_HMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryAPPPCR_HUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryAPPPCR_HDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryAPPPCR_HCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryAPPPCR_HCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
      Size = 1
    end
    object qryAPPPCR_HCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = CHK3GetText
      Size = 10
    end
    object qryAPPPCR_HMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryAPPPCR_HMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryAPPPCR_HAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryAPPPCR_HAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryAPPPCR_HAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryAPPPCR_HAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryAPPPCR_HAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryAPPPCR_HAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryAPPPCR_HAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryAPPPCR_HAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryAPPPCR_HAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryAPPPCR_HAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 10
    end
    object qryAPPPCR_HSUP_CODE: TStringField
      FieldName = 'SUP_CODE'
      Size = 10
    end
    object qryAPPPCR_HSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryAPPPCR_HSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryAPPPCR_HSUP_NAME3: TStringField
      FieldName = 'SUP_NAME3'
      Size = 35
    end
    object qryAPPPCR_HSUP_ADDR1: TStringField
      FieldName = 'SUP_ADDR1'
      Size = 35
    end
    object qryAPPPCR_HSUP_ADDR2: TStringField
      FieldName = 'SUP_ADDR2'
      Size = 35
    end
    object qryAPPPCR_HSUP_ADDR3: TStringField
      FieldName = 'SUP_ADDR3'
      Size = 35
    end
    object qryAPPPCR_HITM_G1: TStringField
      FieldName = 'ITM_G1'
      Size = 3
    end
    object qryAPPPCR_HITM_G2: TStringField
      FieldName = 'ITM_G2'
      Size = 3
    end
    object qryAPPPCR_HITM_G3: TStringField
      FieldName = 'ITM_G3'
      Size = 3
    end
    object qryAPPPCR_HTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryAPPPCR_HTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryAPPPCR_HTAMT1: TBCDField
      FieldName = 'TAMT1'
      DisplayFormat = '#,0.###'
      Precision = 18
    end
    object qryAPPPCR_HTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryAPPPCR_HTAMT2: TBCDField
      FieldName = 'TAMT2'
      Precision = 18
    end
    object qryAPPPCR_HTAMT2C: TStringField
      FieldName = 'TAMT2C'
      Size = 3
    end
    object qryAPPPCR_HCHG_G: TStringField
      FieldName = 'CHG_G'
      Size = 3
    end
    object qryAPPPCR_HC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryAPPPCR_HC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryAPPPCR_HC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryAPPPCR_HBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 70
    end
    object qryAPPPCR_HBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 70
    end
    object qryAPPPCR_HPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryAPPPCR_HPCrLic_No: TStringField
      FieldName = 'PCrLic_No'
      Size = 35
    end
    object qryAPPPCR_HChgCd: TStringField
      FieldName = 'ChgCd'
      Size = 3
    end
    object qryAPPPCR_HAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryAPPPCR_HDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryAPPPCR_HDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryAPPPCR_HBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryAPPPCR_HBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryAPPPCR_HBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryAPPPCR_HBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryAPPPCR_HBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryAPPPCR_HEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryAPPPCR_HSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryAPPPCR_HBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryAPPPCR_HBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 4
    end
    object qryAPPPCR_HBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryAPPPCR_HBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryAPPPCR_HF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryAPPPCR_HSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryAPPPCR_HITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryAPPPCR_HITM_GNAME: TStringField
      FieldName = 'ITM_GNAME'
      Size = 70
    end
    object qryAPPPCR_HISSUE_GBN: TStringField
      FieldName = 'ISSUE_GBN'
      Size = 3
    end
    object qryAPPPCR_HDOC_GBN: TStringField
      FieldName = 'DOC_GBN'
      Size = 3
    end
    object qryAPPPCR_HDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryAPPPCR_HBAMT2: TBCDField
      FieldName = 'BAMT2'
      Precision = 18
    end
    object qryAPPPCR_HBAMTC2: TStringField
      FieldName = 'BAMTC2'
      Size = 3
    end
    object qryAPPPCR_HBAMT3: TBCDField
      FieldName = 'BAMT3'
      Precision = 18
    end
    object qryAPPPCR_HBAMTC3: TStringField
      FieldName = 'BAMTC3'
      Size = 3
    end
    object qryAPPPCR_HPCRLIC_ISS_NO: TStringField
      FieldName = 'PCRLIC_ISS_NO'
      Size = 35
    end
    object qryAPPPCR_HFIN_CODE: TStringField
      FieldName = 'FIN_CODE'
      Size = 11
    end
    object qryAPPPCR_HFIN_NAME: TStringField
      FieldName = 'FIN_NAME'
      Size = 70
    end
    object qryAPPPCR_HFIN_NAME2: TStringField
      FieldName = 'FIN_NAME2'
      Size = 70
    end
    object qryAPPPCR_HNEGO_APPDT: TStringField
      FieldName = 'NEGO_APPDT'
      Size = 8
    end
    object qryAPPPCR_HEMAIL_ID: TStringField
      FieldName = 'EMAIL_ID'
      Size = 35
    end
    object qryAPPPCR_HEMAIL_DOMAIN: TStringField
      FieldName = 'EMAIL_DOMAIN'
      Size = 35
    end
    object qryAPPPCR_HBK_CD: TStringField
      FieldName = 'BK_CD'
      Size = 11
    end
    object qryAPPPCR_HITM_GBN_NM: TStringField
      FieldName = 'ITM_GBN_NM'
      Size = 100
    end
    object qryAPPPCR_HChgCd_NM: TStringField
      FieldName = 'ChgCd_NM'
      Size = 100
    end
    object qryAPPPCR_HCHG_G_NM: TStringField
      FieldName = 'CHG_G_NM'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryAPPPCR_H
    Left = 40
    Top = 184
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDetailAfterOpen
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, HS_NO, NAME, NAME1, SIZE, SIZE1, REMARK, QTY, ' +
        'QTYC, AMT1, AMT1C, AMT2, PRI1, PRI2, PRI_BASE, PRI_BASEC,'
      
        'ACHG_G, AC1_AMT, AC1_C, AC2_AMT, LOC_TYPE, NAMESS, SIZESS, APP_D' +
        'ATE, ITM_NUM, COMPANY_CD'
      'FROM APPPCRD1'
      'WHERE KEYY = :KEYY')
    Left = 8
    Top = 216
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '0000.00-0000;0;'
      Size = 10
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryDetailNAME1GetText
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailREMARK: TMemoField
      FieldName = 'REMARK'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryDetailQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryDetailAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryDetailAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailAMT2: TBCDField
      FieldName = 'AMT2'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI1: TBCDField
      FieldName = 'PRI1'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI2: TBCDField
      FieldName = 'PRI2'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryDetailPRI_BASE: TBCDField
      FieldName = 'PRI_BASE'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI_BASEC: TStringField
      FieldName = 'PRI_BASEC'
      Size = 3
    end
    object qryDetailACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryDetailAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryDetailAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryDetailNAMESS: TStringField
      FieldName = 'NAMESS'
    end
    object qryDetailSIZESS: TStringField
      FieldName = 'SIZESS'
    end
    object qryDetailAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryDetailITM_NUM: TStringField
      FieldName = 'ITM_NUM'
      Size = 35
    end
    object qryDetailCOMPANY_CD: TStringField
      FieldName = 'COMPANY_CD'
      Size = 35
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 40
    Top = 216
  end
  object qryDocument: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDocumentAfterOpen
    AfterScroll = qryDocumentAfterScroll
    Parameters = <
      item
        Name = 'KEY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R-21101'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, DOC_G,ISNULL(DOC_NAME,'#39#39') DOC_NAME, DOC_D, BHS' +
        '_NO, BNAME, BNAME1, BAMT, BAMTC, EXP_DATE, SHIP_DATE, BSIZE1, BA' +
        'L_NAME1, BAL_NAME2, BAL_CD, NAT'
      
        'FROM APPPCRD2 with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME'
      #9#9#9#9#9#9'FROM CODE2NDD with(nolock)'
      
        #9#9#9#9#9#9'WHERE Prefix = '#39'APPPCR'#49436#47448#39') DOC ON APPPCRD2.DOC_G = DOC.COD' +
        'E'
      'WHERE [KEYY] = :KEY')
    Left = 8
    Top = 248
    object qryDocumentKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDocumentSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDocumentDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryDocumentDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryDocumentBHS_NO: TStringField
      FieldName = 'BHS_NO'
      EditMask = '9999.99-9999;0;'
      Size = 10
    end
    object qryDocumentBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryDocumentBNAME1: TMemoField
      FieldName = 'BNAME1'
      OnGetText = qryDetailNAME1GetText
      BlobType = ftMemo
    end
    object qryDocumentBAMT: TBCDField
      FieldName = 'BAMT'
      DisplayFormat = '#,0.###'
      Precision = 18
    end
    object qryDocumentBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryDocumentEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryDocumentSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryDocumentBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryDocumentBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryDocumentBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryDocumentBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 11
    end
    object qryDocumentNAT: TStringField
      FieldName = 'NAT'
      Size = 4
    end
    object qryDocumentDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      ReadOnly = True
      Size = 100
    end
  end
  object dsDocument: TDataSource
    DataSet = qryDocument
    Left = 40
    Top = 248
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryTaxAfterOpen
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT KEYY, SEQ, DATEE, BILL_NO, BILL_DATE,'
      
        'ITEM_NAME1, ITEM_NAME2, ITEM_NAME3, ITEM_NAME4, ITEM_NAME5, ITEM' +
        '_DESC, BILL_AMOUNT, BILL_AMOUNT_UNIT, TAX_AMOUNT, TAX_AMOUNT_UNI' +
        'T, QUANTITY, QUANTITY_UNIT'
      'FROM APPPCR_TAX'
      'WHERE KEYY = :KEYY')
    Left = 8
    Top = 280
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryTaxITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      BlobType = ftMemo
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 40
    Top = 280
  end
  object qryDetailBackup: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, HS_NO, NAME, NAME1, SIZE, SIZE1, REMARK, QTY, ' +
        'QTYC, AMT1, AMT1C, AMT2, PRI1, PRI2, PRI_BASE, PRI_BASEC,'
      
        'ACHG_G, AC1_AMT, AC1_C, AC2_AMT, LOC_TYPE, NAMESS, SIZESS, APP_D' +
        'ATE, ITM_NUM, COMPANY_CD'
      'FROM APPPCRD1'
      'WHERE KEYY = :KEYY')
    Left = 72
    Top = 216
    object qryDetailBackupKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailBackupSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailBackupHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryDetailBackupNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailBackupNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryDetailBackupSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailBackupSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailBackupREMARK: TMemoField
      FieldName = 'REMARK'
      BlobType = ftMemo
    end
    object qryDetailBackupQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryDetailBackupQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryDetailBackupAMT1: TBCDField
      FieldName = 'AMT1'
      Precision = 18
    end
    object qryDetailBackupAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailBackupAMT2: TBCDField
      FieldName = 'AMT2'
      Precision = 18
    end
    object qryDetailBackupPRI1: TBCDField
      FieldName = 'PRI1'
      Precision = 18
    end
    object qryDetailBackupPRI2: TBCDField
      FieldName = 'PRI2'
      Precision = 18
    end
    object qryDetailBackupPRI_BASE: TBCDField
      FieldName = 'PRI_BASE'
      Precision = 18
    end
    object qryDetailBackupPRI_BASEC: TStringField
      FieldName = 'PRI_BASEC'
      Size = 3
    end
    object qryDetailBackupACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryDetailBackupAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryDetailBackupAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryDetailBackupAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryDetailBackupLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryDetailBackupNAMESS: TStringField
      FieldName = 'NAMESS'
    end
    object qryDetailBackupSIZESS: TStringField
      FieldName = 'SIZESS'
    end
    object qryDetailBackupAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryDetailBackupITM_NUM: TStringField
      FieldName = 'ITM_NUM'
      Size = 35
    end
    object qryDetailBackupCOMPANY_CD: TStringField
      FieldName = 'COMPANY_CD'
      Size = 35
    end
  end
  object excelOpen: TsOpenDialog
    Left = 920
    Top = 152
  end
  object qryDocumentBackup: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDocumentAfterOpen
    AfterScroll = qryDocumentAfterScroll
    Parameters = <
      item
        Name = 'KEY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R-21101'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, DOC_G, DOC_D, BHS_NO, BNAME, BNAME1, BAMT, BAM' +
        'TC, EXP_DATE, SHIP_DATE, BSIZE1, BAL_NAME1, BAL_NAME2, BAL_CD, N' +
        'AT'
      'FROM APPPCRD2'
      'WHERE [KEYY] = :KEY')
    Left = 72
    Top = 248
    object qryDocumentBackupKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDocumentBackupSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDocumentBackupDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryDocumentBackupDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryDocumentBackupBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryDocumentBackupBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryDocumentBackupBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryDocumentBackupBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryDocumentBackupBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryDocumentBackupEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryDocumentBackupSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryDocumentBackupBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryDocumentBackupBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryDocumentBackupBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryDocumentBackupBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 11
    end
    object qryDocumentBackupNAT: TStringField
      FieldName = 'NAT'
      Size = 4
    end
  end
  object qryTaxBackup: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryTaxAfterOpen
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT KEYY, SEQ, DATEE, BILL_NO, BILL_DATE,'
      
        'ITEM_NAME1, ITEM_NAME2, ITEM_NAME3, ITEM_NAME4, ITEM_NAME5, ITEM' +
        '_DESC, BILL_AMOUNT, BILL_AMOUNT_UNIT, TAX_AMOUNT, TAX_AMOUNT_UNI' +
        'T, QUANTITY, QUANTITY_UNIT'
      'FROM APPPCR_TAX'
      'WHERE KEYY = :KEYY')
    Left = 72
    Top = 280
    object qryTaxBackupKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxBackupSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryTaxBackupDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBackupBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBackupBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      Size = 8
    end
    object qryTaxBackupITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxBackupITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxBackupITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxBackupITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxBackupITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxBackupITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      BlobType = ftMemo
    end
    object qryTaxBackupBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      Precision = 18
    end
    object qryTaxBackupBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxBackupTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      Precision = 18
    end
    object qryTaxBackupTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxBackupQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      Precision = 18
    end
    object qryTaxBackupQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
  end
  object qryCopyDoc: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryTaxAfterOpen
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'OriginalKey'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'NewKey'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @OriginalKey varchar(35),'
      '        @NewKey varchar(35)'
      ''
      'SET @OriginalKey = :OriginalKey'
      'SET @NewKey = :NewKey'
      ''
      'INSERT INTO APPPCRD1'
      
        'SELECT @NewKey, SEQ, HS_NO, NAME, NAME1, SIZE, SIZE1, REMARK, QT' +
        'Y, QTYC, AMT1, AMT1C, AMT2,'
      
        'PRI1, PRI2, PRI_BASE, PRI_BASEC, ACHG_G, AC1_AMT, AC1_C, AC2_AMT' +
        ', LOC_TYPE, NAMESS, SIZESS,'
      'APP_DATE, ITM_NUM, COMPANY_CD FROM APPPCRD1'
      'WHERE KEYY = @OriginalKey'
      ''
      'INSERT INTO APPPCRD2'
      
        'SELECT @NewKey, SEQ, DOC_G, DOC_D, BHS_NO, BNAME, BNAME1, BAMT, ' +
        'BAMTC, EXP_DATE, SHIP_DATE, BSIZE1, BAL_NAME1, BAL_NAME2, BAL_CD' +
        ', NAT FROM APPPCRD2'
      'WHERE KEYY = @OriginalKey'
      ''
      'INSERT INTO APPPCR_TAX'
      'SELECT @NewKey, SEQ, DATEE, BILL_NO, BILL_DATE,'
      
        'ITEM_NAME1, ITEM_NAME2, ITEM_NAME3, ITEM_NAME4, ITEM_NAME5, ITEM' +
        '_DESC, BILL_AMOUNT, BILL_AMOUNT_UNIT,'
      
        'TAX_AMOUNT, TAX_AMOUNT_UNIT, QUANTITY, QUANTITY_UNIT FROM APPPCR' +
        '_TAX'
      'WHERE KEYY = @OriginalKey')
    Left = 8
    Top = 320
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = A4
    Left = 8
    Top = 352
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPPCR_H '
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 40
    Top = 320
  end
  object qryCalcTotal: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT KEYY,'
      '       MAX(QTYC) as QTYC,'
      '       SUM(QTY) as TQTY,'
      '       MAX(AMT1C) as AMT1C,'
      '       SUM(AMT1) as TAMT1'
      '  FROM [APPPCRD1] with(nolock)'
      '  GROUP BY KEYY'
      '  HAVING [KEYY] = :KEY')
    Left = 16
    Top = 384
  end
end
