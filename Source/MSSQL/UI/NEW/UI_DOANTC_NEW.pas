unit UI_DOANTC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sCheckBox, sMemo, sComboBox,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids, acDBGrid,
  sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, DB, ADODB, StrUtils, DateUtils;

type
  TUI_DOANTC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    mask_RESDATE: TsMaskEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo5: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    sPanel2: TsPanel;
    sPanel59: TsPanel;
    com_Carriage: TsComboBox;
    sPanel89: TsPanel;
    edt_BANKCODE: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBANK_CODE: TStringField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListAMT: TBCDField;
    qryListAMTC: TStringField;
    qryListCHRG: TBCDField;
    qryListCHRGC: TStringField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListREMARK1: TMemoField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    mask_SETDATE: TsMaskEdit;
    edt_BANK1: TsEdit;
    edt_BANK2: TsEdit;
    sPanel1: TsPanel;
    edt_LCG: TsEdit;
    edt_BLG: TsEdit;
    edt_LCNO: TsEdit;
    edt_BLNO: TsEdit;
    edt_AMTC: TsEdit;
    edt_CHRGC: TsEdit;
    curr_AMT: TsCurrencyEdit;
    curr_CHRG: TsCurrencyEdit;
    sPanel8: TsPanel;
    edt_APPCODE: TsEdit;
    edt_APPNAME1: TsEdit;
    edt_APPNAME2: TsEdit;
    edt_APPNAME3: TsEdit;
    sPanel9: TsPanel;
    edt_BKNAME1: TsEdit;
    edt_BKNAME2: TsEdit;
    edt_BKNAME3: TsEdit;
    sPanel10: TsPanel;
    memo_REMARK1: TsMemo;
    sPanel6: TsPanel;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sSpeedButton1: TsSpeedButton;
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure InitData;
    procedure DeleteDocument;
  protected
    procedure ReadData; override;
  public
    procedure ReadList(SortField: TColumn=nil);  
    { Public declarations }
  end;

var
  UI_DOANTC_NEW_frm: TUI_DOANTC_NEW_frm;

implementation

{$R *.dfm}
uses
  MSSQL, TypeDefine, MessageDefine, DOANTC_PRINT, Preview;

procedure TUI_DOANTC_NEW_frm.InitData;
begin
  ClearControl(sPanel7);
end;

procedure TUI_DOANTC_NEW_frm.ReadData;
begin
  IF not qryList.Active Then Exit;

  //------------------------------------------------------------------------------
  // 헤더
  //------------------------------------------------------------------------------
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;
  //문서기능
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString, 2);
  //----------------------------------------------------------------------------
  //통지은행
  //----------------------------------------------------------------------------
  //통지은행
  edt_BANKCODE.Text := qryListBANK_CODE.AsString;
  edt_BANK1.Text := qryListBANK1.AsString;
  edt_BANK2.Text := qryListBANK2.AsString;
  //통지일자
  mask_RESDATE.Text := qryListRES_DATE.AsString;
  //최종결제일
  mask_SETDATE.Text := qryListSET_DATE.AsString;

  //----------------------------------------------------------------------------
  //신용장,선하증권
  //----------------------------------------------------------------------------
    //신용장 구분 및 번호
    edt_LCG.Text := qryListLC_G.AsString;
    edt_LCNO.Text := qryListLC_NO.AsString;
    //선하증건 구분 및 번호
    edt_BLG.Text := qryListBL_G.AsString;
    edt_BLNO.Text := qryListBL_NO.AsString;
    //어음금액
    edt_AMTC.Text := qryListAMTC.AsString;
    curr_AMT.Value := qryListAMT.AsCurrency;
    //기타수수료
    edt_CHRGC.Text := qryListCHRGC.AsString;
    curr_CHRG.Value := qryListCHRG.AsCurrency;
  //----------------------------------------------------------------------------
  //수신업체
  //---------------------------------------------------------------------------
    edt_APPCODE.Text := qryListAPP_CODE.AsString;
    edt_APPNAME1.Text := qryListAPP_NAME1.AsString;
    edt_APPNAME2.Text := qryListAPP_NAME2.AsString;
    edt_APPNAME3.Text := qryListAPP_NAME3.AsString;
  //----------------------------------------------------------------------------
  //발신기관
  //---------------------------------------------------------------------------
    edt_BKNAME1.Text := qryListBK_NAME1.AsString;
    edt_BKNAME2.Text := qryListBK_NAME2.AsString;
    edt_BKNAME3.Text := qryListBK_NAME3.AsString;
  //---------------------------------------------------------------------------
  //기타조건
  //-------------------------------------------------------------------------
    memo_REMARK1.Lines.Text := qryListREMARK1.AsString;  

end;

procedure TUI_DOANTC_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAMR.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_DOANTC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_DOANTC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);
//  EnableControl(sPanel7, False);
  sPageControl1.ActivePageIndex := 0;
  ReadOnlyControl(spanel7,false);
end;

procedure TUI_DOANTC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_DOANTC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_DOANTC_NEW_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_DOANTC_NEW_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'선적서류도착통보서'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM DOANTC WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end
      else
      begin
        if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;
end;

procedure TUI_DOANTC_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  Preview_frm := TPreview_frm.Create(Self);
  DOANTC_PRINT_frm := TDOANTC_PRINT_frm.Create(Self);
  try
    DOANTC_PRINT_frm.MaintNo := edt_MAINT_NO.Text;  
//    DOANTC_PRINT_frm.PrintDocument(qryINF700.Fields);
    Preview_frm.Report := DOANTC_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(DOANTC_PRINT_frm);
  end;
end;

procedure TUI_DOANTC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOANTC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_DOANTC_NEW_frm := nil;
end;

end.

