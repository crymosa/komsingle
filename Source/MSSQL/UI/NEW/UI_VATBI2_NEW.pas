unit UI_VATBI2_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, DBCtrls, sDBEdit, StdCtrls, sComboBox,
  ComCtrls, sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids,
  DBGrids, acDBGrid, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel,
  sSkinProvider, sDBMemo, sMemo, DateUtils;

type
  TUI_VATBI2_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    edt_AdInfo5: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    sPanel59: TsPanel;
    com_Carriage: TsComboBox;
    sPanel89: TsPanel;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sDBEdit1: TsDBEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    Shape1: TShape;
    sDBEdit7: TsDBEdit;
    sLabel1: TsLabel;
    sDBEdit8: TsDBEdit;
    sLabel2: TsLabel;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    Shape2: TShape;
    sDBEdit12: TsDBEdit;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRE_NO: TStringField;
    qryListSE_NO: TStringField;
    qryListFS_NO: TStringField;
    qryListACE_NO: TStringField;
    qryListRFF_NO: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_SAUP: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_ADDR1: TStringField;
    qryListSE_ADDR2: TStringField;
    qryListSE_ADDR3: TStringField;
    qryListSE_UPTA: TStringField;
    qryListSE_UPTA1: TMemoField;
    qryListSE_ITEM: TStringField;
    qryListSE_ITEM1: TMemoField;
    qryListBY_CODE: TStringField;
    qryListBY_SAUP: TStringField;
    qryListBY_NAME1: TStringField;
    qryListBY_NAME2: TStringField;
    qryListBY_ADDR1: TStringField;
    qryListBY_ADDR2: TStringField;
    qryListBY_ADDR3: TStringField;
    qryListBY_UPTA: TStringField;
    qryListBY_UPTA1: TMemoField;
    qryListBY_ITEM: TStringField;
    qryListBY_ITEM1: TMemoField;
    qryListAG_CODE: TStringField;
    qryListAG_SAUP: TStringField;
    qryListAG_NAME1: TStringField;
    qryListAG_NAME2: TStringField;
    qryListAG_NAME3: TStringField;
    qryListAG_ADDR1: TStringField;
    qryListAG_ADDR2: TStringField;
    qryListAG_ADDR3: TStringField;
    qryListAG_UPTA: TStringField;
    qryListAG_UPTA1: TMemoField;
    qryListAG_ITEM: TStringField;
    qryListAG_ITEM1: TMemoField;
    qryListDRAW_DAT: TStringField;
    qryListDETAILNO: TBCDField;
    qryListSUP_AMT: TBCDField;
    qryListTAX_AMT: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAMT11: TBCDField;
    qryListAMT11C: TStringField;
    qryListAMT12: TBCDField;
    qryListAMT21: TBCDField;
    qryListAMT21C: TStringField;
    qryListAMT22: TBCDField;
    qryListAMT31: TBCDField;
    qryListAMT31C: TStringField;
    qryListAMT32: TBCDField;
    qryListAMT41: TBCDField;
    qryListAMT41C: TStringField;
    qryListAMT42: TBCDField;
    qryListINDICATOR: TStringField;
    qryListTAMT: TBCDField;
    qryListSUPTAMT: TBCDField;
    qryListTAXTAMT: TBCDField;
    qryListUSTAMT: TBCDField;
    qryListUSTAMTC: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListVAT_CODE: TStringField;
    qryListVAT_CODE_NM: TStringField;
    qryListVAT_TYPE: TStringField;
    qryListVAT_TYPE_NM: TStringField;
    qryListNEW_INDICATOR: TStringField;
    qryListNEW_INDICATOR_NM: TStringField;
    qryListSE_ADDR4: TStringField;
    qryListSE_ADDR5: TStringField;
    qryListSE_SAUP1: TStringField;
    qryListSE_SAUP2: TStringField;
    qryListSE_SAUP3: TStringField;
    qryListSE_FTX1: TStringField;
    qryListSE_FTX2: TStringField;
    qryListSE_FTX3: TStringField;
    qryListSE_FTX4: TStringField;
    qryListSE_FTX5: TStringField;
    qryListBY_SAUP_CODE: TStringField;
    qryListBY_ADDR4: TStringField;
    qryListBY_ADDR5: TStringField;
    qryListBY_SAUP1: TStringField;
    qryListBY_SAUP2: TStringField;
    qryListBY_SAUP3: TStringField;
    qryListBY_FTX1: TStringField;
    qryListBY_FTX2: TStringField;
    qryListBY_FTX3: TStringField;
    qryListBY_FTX4: TStringField;
    qryListBY_FTX5: TStringField;
    qryListBY_FTX1_1: TStringField;
    qryListBY_FTX2_1: TStringField;
    qryListBY_FTX3_1: TStringField;
    qryListBY_FTX4_1: TStringField;
    qryListBY_FTX5_1: TStringField;
    qryListAG_ADDR4: TStringField;
    qryListAG_ADDR5: TStringField;
    qryListAG_SAUP1: TStringField;
    qryListAG_SAUP2: TStringField;
    qryListAG_SAUP3: TStringField;
    qryListAG_FTX1: TStringField;
    qryListAG_FTX2: TStringField;
    qryListAG_FTX3: TStringField;
    qryListAG_FTX4: TStringField;
    qryListAG_FTX5: TStringField;
    qryListSE_NAME3: TStringField;
    qryListBY_NAME3: TStringField;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sMemo1: TsMemo;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit22: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sDBEdit24: TsDBEdit;
    sDBEdit25: TsDBEdit;
    sDBEdit26: TsDBEdit;
    sDBEdit27: TsDBEdit;
    sDBEdit28: TsDBEdit;
    sDBEdit29: TsDBEdit;
    sDBEdit30: TsDBEdit;
    sDBEdit31: TsDBEdit;
    sMemo2: TsMemo;
    sDBMemo3: TsDBMemo;
    sDBMemo4: TsDBMemo;
    sTabSheet2: TsTabSheet;
    sPanel1: TsPanel;
    sDBGrid4: TsDBGrid;
    qryGoods: TADOQuery;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsDE_DATE: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsDE_REM1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsSUPAMT: TBCDField;
    qryGoodsTAXAMT: TBCDField;
    qryGoodsUSAMT: TBCDField;
    qryGoodsUSAMT_G: TStringField;
    qryGoodsSUPSTAMT: TBCDField;
    qryGoodsTAXSTAMT: TBCDField;
    qryGoodsUSSTAMT: TBCDField;
    qryGoodsUSSTAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsRATE: TBCDField;
    dsGoods: TDataSource;
    sPanel2: TsPanel;
    sPanel8: TsPanel;
    sPanel23: TsPanel;
    sDBEdit32: TsDBEdit;
    sDBMemo5: TsDBMemo;
    sDBMemo6: TsDBMemo;
    sPanel21: TsPanel;
    sPanel28: TsPanel;
    sDBEdit34: TsDBEdit;
    sSpeedButton11: TsSpeedButton;
    sDBEdit35: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sDBEdit39: TsDBEdit;
    sDBEdit40: TsDBEdit;
    sDBEdit33: TsDBEdit;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    Shape3: TShape;
    sDBEdit47: TsDBEdit;
    sDBEdit48: TsDBEdit;
    sDBEdit49: TsDBEdit;
    sDBEdit50: TsDBEdit;
    sTabSheet3: TsTabSheet;
    sPanel11: TsPanel;
    sEdit1: TsEdit;
    sEdit2: TsEdit;
    sEdit3: TsEdit;
    sPanel12: TsPanel;
    sComboBox1: TsComboBox;
    sPanel13: TsPanel;
    sDBEdit51: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit53: TsDBEdit;
    sDBEdit54: TsDBEdit;
    Shape4: TShape;
    sDBMemo7: TsDBMemo;
    Shape5: TShape;
    sDBEdit55: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit63: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    sDBEdit66: TsDBEdit;
    sDBEdit67: TsDBEdit;
    sDBEdit68: TsDBEdit;
    sDBEdit69: TsDBEdit;
    sDBEdit70: TsDBEdit;
    sDBEdit71: TsDBEdit;
    sDBEdit72: TsDBEdit;
    sDBEdit73: TsDBEdit;
    sDBEdit74: TsDBEdit;
    sDBEdit75: TsDBEdit;
    sTabSheet4: TsTabSheet;
    sPanel14: TsPanel;
    sDBEdit76: TsDBEdit;
    sDBEdit77: TsDBEdit;
    sDBEdit78: TsDBEdit;
    sDBEdit79: TsDBEdit;
    sDBEdit80: TsDBEdit;
    sDBEdit81: TsDBEdit;
    sDBEdit82: TsDBEdit;
    sDBEdit83: TsDBEdit;
    sDBEdit84: TsDBEdit;
    sDBEdit85: TsDBEdit;
    sMemo3: TsMemo;
    sDBMemo8: TsDBMemo;
    sDBMemo9: TsDBMemo;
    qryListINDICATOR_NM: TStringField;
    Shape6: TShape;
    procedure qryGoodsNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
    procedure ReadDetail;
  protected
    procedure ReadData; override;
  public
    { Public declarations }
  end;

var
  UI_VATBI2_NEW_frm: TUI_VATBI2_NEW_frm;

implementation

uses
  MSSQL, ICON, MessageDefine, Preview, VATBI2_PRINT;

{$R *.dfm}

procedure TUI_VATBI2_NEW_frm.qryGoodsNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
end;

procedure TUI_VATBI2_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_VATBI2_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_VATBI2_NEW_frm := nil;
end;

procedure TUI_VATBI2_NEW_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    Open;
  end;
end;

procedure TUI_VATBI2_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_VATBI2_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_VATBI2_NEW_frm.ReadDetail;
begin
  IF sPageControl1.ActivePageIndex = 1 Then
  begin
    with qryGoods do
    begin
      Close;
      Parameters[0].Value := qryListMAINT_NO.AsString;
      Open;
    end;
  end;
end;

procedure TUI_VATBI2_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadDetail;
end;

procedure TUI_VATBI2_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_VATBI2_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  IF sPageControl1.ActivePageIndex = 1 Then ReadDetail;
end;

procedure TUI_VATBI2_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);

  sPageControl1.ActivePageIndex := 0;   
  ReadOnlyControl(spanel7,false);
  ReadOnlyControl(spanel2,false);
  ReadOnlyControl(spanel11,false);
  ReadOnlyControl(spanel14,false);
end;

procedure TUI_VATBI2_NEW_frm.ReadData;
var
  TMP_STR : String;
begin
  inherited;
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString, 0);

  //공급자의 담당자
  TMP_STR := '';
  IF Trim(qryListSE_FTX1.AsString) <> '' Then
    TMP_STR := qryListSE_FTX1.AsString;
  IF Trim(qryListSE_FTX2.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListSE_FTX2.AsString;
  IF Trim(qryListSE_FTX3.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListSE_FTX3.AsString;
  IF Trim(qryListSE_FTX4.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListSE_FTX4.AsString+'@'+qryListSE_FTX5.AsString;
  sMemo1.Text := TMP_STR;

  //공급받는자의 담당자
  TMP_STR := '';
  IF Trim(qryListBY_FTX1.AsString) <> '' Then
    TMP_STR := qryListBY_FTX1.AsString;
  IF Trim(qryListBY_FTX2.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListBY_FTX2.AsString;
  IF Trim(qryListBY_FTX3.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListBY_FTX3.AsString;
  IF Trim(qryListBY_FTX4.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListBY_FTX4.AsString+'@'+qryListBY_FTX5.AsString;
  sMemo2.Text := TMP_STR;

  //수탁자의 담당자
  TMP_STR := '';
  IF Trim(qryListAG_FTX1.AsString) <> '' Then
    TMP_STR := qryListAG_FTX1.AsString;
  IF Trim(qryListAG_FTX2.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListAG_FTX2.AsString;
  IF Trim(qryListAG_FTX3.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListAG_FTX3.AsString;
  IF Trim(qryListAG_FTX4.AsString) <> '' Then
    TMP_STR := TMP_STR + #13#10 + qryListAG_FTX4.AsString+'@'+qryListAG_FTX5.AsString;
  sMemo3.Text := TMP_STR;
end;

procedure TUI_VATBI2_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : Integer;
begin
//  inherited;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
    try
      try
       IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'세금계산서'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
       begin
         nCursor := qryList.RecNo;

         SQL.Text :=  'DELETE FROM VATBI2_H WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM VATBI2_D WHERE KEYY =' + QuotedStr(maint_no);
         ExecSQL;

         //트랜잭션 커밋
         DMMssql.KISConnect.CommitTrans;

         qryList.Close;
         qryList.Open;

         if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
         begin
           qryList.MoveBy(nCursor-1);
         end
         else
           qryList.First;
       end
       else
         DMMssql.KISConnect.RollbackTrans;

      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;

    finally
      if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
     Close;
     Free;
    end;
  end;
end;

procedure TUI_VATBI2_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  VATBI2_PRINT_frm := TVATBI2_PRINT_frm.Create(Self);
  try
    VATBI2_PRINT_frm.MaintNo := edt_MAINT_NO.Text;

    Preview_frm.Report := VATBI2_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(VATBI2_PRINT_frm);
  end;

end;

end.
