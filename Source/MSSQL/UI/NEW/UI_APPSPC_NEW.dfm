inherited UI_APPSPC_NEW_frm: TUI_APPSPC_NEW_frm
  Left = 293
  Top = 187
  BorderWidth = 4
  Caption = '[APPSPC] '#54032#47588#45824#44552#52628#49900'('#47588#51077') '#51032#47280#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object sPanel3: TsPanel
      Left = 316
      Top = 1
      Width = 797
      Height = 630
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alClient
      
      TabOrder = 0
      object sPanel20: TsPanel
        Left = 472
        Top = 59
        Width = 321
        Height = 24
        SkinData.SkinSection = 'TRANSPARENT'
        
        TabOrder = 0
      end
      object sPageControl1: TsPageControl
        Left = 1
        Top = 56
        Width = 795
        Height = 573
        ActivePage = sTabSheet1
        Align = alClient
        TabHeight = 26
        TabOrder = 1
        TabPadding = 15
        object sTabSheet1: TsTabSheet
          Caption = #44277#53685#49324#54637
          object sPanel7: TsPanel
            Left = 0
            Top = 0
            Width = 787
            Height = 537
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alClient
            
            TabOrder = 0
            object sPanel32: TsPanel
              Left = 1
              Top = 1
              Width = 785
              Height = 130
              Align = alTop
              
              TabOrder = 0
              object edt_BGM1Label: TsEditLabel
                Left = 147
                Top = 105
                Width = 107
                Height = 15
                Alignment = taRightJustify
                Caption = #52628#49900'('#49888#52397'),'#47588#51077#48264#54840
                Enabled = False
                FocusControl = edt_BGM1
                ParentFont = False
                WordWrap = True
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                UseHTML = False
              end
              object com_BGM_GUBUN: TsComboBox
                Left = 258
                Top = 54
                Width = 269
                Height = 23
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#47928#49436#44396#48516
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
                Style = csDropDownList
                Ctl3D = True
                Enabled = False
                ItemHeight = 17
                ItemIndex = 0
                ParentCtl3D = False
                TabOrder = 0
                Text = '2BK  :  '#52628#49900#49888#52397
                OnChange = com_BGM_GUBUNChange
                Items.Strings = (
                  '2BK  :  '#52628#49900#49888#52397
                  '2BJ  :  '#47588#51077#49888#52397'  ')
              end
              object edt_BGM3: TsEdit
                Tag = 102
                Left = 415
                Top = 78
                Width = 21
                Height = 23
                CharCase = ecUpperCase
                Color = 12775866
                Ctl3D = True
                Enabled = False
                ParentCtl3D = False
                TabOrder = 3
                OnDblClick = CODEDblClick
                SkinData.CustomColor = True
                BoundLabel.Caption = #12641
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.ParentFont = False
              end
              object edt_BGM2: TsEdit
                Tag = 101
                Left = 393
                Top = 78
                Width = 21
                Height = 23
                CharCase = ecUpperCase
                Color = 12775866
                Ctl3D = True
                Enabled = False
                ParentCtl3D = False
                TabOrder = 2
                OnDblClick = CODEDblClick
                OnExit = CODEExit
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = '-'
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.ParentFont = False
              end
              object edt_BGM4: TsEdit
                Left = 437
                Top = 78
                Width = 90
                Height = 23
                Ctl3D = True
                Enabled = False
                MaxLength = 35
                ParentCtl3D = False
                TabOrder = 4
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #52628#49900#51008#54665#53076#46300' - '#44060#49444#51008#54665#53076#46300
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclRight
                BoundLabel.ParentFont = False
              end
              object edt_BGM1: TsEdit
                Left = 258
                Top = 78
                Width = 119
                Height = 23
                Color = clWhite
                Ctl3D = True
                Enabled = False
                MaxLength = 35
                ParentCtl3D = False
                TabOrder = 1
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #47928#49436#48264#54840
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.ParentFont = False
              end
              object edt_CP_NO: TsEdit
                Left = 345
                Top = 102
                Width = 182
                Height = 23
                Color = clWhite
                Ctl3D = True
                Enabled = False
                MaxLength = 35
                ParentCtl3D = False
                TabOrder = 5
                SkinData.CustomColor = True
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
              end
              object sPanel9: TsPanel
                Left = 1
                Top = 1
                Width = 783
                Height = 23
                SkinData.ColorTone = clGradientInactiveCaption
                Align = alTop
                Caption = #44277#53685#49324#54637
                Color = clBtnText
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
              end
              object com_CP_CODE: TsComboBox
                Left = 258
                Top = 102
                Width = 86
                Height = 23
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
                Style = csDropDownList
                Ctl3D = True
                Enabled = False
                ItemHeight = 17
                ItemIndex = 0
                ParentCtl3D = False
                TabOrder = 7
                Text = #52628#49900#48264#54840
                Items.Strings = (
                  #52628#49900#48264#54840
                  #47588#51077#48264#54840)
              end
              object msk_APP_DATE: TsMaskEdit
                Left = 258
                Top = 30
                Width = 87
                Height = 23
                AutoSize = False
                
                Enabled = False
                MaxLength = 10
                TabOrder = 8
                BoundLabel.Active = True
                BoundLabel.Caption = #49888#52397#51068#51088
                CheckOnExit = True
                EditMask = '9999-99-99;0'
                Text = '20190124'
              end
            end
            object sPanel44: TsPanel
              Left = 1
              Top = 131
              Width = 785
              Height = 210
              Align = alTop
              
              TabOrder = 1
              object sPanel45: TsPanel
                Left = 1
                Top = 1
                Width = 392
                Height = 208
                Align = alLeft
                
                TabOrder = 0
                object sPanel47: TsPanel
                  Left = 1
                  Top = 1
                  Width = 390
                  Height = 206
                  Align = alClient
                  
                  TabOrder = 0
                  object edt_APP_CODE: TsEdit
                    Tag = 103
                    Left = 95
                    Top = 31
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Enabled = False
                    TabOrder = 0
                    OnDblClick = CODEDblClick
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #49888#52397#51064' '#49345#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_SNAME: TsEdit
                    Left = 146
                    Top = 31
                    Width = 209
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 1
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #49345#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_NAME: TsEdit
                    Left = 95
                    Top = 79
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 3
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #45824#54364#51088
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_ADDR1: TsEdit
                    Left = 95
                    Top = 103
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 4
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #51452#49548
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_ADDR3: TsEdit
                    Left = 95
                    Top = 151
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 6
                    SkinData.CustomColor = True
                    BoundLabel.Caption = 'edt_pubaddr2'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_ADDR2: TsEdit
                    Left = 95
                    Top = 127
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 5
                    SkinData.CustomColor = True
                    BoundLabel.Caption = 'edt_pubaddr3'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_APP_ELEC: TsEdit
                    Left = 95
                    Top = 175
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 7
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #51204#51088#49436#47749
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object sPanel12: TsPanel
                    Left = 1
                    Top = 1
                    Width = 388
                    Height = 23
                    SkinData.ColorTone = clGradientInactiveCaption
                    Align = alTop
                    Caption = #49888#52397#51064
                    Color = clBtnText
                    
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = #47569#51008' '#44256#46357
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 8
                  end
                  object msk_APP_SAUPNO: TsMaskEdit
                    Left = 95
                    Top = 55
                    Width = 93
                    Height = 23
                    Ctl3D = True
                    
                    Enabled = False
                    MaxLength = 12
                    ParentCtl3D = False
                    TabOrder = 2
                    BoundLabel.Active = True
                    BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                    CheckOnExit = True
                    EditMask = '999-99-99999;0;'
                  end
                end
              end
              object sPanel48: TsPanel
                Left = 393
                Top = 1
                Width = 392
                Height = 208
                Align = alLeft
                
                TabOrder = 1
                object sPanel50: TsPanel
                  Left = 1
                  Top = 1
                  Width = 390
                  Height = 206
                  Align = alClient
                  
                  TabOrder = 0
                  object edt_CP_BANK: TsEdit
                    Tag = 104
                    Left = 95
                    Top = 31
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 4
                    ParentCtl3D = False
                    TabOrder = 0
                    OnDblClick = CODEDblClick
                    OnExit = edt_CP_BANKExit
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #51008#54665#53076#46300
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_BANKNAME: TsEdit
                    Left = 146
                    Top = 31
                    Width = 209
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 1
                    SkinData.CustomColor = True
                    BoundLabel.Caption = #51008#54665#47749
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_BANKBU: TsEdit
                    Left = 95
                    Top = 55
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 2
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #51648#51216#47749
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ACCOUNTNO: TsEdit
                    Left = 95
                    Top = 79
                    Width = 152
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 3
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#48264#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_CURR: TsEdit
                    Tag = 105
                    Left = 305
                    Top = 79
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Ctl3D = True
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 4
                    OnDblClick = CODEDblClick
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#53685#54868
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_NAME1: TsEdit
                    Left = 95
                    Top = 103
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 5
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#51452
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_NAME2: TsEdit
                    Left = 95
                    Top = 127
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 6
                    SkinData.CustomColor = True
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_AMT: TsCurrencyEdit
                    Left = 95
                    Top = 151
                    Width = 141
                    Height = 23
                    AutoSelect = False
                    CharCase = ecUpperCase
                    Ctl3D = True
                    
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 7
                    BoundLabel.Active = True
                    BoundLabel.Caption = #52628#49900#44552#50529'('#50896#54868')'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                    SkinData.SkinSection = 'EDIT'
                    DecimalPlaces = 4
                    DisplayFormat = '###,###,##0.00;'
                  end
                  object edt_CP_AMTC: TsEdit
                    Tag = 106
                    Left = 95
                    Top = 175
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Ctl3D = True
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 9
                    OnDblClick = CODEDblClick
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #52628#49900#44552#50529'('#50808#54868')'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_AMTU: TsCurrencyEdit
                    Left = 146
                    Top = 175
                    Width = 209
                    Height = 23
                    AutoSelect = False
                    CharCase = ecUpperCase
                    Ctl3D = True
                    
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 10
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                    SkinData.SkinSection = 'EDIT'
                    DecimalPlaces = 4
                    DisplayFormat = '###,###,##0.00;'
                  end
                  object sPanel13: TsPanel
                    Left = 1
                    Top = 1
                    Width = 388
                    Height = 23
                    SkinData.ColorTone = clGradientInactiveCaption
                    Align = alTop
                    Caption = #52628#49900'('#47588#51077')'#51008#54665
                    Color = clBtnText
                    
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = #47569#51008' '#44256#46357
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 11
                  end
                  object edt_CP_CUX: TsCurrencyEdit
                    Left = 295
                    Top = 151
                    Width = 60
                    Height = 23
                    AutoSelect = False
                    CharCase = ecUpperCase
                    Ctl3D = True
                    
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 8
                    BoundLabel.Active = True
                    BoundLabel.Caption = #51201#50857#54872#50984
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                    SkinData.SkinSection = 'EDIT'
                    DecimalPlaces = 4
                    DisplayFormat = '###,###,##0.00;'
                  end
                end
              end
            end
            object sPanel1: TsPanel
              Left = 1
              Top = 341
              Width = 785
              Height = 195
              Align = alTop
              Caption = 'sPanel1'
              
              TabOrder = 2
              object sPanel8: TsPanel
                Left = 1
                Top = 1
                Width = 392
                Height = 193
                Align = alLeft
                
                TabOrder = 0
                object sPanel39: TsPanel
                  Left = 1
                  Top = 1
                  Width = 390
                  Height = 191
                  Align = alClient
                  
                  TabOrder = 0
                  object msk_DF_SAUPNO: TsMaskEdit
                    Left = 95
                    Top = 31
                    Width = 91
                    Height = 23
                    Ctl3D = True
                    
                    Enabled = False
                    MaxLength = 12
                    ParentCtl3D = False
                    TabOrder = 0
                    BoundLabel.Active = True
                    BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                    CheckOnExit = True
                    EditMask = '999-99-99999;0;'
                  end
                  object edt_DF_NAME1: TsEdit
                    Left = 95
                    Top = 55
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 1
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44396#47588#51088' '#49345#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_DF_NAME2: TsEdit
                    Left = 95
                    Top = 79
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 2
                    SkinData.CustomColor = True
                    BoundLabel.Caption = 'edt_pubaddr2'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_DF_NAME3: TsEdit
                    Left = 95
                    Top = 103
                    Width = 260
                    Height = 23
                    Color = clWhite
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 3
                    SkinData.CustomColor = True
                    BoundLabel.Caption = 'edt_pubaddr3'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_DF_EMAIL1: TsEdit
                    Left = 95
                    Top = 127
                    Width = 64
                    Height = 23
                    Color = clWhite
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 4
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = 'Email'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_DF_EMAIL2: TsEdit
                    Left = 178
                    Top = 127
                    Width = 177
                    Height = 23
                    Color = clWhite
                    Enabled = False
                    MaxLength = 35
                    TabOrder = 5
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = '@'
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object sPanel10: TsPanel
                    Left = 1
                    Top = 1
                    Width = 388
                    Height = 23
                    SkinData.ColorTone = clGradientInactiveCaption
                    Align = alTop
                    Caption = #44396#47588#51088
                    Color = clBtnText
                    
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = #47569#51008' '#44256#46357
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 6
                  end
                end
              end
              object sPanel35: TsPanel
                Left = 393
                Top = 1
                Width = 392
                Height = 193
                Align = alLeft
                
                TabOrder = 1
                object sPanel40: TsPanel
                  Left = 1
                  Top = 1
                  Width = 390
                  Height = 191
                  Align = alClient
                  
                  TabOrder = 0
                  object sBevel1: TsBevel
                    Left = 13
                    Top = 106
                    Width = 366
                    Height = 1
                    Shape = bsBottomLine
                  end
                  object edt_CP_ADD_ACCOUNTNO1: TsEdit
                    Left = 95
                    Top = 31
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 0
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#48264#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ADD_NAME1: TsEdit
                    Left = 95
                    Top = 55
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 1
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#51452
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ADD_CURR1: TsEdit
                    Tag = 107
                    Left = 95
                    Top = 79
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Ctl3D = True
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 2
                    OnDblClick = CODEDblClick
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#53685#54868
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ADD_ACCOUNTNO2: TsEdit
                    Left = 95
                    Top = 113
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 3
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#48264#54840
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ADD_NAME2: TsEdit
                    Left = 95
                    Top = 137
                    Width = 260
                    Height = 23
                    Ctl3D = True
                    Enabled = False
                    MaxLength = 35
                    ParentCtl3D = False
                    TabOrder = 4
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#51452
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = []
                    BoundLabel.ParentFont = False
                  end
                  object edt_CP_ADD_CURR2: TsEdit
                    Tag = 108
                    Left = 95
                    Top = 161
                    Width = 50
                    Height = 23
                    CharCase = ecUpperCase
                    Color = 12775866
                    Ctl3D = True
                    Enabled = False
                    ParentCtl3D = False
                    TabOrder = 5
                    OnDblClick = CODEDblClick
                    SkinData.CustomColor = True
                    BoundLabel.Active = True
                    BoundLabel.Caption = #44228#51340#53685#54868
                    BoundLabel.Font.Charset = DEFAULT_CHARSET
                    BoundLabel.Font.Color = clWindowText
                    BoundLabel.Font.Height = -12
                    BoundLabel.Font.Name = #47569#51008' '#44256#46357
                    BoundLabel.Font.Style = [fsBold]
                    BoundLabel.ParentFont = False
                  end
                  object sPanel14: TsPanel
                    Left = 1
                    Top = 1
                    Width = 388
                    Height = 23
                    SkinData.ColorTone = clGradientInactiveCaption
                    Align = alTop
                    Caption = #52628#44032#44228#51340
                    Color = clBtnText
                    
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = #47569#51008' '#44256#46357
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 6
                  end
                end
              end
            end
          end
        end
        object sTabSheet9: TsTabSheet
          Caption = #47928#49436#47785#47197
          object sPanel38: TsPanel
            Left = 0
            Top = 0
            Width = 787
            Height = 537
            Align = alClient
            
            TabOrder = 0
            object sPanel55: TsPanel
              Left = 1
              Top = 403
              Width = 785
              Height = 134
              Align = alTop
              
              TabOrder = 0
              object sPanel56: TsPanel
                Left = 1
                Top = 1
                Width = 783
                Height = 23
                SkinData.ColorTone = clGradientInactiveCaption
                Align = alTop
                Caption = #49345#50629#49569#51109' : 0'#44148
                Color = clBtnText
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object sPanel57: TsPanel
                Left = 1
                Top = 24
                Width = 783
                Height = 109
                Align = alClient
                
                TabOrder = 1
                object sDBGrid8: TsDBGrid
                  Left = 1
                  Top = 1
                  Width = 702
                  Height = 107
                  Align = alLeft
                  Color = clWhite
                  DataSource = dsListD1_1BW
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clBlack
                  Font.Height = -12
                  Font.Name = #47569#51008' '#44256#46357
                  Font.Style = []
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = #47569#51008' '#44256#46357
                  TitleFont.Style = []
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'DOC_NO'
                      Title.Alignment = taCenter
                      Title.Caption = #47928#49436#48264#54840
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_DATE'
                      Title.Alignment = taCenter
                      Title.Caption = #48156#44553#51068#51088
                      Width = 80
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'SE_SNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #49688#52636#51088
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'BY_SNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #49688#51077#51088
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_TOTAMT'
                      Title.Alignment = taCenter
                      Title.Caption = #52509#44552#50529
                      Width = 91
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'TOTCNT'
                      Title.Alignment = taCenter
                      Title.Caption = #52509#49688#47049
                      Width = 91
                      Visible = True
                    end>
                end
                object sPanel41: TsPanel
                  Left = 701
                  Top = 1
                  Width = 81
                  Height = 107
                  Align = alRight
                  
                  TabOrder = 1
                  object btn1BW_EDT: TsBitBtn
                    Tag = 3
                    Left = 0
                    Top = 35
                    Width = 81
                    Height = 37
                    Caption = #49688#51221
                    Enabled = False
                    TabOrder = 1
                    OnClick = DOCEDT
                    ImageIndex = 3
                    Images = DMICON.System18
                  end
                  object btn1BW_DEL: TsBitBtn
                    Tag = 3
                    Left = 0
                    Top = 71
                    Width = 81
                    Height = 37
                    Caption = #49325#51228
                    Enabled = False
                    TabOrder = 2
                    OnClick = DOCDEL
                    ImageIndex = 1
                    Images = DMICON.System18
                  end
                  object btn1BW_ADD: TsBitBtn
                    Tag = 3
                    Left = 0
                    Top = -1
                    Width = 81
                    Height = 37
                    Caption = #51077#47141
                    Enabled = False
                    TabOrder = 0
                    OnClick = DOCADD
                    ImageIndex = 2
                    Images = DMICON.System18
                  end
                end
              end
            end
            object sPanel59: TsPanel
              Left = 1
              Top = 135
              Width = 785
              Height = 134
              Align = alTop
              
              TabOrder = 1
              object sPanel60: TsPanel
                Left = 1
                Top = 1
                Width = 783
                Height = 23
                SkinData.ColorTone = clGradientInactiveCaption
                Align = alTop
                Caption = #47932#54408#49688#47161#51613#47749#49436' : 0'#44148
                Color = clBtnText
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object sPanel61: TsPanel
                Left = 1
                Top = 24
                Width = 783
                Height = 109
                Align = alClient
                
                TabOrder = 1
                object sDBGrid9: TsDBGrid
                  Left = 1
                  Top = 1
                  Width = 702
                  Height = 107
                  Align = alLeft
                  Color = clWhite
                  DataSource = dsListD1_2AH
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clBlack
                  Font.Height = -12
                  Font.Name = #47569#51008' '#44256#46357
                  Font.Style = []
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = #47569#51008' '#44256#46357
                  TitleFont.Style = []
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'DOC_NO'
                      Title.Alignment = taCenter
                      Title.Caption = #47928#49436#48264#54840
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'LR_NO'
                      Title.Alignment = taCenter
                      Title.Caption = #45236#44397#49888#50857#51109#48264#54840
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'BY_SNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #47932#54408#49688#47161#51064
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'LA_BANKNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #44060#49444#51008#54665
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_AMT'
                      Title.Alignment = taCenter
                      Title.Caption = #44060#49444#44552#50529
                      Width = 100
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_AMTU'
                      Title.Alignment = taCenter
                      Title.Caption = #44060#49444#44552#50529#50808#54868
                      Width = 100
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_DATE'
                      Title.Alignment = taCenter
                      Title.Caption = #48156#44553#51068#51088
                      Width = 80
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ACC_DATE'
                      Title.Alignment = taCenter
                      Title.Caption = #51064#49688#51068#51088
                      Width = 80
                      Visible = True
                    end>
                end
                object sPanel52: TsPanel
                  Left = 701
                  Top = 1
                  Width = 81
                  Height = 107
                  Align = alRight
                  
                  TabOrder = 1
                  object btn2AH_EDT: TsBitBtn
                    Tag = 1
                    Left = 0
                    Top = 35
                    Width = 81
                    Height = 37
                    Caption = #49688#51221
                    Enabled = False
                    TabOrder = 1
                    OnClick = DOCEDT
                    ImageIndex = 3
                    Images = DMICON.System18
                  end
                  object btn2AH_DEL: TsBitBtn
                    Tag = 1
                    Left = 0
                    Top = 71
                    Width = 81
                    Height = 37
                    Caption = #49325#51228
                    Enabled = False
                    TabOrder = 2
                    OnClick = DOCDEL
                    ImageIndex = 1
                    Images = DMICON.System18
                  end
                  object btn2AH_ADD: TsBitBtn
                    Tag = 1
                    Left = 0
                    Top = -1
                    Width = 81
                    Height = 37
                    Caption = #51077#47141
                    Enabled = False
                    TabOrder = 0
                    OnClick = DOCADD
                    ImageIndex = 2
                    Images = DMICON.System18
                  end
                end
              end
            end
            object sPanel63: TsPanel
              Left = 1
              Top = 1
              Width = 785
              Height = 134
              Align = alTop
              
              TabOrder = 2
              object sPanel64: TsPanel
                Left = 1
                Top = 1
                Width = 783
                Height = 23
                SkinData.ColorTone = clGradientInactiveCaption
                Align = alTop
                Caption = #45236#44397#49888#50857#51109
                Color = clBtnText
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
              end
              object edt_DOC_NO: TsEdit
                Tag = 101
                Left = 92
                Top = 37
                Width = 264
                Height = 23
                CharCase = ecUpperCase
                Color = clWhite
                Enabled = False
                TabOrder = 0
                OnDblClick = edt_DOC_NODblClick
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #47928#49436#48264#54840
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.ParentFont = False
              end
              object com_LA_TYPE: TsComboBox
                Left = 485
                Top = 37
                Width = 288
                Height = 23
                BoundLabel.Active = True
                BoundLabel.Caption = #45236#44397#49888#50857#51109' '#51333#47448
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
                Style = csDropDownList
                Enabled = False
                ItemHeight = 17
                ItemIndex = 0
                TabOrder = 1
                Text = '2AA : '#50896#54868#54364#49884' '#50808#54868#48512#44592' '#45236#44397#49888#50857#51109
                Items.Strings = (
                  '2AA : '#50896#54868#54364#49884' '#50808#54868#48512#44592' '#45236#44397#49888#50857#51109
                  '2AB : '#50808#54868#54364#49884' '#45236#44397#49888#50857#51109
                  '2AC : '#50896#54868#54364#49884' '#45236#44397#49888#50857#51109)
              end
              object msk_ISS_DATE: TsMaskEdit
                Left = 92
                Top = 62
                Width = 72
                Height = 23
                Ctl3D = True
                
                Enabled = False
                MaxLength = 10
                ParentCtl3D = False
                TabOrder = 2
                BoundLabel.Active = True
                BoundLabel.Caption = #44060#49444#51068#51088
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
                CheckOnExit = True
                EditMask = '9999-99-99;0;_'
              end
              object edt_LR_NO: TsEdit
                Left = 485
                Top = 62
                Width = 288
                Height = 23
                Enabled = False
                MaxLength = 35
                TabOrder = 3
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #45236#44397#49888#50857#51109' '#48264#54840
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
              end
              object edt_LA_BANKBUCODE: TsEdit
                Tag = 109
                Left = 92
                Top = 87
                Width = 49
                Height = 23
                CharCase = ecUpperCase
                Color = 12775866
                Enabled = False
                TabOrder = 4
                OnDblClick = CODEDblClick
                OnExit = edt_CP_BANKExit
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #44060#49444#51008#54665#53076#46300
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.ParentFont = False
              end
              object edt_LA_BANKNAMEP: TsEdit
                Left = 142
                Top = 87
                Width = 238
                Height = 23
                Enabled = False
                MaxLength = 35
                TabOrder = 5
                SkinData.CustomColor = True
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
              end
              object edt_LA_BANKBUP: TsEdit
                Left = 485
                Top = 87
                Width = 288
                Height = 23
                Enabled = False
                MaxLength = 35
                TabOrder = 6
                SkinData.CustomColor = True
                BoundLabel.Active = True
                BoundLabel.Caption = #44060#49444#51008#54665' '#51648#51216#47749
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -12
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.ParentFont = False
              end
              object btnCpy2AP: TsBitBtn
                Tag = 102
                Left = 357
                Top = 37
                Width = 23
                Height = 23
                Cursor = crHandPoint
                Enabled = False
                TabOrder = 8
                TabStop = False
                OnClick = btnCpy2APClick
                ImageIndex = 25
                Images = DMICON.System18
              end
            end
            object sPanel54: TsPanel
              Left = 1
              Top = 269
              Width = 785
              Height = 134
              Align = alTop
              
              TabOrder = 3
              object sPanel58: TsPanel
                Left = 1
                Top = 1
                Width = 783
                Height = 23
                SkinData.ColorTone = clGradientInactiveCaption
                Align = alTop
                Caption = #49464#44552#44228#49328#49436' : 0'#44148
                Color = clBtnText
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object sPanel62: TsPanel
                Left = 1
                Top = 24
                Width = 783
                Height = 109
                Align = alClient
                
                TabOrder = 1
                object sDBGrid7: TsDBGrid
                  Left = 1
                  Top = 1
                  Width = 702
                  Height = 107
                  Align = alLeft
                  Color = clWhite
                  DataSource = dsListD1_2AJ
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clBlack
                  Font.Height = -12
                  Font.Name = #47569#51008' '#44256#46357
                  Font.Style = []
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = #47569#51008' '#44256#46357
                  TitleFont.Style = []
                  Columns = <
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'DOC_NO'
                      Title.Alignment = taCenter
                      Title.Caption = #47928#49436#48264#54840
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_DATE'
                      Title.Alignment = taCenter
                      Title.Caption = #51089#49457#51068#51088
                      Width = 80
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'SE_SNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #44277#44553#51088
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'BY_SNAME'
                      Title.Alignment = taCenter
                      Title.Caption = #44277#44553#48155#45716#51088
                      Width = 140
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_EXPAMT'
                      Title.Alignment = taCenter
                      Title.Caption = #44277#44553#44032#50529
                      Width = 91
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'ISS_EXPAMTU'
                      Title.Alignment = taCenter
                      Title.Caption = #49464#50529
                      Width = 91
                      Visible = True
                    end>
                end
                object sPanel11: TsPanel
                  Left = 701
                  Top = 1
                  Width = 81
                  Height = 107
                  Align = alRight
                  
                  TabOrder = 1
                  object btn2AJ_EDT: TsBitBtn
                    Tag = 2
                    Left = 0
                    Top = 35
                    Width = 81
                    Height = 37
                    Caption = #49688#51221
                    Enabled = False
                    TabOrder = 1
                    OnClick = DOCEDT
                    ImageIndex = 3
                    Images = DMICON.System18
                  end
                  object btn2AJ_DEL: TsBitBtn
                    Tag = 2
                    Left = 0
                    Top = 71
                    Width = 81
                    Height = 37
                    Caption = #49325#51228
                    Enabled = False
                    TabOrder = 2
                    OnClick = DOCDEL
                    ImageIndex = 1
                    Images = DMICON.System18
                  end
                  object btn2AJ_ADD: TsBitBtn
                    Tag = 2
                    Left = 0
                    Top = -1
                    Width = 81
                    Height = 37
                    Caption = #51077#47141
                    Enabled = False
                    TabOrder = 0
                    OnClick = DOCADD
                    ImageIndex = 2
                    Images = DMICON.System18
                  end
                end
              end
            end
          end
        end
        object sTabSheet4: TsTabSheet
          Caption = #45936#51060#53552#51312#54924
          OnClickBtn = CODEDblClick
          object sDBGrid3: TsDBGrid
            Left = 0
            Top = 32
            Width = 787
            Height = 505
            Align = alClient
            Ctl3D = False
            DataSource = dsList
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.CustomColor = True
            Columns = <
              item
                Alignment = taCenter
                Color = clWhite
                Expanded = False
                FieldName = 'CHK2'
                Title.Alignment = taCenter
                Title.Caption = #49345#54889
                Width = 40
                Visible = True
              end
              item
                Alignment = taCenter
                Color = clWhite
                Expanded = False
                FieldName = 'CHK3'
                Title.Alignment = taCenter
                Title.Caption = #52376#47532
                Width = 75
                Visible = True
              end
              item
                Alignment = taCenter
                Color = clWhite
                Expanded = False
                FieldName = 'DATEE'
                Title.Alignment = taCenter
                Title.Caption = #46321#47197#51068#51088
                Width = 70
                Visible = True
              end
              item
                Alignment = taCenter
                Color = 12582911
                Expanded = False
                FieldName = 'MAINT_NO'
                Title.Alignment = taCenter
                Title.Caption = #44288#47532#48264#54840
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'APP_SNAME'
                Title.Alignment = taCenter
                Title.Caption = #49888#52397#51064
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CP_BANKNAME'
                Title.Alignment = taCenter
                Title.Caption = #52628#49900#51008#54665
                Width = 140
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'CP_AMTU'
                Title.Alignment = taCenter
                Title.Caption = #52628#49900#44552#50529'('#50808#54868')'
                Width = 140
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'CP_AMTC'
                Title.Alignment = taCenter
                Title.Caption = #53685#54868#45800#50948
                Width = 59
                Visible = True
              end>
          end
          object sPanel24: TsPanel
            Left = 0
            Top = 0
            Width = 787
            Height = 32
            Align = alTop
            
            TabOrder = 1
            object sSpeedButton12: TsSpeedButton
              Left = 230
              Top = 4
              Width = 11
              Height = 23
              ButtonStyle = tbsDivider
            end
            object sMaskEdit3: TsMaskEdit
              Tag = -1
              Left = 57
              Top = 4
              Width = 76
              Height = 23
              AutoSize = False
              
              MaxLength = 10
              TabOrder = 0
              OnChange = sMaskEdit3Change
              BoundLabel.Active = True
              BoundLabel.Caption = #46321#47197#51068#51088
              CheckOnExit = True
              EditMask = '9999-99-99;0'
              Text = '20190101'
            end
            object sMaskEdit4: TsMaskEdit
              Tag = -1
              Left = 150
              Top = 4
              Width = 76
              Height = 23
              AutoSize = False
              
              MaxLength = 10
              TabOrder = 1
              OnChange = sMaskEdit4Change
              BoundLabel.Active = True
              BoundLabel.Caption = '~'
              CheckOnExit = True
              EditMask = '9999-99-99;0'
              Text = '20191231'
            end
            object sBitBtn5: TsBitBtn
              Tag = 1
              Left = 469
              Top = 5
              Width = 66
              Height = 23
              Caption = #51312#54924
              TabOrder = 2
              OnClick = sBitBtn5Click
            end
            object sEdit1: TsEdit
              Tag = -1
              Left = 297
              Top = 5
              Width = 171
              Height = 23
              TabOrder = 3
              OnChange = sEdit1Change
              BoundLabel.Active = True
              BoundLabel.Caption = #44288#47532#48264#54840
            end
          end
        end
      end
      object sPanel6: TsPanel
        Left = 1
        Top = 1
        Width = 795
        Height = 55
        Align = alTop
        
        TabOrder = 2
        object sSpeedButton1: TsSpeedButton
          Left = 334
          Top = 5
          Width = 11
          Height = 46
          ButtonStyle = tbsDivider
        end
        object edt_MAINT_NO: TsEdit
          Left = 64
          Top = 4
          Width = 265
          Height = 23
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          MaxLength = 35
          ParentFont = False
          TabOrder = 0
          SkinData.CustomFont = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object msk_DATEE: TsMaskEdit
          Left = 64
          Top = 28
          Width = 87
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20190124'
        end
        object com_MESSAGE1: TsComboBox
          Left = 400
          Top = 4
          Width = 121
          Height = 23
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#44592#45733
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          Enabled = False
          ItemHeight = 17
          ItemIndex = 1
          TabOrder = 2
          TabStop = False
          Text = '9: Original'
          Items.Strings = (
            '7: Duplicate'
            '9: Original')
        end
        object com_MESSAGE2: TsComboBox
          Left = 400
          Top = 28
          Width = 211
          Height = 23
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#50976#54805
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          Enabled = False
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 3
          TabStop = False
          Text = 'AB: Message Acknowledgement'
          Items.Strings = (
            'AB: Message Acknowledgement'
            'AP: Accepted'
            'NA: No acknowledgement needed'
            'RE: Rejected')
        end
        object edt_USER_ID: TsEdit
          Left = 272
          Top = 28
          Width = 57
          Height = 23
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50857#51088
        end
      end
      object sPanel30: TsPanel
        Left = 272
        Top = 56
        Width = 524
        Height = 29
        Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077') '#51032#47280#49436
        
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Visible = False
      end
    end
    object sPanel4: TsPanel
      Left = 1
      Top = 1
      Width = 315
      Height = 630
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alLeft
      
      TabOrder = 1
      object sDBGrid1: TsDBGrid
        Left = 1
        Top = 57
        Width = 313
        Height = 572
        Align = alClient
        Ctl3D = False
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'MAINT_NO'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 163
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 67
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #51652#54665
            Width = 49
            Visible = True
          end>
      end
      object sPanel5: TsPanel
        Left = 1
        Top = 1
        Width = 313
        Height = 56
        Align = alTop
        
        TabOrder = 1
        object sSpeedButton9: TsSpeedButton
          Left = 230
          Top = 5
          Width = 11
          Height = 46
          ButtonStyle = tbsDivider
        end
        object edt_SearchNo: TsEdit
          Tag = -1
          Left = 57
          Top = 29
          Width = 169
          Height = 23
          TabOrder = 0
          OnChange = edt_SearchNoChange
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
        object sMaskEdit1: TsMaskEdit
          Tag = -1
          Left = 57
          Top = 4
          Width = 76
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 1
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20190101'
        end
        object sMaskEdit2: TsMaskEdit
          Tag = -1
          Left = 150
          Top = 4
          Width = 74
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 2
          OnChange = sMaskEdit2Change
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20191231'
        end
        object btnSearch: TsBitBtn
          Left = 241
          Top = 4
          Width = 66
          Height = 48
          Caption = #51312#54924
          TabOrder = 3
          OnClick = btnSearchClick
        end
      end
      object sPanel29: TsPanel
        Left = 1
        Top = 57
        Width = 313
        Height = 572
        
        TabOrder = 2
        Visible = False
        object sLabel1: TsLabel
          Left = 57
          Top = 239
          Width = 194
          Height = 21
          Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077') '#51032#47280#49436
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel2: TsLabel
          Left = 57
          Top = 264
          Width = 124
          Height = 15
          Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
        end
      end
    end
  end
  object btn_Panel: TsPanel [1]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    
    TabOrder = 1
    object sSpeedButton2: TsSpeedButton
      Left = 447
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton3: TsSpeedButton
      Left = 645
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton4: TsSpeedButton
      Left = 731
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton5: TsSpeedButton
      Left = 901
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 153
      Height = 17
      Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#51032#47280#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'APPSPC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 370
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton7: TsSpeedButton
      Left = 654
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 160
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 171
      Top = 2
      Width = 65
      Height = 39
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 25
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 236
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Tag = 2
      Left = 302
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 380
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 664
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 457
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = btnTempClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 11
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 522
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 0
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Tag = 2
      Left = 586
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 741
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      TabStop = False
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 834
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      TabStop = False
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 21
      ContentMargin = 8
    end
    object sCheckBox1: TsCheckBox
      Left = 912
      Top = 13
      Width = 62
      Height = 16
      Caption = #46356#48260#44536
      TabOrder = 11
      Visible = False
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 56
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 64
    Top = 152
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'DATEE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATEE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, CHK1, CHK2,' +
        ' CHK3, BGM_GUBUN, BGM1, BGM2, BGM3, BGM4, CP_ACCOUNTNO, CP_NAME1' +
        ', CP_NAME2, CP_CURR, CP_BANK, CP_BANKNAME, CP_BANKBU, CP_ADD_ACC' +
        'OUNTNO1, CP_ADD_NAME1, CP_ADD_CURR1, CP_ADD_ACCOUNTNO2, CP_ADD_N' +
        'AME2, CP_ADD_CURR2, APP_DATE, CP_CODE, CP_NO, APP_CODE, APP_SNAM' +
        'E, APP_SAUPNO, APP_NAME, APP_ELEC, APP_ADDR1, APP_ADDR2, APP_ADD' +
        'R3, CP_AMTU, CP_AMTC, CP_AMT, CP_CUX, DF_SAUPNO, DF_EMAIL1, DF_E' +
        'MAIL2, DF_NAME1, DF_NAME2, DF_NAME3, DF_CNT, VB_CNT, SS_CNT '
      'FROM APPSPC_H'
      
        'WHERE DATEE BETWEEN :DATEE1 and :DATEE2 AND MAINT_NO LIKE :MAINT' +
        '_NO'
      'ORDER BY DATEE DESC')
    Left = 24
    Top = 152
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = CHK3GetText
      Size = 10
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      Size = 3
    end
    object qryListBGM1: TStringField
      FieldName = 'BGM1'
      Size = 12
    end
    object qryListBGM2: TStringField
      FieldName = 'BGM2'
      Size = 2
    end
    object qryListBGM3: TStringField
      FieldName = 'BGM3'
      Size = 2
    end
    object qryListBGM4: TStringField
      FieldName = 'BGM4'
      Size = 12
    end
    object qryListCP_ACCOUNTNO: TStringField
      FieldName = 'CP_ACCOUNTNO'
      Size = 17
    end
    object qryListCP_NAME1: TStringField
      FieldName = 'CP_NAME1'
      Size = 35
    end
    object qryListCP_NAME2: TStringField
      FieldName = 'CP_NAME2'
      Size = 35
    end
    object qryListCP_CURR: TStringField
      FieldName = 'CP_CURR'
      Size = 3
    end
    object qryListCP_BANK: TStringField
      FieldName = 'CP_BANK'
      Size = 11
    end
    object qryListCP_BANKNAME: TStringField
      FieldName = 'CP_BANKNAME'
      Size = 70
    end
    object qryListCP_BANKBU: TStringField
      FieldName = 'CP_BANKBU'
      Size = 70
    end
    object qryListCP_ADD_ACCOUNTNO1: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO1'
      Size = 17
    end
    object qryListCP_ADD_NAME1: TStringField
      FieldName = 'CP_ADD_NAME1'
      Size = 35
    end
    object qryListCP_ADD_CURR1: TStringField
      FieldName = 'CP_ADD_CURR1'
      Size = 3
    end
    object qryListCP_ADD_ACCOUNTNO2: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO2'
      Size = 17
    end
    object qryListCP_ADD_NAME2: TStringField
      FieldName = 'CP_ADD_NAME2'
      Size = 35
    end
    object qryListCP_ADD_CURR2: TStringField
      FieldName = 'CP_ADD_CURR2'
      Size = 3
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 35
    end
    object qryListCP_CODE: TStringField
      FieldName = 'CP_CODE'
      Size = 3
    end
    object qryListCP_NO: TStringField
      FieldName = 'CP_NO'
      Size = 35
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_SNAME: TStringField
      FieldName = 'APP_SNAME'
      Size = 35
    end
    object qryListAPP_SAUPNO: TStringField
      FieldName = 'APP_SAUPNO'
      Size = 10
    end
    object qryListAPP_NAME: TStringField
      FieldName = 'APP_NAME'
      Size = 35
    end
    object qryListAPP_ELEC: TStringField
      FieldName = 'APP_ELEC'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListCP_AMTU: TBCDField
      FieldName = 'CP_AMTU'
      Precision = 18
    end
    object qryListCP_AMTC: TStringField
      FieldName = 'CP_AMTC'
      Size = 3
    end
    object qryListCP_AMT: TBCDField
      FieldName = 'CP_AMT'
      Precision = 18
    end
    object qryListCP_CUX: TBCDField
      FieldName = 'CP_CUX'
      Precision = 18
    end
    object qryListDF_SAUPNO: TStringField
      FieldName = 'DF_SAUPNO'
      Size = 10
    end
    object qryListDF_EMAIL1: TStringField
      FieldName = 'DF_EMAIL1'
      Size = 35
    end
    object qryListDF_EMAIL2: TStringField
      FieldName = 'DF_EMAIL2'
      Size = 35
    end
    object qryListDF_NAME1: TStringField
      FieldName = 'DF_NAME1'
      Size = 35
    end
    object qryListDF_NAME2: TStringField
      FieldName = 'DF_NAME2'
      Size = 35
    end
    object qryListDF_NAME3: TStringField
      FieldName = 'DF_NAME3'
      Size = 35
    end
    object qryListDF_CNT: TStringField
      FieldName = 'DF_CNT'
      Size = 70
    end
    object qryListVB_CNT: TStringField
      FieldName = 'VB_CNT'
      Size = 70
    end
    object qryListSS_CNT: TStringField
      FieldName = 'SS_CNT'
      Size = 70
    end
  end
  object qryListD1_2AP: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPSPC_D1 WHERE KEYY = :KEYY and DOC_GUBUN = '#39'2AP'#39)
    Left = 24
    Top = 184
  end
  object qryListD1_2AJ: TADOQuery
    Connection = DMMssql.KISConnect
    AfterScroll = qryListD1_2AJAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPSPC_D1 WHERE KEYY = :KEYY and DOC_GUBUN = '#39'2AJ'#39)
    Left = 192
    Top = 184
  end
  object qryListD1_1BW: TADOQuery
    Connection = DMMssql.KISConnect
    AfterScroll = qryListD1_1BWAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPSPC_D1 WHERE KEYY = :KEYY AND DOC_GUBUN = '#39'1BW'#39)
    Left = 248
    Top = 184
  end
  object qryListD1_2AH: TADOQuery
    Connection = DMMssql.KISConnect
    AfterScroll = qryListD1_2AHAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM APPSPC_D1 WHERE KEYY = :KEYY and DOC_GUBUN = '#39'2AH'#39)
    Left = 112
    Top = 184
  end
  object dsListD1_2AH: TDataSource
    DataSet = qryListD1_2AH
    Left = 136
    Top = 184
  end
  object dsListD1_2AJ: TDataSource
    DataSet = qryListD1_2AJ
    Left = 216
    Top = 184
  end
  object dsListD1_1BW: TDataSource
    DataSet = qryListD1_1BW
    Left = 272
    Top = 184
  end
  object DataSource1: TDataSource
    DataSet = DMCodeContents.GEOLAECHEO
    Left = 280
    Top = 616
  end
  object spCopyAPPSPC: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyAPPSPC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrgMaint_No'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@CpyMaint_No'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end>
    Left = 120
    Top = 416
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE DOMOFC_H1'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 104
    Top = 272
  end
  object qryD1Edit: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO APPSPC_D2 '
      
        'SELECT '#39'0'#39', DOC_GUBUN, SEQ, LINE_NO, HS_NO, DE_DATE, IMD_CODE1, ' +
        'IMD_CODE2, IMD_CODE3, IMD_CODE4, SIZE1, SIZE2, SIZE3, SIZE4, SIZ' +
        'E5, SIZE6, SIZE7, SIZE8, SIZE9, SIZE10, QTY, QTYC, TOTQTY, TOTQT' +
        'YC, PRICE, PRICE_G, PRICEC, CUX_RATE, SUP_AMT, SUP_AMTC, VB_TAX,' +
        ' VB_TAXC, VB_AMT, VB_AMTC, SUP_TOTAMT, SUP_TOTAMTC, VB_TOTTAX, V' +
        'B_TOTTAXC, VB_TOTAMT, VB_TOTAMTC, BIGO1, BIGO2, BIGO3, BIGO4, BI' +
        'GO5 FROM APPSPC_D2 '
      'WHERE KEYY = :KEYY and SEQ = :SEQ and DOC_GUBUN = :DOC_GUBUN')
    Left = 104
    Top = 224
  end
  object qryD1Cancel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'DELETE FROM APPSPC_D2 WHERE KEYY = :KEYY and SEQ = :SEQ and DOC_' +
        'GUBUN = :DOC_GUBUN '
      'UPDATE APPSPC_D2 SET KEYY = :KEYY WHERE KEYY = '#39'0'#39)
    Left = 144
    Top = 224
  end
  object QRCompositeReport1: TQRCompositeReport
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 48
    Top = 456
  end
end
