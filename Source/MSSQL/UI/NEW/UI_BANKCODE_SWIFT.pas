unit UI_BANKCODE_SWIFT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, DB, ADODB, Grids, DBGrids, acDBGrid,
  StdCtrls, sComboBox, sEdit, sButton, Buttons, sSpeedButton, ExtCtrls,
  sPanel, sLabel, SQLCreator, TypeDefine, MessageDefine;

type
  TUI_BANKCODE_SWIFT_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sSpeedButton6: TsSpeedButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnExit: TsButton;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel8: TsPanel;
    edt_FindText: TsEdit;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListBANK_CD: TStringField;
    qryListBANK_NM1: TStringField;
    qryListBANK_NM2: TStringField;
    qryListBANK_BNCH_NM1: TStringField;
    qryListBANK_BNCH_NM2: TStringField;
    qryListACCT_NM: TStringField;
    qryListBANK_NAT_CD: TStringField;
    qryListIS_USE: TBooleanField;
    qryListBANK_UNIT: TStringField;
    pan_input: TsPanel;
    edt_code: TsEdit;
    edt_banknm1: TsEdit;
    edt_banknm2: TsEdit;
    edt_branchnm1: TsEdit;
    edt_branchnm2: TsEdit;
    edt_nat: TsEdit;
    edt_unit: TsEdit;
    edt_accnm: TsEdit;
    sLabel1: TsLabel;
    sButton2: TsButton;
    sButton3: TsButton;
    procedure btnNewClick(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure togglePanel;
    procedure EnableControl;
    procedure readList;
    function existsCode(keys:array of String):boolean;
  public
    { Public declarations }
  end;

var
  UI_BANKCODE_SWIFT_frm: TUI_BANKCODE_SWIFT_frm;

implementation

uses
  ICON, MSSQL;

{$R *.dfm}

{ TUI_BANKCODE_SWIFT_frm }

procedure TUI_BANKCODE_SWIFT_frm.togglePanel;
begin
  with pan_input do
  begin
    Top  := 54;
    Left := 209;
    Visible := not Visible;

    edt_code.Enabled := not (ProgramControlType = ctModify);
  end;

  EnableControl;
end;

procedure TUI_BANKCODE_SWIFT_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctInsert;
  togglePanel;
  ClearControl(pan_input);
end;

procedure TUI_BANKCODE_SWIFT_frm.EnableControl;
begin
    btnNew.Enabled := not pan_input.Visible;
    btnEdit.Enabled := btnNew.Enabled;
    btnDel.Enabled := btnNew.Enabled;
    btnExit.Enabled := btnNew.Enabled;

    sDBGrid1.Enabled := btnNew.Enabled;
    sComboBox1.Enabled := btnNew.Enabled;
    edt_FindText.Enabled := btnNew.Enabled;
    sButton1.Enabled := btnNew.Enabled;
end;

procedure TUI_BANKCODE_SWIFT_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;
  togglePanel;
end;

procedure TUI_BANKCODE_SWIFT_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  edt_code.Text := qryListBANK_CD.AsString;
  edt_nat.Text := qryListBANK_NAT_CD.AsString;
  edt_banknm1.Text := qryListBANK_NM1.AsString;
  edt_banknm2.Text := qryListBANK_NM2.AsString;
  edt_branchnm1.Text := qryListBANK_BNCH_NM1.AsString;
  edt_branchnm2.Text := qryListBANK_BNCH_NM2.AsString;
  edt_accnm.Text := qryListACCT_NM.AsString;
  edt_unit.Text := qryListBANK_UNIT.AsString;
  ProgramControlType := ctModify;
  togglePanel;
end;

procedure TUI_BANKCODE_SWIFT_frm.sButton2Click(Sender: TObject);
var
  SQLCreator : TSQLCreate;
begin
  inherited;
  IF existsCode([edt_code.Text]) AND (ProgramControlType = ctInsert) then
  begin
    MessageBox(Self.Handle, 'SWIFT COD가 이미 있습니다', '중복 데이터', MB_OK + MB_ICONINFORMATION);
    Exit;
  end;

  if isEmpty(edt_code) then
  begin
    MessageBox(Self.Handle, 'SWIFT CODE를 입력하세요', '데이터 유효성 에러', MB_OK + MB_ICONINFORMATION);
    edt_code.SetFocus;
    Exit;
  end;
  if isEmpty(edt_nat) then
  begin
    MessageBox(Self.Handle, '국가코드를 입력하세요', '데이터 유효성 에러', MB_OK + MB_ICONINFORMATION);
    edt_nat.SetFocus;
    Exit;
  end;

  if isEmpty(edt_banknm1) then
  begin
    MessageBox(Self.Handle, '은행명을 입력하세요', '데이터 유효성 에러', MB_OK + MB_ICONINFORMATION);
    edt_banknm1.SetFocus;
    Exit;
  end;
  if isEmpty(edt_unit) then
  begin
    MessageBox(Self.Handle, '통화단위를 입력하세요', '데이터 유효성 에러', MB_OK + MB_ICONINFORMATION);
    edt_unit.SetFocus;
    Exit;
  end;
  if isEmpty(edt_accnm) then
  begin
    MessageBox(Self.Handle, '수취인을 입력하세요', '데이터 유효성 에러', MB_OK + MB_ICONINFORMATION);
    edt_accnm.SetFocus;
    Exit;
  end;

  SQLCreator := TSQLCreate.Create;

  with SQLCreator do
  begin
    case ProgramControlType of
      ctInsert :
      begin
        DMLType := dmlInsert;
        ADDValue('BANK_CD', UpperCase(edt_code.Text));
      end;
      ctModify :
      begin
        DMLType := dmlUpdate;
        ADDWhere('BANK_CD', UpperCase(edt_code.Text));
        ADDValue('UPT_DT', 'getdate()', vtVariant);
      end;
    end;

    SQLHeader('BANKCODE_SWIFT');
    ADDValue('BANK_NM1', edt_banknm1.Text);
    ADDValue('BANK_NM2', edt_banknm2.Text);
    ADDValue('BANK_BNCH_NM1', edt_branchnm1.Text);
    ADDValue('BANK_BNCH_NM2', edt_branchnm2.Text);
    ADDValue('ACCT_NM', edt_accnm.Text);
    ADDValue('BANK_NAT_CD', edt_nat.Text);
    ADDValue('BANK_UNIT', edt_unit.Text);

    ExecSQL(CreateSQL);
  end;

  ProgramControlType := ctView;
  togglePanel;

  edt_FindText.Clear;
  readList;
  qryList.Locate('BANK_CD',VarArrayOf([edt_code.Text]),[]);
end;

procedure TUI_BANKCODE_SWIFT_frm.readList;
begin
  with qryList do
  begin
    close;
    SQL.Text := 'SELECT BANK_CD, BANK_NM1, BANK_NM2, BANK_BNCH_NM1,'#13+
                'BANK_BNCH_NM2, ACCT_NM, BANK_NAT_CD, BANK_UNIT, IS_USE'#13+
                'FROM BANKCODE_SWIFT'#13+
                'WHERE 1=1';
    if Trim(edt_FindText.Text) <> '' then
    begin
      case sComboBox1.ItemIndex of
        0: SQL.Add('AND BANK_CD LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
        1: SQL.Add('AND BANK_NM1 LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
      end;
    end;
    
    Open;
  end;
end;

procedure TUI_BANKCODE_SWIFT_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  readList;
end;

procedure TUI_BANKCODE_SWIFT_frm.btnDelClick(Sender: TObject);
var
  SQLCreator : TSQLCreate;
begin
  inherited;

  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  SQLCreator := TSQLCreate.Create;

  with SQLCreator do
  begin
    DMLType := dmlDelete;
    SQLHeader('BANKCODE_SWIFT');
    ADDWhere('BANK_CD', qryListBANK_CD.AsString);
    ExecSQL(CreateSQL);
  end;

  ProgramControlType := ctView;
  edt_FindText.Clear;
  readList;
end;

function TUI_BANKCODE_SWIFT_frm.existsCode(keys: array of String): boolean;
begin
  result := false;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT 1 FROM BANKCODE_SWIFT WHERE BANK_CD = '+QuotedStr(keys[0])+' AND ACCT_NO = '+QuotedStr(keys[1]);
      Open;

      Result := RecordCount > 0;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_BANKCODE_SWIFT_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_BANKCODE_SWIFT_frm := nil;
end;

procedure TUI_BANKCODE_SWIFT_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_BANKCODE_SWIFT_frm.FormShow(Sender: TObject);
begin
  inherited;
  readList;
end;

procedure TUI_BANKCODE_SWIFT_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsDBGrid).ScreenToClient(Mouse.CursorPos).Y > 21 then
  begin
    btnEditClick(nil);
  end;
end;

end.

