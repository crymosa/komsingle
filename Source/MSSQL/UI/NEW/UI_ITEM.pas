unit UI_ITEM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, ExtCtrls, sPanel, Grids, DBGrids,
  acDBGrid, StdCtrls, DBCtrls, sDBMemo, Mask, sDBEdit, Buttons,
  sSpeedButton, sButton, sComboBox, sEdit, DB, ADODB;

type
  TUI_ITEM_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    edt_FindText: TsEdit;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sSpeedButton6: TsSpeedButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnExit: TsButton;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    procedure FormShow(Sender: TObject);
    procedure edt_FindTextKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sButton1Click(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSQL : String;
    procedure ReadList;
    procedure doRefresh;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_ITEM_frm: TUI_ITEM_frm;

implementation

uses
  ICON, MSSQL, Dlg_ITEM, TypeDefine;

{$R *.dfm}

procedure TUI_ITEM_frm.ReadList;
begin
  with qryList do
  begin
    qryList.Close;
    SQL.Text := FSQL;

    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
      1: SQL.Add('WHERE [SNAME] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
    end;

    Open;
  end;
end;

procedure TUI_ITEM_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_ITEM_frm.edt_FindTextKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_ITEM_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_ITEM_frm.btnNewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;
  Case (sender as TsButton).Tag of
    0: TempControl := ctInsert;
    1: TempControl := ctModify;
    2: TempControl := ctDelete;
  end;

  Dlg_ITEM_frm := TDlg_ITEM_frm.Create(Self);

  try
    IF Dlg_ITEM_frm.Run(TempControl,qryList.Fields) = mrOk Then
    begin
      IF TempControl IN [ctinsert,ctDelete] Then
      begin
        qryList.Close;
        qrylist.Open;
      end
      else if TempControl = ctModify Then
      begin
        doRefresh;
      end;
    end;
  finally
    FreeAndNil(Dlg_ITEM_frm);
  end;
end;
procedure TUI_ITEM_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TUI_ITEM_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ITEM_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ITEM_frm := nil;
end;

procedure TUI_ITEM_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

end.
