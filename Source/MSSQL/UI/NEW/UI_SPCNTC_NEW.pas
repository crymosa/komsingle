unit UI_SPCNTC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, DBCtrls, sDBMemo, sListBox,
  sDBEdit, ExtCtrls, ComCtrls, sPageControl, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton, sSpeedButton,
  sLabel, sPanel, sSkinProvider, DB, ADODB, acImage, StrUtils, DateUtils;

type
  TUI_SPCNTC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    Shape2: TShape;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit7: TsDBEdit;
    Shape1: TShape;
    sDBEdit5: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sDBEdit22: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sDBEdit12: TsDBEdit;
    Shape3: TShape;
    sPageControl2: TsPageControl;
    sTabSheet2: TsTabSheet;
    lst_RCNO: TsListBox;
    sTabSheet3: TsTabSheet;
    lst_FINNO: TsListBox;
    sTabSheet5: TsTabSheet;
    sDBMemo1: TsDBMemo;
    sTabSheet6: TsTabSheet;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailCHARGE_NO: TStringField;
    qryDetailCHARGE_TYPE: TStringField;
    qryDetailCHARGE_RATE: TStringField;
    qryDetailBAS_AMT: TBCDField;
    qryDetailBAS_AMTC: TStringField;
    qryDetailCHA_AMT: TBCDField;
    qryDetailCHA_AMTC: TStringField;
    qryDetailCUX_RATE: TBCDField;
    qryDetailSES_DAY: TStringField;
    qryDetailRES_DT: TStringField;
    sDBGrid2: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    com_func: TsComboBox;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM_NM: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListNT_DATE: TStringField;
    qryListCP_DATE: TStringField;
    qryListDM_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_NAME: TStringField;
    qryListCP_BANKELEC: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_TOTAMT: TBCDField;
    qryListCP_TOTAMTC: TStringField;
    qryListCP_TOTCHARGE: TBCDField;
    qryListCP_TOTCHARGEC: TStringField;
    qryListCP_FTX: TStringField;
    qryListCP_FTX1: TMemoField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPP_NO: TStringField;
    qryListCP_NO: TStringField;
    qryListRC_NO: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    sImage1: TsImage;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sDBEdit24: TsDBEdit;
    sDBEdit25: TsDBEdit;
    sDBEdit26: TsDBEdit;
    qryDetailCHARGE_NM: TStringField;
    qryDetailSES_SDATE: TStringField;
    qryDetailSES_EDATE: TStringField;
    sDBEdit27: TsDBEdit;
    sSpeedButton2: TsSpeedButton;
    sDBEdit28: TsDBEdit;
    sDBEdit29: TsDBEdit;
    sDBEdit30: TsDBEdit;
    sDBEdit31: TsDBEdit;
    sDBEdit32: TsDBEdit;
    sDBEdit33: TsDBEdit;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    procedure qryListMESSAGE1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
    procedure ReadDetail;
  protected
    procedure ReadData; override;
  public
    { Public declarations }
  end;

var
  UI_SPCNTC_NEW_frm: TUI_SPCNTC_NEW_frm;

implementation

uses
  MSSQL, ICON, Preview, SPCNTC_PRINT;

{$R *.dfm}

procedure TUI_SPCNTC_NEW_frm.qryListMESSAGE1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  IF qryListBGM_GUBUN.AsString = '2CQ' Then
  begin
    case AnsiIndexText(qryListMESSAGE1.AsString, ['1','7','9']) of
      0: Text := '취소';
      1: Text := '부도';
      2: Text := '승인';
    else
      Text := 'ERR';
    end;
  end
  else
  IF qryListBGM_GUBUN.AsString = '2CR' Then
  begin
    IF qryListMESSAGE1.AsString = '9' Then
      Text := '반려'
    else
      Text := 'ERR';
  end
  else
  IF qryListBGM_GUBUN.AsString = '2DT' Then
  begin
    case AnsiIndexText(qryListMESSAGE1.AsString, ['1','7','9']) of
      0: Text := '취소';
      1: Text := '부도';
      2: Text := '승인';
    else
      Text := 'ERR';
    end;
  end;
end;

procedure TUI_SPCNTC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_SPCNTC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_SPCNTC_NEW_frm := nil;
end;

procedure TUI_SPCNTC_NEW_frm.ReadDetail;
begin
  with qryDetail do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;
end;

procedure TUI_SPCNTC_NEW_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    Open;
  end;
end;

procedure TUI_SPCNTC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6);

  sPageControl1.ActivePageIndex := 0;
  sPageControl2.ActivePageIndex := 0;      
  ReadOnlyControl(spanel7,false); 
  ReadOnlyControl(spanel1,false);
end;

procedure TUI_SPCNTC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_SPCNTC_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_SPCNTC_NEW_frm.ReadData;
var
  i : integer;
begin
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 2);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

  //물품수령증명서
  lst_RCNO.Clear;
  for i := 1 to 8 do
  begin
    IF (i = 1) AND (Trim(qryListRC_NO.AsString) <> '') Then
      lst_RCNO.Items.Add(qryListRC_NO.AsString)
    else
    if i > 1 Then
    begin
      IF Trim(qryList.FieldByName('RC_NO'+IntToStr(i)).AsString) <> '' Then
        lst_RCNO.Items.Add(qryList.FieldByName('RC_NO'+IntToStr(i)).AsString);
    end;
  end;
  IF lst_RCNO.Items.Count > 0 Then
    sTabSheet2.Caption := '물품수령증명서(인수증)번호 ('+IntToStr(lst_RCNO.Items.Count)+')'
  else
    sTabSheet2.Caption := '물품수령증명서(인수증)번호';

  //세금계산서
  lst_FINNO.Clear;
  for i := 1 to 7 do
  begin
    IF (i = 1) AND (Trim(qryListFIN_NO.AsString) <> '') Then
      lst_FINNO.Items.Add(qryListFIN_NO.AsString)
    else
    if i > 1 Then
    begin
      IF Trim(qryList.FieldByName('FIN_NO'+IntToStr(i)).AsString) <> '' Then
        lst_FINNO.Items.Add(qryList.FieldByName('FIN_NO'+IntToStr(i)).AsString);
    end;
  end;
  IF lst_FINNO.Items.Count > 0 Then
    sTabSheet3.Caption := '세금계산서번호 ('+IntToStr(lst_FINNO.Items.Count)+')'
  else
    sTabSheet3.Caption := '세금계산서번호';
end;

procedure TUI_SPCNTC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadDetail;
end;

procedure TUI_SPCNTC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_SPCNTC_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  SPCNTC_PRINT_frm := TSPCNTC_PRINT_frm.Create(Self);
  SPCNTC_PRINT_frm.MaintNo := qryListMAINT_NO.AsString;
  Preview_frm := TPreview_frm.Create(Self);
  try
    Preview_frm.Report := SPCNTC_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(SPCNTC_PRINT_frm);
  end;
end;

end.
