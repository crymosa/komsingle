inherited UI_LOGUAR_NEW_frm: TUI_LOGUAR_NEW_frm
  Left = 606
  Top = 201
  Caption = '[LOGUAR] '#49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49436
  PixelsPerInch = 96
  TextHeight = 15
  inherited btn_Panel: TsPanel
    inherited sSpeedButton4: TsSpeedButton
      Left = 835
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 825
      Visible = False
    end
    inherited sLabel7: TsLabel
      Left = 53
      Width = 233
      Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49436
    end
    inherited sLabel6: TsLabel
      Left = 137
      Width = 65
      Caption = 'LOGUAR'
    end
    inherited sSpeedButton6: TsSpeedButton
      Left = 825
      Visible = False
    end
    inherited sSpeedButton7: TsSpeedButton
      Left = 465
    end
    inherited sSpeedButton8: TsSpeedButton
      Left = 324
    end
    inherited sSpeedButton2: TsSpeedButton
      Left = 825
      Visible = False
    end
    inherited btnNew: TsButton
      Left = 825
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 825
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 333
    end
    inherited btnPrint: TsButton
      Left = 399
    end
    inherited btnTemp: TsButton
      Left = 825
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 825
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 825
      Visible = False
    end
    inherited btnReady: TsButton
      Left = 825
      Visible = False
    end
    inherited btnSend: TsButton
      Left = 825
      Visible = False
    end
    inherited btnCopy: TsButton
      Left = 825
      Visible = False
    end
    inherited sPanel6: TsPanel
      Left = 328
      Width = 782
      inherited sSpeedButton1: TsSpeedButton
        Left = 344
      end
      inherited edt_MAINT_NO: TsEdit
        Width = 273
      end
      inherited msk_Datee: TsMaskEdit
        Left = 392
        BoundLabel.Caption = #49688#49888#51068#51088
      end
      inherited com_func: TsComboBox
        Left = 637
        ItemIndex = 0
        Text = '1: Cancel'
        Items.Strings = (
          '1: Cancel'
          '7: Duplicate'
          '9: Original'
          '51: '#46041#48372#51204#49569#49884' - '#49324#48376
          '52: '#46041#48372#51204#49569#49884' - '#50896#48376)
      end
      inherited com_type: TsComboBox
        Left = 734
      end
      inherited edt_userno: TsEdit
        Left = 520
      end
    end
  end
  inherited sPanel4: TsPanel
    Width = 394
    inherited sDBGrid1: TsDBGrid
      Width = 392
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 265
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    inherited sPanel5: TsPanel
      Width = 392
    end
    inherited sPanel29: TsPanel
      Left = 9
      Top = 608
      Width = 40
      Height = 23
      Align = alNone
      Visible = True
    end
  end
  inherited sPanel3: TsPanel
    Left = 394
    Width = 720
    inherited sPageControl1: TsPageControl
      Width = 718
      ActivePage = sTabSheet3
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          Width = 710
          inherited Shape1: TShape
            Left = 7
            Top = 357
            Width = 695
          end
          object Shape2: TShape [1]
            Left = 7
            Top = 106
            Width = 695
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object Shape3: TShape [2]
            Left = 7
            Top = 171
            Width = 695
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          inherited edt_BL_NO: TsEdit [3]
            Top = 53
          end
          inherited sEdit5: TsEdit [4]
            Top = 53
          end
          inherited edt_BL_G: TsEdit [5]
            Top = 53
          end
          inherited edt_LC_NO: TsEdit [6]
            Top = 29
          end
          inherited sEdit2: TsEdit [7]
            Top = 29
          end
          inherited edt_LC_G: TsEdit [8]
            Top = 29
          end
          inherited edt_Message1NM: TsEdit [9]
            Top = 5
          end
          inherited edt_MESSAGE1: TsEdit [10]
            Top = 5
          end
          inherited edt_CARRIER2: TsEdit [11]
            Top = 177
          end
          inherited edt_CARRIER1: TsEdit [12]
            Top = 177
          end
          inherited edt_CR_NAME1: TsEdit [13]
            Top = 205
          end
          inherited edt_CR_NAME2: TsEdit [14]
            Top = 229
          end
          inherited edt_CR_NAME3: TsEdit [15]
            Top = 253
          end
          inherited edt_B5_NAME1: TsEdit [16]
            Top = 280
          end
          inherited edt_B5_NAME2: TsEdit [17]
            Top = 304
          end
          inherited edt_B5_NAME3: TsEdit [18]
            Top = 328
          end
          object edt_LGNO: TsEdit [19]
            Left = 282
            Top = 115
            Width = 271
            Height = 23
            Color = 13828095
            MaxLength = 17
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          inherited sButton1: TsButton [20]
            Top = 253
          end
          inherited msk_BL_DATE: TsMaskEdit [21]
            Top = 77
          end
          inherited msk_APP_DATE: TsMaskEdit [22]
            Left = 618
            Top = 5
            Visible = False
          end
          object edt_USERMAINTNO: TsEdit [23]
            Left = 370
            Top = 139
            Width = 183
            Height = 23
            Color = 13828095
            MaxLength = 17
            TabOrder = 39
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50629#52404#49888#52397#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          inherited edt_SE_NAME1: TsEdit [24]
            Top = 205
            Width = 239
          end
          inherited edt_SE_NAME2: TsEdit [25]
            Top = 229
            Width = 239
          end
          inherited edt_SE_NAME3: TsEdit [26]
            Top = 253
            Width = 239
          end
          inherited edt_CN_NAME1: TsEdit [27]
            Top = 280
            Width = 239
          end
          inherited edt_CN_NAME2: TsEdit [28]
            Top = 304
            Width = 239
          end
          inherited edt_CN_NAME3: TsEdit [29]
            Top = 328
            Width = 239
          end
          inherited edt_LOAD_LOC: TsEdit [30]
            Top = 365
          end
          inherited edt_LOAD_LOC_NM: TsEdit [31]
            Top = 365
          end
          inherited edt_LOAD_TXT: TsEdit [32]
            Top = 389
          end
          inherited edt_ARR_LOC: TsEdit [33]
            Top = 415
          end
          inherited edt_ARR_LOC_NM: TsEdit [34]
            Top = 415
          end
          inherited edt_ARR_TXT: TsEdit [35]
            Top = 439
          end
          inherited msk_AR_DATE: TsMaskEdit [36]
            Top = 463
          end
          inherited edt_INV_AMTC: TsEdit [37]
            Top = 489
          end
          inherited curr_INV_AMT: TsCurrencyEdit [38]
            Top = 489
          end
          inherited edt_PAC_QTYC: TsEdit [39]
            Top = 513
          end
          inherited curr_PAC_QTY: TsCurrencyEdit [40]
            Top = 513
          end
          object msk_LGDATE: TsMaskEdit
            Left = 202
            Top = 115
            Width = 79
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 36
            Color = 13828095
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#52712#48372#51613#49436' '#48156#44553#51068#51088' / '#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object msk_LCDATE: TsMaskEdit
            Left = 202
            Top = 139
            Width = 79
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 38
            Color = 13828095
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#50857#51109#48156#54665#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
        end
      end
      inherited sTabSheet3: TsTabSheet
        inherited sPanel27: TsPanel
          Width = 710
          inherited memo_SPMARK: TsMemo
            Width = 577
            Enabled = False
          end
          inherited memo_GOODS: TsMemo
            Width = 577
            Enabled = False
          end
          inherited edt_MS_NAME1: TsEdit
            Width = 266
          end
          inherited edt_MS_NAME2: TsEdit
            Width = 266
          end
          inherited edt_AX_NAME1: TsEdit
            Width = 266
          end
          inherited edt_AX_NAME2: TsEdit
            Width = 266
          end
        end
      end
    end
  end
  inherited qryList: TADOQuery
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, C' +
        'R_NAME1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NA' +
        'ME1, MS_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, ' +
        'INV_AMTC, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE,' +
        ' BL_DATE, '#39'NO USED'#39' as APP_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, AR' +
        'R_TXT, SPMARK1, SPMARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPM' +
        'ARK7, SPMARK8, SPMARK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOO' +
        'DS2, GOODS3, GOODS4, GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOO' +
        'DS10, TRM_PAYC, TRM_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2,' +
        ' CHK3, PRNO, CN_NAME1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, B' +
        '5_NAME3'
      ',LG_NO, LG_DATE, LC_DATE, USERMAINT_NO'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      #9',LOADCODE.LOADNAME'
      #9',ARRCODE.ARRNAME'
      #9',TRMPAYCCODE.TRMPAYCNAME'
      #9',SUNSACODE.CRNAEM3NAME'
      'FROM LOGUAR'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'LOGUAR'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as LOADNAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADCODE ON LOAD_LOC = LOADCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as ARRNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#44397#44032#39') ARRCODE ON ARR_LOC = ARRCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as TRMPAYCNAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39#44208#51228#44592#44036#39') TRMPAYCCODE ON TRM_PAYC = TRMPAYCCO' +
        'DE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as CRNAEM3NAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39'SUNSA'#39') SUNSACODE ON CR_NAME3 = SUNSACODE.' +
        'CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    object qryListLG_NO: TStringField
      DisplayWidth = 20
      FieldName = 'LG_NO'
    end
    object qryListLG_DATE: TStringField
      FieldName = 'LG_DATE'
      Size = 8
    end
    object qryListLC_DATE: TStringField
      FieldName = 'LC_DATE'
      Size = 8
    end
    object qryListUSERMAINT_NO: TStringField
      FieldName = 'USERMAINT_NO'
      Size = 35
    end
  end
end
