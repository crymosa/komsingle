unit UI_CREADV_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sCheckBox, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, sComboBox, ComCtrls, sPageControl, Buttons,
  sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton,
  sLabel, sSpeedButton, ExtCtrls, sPanel, sSkinProvider, DB, ADODB, StrUtils,DateUtils;

type
  TUI_CREADV_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    msk_ADV_DATE: TsMaskEdit;
    edt_ADREFNO: TsEdit;
    edt_AdInfo5: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    sPanel59: TsPanel;
    com_Carriage: TsComboBox;
    sPanel89: TsPanel;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet2: TsTabSheet;
    sPanel34: TsPanel;
    sTabSheet8: TsTabSheet;
    sPanel30: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    msk_VAL_DATE: TsMaskEdit;
    msk_POS_DATE: TsMaskEdit;
    msk_EXE_DATE: TsMaskEdit;
    Shape1: TShape;
    memo_PAYDET: TsMemo;
    sPanel2: TsPanel;
    sPanel8: TsPanel;
    memo_ADINFO2: TsMemo;
    sPanel10: TsPanel;
    sPanel29: TsPanel;
    edt_ORD1AMTC: TsEdit;
    cur_ORD1AMT: TsCurrencyEdit;
    sPanel33: TsPanel;
    edt_ORD2AMTC: TsEdit;
    cur_ORD2AMT: TsCurrencyEdit;
    sPanel91: TsPanel;
    edt_ORD3AMTC: TsEdit;
    cur_ORD3AMT: TsCurrencyEdit;
    sPanel95: TsPanel;
    edt_ORD4AMTC: TsEdit;
    cur_ORD4AMT: TsCurrencyEdit;
    cur_EXCRATE4: TsCurrencyEdit;
    msk_EXCDATE41: TsMaskEdit;
    sPanel96: TsPanel;
    sPanel97: TsPanel;
    msk_EXCDATE42: TsMaskEdit;
    edt_RATECD41: TsEdit;
    edt_RATECD42: TsEdit;
    sPanel1: TsPanel;
    edt_BN1NAME1: TsEdit;
    edt_BN1NAME2: TsEdit;
    edt_BN1NAME3: TsEdit;
    edt_BN1PCOD4: TsEdit;
    edt_BN1BANK: TsEdit;
    edt_BN1BANK1: TsEdit;
    edt_BN1BANK2: TsEdit;
    edt_BN1FB: TsEdit;
    sPanel11: TsPanel;
    edt_OD1NAME1: TsEdit;
    edt_OD1NAME2: TsEdit;
    edt_OD1NAME3: TsEdit;
    edt_OD1PCOD4: TsEdit;
    edt_OD1BANK: TsEdit;
    edt_OD1BANK1: TsEdit;
    edt_OD1BANK2: TsEdit;
    edt_OD1FB: TsEdit;
    sPanel12: TsPanel;
    edt_AX1NAME1: TsEdit;
    edt_AX1NAME2: TsEdit;
    edt_AX1NAME3: TsEdit;
    edt_AX1PCOD4: TsEdit;
    edt_AX1BANK: TsEdit;
    edt_AX1BANK1: TsEdit;
    edt_AX1BANK2: TsEdit;
    edt_AX1FB: TsEdit;
    sPanel13: TsPanel;
    edt_TRN3NAME1: TsEdit;
    edt_TRN3NAME2: TsEdit;
    edt_TRN3NAME3: TsEdit;
    sPanel14: TsPanel;
    edt_BEBANK1: TsEdit;
    edt_BENEFC1: TsEdit;
    edt_BEBANK2: TsEdit;
    edt_BENEFC2: TsEdit;
    Shape2: TShape;
    sPanel15: TsPanel;
    edt_TRN1NAME1: TsEdit;
    edt_TRN1NAME2: TsEdit;
    edt_TRN1NAME3: TsEdit;
    edt_TRN1STR1: TsEdit;
    edt_TRN1STR2: TsEdit;
    sPanel16: TsPanel;
    edt_TRN2NAME1: TsEdit;
    edt_TRN2NAME2: TsEdit;
    edt_TRN2NAME3: TsEdit;
    edt_TRN2STR1: TsEdit;
    edt_TRN2STR2: TsEdit;
    Shape3: TShape;
    sDBGrid2: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryFC1: TADOQuery;
    dsFC1: TDataSource;
    qryDoclist: TADOQuery;
    dsDoclist: TDataSource;
    qryDoclistKEYY: TStringField;
    qryDoclistSEQ: TIntegerField;
    qryDoclistRELIST_TYPE: TStringField;
    qryDoclistNAME: TStringField;
    qryDoclistViewType: TStringField;
    qryDoclistRELIST_NO: TStringField;
    qryDoclistRELIST_ADDNO: TStringField;
    qryDoclistRELIST_APPDT: TStringField;
    sDBGrid4: TsDBGrid;
    sPanel17: TsPanel;
    qryD1: TADOQuery;
    dsD1: TDataSource;
    sPanel18: TsPanel;
    sDBGrid5: TsDBGrid;
    sPanel26: TsPanel;
    sPanel28: TsPanel;
    sPanel9: TsPanel;
    memo_ADINFO1: TsMemo;
    edt_TRN1STR3: TsEdit;
    edt_TRN2STR3: TsEdit;
    qryD1KEYY: TStringField;
    qryD1SEQ: TIntegerField;
    qryD1FC1CD: TStringField;
    qryD1FC1NM: TStringField;
    qryD1FC1AMT11: TBCDField;
    qryD1FC1AMT11C: TStringField;
    qryD1FC1AMT12: TBCDField;
    qryD1FC1AMT12C: TStringField;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListADV_DATE: TStringField;
    qryListVAL_DATE: TStringField;
    qryListPOS_DATE: TStringField;
    qryListEXE_DATE: TStringField;
    qryListADREFNO: TStringField;
    qryListPAYDET: TBooleanField;
    qryListPAYDET1: TMemoField;
    qryListADINFO1: TBooleanField;
    qryListADINFO11: TMemoField;
    qryListADINFO2: TBooleanField;
    qryListADINFO21: TMemoField;
    qryListORD1AMT: TBCDField;
    qryListORD1AMTC: TStringField;
    qryListEXCRATE1: TBCDField;
    qryListEXCDATE11: TStringField;
    qryListEXCDATE12: TStringField;
    qryListRATECD11: TStringField;
    qryListRATECD12: TStringField;
    qryListORD2AMT: TBCDField;
    qryListORD2AMTC: TStringField;
    qryListEXCRATE2: TBCDField;
    qryListEXCDATE21: TStringField;
    qryListEXCDATE22: TStringField;
    qryListRATECD21: TStringField;
    qryListRATECD22: TStringField;
    qryListORD3AMT: TBCDField;
    qryListORD3AMTC: TStringField;
    qryListEXCRATE3: TBCDField;
    qryListEXCDATE31: TStringField;
    qryListEXCDATE32: TStringField;
    qryListRATECD31: TStringField;
    qryListRATECD32: TStringField;
    qryListORD4AMT: TBCDField;
    qryListORD4AMTC: TStringField;
    qryListEXCRATE4: TBCDField;
    qryListEXCDATE41: TStringField;
    qryListEXCDATE42: TStringField;
    qryListRATECD41: TStringField;
    qryListRATECD42: TStringField;
    qryListBN1NAME1: TStringField;
    qryListBN1NAME2: TStringField;
    qryListBN1NAME3: TStringField;
    qryListBN1PCOD4: TStringField;
    qryListBN1BANK: TStringField;
    qryListBN1BANK1: TStringField;
    qryListBN1BANK2: TStringField;
    qryListBN1FB: TStringField;
    qryListOD1NAME1: TStringField;
    qryListOD1NAME2: TStringField;
    qryListOD1NAME3: TStringField;
    qryListOD1PCOD4: TStringField;
    qryListOD1BANK: TStringField;
    qryListOD1BANK1: TStringField;
    qryListOD1BANK2: TStringField;
    qryListOD1FB: TStringField;
    qryListAX1NAME1: TStringField;
    qryListAX1NAME2: TStringField;
    qryListAX1NAME3: TStringField;
    qryListAX1PCOD4: TStringField;
    qryListAX1BANK: TStringField;
    qryListAX1BANK1: TStringField;
    qryListAX1BANK2: TStringField;
    qryListAX1FB: TStringField;
    qryListTRN1NAME1: TStringField;
    qryListTRN1NAME2: TStringField;
    qryListTRN1NAME3: TStringField;
    qryListTRN1STR1: TStringField;
    qryListTRN1STR2: TStringField;
    qryListTRN1STR3: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListTRN2NAME1: TStringField;
    qryListTRN2NAME2: TStringField;
    qryListTRN2NAME3: TStringField;
    qryListTRN2STR1: TStringField;
    qryListTRN2STR2: TStringField;
    qryListTRN2STR3: TStringField;
    qryListTRN3NAME1: TStringField;
    qryListTRN3NAME2: TStringField;
    qryListTRN3NAME3: TStringField;
    qryListBEBANK1: TStringField;
    qryListBENEFC1: TStringField;
    qryListBEBANK2: TStringField;
    qryListBENEFC2: TStringField;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryD1AfterScroll(DataSet: TDataSet);
    procedure qryD1AfterOpen(DataSet: TDataSet);
    procedure btnExitClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList(SortField: TColumn = nil);
  protected
    procedure ReadData; override;
  public
    { Public declarations }
  end;

var
  UI_CREADV_NEW_frm: TUI_CREADV_NEW_frm;

implementation

uses
  MSSQL, MessageDefine, QR_CREADV1, Preview;

{$R *.dfm}

{ TUI_CREADV_NEW_frm }

procedure TUI_CREADV_NEW_frm.ReadList;
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'H1.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_CREADV_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_CREADV_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_CREADV_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6,false);
  ReadOnlyControl(sPanel7,False);
  ReadOnlyControl(sPanel27,False);
  ReadOnlyControl(sPanel30,False);
  ReadOnlyControl(sPanel34,False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_CREADV_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_CREADV_NEW_frm := nil;
end;

procedure TUI_CREADV_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;

  with qryDoclist do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;

  with qryD1 do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;

end;

procedure TUI_CREADV_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_CREADV_NEW_frm.qryD1AfterScroll(DataSet: TDataSet);
begin
  inherited;
  with qryFC1 do
  begin
    Close;
    Parameters[0].Value := qryD1KEYY.AsString;
    Parameters[1].Value := qryD1SEQ.AsInteger;
    Open;
  end;
end;

procedure TUI_CREADV_NEW_frm.qryD1AfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryD1AfterScroll(DataSet);
end;

procedure TUI_CREADV_NEW_frm.ReadData;
begin
  inherited;
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

//------------------------------------------------------------------------------
// 1PAGE
//------------------------------------------------------------------------------
  msk_ADV_DATE.Text := qryListADV_DATE.AsString;
  msk_VAL_DATE.Text := qryListVAL_DATE.AsString;
  msk_POS_DATE.Text := qryListPOS_DATE.AsString;
  msk_EXE_DATE.Text := qryListEXE_DATE.AsString;

  edt_ADREFNO.Text := qryListADREFNO.AsString;

  memo_PAYDET.Text := qryListPAYDET1.AsString;
  memo_ADINFO1.Text := qryListADINFO11.AsString;
  memo_ADINFO2.Text := qryListADINFO21.AsString;

  edt_ORD1AMTC.Text := qryListORD1AMTC.AsString;
  cur_ORD1AMT.Value := qryListORD1AMT.AsCurrency;
//  msk_EXCDATE11.Text := qryListEXCDATE11.AsString;
//  msk_EXCDATE12.Text := qryListEXCDATE12.AsString;
//  edt_RATECD11.Text := qryListRATECD11.AsString;
//  edt_RATECD12.Text := qryListRATECD12.AsString;

  edt_ORD2AMTC.Text := qryListORD2AMTC.AsString;
  cur_ORD2AMT.Value := qryListORD2AMT.AsCurrency;
//  msk_EXCDATE21.Text := qryListEXCDATE21.AsString;
//  msk_EXCDATE22.Text := qryListEXCDATE22.AsString;
//  edt_RATECD21.Text := qryListRATECD21.AsString;
//  edt_RATECD22.Text := qryListRATECD22.AsString;

  edt_ORD3AMTC.Text := qryListORD3AMTC.AsString;
  cur_ORD3AMT.Value := qryListORD3AMT.AsCurrency;
//  msk_EXCDATE31.Text := qryListEXCDATE31.AsString;
//  msk_EXCDATE32.Text := qryListEXCDATE32.AsString;
//  edt_RATECD31.Text := qryListRATECD31.AsString;
//  edt_RATECD32.Text := qryListRATECD32.AsString;

  edt_ORD4AMTC.Text := qryListORD4AMTC.AsString;
  cur_ORD4AMT.Value := qryListORD4AMT.AsCurrency;
  msk_EXCDATE41.Text := qryListEXCDATE41.AsString;
  msk_EXCDATE42.Text := qryListEXCDATE42.AsString;
  edt_RATECD41.Text := qryListRATECD41.AsString;
  edt_RATECD42.Text := qryListRATECD42.AsString;

  //수익자
  edt_BN1NAME1.Text := qryListBN1NAME1.AsString;
  edt_BN1NAME2.Text := qryListBN1NAME2.AsString;
  edt_BN1NAME3.Text := qryListBN1NAME3.AsString;
  edt_BN1PCOD4.Text := qryListBN1PCOD4.AsString;
  edt_BN1BANK.Text  := qryListBN1BANK.AsString;
  edt_BN1BANK1.Text := qryListBN1BANK1.AsString;
  edt_BN1BANK2.Text := qryListBN1BANK2.AsString;
  edt_BN1FB.Text    := qryListBN1FB.AsString;

  //지급의뢰인
  edt_OD1NAME1.Text := qryListOD1NAME1.AsString;
  edt_OD1NAME2.Text := qryListOD1NAME2.AsString;
  edt_OD1NAME3.Text := qryListOD1NAME3.AsString;
  edt_OD1PCOD4.Text := qryListOD1PCOD4.AsString;
  edt_OD1BANK.Text  := qryListOD1BANK.AsString;
  edt_OD1BANK1.Text := qryListOD1BANK1.AsString;
  edt_OD1BANK2.Text := qryListOD1BANK2.AsString;
  edt_OD1FB.Text    := qryListOD1FB.AsString;

  //중간경유인
  edt_AX1NAME1.Text := qryListAX1NAME1.AsString;
  edt_AX1NAME2.Text := qryListAX1NAME2.AsString;
  edt_AX1NAME3.Text := qryListAX1NAME3.AsString;
  edt_AX1PCOD4.Text := qryListAX1PCOD4.AsString;
  edt_AX1BANK.Text  := qryListAX1BANK.AsString;
  edt_AX1BANK1.Text := qryListAX1BANK1.AsString;
  edt_AX1BANK2.Text := qryListAX1BANK2.AsString;
  edt_AX1FB.Text := qryListAX1FB.AsString;

  //발신기관 전자서명
  edt_TRN3NAME1.Text := qryListTRN3NAME1.Text;
  edt_TRN3NAME2.Text := qryListTRN3NAME2.Text;
  edt_TRN3NAME3.Text := qryListTRN3NAME3.Text;

  //지시당사자 - 지시하는 당사자/기관
  edt_BEBANK1.Text := qryListBEBANK1.AsString;
  edt_BEBANK2.Text := qryListBEBANK2.AsString;
  //지시당사자 - 지시받는 당사자/기관
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;

  //수익자(상호/주소)
  edt_TRN1NAME1.Text := qryListTRN1NAME1.AsString;
  edt_TRN1NAME2.Text := qryListTRN1NAME2.AsString;
  edt_TRN1NAME3.Text := qryListTRN1NAME3.AsString;
  edt_TRN1STR1.Text := qryListTRN1STR1.AsString;
  edt_TRN1STR2.Text := qryListTRN1STR2.AsString;
  edt_TRN1STR3.Text := qryListTRN1STR3.AsString;

  //지급의뢰인(상호/주소)
  edt_TRN2NAME1.Text := qryListTRN2NAME1.AsString;
  edt_TRN2NAME2.Text := qryListTRN2NAME2.AsString;
  edt_TRN2NAME3.Text := qryListTRN2NAME3.AsString;
  edt_TRN2STR1.Text  := qryListTRN2STR1.AsString;
  edt_TRN2STR2.Text  := qryListTRN2STR2.AsString;
  edt_TRN2STR3.Text  := qryListTRN2STR3.AsString;
end;

procedure TUI_CREADV_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_CREADV_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : Integer;
begin
//  inherited;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
   try
     try
       IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'입급통지서'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
       begin
         nCursor := qryList.RecNo;
         SQL.Text :=  'DELETE FROM CREADV_H1 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM CREADV_H2 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM CREADV_D1 WHERE KEYY =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM CREADV_DOCLIST WHERE KEYY =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM CREADV_FC1 WHERE KEYY =' + QuotedStr(maint_no);
         ExecSQL;

         //트랜잭션 커밋
         DMMssql.KISConnect.CommitTrans;

         qryList.Close;
         qryList.Open;

         if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
         begin
           qryList.MoveBy(nCursor-1);
         end
         else
           qryList.First;
       end
       else
         DMMssql.KISConnect.RollbackTrans;

     except
       on E:Exception do
       begin
         DMMssql.KISConnect.RollbackTrans;
         MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
       end;
     end;

   finally
     if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
     Close;
     Free;
   end;
  end;
end;

procedure TUI_CREADV_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  QR_CREADV1_PRN := TQR_CREADV1_PRN.Create(Self);
  try
    QR_CREADV1_PRN.COMMON := qryList;
    QR_CREADV1_PRN.DOCLIST := qryDoclist;
    QR_CREADV1_PRN.D1 := qryD1;
    QR_CREADV1_PRN.FC1 := qryFC1;

    Preview_frm.Report := QR_CREADV1_PRN;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(QR_CREADV1_PRN);
  end;
end;

procedure TUI_CREADV_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 4;
end;

end.
