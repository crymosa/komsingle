unit UI_APP707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, UI_APP707_BP, DB, ADODB, Grids,
  DBGrids, acDBGrid, StdCtrls, sComboBox, ExtCtrls, sSplitter, sMemo,
  sLabel, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, sCheckBox, Buttons, sBitBtn, Mask, sMaskEdit, sEdit,
  sButton, sSpeedButton, sPanel, Clipbrd, sBevel, Menus;

type
  TUI_APP707_frm = class(TUI_APP707_BP_frm)
    qryListMAINT_NO: TStringField;
    qryListMSeq: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListMSeq_1: TIntegerField;
    qryListAmd_No_1: TIntegerField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListSRBUHO: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListCARRIAGE: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    sPanel32: TsPanel;
    edt_lstDate: TsMaskEdit;
    sPanel33: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    sPanel36: TsPanel;
    edt_Carriage: TsEdit;
    sBitBtn27: TsBitBtn;
    edt_Carriage1: TsEdit;
    sPanel10: TsPanel;
    memo_Narrat1: TsMemo;
    sPanel37: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel38: TsPanel;
    edt_dochackPort: TsEdit;
    sLabel24: TsLabel;
    sPanel39: TsPanel;
    edt_loadOn: TsEdit;
    sLabel27: TsLabel;
    sPanel40: TsPanel;
    sPanel20: TsPanel;
    edt_Aacv1: TsEdit;
    edt_Aacv2: TsEdit;
    edt_Aacv3: TsEdit;
    edt_Aacv4: TsEdit;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListCarriage_Name: TStringField;
    sp_attachAPP707fromINF700: TADOStoredProc;
    sp_attachAPP707fromINF707: TADOStoredProc;
    sLabel2: TsLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    qryReady: TADOQuery;
    sSpeedButton8: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton9: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sLabel26: TsLabel;
    sLabel25: TsLabel;
    edt_forTran: TsEdit;
    sLabel46: TsLabel;
    sSpeedButton7: TsSpeedButton;
    procedure sBitBtn22Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sPageControl1Change(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure edt_CarriageDblClick(Sender: TObject);
    procedure sBitBtn27Click(Sender: TObject);
    procedure edt_In_MathodChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure memo_Narrat1Change(Sender: TObject);
    procedure memo_Narrat1KeyPress(Sender: TObject; var Key: Char);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sButton1Click(Sender: TObject);
    procedure edt_AmdNoChange(Sender: TObject);

  private
    { Private declarations }
    APP707_SQL : string;

    procedure Readlist(fromDate, toDate : String;KeyValue : String='');overload;
    procedure Readlist(OrderSyntax : string = '');overload;
    procedure ReadDocument; override;
    procedure NewDocument; override;
    procedure CancelDocument; override;
    procedure SaveDocument(Sender : TObject); override;
    procedure DeleteDocument; override;
    procedure EditDocument; override;
    procedure ReadyDocument(DocNo : String);

    procedure AttachDocument;

    function ValidData:Boolean; override;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string;KeyCount : integer=0): Boolean; override;
    function CHECK_VALIDITY:String;

   //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
   procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  public
    { Public declarations }
  end;

var
  UI_APP707_frm: TUI_APP707_frm;

implementation

uses StrUtils, MessageDefine, TypeDefine, DateUtils, MSSQL, dlg_CreateDocumentChoice_APP707,
  dlg_CreateDocumentChoice_APP700, Commonlib, VarDefine, Dlg_Customer,
  Config_MSSQL, SQLCreator, KISCalendar, Dialog_CodeList,
  Dialog_AttachFromAPP700, Dialog_AttachFromINF707, Dlg_ErrorMessage,
  CodeContents, APP707_PRINT, Dlg_RecvSelect, DocumentSend, CreateDocuments ;

{$R *.dfm}

procedure TUI_APP707_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := APP707_SQL;
    SQL.Add(' WHERE DATEE between '+ QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('  ORDER BY DATEE ASC ');
    Open;

    if Trim(KeyValue) <> '' then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;

  end;
end;

procedure TUI_APP707_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  if (Sender as TsBitBtn).Name = sBitBtn1.Name then
  begin
    Readlist();
    Mask_fromDate.Text := Mask_SearchDate1.Text;
    Mask_toDate.Text := Mask_SearchDate2.Text;
  end
  else if (Sender as TsBitBtn).Name = sBitBtn22.Name then
  begin
    if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text])then
    begin
      MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
      Exit;
    end;
    Readlist(Mask_fromDate.Text,Mask_toDate.Text,'');
    Mask_SearchDate1.Text := Mask_fromDate.Text;
    Mask_SearchDate2.Text := Mask_toDate.Text;
  end;
end;

procedure TUI_APP707_frm.FormCreate(Sender: TObject);
begin
  inherited;
  APP707_SQL := qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
  ProgramControlType := ctView;

end;

procedure TUI_APP707_frm.FormShow(Sender: TObject);
begin
  inherited;
  Self.Caption := '취소불능화환신용장변경신청서';
  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;
//  ClearControlValue(page1_RightPanel);
//  ClearControlValue(page2_RightPanel);
//  ClearControlValue(page3_RightPanel);

  EnabledControlValue(maintno_Panel);
  EnabledControlValue(page1_RightPanel);
  EnabledControlValue(page2_RightPanel);
  EnabledControlValue(page3_RightPanel);

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadListBetween(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

end;

procedure TUI_APP707_frm.btnExitClick(Sender: TObject);
begin
  if DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.RollbackTrans;
  Close;
end;

procedure TUI_APP707_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APP707_frm := nil;
end;

procedure TUI_APP707_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;


//------------------------------------------------------------------------------
//기본정보
//------------------------------------------------------------------------------
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //차수
  edt_chasu.Text := qryListMSeq.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서유형
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;

//------------------------------------------------------------------------------
//개설은행
//------------------------------------------------------------------------------
  //신청일자
  edt_APPDATE.Text := qryListAPP_DATE.AsString;
  //개설방법
  edt_In_Mathod.Text := qryListIN_MATHOD.AsString;
  edt_In_Mathod1.Text := qryListmathod_Name.AsString;
  //개설의뢰은행
  edt_ApBank.Text := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString;
  edt_ApBank2.Text := qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString;
  edt_ApBank4.Text := qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;

//------------------------------------------------------------------------------
//통지은행
//------------------------------------------------------------------------------
  //(희망)통지은행
  edt_AdBank.Text := qryListAD_BANK.AsString;
  edt_AdBank1.Text := qryListAD_BANK1.AsString;
  edt_AdBank2.Text := qryListAD_BANK2.AsString;
  edt_AdBank3.Text := qryListAD_BANK3.AsString;
  edt_AdBank4.Text := qryListAD_BANK4.AsString;
  //신용공여
  edt_AdPay.Text := qryListAD_PAY.AsString;
  edt_AdPay1.Text := qryListpay_Name.AsString;

//------------------------------------------------------------------------------
//수입용도
//------------------------------------------------------------------------------
  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd1_1.Text := qryListImp_Name_1.AsString;

  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd2_1.Text := qryListImp_Name_2.AsString;

  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd3_1.Text := qryListImp_Name_3.AsString;

  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd4_1.Text := qryListImp_Name_4.AsString;

  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd5_1.Text := qryListImp_Name_5.AsString;

//------------------------------------------------------------------------------
// I/L 번호
//------------------------------------------------------------------------------
  // I/L 번호
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILno5.Text := qryListIL_NO5.AsString;
  // I/L 통화단위
  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;
  // I/L 금액
  edt_ILAMT1.Value := qryListIL_AMT1.AsCurrency;
  edt_ILAMT2.Value := qryListIL_AMT2.AsCurrency;
  edt_ILAMT3.Value := qryListIL_AMT3.AsCurrency;
  edt_ILAMT4.Value := qryListIL_AMT4.AsCurrency;
  edt_ILAMT5.Value := qryListIL_AMT5.AsCurrency;

//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;

//------------------------------------------------------------------------------
// 명의인정보
//------------------------------------------------------------------------------
  //명의인
  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  //식별부호
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  //주소
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

//Sender's Reference (L/C No)
  edt_cdNo.Text := qryListCD_NO.AsString;
//Date Of Issue
  edt_IssDate.Text := qryListISS_DATE.AsString;
//Number Of Amendment
  edt_AmdNo.Text := qryListAMD_NO.AsString;

//------------------------------------------------------------------------------
// Applicant
//------------------------------------------------------------------------------
  edt_Applic1.Text := qryListAPPLIC1.AsString;
  edt_Applic2.Text := qryListAPPLIC2.AsString;
  edt_Applic3.Text := qryListAPPLIC3.AsString;
  edt_Applic4.Text := qryListAPPLIC4.AsString;
  edt_Applic5.Text := qryListAPPLIC5.AsString;
//------------------------------------------------------------------------------
// Applicant
//------------------------------------------------------------------------------
  edt_Benefc.Text := qryListBENEFC.AsString;
  edt_Benefc1.Text := qryListBENEFC1.AsString;
  edt_Benefc2.Text := qryListBENEFC2.AsString;
  edt_Benefc3.Text := qryListBENEFC3.AsString;
  edt_Benefc4.Text := qryListBENEFC4.AsString;
  edt_Benefc5.Text := qryListBENEFC5.AsString;

// New Date of Expiry
  edt_exDate.Text := qryListEX_DATE.AsString;
//Increase of  Documentary Credit Amount
  edt_IncdCur.Text := qryListINCD_CUR.AsString;
  edt_IncdAmt.Value := qryListINCD_AMT.AsCurrency;
//Decrease of  Documentary Credit Amount
  edt_DecdCur.Text := qryListDECD_CUR.AsString;
  edt_DecdAmt.Value := qryListDECD_AMT.AsCurrency;
//New Documentary CreditAmount after Amendment
  edt_NwcdCur.Text := qryListNWCD_CUR.AsString;
  edt_NwcdAmt.Value := qryListNWCD_AMT.AsCurrency;
//New Documentary CreditAmount Before Amendment
  edt_BfcdCur.Text := qryListBFCD_CUR.AsString;
  edt_BfcdAmt.Value := qryListBFCD_AMT.AsCurrency;
//Maximum Credit Amount
  edt_CdMax.Text := qryListCD_MAX.AsString;
  edt_CdMax1.Text := qryListCDMAX_Name.AsString;
//Percentage Credit Amount Tolerance
  edt_CdPerp.Value := qryListCD_PERP.AsCurrency;
  edt_CdPerm.Value := qryListCD_PERM.AsCurrency;


//Latest Date of Shipment
  edt_lstDate.Text := qryListLST_DATE.AsString;
//------------------------------------------------------------------------------
//Shipment Period
//------------------------------------------------------------------------------
  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;
  edt_shipPD6.Text := qryListSHIP_PD6.AsString;
//운송수단
  edt_Carriage.Text := qryListCARRIAGE.AsString;
  edt_Carriage1.Text := qryListCarriage_Name.AsString;
//Port of Loading/Airport of Depature
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;
//Port of Discharge/Airport of Destination
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;
//Place of Taking in Charge/Dispatch from ···/Place of Receipt
  edt_loadOn.Text := qryListLOAD_ON.AsString;
//Place of Final Destination/For Transportation to ···/Place of Delivery
  edt_forTran.Text := qryListFOR_TRAN.AsString;
//Narrative
  memo_Narrat1.Lines.Text := qryListNARRAT_1.AsString;
//Additional Amounts Covered
  edt_Aacv1.Text := qryListAA_CV1.AsString;
  edt_Aacv2.Text := qryListAA_CV2.AsString;
  edt_Aacv3.Text := qryListAA_CV3.AsString;
  edt_Aacv4.Text := qryListAA_CV4.AsString;

end;

procedure TUI_APP707_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then ReadDocument;  
end;

procedure TUI_APP707_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

procedure TUI_APP707_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_APP707_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_APP707_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  if (Sender as TsPageControl).ActivePageIndex = 3 then
  begin
    //관리번호가있는 패널  숨김
    //maintno_Panel.Visible := False;
    //DBGrid2이 위치해 있는 패널 숨김
    sPanel52.Visible := False;
  end
  else
  begin
    //관리번호가있는 패널
    //maintno_Panel.Visible := True;
    //DBGrid2이 위치해 있는 패널
    sPanel52.Visible := True;
  end;

  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(dataSearch_Panel)
  else
    EnabledControlValue(dataSearch_Panel,False);
end;

procedure TUI_APP707_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  //트랜잭션 시작
  DMMssql.KISConnect.BeginTrans;

  dlg_CreateDocumentChoice_APP707_frm := Tdlg_CreateDocumentChoice_APP707_frm.Create(self);

  try
    case dlg_CreateDocumentChoice_APP707_frm.ShowModal of

      //신규등록
      mrRetry :
      begin
        NewDocument;
        //edt_MaintNo.SetFocus;
      end;
      
      //취소불능화환신용장개설응답서에서 선택
      mrOk :
      begin
        AttachDocument ;
      end;

      //취소불능화환신용장조건변경 응답서 목록에서 선택
      mrYes :
      begin
        AttachDocument ;
      end;

    end;
  finally
    FreeAndNil(dlg_CreateDocumentChoice_APP707_frm);
  end;

end;

procedure TUI_APP707_frm.NewDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;

  //관리번호가있는 패널
  maintno_Panel.Visible := True;
  //DBGrid3이 위치해 있는 패널
  sPanel52.Visible := True;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  ClearControlValue(maintno_Panel);
  ClearControlValue(page1_RightPanel);  //page1
  ClearControlValue(page2_RightPanel);  //page2
  ClearControlValue(page3_RightPanel);  //page3

  EnabledControlValue(maintno_Panel , False);
  EnabledControlValue(page1_RightPanel , False);
  EnabledControlValue(page2_RightPanel , False);
  EnabledControlValue(page3_RightPanel , False);
  EnabledControlValue(sPanel53);

//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  edt_APPDATE.Text := FormatDateTime('YYYYMMDD',now);

  //사용자
  edt_UserNo.Text := LoginData.sID;

  //관리번호 설정
  edt_MaintNo.ReadOnly := False;
  edt_MaintNo.Color := clWhite;

  //등록일자
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);

  //문서기능
  edt_msg1.ReadOnly := False;
  edt_msg1.Color := clWhite;
  edt_msg1.Text := '9';
  sBitBtn2.Enabled := True;

  //유형
  edt_msg2.ReadOnly := False;
  edt_msg2.Color := clWhite;
  edt_msg2.Text := 'AB';
  sBitBtn3.Enabled := True;

//------------------------------------------------------------------------------
//명의인 정보
//------------------------------------------------------------------------------
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try

    with Dlg_Customer_frm do
    begin
      if isValue('00000') then
        edt_EXName1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
        edt_EXName2.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        edt_EXName3.Text := DataSource1.DataSet.FieldByName('JENJA').AsString;
        edt_EXAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
        edt_EXAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;

//------------------------------------------------------------------------------
//Applicant
//------------------------------------------------------------------------------
  Config_MSSQL_frm := TConfig_MSSQL_frm.Create(Self);
  try
    with Config_MSSQL_frm do
    begin
        qryConfig.Active := True;
        edt_Applic1.Text := qryConfigENAME1.AsString;
        edt_Applic2.Text := qryConfigEADDR1.AsString;
        edt_Applic3.Text := qryConfigEADDR2.AsString;
        edt_Applic4.Text := qryConfigEADDR3.AsString;
        edt_Applic5.Text := qryConfigENAME3.AsString;
    end;
  finally
    Config_MSSQL_frm.qryConfig.Active := False;
    FreeAndNil(Config_MSSQL_frm);
  end;


end;

procedure TUI_APP707_frm.CancelDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(True);

  //문서기능 , 유형
  sBitBtn2.Enabled := False;
  sBitBtn3.Enabled := False;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(page1_RightPanel);
  EnabledControlValue(page2_RightPanel);
  EnabledControlValue(page3_RightPanel);
  EnabledControlValue(maintno_Panel);
  EnabledControlValue(sPanel53,False);

//  ClearControlValue(page1_RightPanel);
//  ClearControlValue(page2_RightPanel);
//  ClearControlValue(page3_RightPanel);
//  ClearControlValue(maintno_Panel);

  //등록일자 초기화
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);


end;

procedure TUI_APP707_frm.btnCancelClick(Sender: TObject);
var
  CreateDate : TDateTime;
begin
  inherited;

  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

  IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_CANCEL),'취소확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
  begin
    CancelDocument;

    //----------------------------------------------------------------------------
    //새로고침
    //----------------------------------------------------------------------------
      CreateDate := ConvertStr2Date(sMaskEdit1.Text);
      Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
              FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)));
  end;


end;

function TUI_APP707_frm.ValidData: Boolean;
begin
  //저장시 유효성 검사 (필수입력사항 체크)

end;

procedure TUI_APP707_frm.SaveDocument(Sender: TObject);
var
  SqlCreate : TSQLCreate;
  DocNo , DocCount: String;
  CreateDate : TDateTime;
  MSeqVal : Integer;
begin
  inherited;
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  try
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
       Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
       if Dlg_ErrorMessage_frm.Run_ErrorMessage( '취소불능화환신용장 변경 신청서' ,CHECK_VALIDITY ) Then
       begin
         FreeAndNil(Dlg_ErrorMessage_frm);
         Exit;
       end;
    end;
//------------------------------------------------------------------------------
//  SQL생성기
//------------------------------------------------------------------------------
    SqlCreate := TSQLCreate.Create;
    try
      with SqlCreate do
      begin
        DocNo := edt_MaintNo.Text;
        //SQL update 문에 조건으로 들어가기때문에 수정이 불가능한 edt_chasu.text를 사용하는것이 맞음
        DocCount := edt_chasu.Text;

        //관리번호 생성
        case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
            ADDWhere('MSEQ',DocCount);
          end;
        end;

       //-----------------------------------------------------------------------
       // 문서헤더
       //-----------------------------------------------------------------------
       SQLHeader('APP707_1');

       //문서저장구분(0:임시, 1:저장)
        ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));

       //등록일자
        ADDValue('DATEE',sMaskEdit1.Text);

       //유저ID
        ADDValue('USER_ID',edt_UserNo.Text);

       //메세지1
        ADDValue('MESSAGE1','9');
       //메세지2
        ADDValue('MESSAGE2','AB');

       //APP_DATE
        ADDValue('APP_DATE',edt_APPDATE.Text);

       //조건변경횟수 AMD_NO
       ADDValue('AMD_NO', edt_AmdNo.Text);
       //보여주기용 차수(관리번호옆)
       ADDValue('MSEQ', edt_AmdNo.Text);


       //----------------------------------------------------------------------
       // 개설은행
       //----------------------------------------------------------------------
        //개설방법
        ADDValue('IN_MATHOD',edt_In_Mathod.Text);

        //개설은행
        ADDValue('AP_BANK',edt_ApBank.Text);
        ADDValue('AP_BANK1',edt_ApBank1.Text);
        ADDValue('AP_BANK2',edt_ApBank2.Text);
        ADDValue('AP_BANK3',edt_ApBank3.Text);
        ADDValue('AP_BANK4',edt_ApBank4.Text);
        ADDValue('AP_BANK5',edt_ApBank5.Text);

       //----------------------------------------------------------------------
       //통지은행
       //----------------------------------------------------------------------
        ADDValue('AD_BANK',edt_AdBank.Text);
        ADDValue('AD_BANK1',edt_AdBank1.Text);
        ADDValue('AD_BANK2',edt_AdBank2.Text);
        ADDValue('AD_BANK3',edt_AdBank3.Text);
        ADDValue('AD_BANK4',edt_AdBank4.Text);

        //신용공여
        ADDValue('AD_PAY',edt_AdPay.Text);

       //----------------------------------------------------------------------
       //수입용도
       //----------------------------------------------------------------------
        ADDValue('IMP_CD1',edt_ImpCd1.Text);
        ADDValue('IMP_CD2',edt_ImpCd2.Text);
        ADDValue('IMP_CD3',edt_ImpCd3.Text);
        ADDValue('IMP_CD4',edt_ImpCd4.Text);
        ADDValue('IMP_CD5',edt_ImpCd5.Text);

       //----------------------------------------------------------------------
       //I/L번호
       //----------------------------------------------------------------------
        ADDValue('IL_NO1',edt_ILno1.Text);
        ADDValue('IL_NO2',edt_ILno2.Text);
        ADDValue('IL_NO3',edt_ILno3.Text);
        ADDValue('IL_NO4',edt_ILno4.Text);
        ADDValue('IL_NO5',edt_ILno5.Text);

        ADDValue('IL_CUR1',edt_ILCur1.Text);
        ADDValue('IL_CUR2',edt_ILCur2.Text);
        ADDValue('IL_CUR3',edt_ILCur3.Text);
        ADDValue('IL_CUR4',edt_ILCur4.Text);
        ADDValue('IL_CUR5',edt_ILCur5.Text);

        ADDValue('IL_AMT1',edt_ILAMT1.Text);
        ADDValue('IL_AMT2',edt_ILAMT2.Text);
        ADDValue('IL_AMT3',edt_ILAMT3.Text);
        ADDValue('IL_AMT4',edt_ILAMT4.Text);
        ADDValue('IL_AMT5',edt_ILAMT5.Text);

       //----------------------------------------------------------------------
       //기타정보
       //----------------------------------------------------------------------
        ADDValue('AD_INFO1',edt_AdInfo1.Text);
        ADDValue('AD_INFO2',edt_AdInfo2.Text);
        ADDValue('AD_INFO3',edt_AdInfo3.Text);
        ADDValue('AD_INFO4',edt_AdInfo4.Text);
        ADDValue('AD_INFO5',edt_AdInfo5.Text);

        //Sender's Reference(L/C No)
        ADDValue('CD_NO',edt_cdNo.Text);

        //Date Of Issue
        ADDValue('ISS_DATE',edt_IssDate.Text);

        //New Date of Expiry
        ADDValue('EX_DATE',edt_exDate.Text);

      end;

      with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            if sCheckBox1.Checked then
              Clipboard.AsText := SQL.Text;
              //ShowMessage(SQL.Text);
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;
//------------------------------------------------------------------------------
// APP707_2.DB
//------------------------------------------------------------------------------
      with SqlCreate do
      begin
      //문서헤더
        SQLHeader('APP707_2');

      // 관리번호
        Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
              ADDWhere('MSeq',DocCount);
            end;
        end;

        //----------------------------------------------------------------------
        //명의인 정보
        //----------------------------------------------------------------------
          //명의인
          ADDValue('EX_NAME1',edt_EXName1.Text);
          ADDValue('EX_NAME2',edt_EXName2.Text);
          //식별부호
          ADDValue('EX_NAME3',edt_EXName3.Text);
          //주소
          ADDValue('EX_ADDR1',edt_EXAddr1.Text);
          ADDValue('EX_ADDR2',edt_EXAddr2.Text);

        //Number Of Amendment
          ADDValue('Amd_No',edt_AmdNo.Text);
        //보여주기용 차수(관리번호옆)
          ADDValue('MSEQ', edt_AmdNo.Text);


        //Applicant
          ADDValue('APPLIC1',edt_Applic1.Text);
          ADDValue('APPLIC2',edt_Applic2.Text);
          ADDValue('APPLIC3',edt_Applic3.Text);
          ADDValue('APPLIC4',edt_Applic4.Text);
          ADDValue('APPLIC5',edt_Applic5.Text);

        //Beneficiary
          ADDValue('BENEFC',edt_Benefc.Text);
          ADDValue('BENEFC1',edt_Benefc1.Text);
          ADDValue('BENEFC2',edt_Benefc2.Text);
          ADDValue('BENEFC3',edt_Benefc3.Text);
          ADDValue('BENEFC4',edt_Benefc4.Text);
          ADDValue('BENEFC5',edt_Benefc5.Text);

        //Increase of  Documentary Credit Amount
          ADDValue('INCD_CUR',edt_IncdCur.Text);
          ADDValue('INCD_AMT',edt_IncdAmt.Text);

        //Decrease of  Documentary Credit Amount
          ADDValue('DECD_CUR',edt_DecdCur.Text);
          ADDValue('DECD_AMT',edt_DecdAmt.Text);

        //New Documentary CreditAmount after Amendment
          ADDValue('NWCD_CUR',edt_NwcdCur.Text);
          ADDValue('NWCD_AMT',edt_NwcdAmt.Text);

        //New Documentary CreditAmount before Amendment
          ADDValue('BFCD_CUR',edt_BfcdCur.Text);
          ADDValue('BFCD_AMT',edt_BfcdAmt.Text);

        //Maximum Credit Amount
          ADDValue('CD_MAX',edt_CdMax.Text);

        //Percentage Credit Amount Tolerance
          ADDValue('CD_PERP',edt_CdPerp.Text);
          ADDValue('CD_PERM',edt_CdPerm.Text);

        //Latest Date of Shipment
          ADDValue('LST_DATE',edt_IssDate.Text);

        //Shipment Period
          ADDValue('SHIP_PD1',edt_shipPD1.Text);
          ADDValue('SHIP_PD2',edt_shipPD2.Text);
          ADDValue('SHIP_PD3',edt_shipPD3.Text);
          ADDValue('SHIP_PD4',edt_shipPD4.Text);
          ADDValue('SHIP_PD5',edt_shipPD5.Text);
          ADDValue('SHIP_PD6',edt_shipPD6.Text);

        //운송수단
          ADDValue('CARRIAGE',edt_Carriage.Text);

        //선적항
          ADDValue('SUNJUCK_PORT',edt_SunjukPort.Text);

        //도착항
          ADDValue('DOCHACK_PORT',edt_dochackPort.Text);

        //수탁발송지
          ADDValue('LOAD_ON',edt_loadOn.Text);

        //최종목적지
          ADDValue('FOR_TRAN',edt_forTran.Text);

        //Narrative
          ADDValue('NARRAT_1',memo_Narrat1.Lines.Text);

        //Additional Amounts Covered
          ADDValue('AA_CV1',edt_Aacv1.Text);
          ADDValue('AA_CV2',edt_Aacv2.Text);
          ADDValue('AA_CV3',edt_Aacv3.Text);
          ADDValue('AA_CV4',edt_Aacv4.Text);
      end;

       with TADOQuery.Create(nil) do
       begin
         try
           Connection := DMMssql.KISConnect;
           SQL.Text := SQLCreate.CreateSQL;
           if sCheckBox1.Checked then
             Clipboard.AsText := SQL.Text;
             //ShowMessage(SQL.Text);
           ExecSQL;
         finally
           Close;
           Free;
         end;
       end;

    finally
      SqlCreate.Free;
    end;
 //------------------------------------------------------------------------------
 // 프로그램 제어
 //------------------------------------------------------------------------------
   ProgramControlType := ctView;
 //------------------------------------------------------------------------------
 // 버튼정리
 //------------------------------------------------------------------------------
   ButtonEnable(True);

   //문서유형,기능
   sBitBtn2.Enabled := False;
   sBitBtn3.Enabled := False;
 //------------------------------------------------------------------------------
 // 접근제어
 //------------------------------------------------------------------------------
   sPageControl1.ActivePageIndex:= 0;
    sPageControl1Change(sPageControl1);
   sDBGrid1.Enabled := True;
   sDBGrid2.Enabled := True;

 //------------------------------------------------------------------------------
 // 데이터제어
 //------------------------------------------------------------------------------
   EnabledControlValue(page1_RightPanel);
   EnabledControlValue(page2_RightPanel);
   EnabledControlValue(page3_RightPanel);
   EnabledControlValue(maintno_Panel);
   EnabledControlValue(sPanel53,False);

   ClearControlValue(page1_RightPanel);
   ClearControlValue(page2_RightPanel);
   ClearControlValue(page3_RightPanel);
   ClearControlValue(maintno_Panel);

  //----------------------------------------------------------------------------
  //새로고침
  //----------------------------------------------------------------------------
    //등록일자 초기화
    sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);

    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);
             
    MessageBox(Self.Handle,MSG_SYSTEM_SAVE_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);

   //트랜잭션
   DMMssql.KISConnect.CommitTrans;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;

  end;
end;

procedure TUI_APP707_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

procedure TUI_APP707_frm.DeleteDocument;
var
  maint_no,mseq_no : String;
  nCursor : Integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------

  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin

    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
    mseq_no := edt_chasu.Text;
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;
        SQL.Text :=  'DELETE FROM APP707_1 WHERE MAINT_NO =' + QuotedStr(maint_no)+' AND MSEQ = '+QuotedStr(mseq_no)  ;
        ExecSQL;

        SQL.Text :=  'DELETE FROM APP707_2 WHERE MAINT_NO =' + QuotedStr(maint_no)+' AND MSEQ = '+QuotedStr(mseq_no)  ;
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;
    finally
      if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      Close;
      Free;
    end;

    try

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TUI_APP707_frm.btnDelClick(Sender: TObject);
begin
  inherited;

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(page1_RightPanel);
    ClearControlValue(page2_RightPanel);
    ClearControlValue(page3_RightPanel);
    ClearControlValue(maintno_Panel);
  end;
end;

procedure TUI_APP707_frm.EditDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;

//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
  EnabledControlValue(maintno_Panel,False);
  //관리번호 수정불가
  edt_MaintNo.Enabled := False;
  //등록일자 수정가능
  sMaskEdit1.ReadOnly := False;
  btn_Cal.Enabled := True;
  //문서기능 수정가능
  edt_msg1.ReadOnly := False;
  sBitBtn7.Enabled := True;
  //유형 수정가능
  edt_msg2.ReadOnly := False;
  sBitBtn8.Enabled := True;

  EnabledControlValue(page1_RightPanel,False);
  EnabledControlValue(page2_RightPanel,False);
  EnabledControlValue(page3_RightPanel,False);
  EnabledControlValue(sPanel53);

//------------------------------------------------------------------------------
// 버튼설정
//------------------------------------------------------------------------------
  ButtonEnable(false);

end;

procedure TUI_APP707_frm.btnEditClick(Sender: TObject);
begin
  inherited;

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
    EditDocument
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가',MB_OK+MB_ICONINFORMATION)
end;

procedure TUI_APP707_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_APP707_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top + (Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_APP707_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
      901 : sMaskEdit1DblClick(sMaskEdit1);
      902 : sMaskEdit1DblClick(Mask_SearchDate1);
      903 : sMaskEdit1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_APP707_frm.edt_CarriageDblClick(Sender: TObject);
begin
   inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    //if (Sender as TsEdit).ReadOnly = False then
    //begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
        //선택된 코드 출력
        (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

        //선택된 코드의 값까지 출력
        case (Sender as TsEdit).Tag of
           //운송수단
           124 :  edt_Carriage1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
        end;
      end;
    //end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_APP707_frm.sBitBtn27Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    124 : edt_CarriageDblClick(edt_Carriage);

  end;
end;

procedure TUI_APP707_frm.Readlist(OrderSyntax: string);
begin
   if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 4 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  
  with qryList do
  begin
    Close;
    sql.Text := APP707_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : SQL.Add(' WHERE A707_1.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ASC ');
    end;

    Open;
    
  end;
end;

procedure TUI_APP707_frm.AttachDocument;
var
  DocNo : String;
  DocCount : Integer;
begin

  if dlg_CreateDocumentChoice_APP707_frm.ModalResult = mrOk then
  begin

    Dialog_AttachFromAPP700_frm := TDialog_AttachFromAPP700_frm.Create(Self);

      try

          DocNo := Dialog_AttachFromAPP700_frm.openDialog;

          IF Trim(DocNo) = '' then Exit;
          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with sp_attachAPP707fromINF700 do
          begin
            Close;
            Parameters.ParamByName('@DocNo').Value := DocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
              DocCount := Parameters.ParamByName('@DOC_COUNT').Value;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;  

       // ReadListBetween(오늘날짜 ~, 오늘날짜 , 새로복사 된  관리번호)
        if  ReadListBetween(FormatDateTime('YYYYMMDD',Now),FormatDateTime('YYYYMMDD',Now),DocNo,DocCount) Then
        begin
          EditDocument;
          edt_MaintNo.ReadOnly := True;
        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');

          //MessageBox(Self.Handle,MSG_SYSTEM_COPY_OK,'복사완료',MB_OK+MB_ICONINFORMATION);

      finally

        FreeAndNil(Dialog_AttachFromAPP700_frm);

      end;

  end
  else if dlg_CreateDocumentChoice_APP707_frm.ModalResult = mrYes then
  begin

    Dialog_AttachFromINF707_frm := TDialog_AttachFromINF707_frm.Create(Self);

      try

          DocNo :=  Dialog_AttachFromINF707_frm.openDialog;

          IF Trim(DocNo) = '' then Exit;
          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with sp_attachAPP707fromINF707 do
          begin
            Close;
            Parameters.ParamByName('@DocNo').Value := DocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
              DocCount := Parameters.ParamByName('@DOC_COUNT').Value;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;

        if  ReadListBetween(FormatDateTime('YYYYMMDD',Now),FormatDateTime('YYYYMMDD',Now),DocNo,DocCount) Then
        begin
          EditDocument;
          edt_MaintNo.ReadOnly := True;
        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');

          //MessageBox(Self.Handle,MSG_SYSTEM_COPY_OK,'복사완료',MB_OK+MB_ICONINFORMATION);

      finally

        FreeAndNil(Dialog_AttachFromINF707_frm);

      end;
  end;

//버튼,에디트,패널 정리-------------------------------------------------------
  edt_msg1.ReadOnly := False;
  edt_msg1.Color := clWhite;
  edt_msg2.ReadOnly := False;
  edt_msg2.Color := clWhite;

  sBitBtn7.Enabled := True;
  sBitBtn8.Enabled := True;

  //관리번호가있는 패널
  maintno_Panel.Visible := True;
  //DBGrid3이 위치해 있는 패널
  sPanel52.Visible := True;
end;

function TUI_APP707_frm.ReadListBetween(fromDate, toDate, KeyValue: string;
  KeyCount: integer): Boolean;
begin                                                                              

  Result := false;
  with qryList do
  begin

    Close;
    SQL.Text := APP707_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ');
    Open;

    //ShowMessage(SQL.Text);

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO;MSEQ',VarArrayOf([KeyValue,KeyCount]),[]);
    end;
  end;
end;



function TUI_APP707_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;

begin
  ErrMsg := TStringList.Create;

  Try
    IF Trim(edt_MaintNo.Text) = '' then
      ErrMsg.Add('관리번호를 입력해야 합니다');

    IF Trim(edt_In_Mathod.Text) = '' then
      ErrMsg.Add('[Page1] 개설방법을 입력해야 합니다');

    IF Trim(edt_ApBank.Text) = '' then
      ErrMsg.Add('[Page1] 개설은행코드를 입력해야 합니다');

    IF Trim(edt_ApBank1.Text) = '' then
      ErrMsg.Add('[Page1] 개설은행을 입력해야 합니다');

    IF (Trim(edt_Benefc.Text) = '') AND (Trim(edt_Benefc1.Text) = '' ) then
      ErrMsg.Add('[Page2] Beneficiary를 입력해야 합니다');

    if (edt_IncdAmt.Value <> 0) and (edt_DecdAmt.Value <> 0) then
      ErrMsg.Add('[Page2] 신용장증액분과 신용장감액분 중 한가지만 입력가능합니다.');

    if (edt_IncdAmt.Value <> 0) and (edt_DecdAmt.Value = 0) then
    begin
      if (edt_NwcdAmt.Value = 0) or (Trim(edt_CdMax.Text) = '') or ((edt_CdPerp.Value = 0) and (edt_CdPerm.Value =0)) then
        ErrMsg.Add('[Page2] 신용장증액분 입력시 변경후 최종금액 , 과부족허용율 사용여부 , 과부족허용율을 입력해야합니다.');
    end;

    if (edt_DecdAmt.Value <> 0) and (edt_IncdAmt.Value = 0) then
    begin
      if (edt_NwcdAmt.Value = 0) or (Trim(edt_CdMax.Text) = '') or ((edt_CdPerp.Value = 0) and (edt_CdPerm.Value =0)) then
        ErrMsg.Add('[Page2] 신용장감액분 입력시 변경후 최종금액 , 과부족허용율 사용여부 , 과부족허용율을 입력해야합니다.');
    end;

    if ((edt_IncdAmt.Value <> 0) or (edt_DecdAmt.Value <> 0)) and (edt_BfcdAmt.Value <> 0) then
      ErrMsg.Add('[Page2] 변경전 최종금액 입력시 신용장증액분과 신용장감액분은 입력할수없습니다.');

    if edt_BfcdAmt.Value <> 0 then
    begin
      if (Trim(edt_CdMax.Text) = '') or ((edt_CdPerp.Value = 0) and (edt_CdPerm.Value =0)) then
        ErrMsg.Add('[Page2] 변경전 최종금액 입력시 과부족허용율 사용여부와 과부족허용율을 입력해야합니다.');
    end;


    if (edt_Carriage.Text <> 'DQ') and (Trim(edt_loadOn.Text) <> '') then
      ErrMsg.Add('[Page3] 수탁(발송)지는 운송수단이 DQ 일때만 입력가능합니다.');

    if (edt_Carriage.Text <> 'DQ') and (Trim(edt_forTran.Text) <> '') then
      ErrMsg.Add('[Page3] 최종목적지는 운송수단이 DQ 일때만 입력가능합니다.');

    if (edt_Carriage.Text = 'DQ') and (Trim(edt_SunjukPort.Text) <> '') then
      ErrMsg.Add('[Page3] 운송수단이 DQ(복합운송/ETC)일때는 선적항을 입력할 수 없습니다.');

    if (edt_Carriage.Text = 'DQ') and (Trim(edt_dochackPort.Text) <> '') then
      ErrMsg.Add('[Page3] 운송수단이 DQ(복합운송/ETC)일때는 도착항을 입력할 수 없습니다.');
    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TUI_APP707_frm.edt_In_MathodChange(Sender: TObject);
var
  CodeRecord : TCodeRecord;
  sGroup,sCode : String;
begin
  // Edit에 notFound가 출력되는것을 방지
  if not (Sender as TsEdit).Focused then Exit;

  sGroup := (Sender as TsEdit).Hint;
  sCode := (Sender as TsEdit).Text;

  CodeRecord := DMCodeContents.getCodeName(sGroup,sCode);

  case (Sender as TsEdit).Tag of
    //개설방법
    101 : edt_In_Mathod1.Text := CodeRecord.sName;
    //신용공여
    104 : edt_AdPay1.Text := CodeRecord.sName;
    //수입용도
    105 : edt_ImpCd1_1.Text := CodeRecord.sName;
    106 : edt_ImpCd2_1.Text := CodeRecord.sName;
    107 : edt_ImpCd3_1.Text := CodeRecord.sName;
    108 : edt_ImpCd4_1.Text := CodeRecord.sName;
    109 : edt_ImpCd5_1.Text := CodeRecord.sName;
    //Maximum Credit Amount
    120 : edt_CdMax1.Text := CodeRecord.sName;
    //운송수단
    124 : edt_Carriage1.Text := CodeRecord.sName;

  end;
end;

procedure TUI_APP707_frm.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;
  if not qryList.Active then exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    KeyValue := qryListMAINT_NO.AsString;
    Readlist(Mask_fromDate.Text, Mask_toDate.Text, '');
    qryList.Locate('MAINT_NO',KeyValue,[]);
  end;

end;

procedure TUI_APP707_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  APP707_PRINT_frm := TAPP707_PRINT_frm.Create(Self);
  try

    APP707_PRINT_frm.PrintDocument(qryList.Fields,True);

  finally
    FreeAndNil( APP707_PRINT_frm );
  end;


end;

procedure TUI_APP707_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

procedure TUI_APP707_frm.memo_Narrat1Change(Sender: TObject);
var
  memorow,memoCol : integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);

  sLabel2.Caption := IntToStr(memoRow+1)+'행   ' + IntToStr(memoCol)+'열';

end;

procedure TUI_APP707_frm.memo_Narrat1KeyPress(Sender: TObject;
  var Key: Char);
var
  limitcount : Integer;
begin
  limitcount := 35;
  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;
end;

procedure TUI_APP707_frm.PopupMenu1Popup(Sender: TObject);
begin
  if qryList.RecordCount = 0 then
  begin
    N1.Enabled := False;
    N3.Enabled := False;
    N4.Enabled := False;
    d1.Enabled := False;
  end
  else
  begin
    N1.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
    N3.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
    N4.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
  end;
end;

procedure TUI_APP707_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APP707(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APP707';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APP707_1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

//        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
//                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
//                 RecvDoc);
        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APP707_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_APP707_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_APP707_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_APP707_frm.edt_AmdNoChange(Sender: TObject);
begin
  inherited;
   edt_chasu.Text := edt_AmdNo.Text;
end;

procedure TUI_APP707_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

   if (ActiveControl.Name = 'edt_EXAddr2') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 1;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'edt_CdPerm') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 2;
     Self.KeyPreview := False;
   end
   else
   begin
     Self.KeyPreview := True;
     inherited;
   end;
end;

end.



