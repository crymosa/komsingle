unit UI_DOMOFB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, sButton, DB, ADODB, StdCtrls,
  sComboBox, ExtCtrls, sSplitter, Buttons, sSpeedButton, Grids, DBGrids,
  acDBGrid, sCustomComboEdit, sCurrEdit, sCurrencyEdit, DBCtrls, sDBMemo,
  sDBEdit, sMemo, sLabel, sGroupBox, ComCtrls, sPageControl, sCheckBox,
  sBitBtn, Mask, sMaskEdit, sEdit, sPanel, TypeDefine;

type
  TUI_DOMOFB_frm = class(TChildForm_frm)
    maintno_Panel: TsPanel;
    edt_MaintNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    sCheckBox1: TsCheckBox;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    sPanel5: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    sButton2: TsButton;
    sButton4: TsButton;
    qryListMAINT_NO: TStringField;
    qryListMaint_Rff: TStringField;
    qryListChk1: TStringField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListDATEE: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NO: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ADDR1: TStringField;
    qryListSR_ADDR2: TStringField;
    qryListSR_ADDR3: TStringField;
    qryListUD_CODE: TStringField;
    qryListUD_RENO: TStringField;
    qryListUD_NO: TStringField;
    qryListUD_NAME1: TStringField;
    qryListUD_NAME2: TStringField;
    qryListUD_NAME3: TStringField;
    qryListUD_ADDR1: TStringField;
    qryListUD_ADDR2: TStringField;
    qryListUD_ADDR3: TStringField;
    qryListOFR_NO: TStringField;
    qryListOFR_DATE: TStringField;
    qryListOFR_SQ: TStringField;
    qryListOFR_SQ1: TStringField;
    qryListOFR_SQ2: TStringField;
    qryListOFR_SQ3: TStringField;
    qryListOFR_SQ4: TStringField;
    qryListOFR_SQ5: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK_1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListTSTINST: TStringField;
    qryListTSTINST1: TStringField;
    qryListTSTINST2: TStringField;
    qryListTSTINST3: TStringField;
    qryListTSTINST4: TStringField;
    qryListTSTINST5: TStringField;
    qryListPCKINST: TStringField;
    qryListPCKINST1: TStringField;
    qryListPCKINST2: TStringField;
    qryListPCKINST3: TStringField;
    qryListPCKINST4: TStringField;
    qryListPCKINST5: TStringField;
    qryListPAY: TStringField;
    qryListPAY_ETC: TStringField;
    qryListPAY_ETC1: TStringField;
    qryListPAY_ETC2: TStringField;
    qryListPAY_ETC3: TStringField;
    qryListPAY_ETC4: TStringField;
    qryListPAY_ETC5: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListTERM_DEL: TStringField;
    qryListTERM_NAT: TStringField;
    qryListTERM_LOC: TStringField;
    qryListORGN1N: TStringField;
    qryListORGN1LOC: TStringField;
    qryListORGN2N: TStringField;
    qryListORGN2LOC: TStringField;
    qryListORGN3N: TStringField;
    qryListORGN3LOC: TStringField;
    qryListORGN4N: TStringField;
    qryListORGN4LOC: TStringField;
    qryListORGN5N: TStringField;
    qryListORGN5LOC: TStringField;
    qryListTRNS_ID: TStringField;
    qryListLOADD: TStringField;
    qryListLOADLOC: TStringField;
    qryListLOADTXT: TStringField;
    qryListLOADTXT1: TStringField;
    qryListLOADTXT2: TStringField;
    qryListLOADTXT3: TStringField;
    qryListLOADTXT4: TStringField;
    qryListLOADTXT5: TStringField;
    qryListDEST: TStringField;
    qryListDESTLOC: TStringField;
    qryListDESTTXT: TStringField;
    qryListDESTTXT1: TStringField;
    qryListDESTTXT2: TStringField;
    qryListDESTTXT3: TStringField;
    qryListDESTTXT4: TStringField;
    qryListDESTTXT5: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYCUR: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMTCUR: TStringField;
    qryListCODE: TStringField;
    qryListDOC_NAME: TStringField;
    qryListCODE_1: TStringField;
    qryListDOC_NAME_1: TStringField;
    qryListCODE_2: TStringField;
    qryListDOC_NAME_2: TStringField;
    qryListTRANSNME: TStringField;
    qryListLOADDNAME: TStringField;
    qryListDESTNAME: TStringField;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_CHK: TStringField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME_CHK: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    qryDetailSTQTY: TBCDField;
    qryDetailSTQTY_G: TStringField;
    qryDetailSTAMT: TBCDField;
    qryDetailSTAMT_G: TStringField;
    qryListHS_CODE: TStringField;
    sSplitter1: TsSplitter;
    sSplitter3: TsSplitter;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter9: TsSplitter;
    page1_Right: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SRADDR3: TsEdit;
    edt_SRADDR2: TsEdit;
    edt_SRADDR1: TsEdit;
    edt_SRNAME3: TsEdit;
    edt_SRNAME2: TsEdit;
    edt_SRNAME1: TsEdit;
    edt_SRNO: TsEdit;
    edt_SRCODE: TsEdit;
    edt_OFRNO: TsEdit;
    edt_OFRDATE: TsMaskEdit;
    edt_UDCODE: TsEdit;
    edt_UDNO: TsEdit;
    edt_UDNAME1: TsEdit;
    edt_UDNAME2: TsEdit;
    edt_UDNAME3: TsEdit;
    edt_UDADDR1: TsEdit;
    edt_UDADDR2: TsEdit;
    edt_UDADDR3: TsEdit;
    edt_UDRENO: TsEdit;
    edt_OFRSQ1: TsEdit;
    edt_OFRSQ2: TsEdit;
    edt_OFRSQ3: TsEdit;
    edt_OFRSQ4: TsEdit;
    edt_OFRSQ5: TsEdit;
    sPanel23: TsPanel;
    sPanel6: TsPanel;
    edt_HSCODE: TsMaskEdit;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    edt_TSTINST1: TsEdit;
    edt_TSTINST2: TsEdit;
    edt_TSTINST3: TsEdit;
    edt_TSTINST4: TsEdit;
    edt_TSTINST5: TsEdit;
    edt_PCKINST1: TsEdit;
    edt_PCKINST2: TsEdit;
    edt_PCKINST3: TsEdit;
    edt_PCKINST4: TsEdit;
    edt_PCKINST5: TsEdit;
    edt_PAYETC1: TsEdit;
    edt_PAYETC2: TsEdit;
    edt_PAYETC3: TsEdit;
    edt_PAYETC4: TsEdit;
    edt_PAYETC5: TsEdit;
    page1_Left: TsPanel;
    sTabSheet3: TsTabSheet;
    sSplitter7: TsSplitter;
    page3_Left: TsPanel;
    page3_Right: TsPanel;
    sSpeedButton14: TsSpeedButton;
    edt_ORGN1N: TsEdit;
    edt_ORGN1LOC: TsEdit;
    edt_ORGN2LOC: TsEdit;
    edt_ORGN2N: TsEdit;
    edt_ORGN3N: TsEdit;
    edt_ORGN3LOC: TsEdit;
    edt_ORGN4LOC: TsEdit;
    edt_ORGN4N: TsEdit;
    edt_ORGN5N: TsEdit;
    edt_ORGN5LOC: TsEdit;
    sPanel12: TsPanel;
    memo_REMARK1: TsMemo;
    edt_TRNSID: TsEdit;
    edt_TRNSIDNAME: TsEdit;
    sPanel14: TsPanel;
    sPanel13: TsPanel;
    edt_LOADD: TsEdit;
    edt_LOADLOC: TsEdit;
    edt_LOADTXT1: TsEdit;
    edt_LOADTXT2: TsEdit;
    edt_LOADTXT3: TsEdit;
    edt_LOADTXT4: TsEdit;
    edt_LOADTXT5: TsEdit;
    sPanel15: TsPanel;
    sTabSheet5: TsTabSheet;
    sSplitter5: TsSplitter;
    sPanel3: TsPanel;
    detailDBGrid: TsDBGrid;
    sPanel9: TsPanel;
    sSpeedButton10: TsSpeedButton;
    Btn_DetailAdd: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    Btn_DetailMod: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    sSpeedButton15: TsSpeedButton;
    Btn_DetailOk: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    Btn_DetailCancel: TsSpeedButton;
    sPanel4: TsPanel;
    sTabSheet6: TsTabSheet;
    sSplitter4: TsSplitter;
    sDBGrid1: TsDBGrid;
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn4: TsBitBtn;
    sPanel2: TsPanel;
    edt_DESTTXT5: TsEdit;
    edt_DESTTXT4: TsEdit;
    edt_DESTTXT3: TsEdit;
    edt_DESTTXT2: TsEdit;
    edt_DESTTXT1: TsEdit;
    edt_DEST: TsEdit;
    edt_DESTLOC: TsEdit;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    sPanel22: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sSpeedButton16: TsSpeedButton;
    sPanel24: TsPanel;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sPanel18: TsPanel;
    edt_TQTYCUR: TsEdit;
    edt_TAMTCUR: TsEdit;
    edt_TQTY: TsCurrencyEdit;
    edt_TAMT: TsCurrencyEdit;
    sPanel32: TsPanel;
    LeftGrid_Panel: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure qryListChk2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure btn_calClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
    DOMOFB_SQL : string;
    DOMOFBDETAIL_SQL : String;
    procedure ReadDocument;
    procedure Readlist(fromDate, toDate : String; KeyValue : String=''); overload;
    procedure Readlist(OrderSyntax : string = ''); overload;
    procedure DeleteDocument;
  public
    { Public declarations }
  protected
    { protected declaration }
    ProgramControlType : TProgramControlType;
  end;

var
  UI_DOMOFB_frm: TUI_DOMOFB_frm;

implementation

uses MSSQL, StrUtils, DateUtils, MessageDefine, Commonlib, VarDefine,DOMOFB_PRINT,
  KISCalendar , Preview;
{$R *.dfm}

procedure TUI_DOMOFB_frm.FormCreate(Sender: TObject);
begin
  inherited;
  DOMOFB_SQL := Self.qryList.SQL.Text;
  DOMOFBDETAIL_SQL := Self.qryDetail.SQL.Text;
  Page_control.ActivePageIndex := 0;
  ProgramControlType := ctView;
end;

procedure TUI_DOMOFB_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    qryList.Close;
    qryList.Open;

  end;
end;

procedure TUI_DOMOFB_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  Action := caFree;
  UI_DOMOFB_frm := nil;
end;

procedure TUI_DOMOFB_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOMOFB_frm.ReadDocument;
begin
 if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

//----------------------------------------------------------------------------
//기본정보
//----------------------------------------------------------------------------
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //신청일자
  edt_OFRDATE.Text := qryListOFR_DATE.AsString;
  //발행번호
  edt_OFRNO.Text := qryListOFR_NO.AsString;

//----------------------------------------------------------------------------
//발행자
//----------------------------------------------------------------------------
  //발행자
  edt_SRCODE.Text := qryListSR_CODE.AsString;
  //무역대리점 등록번호
  edt_SRNO.Text := qryListSR_NO.AsString;
  //상호
  edt_SRNAME1.Text := qryListSR_NAME1.AsString;
  edt_SRNAME2.Text := qryListSR_NAME2.AsString;
  //전자서명
  edt_SRNAME3.Text := qryListSR_NAME3.AsString;
  //주소
  edt_SRADDR1.Text := qryListSR_ADDR1.AsString;
  edt_SRADDR2.Text := qryListSR_ADDR2.AsString;
  edt_SRADDR3.Text := qryListSR_ADDR3.AsString;

//----------------------------------------------------------------------------
//수요자
//----------------------------------------------------------------------------
  //수요자
  edt_UDCODE.Text := qryListUD_CODE.AsString;
  //사업자등록번호
  edt_UDNO.Text := qryListUD_NO.AsString;
  //상호
  edt_UDNAME1.Text := qryListUD_NAME1.AsString;
  edt_UDNAME2.Text := qryListUD_NAME2.AsString;
  //전자서명
  edt_UDNAME3.Text := qryListUD_NAME3.AsString;
  //주소
  edt_UDADDR1.Text := qryListUD_ADDR1.AsString;
  edt_UDADDR2.Text := qryListUD_ADDR2.AsString;
  edt_UDADDR3.Text := qryListUD_ADDR3.AsString;
  //수요자참조번호
  edt_UDRENO.Text := qryListUD_RENO.AsString;
  //대표공급물품 HS부호
  edt_HSCODE.Text := qryListHS_CODE.AsString;
  //유효기간
  edt_OFRSQ1.Text := qryListOFR_SQ1.AsString;
  edt_OFRSQ2.Text := qryListOFR_SQ2.AsString;
  edt_OFRSQ3.Text := qryListOFR_SQ3.AsString;
  edt_OFRSQ4.Text := qryListOFR_SQ4.AsString;
  edt_OFRSQ5.Text := qryListOFR_SQ5.AsString;

//----------------------------------------------------------------------------
//Page2
//----------------------------------------------------------------------------
  //검사방법
  edt_TSTINST1.Text := qryListTSTINST1.AsString;
  edt_TSTINST2.Text := qryListTSTINST2.AsString;
  edt_TSTINST3.Text := qryListTSTINST3.AsString;
  edt_TSTINST4.Text := qryListTSTINST4.AsString;
  edt_TSTINST5.Text := qryListTSTINST5.AsString;
  //포장방법
  edt_PCKINST1.Text := qryListPCKINST1.AsString;
  edt_PCKINST2.Text := qryListPCKINST2.AsString;
  edt_PCKINST3.Text := qryListPCKINST3.AsString;
  edt_PCKINST4.Text := qryListPCKINST4.AsString;
  edt_PCKINST5.Text := qryListPCKINST5.AsString;
  //결제조건
  edt_PAYETC1.Text := qryListPAY_ETC1.AsString;
  edt_PAYETC2.Text := qryListPAY_ETC2.AsString;
  edt_PAYETC3.Text := qryListPAY_ETC3.AsString;
  edt_PAYETC4.Text := qryListPAY_ETC4.AsString;
  edt_PAYETC5.Text := qryListPAY_ETC5.AsString;

//----------------------------------------------------------------------------
//Page3
//----------------------------------------------------------------------------
  //원산지국가
  edt_ORGN1N.Text := qryListORGN1N.AsString;
  edt_ORGN2N.Text := qryListORGN2N.AsString;
  edt_ORGN3N.Text := qryListORGN3N.AsString;
  edt_ORGN4N.Text := qryListORGN4N.AsString;
  edt_ORGN5N.Text := qryListORGN5N.AsString;
  //지방명
  edt_ORGN1LOC.Text := qryListORGN1LOC.AsString;
  edt_ORGN2LOC.Text := qryListORGN2LOC.AsString;
  edt_ORGN3LOC.Text := qryListORGN3LOC.AsString;
  edt_ORGN4LOC.Text := qryListORGN4LOC.AsString;
  edt_ORGN5LOC.Text := qryListORGN5LOC.AsString;
  //기타참조사항
  memo_REMARK1.Lines.Text := qryListREMARK_1.AsString;

//----------------------------------------------------------------------------
//Page4
//----------------------------------------------------------------------------
  //운송방법
  edt_TRNSID.Text := qryListTRNS_ID.AsString;
  edt_TRNSIDNAME.Text := qryListTRANSNME.AsString;
  //선적국가
  edt_LOADD.Text := qryListLOADD.AsString;
  //선적항
  edt_LOADLOC.Text := qryListLOADLOC.AsString;
  //선적시기
  edt_LOADTXT1.Text := qryListLOADTXT1.AsString;
  edt_LOADTXT2.Text := qryListLOADTXT2.AsString;
  edt_LOADTXT3.Text := qryListLOADTXT3.AsString;
  edt_LOADTXT4.Text := qryListLOADTXT4.AsString;
  edt_LOADTXT5.Text := qryListLOADTXT5.AsString;
  //도착국가
  edt_DEST.Text := qryListDEST.AsString;
  //도착항
  edt_DESTLOC.Text := qryListDESTLOC.AsString;
  //도착시기
  edt_DESTTXT1.Text := qryListDESTTXT1.AsString;
  edt_DESTTXT2.Text := qryListDESTTXT2.AsString;
  edt_DESTTXT3.Text := qryListDESTTXT3.AsString;
  edt_DESTTXT4.Text := qryListDESTTXT4.AsString;
  edt_DESTTXT5.Text := qryListDESTTXT5.AsString;

//----------------------------------------------------------------------------
//Page4
//----------------------------------------------------------------------------
  //총수량단위
  edt_TQTYCUR.Text := qryListTQTYCUR.AsString;
  //총수량
  edt_TQTY.Value := qryListTQTY.AsCurrency;
  //총합계단위
  edt_TAMTCUR.Text := qryListTQTYCUR.AsString;
  edt_TAMT.Value := qryListTAMT.AsCurrency;

  //Page5 Detail DBGrid 활성
  with qryDetail do
  begin
    Close;
    SQL.Text :=  DOMOFBDETAIL_SQL;
    SQL.Add(' WHERE KEYY = ' + QuotedStr(edt_MaintNo.Text));
    Open;
  end;
end;

procedure TUI_DOMOFB_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := DOMOFB_SQL;
    SQL.Add(' WHERE DATEE BETWEEN '+ QuotedStr(fromDate)+' AND '+QuotedStr(toDate) );
    SQL.Add(' ORDER BY DATEE ');
    Open;

    if Trim(KeyValue) <> '' then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    //데이터조회탭 부분
    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;

end;

procedure TUI_DOMOFB_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 5 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := DOMOFB_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : SQL.Add(' WHERE H1.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE UD_NAME1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;

  end;

end;

procedure TUI_DOMOFB_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel2.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn3.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn4.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_DOMOFB_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_DOMOFB_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;

  if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text])then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text,'');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_DOMOFB_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

  EnabledControlValue(maintno_Panel);
  EnabledControlValue(page1_Right);
  EnabledControlValue(page3_Right);
  EnabledControlValue(sPanel17);
  EnabledControlValue(sPanel18);
end;

procedure TUI_DOMOFB_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.RecordCount = 0 then ReadDocument;
end;

procedure TUI_DOMOFB_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

procedure TUI_DOMOFB_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM DOMOFB_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H2 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H3 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else if qryList.RecordCount = 0 then
        begin
          ClearControlValue(page1_Right);
          ClearControlValue(page3_Right);
          ClearControlValue(sPanel16);
          ClearControlValue(sPanel17);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;


procedure TUI_DOMOFB_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  DeleteDocument;
end;

procedure TUI_DOMOFB_frm.Page_controlChange(Sender: TObject);
begin
  inherited;
  //상단의 관리번호 패널 숨김
  //maintno_Panel.Visible := not (Page_control.ActivePageIndex = 3);
  //국내발행물품매도확약서 목록 DBGRID 패널을 숨김
  LeftGrid_Panel.Visible := not (Page_control.ActivePageIndex = 3);
end;

procedure TUI_DOMOFB_frm.qryListChk2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : Integer;
begin
  inherited;

  nIndex := StrToIntDef(qryListCHK2.AsString,-1);

  case nIndex of
    -1 : Text := '';
     0 : Text := '임시';
     1 : Text := '저장';
     2 : Text := '결재';
     3 : Text := '반려';
     4 : Text := '접수';
     5 : Text := '준비';
     6 : Text := '취소';
     7 : Text := '전송';
     8 : Text := '오류';
     9 : Text := '승인';
  end;

end;

procedure TUI_DOMOFB_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

procedure TUI_DOMOFB_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_DOMOFB_frm.btn_calClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    902 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    903 : Mask_SearchDate1DblClick(Mask_SearchDate2);

  end;
end;

procedure TUI_DOMOFB_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DOMOFB_PRINT_frm := TDOMOFB_PRINT_frm.Create(Self);
  try
    case(Sender as TsButton).Tag of
      //바로출력
      0 : DOMOFB_PRINT_frm.PrintDocument(qryList.Fields,qryDetail.Fields,False);
      //미리보기
      1 : DOMOFB_PRINT_frm.PrintDocument(qryList.Fields,qryDetail.Fields ,True);

    end;
  finally
    FreeAndNil(DOMOFB_PRINT_frm);
  end;
end;

end.
