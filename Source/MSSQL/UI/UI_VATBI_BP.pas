unit UI_VATBI_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, ComCtrls, sPageControl, ExtCtrls,
  sSplitter, Buttons, sBitBtn, StdCtrls, sEdit, Mask, sMaskEdit, sButton,
  sSpeedButton, sPanel, Grids, DBGrids, acDBGrid, sBevel, sLabel, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sComboBox, DB, ADODB, MSSQL, StrUtils;

type
  TUI_VATBI_BP_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    btn_Cal: TsBitBtn;
    edt_msg1: TsEdit;
    sBitBtn19: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn20: TsBitBtn;
    edt_MAINT_NO: TsEdit;
    sSplitter1: TsSplitter;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter3: TsSplitter;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet5: TsTabSheet;
    page1_RIGHT: TsPanel;
    sPanel2: TsPanel;
    sPanel56: TsPanel;
    sDBGrid8: TsDBGrid;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    taxPanel: TsPanel;
    sBevel1: TsBevel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sPanel5: TsPanel;
    edt_VATCODE: TsEdit;
    sBitBtn4: TsBitBtn;
    edt_VATTYPE: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_NEWINDICATOR: TsEdit;
    sBitBtn6: TsBitBtn;
    edt_RENO: TsEdit;
    edt_SENO: TsEdit;
    edt_FSNO: TsEdit;
    edt_ACENO: TsEdit;
    edt_RFFNO: TsEdit;
    providerPanel: TsPanel;
    sPanel7: TsPanel;
    edt_SENAME3: TsEdit;
    edt_SENAME2: TsEdit;
    edt_SENAME1: TsEdit;
    mask_SESAUP: TsMaskEdit;
    sBitBtn2: TsBitBtn;
    edt_SEADDR1: TsEdit;
    edt_SECODE: TsEdit;
    edt_SEADDR2: TsEdit;
    edt_SEADDR3: TsEdit;
    edt_SEADDR4: TsEdit;
    edt_SEADDR5: TsEdit;
    edt_SESAUP1: TsEdit;
    edt_SEFTX1: TsEdit;
    edt_SEFTX2: TsEdit;
    edt_SEFTX3: TsEdit;
    edt_SEFTX4: TsEdit;
    edt_SEFTX5: TsEdit;
    memo_SEUPTA1: TsMemo;
    memo_SEITEM1: TsMemo;
    sPanel9: TsPanel;
    sSplitter2: TsSplitter;
    sPanel10: TsPanel;
    sPanel13: TsPanel;
    GoodsBtnPanel: TsPanel;
    sSpeedButton10: TsSpeedButton;
    btn_goodsNew: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    btn_goodsMod: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    btn_goodsDel: TsSpeedButton;
    sSpeedButton15: TsSpeedButton;
    btn_goodsSave: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    btn_goodsCancel: TsSpeedButton;
    sDBGrid1: TsDBGrid;
    goodsPanel: TsPanel;
    sPanel22: TsPanel;
    edt_NAMECOD_D: TsEdit;
    sBitBtn15: TsBitBtn;
    memo_NAME1_D: TsMemo;
    memo_SIZE1_D: TsMemo;
    sPanel14: TsPanel;
    mask_DEDATE_D: TsMaskEdit;
    curr_RATE_D: TsCurrencyEdit;
    memo_DEREM1_D: TsMemo;
    edt_QTY_G_D: TsEdit;
    sBitBtn44: TsBitBtn;
    curr_QTY_D: TsCurrencyEdit;
    curr_PRICE_D: TsCurrencyEdit;
    edt_QTYG_G_D: TsEdit;
    sBitBtn47: TsBitBtn;
    curr_QTYG_D: TsCurrencyEdit;
    edt_USAMT_G_D: TsEdit;
    sBitBtn49: TsBitBtn;
    curr_USAMT_D: TsCurrencyEdit;
    curr_SUPAMT_D: TsCurrencyEdit;
    curr_TAXAMT_D: TsCurrencyEdit;
    sPanel15: TsPanel;
    sSplitter4: TsSplitter;
    sPanel16: TsPanel;
    moneyPanel: TsPanel;
    sPanel18: TsPanel;
    mask_DRAWDAT: TsMaskEdit;
    curr_SUPAMT: TsCurrencyEdit;
    curr_TAXAMT: TsCurrencyEdit;
    curr_DETAILNO: TsCurrencyEdit;
    sBevel2: TsBevel;
    memo_REMARK1: TsMemo;
    sBevel3: TsBevel;
    edt_AMT11C: TsEdit;
    sBitBtn11: TsBitBtn;
    curr_AMT12: TsCurrencyEdit;
    curr_AMT11: TsCurrencyEdit;
    curr_AMT22: TsCurrencyEdit;
    edt_AMT21C: TsEdit;
    sBitBtn12: TsBitBtn;
    curr_AMT21: TsCurrencyEdit;
    curr_AMT32: TsCurrencyEdit;
    edt_AMT31C: TsEdit;
    sBitBtn13: TsBitBtn;
    curr_AMT31: TsCurrencyEdit;
    curr_AMT42: TsCurrencyEdit;
    edt_AMT41C: TsEdit;
    sBitBtn14: TsBitBtn;
    curr_AMT41: TsCurrencyEdit;
    edt_INDICATOR: TsEdit;
    sBitBtn16: TsBitBtn;
    edt_INDICATORNAME: TsEdit;
    curr_TAMT: TsCurrencyEdit;
    curr_SUPTAMT: TsCurrencyEdit;
    curr_TAXTAMT: TsCurrencyEdit;
    sBevel4: TsBevel;
    edt_USTAMTC: TsEdit;
    sBitBtn17: TsBitBtn;
    curr_USTAMT: TsCurrencyEdit;
    edt_TQTYC: TsEdit;
    sBitBtn18: TsBitBtn;
    curr_TQTY: TsCurrencyEdit;
    sPanel21: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    sBitBtn23: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn28: TsBitBtn;
    sPanel23: TsPanel;
    sDBGrid2: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryGoods: TADOQuery;
    dsGoods: TDataSource;
    qryMAX_SEQ: TADOQuery;
    edt_STQTY_G_D: TsEdit;
    sBitBtn9: TsBitBtn;
    curr_STQTY_D: TsCurrencyEdit;
    curr_SUPSTAMT_D: TsCurrencyEdit;
    curr_TAXSTAMT_D: TsCurrencyEdit;
    edt_USSTAMT_G_D: TsEdit;
    sBitBtn10: TsBitBtn;
    curr_USSTAMT_D: TsCurrencyEdit;
    sBitBtn8: TsBitBtn;
    sSplitter5: TsSplitter;
    sSplitter6: TsSplitter;
    sTabSheet4: TsTabSheet;
    sPanel3: TsPanel;
    sPanel11: TsPanel;
    buyerPanel: TsPanel;
    sPanel8: TsPanel;
    edt_BYNAME3: TsEdit;
    edt_BYNAME2: TsEdit;
    edt_BYNAME1: TsEdit;
    mask_BYSAUP: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    edt_BYADDR1: TsEdit;
    edt_BYCODE: TsEdit;
    edt_BYADDR2: TsEdit;
    edt_BYADDR3: TsEdit;
    edt_BYADDR4: TsEdit;
    edt_BYADDR5: TsEdit;
    edt_BYSAUP1: TsEdit;
    edt_BYFTX1: TsEdit;
    edt_BYFTX2: TsEdit;
    edt_BYFTX3: TsEdit;
    edt_BYFTX4: TsEdit;
    edt_BYFTX5: TsEdit;
    edt_BYSAUPCODE: TsEdit;
    sBitBtn3: TsBitBtn;
    memo_BYUPTA1: TsMemo;
    memo_BYITEM1: TsMemo;
    sBitBtn7: TsBitBtn;
    sTabSheet6: TsTabSheet;
    sPanel17: TsPanel;
    sPanel12: TsPanel;
    trusteePanel: TsPanel;
    sPanel20: TsPanel;
    edt_AGNAME3: TsEdit;
    edt_AGNAME2: TsEdit;
    edt_AGNAME1: TsEdit;
    mask_AGSAUP: TsMaskEdit;
    sBitBtn21: TsBitBtn;
    edt_AGADDR1: TsEdit;
    edt_AGCODE: TsEdit;
    edt_AGADDR2: TsEdit;
    edt_AGADDR3: TsEdit;
    edt_AGADDR4: TsEdit;
    edt_AGADDR5: TsEdit;
    edt_AGSAUP1: TsEdit;
    edt_AGFTX1: TsEdit;
    edt_AGFTX2: TsEdit;
    edt_AGFTX3: TsEdit;
    edt_AGFTX4: TsEdit;
    edt_AGFTX5: TsEdit;
    memo_AGUPTA1: TsMemo;
    memo_AGITEM1: TsMemo;
    sSpeedButton14: TsSpeedButton;
    sSplitter7: TsSplitter;
    sSplitter8: TsSplitter;
    procedure Page_controlChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edt_VATCODEDblClick(Sender: TObject);
    procedure edt_SECODEDblClick(Sender: TObject);
    procedure edt_NAMECOD_DDblClick(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn15Click(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure mask_DATEEDblClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sBitBtn60Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
  private
    { Private declarations }

    //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
    procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;
    
  public
    { Public declarations }
  protected
      procedure ReadDocument; virtual; abstract;
      procedure GoodsReadDocument; virtual; abstract;
//      procedure NewDocument; virtual; abstract;
//      procedure CancelDocument; virtual; abstract;
//      procedure EditDocument; virtual; abstract;
      procedure DeleteDocument; virtual; abstract;
//      procedure SaveDocument(Sender: TObject); virtual; abstract;
//      procedure GoodsSaveDocument; virtual; abstract;

  end;

var
  UI_VATBI_BP_frm: TUI_VATBI_BP_frm;

implementation

{$R *.dfm}

USES Dialog_CodeList, MessageDefine, Dialog_SearchCustom, Dialog_ItemList,
  KISCalendar, Commonlib;

procedure TUI_VATBI_BP_frm.Page_controlChange(Sender: TObject);
begin
  inherited;
  if (Sender as TsPageControl).ActivePageIndex = 5 then
  begin
    //관리번호가있는 패널  숨김
    //sPanel4.Visible := False;
    //DBGrid3이 위치해 있는 패널 숨김
    sPanel56.Visible := False;
  end
  else
  begin
    //관리번호가있는 패널
    //sPanel4.Visible := True;
    //DBGrid3이 위치해 있는 패널
     sPanel56.Visible := True;
  end;
end;

procedure TUI_VATBI_BP_frm.FormShow(Sender: TObject);
begin
  inherited;
  Page_control.ActivePageIndex := 0;
end;

procedure TUI_VATBI_BP_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TUI_VATBI_BP_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_VATBI_BP_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_VATBI_BP_frm.edt_VATCODEDblClick(Sender: TObject);
begin
  inherited;
    Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      //문서번호의 SHOW HINT가 필요한 EDIT
      if (Sender as TsEdit).Tag = 102 then
      begin
        if Dialog_CodeList_frm.openDialog((Sender as TsEdit).HelpKeyword) = mrOK then
        begin
            //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        end;
      end
      else if (Sender as TsEdit).Tag = 103 then
      begin
        // 수정사유코드는 세금계산서 종류가 0401,0403,0404일 경우에만 사용이가능하다.
        if (edt_VATTYPE.Text = '0401') or (edt_VATTYPE.Text = '0403') or (edt_VATTYPE.Text = '0404')then
        begin
          IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
          begin
          //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
          end;
        end
        else
        begin
          MessageBox(Self.Handle,MSG_VATTYPE_NOT_VAILD_DATA,'세금계산서종류 확인', MB_OK+MB_ICONINFORMATION);
        end;
      end
      //힌트출력이 필요가 없는 EDIT
      ELSE
      begin
        IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
        begin
          //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
            case (Sender as TsEdit).Tag of
              //영수 / 청구
              119 : edt_INDICATORNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
            end;
        end;
      END;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_VATBI_BP_frm.edt_SECODEDblClick(Sender: TObject);
begin
  inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);

  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOK then
      begin
        (Sender as TsEdit).Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).Tag of
          // 공급자부분
          104 :
          BEGIN
            //상호
            edt_SENAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
            //대표자
            edt_SENAME3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
            //사업자등록번호
            mask_SESAUP.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
            //주소
            edt_SEADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
            edt_SEADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
            edt_SEADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          END;
          //공급받는자 부분
          105 :
          begin
            //상호
            edt_BYNAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
            //대표자
            edt_BYNAME3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
            //사업자등록번호
            mask_BYSAUP.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
            //주소
            edt_BYADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
            edt_BYADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
            edt_BYADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;
          //수탁자 부분
          119 :
          begin
            //상호
            edt_AGNAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
            //대표자
            edt_AGNAME3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
            //사업자등록번호
            mask_AGSAUP.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
            //주소
            edt_AGADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
            edt_AGADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
            edt_AGADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;

procedure TUI_VATBI_BP_frm.edt_NAMECOD_DDblClick(Sender: TObject);
begin
  inherited;
  Dialog_ItemList_frm := TDialog_ItemList_frm.Create(Self);
  try
    if Dialog_ItemList_frm.openDialog = mrok then
    begin
      //코드
      (Sender as TsEdit).Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
      case (Sender as TsEdit).Tag of
        107 :
        begin
          //품명
          memo_NAME1_D.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
          //규격
          memo_SIZE1_D.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('MSIZE').AsString;
          //수량단위
          edt_QTY_G_D.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTYC').AsString;
          //수량
          curr_QTY_D.Value := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_U').AsCurrency;
          //단가
          curr_PRICE_D.Value := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('PRICE').AsCurrency;
          //단가기준수량단위
          edt_QTYG_G_D.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_UG').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_ItemList_frm);
  end;
end;

procedure TUI_VATBI_BP_frm.sBitBtn4Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    101 : edt_VATCODEDblClick(edt_VATCODE);
    102 : edt_VATCODEDblClick(edt_VATTYPE);
    103 : edt_VATCODEDblClick(edt_NEWINDICATOR);
    106 : edt_VATCODEDblClick(edt_BYSAUPCODE);
    108 : edt_VATCODEDblClick(edt_QTY_G_D);
    109 : edt_VATCODEDblClick(edt_STQTY_G_D);
    110 : edt_VATCODEDblClick(edt_QTY_G_D);
    111 : edt_VATCODEDblClick(edt_USAMT_G_D);
    112 : edt_VATCODEDblClick(edt_USSTAMT_G_D);
    113 : edt_VATCODEDblClick(edt_AMT11C);
    114 : edt_VATCODEDblClick(edt_AMT21C);
    115 : edt_VATCODEDblClick(edt_AMT31C);
    116 : edt_VATCODEDblClick(edt_AMT41C);
    117 : edt_VATCODEDblClick(edt_USTAMTC);
    118 : edt_VATCODEDblClick(edt_TQTYC);
    119 : edt_VATCODEDblClick(edt_INDICATOR);
    201 : edt_VATCODEDblClick(edt_msg1);
    202 : edt_VATCODEDblClick(edt_msg2);
  end;
end;

procedure TUI_VATBI_BP_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    104 : edt_SECODEDblClick(edt_SECODE);
    105 : edt_SECODEDblClick(edt_BYCODE);
    119 : edt_SECODEDblClick(edt_AGCODE);
  end;
end;

procedure TUI_VATBI_BP_frm.sBitBtn15Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    107 : edt_NAMECOD_DDblClick(edt_NAMECOD_D);  
  end;
end;



procedure TUI_VATBI_BP_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    901 : mask_DATEEDblClick(mask_DATEE);
    902 : mask_DATEEDblClick(Mask_SearchDate1);
    903 : mask_DATEEDblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_VATBI_BP_frm.mask_DATEEDblClick(Sender: TObject);
var
POS : TPoint;
begin
  inherited;

//if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_VATBI_BP_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  //
end;

procedure TUI_VATBI_BP_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;
end;

procedure TUI_VATBI_BP_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  Mask_fromDate.Text := Mask_SearchDate1.Text;
  mask_toDate.Text := Mask_SearchDate2.Text;
end;

procedure TUI_VATBI_BP_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

  if (ActiveControl.Name = 'memo_SEITEM1') and (msg.CharCode = VK_TAB) then
  begin
    Page_control.ActivePageIndex := 1;
    Self.KeyPreview := False;
  end
  else if (ActiveControl.Name = 'memo_BYITEM1') and (msg.CharCode = VK_TAB) then
  begin
    Page_control.ActivePageIndex := 2;
    Self.KeyPreview := False;
  end
  else if (ActiveControl.Name = 'curr_TQTY') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
  begin
    Page_control.ActivePageIndex := 4;
    Self.KeyPreview := False;
  end
  else
  begin
    Self.KeyPreview := True;
    inherited;
  end;
end;

end.
