unit UI_LOCAD1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Grids, DBGrids, acDBGrid, StdCtrls, sRadioButton,
  Buttons, sBitBtn, sComboBox, sMemo, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, Mask, sMaskEdit, sEdit, sLabel, sScrollBox, ComCtrls,
  sPageControl, ExtCtrls, sSplitter, sButton, sSpeedButton, sPanel,
  sSkinProvider, DB, ADODB, TypeDefine, QuickRpt;

type
  TUI_LOCAD1_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton4: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sButton1: TsButton;
    sSplitter1: TsSplitter;
    sPageControl1: TsPageControl;
    sTabSheet2: TsTabSheet;
    sSplitter3: TsSplitter;
    sScrollBox1: TsScrollBox;
    sSpeedButton1: TsSpeedButton;
    sLabel1: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    edt_APPLIC1: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPADDR1: TsEdit;
    edt_APPADDR2: TsEdit;
    sPanel5: TsPanel;
    sPanel7: TsPanel;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BNFADDR1: TsEdit;
    edt_BNFADDR2: TsEdit;
    edt_BNFADDR3: TsEdit;
    edt_BNFEMAILID: TsEdit;
    sPanel8: TsPanel;
    edt_BNFDOMAIN: TsEdit;
    sPanel9: TsPanel;
    edt_AP_BANK: TsEdit;
    edt_AP_BANK1: TsEdit;
    edt_LOC_TYPE: TsEdit;
    edt_LOCTYPE_NAME: TsEdit;
    edt_LOC1AMTC: TsEdit;
    Curr_LOC1AMT: TsCurrencyEdit;
    edt_CDPERP: TsEdit;
    edt_CDPERM: TsEdit;
    edt_BUSINESS: TsEdit;
    edt_BUSINESSNAME: TsEdit;
    edt_OFFERNO1: TsEdit;
    edt_OFFERNO2: TsEdit;
    edt_OFFERNO3: TsEdit;
    edt_OFFERNO4: TsEdit;
    edt_OFFERNO5: TsEdit;
    edt_OFFERNO6: TsEdit;
    edt_OFFERNO7: TsEdit;
    edt_OFFERNO8: TsEdit;
    edt_OFFERNO9: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    curr_DOCCOPY1: TsCurrencyEdit;
    curr_DOCCOPY2: TsCurrencyEdit;
    curr_DOCCOPY3: TsCurrencyEdit;
    curr_DOCCOPY4: TsCurrencyEdit;
    curr_DOCCOPY5: TsCurrencyEdit;
    sPanel12: TsPanel;
    Curr_OPENNO: TsCurrencyEdit;
    mask_ISSDATE: TsMaskEdit;
    sPanel13: TsPanel;
    mask_ADVDATE: TsMaskEdit;
    mask_EXPIRY: TsMaskEdit;
    edt_TRANSPRT: TsEdit;
    edt_TRANSPRTNAME: TsEdit;
    curr_EXRATE: TsCurrencyEdit;
    curr_DOC_PRD: TsCurrencyEdit;
    edt_LOC2AMTC: TsEdit;
    Curr_LOC2AMT: TsCurrencyEdit;
    edt_BENEFC3: TsEdit;
    edt_LCNO: TsEdit;
    sPanel16: TsPanel;
    sPanel18: TsPanel;
    sTabSheet4: TsTabSheet;
    sSplitter4: TsSplitter;
    sPanel20: TsPanel;
    sPanel14: TsPanel;
    memo_GOODDES1: TsMemo;
    mask_BSN_HSCODE: TsMaskEdit;
    memo_DOC_ETC1: TsMemo;
    memo_REMARK1: TsMemo;
    sPanel15: TsPanel;
    edt_DOC_DTL: TsEdit;
    edt_DOCDTL_NAME: TsEdit;
    edt_DOC_NO: TsEdit;
    mask_LOADDATE: TsMaskEdit;
    mask_EXPDATE: TsMaskEdit;
    edt_PAYMENT: TsEdit;
    edt_PAYMENTNAME: TsEdit;
    edt_DEST: TsEdit;
    edt_ISBANK1: TsEdit;
    edt_ISBANK2: TsEdit;
    memo_EXGOOD1: TsMemo;
    sPanel17: TsPanel;
    edt_EXNAME1: TsEdit;
    edt_EXNAME2: TsEdit;
    edt_EXNAME3: TsEdit;
    edt_DESTName: TsEdit;
    sPanel23: TsPanel;
    sTabSheet1: TsTabSheet;
    sSplitter2: TsSplitter;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel22: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListLOC_TYPE: TStringField;
    qryListLOC2AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListEX_RATE: TBCDField;
    qryListBUSINESS: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListOPEN_NO: TBCDField;
    qryListISS_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListLC_NO: TStringField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListDOC_ETC: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    edt_APPADDR3: TsEdit;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
    sPanel: TsPanel;
    sPanel4: TsPanel;
    edt_Maint_No: TsEdit;
    mask_DATEE: TsMaskEdit;
    edt_UserNo: TsEdit;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    mask_DELIVERY: TsMaskEdit;
    qryListDOC_ETC1: TMemoField;
    sSpeedButton2: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSplitter5: TsSplitter;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sPageControl1Change(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    LOCAD1_SQL : String;
    procedure ReadDocument;
    procedure Readlist(fromDate, toDate : String; KeyValue : String=''); overload;
    procedure Readlist(OrderSyntax : string = ''); overload;
    procedure DeleteDocument;
  public
    { Public declarations }
  protected
    { protected declaration }
    ProgramControlType : TProgramControlType;
  end;

var
  UI_LOCAD1_frm: TUI_LOCAD1_frm;

implementation

uses MSSQL, MessageDefine, StrUtils, DateUtils, Commonlib, KISCalendar, LOCAD1_PRINT;

{$R *.dfm}

procedure TUI_LOCAD1_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCAD1_frm := nil;
end;

procedure TUI_LOCAD1_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCAD1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  LOCAD1_SQL := qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
  ProgramControlType := ctView;
end;

procedure TUI_LOCAD1_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_F1 : sPageControl1.ActivePageIndex := 0;
    Vk_F2 : sPageControl1.ActivePageIndex := 1;
    VK_F3 : sPageControl1.ActivePageIndex := 2;
  end;
end;

procedure TUI_LOCAD1_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    qryList.Close;
    qryList.Open;

  end;
end;

procedure TUI_LOCAD1_frm.ReadDocument;
begin
 if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

//----------------------------------------------------------------------------
//기본정보
//----------------------------------------------------------------------------
  //관리번호
  edt_Maint_No.Text := qryListMAINT_NO.AsString;
  //등록일자
  mask_DATEE.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
//----------------------------------------------------------------------------
// 개설의뢰인
//----------------------------------------------------------------------------
  //상호
  edt_APPLIC1.Text := qryListAPPLIC1.AsString;
  edt_APPLIC2.Text := qryListAPPLIC2.AsString;
  edt_APPLIC3.Text := qryListAPPLIC3.AsString;
  //주소
  edt_APPADDR1.Text := qryListAPPADDR1.AsString;
  edt_APPADDR2.Text := qryListAPPADDR2.AsString;
  edt_APPADDR3.Text := qryListAPPADDR3.AsString;
//----------------------------------------------------------------------------
// 개설의뢰인
//----------------------------------------------------------------------------
  //상호
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  //주소
  edt_BNFADDR1.Text := qryListBNFADDR1.AsString;
  edt_BNFADDR2.Text := qryListBNFADDR2.AsString;
  edt_BNFADDR3.Text := qryListBNFADDR3.AsString;
  //이메일
  edt_BNFEMAILID.Text := qryListBNFEMAILID.AsString;
  edt_BNFDOMAIN.Text := qryListBNFDOMAIN.AsString;
//----------------------------------------------------------------------------
// 개설은행/신용장
//----------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qryListAP_BANK.AsString;
  edt_AP_BANK1.Text := qryListAP_BANK1.AsString;
  //신용장 종류
  edt_LOC_TYPE.Text := qryListLOC_TYPE.AsString;
  edt_LOCTYPE_NAME.Text := qryListLOC_TYPENAME.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qryListLOC1AMTC.AsString;
  Curr_LOC1AMT.Value := qryListLOC1AMT.AsCurrency;
  //개설금액(원화)
  edt_LOC2AMTC.Text := qryListLOC2AMTC.AsString;
  Curr_LOC2AMT.Value := qryListLOC2AMT.AsCurrency;
  //허용오차
  edt_CDPERP.Text := qryListCD_PERP.AsString;
  edt_CDPERM.Text := qryListCD_PERM.AsString;
  //매매기준율
  curr_EXRATE.Value := qryListEX_RATE.AsCurrency;
  //개설근거별용도
  edt_BUSINESS.Text := qryListBUSINESS.AsString;
  edt_BUSINESSNAME.Text := qryListBUSINESSNAME.AsString;
//----------------------------------------------------------------------------
// 물품매도확약서
//----------------------------------------------------------------------------
  edt_OFFERNO1.Text := qryListOFFERNO1.AsString;
  edt_OFFERNO2.Text := qryListOFFERNO2.AsString;
  edt_OFFERNO3.Text := qryListOFFERNO3.AsString;
  edt_OFFERNO4.Text := qryListOFFERNO4.AsString;
  edt_OFFERNO5.Text := qryListOFFERNO5.AsString;
  edt_OFFERNO6.Text := qryListOFFERNO6.AsString;
  edt_OFFERNO7.Text := qryListOFFERNO7.AsString;
  edt_OFFERNO8.Text := qryListOFFERNO8.AsString;
  edt_OFFERNO9.Text := qryListOFFERNO9.AsString;
//----------------------------------------------------------------------------
// 주요구비서류
//----------------------------------------------------------------------------
  //물품수령증명서
  curr_DOCCOPY1.Value := qryListDOCCOPY1.AsCurrency;
  //공급자발행 세금계산서 사본
  curr_DOCCOPY2.Value := qryListDOCCOPY2.AsCurrency;
  //물품명세가 기재된 송장
  curr_DOCCOPY3.Value := qryListDOCCOPY3.AsCurrency;
  //본 내국신용장의 사본
  curr_DOCCOPY4.Value := qryListDOCCOPY4.AsCurrency;
  //공급자발행 물품매도확약서 사본
  curr_DOCCOPY5.Value := qryListDOCCOPY5.AsCurrency;
//----------------------------------------------------------------------------
// 개설관련
//---------------------------------------------------------------------------
  //개설회차
  Curr_OPENNO.Value := qryListOPEN_NO.AsCurrency;
  //개설신청일자
  mask_ISSDATE.Text := qryListISS_DATE.AsString;
  //물품인도기일
  mask_DELIVERY.Text := qryListDELIVERY.AsString;
  //통지일자
  mask_ADVDATE.Text := qryListADV_DATE.AsString;
  //유효기일
  mask_EXPIRY.Text := qryListEXPIRY.AsString;
  //분할허용여부
  edt_TRANSPRT.Text := qryListTRANSPRT.AsString;
  edt_TRANSPRTNAME.Text := qryListTRANSPRTNAME.AsString;
  //신용장번호
  edt_LCNO.Text := qryListLC_NO.AsString;
  //서류제시기간
  curr_DOC_PRD.Value := qryListDOC_PRD.AsCurrency;
//----------------------------------------------------------------------------
// 대표공급물품
//---------------------------------------------------------------------------
  //대표공급물품HS부호
  mask_BSN_HSCODE.Text := qryListBSN_HSCODE.AsString;
  //대표공급물품명
  memo_GOODDES1.Text := qryListGOODDES1.AsString;
  //기타구비서류
  memo_DOC_ETC1.Text := qryListDOC_ETC1.AsString;
  //기타정보
  memo_REMARK1.Text := qryListREMARK1.AsString;
//----------------------------------------------------------------------------
// 원수출신용장
//---------------------------------------------------------------------------
  //개설근거서류종류
  edt_DOC_DTL.Text := qryListDOC_DTL.AsString;
  edt_DOCDTL_NAME.Text := qryListDOC_DTLNAME.AsString;
  //신용장(계약서)번호
  edt_DOC_NO.Text := qryListDOC_NO.AsString;
  //선적기일
  mask_LOADDATE.Text := qryListLOADDATE.AsString;
  //유효기일
  mask_EXPDATE.Text := qryListEXPDATE.AsString;
  //수출지역
  edt_DEST.Text := qryListDEST.AsString;
  edt_DESTName.Text := qryListDESTNAME.AsString;
  //발행은행(발행기관)
  edt_ISBANK1.Text := qryListISBANK1.AsString;
  edt_ISBANK2.Text := qryListISBANK2.AsString;
  //대금결제조건
  edt_PAYMENT.Text := qryListPAYMENT.AsString;
  edt_PAYMENTNAME.Text := qryListPAYMENTNAME.AsString;
  //대표수출품목
  memo_EXGOOD1.Text := qryListEXGOOD1.AsString;
//----------------------------------------------------------------------------
// 발신기관 전자서명
//---------------------------------------------------------------------------
  //발신기관 전자서명
  edt_EXNAME1.Text := qryListEXNAME1.AsString;
  edt_EXNAME2.Text := qryListEXNAME2.AsString;
  //전자서명
  edt_EXNAME3.Text := qryListEXNAME3.AsString;
end;

procedure TUI_LOCAD1_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := LOCAD1_SQL;
    SQL.Add(' WHERE DATEE BETWEEN '+ QuotedStr(fromDate)+' AND '+QuotedStr(toDate) );
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    if Trim(KeyValue) <> '' then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    //데이터조회탭 부분
    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;
end;

procedure TUI_LOCAD1_frm.Readlist(OrderSyntax: string);
begin
 if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 2 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := LOCAD1_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ASC');
    end;
    Open;
  end;
end;

procedure TUI_LOCAD1_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_LOCAD1_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_LOCAD1_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;

  if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text])then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text,'');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_LOCAD1_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

  EnabledControlValue(sPanel4);
  EnabledControlValue(sScrollBox1);
  EnabledControlValue(sPanel);

end;

procedure TUI_LOCAD1_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.RecordCount = 0 then ReadDocument;
end;

procedure TUI_LOCAD1_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

procedure TUI_LOCAD1_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_Maint_No.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM LOCAD1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;
        
        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_LOCAD1_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //상단의 관리번호 패널 숨김
  //sPanel4.Visible := not (sPageControl1.ActivePageIndex = 2);
  //국내발행물품매도확약서 목록 DBGRID 패널을 숨김
  sPanel22.Visible := not (sPageControl1.ActivePageIndex = 2);
end;

procedure TUI_LOCAD1_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_LOCAD1_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    1 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    2 : Mask_SearchDate1DblClick(Mask_SearchDate2);

  end;
end;

procedure TUI_LOCAD1_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  DeleteDocument;
end;


procedure TUI_LOCAD1_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  LOCAD1_PRINT_frm := TLOCAD1_PRINT_frm.Create(Self);
  try
    LOCAD1_PRINT_frm.MaintNo := edt_MAINT_NO.Text;

    LOCAD1_PRINT_frm.Prepare;                                                                                                                              
    case (Sender as TsButton).Tag of
      0 :
      begin
        LOCAD1_PRINT_frm.PrinterSetup;
        //------------------------------------------------------------------------------
        // 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
        //------------------------------------------------------------------------------
        IF LOCAD1_PRINT_frm.Tag = 0 Then LOCAD1_PRINT_frm.Print;
      end;

      1 : LOCAD1_PRINT_frm.Preview;
    end;

  finally
    FreeAndNil(LOCAD1_PRINT_frm);
  end;
end;

procedure TUI_LOCAD1_frm.sDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
