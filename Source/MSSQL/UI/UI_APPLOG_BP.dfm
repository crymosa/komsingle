inherited UI_APPLOG_BP_frm: TUI_APPLOG_BP_frm
  Left = 509
  Top = 73
  Caption = 'UI_APPLOG_BP_frm'
  ClientWidth = 1121
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter12: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1121
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter1: TsSplitter [1]
    Left = 0
    Top = 77
    Width = 1121
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1121
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1121
      41)
    object sSpeedButton2: TsSpeedButton
      Left = 427
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 711
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 786
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 502
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton9: TsSpeedButton
      Left = 218
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 205
      Height = 17
      Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 45
      Height = 13
      Caption = 'APPLOG'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton11: TsSpeedButton
      Left = 961
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1045
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 226
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 293
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 360
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 435
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 719
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 510
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 577
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 644
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 794
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 887
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 44
    Width = 1121
    Height = 33
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 241
      Height = 23
      TabStop = False
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object mask_DATEE: TsMaskEdit
      Left = 366
      Top = 5
      Width = 83
      Height = 23
      AutoSize = False
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 528
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 448
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 2
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 1
      Left = 649
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      OnDblClick = edt_CodeClickEvent
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn19: TsBitBtn
      Tag = 11
      Left = 682
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 5
      TabStop = False
      OnClick = btn_CodeClickEvent
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg3: TsEdit
      Tag = 2
      Left = 737
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      OnDblClick = edt_CodeClickEvent
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn20: TsBitBtn
      Tag = 12
      Left = 770
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btn_CodeClickEvent
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPageControl1: TsPageControl [4]
    Left = 0
    Top = 80
    Width = 1121
    Height = 601
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabWidth = 110
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'TRANSPARENT'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1113
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter4: TsSplitter
        Left = 289
        Top = 2
        Width = 3
        Height = 559
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel2: TsPanel
        Left = 292
        Top = 2
        Width = 821
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        DesignSize = (
          821
          559)
        object sSpeedButton1: TsSpeedButton
          Left = 405
          Top = -2
          Width = 10
          Height = 561
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object edt_msg1: TsEdit
          Left = 110
          Top = 90
          Width = 35
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50868#49569#50976#54805
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn6: TsBitBtn
          Tag = 1
          Left = 146
          Top = 90
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 1
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_msg1Name: TsEdit
          Left = 171
          Top = 90
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LCG: TsEdit
          Tag = 102
          Left = 110
          Top = 112
          Width = 35
          Height = 21
          Hint = 'LC'#44396#48516
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn1: TsBitBtn
          Tag = 2
          Left = 146
          Top = 112
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 4
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_LCGNAME: TsEdit
          Left = 171
          Top = 112
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LCNO: TsEdit
          Left = 110
          Top = 134
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BLG: TsEdit
          Tag = 103
          Left = 110
          Top = 156
          Width = 35
          Height = 21
          Hint = #49440#54616#51613#44428
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#54616#51613#44428' or'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn2: TsBitBtn
          Tag = 3
          Left = 146
          Top = 156
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 8
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_BLGNAME: TsEdit
          Left = 171
          Top = 156
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BLNO: TsEdit
          Left = 110
          Top = 178
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54637#44277#54868#47932#50868#49569#51109
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel8: TsPanel
          Left = 1
          Top = 2
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #44592#48376#51077#47141#49324#54637
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
        end
        object mask_BLDATE: TsMaskEdit
          Left = 110
          Top = 46
          Width = 76
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#54616#51613#44428' '#48156#44553#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn16: TsBitBtn
          Left = 187
          Top = 46
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 13
          TabStop = False
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object mask_APPDATE: TsMaskEdit
          Left = 110
          Top = 24
          Width = 76
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn3: TsBitBtn
          Left = 187
          Top = 24
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 15
          TabStop = False
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object mask_ARDATE: TsMaskEdit
          Left = 110
          Top = 68
          Width = 76
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265'('#50696#51221')'#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn4: TsBitBtn
          Left = 187
          Top = 68
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 17
          TabStop = False
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_CARRIER1: TsEdit
          Left = 110
          Top = 200
          Width = 203
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#44592#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CARRIER2: TsEdit
          Left = 110
          Top = 222
          Width = 203
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54637#54644#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel9: TsPanel
          Left = 1
          Top = 314
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49440#48149#54924#49324#47749' / '#51064#49688#50696#51221#51088
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
        end
        object edt_CRNAME1: TsEdit
          Left = 110
          Top = 336
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#48149#54924#49324#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CRNAME2: TsEdit
          Left = 110
          Top = 358
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CRNAME3: TsEdit
          Tag = 104
          Left = 110
          Top = 380
          Width = 67
          Height = 21
          Hint = 'SUNSA'
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn5: TsBitBtn
          Tag = 6
          Left = 178
          Top = 380
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 24
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_CRCODENAME3: TsEdit
          Left = 203
          Top = 380
          Width = 191
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
        end
        object edt_B5NAME1: TsEdit
          Left = 110
          Top = 402
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51064#49688#50696#51221#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_B5NAME2: TsEdit
          Left = 110
          Top = 424
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.SkinSection = 'EDIT'
        end
        object edt_B5NAME3: TsEdit
          Left = 110
          Top = 446
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel11: TsPanel
          Left = 417
          Top = 8
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49569#54616#51064' / '#49688#54616#51064
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
        end
        object edt_SENAME1: TsEdit
          Left = 526
          Top = 31
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49569#54616#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SENAME2: TsEdit
          Left = 526
          Top = 53
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SENAME3: TsEdit
          Left = 526
          Top = 75
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CNNAME1: TsEdit
          Left = 526
          Top = 97
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54616#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CNNAME2: TsEdit
          Left = 526
          Top = 119
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CNNAME3: TsEdit
          Left = 526
          Top = 141
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel10: TsPanel
          Left = 417
          Top = 167
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49440#51201#54637' / '#46020#52265#54637
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
        end
        object edt_LOADLOC: TsEdit
          Tag = 105
          Left = 526
          Top = 190
          Width = 35
          Height = 21
          Hint = #44397#44032
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn7: TsBitBtn
          Tag = 7
          Left = 562
          Top = 190
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 38
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_LOADLOCNAME: TsEdit
          Left = 587
          Top = 190
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LOADTXT: TsEdit
          Left = 526
          Top = 212
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ARRLOC: TsEdit
          Tag = 106
          Left = 526
          Top = 234
          Width = 35
          Height = 21
          Hint = #44397#44032
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn8: TsBitBtn
          Tag = 8
          Left = 562
          Top = 234
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 42
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_ARRLOCNAME: TsEdit
          Left = 587
          Top = 234
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ARRTXT: TsEdit
          Left = 526
          Top = 256
          Width = 284
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 44
          SkinData.SkinSection = 'EDIT'
        end
        object edt_INVAMTC: TsEdit
          Tag = 104
          Left = 110
          Top = 244
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 45
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#50629#49569#51109#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn9: TsBitBtn
          Tag = 4
          Left = 146
          Top = 244
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 46
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object curr_INVAMT: TsCurrencyEdit
          Left = 171
          Top = 244
          Width = 142
          Height = 21
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 47
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 3
          DisplayFormat = '#,###.###;0'
        end
        object edt_PACQTYC: TsEdit
          Left = 110
          Top = 266
          Width = 35
          Height = 21
          Hint = #45800#50948
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 48
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54252#51109#49688
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn10: TsBitBtn
          Tag = 5
          Left = 146
          Top = 266
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 49
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object curr_PACQTY: TsCurrencyEdit
          Left = 171
          Top = 266
          Width = 142
          Height = 21
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 50
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 3
          DisplayFormat = '#,###.###;0'
        end
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 2
        Width = 289
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 1
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685'2'
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1113
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter6: TsSplitter
        Left = 289
        Top = 2
        Width = 3
        Height = 559
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel16: TsPanel
        Left = 0
        Top = 2
        Width = 289
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
      end
      object sPanel17: TsPanel
        Left = 292
        Top = 2
        Width = 821
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        DesignSize = (
          821
          559)
        object sSpeedButton7: TsSpeedButton
          Left = 405
          Top = -2
          Width = 10
          Height = 561
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel12: TsPanel
          Left = 1
          Top = 2
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #54868#47932#54364#49884' '#48143' '#48264#54840
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
        object edt_SPMARK1: TsEdit
          Left = 112
          Top = 25
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54868#47932#54364#49884' '#48143' '#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SPMARK2: TsEdit
          Left = 112
          Top = 47
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK3: TsEdit
          Left = 112
          Top = 69
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK5: TsEdit
          Left = 112
          Top = 113
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK4: TsEdit
          Left = 112
          Top = 91
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK7: TsEdit
          Left = 112
          Top = 157
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK6: TsEdit
          Left = 112
          Top = 135
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK9: TsEdit
          Left = 112
          Top = 201
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK8: TsEdit
          Left = 112
          Top = 179
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SPMARK10: TsEdit
          Left = 112
          Top = 223
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel13: TsPanel
          Left = 1
          Top = 249
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49345#54408#47749#49464
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
        end
        object edt_GOODS1: TsEdit
          Left = 112
          Top = 272
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54408#47749#49464
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_GOODS2: TsEdit
          Left = 112
          Top = 294
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS3: TsEdit
          Left = 112
          Top = 316
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS4: TsEdit
          Left = 112
          Top = 338
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS5: TsEdit
          Left = 112
          Top = 360
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel14: TsPanel
          Left = 425
          Top = 8
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #49888#52397#51088' / '#47749#51032#51064
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
        end
        object edt_MSNAME1: TsEdit
          Left = 534
          Top = 31
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#52397#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_MSNAME2: TsEdit
          Left = 534
          Top = 53
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
        end
        object edt_MSNAME3: TsEdit
          Left = 534
          Top = 75
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_AXNAME1: TsEdit
          Left = 534
          Top = 97
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_AXNAME2: TsEdit
          Left = 534
          Top = 119
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AXNAME3: TsEdit
          Left = 534
          Top = 141
          Width = 284
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel5: TsPanel
          Left = 425
          Top = 167
          Width = 394
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #44208#51228#44396#48516' / '#48156#44553#51008#54665
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
        end
        object edt_TRMPAYC: TsEdit
          Tag = 107
          Left = 536
          Top = 190
          Width = 35
          Height = 18
          Hint = #44208#51228#44592#44036
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          OnDblClick = edt_CodeClickEvent
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44208#51228#44396#48516' '#48143' '#44592#44036
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn11: TsBitBtn
          Tag = 9
          Left = 572
          Top = 190
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 26
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_TRMPAYCNAME: TsEdit
          Left = 597
          Top = 190
          Width = 223
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TRMPAY: TsEdit
          Left = 536
          Top = 212
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BANKCD: TsEdit
          Tag = 10
          Left = 536
          Top = 234
          Width = 35
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          OnDblClick = edt_BANKCDDblClick
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#44553#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn12: TsBitBtn
          Tag = 10
          Left = 572
          Top = 234
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 30
          TabStop = False
          OnClick = btn_CodeClickEvent
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_BANKTXT: TsEdit
          Left = 597
          Top = 234
          Width = 223
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BANKBR: TsEdit
          Left = 536
          Top = 256
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS6: TsEdit
          Left = 112
          Top = 382
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS7: TsEdit
          Left = 112
          Top = 404
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS8: TsEdit
          Left = 112
          Top = 426
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS9: TsEdit
          Left = 112
          Top = 448
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.SkinSection = 'EDIT'
        end
        object edt_GOODS10: TsEdit
          Left = 112
          Top = 470
          Width = 283
          Height = 18
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 1113
        Height = 561
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        object sSplitter2: TsSplitter
          Left = 1
          Top = 33
          Width = 1111
          Height = 3
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sPanel6: TsPanel
          Left = 1
          Top = 1
          Width = 1111
          Height = 32
          SkinData.SkinSection = 'PANEL'
          Align = alTop
          
          TabOrder = 0
          object edt_SearchText: TsEdit
            Left = 86
            Top = 5
            Width = 233
            Height = 25
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Visible = False
            SkinData.SkinSection = 'EDIT'
          end
          object com_SearchKeyword: TsComboBox
            Left = 4
            Top = 5
            Width = 81
            Height = 23
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            SkinData.SkinSection = 'COMBOBOX'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 0
            TabOrder = 2
            TabStop = False
            Text = #46321#47197#51068#51088
            OnSelect = com_SearchKeywordSelect
            Items.Strings = (
              #46321#47197#51068#51088
              #44288#47532#48264#54840
              #44396#47588#51088)
          end
          object Mask_SearchDate1: TsMaskEdit
            Left = 86
            Top = 5
            Width = 82
            Height = 25
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 0
            Text = '20161125'
            CheckOnExit = True
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn13: TsBitBtn
            Left = 319
            Top = 5
            Width = 70
            Height = 23
            Cursor = crHandPoint
            Caption = #44160#49353
            TabOrder = 3
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sBitBtn14: TsBitBtn
            Tag = 902
            Left = 167
            Top = 5
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 5
            TabStop = False
            ImageIndex = 9
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object Mask_SearchDate2: TsMaskEdit
            Left = 214
            Top = 5
            Width = 82
            Height = 25
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 1
            Text = '20161125'
            CheckOnExit = True
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn28: TsBitBtn
            Tag = 903
            Left = 295
            Top = 5
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 6
            TabStop = False
            ImageIndex = 9
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sPanel15: TsPanel
            Left = 190
            Top = 5
            Width = 25
            Height = 23
            SkinData.SkinSection = 'PANEL'
            Caption = '~'
            
            TabOrder = 7
          end
        end
        object sDBGrid1: TsDBGrid
          Left = 1
          Top = 36
          Width = 1111
          Height = 524
          Align = alClient
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #44404#47548#52404
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid2DrawColumnCell
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
  end
  object sPanel18: TsPanel [5]
    Left = 4
    Top = 118
    Width = 289
    Height = 635
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    object sPanel53: TsPanel
      Left = 1
      Top = 1
      Width = 287
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 0
      object Mask_fromDate: TsMaskEdit
        Left = 111
        Top = 5
        Width = 74
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 260
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 186
        Top = 5
        Width = 74
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 34
      Width = 287
      Height = 600
      Align = alClient
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      SkinData.SkinSection = 'EDIT'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 48
    Top = 216
  end
end
