unit UI_LOCADV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sCheckBox, sButton, Grids, DBGrids,
  acDBGrid, sMemo, sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit,
  ExtCtrls, sPanel, Mask, sMaskEdit, Buttons, sBitBtn, sEdit, sLabel,
  sSpeedButton, sScrollBox, sSplitter, ComCtrls, sPageControl,
  sSkinProvider, DB, ADODB, StrUtils, DateUtils, sRadioButton;

type
  TUI_LOCADV_frm = class(TChildForm_frm)
    sPageControl1: TsPageControl;
    sTabSheet2: TsTabSheet;
    sSplitter3: TsSplitter;
    sScrollBox1: TsScrollBox;
    sSpeedButton1: TsSpeedButton;
    sLabel1: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    edt_CreateUserSangho: TsEdit;
    edt_CreateUserDaepyo: TsEdit;
    edt_CreateUserAddr1: TsEdit;
    edt_CreateUserAddr2: TsEdit;
    edt_CreateUserAddr3: TsEdit;
    edt_CreateUserSaupNo: TsMaskEdit;
    sPanel5: TsPanel;
    sPanel7: TsPanel;
    edt_RecvSangho: TsEdit;
    edt_RecvDaepyo: TsEdit;
    edt_RecvAddr1: TsEdit;
    edt_RecvAddr2: TsEdit;
    edt_RecvAddr3: TsEdit;
    edt_RecvEmail1: TsEdit;
    sPanel8: TsPanel;
    edt_RecvEmail2: TsEdit;
    sPanel9: TsPanel;
    edt_CreateBankCode: TsEdit;
    edt_CreateBankName: TsEdit;
    edt_CreateBankBrunch: TsEdit;
    edt_LOC_TYPE: TsEdit;
    sEdit21: TsEdit;
    edt_LOC1AMTC: TsEdit;
    Curr_LOC1AMT: TsCurrencyEdit;
    edt_PLUS: TsEdit;
    edt_Minus: TsEdit;
    edt_BUSINESS: TsEdit;
    sEdit26: TsEdit;
    edt_Sell1: TsEdit;
    edt_Sell2: TsEdit;
    edt_Sell3: TsEdit;
    edt_Sell4: TsEdit;
    edt_Sell5: TsEdit;
    edt_Sell6: TsEdit;
    edt_Sell7: TsEdit;
    edt_Sell8: TsEdit;
    edt_Sell9: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    Curr_AttachDoc1: TsCurrencyEdit;
    Curr_AttachDoc2: TsCurrencyEdit;
    Curr_AttachDoc3: TsCurrencyEdit;
    Curr_AttachDoc4: TsCurrencyEdit;
    Curr_AttachDoc5: TsCurrencyEdit;
    sPanel12: TsPanel;
    Curr_CreateSeq: TsCurrencyEdit;
    Mask_CreateRequestDate: TsMaskEdit;
    sPanel13: TsPanel;
    Mask_IndoDate: TsMaskEdit;
    Mask_ExpiryDate: TsMaskEdit;
    edt_TRANSPRT: TsEdit;
    sEdit38: TsEdit;
    sPanel18: TsPanel;
    sTabSheet4: TsTabSheet;
    sSplitter4: TsSplitter;
    sPanel21: TsPanel;
    sPanel14: TsPanel;
    sMemo1: TsMemo;
    edt_Hs: TsMaskEdit;
    sMemo2: TsMemo;
    sMemo3: TsMemo;
    sPanel15: TsPanel;
    edt_SetupDocumentType: TsEdit;
    sEdit40: TsEdit;
    edt_LetterOfCreditNo: TsEdit;
    sMaskEdit7: TsMaskEdit;
    sMaskEdit8: TsMaskEdit;
    sEdit43: TsEdit;
    sEdit44: TsEdit;
    edt_ExportAreaCode: TsEdit;
    sEdit50: TsEdit;
    sEdit51: TsEdit;
    sMemo4: TsMemo;
    sPanel17: TsPanel;
    edt_Sign1: TsEdit;
    edt_Sign2: TsEdit;
    edt_Sign3: TsEdit;
    edt_ExportAreaName: TsEdit;
    sPanel23: TsPanel;
    sTabSheet1: TsTabSheet;
    sSplitter2: TsSplitter;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel1: TsPanel;
    sSpeedButton4: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sSplitter1: TsSplitter;
    sPanel4: TsPanel;
    edt_DocNo1: TsEdit;
    edt_DocNo2: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_DocNo3: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    sPanel22: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sCurrencyEdit1: TsCurrencyEdit;
    sCurrencyEdit2: TsCurrencyEdit;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_REF: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    dsList: TDataSource;
    edt_LOC2AMTC: TsEdit;
    Curr_LOC2AMT: TsCurrencyEdit;
    edt_ID: TsEdit;
    Mask_RecvSaup: TsMaskEdit;
    edt_LCNO: TsEdit;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sButton1: TsButton;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sSplitter5: TsSplitter;
    procedure FormCreate(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sRadioButton1Click(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Mask_fromDateChange(Sender: TObject);
    procedure Mask_toDateChange(Sender: TObject);
    procedure Mask_SearchDate1Change(Sender: TObject);
    procedure Mask_SearchDate2Change(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sBitBtn21Click(Sender: TObject);
  private
    { Private declarations }
    FLOCADV_SQL : String;
    procedure ReadDocument;
    procedure Readlist(fromDate, toDate : String;KeyValue : String='');overload;
    procedure Readlist(OrderSyntax : string = '');overload;
    procedure DelDocument;
  public
    { Public declarations }
  end;

var
  UI_LOCADV_frm: TUI_LOCADV_frm;

implementation

uses ICON, MessageDefine, CodeContents, KISCalendar, Commonlib,
  LOCADV_PRINT, MSSQL;

{$R *.dfm}

{ TUI_LOCADV_frm }

procedure TUI_LOCADV_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := FLOCADV_SQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;
end;

procedure TUI_LOCADV_frm.Readlist(OrderSyntax: string);
begin
//------------------------------------------------------------------------------
//0 개설일자
//1 관리번호
//2 수혜자
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// 관리번호 및 수혜자는 입력안할시 엄청난 양의 데이터를
// 검색하기 때문에 꼭 키워드를 입력해야함
//------------------------------------------------------------------------------
  IF com_SearchKeyword.ItemIndex in [1,2] Then
  begin
    IF Trim(edt_SearchText.Text) = '' Then
    begin
      IF sPageControl1.ActivePageIndex = 2 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    SQL.Text := FLOCADV_SQL;
    Case com_SearchKeyword.ItemIndex of
      0: begin
           SQL.Add('WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
           Mask_fromDate.Text := Mask_SearchDate1.Text;
           Mask_toDate.Text := Mask_SearchDate2.Text;
         end;
      1: SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    IF OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;
  end;
end;

procedure TUI_LOCADV_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FLOCADV_SQL := qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOCADV_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;

//  sPanel4.Visible := not (sPageControl1.ActivePageIndex = 2);
  sPanel22.Visible := not (sPageControl1.ActivePageIndex = 2);

end;

procedure TUI_LOCADV_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_LOCADV_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist;  
end;

procedure TUI_LOCADV_frm.FormShow(Sender: TObject);
begin
  inherited;
  DMCodeContents.OPENTABLE(DMCodeContents.Config);
  IF DMCodeContents.ConfigAutoGubun.AsInteger = 0 Then
    edt_DocNo3.Color := clWhite
  else
    edt_DocNo3.Color := clBtnFace;

  sPageControl1Change(sPageControl1);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist;
end;

procedure TUI_LOCADV_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCADV_frm.ReadDocument;
begin
//------------------------------------------------------------------------------
// 읽기전용
//------------------------------------------------------------------------------
//  ReadOnlyControlValue(sPanel4);
//  ReadOnlyControlValue(sScrollBox1);
//  ReadOnlyControlValue(sPanel21);
//
//  ReadOnlyControlValue(sPanel24,False);
  EnabledControlValue(sPanel4);
  EnabledControlValue(sScrollBox1);
  EnabledControlValue(sPanel21);

  EnabledControlValue(sPanel24,False);
//------------------------------------------------------------------------------
// 데이터 가져오기
//------------------------------------------------------------------------------
  //관리번호
  edt_DocNo1.Text := LeftStr( qryListMAINT_NO.AsString , 14);
  edt_DocNo2.Text := MidStr( qryListMAINT_NO.AsString, 16,2 );
  edt_DocNo3.Text := MidStr( qryListMAINT_NO.AsString, 18,20);
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;

//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  edt_CreateUserSangho.Text := qryListAPPLIC1.AsString;
  edt_CreateUserDaepyo.Text := qryListAPPLIC2.AsString;
  edt_CreateUserAddr1.Text  := qryListAPPADDR1.AsString;
  edt_CreateUserAddr2.Text  := qryListAPPADDR2.AsString;
  edt_CreateUserAddr3.Text  := qryListAPPADDR3.AsString;
  edt_CreateUserSangho.Text := qryListAPPLIC3.AsString;
//------------------------------------------------------------------------------
// 수혜자
//------------------------------------------------------------------------------
  //수혜자상호
  edt_RecvSangho.Text := qryListBENEFC1.AsString;
  //대표자
  edt_RecvDaepyo.Text := qryListBENEFC2.AsString;
  //사업자/식별자
  Mask_RecvSaup.Text := LeftStr(qryListBENEFC3.AsString,10);
  edt_ID.Text := AnsiReplaceText( MidStr(qryListBENEFC3.AsString,11,100) , '/' , ' / ');
  //수혜자주소
  edt_RecvAddr1.Text := qryListBNFADDR1.AsString;
  edt_RecvAddr2.Text := qryListBNFADDR2.AsString;
  edt_RecvAddr3.Text := qryListBNFADDR3.AsString;
  //이메일주소
  edt_RecvEmail1.Text := qryListBNFEMAILID.AsString;
  edt_RecvEmail2.Text := qryListBNFDOMAIN.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  edt_CreateBankCode.Text := qryListAP_BANK.AsString;
  edt_CreateBankName.Text := qryListAP_BANK1.AsString;
  edt_CreateBankBrunch.Text := qryListAP_BANK2.AsString;
  //신용장종류
  edt_LOC_TYPE.Text := qryListLOC_TYPE.AsString;
  sEdit21.Text := qryListLOC_TYPENAME.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qryListLOC1AMTC.AsString;
  Curr_LOC1AMT.Value := qryListLOC1AMT.AsCurrency;
  //개설금액(원화)
  edt_LOC2AMTC.Text := qryListLOC2AMTC.AsString;
  Curr_LOC2AMT.Value := qryListLOC2AMT.AsCurrency;

  //허용오차
  edt_PLUS.Text := qryListCD_PERP.AsString;
  edt_Minus.Text := qryListCD_PERM.AsString;
  //개설근거별 용도
  edt_BUSINESS.Text := qryListBUSINESS.AsString;
  sEdit26.Text := qryListBUSINESSNAME.AsString;
//------------------------------------------------------------------------------
// 매도확약서
//------------------------------------------------------------------------------
  edt_Sell1.Text := qryListOFFERNO1.AsString;
  edt_Sell2.Text := qryListOFFERNO2.AsString;
  edt_Sell3.Text := qryListOFFERNO3.AsString;
  edt_Sell4.Text := qryListOFFERNO4.AsString;
  edt_Sell5.Text := qryListOFFERNO5.AsString;
  edt_Sell6.Text := qryListOFFERNO6.AsString;
  edt_Sell7.Text := qryListOFFERNO7.AsString;
  edt_Sell8.Text := qryListOFFERNO8.AsString;
  edt_Sell9.Text := qryListOFFERNO9.AsString;
//------------------------------------------------------------------------------
// 주요구비서류
//------------------------------------------------------------------------------
  Curr_AttachDoc1.Value := qryListDOCCOPY1.AsInteger;
  Curr_AttachDoc2.Value := qryListDOCCOPY2.AsInteger;
  Curr_AttachDoc3.Value := qryListDOCCOPY3.AsInteger;
  Curr_AttachDoc4.Value := qryListDOCCOPY4.AsInteger;
  Curr_AttachDoc5.Value := qryListDOCCOPY5.AsInteger;
//------------------------------------------------------------------------------
// 개설관련
//------------------------------------------------------------------------------
  Curr_CreateSeq.Value := qryListOPEN_NO.AsInteger;
  Mask_CreateRequestDate.Text := qryListADV_DATE.AsString;
  Mask_IndoDate.Text := qryListDELIVERY.AsString;
  Mask_ExpiryDate.Text := qryListEXPIRY.AsString;
  edt_TRANSPRT.Text := qryListTRANSPRT.AsString;
  sEdit38.Text := qryListTRANSPRTNAME.AsString;
  edt_LCNO.Text := qryListLC_NO.AsString;
//------------------------------------------------------------------------------
// 서류제출기간
//------------------------------------------------------------------------------
  sCurrencyEdit2.Value := qryListDOC_PRD.AsInteger;
//------------------------------------------------------------------------------
// 대표공급물품
//------------------------------------------------------------------------------
  edt_Hs.Text := qryListBSN_HSCODE.AsString;
  //대표공급물품명
  sMemo1.Text := qrylistGOODDES1.AsString;
  //기타구비서류
  sMemo2.Text := qrylistDOC_ETC1.AsString;
  //기타정보
  sMemo3.Text := qryListREMARK1.AsString;
//------------------------------------------------------------------------------
// 원수출신용장
//------------------------------------------------------------------------------
  //개설근거서류
  edt_SetupDocumentType.Text := qryListDOC_DTL.AsString;
  sEdit40.Text := qryListDOC_DTLNAME.AsString;
  //신용장(계약서)번호
  edt_LetterOfCreditNo.Text := qryListDOC_NO.AsString;
  //선적기일
  sMaskEdit7.Text := qryListLOADDATE.AsString;
  //유효기일
  sMaskEdit8.Text := qryListEXPDATE.AsString;
  //대금결제조건
  sEdit43.Text := qrylistPAYMENT.AsString;
  sEdit44.Text := qryListPAYMENTNAME.AsString;
  //수출지역
  edt_ExportAreaCode.Text := qrylistDEST.AsString;
  edt_ExportAreaName.Text := qryListDESTNAME.AsString;
  //발행기관
  sEdit50.Text := qryListISBANK1.AsString;
  sEdit51.Text := qryListISBANK2.AsString;
  //대표수출품목
  sMemo4.Text := qryListEXGOOD1.AsString;
//------------------------------------------------------------------------------
// 발신기관 전자서명
//------------------------------------------------------------------------------
  edt_Sign1.Text := qrylistEXNAME1.AsString;
  edt_Sign2.Text := qrylistEXNAME2.AsString;
  edt_Sign3.Text := qrylistEXNAME3.AsString;
end;

procedure TUI_LOCADV_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
end;

procedure TUI_LOCADV_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  If DataSet.RecordCount = 0 Then ReadDocument;
end;

procedure TUI_LOCADV_frm.sRadioButton1Click(Sender: TObject);
var
  i : integer;
begin
  inherited;

  for i := 0 to sDBGrid1.FieldCount-1 do
  begin
    IF (Sender as TsRadioButton).Tag = 0 Then
    begin
      sDBGrid1.Columns[9].FieldName := 'LOC2AMT';
      sDBGrid1.Columns[10].FieldName := 'LOC2AMTC';
    end
    else
    IF (Sender as TsRadioButton).Tag = 1 Then
    begin
      sDBGrid1.Columns[9].FieldName := 'LOC1AMT';
      sDBGrid1.Columns[10].FieldName := 'LOC1AMTC';
    end;
  end;

end;

procedure TUI_LOCADV_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  LOCADV_PRINT_frm := TLOCADV_PRINT_frm.Create(Self);
  try
    LOCADV_PRINT_frm.PrintDocument(qryList.Fields,(Sender as TsButton).Tag = 1);
  finally
    FreeAndNil( LOCADV_PRINT_frm );
  end;
end;

procedure TUI_LOCADV_frm.DelDocument;
var
  DocNo : String;
  nCursor : Integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := edt_DocNo1.Text+'-'+edt_DocNo2.Text+edt_DocNo3.Text;
    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCADV WHERE MAINT_NO = '+QuotedStr(DocNo);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;
          qryList.Close;
          qryList.Open;
          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCADV_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  DelDocument;
end;

procedure TUI_LOCADV_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCADV_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCADV_frm := nil;
end;

procedure TUI_LOCADV_frm.Mask_fromDateChange(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1.Text := Mask_fromDate.Text;
end;

procedure TUI_LOCADV_frm.Mask_toDateChange(Sender: TObject);
begin
  inherited;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_LOCADV_frm.Mask_SearchDate1Change(Sender: TObject);
begin
  inherited;
  Mask_fromDate.Text := Mask_SearchDate1.Text;
end;

procedure TUI_LOCADV_frm.Mask_SearchDate2Change(Sender: TObject);
begin
  inherited;
  Mask_toDate.Text := Mask_SearchDate2.Text;
end;

procedure TUI_LOCADV_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
end;

procedure TUI_LOCADV_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_F1 : sPageControl1.ActivePageIndex := 0;
    Vk_F2 : sPageControl1.ActivePageIndex := 1;
    VK_F3 : sPageControl1.ActivePageIndex := 2;
  end;
    sPageControl1Change(sPageControl1);  
end;

procedure TUI_LOCADV_frm.sDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clblack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
  
end;

procedure TUI_LOCADV_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    900 : sMaskEdit1DblClick(Mask_SearchDate1);
    907 : sMaskEdit1DblClick(Mask_SearchDate2);
  end;
end;

end.
