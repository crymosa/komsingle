inherited UI_LOCADV_frm: TUI_LOCADV_frm
  Left = 2369
  Top = 92
  BorderWidth = 4
  Caption = #45236#44397#49888#50857#51109#44060#49444#51025#45813#49436
  ClientHeight = 673
  ClientWidth = 1114
  Constraints.MaxHeight = 728
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter5: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPageControl1: TsPageControl [2]
    Left = 0
    Top = 79
    Width = 1114
    Height = 594
    ActivePage = sTabSheet2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 0
    TabWidth = 110
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sScrollBox1: TsScrollBox
        Left = 283
        Top = 3
        Width = 823
        Height = 551
        Align = alClient
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          819
          547)
        object sSpeedButton1: TsSpeedButton
          Left = 426
          Top = 8
          Width = 10
          Height = 522
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel1: TsLabel
          Left = 584
          Top = 363
          Width = 76
          Height = 15
          AutoSize = False
          Caption = #52264' '#45236#44397#49888#50857#51109
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel3: TsLabel
          Left = 504
          Top = 504
          Width = 153
          Height = 13
          AutoSize = False
          Caption = #47932#54408#49688#47161#51613#47749#49436' '#48156#44553#51068#47196#48512#53552
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel4: TsLabel
          Left = 690
          Top = 504
          Width = 37
          Height = 13
          Caption = #51068' '#51060#45236
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object edt_CreateUserSangho: TsEdit
          Left = 160
          Top = 22
          Width = 225
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserDaepyo: TsEdit
          Left = 160
          Top = 41
          Width = 225
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserAddr1: TsEdit
          Left = 160
          Top = 60
          Width = 225
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserAddr2: TsEdit
          Left = 160
          Top = 79
          Width = 225
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateUserAddr3: TsEdit
          Left = 160
          Top = 98
          Width = 225
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateUserSaupNo: TsMaskEdit
          Left = 160
          Top = 117
          Width = 97
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '999-99-99999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          Text = '2200222960'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel5: TsPanel
          Left = 40
          Top = 1
          Width = 345
          Height = 20
          Caption = #44060#49444#51032#47280#51064
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel7: TsPanel
          Left = 40
          Top = 140
          Width = 345
          Height = 20
          Caption = #49688#54812#51088
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 42
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_RecvSangho: TsEdit
          Left = 160
          Top = 161
          Width = 223
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvDaepyo: TsEdit
          Left = 160
          Top = 180
          Width = 223
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvAddr1: TsEdit
          Left = 160
          Top = 237
          Width = 223
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54812#51088' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvAddr2: TsEdit
          Left = 160
          Top = 256
          Width = 223
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvAddr3: TsEdit
          Left = 160
          Top = 275
          Width = 223
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvEmail1: TsEdit
          Left = 160
          Top = 294
          Width = 81
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51060#47700#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel8: TsPanel
          Left = 242
          Top = 294
          Width = 20
          Height = 19
          Caption = '@'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 43
          SkinData.SkinSection = 'PANEL'
        end
        object edt_RecvEmail2: TsEdit
          Left = 263
          Top = 294
          Width = 120
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel9: TsPanel
          Left = 40
          Top = 317
          Width = 345
          Height = 20
          Caption = #44060#49444#51008#54665'/'#49888#50857#51109
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_CreateBankCode: TsEdit
          Left = 160
          Top = 338
          Width = 65
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateBankName: TsEdit
          Left = 160
          Top = 357
          Width = 223
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateBankBrunch: TsEdit
          Left = 160
          Top = 376
          Width = 223
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LOC_TYPE: TsEdit
          Tag = 101
          Left = 160
          Top = 395
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4487'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sEdit21: TsEdit
          Left = 194
          Top = 395
          Width = 189
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 45
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LOC1AMTC: TsEdit
          Tag = 102
          Left = 160
          Top = 414
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object Curr_LOC1AMT: TsCurrencyEdit
          Left = 194
          Top = 414
          Width = 143
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_PLUS: TsEdit
          Left = 160
          Top = 452
          Width = 33
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54728#50857#50724#52264'(+)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_Minus: TsEdit
          Left = 216
          Top = 452
          Width = 33
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '(-)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BUSINESS: TsEdit
          Tag = 103
          Left = 160
          Top = 492
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4025'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44540#44144#48324' '#50857#46020
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sEdit26: TsEdit
          Left = 194
          Top = 492
          Width = 189
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 46
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Sell1: TsEdit
          Left = 600
          Top = 22
          Width = 161
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#47588#46020#54869#50557#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_Sell2: TsEdit
          Left = 600
          Top = 41
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Sell3: TsEdit
          Left = 600
          Top = 60
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit30'
        end
        object edt_Sell4: TsEdit
          Left = 600
          Top = 79
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit31'
        end
        object edt_Sell5: TsEdit
          Left = 600
          Top = 98
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit32'
        end
        object edt_Sell6: TsEdit
          Left = 600
          Top = 117
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit33'
        end
        object edt_Sell7: TsEdit
          Left = 600
          Top = 136
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit34'
        end
        object edt_Sell8: TsEdit
          Left = 600
          Top = 155
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit35'
        end
        object edt_Sell9: TsEdit
          Left = 600
          Top = 174
          Width = 161
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit36'
        end
        object sPanel10: TsPanel
          Left = 478
          Top = 1
          Width = 283
          Height = 20
          Caption = #47932#54408#47588#46020#54869#50557#49436
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 47
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel11: TsPanel
          Left = 478
          Top = 197
          Width = 283
          Height = 20
          Caption = #51452#50836#44396#48708#49436#47448
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 48
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object Curr_AttachDoc1: TsCurrencyEdit
          Left = 738
          Top = 218
          Width = 23
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object Curr_AttachDoc2: TsCurrencyEdit
          Left = 738
          Top = 241
          Width = 23
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#51088#48156#54665' '#49464#44552#44228#49328#49436' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 1
          Value = 1
        end
        object Curr_AttachDoc3: TsCurrencyEdit
          Tag = 1
          Left = 738
          Top = 264
          Width = 23
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#47749#49464#44032' '#44592#51116#46108' '#49569#51109
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 1
        end
        object Curr_AttachDoc4: TsCurrencyEdit
          Left = 738
          Top = 287
          Width = 23
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48376' '#45236#44397#49888#50857#51109#51032' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object Curr_AttachDoc5: TsCurrencyEdit
          Left = 738
          Top = 310
          Width = 23
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#51088#48156#54665' '#47932#54408#47588#46020#54869#50557#49436' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object sPanel12: TsPanel
          Left = 478
          Top = 337
          Width = 283
          Height = 20
          Caption = #44060#49444#44288#47144
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 49
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object Curr_CreateSeq: TsCurrencyEdit
          Left = 559
          Top = 358
          Width = 23
          Height = 23
          HelpContext = 1
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#54924#52264
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 0
          DisplayFormat = '0'
        end
        object Mask_CreateRequestDate: TsMaskEdit
          Left = 559
          Top = 381
          Width = 83
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel13: TsPanel
          Left = 478
          Top = 480
          Width = 283
          Height = 20
          Caption = #49436#47448#51228#49884#44592#44036
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 50
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object Mask_IndoDate: TsMaskEdit
          Left = 559
          Top = 400
          Width = 83
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object Mask_ExpiryDate: TsMaskEdit
          Left = 559
          Top = 419
          Width = 83
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TRANSPRT: TsEdit
          Tag = 104
          Left = 559
          Top = 438
          Width = 34
          Height = 19
          Hint = 'PSHIP'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48516#54624#54728#50857#50668#48512
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sEdit38: TsEdit
          Left = 594
          Top = 438
          Width = 167
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 51
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sCurrencyEdit1: TsCurrencyEdit
          Left = 160
          Top = 471
          Width = 89
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 52
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47588#47588' '#44592#51456#50984
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object sCurrencyEdit2: TsCurrencyEdit
          Left = 656
          Top = 501
          Width = 33
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 53
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_LOC2AMTC: TsEdit
          Tag = 102
          Left = 160
          Top = 433
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 54
          Text = 'KRW'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44552#50529'('#50896#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object Curr_LOC2AMT: TsCurrencyEdit
          Left = 194
          Top = 433
          Width = 143
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 55
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_ID: TsEdit
          Left = 160
          Top = 199
          Width = 223
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 56
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49885#48324#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object Mask_RecvSaup: TsMaskEdit
          Left = 160
          Top = 218
          Width = 97
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '999-99-99999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 57
          Text = '2200222960'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LCNO: TsEdit
          Tag = 104
          Left = 559
          Top = 457
          Width = 202
          Height = 19
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          MaxLength = 2
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 58
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109' '#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
      end
      object sPanel18: TsPanel
        Left = 0
        Top = 3
        Width = 283
        Height = 551
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel18'
        TabOrder = 1
        SkinData.SkinSection = 'TRANSPARENT'
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #45824#54364#44277#44553#47932#54408
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel21: TsPanel
        Left = 283
        Top = 3
        Width = 823
        Height = 551
        Align = alClient
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object sPanel14: TsPanel
          Left = 80
          Top = 1
          Width = 281
          Height = 20
          Caption = #45824#54364#44277#44553#47932#54408
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sMemo1: TsMemo
          Left = 201
          Top = 41
          Width = 446
          Height = 63
          HelpContext = 1
          Color = clWhite
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#44277#44553#47932#54408#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Hs: TsMaskEdit
          Tag = 201
          Left = 201
          Top = 22
          Width = 89
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '9999.99-9999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '1234567890'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo2: TsMemo
          Left = 201
          Top = 104
          Width = 446
          Height = 63
          Color = clWhite
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 2
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#44396#48708#49436#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo3: TsMemo
          Left = 201
          Top = 167
          Width = 446
          Height = 63
          Color = clWhite
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel15: TsPanel
          Left = 80
          Top = 242
          Width = 281
          Height = 20
          Caption = #50896#49688#52636#49888#50857#51109
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_SetupDocumentType: TsEdit
          Tag = 105
          Left = 189
          Top = 263
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'1001'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44540#44144#49436#47448#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit40: TsEdit
          Left = 223
          Top = 263
          Width = 138
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LetterOfCreditNo: TsEdit
          Left = 189
          Top = 282
          Width = 172
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sMaskEdit7: TsMaskEdit
          Left = 189
          Top = 301
          Width = 82
          Height = 19
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sMaskEdit8: TsMaskEdit
          Left = 189
          Top = 320
          Width = 82
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit43: TsEdit
          Tag = 107
          Left = 485
          Top = 320
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4277'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#44552#44208#51228#51312#44148
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit44: TsEdit
          Left = 519
          Top = 320
          Width = 178
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ExportAreaCode: TsEdit
          Tag = 108
          Left = 485
          Top = 263
          Width = 33
          Height = 19
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#52636#51648#50669
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit50: TsEdit
          Left = 485
          Top = 282
          Width = 212
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51008#54665'('#54869#51064#44592#44288')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit51: TsEdit
          Left = 485
          Top = 301
          Width = 212
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo4: TsMemo
          Left = 189
          Top = 359
          Width = 512
          Height = 82
          Color = clWhite
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 12
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#49688#52636#54408#47785
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel17: TsPanel
          Left = 80
          Top = 453
          Width = 281
          Height = 20
          Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_Sign1: TsEdit
          Left = 185
          Top = 474
          Width = 189
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_Sign2: TsEdit
          Left = 185
          Top = 493
          Width = 189
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
        end
        object edt_Sign3: TsEdit
          Left = 185
          Top = 512
          Width = 101
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ExportAreaName: TsEdit
          Left = 519
          Top = 263
          Width = 178
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
      object sPanel23: TsPanel
        Left = 0
        Top = 3
        Width = 283
        Height = 551
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel18'
        TabOrder = 1
        SkinData.SkinSection = 'TRANSPARENT'
      end
    end
    object sTabSheet1: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 32
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 1106
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentFont = False
          TabOrder = 4
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 21
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnChange = Mask_SearchDate1Change
          OnDblClick = sMaskEdit1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 320
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnChange = Mask_SearchDate2Change
          OnDblClick = sMaskEdit1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 907
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
        object sRadioButton1: TsRadioButton
          Left = 952
          Top = 7
          Width = 52
          Height = 19
          Caption = #50896#54868
          Checked = True
          TabOrder = 8
          TabStop = True
          OnClick = sRadioButton1Click
          SkinData.SkinSection = 'CHECKBOX'
        end
        object sRadioButton2: TsRadioButton
          Tag = 1
          Left = 1008
          Top = 7
          Width = 52
          Height = 19
          Caption = #50808#54868
          TabOrder = 9
          OnClick = sRadioButton1Click
          SkinData.SkinSection = 'CHECKBOX'
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1106
        Height = 519
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid3DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 213
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ADV_DATE'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LC_NO'
            Title.Caption = 'L/C'#48264#54840
            Width = 122
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ISS_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'EXPIRY'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 176
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC1AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
            Width = -1
            Visible = False
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC1AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = -1
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'LOC2AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC2AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
    end
  end
  object sPanel1: TsPanel [3]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      1114
      41)
    object sSpeedButton4: TsSpeedButton
      Left = 424
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel10: TsLabel
      Left = 8
      Top = 5
      Width = 135
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel11: TsLabel
      Left = 8
      Top = 20
      Width = 45
      Height = 13
      Caption = 'LOCADV'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 147
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1035
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 157
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 224
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton1: TsButton
      Tag = 1
      Left = 323
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [4]
    Left = 0
    Top = 44
    Width = 1114
    Height = 32
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object edt_DocNo1: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_DocNo2: TsEdit
      Left = 186
      Top = 5
      Width = 27
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn2: TsBitBtn
      Left = 212
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 2
      TabStop = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_DocNo3: TsEdit
      Left = 237
      Top = 5
      Width = 92
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      Text = '161122001'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sMaskEdit1: TsMaskEdit
      Left = 398
      Top = 5
      Width = 83
      Height = 23
      AutoSize = False
      Color = clWhite
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      Text = '20161122'
      OnDblClick = sMaskEdit1DblClick
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 544
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg1: TsEdit
      Tag = 109
      Left = 670
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg2: TsEdit
      Tag = 110
      Left = 746
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object sPanel22: TsPanel [5]
    Left = 5
    Top = 118
    Width = 282
    Height = 692
    BevelOuter = bvNone
    Caption = 'sPanel18'
    TabOrder = 3
    SkinData.SkinSection = 'TRANSPARENT'
    object sDBGrid3: TsDBGrid
      Left = 0
      Top = 33
      Width = 282
      Height = 659
      Align = alClient
      Color = clWhite
      Ctl3D = True
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid3DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 188
          Visible = True
        end>
    end
    object sPanel24: TsPanel
      Left = 0
      Top = 0
      Width = 282
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      SkinData.SkinSection = 'PANEL'
      DesignSize = (
        282
        33)
      object Mask_fromDate: TsMaskEdit
        Left = 106
        Top = 5
        Width = 74
        Height = 23
        Anchors = [akTop, akRight]
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        OnChange = Mask_fromDateChange
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 256
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 181
        Top = 5
        Width = 74
        Height = 23
        Anchors = [akTop, akRight]
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        OnChange = Mask_toDateChange
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 232
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      '      ,N1001.DOC_NAME as DOC_DTLNAME '
      '      ,N4277.DOC_NAME as PAYMENTNAME'
      '      ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCADV.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'ADV.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCADV.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCADV.DOC_DTL = N1001.CODE'#9
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCADV.DEST = NAT.CODE')
    Left = 24
    Top = 200
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      EditMask = '9999.99-9999;0'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 200
  end
end
