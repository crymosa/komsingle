<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="APPSPC">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/APPSPC2BK/APPSPC_M/SRID_M/LocalID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/APPSPC2BK/APPSPC_M/SRID_M/PartnerID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailSenderIdentifier">
					<xsl:value-of select="/APPSPC2BK/APPSPC_M/SRID_M/LocalDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailReceiverIdentifier">
					<xsl:value-of select="/APPSPC2BK/APPSPC_M/SRID_M/PartnerDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/APPSPC2BK/DocumentHeader/UniqueReferenceNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">APPSPC</xsl:element>
				<xsl:element name="ApplicationAreaMessageVersionText">
					<xsl:value-of select="/APPSPC2BK/APPSPC_M/DocumentCode"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName">APPSPC</xsl:element>
					<!-- 전자문서의 코드 -->
					<xsl:element name="DocumentCode">2BK</xsl:element>
					<!-- 문서번호 -->
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/APPSPC2BK/APPSPC_M/DocumentIdentifier"/>
					</xsl:element>
					<!-- 문서기능 -->
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/APPSPC2BK/APPSPC_M/FunctionCode"/>
					</xsl:element>
					<!-- 응답유형 -->
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/APPSPC2BK/APPSPC_M/ResponseTypeCode"/>
					</xsl:element>
					<!-- 추심(매입)은행 -->
					<xsl:element name="CollectionBank">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankCode"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Branch">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankBranch"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Account">
							<xsl:element name="AccountIdentifier">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccount1"/>
							</xsl:element>
							<xsl:element name="AccountCurrencyCode">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountCurrency1"/>
							</xsl:element>
							<xsl:element name="AccountHolderName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountHolder1"/>
							</xsl:element>
						</xsl:element>
						<!-- 기타정보 : 추심(매입)신청자의 계좌정보를 입력-->
						<xsl:if test="/APPSPC2BK/APPSPC_M/CollectionBankAccount2 !=''">
							<xsl:element name="AdditionalAccount">
								<xsl:element name="AccountIdentifier">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccount2"/>
								</xsl:element>
								<xsl:element name="AccountCurrencyCode">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountCurrency2"/>
								</xsl:element>
								<xsl:element name="AccountHolderName">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountHolder2"/>
								</xsl:element>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/APPSPC2BK/APPSPC_M/CollectionBankAccoun3 !=''">
							<xsl:element name="AdditionalAccount">
								<xsl:element name="AccountIdentifier">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccount3"/>
								</xsl:element>
								<xsl:element name="AccountCurrencyCode">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountCurrency3"/>
								</xsl:element>
								<xsl:element name="AccountHolderName">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/CollectionBankAccountHolder3"/>
								</xsl:element>
							</xsl:element>
						</xsl:if>
					</xsl:element>
					<!-- 추심(매입) 신청일자 -->
					<xsl:element name="ApplicationDate">
						<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicationDate"/>
					</xsl:element>
					<!-- 추심(매입) 신청번호 -->
					<xsl:element name="ApplicationReference">
						<xsl:element name="ApplicationReferenceCode">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicationCode"/>
						</xsl:element>
						<xsl:element name="ApplicationReferenceIdentifier">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicationIdentifier"/>
						</xsl:element>
					</xsl:element>
					<!-- 추심(매입) 신청인 -->
					<xsl:element name="ApplicantParty">
						<xsl:element name="PartyIdentifier">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantPartyId"/>
						</xsl:element>
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantCEOName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Address">
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantAddressLine1"/>
							</xsl:element>
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantAddressLine2"/>
							</xsl:element>
							<xsl:if test="/APPSPC2BK/APPSPC_M/ApplicantAddressLine3 !=''">
								<xsl:element name="AddressLine2Text">
									<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantAddressLine3"/>
								</xsl:element>
							</xsl:if>
						</xsl:element>
						<xsl:element name="PartySignatureValueText">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ApplicantSignature"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="BuyerParty">
						<xsl:element name="PartyIdentifier">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyId"/>
						</xsl:element>
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyOrgName1"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyOrgName2"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyOrgName3"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Contact">
							<xsl:element name="ContactEmailAccountText">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyContactEmailAccount"/>
							</xsl:element>
							<xsl:element name="ContactEmailDomainText">
								<xsl:value-of select="/APPSPC2BK/APPSPC_M/BuyerPartyContactEmailDomain"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 추심(매입) 금액-->
					<xsl:element name="CollectionAmount">
						<xsl:element name="AmountBasisAmount">
							<xsl:attribute name="currency"><xsl:value-of select="/APPSPC2BK/APPSPC_M/AmountCurrency"/></xsl:attribute>
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/Amount"/>
						</xsl:element>
						<xsl:element name="AmountConvertedAmount">
							<xsl:attribute name="currency">KRW</xsl:attribute>
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ConvertedAmount"/>
						</xsl:element>
					</xsl:element>
					<!-- 원화환산 추심금액 산출에 적용된 적용환율 -->
					<xsl:element name="CollectionExchangeRate">
						<xsl:element name="ExchangeRateCalculationRateNumeric">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ExchangeRate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="RequiredDocuments">
						<!-- 물품수령증명서 첨부통수 -->
						<xsl:element name="ReceiptTestimonyCopyNumber">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/ReceiptNumber"/>
						</xsl:element>
						<!-- 세금계산서 첨부통수 -->
						<xsl:element name="TaxInvoiceCopyNumber">
							<xsl:value-of select="/APPSPC2BK/APPSPC_M/TaxInvoiceNumber"/>
						</xsl:element>
					</xsl:element>
					<!-- 추심(매입)관련 서류 시작 -->
					<xsl:for-each select="/APPSPC2BK/APPSPC_M/APPSPC_DOC">
						<xsl:element name="RequiredDocumentDetails">
							<!-- 문서명 코드 -->
							<xsl:element name="RequiredDocumentCode">
								<xsl:value-of select="./DocumentCode"/>
							</xsl:element>
							<!-- 해당서류의 전자문서번호 -->
							<xsl:element name="RequiredDocumentIdentifier">
								<xsl:value-of select="./DocumentIdentifier"/>
							</xsl:element>
							<!-- 내국신용장 및 물품수령증명서 상의 내국신용장번호 -->
							<xsl:element name="LocalLetterOfCreditIdentifier">
								<xsl:value-of select="./LocalLCIdentifier"/>
							</xsl:element>
							<!-- 물품수령증명서 발급번호 -->
							<xsl:element name="IssueIdentifier">
								<xsl:value-of select="./IssueIdentifier"/>
							</xsl:element>
							<!-- 물품수령증명서 대표공급물품 HS 코드 -->
							<xsl:element name="ClassIdHSIdentifier">
								<xsl:value-of select="./HSCode"/>
							</xsl:element>
							<!-- 세금계산서 권번호 -->
							<xsl:element name="TaxInvoiceVolumeIdentifier">
								<xsl:value-of select="./TaxInvoiceVolume"/>
							</xsl:element>
							<!-- 세금계산서 호번호 -->
							<xsl:element name="TaxInvoiceIssueIdentifier">
								<xsl:value-of select="./TaxInvoiceIssue"/>
							</xsl:element>
							<!-- 세금계산서 일련번호 -->
							<xsl:element name="TaxInvoiceSequenceIdentifier">
								<xsl:value-of select="./TaxInvoiceSequence"/>
							</xsl:element>
							<!-- 세금계산서 관리번호 -->
							<xsl:element name="TaxInvoiceManagementIdentifier">
								<xsl:value-of select="./TaxInvoiceManageIdentifier"/>
							</xsl:element>
							<!-- 세금계산서 참조번호 -->
							<xsl:element name="TaxInvoiceIdentifier">
								<xsl:value-of select="./TaxInvoiceIdentifier"/>
							</xsl:element>
							<!-- 내국신용장 개설일자, 물품수령증명서 발급일자, 세금계산서 작성일 -->
							<xsl:element name="LocalLetterOfCreditIssueDate">
								<xsl:value-of select="./LocalLCIssueDate"/>
							</xsl:element>
							<xsl:element name="IssueDate">
								<xsl:value-of select="./IssueDate"/>
							</xsl:element>
							<xsl:element name="TaxInvoiceIssueDate">
								<xsl:value-of select="./TaxInvoiceIssueDate"/>
							</xsl:element>
							<!-- 물품인도기일 -->
							<xsl:element name="DeliveryPromisedDateTime">
								<xsl:value-of select="./DeliveryDate"/>
							</xsl:element>
							<!-- 물품수령증명서 인수일자 -->
							<xsl:element name="AcceptanceDate">
								<xsl:value-of select="./AcceptanceDate"/>
							</xsl:element>
							<!-- 내국신용장 유효기일 -->
							<xsl:element name="LocalLetterOfCreditEffectiveDate">
								<xsl:value-of select="./LocalLCEffectiveDate"/>
							</xsl:element>
							<!-- 물품수령증명서 유효기일 -->
							<xsl:element name="EffectiveDate">
								<xsl:value-of select="./EffectiveDate"/>
							</xsl:element>
							<!-- 물품공급자 -->
							<xsl:element name="SupplierParty">
								<xsl:element name="PartyIdentifier">
									<xsl:value-of select="./SupplierIdentifier"/>
								</xsl:element>
								<xsl:element name="PartyId">
									<xsl:element name="PartyIdTypeCode">
										<xsl:value-of select="./SupplierType"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Organization">
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./SupplierName"/>
									</xsl:element>
									<xsl:element name="OrganizationCEOName">
										<xsl:value-of select="./SupplierCEOName"/>
									</xsl:element>
									<xsl:element name="OrganizationBusinessTypeName">
										<xsl:value-of select="./SupplierBusinessType"/>
									</xsl:element>
									<xsl:element name="OrganizationBusinessClassificationName">
										<xsl:value-of select="./SupplierBusinessClass"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Address">
									<xsl:element name="AddressLine1Text">
										<xsl:value-of select="./SupplierAddressLine1"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./SupplierAddressLine2"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./SupplierAddressLine3"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="PartySignatureValueText">
									<xsl:value-of select="./SupplieSignatureValue"/>
								</xsl:element>
							</xsl:element>
							<!-- 공급받는자 -->
							<xsl:element name="BuyerParty">
								<xsl:element name="PartyIdentifier">
									<xsl:value-of select="./BuyerIdentifier"/>
								</xsl:element>
								<xsl:element name="Organization">
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./BuyerName"/>
									</xsl:element>
									<xsl:element name="OrganizationCEOName">
										<xsl:value-of select="./BuyerCEOName"/>
									</xsl:element>
									<xsl:element name="OrganizationBusinessTypeName">
										<xsl:value-of select="./BuyerBusinessType"/>
									</xsl:element>
									<xsl:element name="OrganizationBusinessClassificationName">
										<xsl:value-of select="./BuyerBusinessClass"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Address">
									<xsl:element name="AddressLine1Text">
										<xsl:value-of select="./BuyerAddressLine1"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./BuyerAddressLine2"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./BuyerAddressLine3"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="PartySignatureValueText">
									<xsl:value-of select="./BuyerSignature"/>
								</xsl:element>
							</xsl:element>
							<!-- 물품수령인 -->
							<xsl:element name="AcceptanceParty">
								<xsl:element name="Organization">
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./AcceptanceName"/>
									</xsl:element>
									<xsl:element name="OrganizationCEOName">
										<xsl:value-of select="./AcceptanceCEOName"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Address">
									<xsl:element name="AddressLine1Text">
										<xsl:value-of select="./AcceptanceAddressLine1"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./AcceptanceAddressLine2"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./AcceptanceAddressLine3"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="PartySignatureValueText">
									<xsl:value-of select="./AcceptanceSignature"/>
								</xsl:element>
							</xsl:element>
							<!-- 수탁자 -->
							<xsl:element name="AgentParty">
								<xsl:element name="PartyIdentifier">
									<xsl:value-of select="./AgentIdentifier"/>
								</xsl:element>
								<xsl:element name="Organization">
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./AgentName"/>
									</xsl:element>
									<xsl:element name="OrganizationCEOName">
										<xsl:value-of select="./AgentCEOName"/>
									</xsl:element>
									<xsl:element name="OrganizationBusinessTypeName"/>
									<xsl:element name="OrganizationBusinessClassificationName"/>
								</xsl:element>
								<xsl:element name="Address">
									<xsl:element name="AddressLine1Text">
										<xsl:value-of select="./AgentAddressLine1"/>
									</xsl:element>
									<xsl:element name="AddressLine2Text">
										<xsl:value-of select="./AgentAddressLine2"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="PartySignatureValueText">
									<xsl:value-of select="./AgenSignature"/>
								</xsl:element>
							</xsl:element>
							<!-- 개설은행 -->
							<xsl:element name="IssuingBank">
								<xsl:element name="Organization">
									<xsl:element name="OrganizationIdentifier">
										<xsl:value-of select="./IssuingBankCode"/>
									</xsl:element>
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./IssuingBankName"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Branch">
									<xsl:element name="OrganizationName">
										<xsl:value-of select="./IssuingBankBranch"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:element name="IssuingBankSignatureParty">
								<xsl:element name="PartySignatureValueText">
									<xsl:value-of select="./IssuingBankSignature"/>
								</xsl:element>
							</xsl:element>
							<!-- 내국신용장 종류 -->
							<xsl:element name="LocalLetterOfCreditTypeCode">
								<xsl:value-of select="./LocalLCType"/>
							</xsl:element>
							<xsl:element name="LocalLetterOfCreditOpenAmount">
								<!-- 물품수령증명서 상의 내국신용장 개설금액(외화) -->
								<xsl:element name="AmountBasisAmount">
									<xsl:attribute name="currency"><xsl:value-of select="./LocalLCAmountCurrency"/></xsl:attribute>
									<xsl:value-of select="./LocalLCAmount"/>
								</xsl:element>
								<!-- 물품수령증명서 상의 내국신용장 개설금액 -->
								<xsl:element name="AmountConvertedAmount">
									<xsl:attribute name="currency">KRW</xsl:attribute>
									<xsl:value-of select="./LocalLCConvertedAmount"/>
								</xsl:element>
								<xsl:element name="ExchangeRate">
									<xsl:element name="ExchangeRateCalculationRateNumeric">
										<xsl:value-of select="./LocalLCExchangeRate"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:element name="AcceptanceAmount">
								<!-- 물품수령증명서 인수금액(외화) -->
								<xsl:element name="AmountBasisAmount">
									<xsl:attribute name="currency"><xsl:value-of select="./AcceptanceAmountCurrency"/></xsl:attribute>
									<xsl:value-of select="./AcceptanceAmount"/>
								</xsl:element>
								<!-- 물품수령증명서 인수금액 -->
								<xsl:element name="AmountConvertedAmount">
									<xsl:attribute name="currency">KRW</xsl:attribute>
									<xsl:value-of select="./AcceptanceConvertedAmount"/>
								</xsl:element>
							</xsl:element>
							<!-- 세금계산서 세액(원화) -->
							<xsl:element name="TaxAmount">
								<xsl:value-of select="./TaxAmount"/>
							</xsl:element>
							<!-- 세금계산서 공급가액을 -->
							<xsl:element name="ChargeAmount">
								<xsl:value-of select="./ChargeAmount"/>
							</xsl:element>
							<!-- 총금액 -->
							<xsl:element name="TotalAmount">
								<xsl:attribute name="currency"><xsl:value-of select="./TotalAmountCurrency"/></xsl:attribute>
								<xsl:value-of select="./TotalAmount"/>
							</xsl:element>
							<xsl:element name="TotalQuantity">
								<xsl:attribute name="unitCode"><xsl:value-of select="./TotalQuantityUnitCode"/></xsl:attribute>
								<xsl:value-of select="./TotalQuantity"/>
							</xsl:element>
							<!-- 결제방법 -->
							<xsl:if test="concat(./CashPayment,./CashForeignPayment) !=''">
								<xsl:element name="PaymentMeansDetails">
									<xsl:element name="PaymentMeansTypeCode">10</xsl:element>
									<xsl:element name="ForeignPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency"><xsl:value-of select="./CashForeignPaymentCurrency"/></xsl:attribute>
											<xsl:value-of select="./CashForeignPayment"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="KoreanPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency">KRW</xsl:attribute>
											<xsl:value-of select="./CashPayment"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:if>
							<xsl:if test="concat(./CheckPayment,./CheckForeignPayment) !=''">
								<xsl:element name="PaymentMeansDetails">
									<xsl:element name="PaymentMeansTypeCode">20</xsl:element>
									<xsl:element name="ForeignPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency"><xsl:value-of select="./CheckForeignPaymentCurrency"/></xsl:attribute>
											<xsl:value-of select="./CheckForeignPayment"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="KoreanPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency">KRW</xsl:attribute>
											<xsl:value-of select="./CheckPayment"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:if>
							<xsl:if test="concat(./BillPayment,./BillForeignPayment) !=''">
								<xsl:element name="PaymentMeansDetails">
									<xsl:element name="PaymentMeansTypeCode">2AA</xsl:element>
									<xsl:element name="ForeignPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency"><xsl:value-of select="./BillForeignPaymentCurrency"/></xsl:attribute>
											<xsl:value-of select="./BillForeignPayment"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="KoreanPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency">KRW</xsl:attribute>
											<xsl:value-of select="./BillPayment"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:if>
							<xsl:if test="concat(./CreditPayment,./CreditForeignPayment) !=''">
								<xsl:element name="PaymentMeansDetails">
									<xsl:element name="PaymentMeansTypeCode">30</xsl:element>
									<xsl:element name="ForeignPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency"><xsl:value-of select="./CreditForeignPaymentCurrency"/></xsl:attribute>
											<xsl:value-of select="./CreditForeignPayment"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="KoreanPaymentDetails">
										<xsl:element name="PaymentPaidAmount">
											<xsl:attribute name="currency">KRW</xsl:attribute>
											<xsl:value-of select="./CreditPayment"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:if>
							<!-- 영수, 청구의 구분 -->
							<xsl:element name="TaxInvoiceProcessingIndicator">
								<xsl:value-of select="./ProcessingIndicator"/>
							</xsl:element>
							<!-- 세금계산서 공란수 -->
							<xsl:element name="BlankNumber">
								<xsl:value-of select="./BlankNumber"/>
							</xsl:element>
							<!-- 물품수령서상의 내국신용장 참조 사항 및 세금계산서상의 비고 -->
							<xsl:if test="./AdditionalInformation1 !=''">
								<xsl:element name="AdditionalInformationDescriptionText">
									<xsl:value-of select="./AdditionalInformation1"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalInformation2 !=''">
								<xsl:element name="AdditionalInformationDescriptionText">
									<xsl:value-of select="./AdditionalInformation2"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalInformation3 !=''">
								<xsl:element name="AdditionalInformationDescriptionText">
									<xsl:value-of select="./AdditionalInformation3"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalInformation4 !=''">
								<xsl:element name="AdditionalInformationDescriptionText">
									<xsl:value-of select="./AdditionalInformation4"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalInformation5 !=''">
								<xsl:element name="AdditionalInformationDescriptionText">
									<xsl:value-of select="./AdditionalInformation5"/>
								</xsl:element>
							</xsl:if>
							<!-- 기타조건 -->
							<xsl:if test="./AdditionalCondition1 !=''">
								<xsl:element name="AdditionalConditionDescriptionText"><xsl:value-of select="./AdditionalCondition1"/></xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalCondition2 !=''">
								<xsl:element name="AdditionalConditionDescriptionText"><xsl:value-of select="./AdditionalCondition2"/></xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalCondition3 !=''">
								<xsl:element name="AdditionalConditionDescriptionText"><xsl:value-of select="./AdditionalCondition3"/></xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalCondition4 !=''">
								<xsl:element name="AdditionalConditionDescriptionText"><xsl:value-of select="./AdditionalCondition4"/></xsl:element>
							</xsl:if>
							<xsl:if test="./AdditionalCondition5 !=''">
								<xsl:element name="AdditionalConditionDescriptionText"><xsl:value-of select="./AdditionalCondition5"/></xsl:element>
							</xsl:if>
							<!-- 라인아이템 시작 -->
							<xsl:for-each select="./APPSPC_DOC_ITEM">
							<xsl:element name="LineDetails">
							<!-- 품목번호/일련번호 -->
								<xsl:element name="LineNumber">
									<xsl:value-of select="./LineNumber"/>
								</xsl:element>
								<xsl:element name="LineItem">
									<xsl:element name="Item">
									<!-- HS 부호 -->
										<xsl:element name="ClassId">
											<xsl:element name="ClassIdHSIdentifier">
												<xsl:value-of select="./HSCode"/>
											</xsl:element>
										</xsl:element>
										<!-- 품목 -->
										<xsl:if test="./ItemName1 !=''">
										<xsl:element name="ItemName"><xsl:value-of select="./ItemName1"/></xsl:element>
										</xsl:if>
										<xsl:if test="./ItemName2 !=''">
										<xsl:element name="ItemName"><xsl:value-of select="./ItemName2"/></xsl:element>
										</xsl:if>
										<xsl:if test="./ItemName3 !=''">
										<xsl:element name="ItemName"><xsl:value-of select="./ItemName3"/></xsl:element>
										</xsl:if>
										<xsl:if test="./ItemName4 !=''">
										<xsl:element name="ItemName"><xsl:value-of select="./ItemName4"/></xsl:element>
										</xsl:if>
										<!-- 규격 -->
										<xsl:if test="./Definition1 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition1"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition2 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition2"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition3 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition3"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition4 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition4"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition5 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition5"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition6 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition6"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition7 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition7"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition8 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition8"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition9 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition9"/></xsl:element>
										</xsl:if>
										<xsl:if test="./Definition10 !=''">
										<xsl:element name="ItemDefinitionText"><xsl:value-of select="./Definition10"/></xsl:element>
										</xsl:if>
										<!-- 라인별참조사항 -->		
										<xsl:element name="Note">
											<xsl:if test="./Description1 !=''">
											<xsl:element name="NoteDescriptionText"><xsl:value-of select="./Description1"/></xsl:element>
											</xsl:if>
											<xsl:if test="./Description2 !=''">
											<xsl:element name="NoteDescriptionText"><xsl:value-of select="./Description2"/></xsl:element>
											</xsl:if>
											<xsl:if test="./Description3 !=''">
											<xsl:element name="NoteDescriptionText"><xsl:value-of select="./Description3"/></xsl:element>
											</xsl:if>
											<xsl:if test="./Description4 !=''">
											<xsl:element name="NoteDescriptionText"><xsl:value-of select="./Description4"/></xsl:element>
											</xsl:if>
											<xsl:if test="./Description5 !=''">
											<xsl:element name="NoteDescriptionText"><xsl:value-of select="./Description5"/></xsl:element>
											</xsl:if>
										</xsl:element>
									</xsl:element>
									<!-- 라인별공급일자 -->
									<xsl:element name="ItemActualDeliveryDate">
										<xsl:value-of select="./DeliveryDate"/>
									</xsl:element>
									<!-- 수량 -->
									<xsl:element name="LineItemQuantity">
										<xsl:attribute name="unitCode"><xsl:value-of select="./QuantityUnitCode"/></xsl:attribute>
										<xsl:value-of select="./Quantity"/>
									</xsl:element>
									<!-- 수량소계 -->
									<xsl:element name="LineItemSubTotalQuantity">
										<xsl:attribute name="unitCode"><xsl:value-of select="./SubTotalQuantityUnitCode"/></xsl:attribute>
										<xsl:value-of select="./SubTotalQuantity"/>
									</xsl:element>
									<!-- 단가 -->
									<xsl:element name="BasePrice">
										<xsl:element name="BasePriceAmount">
											<!--<xsl:attribute name="currency"><xsl:value-of select="./SubTotalQuantityUnitCode"/></xsl:attribute>-->
											<xsl:value-of select="./BasePrice"/>
										</xsl:element>
										<xsl:element name="BaseQuantity">
											<xsl:attribute name="unitCode"><xsl:value-of select="./BaseQuantityUnitCode"/></xsl:attribute>
											<xsl:value-of select="./BaseQuantity"/>
										</xsl:element>
									</xsl:element>
									<!-- 인수물품금액 -->
									<xsl:element name="LineItemAmount">
										<xsl:attribute name="currency"><xsl:value-of select="./AmountCurrency"/></xsl:attribute>
										<xsl:value-of select="./Amount"/>
									</xsl:element>
									<!-- 인수물품금액 소계 -->
									<xsl:element name="LineItemSubTotalAmount">
										<xsl:attribute name="currency"><xsl:value-of select="./SubTotalAmountCurrency"/></xsl:attribute>
										<xsl:value-of select="./SubTotalAmount"/>
									</xsl:element>
									<!-- 공급가액 -->
									<xsl:element name="ChargeAmount">
										<xsl:attribute name="currency">KRW</xsl:attribute>
										<xsl:value-of select="./ChargeAmount"/>
									</xsl:element>
									<!-- 세액 -->
									<xsl:element name="TaxAmount">
										<xsl:attribute name="currency">KRW</xsl:attribute>
										<xsl:value-of select="./TaxAmount"/>
									</xsl:element>
									<!-- 외화공급가액 -->
									<xsl:element name="ForeignChargeAmount">
										<xsl:attribute name="currency"><xsl:value-of select="./ForeignAmountCurrency"/></xsl:attribute>
										<xsl:value-of select="./ForeignAmount"/>
									</xsl:element>
									<!-- 공급가액 소계 -->
									<xsl:element name="SubTotalChargeAmount">
										<xsl:attribute name="currency">KRW</xsl:attribute>
										<xsl:value-of select="./SubTotalCharge"/>
									</xsl:element>
									<!-- 세액 소계 -->
									<xsl:element name="SubTotalTaxAmount">
										<xsl:attribute name="currency">KRW</xsl:attribute>
										<xsl:value-of select="./SubTotalTax"/>
									</xsl:element>
									<!-- 외화 공급가액소계 -->
									<xsl:element name="SubTotalForeignChargeAmount">
										<xsl:attribute name="currency"><xsl:value-of select="./SubTotalForeignCurrency"/></xsl:attribute>
										<xsl:value-of select="./SubTotalForeign"/>
									</xsl:element>
									<!-- 공급가액 환율 -->
									<xsl:element name="ExchangeRate">
										<xsl:element name="ExchangeRateCalculationRateNumeric">
											<xsl:value-of select="./ExchangeRate"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							</xsl:for-each>
							<!-- 라인아이템 끝 -->
						</xsl:element>
					</xsl:for-each>
					<!-- 추심(매입)관련 서류 끝 -->
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
