<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:include href="template.xsl"/>
	<xsl:template match="/">
		<xsl:element name="PCRLIC2CJ">
			<xsl:element name="DocumentHeader">
				<xsl:element name="UniqueReferenceNo">
					<xsl:value-of select="/PCRLIC/ApplicationArea/ApplicationAreaMessageIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentNo">
					<xsl:value-of select="/PCRLIC/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderID">
					<xsl:value-of select="/PCRLIC/ApplicationArea/ApplicationAreaSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="RecipientID">
					<xsl:value-of select="/PCRLIC/ApplicationArea/ApplicationAreaReceiverIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentType">
					<xsl:value-of select="/PCRLIC/ApplicationArea/ApplicationAreaMessageTypeIndicator"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="PCRLIC_M">
				<xsl:variable name="sMAINKEY" select="js:getKey(position())"/>
				<xsl:element name="MAINKEY">
					<xsl:value-of select="$sMAINKEY"/>
				</xsl:element>
				<!-- 전자문서의 코드 -->
				<xsl:element name="DocumentCode">
					<xsl:value-of select="/PCRLIC/DataArea/Header/DocumentCode"/>
				</xsl:element>
				<!-- 변경전구매확인서번호 -->
				<xsl:element name="BeforeIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Header/BeforeModifyPurchaseOrderIdentifier"/>
				</xsl:element>
				<!-- 문서번호 -->
				<xsl:element name="DocumentIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<!-- 문서기능 -->
				<xsl:element name="FunctionCode">
					<xsl:value-of select="/PCRLIC/DataArea/Header/DocumentFunctionCode"/>
				</xsl:element>
				<!-- 응답유형 -->
				<xsl:element name="ResponseTypeCode">
					<xsl:value-of select="/PCRLIC/DataArea/Header/DocumentResponseTypeCode"/>
				</xsl:element>
				<!-- 신청인 -->
				<xsl:element name="ApplicantName">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="ApplicantCEOName">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<xsl:element name="ApplicantIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="ApplicantAddressLine1">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="ApplicantAddressLine2">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="ApplicantAddressLine3">
					<xsl:value-of select="/PCRLIC/DataArea/Header/ApplicantParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<!-- 공급자 -->
				<xsl:element name="SupplierName">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="SupplierCEOName">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<xsl:element name="SupplierIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="SupplierAddressLine1">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="SupplierAddressLine2">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="SupplierAddressLine3">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<xsl:element name="ContactEmailAccount">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Contact/ContactEmailAccountText"/>
				</xsl:element>
				<xsl:element name="ContactEmailDomain">
					<xsl:value-of select="/PCRLIC/DataArea/Header/SupplierParty/Contact/ContactEmailDomainText"/>
				</xsl:element>
				<!-- 구매(공급)원료/물품등의 용도명세 -->
				<xsl:element name="UsageTypeCode">
					<xsl:value-of select="/PCRLIC/DataArea/Header/MaterialUsageTypeCode"/>
				</xsl:element>
				<xsl:element name="UsageDescription">
					<xsl:value-of select="/PCRLIC/DataArea/Header/MaterialUsageDescriptionText"/>
				</xsl:element>
                <!-- 사전/사후 발급 구분 코드 
				<xsl:element name="IssueClassificationCode">
					<xsl:value-of select="/PCRLIC/DataArea/Header/IssueClassificationCode"/>
				</xsl:element> -->
				<!-- 확인일자 -->
				<xsl:element name="ConfirmationDate">
					<xsl:value-of select="/PCRLIC/DataArea/Header/PurchaseConfirmationDate"/>
				</xsl:element>
				<!-- 총수량 -->
				<xsl:element name="TotalQuantity">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/TotalQuantity"/>
				</xsl:element>
				<xsl:element name="TotalQuantityUunitCode">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/TotalQuantity/@unitCode"/>
				</xsl:element>
				<!-- 총금액 -->
				<xsl:element name="TotalAmount">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/TotalAmount"/>
				</xsl:element>
				<xsl:element name="TotalAmountCurrency">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/TotalAmount/@currency"/>
				</xsl:element>
				<!-- 총금액(USD금액 부기) -->
				<xsl:element name="TotalForeignAmount">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/TotalForeignAmount"/>
				</xsl:element>
				<!-- 총 할인/할증/변동 내역 -->
				<xsl:element name="AllowanceIndicator">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/AllowanceOrCharge/AllowanceOrChargeIndicator"/>
				</xsl:element>
				<!-- 총 할인/할증/변동 금액 -->
				<xsl:element name="AllowanceAmount">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/AllowanceOrCharge/AllowanceOrChargeAmount[1]"/>
				</xsl:element>
				<xsl:element name="AllowanceAmountCurrency">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/AllowanceOrCharge/AllowanceOrChargeAmount[1]/@currency"/>
				</xsl:element>
				<!-- 총 할인/할증/변동 금액(USD 금액 부기) -->
				<xsl:element name="AllowanceForeignAmount">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/AllowanceOrCharge/AllowanceOrChargeAmount[2]"/>
				</xsl:element>
				<!-- 구매확인서번호 -->
				<xsl:element name="ConfirmIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/PurchaseConfirmationIssueIdentifier"/>
				</xsl:element>
				<!-- 업체신청번호 -->
				<xsl:element name="ApplicationIdentifier">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/OrganizationApplicationIdentifier"/>
				</xsl:element>
				<!-- 확인기관 -->
				<xsl:element name="ConfirmBankCode">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/ConfirmParty/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="ConfirmBankName">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/ConfirmParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="ConfirmBankBranch">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/ConfirmParty/Branch/OrganizationName"/>
				</xsl:element>
				<!-- 확인서 기 발급은행 
				<xsl:element name="IssuingBankCode">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/IssuingBank/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="IssuingBankName">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/IssuingBank/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="IssuingBankBranch">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/IssuingBank/Branch/OrganizationName"/>
				</xsl:element> -->
				<!-- 확인은행 전자서명
				<xsl:element name="PurchaseConfirmIssueID">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/RepresentativePurchaseConfirmationIssueIdentifier"/>
				</xsl:element>  -->
				<xsl:element name="BankSignaturName">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/SignatureParty/Organization/OrganizationName"/>
				</xsl:element>								
				<xsl:element name="BankSignaturCEOName">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/SignatureParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<xsl:element name="BankSignaturValue">
					<xsl:value-of select="/PCRLIC/DataArea/Summary/SignatureParty/PartySignatureValueText"/>
				</xsl:element>
				<!-- 라인아이템 시작 -->
				<xsl:for-each select="/PCRLIC/DataArea/Line">
					<xsl:element name="PCRLIC_ITEM">
						<xsl:variable name="sITEMKEY" select="js:getKey(position())"/>
						<xsl:element name="ITEMKEY">
							<xsl:value-of select="$sITEMKEY"/>
						</xsl:element>
						<xsl:element name="MAINKEY">
							<xsl:value-of select="$sMAINKEY"/>
						</xsl:element>
						<!-- 일련번호 -->
						<xsl:element name="LineNumber">
							<xsl:value-of select="./LineNumber"/>
						</xsl:element>
						<!-- 품목 코드 
						<xsl:element name="ItemCode">
							<xsl:value-of select="./LineItem/Item/ItemCode"/>
						</xsl:element> -->
						<!-- HS 부호 -->
						<xsl:element name="HSCode">
							<xsl:value-of select="./LineItem/Item/ClassId/ClassIdHSIdentifier"/>
						</xsl:element>
						<!-- 품목 -->
						<xsl:element name="ItemName1">
							<xsl:value-of select="./LineItem/Item/ItemName[1]"/>
						</xsl:element>
						<xsl:element name="ItemName2">
							<xsl:value-of select="./LineItem/Item/ItemName[2]"/>
						</xsl:element>
						<xsl:element name="ItemName3">
							<xsl:value-of select="./LineItem/Item/ItemName[3]"/>
						</xsl:element>
						<xsl:element name="ItemName4">
							<xsl:value-of select="./LineItem/Item/ItemName[4]"/>
						</xsl:element>
						<xsl:element name="ItemName5">
							<xsl:value-of select="./LineItem/Item/ItemName[5]"/>
						</xsl:element>
						<!-- 수량 -->
						<xsl:element name="Quantity">
							<xsl:value-of select="./LineItem/LineItemQuantity"/>
						</xsl:element>
						<xsl:element name="QuantityUunitCode">
							<xsl:value-of select="./LineItem/LineItemQuantity/@unitCode"/>
						</xsl:element>
						<!-- 비 고 -->
						<xsl:element name="Description1">
							<xsl:value-of select="./LineItem/Item/Note/NoteDescriptionText[1]"/>
						</xsl:element>
						<xsl:element name="Description2">
							<xsl:value-of select="./LineItem/Item/Note/NoteDescriptionText[2]"/>
						</xsl:element>
						<xsl:element name="Description3">
							<xsl:value-of select="./LineItem/Item/Note/NoteDescriptionText[3]"/>
						</xsl:element>
						<xsl:element name="Description4">
							<xsl:value-of select="./LineItem/Item/Note/NoteDescriptionText[4]"/>
						</xsl:element>
						<xsl:element name="Description5">
							<xsl:value-of select="./LineItem/Item/Note/NoteDescriptionText[5]"/>
						</xsl:element>
						<!-- 금액 -->
						<xsl:element name="Amount">
							<xsl:value-of select="./LineItem/LineItemAmount"/>
						</xsl:element>
						<xsl:element name="AmountCurrency">
							<xsl:value-of select="./LineItem/LineItemAmount/@currency"/>
						</xsl:element>
						<!-- 금액(USD 금액 부기) -->
						<xsl:element name="ForeignAmount">
							<xsl:value-of select="./LineItem/LineItemForeignAmount"/>
						</xsl:element>
						<!-- 단가(USD금액 부기) -->
						<xsl:element name="ForeignBaseAmount">
							<xsl:value-of select="./LineItem/LineItemForeignBasePriceAmount"/>
						</xsl:element>
						<!-- 단가 -->
						<xsl:element name="BasePriceAmount">
							<xsl:value-of select="./LineItem/BasePrice/BasePriceAmount"/>
						</xsl:element>
						<xsl:element name="BasePriceQuantity">
							<xsl:value-of select="./LineItem/BasePrice/BasePriceBaseQuantity"/>
						</xsl:element>
						<xsl:element name="BasePriceQuantityUnitCod">
							<xsl:value-of select="./LineItem/BasePrice/BasePriceBaseQuantity/@unitCode"/>
						</xsl:element>
						<!-- 구매(공급)일 -->
						<xsl:element name="PurchaseDate">
							<xsl:value-of select="./LineItem/LineItemPurchaseDate"/>
						</xsl:element>
						<!-- 품목별 할인/할증/변동 내역 -->
						<xsl:element name="AllowanceIndicator">
							<xsl:value-of select="./LineItem/AllowanceOrCharge/AllowanceOrChargeIndicator"/>
						</xsl:element>
						<xsl:element name="AllowanceAmount">
							<xsl:value-of select="./LineItem/AllowanceOrCharge/AllowanceOrChargeAmount[1]"/>
						</xsl:element>
						<xsl:element name="AllowanceAmountCurrency">
							<xsl:value-of select="./LineItem/AllowanceOrCharge/AllowanceOrChargeAmount[1]/@currency"/>
						</xsl:element>
						<xsl:element name="AllowanceForeignAmount">
							<xsl:value-of select="./LineItem/AllowanceOrCharge/AllowanceOrChargeAmount[2]"/>
						</xsl:element>
						<!-- 규격 시작 -->
						<xsl:variable name="sITEMFTXAAA">
							<xsl:for-each select="./LineItem/Item/ItemDefinitionText">
								<xsl:if test="position() = 1">
									<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="position() >1">
									<xsl:value-of select="concat('&#10;',.)"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:call-template name="template_ftx">
							<xsl:with-param name="parent_tag_name">PCRLIC_ITEM_S</xsl:with-param>
							<xsl:with-param name="pk_tag_name">ITEMSUBKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_name1">ITEMKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_name2">MAINKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_value1" select="$sITEMKEY"/>
							<xsl:with-param name="fk_tag_value2" select="$sMAINKEY"/>
							<xsl:with-param name="qul_tag_value">AAA</xsl:with-param>
							<xsl:with-param name="tag_name">Text</xsl:with-param>
							<xsl:with-param name="value" select="$sITEMFTXAAA"/>
							<xsl:with-param name="delimiter" select="'&#10;'"/>
							<xsl:with-param name="sequence">1</xsl:with-param>
							<xsl:with-param name="sequence_tag_name">Sort</xsl:with-param>
							<xsl:with-param name="sequence_tag_value">1</xsl:with-param>
							<xsl:with-param name="rows">5</xsl:with-param>
						</xsl:call-template>
						<!-- 규격 끝 -->
					</xsl:element>
				</xsl:for-each>
				<!-- 라인아이템 끝 -->
				<!-- 세금계산서 시작 -->
				<xsl:for-each select="/PCRLIC/DataArea/Summary/TaxInvoiceInformation">
					<xsl:element name="PCRLIC_TAX">
						<xsl:variable name="sTAXKEY" select="js:getKey(position())"/>
						<xsl:element name="TAXKEY">
							<xsl:value-of select="$sTAXKEY"/>
						</xsl:element>
						<xsl:element name="MAINKEY">
							<xsl:value-of select="$sMAINKEY"/>
						</xsl:element>
						<xsl:element name="TaxInvoiceId">
							<xsl:value-of select="./TaxInvoiceIdentifier"/>
						</xsl:element>
						<xsl:element name="TaxInvoiceIssueDate">
							<xsl:value-of select="./TaxInvoiceIssueDate"/>
						</xsl:element>
						<xsl:element name="ChargeAmount">
							<xsl:value-of select="./ChargeAmount"/>
						</xsl:element>
						<xsl:element name="TaxAmount">
							<xsl:value-of select="./TaxAmount"/>
						</xsl:element>
						<xsl:element name="Tax_ItemName1">
							<xsl:value-of select="./ItemName[1]"/>
						</xsl:element>
						<xsl:element name="Tax_ItemName2">
							<xsl:value-of select="./ItemName[2]"/>
						</xsl:element>
						<xsl:element name="Tax_ItemName3">
							<xsl:value-of select="./ItemName[3]"/>
						</xsl:element>
						<xsl:element name="Tax_ItemName4">
							<xsl:value-of select="./ItemName[4]"/>
						</xsl:element>
						<xsl:element name="Tax_ItemName5">
							<xsl:value-of select="./ItemName[5]"/>
						</xsl:element>
						<xsl:element name="Tax_ItemQuantity">
							<xsl:value-of select="./ItemQuantity"/>
						</xsl:element>
						<xsl:element name="Tax_ItemQuantityunit">
							<xsl:value-of select="./ItemQuantity/@unitCode"/>
						</xsl:element>
						<xsl:element name="Sort">
							<xsl:value-of select="position()"/>
						</xsl:element>
						<!-- 규격 시작 -->
						<xsl:variable name="sTAXFTXAAA">
							<xsl:for-each select="./ItemDefinitionText">
								<xsl:if test="position() = 1">
									<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="position() >1">
									<xsl:value-of select="concat('&#10;',.)"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:call-template name="template_ftx">
							<xsl:with-param name="parent_tag_name">PCRLIC_TAX_S</xsl:with-param>
							<xsl:with-param name="pk_tag_name">TAXSUBKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_name1">TAXKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_name2">MAINKEY</xsl:with-param>
							<xsl:with-param name="fk_tag_value1" select="$sTAXKEY"/>
							<xsl:with-param name="fk_tag_value2" select="$sMAINKEY"/>
							<xsl:with-param name="qul_tag_value">AAA</xsl:with-param>
							<xsl:with-param name="tag_name">Text</xsl:with-param>
							<xsl:with-param name="value" select="$sTAXFTXAAA"/>
							<xsl:with-param name="delimiter" select="'&#10;'"/>
							<xsl:with-param name="sequence">1</xsl:with-param>
							<xsl:with-param name="sequence_tag_name">Sort</xsl:with-param>
							<xsl:with-param name="sequence_tag_value">1</xsl:with-param>
							<xsl:with-param name="rows">5</xsl:with-param>
						</xsl:call-template>
						<!-- 규격 끝 -->
					</xsl:element>
				</xsl:for-each>
				<!-- 세금계산서 끝 -->
				<!-- 발급조건 시작 -->
				<xsl:variable name="sFTXAAX">
					<xsl:for-each select="/PCRLIC/DataArea/Summary/PurchaseConfirmationIssueDescriptionText">
						<xsl:if test="position() = 1">
							<xsl:value-of select="."/>
						</xsl:if>
						<xsl:if test="position() >1">
							<xsl:value-of select="concat('&#10;',.)"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:call-template name="template_ftx">
					<xsl:with-param name="parent_tag_name">PCRLIC_S</xsl:with-param>
					<xsl:with-param name="pk_tag_name">SUBKEY</xsl:with-param>
					<xsl:with-param name="fk_tag_name1">MAINKEY</xsl:with-param>
					<xsl:with-param name="fk_tag_name2"/>
					<xsl:with-param name="fk_tag_value1" select="$sMAINKEY"/>
					<xsl:with-param name="fk_tag_value2"/>
					<xsl:with-param name="qul_tag_value">AAX</xsl:with-param>
					<xsl:with-param name="tag_name">Text</xsl:with-param>
					<xsl:with-param name="value" select="$sFTXAAX"/>
					<xsl:with-param name="delimiter" select="'&#10;'"/>
					<xsl:with-param name="sequence">1</xsl:with-param>
					<xsl:with-param name="sequence_tag_name">Sort</xsl:with-param>
					<xsl:with-param name="sequence_tag_value">1</xsl:with-param>
					<xsl:with-param name="rows">5</xsl:with-param>
				</xsl:call-template>
				<!-- 발급조건 끝 -->
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
