<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xsl:template match="/">
		<xsl:element name="EDIADV">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/EDIADVBE/DocumentHeader/SenderID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/EDIADVBE/DocumentHeader/RecipientID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/EDIADVBE/DocumentHeader/DocumentNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">
					<xsl:value-of select="/EDIADVBE/DocumentHeader/DocumentType"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName"><xsl:text>EDIADV</xsl:text></xsl:element>
					<xsl:element name="DocumentCode">BE</xsl:element>
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/EDIADVBE/EDIADV_M/DocId"/>
					</xsl:element>
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/EDIADVBE/EDIADV_M/FunctionCode"/>
					</xsl:element>
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/EDIADVBE/EDIADV_M/ResponseTypeCode"/>
					</xsl:element>
					<xsl:element name="SenderParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/SP_OrgName"/>
							</xsl:element>
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/SP_OrgId1"/>
							</xsl:element>
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/SP_OrgId2"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Branch">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/SP_BranchOrgName"/>
							</xsl:element>	
						</xsl:element>	
						<xsl:element name="AuthenticationValueIdentifier">
							<xsl:value-of select="/EDIADVBE/EDIADV_M/SP_AuthValueId"/>
						</xsl:element>	
					</xsl:element>
					<xsl:element name="RecipientParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/RP_OrgName"/>
							</xsl:element>
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/EDIADVBE/EDIADV_M/RP_OrgId"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="AuthenticationValueIdentifier">
							<xsl:value-of select="/EDIADVBE/EDIADV_M/RP_AuthValueId"/>
						</xsl:element>	
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
