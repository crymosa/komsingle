<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" version="1.0" extension-element-prefixes="date js" exclude-result-prefixes="str">
  <xalan:component prefix="js" functions="getKey">
    <xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
  </xalan:component>
  <xsl:template match="/">
    <xsl:element name="APPPCR">
      <xsl:element name="ApplicationArea">
        <xsl:element name="ApplicationAreaSenderIdentifier">
          <xsl:value-of select="/APPPCR2CG/APPPCR_M/SRID_M/LocalID"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaReceiverIdentifier">
          <xsl:value-of select="/APPPCR2CG/APPPCR_M/SRID_M/PartnerID"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaDetailSenderIdentifier">
          <xsl:value-of select="/APPPCR2CG/APPPCR_M/SRID_M/LocalDivisionID"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaDetailReceiverIdentifier">
          <xsl:value-of select="/APPPCR2CG/APPPCR_M/SRID_M/PartnerDivisionID"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaCreationDateTime">
          <xsl:value-of select="substring(date:date(), 1, 4)"/>
          <xsl:value-of select="substring(date:date(), 6, 2)"/>
          <xsl:value-of select="substring(date:date(), 9, 2)"/>
          <xsl:value-of select="substring(date:time(), 1, 2)"/>
          <xsl:value-of select="substring(date:time(), 4, 2)"/>
          <xsl:value-of select="substring(date:time(), 7, 2)"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaMessageIdentifier">
          <xsl:value-of select="/APPPCR2CG/DocumentHeader/UniqueReferenceNo"/>
        </xsl:element>
        <xsl:element name="ApplicationAreaMessageTypeIndicator">APPPCR</xsl:element>
        <xsl:element name="ApplicationAreaMessageVersionText">2CG</xsl:element>
      </xsl:element>
      <xsl:element name="DataArea">
        <xsl:element name="Header">
          <xsl:element name="DocumentName">APPPCR</xsl:element>
          <!-- 전자문서의 코드 -->
          <xsl:element name="DocumentCode">2CG</xsl:element>
          <!-- 변경전 문서번호 -->
          <xsl:if test="/APPPCR2CG/APPPCR_M/BeforeIdentifier !=''">
            <xsl:element name="BeforeModifyPurchaseOrderIdentifier">
              <xsl:value-of select="/APPPCR2CG/APPPCR_M/BeforeIdentifier"/>
            </xsl:element>
          </xsl:if>
          <!-- 문서번호 -->
          <xsl:element name="DocumentIdentifier">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/DocumentIdentifier"/>
          </xsl:element>
          <!-- 문서기능 -->
          <xsl:element name="DocumentFunctionCode">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/FunctionCode"/>
          </xsl:element>
          <!-- 응답유형 -->
          <xsl:element name="DocumentResponseTypeCode">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/ResponseTypeCode"/>
          </xsl:element>
          <!-- 구매신청인 -->
          <xsl:element name="ApplicantParty">
            <xsl:element name="Organization">
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantName"/>
              </xsl:element>
              <xsl:element name="OrganizationCEOName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantCEOName"/>
              </xsl:element>
              <xsl:element name="OrganizationIdentifier">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantIdentifier"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="Address">
              <xsl:element name="AddressLine1Text">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantAddressLine1"/>
              </xsl:element>
              <xsl:if test="/APPPCR2CG/APPPCR_M/ApplicantAddressLine2 !=''">
                <xsl:element name="AddressLine2Text">
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantAddressLine2"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="/APPPCR2CG/APPPCR_M/ApplicantAddressLine3 !=''">
                <xsl:element name="AddressLine2Text">
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/ApplicantAddressLine3"/>
                </xsl:element>
              </xsl:if>
            </xsl:element>
          </xsl:element>
          <!-- 공급자 -->
          <xsl:element name="SupplierParty">
            <xsl:element name="Organization">
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierName"/>
              </xsl:element>
              <xsl:element name="OrganizationCEOName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierCEOName"/>
              </xsl:element>
              <xsl:element name="OrganizationIdentifier">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierIdentifier"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="Address">
              <xsl:element name="AddressLine1Text">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierAddressLine1"/>
              </xsl:element>
              <xsl:if test="/APPPCR2CG/APPPCR_M/SupplierAddressLine2 !=''">
                <xsl:element name="AddressLine2Text">
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierAddressLine2"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="/APPPCR2CG/APPPCR_M/SupplierAddressLine3 !=''">
                <xsl:element name="AddressLine2Text">
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/SupplierAddressLine3"/>
                </xsl:element>
              </xsl:if>
            </xsl:element>
            <xsl:element name="Contact">
              <xsl:element name="ContactEmailAccountText">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ContactEmailAccount"/>
              </xsl:element>
              <xsl:element name="ContactEmailDomainText">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ContactEmailDomain"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
          <!-- 구매(공급)원료/물품등의 용도명세 구분 -->
          <xsl:element name="MaterialUsageTypeCode">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/UsageTypeCode"/>
          </xsl:element>
          <xsl:element name="MaterialUsageDescriptionText">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/UsageDescription"/>
          </xsl:element>
		  <!--
          <xsl:element name="IssueClassificationCode">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/IssueClassificationCode"/>
          </xsl:element>
	      -->
        </xsl:element>
        <!-- 라인아이템 -->
        <xsl:for-each select="/APPPCR2CG/APPPCR_M/APPPCR_ITEM">
          <xsl:element name="Line">
            <!-- 일련품목번호 -->
            <xsl:element name="LineNumber">
              <xsl:value-of select="./LineNumber"/>
            </xsl:element>
            <xsl:element name="LineItem">
              <!-- HS 부호 -->
              <xsl:element name="Item">
                <xsl:element name="ClassId">
                  <xsl:element name="ClassIdHSIdentifier">
                    <xsl:value-of select="./HSCode"/>
                  </xsl:element>
                </xsl:element>
                <!-- 품목 -->
                <xsl:if test="./ItemName1 !=''">
                  <xsl:element name="ItemName">
                    <xsl:value-of select="./ItemName1"/>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="./ItemName2 !=''">
                  <xsl:element name="ItemName">
                    <xsl:value-of select="./ItemName2"/>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="./ItemName3 !=''">
                  <xsl:element name="ItemName">
                    <xsl:value-of select="./ItemName3"/>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="./ItemName4 !=''">
                  <xsl:element name="ItemName">
                    <xsl:value-of select="./ItemName4"/>
                  </xsl:element>
                </xsl:if>
                <xsl:if test="./ItemName5 !=''">
                  <xsl:element name="ItemName">
                    <xsl:value-of select="./ItemName5"/>
                  </xsl:element>
                </xsl:if>
                <!-- 품목 규격 -->
                <xsl:for-each select="./APPPCR_ITEM_S[TextQualifier='AAA']">
                  <xsl:element name="ItemDefinitionText">
                    <xsl:value-of select="./Text1"/>
                  </xsl:element>
                  <xsl:element name="ItemDefinitionText">
                    <xsl:value-of select="./Text2"/>
                  </xsl:element>
                  <xsl:element name="ItemDefinitionText">
                    <xsl:value-of select="./Text3"/>
                  </xsl:element>
                  <xsl:element name="ItemDefinitionText">
                    <xsl:value-of select="./Text4"/>
                  </xsl:element>
                  <xsl:element name="ItemDefinitionText">
                    <xsl:value-of select="./Text5"/>
                  </xsl:element>
                </xsl:for-each>
                <!-- 비고 -->
                <xsl:element name="Note">
                  <xsl:if test="./Description1 !=''">
                    <xsl:element name="NoteDescriptionText">
                      <xsl:value-of select="./Description1"/>
                    </xsl:element>
                  </xsl:if>
                  <xsl:if test="./Description2 !=''">
                    <xsl:element name="NoteDescriptionText">
                      <xsl:value-of select="./Description2"/>
                    </xsl:element>
                  </xsl:if>
                  <xsl:if test="./Description3 !=''">
                    <xsl:element name="NoteDescriptionText">
                      <xsl:value-of select="./Description3"/>
                    </xsl:element>
                  </xsl:if>
                  <xsl:if test="./Description4 !=''">
                    <xsl:element name="NoteDescriptionText">
                      <xsl:value-of select="./Description4"/>
                    </xsl:element>
                  </xsl:if>
                  <xsl:if test="./Description5 !=''">
                    <xsl:element name="NoteDescriptionText">
                      <xsl:value-of select="./Description5"/>
                    </xsl:element>
                  </xsl:if>
                </xsl:element>
              </xsl:element>
              <!-- 수량 -->
              <xsl:if test="./Quantity !=''">
                <xsl:element name="LineItemQuantity">
                  <xsl:attribute name="unitCode">
                    <xsl:value-of select="./QuantityUunitCode"/>
                  </xsl:attribute>
                  <xsl:value-of select="./Quantity"/>
                </xsl:element>
              </xsl:if>
              <!-- 금액 -->
              <xsl:element name="LineItemAmount">
                <xsl:attribute name="currency">
                  <xsl:value-of select="./AmountCurrency"/>
                </xsl:attribute>
                <xsl:value-of select="./Amount"/>
              </xsl:element>
              <!-- 금액(USD 금액 부기) -->
              <xsl:if test="./ForeignAmount !=''">
				  <xsl:element name="LineItemForeignAmount">
					<xsl:attribute name="currency">USD</xsl:attribute>
					<xsl:value-of select="./ForeignAmount"/>
				  </xsl:element>
			 </xsl:if>
              <!-- 단가(USD금액 부기) -->
              <xsl:if test="./ForeignBaseAmount !=''">
				  <xsl:element name="LineItemForeignBasePriceAmount">
					<xsl:attribute name="currency">USD</xsl:attribute>
					<xsl:value-of select="./ForeignBaseAmount"/>
				  </xsl:element>
              </xsl:if>
              <!-- 단가 -->
              <xsl:element name="BasePrice">
                <xsl:element name="BasePriceAmount">
                  <xsl:value-of select="./BasePriceAmount"/>
                </xsl:element>
                <xsl:element name="BasePriceBaseQuantity">
                  <xsl:attribute name="unitCode">
                    <xsl:value-of select="./BasePriceQuantityUnitCod"/>
                  </xsl:attribute>
                  <xsl:value-of select="./BasePriceQuantity"/>
                </xsl:element>
              </xsl:element>
              <!-- 구매(공급)일 -->
              <xsl:element name="LineItemPurchaseDate">
                <xsl:value-of select="./PurchaseDate"/>
              </xsl:element>
              <!-- 품목별 할인/할증/변동 내역 -->
              <xsl:if test="./AllowanceIndicator !=''">
                <xsl:element name="AllowanceOrCharge">
                  <xsl:element name="AllowanceOrChargeIndicator">
                    <xsl:value-of select="./AllowanceIndicator"/>
                  </xsl:element>
                  <xsl:if test="./AllowanceAmount !=''">
                    <xsl:element name="AllowanceOrChargeAmount">
                      <xsl:attribute name="currency">
						<xsl:value-of select="./AllowanceAmountCurrency"/>
					  </xsl:attribute>
                      <xsl:value-of select="./AllowanceAmount"/>
                    </xsl:element>
                  </xsl:if>
                  <xsl:if test="./AllowanceForeignAmount !=''">
                    <xsl:element name="AllowanceOrChargeAmount">
                      <xsl:attribute name="currency">USD</xsl:attribute>
                      <xsl:value-of select="./AllowanceForeignAmount"/>
                    </xsl:element>
                  </xsl:if>
                </xsl:element>
              </xsl:if>
            </xsl:element>
          </xsl:element>
        </xsl:for-each>
        <xsl:element name="Summary">
          <!-- 총수량 -->
          <xsl:element name="TotalQuantity">
            <xsl:attribute name="unitCode">
              <xsl:value-of select="/APPPCR2CG/APPPCR_M/TotalQuantityUunitCode"/>
            </xsl:attribute>
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/TotalQuantity"/>
          </xsl:element>
          <!-- 총금액 -->
          <xsl:element name="TotalAmount">
            <xsl:attribute name="currency">
              <xsl:value-of select="/APPPCR2CG/APPPCR_M/TotalAmountCurrency"/>
            </xsl:attribute>
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/TotalAmount"/>
          </xsl:element>
          <!-- 총금액(USD금액 부기) -->
          <xsl:if test="/APPPCR2CG/APPPCR_M/TotalForeignAmount !=''">
            <xsl:element name="TotalForeignAmount">
              <xsl:attribute name="currency">USD</xsl:attribute>
              <xsl:value-of select="/APPPCR2CG/APPPCR_M/TotalForeignAmount"/>
            </xsl:element>
          </xsl:if>
          <!-- 총 할인/할증/변동 내역 -->
          <xsl:if test="/APPPCR2CG/APPPCR_M/AllowanceIndicator !=''">
            <xsl:element name="AllowanceOrCharge">
              <xsl:element name="AllowanceOrChargeIndicator">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/AllowanceIndicator"/>
              </xsl:element>
              <xsl:if test="/APPPCR2CG/APPPCR_M/AllowanceAmount !=''">
                <xsl:element name="AllowanceOrChargeAmount">
					<xsl:attribute name="currency">
						<xsl:value-of select="./AllowanceAmountCurrency"/>
					</xsl:attribute>
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/AllowanceAmount"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="/APPPCR2CG/APPPCR_M/AllowanceForeignAmount !=''">
                <xsl:element name="AllowanceOrChargeAmount">
                  <xsl:attribute name="currency">USD</xsl:attribute>
                  <xsl:value-of select="/APPPCR2CG/APPPCR_M/AllowanceForeignAmount"/>
                </xsl:element>
              </xsl:if>
            </xsl:element>
          </xsl:if>
          <!-- 근거서류 시작  -->
          <xsl:for-each select="/APPPCR2CG/APPPCR_M/APPPCR_REF">
            <xsl:element name="ReferenceInformation">
              <!-- 근거서류명 및 번호 -->
              <xsl:element name="DocumentCode">
                <xsl:value-of select="./DocumentCode"/>
              </xsl:element>
              <xsl:element name="DocumentIdentifier">
                <xsl:value-of select="./DocumentIdentifier"/>
              </xsl:element>
              <xsl:element name="IssuingParty">
                <xsl:element name="Organization">
					<xsl:element name="OrganizationIdentifier">
						<xsl:value-of select="./Issue_OrgId"/>
					</xsl:element>
					<xsl:element name="OrganizationName">
						<xsl:value-of select="./Issue_OrgName"/>
					</xsl:element>
				</xsl:element>
              </xsl:element>
              <xsl:element name="Branch">
                <xsl:element name="OrganizationName">
					<xsl:value-of select="./Issue_BranchOrgName"/>
				  </xsl:element>
              </xsl:element>
              <xsl:element name="Address">
                <xsl:element name="CountryCode">
					<xsl:value-of select="./Issue_CountryCode"/>
				  </xsl:element>
              </xsl:element>
              <!--
              <xsl:element name="AmendmentTypeCode">
                <xsl:value-of select="./AmendmentTypeCode"/>
              </xsl:element>
              <xsl:element name="BasedDocumentIdentifier">
                <xsl:value-of select="./BasedDocumentID"/>
              </xsl:element>
              <xsl:element name="ConfirmBank">
                <xsl:element name="Organization">
                  <xsl:element name="OrganizationIdentifier">
                    <xsl:value-of select="./DocConfirmBankCode"/>
                  </xsl:element>
                  <xsl:element name="OrganizationName">
                    <xsl:value-of select="./DocConfirmBankName"/>
                  </xsl:element>
                </xsl:element> 
                <xsl:element name="Branch">
                  <xsl:element name="OrganizationName">
                    <xsl:value-of select="./DocConfirmBankBranch"/>
                  </xsl:element>
                </xsl:element>
              </xsl:element>
              -->
              <!-- HS 부호 -->
              <xsl:if test="./HSCode !=''">
                <xsl:element name="ClassIdHSIdentifier">
                  <xsl:value-of select="./HSCode"/>
                </xsl:element>
              </xsl:if>
              <!-- 품목 -->
              <xsl:if test="./ItemName1 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./ItemName1"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./ItemName2 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./ItemName2"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./ItemName3 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./ItemName3"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./ItemName4 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./ItemName4"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./ItemName5 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./ItemName5"/>
                </xsl:element>
              </xsl:if>
              <!-- 품목 규격 -->
              <xsl:for-each select="./APPPCR_REF_S[TextQualifier='AAA']">
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text1"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text2"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text3"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text4"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text5"/>
                </xsl:element>
              </xsl:for-each>
              <!-- 금액 -->
              <xsl:if test="./LCAmount !=''">
                <xsl:element name="LetterOfCreditAmount">
                  <xsl:attribute name="currency">
                    <xsl:value-of select="./LCAmountCurrency"/>
                  </xsl:attribute>
                  <xsl:value-of select="./LCAmount"/>
                </xsl:element>
              </xsl:if>
              <!--
              <xsl:if test="./RelatedAmount !=''">
                <xsl:element name="RelatedAmount">
                  <xsl:attribute name="currency">
                    <xsl:value-of select="./RelatedAmountCurrency"/>
                  </xsl:attribute>
                  <xsl:value-of select="./RelatedAmount"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./OriginalAmount !=''">
                <xsl:element name="OriginalAmount">
                  <xsl:attribute name="currency">
                    <xsl:value-of select="./OriginalAmountCurrency"/>
                  </xsl:attribute>
                  <xsl:value-of select="./OriginalAmount"/>
                </xsl:element>
              </xsl:if>
			-->
              <!-- 선적기일 -->
              <xsl:if test="./ShipingDate !=''">
                <xsl:element name="ShipmentShipingDate">
                  <xsl:value-of select="./ShipingDate"/>
                </xsl:element>
              </xsl:if>
            </xsl:element>
          </xsl:for-each>
          <!-- 근거서류 끝 -->
		  <!-- 세금계산서 시작  -->
          <xsl:for-each select="/APPPCR2CG/APPPCR_M/APPPCR_TAX">
            <xsl:element name="TaxInvoiceInformation">
              <!-- 근거서류명 및 번호 -->
              <xsl:element name="TaxInvoiceIdentifier">
                <xsl:value-of select="./TaxInvoiceId"/>
              </xsl:element>
              <xsl:element name="TaxInvoiceIssueDate">
                <xsl:value-of select="./TaxInvoiceIssueDate"/>
              </xsl:element>
			   <xsl:if test="./ChargeAmount !=''">
                <xsl:element name="ChargeAmount">
                  <xsl:attribute name="currency">KRW</xsl:attribute>
                  <xsl:value-of select="./ChargeAmount"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./TaxAmount !=''">
                <xsl:element name="TaxAmount">
                  <xsl:attribute name="currency">KRW</xsl:attribute>
                  <xsl:value-of select="./TaxAmount"/>
                </xsl:element>
              </xsl:if>
              <!-- 품목 -->
              <xsl:if test="./Tax_ItemName1 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./Tax_ItemName1"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./Tax_ItemName2 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./Tax_ItemName2"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./Tax_ItemName3 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./Tax_ItemName3"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./Tax_ItemName4 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./Tax_ItemName4"/>
                </xsl:element>
              </xsl:if>
              <xsl:if test="./Tax_ItemName5 !=''">
                <xsl:element name="ItemName">
                  <xsl:value-of select="./Tax_ItemName5"/>
                </xsl:element>
              </xsl:if>
              <!-- 품목 규격 -->
              <xsl:for-each select="./APPPCR_TAX_S[TextQualifier='AAA']">
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text1"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text2"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text3"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text4"/>
                </xsl:element>
                <xsl:element name="ItemDefinitionText">
                  <xsl:value-of select="./Text5"/>
                </xsl:element>
              </xsl:for-each>
              <!-- 금액 -->
              <xsl:if test="./Tax_ItemQuantity !=''">
                <xsl:element name="ItemQuantity">
                  <xsl:attribute name="unitCode">
				    <xsl:value-of select="./Tax_ItemQuantityunit"/>
				  </xsl:attribute>
				<xsl:value-of select="./Tax_ItemQuantity"/>
                </xsl:element>
              </xsl:if>           
            </xsl:element>
          </xsl:for-each>
          <!-- 세금계산서 끝 -->
          <!-- 신청인 전자서명 -->
          <xsl:element name="SignatureParty">
            <xsl:element name="Organization">
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SignatureName"/>
              </xsl:element>
              <xsl:element name="OrganizationCEOName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/SignatureCEOName"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="PartySignatureValueText">
              <xsl:value-of select="/APPPCR2CG/APPPCR_M/SignatureValue"/>
            </xsl:element>
          </xsl:element>
          <!-- 확인기관 -->
          <xsl:element name="ConfirmParty">
            <xsl:element name="Organization">
              <xsl:element name="OrganizationIdentifier">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ConfirmBankCode"/>
              </xsl:element>
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ConfirmBankName"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="Branch">
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/ConfirmBankBranch"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
          <!-- 발급은행
          <xsl:element name="IssuingBank">
            <xsl:element name="Organization">
              <xsl:element name="OrganizationIdentifier">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/IssuingBankCode"/>
              </xsl:element>
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/IssuingBankName"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="Branch">
              <xsl:element name="OrganizationName">
                <xsl:value-of select="/APPPCR2CG/APPPCR_M/IssuingBankBranch"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
          <xsl:element name="RepresentativePurchaseConfirmationIssueIdentifier">
            <xsl:value-of select="/APPPCR2CG/APPPCR_M/PurchaseConfirmIssueID"/>
          </xsl:element>
          -->
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
